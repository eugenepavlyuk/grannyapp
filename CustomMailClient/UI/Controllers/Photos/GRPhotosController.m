//
//  GRPhotosController.m
//  Granny
//
//  Created by Ievgen Pavliuk on 3/28/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "GRPhotosController.h"
#import "GRPhotoSliderController.h"
#import "UIImage+Border.h"
#import "BDGridCell.h"
#import "UIViewController+backgroundDim.h"

@interface GRPhotosController ()

@property (retain, nonatomic) IBOutlet UIView *popoverView;
@property (retain, nonatomic) IBOutlet UIButton *btnSlide;
@end

@implementation GRPhotosController

@synthesize photoContentView;

@synthesize headerImageView;

#pragma mark - Initialization

- (BOOL)prefersStatusBarHidden
{
    return YES;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.screenName = @"Photos Screen";
    
    UIImage *buttonBackground = nil;
    
    if (IS_IPAD)
    {
        buttonBackground = [[UIImage imageNamed:@"photo_header"] resizableImageWithCapInsets:UIEdgeInsetsMake(0, 20, 0, 20)];
    }
    else
    {
        buttonBackground = [[UIImage imageNamed:@"photo_header"] stretchableImageWithLeftCapWidth:74 topCapHeight:0];
    }
    
    self.headerImageView.image = buttonBackground;
    
    self.photoContentView.minimumPadding = 5.f;
}

- (IBAction)startButtonTapped
{
    GRPhotoSliderController *sliderController = [[GRPhotoSliderController alloc] init];
    sliderController.assets = assets;
    sliderController.selectedPhotoIndex = 0;
    [self.navigationController pushViewController:sliderController animated:YES];
    [sliderController release];
    [sliderController sliderOnBtnPressed:nil];
    
    [self.popoverView removeFromSuperview];
    [self removeBackgroundView];
}

- (IBAction)slideButtonTapped
{
    [self createBackgrondDimWithButton:self.btnSlide];

    [self.view addSubview:self.popoverView];
    self.popoverView.frame = CGRectMake(self.view.frame.size.width - self.popoverView.frame.size.width, 57, self.popoverView.bounds.size.width, self.popoverView.bounds.size.height);

}

- (IBAction)cancelButtonTapped
{
    [self.popoverView removeFromSuperview];
    [self removeBackgroundView];
}

- (void)startLoadingPhotoFromGallery 
{
//#if TARGET_IPHONE_SIMULATOR
//#else
    void (^assetEnumerator)(ALAsset *, NSUInteger, BOOL *) = ^(ALAsset *result, NSUInteger index, BOOL *stop) {
        if(result != NULL) {
            //            NSLog(@"See Asset: %@", result);
            [assets addObject:result];
        }
    };
    
    void (^assetGroupEnumerator)(ALAssetsGroup *, BOOL *) =  ^(ALAssetsGroup *group, BOOL *stop) {
    	if(group != nil) {
            [group enumerateAssetsUsingBlock:assetEnumerator];
        } else {
            [self.photoContentView reloadData];
        }
    };
    
    assets = [[NSMutableArray alloc] init];
    library = [[ALAssetsLibrary alloc] init];
    [library enumerateGroupsWithTypes:ALAssetsGroupSavedPhotos
                           usingBlock:assetGroupEnumerator
                         failureBlock: ^(NSError *error) {
                             NSLog(@"Failure");
                         }];
//#endif
 
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    [self startLoadingPhotoFromGallery];
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    self.headerImageView = nil;
    self.popoverView = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return UIInterfaceOrientationIsLandscape(interfaceOrientation);
}

- (void)dealloc 
{
    [photoContentView release];
    [assets release];
    
    self.headerImageView = nil;
    self.popoverView = nil;
    
    [_btnSlide release];
    [super dealloc];
}

- (IBAction)homeButtonTapped
{
    [self.navigationController popToRootViewControllerAnimated:YES];
}

#pragma mark - BDGridViewDataSource's methods

- (NSUInteger)gridViewCountOfCells:(BDGridView *)gridView
{
    return [assets count];
}

- (CGSize)gridViewSizeOfCell:(BDGridView *)gridView
{
    if (IS_IPAD)
    {
        CGFloat width = (gridView.bounds.size.width - 4 * gridView.minimumPadding)/ 5;
        
        return CGSizeMake(width, width);
    }
    else
    {
//        if(orientation == UIInterfaceOrientationLandscapeLeft || orientation == UIInterfaceOrientationLandscapeRight)
//        {
//        CGFloat width = (gridView.bounds.size.width - 3 * gridView.minimumPadding)/ 4;
//            return CGSizeMake(width, width);
//        }
//        else
//        {
            CGFloat width = (gridView.bounds.size.width - 2 * gridView.minimumPadding)/ 3;
        
            return CGSizeMake(width, width);
//        }
    }
    
    return CGSizeZero;
}

- (BDGridCell *)gridView:(BDGridView *)gridView cellForIndex:(NSUInteger)index
{
    ALAsset *asset = [assets objectAtIndex:index];
    UIImage *pImage = nil;

    if ([[[UIDevice currentDevice] systemVersion] floatValue] >= 5.f)
    {
        pImage = [[UIImage imageWithCGImage:[asset aspectRatioThumbnail]] imageWithBorderWidth:3 andColor:[UIColor whiteColor].CGColor];
    }
    else
    {
        pImage = [[UIImage imageWithCGImage:[asset thumbnail]] imageWithBorderWidth:3 andColor:[UIColor whiteColor].CGColor];
    }
    
    PhotoCell *cell = (PhotoCell*)[gridView dequeueCell];
    
    if (!cell)
    {
        cell = [[[PhotoCell alloc] initCell] autorelease];
    }
    
    cell.photoImageView.image = pImage;
    
    return cell;
}

#pragma mark - BDGridViewDelegate's methods

- (void)gridView:(BDGridView *)gridView didTapCell:(BDGridCell *)cell
{
    GRPhotoSliderController *sliderController = [[GRPhotoSliderController alloc] init];
    sliderController.assets = assets;
    sliderController.selectedPhotoIndex = cell.index;
    [self.navigationController pushViewController:sliderController animated:YES];
    [sliderController release];
}

@end
