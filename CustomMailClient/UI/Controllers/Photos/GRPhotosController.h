//
//  GRPhotosController.h
//  Granny
//
//  Created by Ievgen Pavliuk on 3/28/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BDGridView.h"


@interface GRPhotosController : GAITrackedViewController <BDGridViewDataSource, BDGridViewDelegate>
{
    NSMutableArray *assets;
    ALAssetsLibrary *library;
    
    BDGridView *photoContentView;
}

@property (nonatomic, retain) IBOutlet BDGridView *photoContentView;
@property (nonatomic, retain) IBOutlet UIImageView *headerImageView;

- (IBAction)homeButtonTapped;
- (IBAction)slideButtonTapped;
- (IBAction)startButtonTapped;
- (IBAction)cancelButtonTapped;
- (void)startLoadingPhotoFromGallery;

@end
