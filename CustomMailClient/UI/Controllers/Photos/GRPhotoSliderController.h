//
//  GRPhotoSliderController.h
//  Granny
//
//  Created by Ievgen Pavliuk on 3/28/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MessageUI/MFMailComposeViewController.h>

typedef void (^TouchesEventBlock)(NSSet * touches, UIEvent * event);

@interface WildcardGestureRecognizer : UIGestureRecognizer {
    TouchesEventBlock touchesBeganCallback;
}
@property (copy) TouchesEventBlock touchesBeganCallback;

@end

@interface GRPhotoSliderController : GAITrackedViewController <UIScrollViewDelegate, MFMailComposeViewControllerDelegate, UIPrintInteractionControllerDelegate>
{    
    UIScrollView     *scrollView;
    UIButton         *bSlider;
    
    NSMutableArray   *assets;
    NSInteger         selectedPhotoIndex;
    NSInteger         currentPage;
    
    CGFloat           superWidth;
    CGFloat           superHeight;
    BOOL              scrollFromRotation;
    BOOL              isSliderOn;
    
    NSTimer          *timer;
    
    NSTimer          *headerTimer;
    
    WildcardGestureRecognizer * tapInterceptor;
    
    UIPrintInteractionController *printInteraction;
}

@property (nonatomic, retain) IBOutlet UIScrollView     *scrollView;
@property (nonatomic, retain) IBOutlet UIButton     *bSlider;
@property (nonatomic, retain) IBOutlet UIButton     *actionButton;
@property (nonatomic, retain) NSMutableArray   *assets;
@property (nonatomic, assign) BOOL isSliderOn;
@property (nonatomic, assign) NSInteger   selectedPhotoIndex;

@property (nonatomic, retain) IBOutlet UIImageView *headerImageView;
@property (nonatomic, retain) IBOutlet UIView *headerView;

@property (nonatomic, retain) IBOutlet UIView *actionPopover;
@property (nonatomic, retain) IBOutlet UIView *slideshowPopover;

@property (nonatomic, retain) IBOutlet UIButton *startSlideshowButton;

- (IBAction)sliderOnBtnPressed:(id)sender;
- (IBAction)slideshowButtonTapped;
- (IBAction)homeButtonTapped;
- (IBAction)backButtonTapped;
- (IBAction)actionButtonTapped;
- (IBAction)emailButtonTapped;
- (IBAction)printButtonTapped;
- (IBAction)cancelButtonTapped;
- (IBAction)startButtonTapped;

- (void)loadScrollViewWithPage:(int)page;
- (void)removeNotVisibleViews:(NSInteger) page direction:(NSInteger)direction;
- (void)reloadDisplayedPhotoViews;
- (IBAction)runSlider;
- (IBAction)stopSlider;
- (IBAction)cancelSlideshowButtonTapped;

@end
