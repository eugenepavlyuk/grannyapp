//
//  GRPhotoSliderController.m
//  Granny
//
//  Created by Ievgen Pavliuk on 3/28/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "GRPhotoSliderController.h"
#import "GRPhotoView.h"
#import "Settings.h"
#import "DataManager.h"
#import "GRNewMessageController.h"
#import "GRMailAccountManager.h"
#import "TMailAccount.h"
#import "GRCommonAlertView.h"
#import "UIViewController+backgroundDim.h"

@interface GRPhotoSliderController()
{
    GRNewMessageController *newMessageController;
}

@property (nonatomic, retain) MFMailComposeViewController *mailController;
@end

@implementation WildcardGestureRecognizer

@synthesize touchesBeganCallback;

- (id) init
{
    if (self = [super init])
    {
        self.cancelsTouchesInView = NO;
    }
    
    return self;
}

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    if (touchesBeganCallback)
    {
        touchesBeganCallback(touches, event);
    }
    
    //[super touchesBegan:touches withEvent:event];
}

- (void)touchesCancelled:(NSSet *)touches withEvent:(UIEvent *)event
{
    //[super touchesCancelled:touches withEvent:event];
}

- (void)touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event
{
    //[super touchesEnded:touches withEvent:event];
}

- (void)touchesMoved:(NSSet *)touches withEvent:(UIEvent *)event
{
    //[super touchesMoved:touches withEvent:event];
}

- (void)reset
{
    
}

- (void)ignoreTouch:(UITouch *)touch forEvent:(UIEvent *)event
{
    
}

- (BOOL)canBePreventedByGestureRecognizer:(UIGestureRecognizer *)preventingGestureRecognizer
{
    return NO;
}

- (BOOL)canPreventGestureRecognizer:(UIGestureRecognizer *)preventedGestureRecognizer
{
    return NO;
}

@end

@implementation GRPhotoSliderController

@synthesize assets;
@synthesize scrollView;
@synthesize selectedPhotoIndex;
@synthesize bSlider;
@synthesize mailController;
@synthesize isSliderOn;
@synthesize headerImageView;
@synthesize headerView;

@synthesize actionPopover;

#pragma mark - Initialization

- (void)setIsSliderOn:(BOOL)bIsSliderOn
{
    isSliderOn = bIsSliderOn;
    
    self.actionButton.hidden = isSliderOn;
}

- (BOOL)prefersStatusBarHidden
{
    return YES;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.actionPopover.alpha = 0;
    self.slideshowPopover.alpha = 0;
    
    self.screenName = @"Photo Slider Screen";
    
    UIImage *buttonBackground = nil;
    
    if (IS_IPAD)
    {
        buttonBackground = [[UIImage imageNamed:@"photo_header"] resizableImageWithCapInsets:UIEdgeInsetsMake(0, 20, 0, 20)];
    }
    else
    {
        buttonBackground = [[UIImage imageNamed:@"photo_header"] stretchableImageWithLeftCapWidth:74 topCapHeight:0];
    }
    
    self.headerImageView.image = buttonBackground;

    self.scrollView.backgroundColor = [UIColor colorWithRed:0 green:0 blue:0 alpha:0.6f];
}

- (void)sendButtonTapped
{
    if ([MFMailComposeViewController canSendMail])
    {
        self.mailController = [[[MFMailComposeViewController alloc] init] autorelease];
        
        ALAsset *asset = [assets objectAtIndex:currentPage];
        
        UIImage *image = [UIImage imageWithCGImage:[[asset defaultRepresentation] fullScreenImage]];
        
        NSData *data = UIImagePNGRepresentation(image);
        
        [self.mailController addAttachmentData:data mimeType:@"image/png" fileName:[[asset defaultRepresentation] filename]];
        
        self.mailController.modalPresentationStyle = UIModalPresentationFormSheet;
        self.mailController.mailComposeDelegate = self;
        [self presentViewController:self.mailController animated:YES completion:^{
            
        }];
    }
    else
    {
        [[[[UIAlertView alloc] initWithTitle:nil 
                                     message:NSLocalizedString(@"Email can't be sent. Settings needs to be checked", @"Email error") 
                                    delegate:nil 
                           cancelButtonTitle:NSLocalizedString(@"OK", @"OK") 
                           otherButtonTitles:nil] autorelease] show];
    }
}

- (IBAction)homeButtonTapped
{
    [self.navigationController popToRootViewControllerAnimated:YES];
}

- (IBAction)backButtonTapped
{
    [self.navigationController popViewControllerAnimated:YES];
}

- (void) addPopoverView:(UIView *) view actionButton:(UIButton *) btn
{
    [self createBackgrondDimWithButton:btn];
    
    view.alpha = 1;
    [self.view addSubview:view];
    view.frame = CGRectMake(self.view.frame.size.width - view.frame.size.width, 57, view.frame.size.width, view.frame.size.height);
}


- (void) removePopoverView:(UIView *) view
{
    view.alpha = 0;
    [view removeFromSuperview];
    [self removeBackgroundView];
}

- (IBAction)actionButtonTapped
{
    [self addPopoverView:self.actionPopover actionButton:self.actionButton];
    
    self.actionButton.selected = YES;
}

- (IBAction)cancelButtonTapped
{
    [self removePopoverView:self.actionPopover];
    
    self.actionButton.selected = NO;
}

- (IBAction)slideshowButtonTapped
{
    [self addPopoverView:self.slideshowPopover actionButton:self.bSlider];
    
//    if (self.slideshowPopover.superview == nil)
//    {
//        [self.view addSubview:self.slideshowPopover];
//        
//        NSInteger y;
//        NSInteger x;
//        
//        if (IS_IPAD)
//        {
//            y = 60;
//            x = self.view.bounds.size.width - self.slideshowPopover.bounds.size.width - 20;
//        }
//        else
//        {
//            y = 30;
//            x = self.view.bounds.size.width - self.slideshowPopover.bounds.size.width;
//        }
//        
//        self.slideshowPopover.frame = CGRectMake(x, y, self.slideshowPopover.bounds.size.width, self.slideshowPopover.bounds.size.height);
//    }
//    else
//    {
//        [self.slideshowPopover removeFromSuperview];
//    }
}

- (IBAction)emailButtonTapped
{
    [self removePopoverView:self.actionPopover];
    
    self.actionButton.selected = NO;
    
    NSArray *accounts = [[GRMailAccountManager manager] getAccounts];
    
    if (accounts.count > 0)
    {
        TMailAccount *mailAccount = [accounts objectAtIndex:0];
        [GRMailServiceManager shared].currentAccount = mailAccount;
        
        if (newMessageController)
        {
            [newMessageController release];
            newMessageController = nil;
        }
        
        newMessageController = [[GRNewMessageController alloc] init];
        newMessageController.delegate = self;
        
        if (IS_IPAD)
            [self.view addSubview:newMessageController.view];
        else
            [self.navigationController pushViewController:newMessageController animated:YES];
        
        if (self.assets.count > 0)
        {
            [newMessageController mailAttachemntsSelectedWithImages:@[[self.assets objectAtIndex:currentPage]]];
        }
    }
    else
    {
        GRCommonAlertView *alert = [[[GRCommonAlertView alloc] initCommonAlert:NSLocalizedString(@"Email account was not found", @"Email account was not found") inView:self.view] autorelease];
        alert.delegate = nil;
    }
}

- (IBAction)printButtonTapped
{
    [self removePopoverView:self.actionPopover];
    
    self.actionButton.selected = NO;
    
    [self printDocument];
}

- (void)printDocument
{
	Class printInteractionController = NSClassFromString(@"UIPrintInteractionController");
    
    ALAsset *asset = [self.assets objectAtIndex:currentPage];
    
	if ((printInteractionController != nil) && [printInteractionController isPrintingAvailable])
	{
        ALAssetRepresentation *assetRepresentation = asset.defaultRepresentation;
        
		NSURL *fileURL = assetRepresentation.url; // Document file URL
        
		printInteraction = [printInteractionController sharedPrintController];
        
        Byte *buffer = (Byte*)malloc((size_t)assetRepresentation.size);
        NSUInteger buffered = [assetRepresentation getBytes:buffer fromOffset:0.0 length:(NSUInteger)assetRepresentation.size error:nil];
        NSData *data = [NSData dataWithBytesNoCopy:buffer length:buffered freeWhenDone:YES];
        
        if ([printInteractionController canPrintData:data] == YES) // Check first
		{
			UIPrintInfo *printInfo = [NSClassFromString(@"UIPrintInfo") printInfo];
            
			printInfo.duplex = UIPrintInfoDuplexLongEdge;
			printInfo.outputType = UIPrintInfoOutputGeneral;
			printInfo.jobName = @"Asset";
			printInteraction.printInfo = printInfo;
			printInteraction.printingItem = fileURL;
			printInteraction.showsPageRange = YES;
            printInteraction.delegate = self;
            
                [printInteraction presentAnimated:YES completionHandler:
                 ^(UIPrintInteractionController *pic, BOOL completed, NSError *error)
                 {
#ifdef DEBUG
                     if ((completed == NO) && (error != nil)) NSLog(@"%s %@", __FUNCTION__, error);
#endif
                 }];
		}
	}
}

- (void)printInteractionControllerWillPresentPrinterOptions:(UIPrintInteractionController *)printInteractionController
{
}

- (void)printInteractionControllerWillDismissPrinterOptions:(UIPrintInteractionController *)printInteractionController
{

}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    currentPage = selectedPhotoIndex;
    [self loadScrollViewWithPage:currentPage];
    [self adjustToInterfaceOrientation:[[UIApplication sharedApplication] statusBarOrientation]];
    
    [self enableTimer];
    
    tapInterceptor = [[WildcardGestureRecognizer alloc] init];
    
    tapInterceptor.touchesBeganCallback = ^(NSSet * touches, UIEvent * event) 
    {
        [self enableTimer];
        [self showHeader];
    };
    
    [self.scrollView addGestureRecognizer:tapInterceptor];
}

- (void)viewWillDisappear:(BOOL)animated 
{    
    [super viewWillDisappear:animated];
    [self disableTimer];
    
    [self.scrollView removeGestureRecognizer:tapInterceptor];
    tapInterceptor.touchesBeganCallback = nil;
    [tapInterceptor release];
    tapInterceptor = nil;
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.

    self.headerImageView = nil;
    self.headerView = nil;
    self.actionPopover = nil;
    self.slideshowPopover = nil;
    self.startSlideshowButton = nil;
    self.actionButton = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return UIInterfaceOrientationIsLandscape(interfaceOrientation);
}

- (void)willRotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)duration
{
    [self adjustToInterfaceOrientation:toInterfaceOrientation];
}

- (void)adjustToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation
{
    scrollFromRotation = YES;
    
    if(toInterfaceOrientation == UIInterfaceOrientationLandscapeLeft ||toInterfaceOrientation == UIInterfaceOrientationLandscapeRight)
    {
        if (IS_IPAD)
        {
//            superWidth = 1024;
//            superHeight = 768;
        }
        else
        {
            superWidth = 480;
            superHeight = 320;
        }
    }
    else
    {
        if (IS_IPAD)
        {
//            superWidth = 768;
//            superHeight = 960;
        }
        else
        {
            superWidth = 320;
            superHeight = 480;
        }
    }
 
    if (!IS_IPAD)
    {
        self.scrollView.frame = CGRectMake(0, 0, superWidth, superHeight);
    }
    
    self.scrollView.contentSize = CGSizeMake(self.scrollView.frame.size.width * [self.assets count], self.scrollView.frame.size.height);
    [self reloadDisplayedPhotoViews];
    [self.scrollView setContentOffset:CGPointMake(scrollView.frame.size.width *currentPage, 0) animated:NO];
}

- (void)reloadDisplayedPhotoViews
{
    CGRect frame = self.scrollView.frame;
    
    for (GRPhotoView * pView in [self.scrollView subviews])
    {
        NSInteger page = pView.tag-100;
        
        if (page >= 0)
        {
            frame.origin.x = frame.size.width * page;
            frame.origin.y = 0;
            pView.frame = frame;
        }
    }
}

- (void)disableTimer
{
    if (headerTimer)
    {
        if ([headerTimer isValid])
        {
            [headerTimer invalidate];
        }
        
        [headerTimer release];
        headerTimer = nil;
    }
}

- (void) hideHeader
{
    if (!self.actionPopover.alpha && !self.slideshowPopover.alpha)
    {
        [UIView animateWithDuration:0.3 animations:^(void) {
            
            self.headerView.alpha = 0;
            
        } completion:^(BOOL finished) {
            
        }];
    }
}

- (void) showHeader
{
    [UIView animateWithDuration:0.3 animations:^(void) {
        
        self.headerView.alpha = 1;
        
    } completion:^(BOOL finished) {
        
    }];
}

- (void)enableTimer
{
    [self disableTimer];
    
    headerTimer = [[NSTimer scheduledTimerWithTimeInterval:5.f target:self selector:@selector(timerFired:) userInfo:nil repeats:NO] retain];
}

- (void)timerFired:(NSTimer*)timer
{
    [self disableTimer];
    
    [self hideHeader];
}

- (void)dealloc
{
    self.scrollView.delegate = nil;
    [scrollView release];
    [assets release];
    [bSlider release];
    self.mailController = nil;
    
    self.headerImageView = nil;
    self.headerView = nil;
    self.actionPopover = nil;
    self.slideshowPopover = nil;
    self.startSlideshowButton = nil;
    self.actionButton = nil;
    [super dealloc];
}

#pragma mark - UIScrollViewDelegate

- (void)loadScrollViewWithPage:(int)page
{
    if (page < 0)
        return;
    
    if (page >= [self.assets count])
        return;
    
    GRPhotoView *photoView = (GRPhotoView *)[self.scrollView viewWithTag:page+100];
    
    if (photoView == nil) 
    {
        GRPhotoView *photoView = [[GRPhotoView alloc] initWithAsset:[assets objectAtIndex:page] frame:self.scrollView.frame];
        CGRect frame = self.scrollView.frame;
        frame.origin.x = frame.size.width * page;
        frame.origin.y = 0;
        photoView.frame = frame;
        photoView.tag = page+100;
        [self.scrollView addSubview:photoView];
        [photoView release];
    }
}


- (void)scrollViewDidScroll:(UIScrollView *)sender
{
    if (!scrollFromRotation)
    {
        CGFloat pageWidth = scrollView.frame.size.width;
        int page = floor((scrollView.contentOffset.x - pageWidth / 2) / pageWidth) + 1;
        
        NSInteger direction;
        
        if (page > currentPage)
        {
            direction = 1;
        }
        
        if (page < currentPage)
        {
            direction = 0;
        }
        
        currentPage = page;
        [self removeNotVisibleViews:page direction:direction];
        
        //NSLog(@"%i", currentPage);
        // load the visible page and the page on either side of it (to avoid flashes when the user starts scrolling)
        [self loadScrollViewWithPage:page - 1];
        [self loadScrollViewWithPage:page];
        [self loadScrollViewWithPage:page + 1];
    }
}

- (void)removeNotVisibleViews:(NSInteger) page direction:(NSInteger)direction
{
    GRPhotoView *photoView = nil;
    
    switch (direction)
    {
        case 0:
            photoView = (GRPhotoView *)[self.scrollView viewWithTag:page+2+100];
            break;
        case 1:
            photoView = (GRPhotoView *)[self.scrollView viewWithTag:page-2+100];
            break;
            
        default:
            break;
    }
    
    if (photoView)
    {
        [photoView removeFromSuperview];
    }
}

- (void)scrollViewWillBeginDragging:(UIScrollView *)scrollView
{
    scrollFromRotation = NO;
    
    if (isSliderOn)
    {
        [self sliderOnBtnPressed:nil];
    }
}

- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView
{
    
}

#pragma mark - Actions

- (IBAction)cancelSlideshowButtonTapped
{    
    [self removePopoverView:self.slideshowPopover];
}

- (IBAction)startButtonTapped
{
    [self removePopoverView:self.slideshowPopover];

    [self sliderOnBtnPressed:self.bSlider];
}

- (IBAction)sliderOnBtnPressed:(id)sender
{
    if (!IS_IPAD)
    {
        if (!isSliderOn)
        {
            self.bSlider.selected = YES;
            self.isSliderOn = YES;
            [self runSlider];
        }
        else
        {
            self.bSlider.selected = NO;
            self.isSliderOn = NO;
            [self stopSlider];
        }
    }
    else
    {
        if (!isSliderOn)
        {
            [self.startSlideshowButton setTitle:NSLocalizedString(@"Stop slideshow", @"Stop slideshow") forState:UIControlStateNormal];
            self.bSlider.selected = YES;
            self.isSliderOn = YES;
            [self runSlider];
        }
        else
        {
            [self.startSlideshowButton setTitle:NSLocalizedString(@"Start slideshow", @"Start slideshow") forState:UIControlStateNormal];
            self.isSliderOn = NO;
            self.bSlider.selected = NO;
            [self stopSlider];
        }
    }
}


#pragma mark - Slider 

- (void)runSlider
{    
    Settings *settings = [[DataManager sharedInstance] getEntityWithName:kSettingsEntityName];
        
    NSInteger time = [settings.slidingTimeSeconds intValue] + [settings.slidingTimeMinutes intValue] * 60;
    
    timer = [NSTimer scheduledTimerWithTimeInterval:time
                                             target:self
                                           selector:@selector(moveSlide)
                                           userInfo:nil
                                            repeats:YES];
}

- (void)moveSlide
{
    scrollFromRotation = NO;
    
    if (currentPage < [assets count] - 1)
    {
        currentPage++;
        
        [self.scrollView setContentOffset:CGPointMake(scrollView.frame.size.width*currentPage, 0) animated:YES];
        //[self loadScrollViewWithPage:currentPage];
    }
}

- (void)stopSlider
{
    if (timer)
    {
        [timer invalidate];
        timer = nil;
    }
}

#pragma mark -
#pragma mark MFMailComposeViewControllerDelegate's methods

- (void)mailComposeController:(MFMailComposeViewController *)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError *)error
{
    [self.mailController dismissViewControllerAnimated:YES completion:^{
        
    }];
}

@end
