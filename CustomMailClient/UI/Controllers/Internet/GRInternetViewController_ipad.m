//
//  GRInternetViewController_ipad.m
//  GrannyApp
//
//  Created by Eugene Pavlyuk on 7/22/12.
//  Copyright (c) 2012 Home. All rights reserved.
//

#import "GRInternetViewController_ipad.h"

@interface GRInternetViewController_ipad ()

@end

@implementation GRInternetViewController_ipad

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    
    if (self)
    {
        // Custom initialization
    }
    
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
}

//- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
//{
//	return YES;
//}

@end
