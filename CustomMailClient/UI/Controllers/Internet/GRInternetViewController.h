//
//  GRInternetViewController.h
//  GrannyApp
//
//  Created by Eugene Pavlyuk on 7/22/12.
//  Copyright (c) 2012 Home. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface WebPage : NSObject <NSCoding>

@property (nonatomic, copy) NSURL *url;
@property (nonatomic, copy) NSString *path;
@property (nonatomic, copy) NSDate *date;

@end

typedef enum {
    BS_NORMAL = 0,
    BS_FAVORITE,
    BS_HISTORY
} BrowserState;

@class TreeListModel;
@class GRGridView;
@class GRNewMessageController;

@interface GRInternetViewController : GAITrackedViewController

@property (nonatomic, retain) IBOutlet UIView *toolbar;
@property (nonatomic, retain) IBOutlet UIWebView *mainWebView;
@property (nonatomic, retain) IBOutlet UIView *actionPopover;
@property (nonatomic, retain) IBOutlet UIView *editPopover;
@property (nonatomic, retain) IBOutlet UIButton *actionButton;
@property (nonatomic, retain) IBOutlet UIButton *clearHistoryButton;
@property (nonatomic, retain) IBOutlet UIButton *favoriteButton;
@property (nonatomic, retain) IBOutlet UIButton *historyButton;
@property (nonatomic, retain) IBOutlet GRGridView *gridView;
@property (nonatomic, retain) IBOutlet UILabel *mainTitleLabel;
@property (nonatomic, retain) IBOutlet UIActivityIndicatorView *spinner;
@property (nonatomic, retain) IBOutlet UITextField *addressTextField;
@property (nonatomic, retain) IBOutlet UIButton *secretButton;
@property (nonatomic, retain) IBOutlet UIButton *editPopupButton;
@property (nonatomic, retain) IBOutlet UIView *inputView;
@property (nonatomic, retain) IBOutlet UIDatePicker *datePicker;

@property (nonatomic, retain) TreeListModel *model;
@property (nonatomic, retain) NSString *externalLink;

@property (nonatomic, retain) UIPrintInteractionController *printInteraction;

@property (nonatomic, assign) NSInteger indexOfEditedCell;
@property (nonatomic, retain) GRNewMessageController *newMessageController;

- (IBAction)emailButtonTapped;
- (IBAction)addToFavoriteButtonTapped;
- (IBAction)printButtonTapped;
- (IBAction)homeButtonTapped;
- (IBAction)cancelButtonTapped;
- (IBAction)secretButtonTapped;
- (IBAction)setButtonTapped;
- (IBAction)favButtonTapped;
- (IBAction)historyButtonTapped;
- (IBAction)actionButtonTapped;

- (IBAction)clearHistoryButtonTapped;

- (void)startLoadAddress:(NSString*)link;

@end
