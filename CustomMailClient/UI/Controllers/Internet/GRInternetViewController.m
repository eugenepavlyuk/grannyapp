//
//  GRInternetViewController.m
//  GrannyApp
//
//  Created by Eugene Pavlyuk on 7/22/12.
//  Copyright (c) 2012 Home. All rights reserved.
//

#import "GRInternetViewController.h"
#import "GRGridCell.h"
#import "DataManager.h"
#import "InternetPages.h"
#import "TimelineTableViewCell.h"
#import "TreeListModel.h"
#import <objc/runtime.h>
#import "GRMailAccountManager.h"
#import "GRCommonAlertView.h"
#import "GRGridView.h"
#import "AlertView.h"
#import <MessageUI/MFMailComposeViewController.h>
#import "GRNewMessageController.h"
#import "WYPopoverController.h"
#import "DeleteRecipientView.h"


@implementation NSURL (IsEqualTesting)

- (BOOL) isEqualToURL:(NSURL*)otherURL;
{
	return ([[self absoluteString] isEqual:[otherURL absoluteString]]);
}

- (BOOL) isEqualToStringURL:(NSString*)otherURL;
{
	return ([[self absoluteString] isEqual:otherURL]);
}


@end

//@interface MyObject : NSObject
//
//- (double)estimatedProgress;
//
//@end
//
//@implementation MyObject
//@end

@implementation WebPage

@synthesize path;
@synthesize url;
@synthesize date;

- (void)dealloc
{
    self.path = nil;
    self.url = nil;
    self.date = nil;
    [super dealloc];
}

- (id)initWithCoder:(NSCoder *)coder 
{
    if((self = [super init])) 
    {
		self.path = [coder decodeObjectForKey:@"path"];
        self.url = [coder decodeObjectForKey:@"url"];
        self.date = [coder decodeObjectForKey:@"date"];
    }
    
    return self;
}

- (void)encodeWithCoder:(NSCoder *)aCoder 
{
	[aCoder encodeObject:self.path forKey:@"path"];
    [aCoder encodeObject:self.url forKey:@"url"];
    [aCoder encodeObject:self.date forKey:@"date"];
}

- (NSComparisonResult)sortByDates:(WebPage*)page
{
    return [page.date compare:self.date];
}

- (NSString*) description
{
    return [NSString stringWithFormat:@"%@", self.date];
}

@end

@interface NSDate(String)

- (NSString*)getMonthName;
- (NSString*)getYearName;
- (NSString*)getDayMonthName;

@end

@implementation NSDate(String)

- (NSString*)getMonthName
{
    NSDateFormatter *df = [[[NSDateFormatter alloc] init] autorelease];
    
    [df setDateFormat:@"MMMM"];
    
    return [df stringFromDate:self];
}

- (NSString*)getYearName
{
    NSDateFormatter *df = [[[NSDateFormatter alloc] init] autorelease];
    
    [df setDateFormat:@"yyyy"];
    
    return [df stringFromDate:self];
}

- (NSString*)getDayMonthName
{
    NSDateFormatter *df = [[[NSDateFormatter alloc] init] autorelease];
    
    [df setDateFormat:@"d MMM"];
    
    return [df stringFromDate:self];
}

@end


@interface GRInternetViewController () <UITextFieldDelegate, UIWebViewDelegate,
GRGridViewDataSource, GRGridViewDelegate, WYPopoverControllerDelegate,
MFMailComposeViewControllerDelegate, UIPrintInteractionControllerDelegate,
UITableViewDelegate, UITableViewDataSource, AlertViewDelegate>

@property (nonatomic, retain) NSMutableArray *historyArray;
@property (nonatomic, retain) NSMutableArray *favoriteArray;
@property (nonatomic, retain) NSMutableArray *readingListArray;
@property (nonatomic, retain) NSMutableArray *currentHistoryArray;

@property (nonatomic, retain) MFMailComposeViewController *mailController;

@property (nonatomic, assign) BrowserState browserState;
@property (nonatomic, assign) BOOL editMode;

@property (nonatomic, assign) BOOL newPage;

@property (nonatomic, retain) WYPopoverController *alertPopoverController;

@end

@implementation GRInternetViewController
{
    UIButton *stopReloadButton;
}

- (void)setupPopoverAppearance
{
    WYPopoverBackgroundView *popoverAppearance = [WYPopoverBackgroundView appearance];
    
    [popoverAppearance setOverlayColor:[UIColor colorWithWhite:0.1f alpha:0.6f]];
    [popoverAppearance setOuterCornerRadius:20];
    [popoverAppearance setOuterShadowBlurRadius:4];
    [popoverAppearance setOuterShadowColor:[UIColor colorWithWhite:0.3f alpha:0.6f]];
    [popoverAppearance setOuterShadowOffset:CGSizeMake(4, 4)];
    
    [popoverAppearance setGlossShadowColor:[UIColor clearColor]];
    [popoverAppearance setGlossShadowOffset:CGSizeMake(0, 0)];
    
    [popoverAppearance setBorderWidth:0];
    [popoverAppearance setArrowHeight:0];
    [popoverAppearance setArrowBase:0];
    
    [popoverAppearance setInnerCornerRadius:20];
    [popoverAppearance setInnerShadowBlurRadius:0];
    [popoverAppearance setInnerShadowColor:[UIColor clearColor]];
    [popoverAppearance setInnerShadowOffset:CGSizeMake(0, 0)];
    
    [popoverAppearance setFillTopColor:[UIColor whiteColor]];
    [popoverAppearance setFillBottomColor:[UIColor whiteColor]];
    [popoverAppearance setOuterStrokeColor:[UIColor clearColor]];
    [popoverAppearance setInnerStrokeColor:[UIColor clearColor]];
}

- (BOOL)prefersStatusBarHidden
{
    return YES;
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    
    if (self) 
    {
        _newPage = NO;
        
        InternetPages *internetPages = (InternetPages*)[[DataManager sharedInstance] getEntityWithName:@"InternetPages"];
        
        if (!internetPages)
        {
            internetPages = [[DataManager sharedInstance] createEntityWithName:@"InternetPages"];
            internetPages.history = [NSArray array];
            internetPages.readinglist = [NSArray array];
            internetPages.favorites = [NSArray array];
            
            [[DataManager sharedInstance] save];
        }
        
        self.historyArray = [NSMutableArray arrayWithArray:internetPages.history];
        self.favoriteArray = [NSMutableArray arrayWithArray:internetPages.favorites];
        self.readingListArray = [NSMutableArray arrayWithArray:internetPages.readinglist];
        
        self.currentHistoryArray = [NSMutableArray array];
        
        _editMode = NO;
        _indexOfEditedCell = -1;
        
        [[NSNotificationCenter defaultCenter] addObserver:self 
                                                 selector:@selector(deleteButtonTapped:) 
                                                     name:@"CancelButtonTappedNotification" 
                                                   object:nil];
        
        [[NSNotificationCenter defaultCenter] addObserver:self
                                                 selector:@selector(applicationDidEnterBackground)
                                                     name:UIApplicationDidEnterBackgroundNotification object:nil];
        
    }
    
    return self;
}

- (void)applicationDidEnterBackground
{
    [self saveBrowserData];
}

- (NSMutableArray*)getCurrentMonthDatesFromArray:(NSArray*)allDates
{
    NSMutableArray *array = [NSMutableArray array];
    
    NSCalendar *calendar = [AppDelegate getInstance].globalCalendar;
    
    NSDateComponents *dateComponents = [calendar components:DATE_COMPONENTS fromDate:[NSDate date]];
    
    NSInteger currentYear = [dateComponents year];
    NSInteger currentMonth = [dateComponents month];
    
    for (WebPage *webPage in allDates)
    {
        NSDate *creationDate = webPage.date;
        
        dateComponents = [calendar components:DATE_COMPONENTS fromDate:creationDate];
        
        NSInteger year = [dateComponents year];
        
        if (year != currentYear)
        {
            return array;
        }
        
        NSInteger month = [dateComponents month];
        
        if (month != currentMonth)
        {
            return array;
        }
        
        if (month == currentMonth && currentYear == year)
        {
            [array addObject:webPage];
        }
    }
    
    return array;
}

- (NSMutableArray*)getThreeMonthDatesFromArray:(NSArray*)allDates
{
    NSMutableArray *array = [NSMutableArray array];
    
    if (![allDates count])
    {
        return array;
    }
    
    NSCalendar *calendar = [AppDelegate getInstance].globalCalendar;
    
    NSDateComponents *dateComponents = [calendar components:DATE_COMPONENTS fromDate:[NSDate date]];
    
    NSInteger currentYear = [dateComponents year];
    
    WebPage *webPage = [allDates objectAtIndex:0];
    
    dateComponents = [calendar components:DATE_COMPONENTS fromDate:webPage.date];
    
    NSInteger year = [dateComponents year];
    
    if (year != currentYear)
    {
        return array;
    }
    
    NSInteger monthToCheck = [dateComponents month];
    NSInteger startMonth = monthToCheck;
    
    int i = 0;
    
    do 
    {
        if (i >= [allDates count])
        {
            return array;
        }
        
        WebPage *webPage = [allDates objectAtIndex:i];
        
        NSDate *creationDate = webPage.date;
        
        dateComponents = [calendar components:DATE_COMPONENTS fromDate:creationDate];
        
        year = [dateComponents year];
        
        if (year != currentYear)
        {
            return array;
        }
        
        NSInteger month = [dateComponents month];
                
        if (month == monthToCheck)
        {
            [array addObject:webPage];
            i++;
        }
        else
        {
            monthToCheck--;
        }
    } 
    while (monthToCheck > 0 && monthToCheck + 3 > startMonth);
    
    return array;
}

- (NSMutableArray*)getLastSevenDaysFromArray:(NSMutableArray*)items
{
    NSMutableArray *array = [NSMutableArray array];
    
    if (![items count])
    {
        return array;
    }
    
    NSMutableArray *itemsToRemove = [NSMutableArray array];
    
    NSCalendar *calendar = [AppDelegate getInstance].globalCalendar;
    
    NSDateComponents *dateComponents = [calendar components:DATE_COMPONENTS fromDate:[NSDate date]];
    
    NSInteger currentDay = [dateComponents day];

    int i = 0;
    
    NSInteger dayToCheck = currentDay;
    
    HistoryItem *item = nil;
    
    do 
    {
        if (i >= [items count])
        {
            [items removeObjectsInArray:itemsToRemove];
            
            return array;
        }
        
        WebPage *webPage = [items objectAtIndex:i];
        
        dateComponents = [calendar components:DATE_COMPONENTS fromDate:webPage.date];
        
        NSInteger day = [dateComponents day];
        
        if (day == dayToCheck)
        {
            if (!item)
            {
                item = [[HistoryItem new] autorelease];
                
                item.key = [webPage.date getDayMonthName];
                item.date = webPage.date;
                
                [array addObject:item];
            }
            
            [item.pages addObject:webPage];
            
            [itemsToRemove addObject:webPage];
            
            i++;
        }
        else
        {
            item = nil;
            dayToCheck--;
        }
        
    } while (dayToCheck > 0 && (dayToCheck + 7 > currentDay));
    
    [items removeObjectsInArray:itemsToRemove];
    
    return array;
}

- (void)formMonth:(HistoryItem*)item withDaysInArray:(NSMutableArray*)monthArray
{    
    if (![monthArray count])
    {
        return ;
    }
    
    WebPage *webPage = [monthArray objectAtIndex:0];
    
    NSCalendar *calendar = [AppDelegate getInstance].globalCalendar;
    
    NSDateComponents *dateComponents = [calendar components:DATE_COMPONENTS fromDate:webPage.date];
    
    NSInteger currentDay = [dateComponents day];
    
    int i = 0;
    
    NSInteger dayToCheck = currentDay;
    
    NSMutableArray *dayArray = [NSMutableArray array];
    
    do 
    {
        if (i >= [monthArray count])
        {
            return ;
        }
        
        webPage = [monthArray objectAtIndex:i];
        
        dateComponents = [calendar components:DATE_COMPONENTS fromDate:webPage.date];
        
        NSInteger day = [dateComponents day];
        
        if (day == dayToCheck)
        {
            [dayArray addObject:webPage];
            
            i++;
        }
        
        if (day != dayToCheck || i == [monthArray count])
        {
            HistoryItem *dayItem = [[HistoryItem new] autorelease];
            
            dayItem.date = webPage.date;
            dayItem.key = [webPage.date getDayMonthName];
            
            [item.children addObject:dayItem];
            
            [dayItem.pages addObjectsFromArray:dayArray];
            
            dayToCheck = day;
            
            [dayArray removeAllObjects];
        }
        
    } while (i < [monthArray count]);
    
    return ;
}

- (void)formYear:(HistoryItem*)item withDaysInArray:(NSMutableArray*)yearArray
{    
    if (![yearArray count])
    {
        return ;
    }
    
    WebPage *webPage = [yearArray objectAtIndex:0];
    
    NSCalendar *calendar = [NSCalendar currentCalendar];
    
    NSDateComponents *dateComponents = [calendar components:DATE_COMPONENTS fromDate:webPage.date];
    
    NSInteger currentMonth = [dateComponents month];
    
    int i = 0;
    
    NSInteger monthToCheck = currentMonth;
    
    NSMutableArray *monthArray = [NSMutableArray array];
    
    do 
    {
        if (i >= [yearArray count])
        {
            return ;
        }
        
        webPage = [yearArray objectAtIndex:i];
        
        dateComponents = [calendar components:DATE_COMPONENTS fromDate:webPage.date];
        
        NSInteger month = [dateComponents month];
        
        if (month == monthToCheck)
        {
            [monthArray addObject:webPage];
            
            i++;
        }
        
        if (month != monthToCheck || i == [yearArray count])
        {
            HistoryItem *monthItem = [[HistoryItem new] autorelease];
            
            monthItem.date = webPage.date;
            monthItem.key = [webPage.date getMonthName];
            
            [item.children addObject:monthItem];
            
            [self formMonth:monthItem withDaysInArray:yearArray];
            
            monthToCheck = month;
            
            [yearArray removeAllObjects];
        }
        
    } while (i < [yearArray count]);
    
    return ;
}

- (NSMutableArray*)getAllYearsHistoryItems:(NSArray*)items
{
    NSMutableArray *array = [NSMutableArray array];
    
    if (![items count])
    {
        return array;
    }
    
    WebPage *webPage = [items objectAtIndex:0];
    
    NSCalendar *calendar = [NSCalendar currentCalendar];
    
    NSDateComponents *dateComponents = [calendar components:DATE_COMPONENTS fromDate:webPage.date];
    
    NSInteger currentYear = [dateComponents year];
    
    int i = 0;
    
    NSInteger yearToCheck = currentYear;
    
    NSMutableArray *yearArray = [NSMutableArray array];
    
    do 
    {
        if (i >= [items count])
        {
            return array;
        }
        
        webPage = [items objectAtIndex:i];
        
        dateComponents = [calendar components:DATE_COMPONENTS fromDate:webPage.date];
        
        NSInteger year = [dateComponents year];
        
        if (year == yearToCheck)
        {
            [yearArray addObject:webPage];
            
            i++;
        }
        
        if (i == [items count] || year != yearToCheck)
        {
            HistoryItem *yearItem = [[HistoryItem new] autorelease];
            
            yearItem.date = webPage.date;
            yearItem.key = [webPage.date getYearName];
            
            [array addObject:yearItem];
            
            [self formYear:yearItem withDaysInArray:yearArray];
            
            yearToCheck = year;
            
            [yearArray removeAllObjects];
        }
        
    } while (i <= [items count]);
    
    return array;    
}

- (NSMutableArray*)getLastThreeMonthsHistoryItems:(NSArray*)items
{
    NSMutableArray *array = [NSMutableArray array];
    
    if (![items count])
    {
        return array;
    }
    
    WebPage *webPage = [items objectAtIndex:0];
    
    NSCalendar *calendar = [NSCalendar currentCalendar];
    
    NSDateComponents *dateComponents = [calendar components:NSCalendarUnitMonth fromDate:webPage.date];
    
    NSInteger currentMonth = [dateComponents month];
    
    int i = 0;
    
    NSInteger monthToCheck = currentMonth;
    
    NSMutableArray *monthArray = [NSMutableArray array];
    
    do 
    {
        if (i >= [items count])
        {
            return array;
        }
        
        webPage = [items objectAtIndex:i];
        
        dateComponents = [calendar components:NSCalendarUnitMonth fromDate:webPage.date];
        
        NSInteger month = [dateComponents month];
        
        if (month == monthToCheck)
        {
            [monthArray addObject:webPage];
            
            i++;
        }
        else
        {
            HistoryItem *item = [[HistoryItem new] autorelease];
            
            item.date = webPage.date;
            item.key = [webPage.date getMonthName];
            
            [array addObject:item];
            
            [self formMonth:item withDaysInArray:monthArray];
            
            monthToCheck = month;
            
            [monthArray removeAllObjects];
        }
        
    } while (i < [items count]);
    
    return array;
}

- (NSMutableArray*)createTimelineTree
{
    NSMutableArray *array = [NSMutableArray array];
    
    [self.historyArray sortUsingSelector:@selector(sortByDates:)];
    
    NSMutableArray *allItems = [NSMutableArray arrayWithArray:self.historyArray];
    
    // get all items for this month
    NSMutableArray *currentMonthArray = [self getCurrentMonthDatesFromArray:allItems];
    
    NSMutableArray *currentMonthArrayCopy = [NSMutableArray arrayWithArray:currentMonthArray];
    
    NSArray *sevenDaysHistoryItems = [self getLastSevenDaysFromArray:currentMonthArray];
    
    [array addObjectsFromArray:sevenDaysHistoryItems];
    
    [currentMonthArrayCopy removeObjectsInArray:currentMonthArray];
    
    [allItems removeObjectsInArray:currentMonthArrayCopy];
    // now in allItems array - all items without current 7 days.
    
    // get all items for 3 months
    NSMutableArray *threeMonthArray = [self getThreeMonthDatesFromArray:allItems];
    
    // remove these 3 month from array
    [allItems removeObjectsInArray:threeMonthArray];
    
    NSArray *threeMonthsHistoryItems = [self getLastThreeMonthsHistoryItems:threeMonthArray];
    
    [array addObjectsFromArray:threeMonthsHistoryItems];
    
    NSArray *allYearsArray = [self getAllYearsHistoryItems:allItems];
    
    [array addObjectsFromArray:allYearsArray];
    
    return array;
}

- (void)formHistoryArray
{
    NSMutableArray *array = nil;
    
    array = [self createTimelineTree];
    
    self.model = [[[TreeListModel alloc] initWithHistoryArray:array] autorelease];
}

- (void)releaseOutlets
{
    self.toolbar = nil;
    self.mainWebView = nil;
    self.actionPopover = nil;
    self.editPopover = nil;
    self.addressTextField = nil;
    self.gridView = nil;
    self.mainTitleLabel = nil;
    self.spinner = nil;
    self.historyButton = nil;
    self.favoriteButton = nil;
    self.secretButton = nil;
    self.inputView = nil;
    self.datePicker = nil;
    self.clearHistoryButton = nil;
    self.editPopupButton = nil;
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    [self.navigationController setNavigationBarHidden:YES animated:NO];
    if (self.externalLink) {
        [self startLoadAddress:self.externalLink];
    }
}

- (void)setupMainToolbar
{
    UIImage *headerBkgPattern = [UIImage imageNamed:@"inet_header_bkg"];
    UIColor *headerBkg = [UIColor colorWithPatternImage:headerBkgPattern];
    self.toolbar.backgroundColor = headerBkg;
    
    CGRect frame = _addressTextField.frame;
    frame.size.height = 33;
    frame.size.width = 520;
    _addressTextField.frame = frame;
    self.addressTextField.center = CGPointMake(self.historyButton.center.x + (self.actionButton.center.x - self.historyButton.center.x)/2, self.toolbar.center.y);
    self.addressTextField.frame = CGRectIntegral(self.addressTextField.frame);
    
    _addressTextField.layer.cornerRadius = _addressTextField.bounds.size.height/2;
    
    // reloadButton
    stopReloadButton = [UIButton buttonWithType:UIButtonTypeCustom];
//    [stopReloadButton setImage:[UIImage imageNamed:@"inet_reload_btn"] forState:UIControlStateNormal];
    [stopReloadButton setImage:[UIImage imageNamed:@"inet_cancel_btn"] forState:UIControlStateNormal];
    [stopReloadButton sizeToFit];
    
    CGRect bounds = stopReloadButton.bounds;
    bounds.size = CGSizeMake(_addressTextField.bounds.size.height, _addressTextField.bounds.size.height);
    stopReloadButton.bounds = bounds;
    
    stopReloadButton.showsTouchWhenHighlighted = YES;
    [stopReloadButton addTarget:self action:@selector(reloadOrStopButtonTapped) forControlEvents:UIControlEventTouchUpInside];
    _addressTextField.rightView = stopReloadButton;
    _addressTextField.rightViewMode = UITextFieldViewModeAlways;
    
    // search view
    UIImageView *searchIcon = [[[UIImageView alloc] initWithImage:[UIImage imageNamed:@"inet_search_btn"]] autorelease];
    
    bounds = searchIcon.bounds;
    bounds.size = CGSizeMake(bounds.size.width/2, _addressTextField.bounds.size.height);
    searchIcon.bounds = bounds;

    _addressTextField.leftView = searchIcon;
    _addressTextField.leftViewMode = UITextFieldViewModeAlways;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    
    self.screenName = @"Internet Browser Screen";
    
    self.gridView.minimumPadding = 0.f;
    
    [self setupMainToolbar];
    
    _newPage = YES;
    if (!self.externalLink) {
        [self.mainWebView loadRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:@"http://www.google.com"]]];
    }
    
    self.mainWebView.layer.borderColor = [UIColor blackColor].CGColor;
    self.mainWebView.layer.borderWidth = 1.f;
    
//    [self.mainWebView addObserver:self forKeyPath:@"loading" options:0 context:nil];
    
//    
//    id object = [self.mainWebView createSnapshotWithRect:CGRectZero];
//
//    NSLog(@"%@", object);
    
//    class_copyIvarList
    
//    int j = 0;
//    unsigned int ic = 0;
//    Ivar * ilist = class_copyIvarList(object_getClass(self.mainWebView), &ic);
//    NSLog(@"%d vars", ic);
//    for(j=0;j<ic;j++)
//        NSLog(@"var no #%d: %s", j, sel_getName(ivar_getName(ilist[j])));
    
//    id obj = [self.mainWebView performSelector:@selector(_documentView)];
//    
//    NSLog(@"%@", obj);
//    
//    int j = 0;
//    unsigned int ic = 0;
//    Method * ilist = class_copyMethodList(object_getClass(self.mainWebView), &ic);
//    NSLog(@"%d vars", ic);
//    for(j=0;j<ic;j++)
//        NSLog(@"var no #%d: %s", j, sel_getName(method_getName(ilist[j])));
//    
//    id internalWebView= [obj webView];       
//    
//    NSLog(@"%@", internalWebView);
//    
////    [internalWebView setMaintainsBackForwardList:NO];
////    [internalWebView setMaintainsBackForwardList:YES];
//    
//    int i=0;
//    unsigned int mc = 0;
//    Method * mlist = class_copyMethodList(object_getClass(internalWebView), &mc);
//    NSLog(@"%d methods", mc);
//    for(i=0;i<mc;i++)
//        NSLog(@"Method no #%d: %s", i, sel_getName(method_getName(mlist[i])));
    
//    id doc = [self.mainWebView performSelector:@selector(_documentView)];
//    id internalWebView = [doc performSelector:@selector(webView)]; 
    
//    NSLog(@"%@ ", doc);
//    NSLog(@"%@ ", internalWebView);
    
//    int i=0;
//    unsigned int mc = 0;
//    Method * mlist = class_copyMethodList(object_getClass([NSNotificationCenter defaultCenter]), &mc);
//    NSLog(@"%d methods", mc);
//    for(i=0;i<mc;i++)
//    {
//        Method m = mlist[i];
//        NSLog(@"Method no #%d: %s", i, sel_getName(method_getName(m)));
//        
//        char ret[ 256 ];
//        method_getReturnType( m, ret, 256 );
//        NSLog( @"Return type: %s", ret );
//    }
    
    NSString *WebViewProgressEstimateChangedNotification = @"WebProgressEstimateChangedNotification";
    
    [[NSNotificationCenter defaultCenter] addObserver:self 
                                             selector:@selector(progressEstimateChanged:) 
                                                 name:WebViewProgressEstimateChangedNotification 
                                               object:nil];
    

//    NSString *WebViewProgressFinishedNotification =         @"WebProgressFinishedNotification";
    
    [[NSNotificationCenter defaultCenter] addObserver:self 
                                             selector:@selector(progressFinishedChanged:) 
                                                 name:@"ProgressFinishedNotification" 
                                               object:nil];
    
#ifdef USE_SECRET_BUTTON
    self.secretButton.hidden = NO;
#endif
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(allNotifications:)
                                                 name:nil
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(historyItemAddedNotification:)
                                                 name:@"WebHistoryItemsAddedNotification"
                                               object:nil];
}

- (void)historyItemAddedNotification:(NSNotification*)note
{
    NSArray *webHistoryItems = [[note userInfo] objectForKey:@"WebHistoryItems"];
    
    for (id item in webHistoryItems)
    {
        NSRange range = [[item description] rangeOfString:@"> "];
        
        NSString *currentURL = [[item description] substringFromIndex:range.location+range.length];
        
        for (HistoryItem *item in self.model.items)
        {
            for (WebPage *webPage in item.pages)
            {
                if ([webPage.url isEqualToStringURL:currentURL])
                {
                    return ;
                }
            }
        }
        
        if ([currentURL isEqualToString:@"about:blank"])
        {
            return ;
        }
        
        if (_newPage)
        {
            _newPage = NO;
        }
        else
        {
            return ;
        }
        
        {
            WebPage *webPage = [self createWebPage];
            webPage.url = [NSURL URLWithString:currentURL];
            [self.historyArray insertObject:webPage atIndex:0];
            
            HistoryItem *historyItem = nil;
            
            if ([self.model.items count])
            {
                historyItem = [self.model.items objectAtIndex:0];
                
                [historyItem.pages insertObject:webPage atIndex:0];
            }
            else
            {
                historyItem = [[[HistoryItem alloc] init] autorelease];
                
                historyItem.key = [webPage.date getDayMonthName];
                [historyItem.pages addObject:webPage];
                
                self.model.items = [NSMutableArray arrayWithObject:historyItem];
            }
            
            [self.gridView reloadData];
        }
    }
}

- (void)allNotifications:(NSNotification*)note
{
    //NSLog(@"notification %@", note);
}

- (void)progressFinishedChanged:(NSNotification*)theNotification 
{    
//    NSObject *object = [theNotification object];
    
    //NSLog(@"%@", object);
    
    
    
//	[theProgressView setProgress:[ estimatedProgress]];
//	if ((int)[[theNotification object] estimatedProgress] == 1) {
//		theProgressView.hidden = TRUE;
//		// Hide the progress view. This is optional, but depending on where
//		// you put it, this may be a good idea.
//		// If you wanted to do this, you'd
//		// have to set theProgressView to visible in your
//		// webViewDidStartLoad delegate method,
//		// see Apple's UIWebView documentation.
//	}
}

- (void)progressEstimateChanged:(NSNotification*)theNotification
{    
//    MyObject *object = (MyObject*)[theNotification object];
//    
//    double value = (double)[object estimatedProgress];
//    
//    NSLog(@"%f", value);

    
    //NSLog(@"%f", [object performSelector:@selector(estimatedProgress)]);
    
//    double est = [object estimatedProgress];
//    
//    NSLog(@"%f", est);
    
    //	[theProgressView setProgress:[ estimatedProgress]];
//	if ((int)[object estimatedProgress] == 1) {
		//theProgressView.hidden = TRUE;
		// Hide the progress view. This is optional, but depending on where
		// you put it, this may be a good idea.
		// If you wanted to do this, you'd
		// have to set theProgressView to visible in your
		// webViewDidStartLoad delegate method,
		// see Apple's UIWebView documentation.
        
//        NSLog(@"%@", object);
//	}
    
    NSDictionary *userInfo = [theNotification userInfo];
    
    NSNumber *progress = [userInfo objectForKey:@"WebProgressEstimatedProgressKey"];
    
    //NSLog(@"%@", theNotification);
    
    if ([progress intValue] == 1)
    {
        [self.spinner stopAnimating];
        [self.view bringSubviewToFront:self.spinner];
    }
}

//- (void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary *)change context:(void *)context
//{
//    NSLog(@"%@", keyPath);
//    NSLog(@"%@", object);
//     NSLog(@"%@", change);
//}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    [self releaseOutlets];
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
	return UIInterfaceOrientationIsLandscape(interfaceOrientation);
}

- (id)initWithNib
{
    if (IS_IPAD)
    {
        return [self initWithNibName:@"GRInternetViewController_iPad" bundle:nil];
    }
    
    return [self initWithNibName:@"GRInternetViewController" bundle:nil];
}

- (void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
    
    self.mainWebView.delegate = nil;
    [self.mainWebView stopLoading];
    
    [self releaseOutlets];
    
    self.mailController = nil;
    self.model = nil;
    
    self.currentHistoryArray = nil;
    self.readingListArray = nil;
    self.favoriteArray = nil;
    self.historyArray = nil;
    self.externalLink = nil;
    [_newMessageController release];
    
    [self.alertPopoverController dismissPopoverAnimated:NO];
    self.alertPopoverController = nil;
    
    [super dealloc];
}

- (void)setEditMode:(BOOL)bEditMode
{
    _editMode = bEditMode;
    
    if (_browserState == BS_FAVORITE) {
        if (_editMode) {
            [self.clearHistoryButton setTitle:NSLocalizedString(@"Done", @"Done button")  forState:UIControlStateNormal];
            [self.clearHistoryButton sizeToFit];
            CGRect frame = self.clearHistoryButton.frame;
            frame.origin.x = CGRectGetWidth(self.view.bounds) - 20 - CGRectGetWidth(self.clearHistoryButton.bounds);
            self.clearHistoryButton.frame = frame;
        }
        else {
            [self.clearHistoryButton setTitle:NSLocalizedString(@"Edit", @"Edit button") forState:UIControlStateNormal];
            [self.clearHistoryButton sizeToFit];
            CGRect frame = self.clearHistoryButton.frame;
            frame.origin.x = CGRectGetWidth(self.view.bounds) - 20 - CGRectGetWidth(self.clearHistoryButton.bounds);
            self.clearHistoryButton.frame = frame;
        }
    }
    
    if (!bEditMode)
    {
        _indexOfEditedCell = -1;
    }
    
    [self.gridView reloadData];
}

- (void)setBrowserState:(BrowserState)aBrowserState
{
    _browserState = aBrowserState;
    
    [self hideActionPopover];
    [self hideEditPopover];
    
    self.editMode = NO;
    
    if (_browserState == BS_FAVORITE)
    {        
        self.favoriteButton.selected = YES;
        self.historyButton.selected = NO;
        self.actionButton.hidden = YES;
        
        self.clearHistoryButton.hidden = NO;
        [self.clearHistoryButton setTitle:NSLocalizedString(@"Edit", @"Edit")  forState:UIControlStateNormal];
        [self.clearHistoryButton sizeToFit];
        CGRect frame = self.clearHistoryButton.frame;
        frame.origin.x = CGRectGetWidth(self.view.bounds) - 20 - CGRectGetWidth(self.clearHistoryButton.bounds);
        self.clearHistoryButton.frame = frame;
        
//        self.gridView.frame = CGRectMake(20, self.gridView.frame.origin.y, self.gridView.bounds.size.width, self.gridView.bounds.size.height);
        
        CGPoint center = self.view.center;
        
        CGRect rect = CGRectMake(center.x - 2, center.y - 2, 4, 4);
        
        [self hideWebViewToRect:rect animated:YES];

    }
    else if (_browserState == BS_HISTORY)
    {        
        self.historyButton.selected = YES;
        self.favoriteButton.selected = NO;
        self.actionButton.hidden = YES;
        
        self.clearHistoryButton.hidden = NO;
        [self.clearHistoryButton setTitle:NSLocalizedString(@"Clear History", @"Clear History")  forState:UIControlStateNormal];
        [self.clearHistoryButton sizeToFit];
        CGRect frame = self.clearHistoryButton.frame;
        frame.origin.x = CGRectGetWidth(self.view.bounds) - 20 - CGRectGetWidth(self.clearHistoryButton.bounds);
        self.clearHistoryButton.frame = frame;
        
//        self.gridView.frame = CGRectMake(67, self.gridView.frame.origin.y, self.gridView.bounds.size.width, self.gridView.bounds.size.height);
        
        CGPoint center = self.view.center;
        
        CGRect rect = CGRectMake(center.x - 2, center.y - 2, 4, 4);
        
        [self hideWebViewToRect:rect animated:YES];
    }
    else
    {
        self.historyButton.selected = NO;
        self.favoriteButton.selected = NO;
        self.actionButton.hidden = NO;
        
        self.clearHistoryButton.hidden = YES;
        
//        self.gridView.frame = CGRectMake(20, self.gridView.frame.origin.y, self.gridView.bounds.size.width, self.gridView.bounds.size.height);
    }
    
    [self.gridView reloadData];
}

- (void)showEditPopover
{
    self.editPopover.frame = self.view.frame;
    
    if ([self.editPopover superview] == nil)
    {
        [self.view addSubview:self.editPopover];
        self.editPopover.alpha = 0;
        [UIView animateWithDuration:0.3f animations:^{
            self.editPopover.alpha = 1.f;
        }];
    }
    else
    {
        self.editPopover.alpha = 1;
        [UIView animateWithDuration:0.3f animations:^{
            self.editPopover.alpha = 0.f;
        }
                         completion:^(BOOL finished) {
                             [self.editPopover removeFromSuperview];
                         }];
    }
}

- (void)showActionPopover
{
    if (IS_IPAD)
    {
        self.actionPopover.frame = self.view.frame;
    }
    else
    {
        [self.view bringSubviewToFront:self.actionPopover];
    }
    
    if ([self.actionPopover superview] == nil || self.actionPopover.hidden || self.actionPopover.alpha == 0.f)
    {
        if (IS_IPAD)
        {
            [self.view addSubview:self.actionPopover];
        }
        else
        {
            self.actionPopover.hidden = NO;
        }
        
        self.actionPopover.alpha = 0;
        [UIView animateWithDuration:0.3f animations:^{
            self.actionPopover.alpha = 1.f;
        }];
    }
    else
    {
        self.actionPopover.alpha = 1;
        [UIView animateWithDuration:0.3f animations:^{
            self.actionPopover.alpha = 0.f;
        }
         completion:^(BOOL finished) {
             if (IS_IPAD)
             {
                 [self.actionPopover removeFromSuperview];
             }
             else
             {
                 self.actionPopover.hidden = YES;
             }
         }];
    }
}

- (void)hideActionPopover
{
    [UIView animateWithDuration:0.3f animations:^{
        self.actionPopover.alpha = 0.f;
    }
     completion:^(BOOL finished) {
         if (IS_IPAD)
         {
             [self.actionPopover removeFromSuperview];
         }
         else
         {
             self.actionPopover.hidden = YES;
         }
     }];
}

- (void)hideEditPopover
{
    [UIView animateWithDuration:0.3f animations:^{
        self.editPopover.alpha = 0.f;
    }
                     completion:^(BOOL finished) {
                         [self.editPopover removeFromSuperview];
                     }];
}

- (void)hideWebViewToRect:(CGRect)rect animated:(BOOL)animated
{
    [self.spinner stopAnimating];
    
    
    if (animated)
    {
        [UIView animateWithDuration:0.3f
            animations:^{
                self.view.userInteractionEnabled = NO;
                CGRect oldBounds = self.mainWebView.bounds;
                CGFloat scale = rect.size.height / oldBounds.size.height;
                self.mainWebView.transform = CGAffineTransformMakeScale(scale, scale);
            }
            completion:^(BOOL finished) {
                [self.mainWebView removeFromSuperview];
                self.view.userInteractionEnabled = YES;
            }];
    }
    else
    {
        [self.mainWebView removeFromSuperview];
    }
}

- (void)showWebViewFromRect:(CGRect)rect
{
    self.favoriteButton.selected = NO;
    self.historyButton.selected = NO;
    
    [self.view addSubview:self.mainWebView];
    
    if (!CGAffineTransformIsIdentity(self.mainWebView.transform)) {
        [UIView animateWithDuration:0.3f
            animations:^{
                self.view.userInteractionEnabled = NO;
                self.mainWebView.transform = CGAffineTransformIdentity;
            }
            completion:^(BOOL finished) {
                self.view.userInteractionEnabled = YES;
            }];
    }
}

- (void)startSearchForTerm:(NSString*)term
{
    if ([term length])
    {
        term = [term stringByReplacingOccurrencesOfString:@" " withString:@"+"];
        
        NSString *searchTerm = [NSString stringWithFormat:@"http://www.google.com/search?q=%@", term];
        searchTerm = [searchTerm stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
        
        NSURL *url = [NSURL URLWithString:searchTerm];
        NSURLRequest *request = [NSURLRequest requestWithURL:url];
        
        _newPage = YES;
        
        [self.mainWebView loadRequest:request];
        self.addressTextField.text = searchTerm;
    }
}

- (void)startLoadAddress:(NSString*)link
{
    _newPage = YES;
    
    [self.mainWebView loadRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:@"about:blank"]]];
    self.addressTextField.text = @"";
    
    if ([link length])
    {
        link = [link stringByReplacingOccurrencesOfString:@"http://" withString:@""];
        link = [link stringByReplacingOccurrencesOfString:@"https://" withString:@""];
        link = [link stringByReplacingOccurrencesOfString:@"www." withString:@""];
    
//    if ([link rangeOfString:@"www."].location == NSNotFound)
//    {
//        link = [@"www." stringByAppendingString:link];
//    }
    
        if ([link rangeOfString:@"http://"].location == NSNotFound && [link rangeOfString:@"https://"].location == NSNotFound)
        {
            link = [@"http://" stringByAppendingString:link];
        }
        
        link = [link stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
        
        NSURL *url = [NSURL URLWithString:link];
        NSURLRequest *request = [NSURLRequest requestWithURL:url];
        [self.mainWebView loadRequest:request];
    }
}

- (UIImage*)getWebPageContentPreview
{
    // handy UIKit wrappers make this a pretty high level task...
    UIGraphicsBeginImageContext(self.mainWebView.bounds.size);
    [self.mainWebView.layer renderInContext: UIGraphicsGetCurrentContext()];
    UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    return image;
}

- (BOOL)checkCurrentUrl
{
    NSString *currentURL = [self.mainWebView stringByEvaluatingJavaScriptFromString:@"window.location.href"];
    
    if ([currentURL isEqualToString:@"about:blank"])
    {
        return NO;
    }
    
    for (HistoryItem *item in self.model.items)
    {
        for (WebPage *webPage in item.pages)
        {
            if ([webPage.url isEqualToStringURL:currentURL])
            {
                return NO;
            }
            
            break ;
        }
        
        break ;
    }
    
    return YES;
}

- (WebPage*)createWebPage
{
    WebPage *webPage = [[WebPage new] autorelease];
    
    NSString *currentURL = [self.mainWebView stringByEvaluatingJavaScriptFromString:@"window.location.href"];
    
    webPage.url = [NSURL URLWithString:currentURL];//self.mainWebView.request.URL;
    
//    UIImage *preview = [self getWebPageContentPreview];
//    
//    NSData *imgData = UIImagePNGRepresentation(preview);
//    
//    if (imgData)
//    {
//        NSArray *paths = NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES);
//        NSString *dirPath = [paths objectAtIndex:0];
//        
//        NSString *fileName = [[NSString stringWithFormat:@"%f", [[NSDate date] timeIntervalSince1970]] stringByReplacingOccurrencesOfString:@"." withString:@"_"];
//        
//        dirPath = [[dirPath stringByAppendingPathComponent:fileName] stringByAppendingPathExtension:@"png"];
//        
//        [[NSFileManager defaultManager] createFileAtPath:dirPath contents:imgData attributes:nil];
//        
//        webPage.path = dirPath;
//    }
    
    webPage.date = [NSDate date];
    
//    NSLog(@"created page with date %@", webPage.date);
//    NSLog(@"created page with path %@", webPage.path);
    NSLog(@"created page with url %@", webPage.url);
    
    return webPage;
}

- (void)saveBrowserData
{
    InternetPages *internetPages = (InternetPages*)[[DataManager sharedInstance] getEntityWithName:@"InternetPages"];

    internetPages.history = self.historyArray;
    internetPages.readinglist = self.readingListArray;
    internetPages.favorites = self.favoriteArray;
        
    [[DataManager sharedInstance] save];
}

- (void)removeHistory
{
    [self.historyArray removeAllObjects];
    [self.model.items removeAllObjects];
    
    self.model.items = [NSMutableArray array];
    
    [self.gridView reloadData];
    
    [self saveBrowserData];
}

- (void)touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event
{
    [super touchesEnded:touches withEvent:event];
    
    self.editMode = NO;
}


#pragma mark - Actions

- (void)deleteButtonTapped:(NSNotification*)notification
{
    self.editMode = NO;
    
    GRGridCell *cell = [notification object];
    
    NSInteger index = cell.index;
    
    if (_browserState == BS_FAVORITE)
    {
        if (index < [self.favoriteArray count])
        {
            [self.favoriteArray removeObjectAtIndex:index];
        }
    }
    else if (_browserState == BS_HISTORY)
    {
    }
    
    [_gridView reloadData];
}

- (void)backbuttonTapped
{
    if ([self.mainWebView canGoBack])
    {
        [self.mainWebView goBack];
    }
}

- (IBAction)favButtonTapped
{
    [self.addressTextField resignFirstResponder];
    
    if (self.browserState != BS_FAVORITE)
    {
        self.browserState = BS_FAVORITE;
        
        self.mainTitleLabel.text = NSLocalizedString(@"Favorites", @"Favorites");
    }
    else
    {
        [self showWebViewFromRect:CGRectMake(self.view.center.x - 2, self.view.center.y - 2, 4, 4)];
        self.browserState = BS_NORMAL;
    }
}

- (IBAction)historyButtonTapped
{
    [self.addressTextField resignFirstResponder];
    
    if (self.browserState != BS_HISTORY)
    {
        self.browserState = BS_HISTORY;
        
        self.mainTitleLabel.text = NSLocalizedString(@"History", @"History");
    }
    else
    {
        [self showWebViewFromRect:CGRectMake(self.view.center.x - 2, self.view.center.y - 2, 4, 4)];
        self.browserState = BS_NORMAL;
    }
}

- (IBAction)actionButtonTapped
{
    [self.addressTextField resignFirstResponder];
    
    if (_browserState == BS_NORMAL)
    {
        [self showActionPopover];
    }
    else
    {
        [self showEditPopover];
        
        if (_browserState != BS_HISTORY)
        {
            [self.editPopupButton setTitle:NSLocalizedString(@"EDIT", @"EDIT") forState:UIControlStateNormal];
        }
        else
        {
            [self.editPopupButton setTitle:NSLocalizedString(@"CLEAR HISTORY", @"CLEAR HISTORY") forState:UIControlStateNormal];
        }
    }
}

- (void)reloadOrStopButtonTapped
{
//    if (!stopReloadButton.selected)
//    {
//        [self.mainWebView reload];
//        stopReloadButton.selected = YES;
//    }
//    else
//    {
        [self.mainWebView stopLoading];
        
        [self startLoadAddress:@""];
        
        stopReloadButton.selected = NO;
//    }
}

- (IBAction)emailButtonTapped
{
    [self hideActionPopover];
    [self hideEditPopover];
    
    NSArray *accounts = [[GRMailAccountManager manager] getAccounts];
    if (accounts.count > 0)
    {
        NSString *urlName = [self.mainWebView stringByEvaluatingJavaScriptFromString:@"window.location.href"];
        
        TMailAccount *mailAccount = [accounts objectAtIndex:0];
        
        [[GRMailServiceManager shared] setupMailAccount:mailAccount];
        self.newMessageController = [[GRNewMessageController alloc] init];
        _newMessageController.delegate = self;
        [self.view addSubview:_newMessageController.view];
        _newMessageController.tMessage.text = [NSString stringWithFormat:@"%@ \n", urlName];
        _newMessageController.lBodyPlaceHodler.hidden = YES;
        
        [_newMessageController reloadSubviews];
    }
    else
    {
        GRCommonAlertView *alert = [[[GRCommonAlertView alloc] initCommonAlert:NSLocalizedString(@"Email account was not found", @"Email account was not found") inView:self.view] autorelease];
        alert.delegate = nil;
    }
}

- (IBAction)addToFavoriteButtonTapped
{
    [self hideActionPopover];
    [self hideEditPopover];
    
    NSString *currentURL = [self.mainWebView stringByEvaluatingJavaScriptFromString:@"window.location.href"];
    
    if ([currentURL isEqualToString:@"about:blank"])
    {
        return ;
    }
    
    for (WebPage *webPage in self.favoriteArray)
    {
        if ([webPage.url isEqualToStringURL:currentURL])
        {
            return ;
        }
    }
    
    WebPage *webPage = [self createWebPage];
        
    [self.favoriteArray insertObject:webPage atIndex:0];
    
    if ([self.favoriteArray count] > 6)
    {
        [self.favoriteArray removeLastObject];
    }
}

- (IBAction)printButtonTapped
{
    [self hideActionPopover];
    [self hideEditPopover];
    
    Class printInteractionController = NSClassFromString(@"UIPrintInteractionController");
    
    if ((printInteractionController != nil) && [printInteractionController isPrintingAvailable])
    {
        self.printInteraction = [printInteractionController sharedPrintController];
        self.printInteraction.delegate = self;
        
        UIPrintInfo *printInfo = [NSClassFromString(@"UIPrintInfo") printInfo];
        
        printInfo.duplex = UIPrintInfoDuplexLongEdge;
        printInfo.outputType = UIPrintInfoOutputGeneral;
        printInfo.jobName = [self.mainWebView stringByEvaluatingJavaScriptFromString:@"document.title"];
        
        self.printInteraction.printInfo = printInfo;
        self.printInteraction.showsPageRange = YES;
        
        UIViewPrintFormatter *formatter = [self.mainWebView viewPrintFormatter];
        self.printInteraction.printFormatter = formatter;
        
        [self.printInteraction presentAnimated:YES completionHandler:^(UIPrintInteractionController *printInteractionController, BOOL completed, NSError *error) {
            
        }];
        
//        [printInteraction presentFromBarButtonItem:actionButtonItem
//                                          animated:YES
//                                 completionHandler:
//         ^(UIPrintInteractionController *pic, BOOL completed, NSError *error) {
//         }
//         ];
    }
}

- (void) mailButtonTapped
{
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)homeButtonTapped
{
    [self saveBrowserData];
    
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)secretButtonTapped
{
    self.inputView.hidden = !self.inputView.hidden;
}

- (IBAction)setButtonTapped
{
    WebPage *webPage = [self createWebPage];
    webPage.date = self.datePicker.date;
    [self.historyArray addObject:webPage];
}

- (IBAction)cancelButtonTapped
{
    [self hideActionPopover];
    [self hideEditPopover];
}

- (void)cancelDeletionButtonTapped
{
    [self.alertPopoverController dismissPopoverAnimated:NO];
    self.alertPopoverController = nil;
}

- (void)okDeletionButtonTapped
{
    [self removeHistory];
    
    [self.alertPopoverController dismissPopoverAnimated:NO];
    self.alertPopoverController = nil;
}

- (IBAction)clearHistoryButtonTapped
{
    [self hideEditPopover];
    
    if (_browserState == BS_HISTORY) {
        
        [self setupPopoverAppearance];
        
        UIViewController *contentViewController = [[[UIViewController alloc] init] autorelease];
        
        contentViewController.contentSizeForViewInPopover = CGSizeMake(510, 270);
        
        DeleteRecipientView *deleteRecipientView = [[[NSBundle mainBundle] loadNibNamed:@"DeleteRecipientView" owner:nil options:nil] lastObject];
        
        [[contentViewController view] addSubview:deleteRecipientView];
        deleteRecipientView.frame = CGRectMake(0, 0, 510, 270);
        
        [deleteRecipientView.cancelButton addTarget:self action:@selector(cancelDeletionButtonTapped) forControlEvents:UIControlEventTouchUpInside];
        
        deleteRecipientView.titleLabel.attributedText = [[NSAttributedString alloc] initWithString:NSLocalizedString(@"Would you like\nto clear History?", @"Would you like\nto clear History?")];
        
        [deleteRecipientView.okButton addTarget:self action:@selector(okDeletionButtonTapped) forControlEvents:UIControlEventTouchUpInside];
        
        self.alertPopoverController = [[[WYPopoverController alloc] initWithContentViewController:contentViewController] autorelease];
        self.alertPopoverController.delegate = self;
        
        [self.alertPopoverController presentPopoverAsDialogAnimated:YES];
    }
    else if (_browserState == BS_FAVORITE) {
        self.editMode = !_editMode;
    }
}

#pragma mark - UITextFieldDelegate

- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField
{
    [self hideActionPopover];
    [self hideEditPopover];
    
    return YES;
}

- (void)textFieldDidBeginEditing:(UITextField *)textField
{
    [UIView animateWithDuration:0.25f animations:^{
        
        self.actionButton.hidden = YES;
        
        CGRect rect = self.addressTextField.frame;
        rect.size = CGSizeMake(784, 65);
        rect.origin = CGPointMake(CGRectGetWidth(self.toolbar.frame) - 6 - rect.size.width, CGRectGetMidY(self.toolbar.frame) - rect.size.height/2);
        self.addressTextField.frame = rect;
        
        CGRect bounds = self.addressTextField.rightView.bounds;
        bounds.size = CGSizeMake(bounds.size.width*2, _addressTextField.bounds.size.height);
        self.addressTextField.rightView.bounds = bounds;
        
        bounds = self.addressTextField.leftView.bounds;
        bounds.size = CGSizeMake(bounds.size.width*2, _addressTextField.bounds.size.height);
        self.addressTextField.leftView.bounds = bounds;
        
        self.addressTextField.layer.cornerRadius = _addressTextField.bounds.size.height/2;
    }
     completion:^(BOOL finished) {
         self.addressTextField.frame = CGRectIntegral(self.addressTextField.frame);
         self.addressTextField.font = [UIFont fontWithName:@"Helvetica" size:40.0f];
         [self.addressTextField setNeedsLayout];
     }];
}

- (BOOL)textFieldShouldEndEditing:(UITextField *)textField
{
    return YES;
}

- (void)textFieldDidEndEditing:(UITextField *)textField
{
    [UIView animateWithDuration:0.25 animations:^{
        
        if (self.browserState == BS_NORMAL)
        {
            self.actionButton.hidden = NO;
        }
        
        CGRect rect = self.addressTextField.frame;
        rect.size = CGSizeMake(520, 33);
        rect.origin = CGPointMake(CGRectGetMaxX(self.historyButton.frame) + (CGRectGetMinX(self.actionButton.frame) - CGRectGetMaxX(self.historyButton.frame))/2 - rect.size.width/2, self.toolbar.center.y - rect.size.height/2);
        self.addressTextField.frame = rect;
        
        CGRect bounds = self.addressTextField.rightView.bounds;
        bounds.size = CGSizeMake(bounds.size.width/2, _addressTextField.bounds.size.height);
        self.addressTextField.rightView.bounds = bounds;
        
        bounds = self.addressTextField.leftView.bounds;
        bounds.size = CGSizeMake(bounds.size.width/2, _addressTextField.bounds.size.height);
        self.addressTextField.leftView.bounds = bounds;
        
        self.addressTextField.layer.cornerRadius = self.addressTextField.bounds.size.height/2;
    }
    completion:^(BOOL finished) {
        self.addressTextField.frame = CGRectIntegral(self.addressTextField.frame);
        self.addressTextField.font = [UIFont fontWithName:@"Helvetica" size:24.0f];
    }];
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    return YES;
}

- (BOOL)textFieldShouldClear:(UITextField *)textField
{
    return YES;
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    
    NSError *error = NULL;
    NSDataDetector *detector = [NSDataDetector dataDetectorWithTypes:NSTextCheckingTypeLink
                                                               error:&error];
    
    NSUInteger numberOfMatches = [detector numberOfMatchesInString:textField.text
                                                           options:0
                                                             range:NSMakeRange(0, [textField.text length])];
    
    if (numberOfMatches != 1)
    {
        [self startSearchForTerm:textField.text];
        textField.text = @"";
    }
    else
    {
        [self startLoadAddress:textField.text];
    }
    
    if (self.mainWebView.superview == nil)
    {
        self.browserState = BS_NORMAL;
        [self showWebViewFromRect:CGRectMake(self.view.center.x - 2, self.view.center.y - 2, 4, 4)];
    }
    
    [self hideActionPopover];
    [self hideEditPopover];
    
    return YES;
}

#pragma mark - UIWebViewDelegate's methods

- (BOOL)webView:(UIWebView *)webView shouldStartLoadWithRequest:(NSURLRequest *)request navigationType:(UIWebViewNavigationType)navigationType
{
    if (_browserState == 0)
    {
//        NSLog(@"%@", request);
        
        if (![[request.URL absoluteString] isEqualToString:@"about:blank"])
        {
            self.addressTextField.text = [request.URL absoluteString];
        }
    }
    
    //NSLog(@"shouldStartLoadWithRequest %@", request);
    
    return YES;
}

- (void)webViewDidStartLoad:(UIWebView *)webView
{
    [self.spinner startAnimating];
    [self.view bringSubviewToFront:self.spinner];
    stopReloadButton.selected = YES;
    
    //NSLog(@"webViewDidStartLoad %@", webView.request);
}

- (void)webViewDidFinishLoad:(UIWebView *)webView
{
//    [self.spinner stopAnimating];
//    [self.view bringSubviewToFront:self.spinner];
//    
//    NSLog(@"webViewDidFinishLoad");
//    
//    if (!webView.isLoading)
//    {
//        NSLog(@"not loading");
//        
//        if ([self checkCurrentUrl])
//        {
//            WebPage *webPage = [self createWebPage];
//            
//            [self.historyArray addObject:webPage];
//            
//            HistoryItem *historyItem = nil;
//            
//            if ([self.model.items count])
//            {            
//                historyItem = [self.model.items objectAtIndex:0];
//                
//                [historyItem.pages insertObject:webPage atIndex:0];
//            }
//            else
//            {
//                historyItem = [[[HistoryItem alloc] init] autorelease];
//                
//                historyItem.key = [webPage.date getDayMonthName];
//                [historyItem.pages addObject:webPage];
//                
//                self.model.items = [NSMutableArray arrayWithObject:historyItem];
//            }
//            
//            [self.historyTableView reloadData];
//        }
//    }
    
    //NSLog(@"webViewDidFinishLoad %@", webView.request);
    
    stopReloadButton.selected = NO;
}

- (void)webView:(UIWebView *)webView didFailLoadWithError:(NSError *)error
{
    [self.spinner stopAnimating];
    [self.view bringSubviewToFront:self.spinner];
    stopReloadButton.selected = NO;
    
    //NSLog(@"didFailLoadWithError %@", webView.request);
}

#pragma mark - GRGridViewDataSource's methods

- (NSUInteger)gridViewCountOfCells:(GRGridView *)gridView
{
    if (_browserState == BS_FAVORITE)
    {
        return [self.favoriteArray count];
    }
    else if (_browserState == BS_HISTORY) 
    {        
        return [self.historyArray count];
    }
    
    return 0;
}

- (CGSize)gridViewSizeOfCell:(GRGridView *)theGridView
{
    return CGSizeMake(315, 205);
}

- (GRGridCell *)gridView:(GRGridView *)theGridView cellForIndex:(NSUInteger)index
{
    GRGridCell *cell = (GRGridCell*)[theGridView dequeueCell];
    
    if (!cell)
    {
        cell = [[[GRGridCell alloc] initCellWithRect:CGRectMake(0, 0, 315, 205)] autorelease];
    }
    
    cell.parentSize = self.view.bounds.size;
    
    WebPage *webPage = nil;
    
    cell.imageView.image = nil;
    
    if (_browserState == BS_FAVORITE)
    {
        if (cell.index < [self.favoriteArray count])
        {
            webPage = [self.favoriteArray objectAtIndex:index];
        }
        
        [cell.spinner stopAnimating];
    }
    else if (_browserState == BS_HISTORY) 
    {        
        if (cell.index < [self.historyArray count])
        {
            webPage = [self.historyArray objectAtIndex:index];
        }
    }
    
//    if (browserState == BS_HISTORY || browserState == BS_FAVORITE)
//    {
//        [cell.imageView setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"http://immediatenet.com/t/l?Size=1024x768&URL=%@", [webPage.url absoluteString]]] usingActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];

//        [cell.imageView setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"http://free.pagepeeker.com/v2/thumbs.php?size=l&url=%@", [webPage.url absoluteString]]] usingActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];

        [cell.spinner startAnimating];
        
        cell.webView.hidden = NO;
//        
//        [cell.webView stopLoading];
        
//        [cell.imageView setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"http://api.snapito.com/mobile/21cd390325d16517844ed4146d798b63000da1dc/200x150?url=%@", [webPage.url absoluteString]]] success:^(UIImage *image) {
//            [cell.spinner stopAnimating];
//            cell.webView.hidden = YES;
//        } failure:^(NSError *error) {
//            //[cell.spinner stopAnimating];
//            cell.webView.hidden = NO;
//            [cell.webView loadRequest:[NSURLRequest requestWithURL:webPage.url]];
//        }];
    
    UIImage *image = [UIImage imageWithContentsOfFile:webPage.path];
    
    if (image)
    {
        cell.imageView.image = image;
        [cell.spinner stopAnimating];
        cell.webView.hidden = YES;
    }
    else
    {
        //[cell.spinner stopAnimating];
        cell.webView.hidden = NO;
        
        [cell createScreenshotFromUrl:webPage.url
                       competionBlock:^(UIImage *image) {
                           
                            NSData *imgData = UIImagePNGRepresentation(image);

                            if (imgData)
                            {
                                NSArray *paths = NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES);
                                NSString *dirPath = [paths objectAtIndex:0];

                                NSString *fileName = [[NSString stringWithFormat:@"%f", [[NSDate date] timeIntervalSince1970]] stringByReplacingOccurrencesOfString:@"." withString:@"_"];

                                dirPath = [[dirPath stringByAppendingPathComponent:fileName] stringByAppendingPathExtension:@"png"];
                                
                                [[NSFileManager defaultManager] createFileAtPath:dirPath contents:imgData attributes:nil];
                                
                                webPage.path = dirPath;
                            }
                           
                           cell.imageView.image = image;
                           
                           [cell.webView stopLoading];
                           
                           cell.webView.hidden = YES;
                           
                       } failureBlock:^{
                           
                       }];
    };
    
        //NSLog(@"%@", [NSString stringWithFormat:@"http://api.snapito.com/mobile/21cd390325d16517844ed4146d798b63000da1dc/200x150?url=%@", [webPage.url absoluteString]]);
//    }
//    else
//    {
//        UIImage *image = [UIImage imageWithContentsOfFile:webPage.path];
//        
//        cell.imageView.image = image;
//    }
    
    if (_editMode && ((_browserState == BS_FAVORITE) || (cell.index == self.indexOfEditedCell)))
    {
        cell.deleteButton.hidden = NO;
        
        [cell startAnimating];
    }
    else
    {
        [cell stopAnimating];
        cell.deleteButton.hidden = YES;
    }
    
    return cell;
}

#pragma mark - GRGridViewDelegate's methods

- (void)gridView:(GRGridView *)gridView didLongTapCell:(GRGridCell *)cell
{        
    if (!self.editMode && self.browserState != BS_HISTORY)
    {
        self.indexOfEditedCell = cell.index;
        self.editMode = YES;
    }
}

- (void)gridView:(GRGridView *)theGridView didTapCell:(GRGridCell *)cell
{
    if (!theGridView && !cell)
    {
        // just tap somewhere
        self.editMode = NO;
        return ;
    }
    
    CGRect rect = [theGridView convertRect:cell.frame toView:self.view];
    
    [self showWebViewFromRect:rect];
    
    WebPage *webPage = nil;
    
    if (_browserState == BS_FAVORITE)
    {
        if (cell.index < [self.favoriteArray count])
        {
            webPage = [self.favoriteArray objectAtIndex:cell.index];
        }
    }
    else if (_browserState == BS_HISTORY) 
    {        
        if (cell.index < [self.historyArray count])
        {
            webPage = [self.historyArray objectAtIndex:cell.index];
        }
    }
    
    if (!webPage)
    {
        [self.mainWebView loadRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:@"about:blank"]]];
        self.addressTextField.text = @"";
    }
    else
    {
        self.addressTextField.text = [webPage.url absoluteString];
        [self startLoadAddress:[webPage.url absoluteString]];
    }
    
    self.browserState = BS_NORMAL;
    
//    [self createMainToolbarForState:browserState];
}

#pragma mark -
#pragma mark MFMailComposeViewControllerDelegate's methods

- (void)mailComposeController:(MFMailComposeViewController *)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError *)error
{
    [self.mailController dismissViewControllerAnimated:YES completion:^{
        
    }];
}

#pragma mark -
#pragma mark UIPrintInteractionControllerDelegate

- (void)printInteractionControllerDidDismissPrinterOptions:(UIPrintInteractionController *)printInteractionController 
{
    //NSLog(@"printInteractionControllerDidDismissPrinterOptions");
    self.self.printInteraction = nil;
}

#pragma mark -
#pragma mark Table view data source

// Customize the number of sections in the table view.
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView 
{
    return 1;
}

// Customize the number of rows in the table view.
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.model.cellCount;
}


// Customize the appearance of table view cells.
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath 
{    
    TimelineTableViewCell *cell = (TimelineTableViewCell *)[tableView dequeueReusableCellWithIdentifier:@"TimelineTableViewCellIdentifier"];
    
    if (cell == nil) 
    {
        cell = [[[NSBundle mainBundle] loadNibNamed:@"TimelineTableViewCell" owner:nil options:nil] lastObject];
    }
    
    HistoryItem *item = [self.model itemForRowAtIndexPath:indexPath];
    
    cell.titleLabel.text = item.key;
    
    cell.backgroundColor = [UIColor whiteColor];
    
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 30;
}

#pragma mark -
#pragma mark Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    HistoryItem *item = [self.model itemForRowAtIndexPath:indexPath];
    
    self.currentHistoryArray = item.pages;
    
    [self.gridView reloadData];
    
    int item_count = [item.children count];
    
    if (item_count <= 0)
    {
        return;
    }
    
    BOOL newState = NO;
    
    BOOL isOpen = [self.model isCellOpenForRowAtIndexPath:indexPath];
    
    if (NO == isOpen) 
    {
        newState = YES;
    } 
    else 
    {
        newState = NO;
    }
    
    [self.model setOpenClose:newState forRowAtIndexPath:indexPath];
    
    [tableView beginUpdates];
    [tableView reloadSections:[NSIndexSet indexSetWithIndex:0]
             withRowAnimation:UITableViewRowAnimationFade];
    [tableView endUpdates];
}

#pragma mark - WYPopoverControllerDelegate's methods

- (BOOL)popoverControllerShouldDismissPopover:(WYPopoverController *)popoverController
{
    return YES;
}

- (void)popoverControllerDidDismissPopover:(WYPopoverController *)popoverController
{
    self.alertPopoverController = nil;
}

@end

