//
//  GRMainController.m
//  Granny
//
//  Created by Ievgen Pavliuk on 3/27/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "GRMainController.h"
#import "GRRadioListController.h"
#import "GRAlarmViewController.h"
#import "GRCalendarViewController.h"
#import "MagnifyingGlassViewController.h"
#import "GRPhotosController.h"
#import "GRGamesController.h"
#import "GRMailSetupController.h"
#import "GRAllPeopleViewController.h"
#import "GRSettingsViewController.h"
#import "GRInternetViewController.h"
#import "GRRadioViewController.h"
#import "GRMailAccountManager.h"
#import "GRMailInboxListController.h"
#import "GRPeopleManager.h"
#import "GRMailDataManager.h"
#import "SOSViewController.h"
#import "GRPeopleManager.h"
#import "MBProgressHUD.h"
#import "Settings.h"
#import "DataManager.h"
#import "PlaceholderTextField.h"
#import "WYPopoverController.h"
#import "GRAlertViewController.h"
#import "IntroductionViewController.h"
#import "INTULocationManager.h"
#import "WeatherKit.h"
#import "Weather.h"
#import "UIImageView+AFNetworking.h"

#define degreesToRadians(deg) (deg / 180.0 * M_PI)

@interface ALAsset (ALAssetCoding) <NSCoding>

@end

@implementation ALAsset (ALAssetCoding)

-(id)initWithCoder:(NSCoder *)encoder
{
    if ((self=[super init]))
    {
    }
    return self;
}
- (void)encodeWithCoder:(NSCoder *)encoder
{
}
@end


@implementation ApplicationButton

- (void)awakeFromNib
{
    self.imageView.contentMode = UIViewContentModeCenter;
    self.imageView.clipsToBounds = NO;
    self.titleLabel.numberOfLines = 2;
    self.layer.shadowColor = [UIColor blackColor].CGColor;
    self.layer.shadowOpacity = 0.4f;
    self.layer.shadowOffset = CGSizeMake(3, 3);
    self.layer.shadowRadius = 5.f;
    self.titleLabel.textAlignment = NSTextAlignmentCenter;
    self.titleLabel.baselineAdjustment = UIBaselineAdjustmentAlignCenters;
    self.layer.borderWidth = 1.f;
    self.layer.cornerRadius = 35.f;
    self.titleLabel.font = [UIFont fontWithName:@"Helvetica" size:43];
    
    self.layer.shouldRasterize = YES;
}

- (void)layoutSubviews
{
    [super layoutSubviews];
    
    if ([self.titleLabel.text length])
    {
        self.titleLabel.frame = UIEdgeInsetsInsetRect(self.bounds, UIEdgeInsetsMake(120, 5, 5, 5));
        
        self.imageView.frame = UIEdgeInsetsInsetRect(self.bounds, UIEdgeInsetsMake(25, 10, 90, 10));
    }
}

@end

@implementation MoreButton

- (void)awakeFromNib
{
    self.titleLabel.numberOfLines = 2;
    self.titleLabel.layer.shadowColor = [UIColor blackColor].CGColor;
    self.titleLabel.layer.shadowOpacity = 0.4f;
    self.titleLabel.layer.shadowOffset = CGSizeZero;
    self.titleLabel.layer.shadowRadius = 2.f;
    self.titleLabel.textAlignment = NSTextAlignmentCenter;
    self.titleLabel.baselineAdjustment = UIBaselineAdjustmentAlignCenters;
    
    self.layer.shouldRasterize = YES;
}

- (void)layoutSubviews
{
    [super layoutSubviews];
    
    self.titleLabel.frame = CGRectInset(self.bounds, 10, 10);
}

@end


@interface GRMainController () <WYPopoverControllerDelegate>

@property (nonatomic, retain) Settings *settings;

@property (nonatomic, retain) WYPopoverController *alertPopoverController;

@property (assign, nonatomic) NSInteger locationRequestID;
@property (nonatomic, retain) CLLocation *currentLocation;

@end

@implementation GRMainController
{
    SOSViewController *sosViewController;
    NSTimer *updateTimeTimer;
    BOOL isMail;
    Weather *weather;
}

@synthesize internetBtn;
@synthesize eventsBtn;
@synthesize alarmBtn;
@synthesize mailBtn;
@synthesize radioBtn;
@synthesize gamesBtn;
@synthesize photoBtn;
@synthesize peopleBtn;
@synthesize callBtn;
@synthesize mainScrollView;
@synthesize dateLabel;
@synthesize textField;
@synthesize calDayLabel;
@synthesize calMonthLabel;
@synthesize calWeekDayLabel;
@synthesize minuteArrow;
@synthesize hourArrow;
@synthesize mgnBtn;
@synthesize settings;
@synthesize favBtn;
@synthesize tvBtn;
@synthesize youtubeBtn;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    
    if (self)
    {
        self.wantsFullScreenLayout = YES;
        
        self.locationRequestID = NSNotFound;
    }
    
    return self;
}

- (BOOL)prefersStatusBarHidden
{
    return YES;
}

- (void)dealloc
{
    [weather release];
    weather = nil;
    
    self.currentLocation = nil;
    self.favBtn = nil;
    self.youtubeBtn = nil;
    self.tvBtn = nil;
    [sosViewController release];
    [internetBtn release];
    [eventsBtn release];
    [alarmBtn release];
    [mailBtn release];
    [radioBtn release];
    [gamesBtn release];
    [photoBtn release];
    [peopleBtn release];
    [callBtn release];
    [dateLabel release];
    [textField release];
    [mainScrollView release];
    [calDayLabel release];
    [calMonthLabel release];
    [calWeekDayLabel release];
    [minuteArrow release];
    [hourArrow release];
    [mgnBtn release];
    self.settings = nil;
    self.moreBtn = nil;
    self.musicBtn = nil;
    self.temperatureLabel = nil;
    self.temperatureImageView = nil;
    
    [self.alertPopoverController dismissPopoverAnimated:YES];
    self.alertPopoverController = nil;
    
    [super dealloc];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.textField.placeholderColor = rgb(0x808080);
    self.textField.placeholderFont = [UIFont boldSystemFontOfSize:46];
    
    mailBtn.layer.borderColor = rgb(0x338dc8).CGColor;
    [mailBtn setTitleColor:rgb(0x338dc8) forState:UIControlStateNormal];
    peopleBtn.layer.borderColor = rgb(0x666666).CGColor;
    [peopleBtn setTitleColor:rgb(0x666666) forState:UIControlStateNormal];
    internetBtn.layer.borderColor = rgb(0x2e3190).CGColor;
    [internetBtn setTitleColor:rgb(0x2e3190) forState:UIControlStateNormal];
    alarmBtn.layer.borderColor = rgb(0x750046).CGColor;
    [alarmBtn setTitleColor:rgb(0x750046) forState:UIControlStateNormal];
    eventsBtn.layer.borderColor = rgb(0x009999).CGColor;
    [eventsBtn setTitleColor:rgb(0x009999) forState:UIControlStateNormal];
    photoBtn.layer.borderColor = [UIColor blackColor].CGColor;
    [photoBtn setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    radioBtn.layer.borderColor = rgb(0xdc3600).CGColor;
    [radioBtn setTitleColor:rgb(0xdc3600) forState:UIControlStateNormal];
    
    gamesBtn.layer.borderColor = rgb(0xf5911e).CGColor;
    [gamesBtn setTitleColor:rgb(0xf5911e) forState:UIControlStateNormal];
    
    mgnBtn.layer.borderColor = rgb(0x736357).CGColor;
    [mgnBtn setTitleColor:rgb(0x736357) forState:UIControlStateNormal];
    
    self.musicBtn.layer.borderColor = rgb(0xcccccc).CGColor;
    [self.musicBtn setTitleColor:rgb(0xcccccc) forState:UIControlStateNormal];
    
    self.moreBtn.layer.borderColor = rgb(0xcccccc).CGColor;
    [self.moreBtn setTitleColor:rgb(0xcccccc) forState:UIControlStateNormal];
    
    youtubeBtn.layer.borderColor = rgb(0xcccccc).CGColor;
    [youtubeBtn setTitleColor:rgb(0xcccccc) forState:UIControlStateNormal];
    
    self.screenName = @"Home Screen";
    
//    self.textField.font = kARSMaquetteBoldFontWithSize(29);
    
    [self.navigationController setNavigationBarHidden:YES];
    
    NSDictionary *info = [[NSBundle mainBundle] infoDictionary];
    
    self.versionLabel.text = [NSString stringWithFormat:@"Version %@", info[@"CFBundleVersion"]];
    
    mainScrollView.contentSize = CGSizeMake(mainScrollView.contentSize.width, mgnBtn.frame.origin.y + 2.80*mgnBtn.frame.size.height);
    
    if (!self.settings)
        self.settings = [[DataManager sharedInstance] getEntityWithName:kSettingsEntityName];
    
    updateTimeTimer = [NSTimer scheduledTimerWithTimeInterval:10 target:self selector:@selector(updateTime) userInfo:nil repeats:YES];
    
#ifdef REMOVE_ALL_FAKE_CONTACTS
    
    [[GRPeopleManager sharedManager] removeAllFakeContacts];
    
#endif
    
#ifdef CREATE_FAKE_CONTACTS
    
    [[GRPeopleManager sharedManager] populateWithFakeContactsCompletionBlock:^{
        
        [MBProgressHUD hideHUDForView:self.view animated:NO];
    }
     updateBlock:^(float progress) {
         MBProgressHUD *Hud = [MBProgressHUD HUDForView:self.view];
        
         Hud.progress = progress;
     }
      startBlock:^{
          MBProgressHUD *Hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
          
          Hud.mode = MBProgressHUDModeAnnularDeterminate;
          
          Hud.labelText = NSLocalizedString(@"Creating contacts", @"Creating contacts");
      }];
#endif
    
    [[GRPeopleManager sharedManager] syncContactsFromAddressBook:^{
        //[MBProgressHUD hideHUDForView:self.view animated:YES];
        
        [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:NO];
        
    } withProgressBlock:^(float progress) {
//        MBProgressHUD *progressHud = [MBProgressHUD HUDForView:self.view];
//        
//        progressHud.progress = progress;
    } withStartBlock:^{
//        MBProgressHUD *progressHud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
//        
//        progressHud.mode = MBProgressHUDModeAnnularDeterminate;
//        
//        progressHud.labelText = NSLocalizedString(@"Syncing contacts", @"Syncing contacts");
        
        [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:YES];
    }];
    
    if (![[NSUserDefaults standardUserDefaults] boolForKey:@"userSawIntroduction"])
    {
        IntroductionViewController *introductionViewController = [[[IntroductionViewController alloc] init] autorelease];
        
        [self presentViewController:introductionViewController animated:NO completion:NULL];
    }
}

- (void)getWeather
{
    [[WeatherKit sharedInstance] weatherAtLocation:self.currentLocation success:^(NSDictionary *result) {
        
        [weather release];
        weather = nil;
        
        weather = [[Weather alloc] initWithDictionary:result];
        
        self.temperatureLabel.text = [NSString stringWithFormat:@"%@%i", ((weather.temperature <= 0) ? @"": @"+"), weather.temperature];
        
        [self.temperatureImageView setImage:[UIImage imageNamed:weather.iconName]];

    } faliure:^(NSError *error) {
        NSLog(@"Failed to get location!");
    }];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    [self.navigationController setNavigationBarHidden:YES];
    
    [self adjustToInterfaceOrientation:[[UIApplication sharedApplication] statusBarOrientation]];
    
    if (IS_IPAD)
    {
        NSDateFormatter *dateFormater = [[[NSDateFormatter alloc] init] autorelease];
        [dateFormater setDateFormat:@"E dd MMM hh:mm a"];
        dateLabel.text = [dateFormater stringFromDate:[NSDate date]];
        
        [dateFormater setDateFormat:@"MMM"];
        calMonthLabel.text = [dateFormater stringFromDate:[NSDate date]];
        
        [dateFormater setDateFormat:@"EEEE"];
        calWeekDayLabel.text = [dateFormater stringFromDate:[NSDate date]];
        
        [dateFormater setDateFormat:@"dd"];
        calDayLabel.text = [dateFormater stringFromDate:[NSDate date]];
        
        NSDate *dateNow = [NSDate date];
        NSCalendar *calendar = [[[NSCalendar alloc] initWithCalendarIdentifier:NSGregorianCalendar] autorelease];
        
        float degreesPerMinute = 6;
        float min = [[calendar components:NSMinuteCalendarUnit fromDate:dateNow] minute];
        double minutesAngle = degreesToRadians(min * degreesPerMinute);
        minuteArrow.transform = CGAffineTransformRotate(CGAffineTransformIdentity, minutesAngle);
        
        float degreesPerHour = 30;
        float hour = [[calendar components:NSHourCalendarUnit fromDate:dateNow] hour];
        double hourAngle = degreesToRadians((hour + min/100) * degreesPerHour);
        hourArrow.transform = CGAffineTransformRotate(CGAffineTransformIdentity, hourAngle);
        
        if (self.settings && self.settings.personalMessage.length)
            self.textField.text = self.settings.personalMessage;
    }
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
    INTULocationManager *locMgr = [INTULocationManager sharedInstance];
    self.locationRequestID = [locMgr requestLocationWithDesiredAccuracy:INTULocationAccuracyCity
                                                                timeout:10
                                                                  block:^(CLLocation *currentLocation, INTULocationAccuracy achievedAccuracy, INTULocationStatus status) {
                                                                      
                                                                      if (status == INTULocationStatusSuccess) {
                                                                          self.currentLocation = currentLocation;
                                                                          [self getWeather];
                                                                      }
                                                                      else if (status == INTULocationStatusTimedOut) {

                                                                      }
                                                                      else {
                                                                          // An error occurred
                                                                          if (status == INTULocationStatusServicesNotDetermined) {
                                                                              NSLog(@"Error: User has not responded to the permissions alert.");
                                                                          } else if (status == INTULocationStatusServicesDenied) {
                                                                              NSLog(@"Error: User has denied this app permissions to access device location.");
                                                                          } else if (status == INTULocationStatusServicesRestricted) {
                                                                              NSLog(@"Error: User is restricted from using location services by a usage policy.");
                                                                          } else if (status == INTULocationStatusServicesDisabled) {
                                                                              NSLog(@"Error: Location services are turned off for all apps on this device.");
                                                                          } else {
                                                                          NSLog(@"An unknown error occurred.\n(Are you using iOS Simulator with location set to 'None'?)");
                                                                          }
                                                                      }
                                                                      
                                                                      self.locationRequestID = NSNotFound;
                                                                  }];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
}

- (NSUInteger)supportedInterfaceOrientations
{
    return UIInterfaceOrientationMaskLandscape;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    if (IS_IPAD)
        return UIInterfaceOrientationIsLandscape(interfaceOrientation);
    else
        return (interfaceOrientation != UIInterfaceOrientationPortraitUpsideDown);
}

#pragma mark -
#pragma mark Rotation helpers

- (void)willRotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)duration {
    [self adjustToInterfaceOrientation:toInterfaceOrientation];
}

- (void)adjustToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
    if (IS_IPAD)
    {
//        if(interfaceOrientation == UIInterfaceOrientationLandscapeLeft ||interfaceOrientation == UIInterfaceOrientationLandscapeRight) {
//            eventsBtn.frame = CGRectMake(10, 73, eventsBtn.frame.size.width, eventsBtn.frame.size.height);
//            readBtn.frame = CGRectMake(212, 73, readBtn.frame.size.width, readBtn.frame.size.height);
//            internetBtn.frame = CGRectMake(10, 273, internetBtn.frame.size.width, internetBtn.frame.size.height);
//            mailBtn.frame = CGRectMake(412, 73, mailBtn.frame.size.width, mailBtn.frame.size.height);
//            photoBtn.frame = CGRectMake(412, 475, photoBtn.frame.size.width, photoBtn.frame.size.height);
//            radioBtn.frame = CGRectMake(614, 475, radioBtn.frame.size.width, radioBtn.frame.size.height);
//            gamesBtn.frame = CGRectMake(815, 73, gamesBtn.frame.size.width, gamesBtn.frame.size.height);
//            peopleBtn.frame = CGRectMake(815, 475, peopleBtn.frame.size.width, peopleBtn.frame.size.height);
//            callBtn.frame = CGRectMake(815, 275, callBtn.frame.size.width, callBtn.frame.size.height);
//        }
//        else
//        {
//            eventsBtn.frame = CGRectMake(486, 3, eventsBtn.frame.size.width, eventsBtn.frame.size.height);
//            readBtn.frame = CGRectMake(486, 205, readBtn.frame.size.width, readBtn.frame.size.height);
//            internetBtn.frame = CGRectMake(83, 3, internetBtn.frame.size.width, internetBtn.frame.size.height);
//            mailBtn.frame = CGRectMake(284, 404, mailBtn.frame.size.width, mailBtn.frame.size.height);
//            photoBtn.frame = CGRectMake(83, 404, photoBtn.frame.size.width, photoBtn.frame.size.height);
//            radioBtn.frame = CGRectMake(83, 606, radioBtn.frame.size.width, radioBtn.frame.size.height);
//            gamesBtn.frame = CGRectMake(486, 805, gamesBtn.frame.size.width, gamesBtn.frame.size.height);
//            peopleBtn.frame = CGRectMake(83, 805, peopleBtn.frame.size.width, peopleBtn.frame.size.height);
//            callBtn.frame = CGRectMake(285, 805, callBtn.frame.size.width, callBtn.frame.size.height);
//        }
    } else {
        if(interfaceOrientation == UIInterfaceOrientationLandscapeLeft ||interfaceOrientation == UIInterfaceOrientationLandscapeRight) {
            eventsBtn.frame = CGRectMake(15, 15, eventsBtn.frame.size.width, eventsBtn.frame.size.height);
            alarmBtn.frame = CGRectMake(105, 15, alarmBtn.frame.size.width, alarmBtn.frame.size.height);
            internetBtn.frame = CGRectMake(15, 105, internetBtn.frame.size.width, internetBtn.frame.size.height);
            mailBtn.frame = CGRectMake(195, 15, mailBtn.frame.size.width, mailBtn.frame.size.height);
            photoBtn.frame = CGRectMake(195, 195, photoBtn.frame.size.width, photoBtn.frame.size.height);
            radioBtn.frame = CGRectMake(285, 195, radioBtn.frame.size.width, radioBtn.frame.size.height);
            gamesBtn.frame = CGRectMake(375, 15, gamesBtn.frame.size.width, gamesBtn.frame.size.height);
            peopleBtn.frame = CGRectMake(375, 195, peopleBtn.frame.size.width, peopleBtn.frame.size.height);
            callBtn.frame = CGRectMake(375, 105, callBtn.frame.size.width, callBtn.frame.size.height);
        } else {
            eventsBtn.frame = CGRectMake(206, 5, eventsBtn.frame.size.width, eventsBtn.frame.size.height);
            alarmBtn.frame = CGRectMake(206, 95, alarmBtn.frame.size.width, alarmBtn.frame.size.height);
            internetBtn.frame = CGRectMake(25, 5, internetBtn.frame.size.width, internetBtn.frame.size.height);
            mailBtn.frame = CGRectMake(116, 185, mailBtn.frame.size.width, mailBtn.frame.size.height);
            photoBtn.frame = CGRectMake(25, 186, photoBtn.frame.size.width, photoBtn.frame.size.height);
            radioBtn.frame = CGRectMake(25, 275, radioBtn.frame.size.width, radioBtn.frame.size.height);
            gamesBtn.frame = CGRectMake(206, 365, gamesBtn.frame.size.width, gamesBtn.frame.size.height);
            peopleBtn.frame = CGRectMake(25, 365, peopleBtn.frame.size.width, peopleBtn.frame.size.height);
            callBtn.frame = CGRectMake(116, 365, callBtn.frame.size.width, callBtn.frame.size.height);
        }

    }

}

- (void) hideSOSScreen
{
    [sosViewController.view removeFromSuperview];
    [sosViewController release];
}

#pragma mark - Actions

- (void) updateTime
{
    NSDateFormatter *dateFormater = [[[NSDateFormatter alloc] init] autorelease];
    
    NSLog(@"%@", [[NSLocale currentLocale] localeIdentifier]);
    
    [dateFormater setLocale:[NSLocale currentLocale]];
    [dateFormater setDateFormat:@"E dd MMM hh:mm a"];
    dateLabel.text = [dateFormater stringFromDate:[NSDate date]];
    
    [dateFormater setDateFormat:@"MMM"];
    calMonthLabel.text = [dateFormater stringFromDate:[NSDate date]];
    
    [dateFormater setDateFormat:@"EEEE"];
    calWeekDayLabel.text = [dateFormater stringFromDate:[NSDate date]];
    
    [dateFormater setDateFormat:@"dd"];
    calDayLabel.text = [dateFormater stringFromDate:[NSDate date]];
    
    NSDate *dateNow = [NSDate date];
    NSCalendar *calendar = [[[NSCalendar alloc] initWithCalendarIdentifier:NSGregorianCalendar] autorelease];
    
    float degreesPerMinute = 6;
    float min = [[calendar components:NSMinuteCalendarUnit fromDate:dateNow] minute];
    double minutesAngle = degreesToRadians(min * degreesPerMinute);
    minuteArrow.transform = CGAffineTransformRotate(CGAffineTransformIdentity, minutesAngle);
    
    float degreesPerHour = 30;
    float hour = [[calendar components:NSHourCalendarUnit fromDate:dateNow] hour];
    double hourAngle = degreesToRadians((hour + min/100) * degreesPerHour);
    hourArrow.transform = CGAffineTransformRotate(CGAffineTransformIdentity, hourAngle);
}

- (IBAction)onSOSButtonTapped:(id)sender
{
    if (![settings.doctor_phone length] &&
        ![settings.home_phone length] &&
        ![settings.contact_phone length] &&
        ![settings.hospital_phone length])
    {
        WYPopoverBackgroundView *popoverAppearance = [WYPopoverBackgroundView appearance];
        
        [popoverAppearance setOverlayColor:[UIColor colorWithWhite:0.1f alpha:0.6f]];
        [popoverAppearance setOuterCornerRadius:0];
        [popoverAppearance setOuterShadowBlurRadius:0];
        [popoverAppearance setOuterShadowColor:[UIColor clearColor]];
        [popoverAppearance setOuterShadowOffset:CGSizeZero];
        
        [popoverAppearance setGlossShadowColor:[UIColor clearColor]];
        [popoverAppearance setGlossShadowOffset:CGSizeMake(0, 0)];
        
        [popoverAppearance setBorderWidth:1];
        [popoverAppearance setArrowHeight:0];
        [popoverAppearance setArrowBase:0];
        
        [popoverAppearance setInnerCornerRadius:0];
        [popoverAppearance setInnerShadowBlurRadius:0];
        [popoverAppearance setInnerShadowColor:[UIColor clearColor]];
        [popoverAppearance setInnerShadowOffset:CGSizeMake(0, 0)];
        
        [popoverAppearance setFillTopColor:[UIColor whiteColor]];
        [popoverAppearance setFillBottomColor:[UIColor whiteColor]];
        [popoverAppearance setOuterStrokeColor:[UIColor clearColor]];
        [popoverAppearance setInnerStrokeColor:[UIColor clearColor]];
        
        
        GRAlertViewController *contentViewController = [[[GRAlertViewController alloc] init] autorelease];
        
        contentViewController.contentSizeForViewInPopover = CGSizeMake(324, 175);
        
        [contentViewController view];
        
        contentViewController.messageLabel.text = NSLocalizedString(@"Emergency numbers \nneed to be set", @"Emergency numbers \nneed to be set");
        
        contentViewController.messageLabel.font = [UIFont fontWithName:@"Helvetica" size:18];
        
        [contentViewController.okButton setBackgroundImage:nil forState:UIControlStateNormal];
        contentViewController.okButton.frame = CGRectMake(0, contentViewController.okButton.frame.origin.y + 30, 50, 28);
        contentViewController.okButton.center = CGPointMake(contentViewController.view.bounds.size.width / 2, contentViewController.okButton.center.y);
        contentViewController.okButton.layer.borderWidth = 1.f;
        contentViewController.okButton.layer.borderColor = RGBCOLOR(190, 190, 190).CGColor;
        [contentViewController.okButton setTitleColor:RGBCOLOR(96, 96, 96) forState:UIControlStateNormal];
        contentViewController.okButton.titleLabel.font = [UIFont fontWithName:@"Helvetica" size:14];
        
        [contentViewController.okButton addTarget:self action:@selector(popoverButtonTapped:) forControlEvents:UIControlEventTouchUpInside];
        
        self.alertPopoverController = [[[WYPopoverController alloc] initWithContentViewController:contentViewController] autorelease];
        self.alertPopoverController.delegate = self;
        
        [self.alertPopoverController presentPopoverAsDialogAnimated:YES];
    
        return ;
    }
    
    sosViewController = [[SOSViewController alloc] init];
    
    sosViewController.delegate = self;
    
    [self.view addSubview:sosViewController.view];
    sosViewController.view.frame = self.view.bounds;
}

- (void)popoverButtonTapped:(UIButton*)button
{
    [self.alertPopoverController dismissPopoverAnimated:YES];
    self.alertPopoverController = nil;
}

- (IBAction)onRadio:(id)sender
{
    GRRadioViewController *radioPlayerController = [[GRRadioViewController alloc] init];
    [self.navigationController pushViewController:radioPlayerController animated:YES];
    [radioPlayerController release];
}

- (IBAction)onClockButtonTapped:(id)sender {
    GRCalendarViewController *clockViewController = [[GRCalendarViewController alloc] init];
    [self.navigationController pushViewController:clockViewController animated:YES];
    [clockViewController release];
}

- (IBAction)onPhotosButtonTapped:(id)sender {
    GRPhotosController *photosController = [[GRPhotosController alloc] init];
    [self.navigationController pushViewController:photosController animated:YES];
    [photosController release];
}

- (IBAction)onMailButtonTapped:(id)sender
{
    [[GRMailDataManager sharedManager].listReadMessages removeAllObjects];
    [[GRMailDataManager sharedManager].listTrashMessages removeAllObjects];
    [[GRMailDataManager sharedManager].listSentMessages removeAllObjects];
    [[GRMailDataManager sharedManager].listDraftMessages removeAllObjects];
    
    [GRMailDataManager sharedManager].fromContact = NO;
    NSArray *accounts = [[GRMailAccountManager manager] getAccounts];
    if (accounts.count == 0)
    {
        GRCommonAlertView *alert = [[[GRCommonAlertView alloc] initCommonAlert:NSLocalizedString(@"Please add an \nemail account", @"Please add an \nemail account") inView:self.view] autorelease];
        alert.delegate = self;
    }
    else if (accounts.count != 1)
    {
        GRMailSetupController *mailSetupController = [[GRMailSetupController alloc] init];
        [self.navigationController setNavigationBarHidden:YES];
        [self.navigationController pushViewController:mailSetupController animated:YES];
        [mailSetupController release];
    }
    else
    {
        TMailAccount *account = [accounts objectAtIndex:0];

        GRMailInboxListController *inboxController = [[GRMailInboxListController alloc] init];
        inboxController.mailAccount = account;
        [self.navigationController setNavigationBarHidden:YES];
        [self.navigationController pushViewController:inboxController animated:YES];
        [inboxController release];
    }
}

- (IBAction)onSettingsButtonTapped:(id)sender
{
//    if ([GRPeopleManager sharedManager].contacts)
//    {
//        [[GRPeopleManager sharedManager].contacts removeAllObjects];
//        [GRPeopleManager sharedManager].contacts = nil;
//    }
//    
        GRSettingsiPadController *contr = [[[GRSettingsiPadController alloc] initWithNibName:@"GRSettingsiPadController" bundle:nil] autorelease];
        if (isMail)
        {
            isMail = NO;
            contr.selectedIndex = 1;
        }
        [self.navigationController pushViewController:contr animated:YES];
    
//    [[AppDelegate getInstance].window onSettingsSplitController];
}

- (IBAction)onPeopleBtnTapped:(id)sender
{
    GRAllPeopleViewController *peopleViewController = [[GRAllPeopleViewController alloc] init];
    [self.navigationController pushViewController:peopleViewController animated:YES];
    [peopleViewController release];
}

- (IBAction)onAlarmButtonTapped:(id)sender
{
    GRAlarmViewController *clockViewController = [[GRAlarmViewController alloc] init];
    [self.navigationController pushViewController:clockViewController animated:YES];
    [clockViewController release];
}


- (IBAction)onGamesBtnTapped:(id)sender
{
    GRGamesController *gamesController = [[GRGamesController alloc] init];
    [self.navigationController pushViewController:gamesController animated:YES];
    [gamesController release];
}

- (IBAction)onInternetBtnTapped:(id)sender
{
    GRInternetViewController *internetViewController = [[GRInternetViewController alloc] init];
    
    [self.navigationController pushViewController:internetViewController animated:YES];
    [internetViewController release];
}

- (IBAction)onTVBtnTapped:(id)sender
{

}

- (IBAction)onYoutubeBtnTapped:(id)sender
{
    
}

- (IBAction)onCallBtnTapped:(id)sender
{
    
}

- (IBAction)onSearchBtnTapped:(id)sender
{
    
}

- (IBAction)onFavouriteBtnTapped:(id)sender
{
    
}

- (IBAction)onMagnifyingGlassButtonTapped
{
    MagnifyingGlassViewController *foundationCameraController = [[MagnifyingGlassViewController alloc] initWithFrame:self.view.bounds];
    
    [self presentViewController:foundationCameraController animated:YES completion:NULL];
    
    [foundationCameraController release];
}

#pragma mark - GRCommonAlertDelegate

- (void)commonAlert:(id)alertView buttonWithIndex:(NSInteger)index
{
    isMail = YES;
    [self onSettingsButtonTapped:nil];
}

#pragma mark - UITextFieldDelegate
- (BOOL)textFieldShouldReturn:(UITextField *)_textField
{
    [_textField resignFirstResponder];
    return YES;
}
- (BOOL)textFieldShouldEndEditing:(UITextField *)_textField
{
    [_textField resignFirstResponder];
    return YES;
}

#pragma mark - WYPopoverControllerDelegate's methods

- (BOOL)popoverControllerShouldDismissPopover:(WYPopoverController *)popoverController
{
    return YES;
}

- (void)popoverControllerDidDismissPopover:(WYPopoverController *)popoverController
{
    self.alertPopoverController = nil;
}

@end
