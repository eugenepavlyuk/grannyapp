//
//  GRMainController.h
//  Granny
//
//  Created by Ievgen Pavliuk on 3/27/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SOSViewControllerProtocol.h"
#import "GRCommonAlertView.h"

@interface ApplicationButton : UIButton

@end


@interface MoreButton : UIButton

@end

@class PlaceholderTextField;

@interface GRMainController : GAITrackedViewController <SOSViewControllerProtocol, GRCommonAlertDelegate, UITextFieldDelegate, UIScrollViewDelegate>
{    
    UIButton *internetBtn;
    UIButton *eventsBtn;
    UIButton *alarmBtn;
    UIButton *mailBtn;
    UIButton *radioBtn;
    UIButton *gamesBtn;
    UIButton *photoBtn;
    UIButton *peopleBtn;
    UIButton *callBtn;
    UIButton *mgnBtn;
    
    UIScrollView *mainScrollView;
    UILabel *dateLabel;
    PlaceholderTextField *textField;
    
    UILabel *calDayLabel;
    UILabel *calWeekDayLabel;
    UILabel *calMonthLabel;
    
    UIImageView *minuteArrow;
    UIImageView *hourArrow;
}

@property (nonatomic, retain) IBOutlet UIButton *internetBtn;
@property (nonatomic, retain) IBOutlet UIButton *eventsBtn;
@property (nonatomic, retain) IBOutlet UIButton *alarmBtn;
@property (nonatomic, retain) IBOutlet UIButton *mailBtn;
@property (nonatomic, retain) IBOutlet UIButton *radioBtn;
@property (nonatomic, retain) IBOutlet UIButton *gamesBtn;
@property (nonatomic, retain) IBOutlet UIButton *photoBtn;
@property (nonatomic, retain) IBOutlet UIButton *peopleBtn;
@property (nonatomic, retain) IBOutlet UIButton *callBtn;
@property (nonatomic, retain) IBOutlet UIButton *mgnBtn;
@property (nonatomic, retain) IBOutlet UIButton *youtubeBtn;
@property (nonatomic, retain) IBOutlet UIButton *tvBtn;
@property (nonatomic, retain) IBOutlet UIButton *favBtn;
@property (nonatomic, retain) IBOutlet UIButton *musicBtn;
@property (nonatomic, retain) IBOutlet UIButton *moreBtn;
@property (nonatomic, retain) IBOutlet UIScrollView *mainScrollView;
@property (nonatomic, retain) IBOutlet UILabel *dateLabel;
@property (nonatomic, retain) IBOutlet PlaceholderTextField *textField;
@property (nonatomic, retain) IBOutlet UILabel *calDayLabel;
@property (nonatomic, retain) IBOutlet UILabel *calWeekDayLabel;
@property (nonatomic, retain) IBOutlet UILabel *calMonthLabel;
@property (nonatomic, retain) IBOutlet UIImageView *minuteArrow;
@property (nonatomic, retain) IBOutlet UIImageView *hourArrow;
@property (nonatomic, retain) IBOutlet UILabel *temperatureLabel;
@property (nonatomic, retain) IBOutlet UIImageView *temperatureImageView;

@property (nonatomic, retain) IBOutlet UILabel *versionLabel;

- (IBAction)onSOSButtonTapped:(id)sender;
- (IBAction)onRadio:(id)sender;
- (IBAction)onPhotosButtonTapped:(id)sender;
- (IBAction)onMailButtonTapped:(id)sender;
- (IBAction)onSettingsButtonTapped:(id)sender;
- (IBAction)onAlarmButtonTapped:(id)sender;
- (IBAction)onGamesBtnTapped:(id)sender;
- (IBAction)onPeopleBtnTapped:(id)sender;
- (IBAction)onInternetBtnTapped:(id)sender;
- (IBAction)onTVBtnTapped:(id)sender;
- (IBAction)onYoutubeBtnTapped:(id)sender;
- (IBAction)onCallBtnTapped:(id)sender;
- (IBAction)onSearchBtnTapped:(id)sender;
- (IBAction)onFavouriteBtnTapped:(id)sender;
- (IBAction)onMagnifyingGlassButtonTapped;

@end
