//
//  GRMainNavigationController.m
//  GrannyApp
//
//  Created by Eugene Pavluk on 12/4/13.
//
//

#import "GRMainNavigationController.h"

@interface GRMainNavigationController ()

@end

@implementation GRMainNavigationController

- (NSUInteger)supportedInterfaceOrientations
{
    return UIInterfaceOrientationMaskLandscape;
}

@end
