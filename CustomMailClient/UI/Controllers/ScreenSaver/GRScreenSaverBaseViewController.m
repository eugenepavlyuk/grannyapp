//
//  GRScreenSaverBaseViewController.m
//  GrannyApp
//
//  Created by Eugene Pavlyuk on 8/7/12.
//  Copyright (c) 2012 Home. All rights reserved.
//

#import "GRScreenSaverBaseViewController.h"
#import "GRScreenSaverImageViewController.h"
#import "GRScreenSaverSlideshowViewController.h"
#import "GRScreenSaverClockViewController.h"
#import "DataManager.h"
#import "Settings.h"
#import "GRRadioViewController.h"

@interface GRScreenSaverBaseViewController ()

@end

@implementation GRScreenSaverBaseViewController

+ (id)screensaver
{
    Settings *settings = [[DataManager sharedInstance] getEntityWithName:kSettingsEntityName];
    
    if ([settings.radioOn boolValue])
    {
        return [[[GRRadioViewController alloc] init] autorelease];
    }
    else if ([settings.clockOn boolValue])
    {
        return [[[GRScreenSaverClockViewController alloc] init] autorelease];
    }
    else if ([settings.photoSliderOn boolValue])
    {
        return [[[GRScreenSaverSlideshowViewController alloc] init] autorelease];
    }
    else if ([settings.imageOn boolValue])
    {
        return [[[GRScreenSaverImageViewController alloc] init] autorelease];
    }
    
    return nil;
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (BOOL)prefersStatusBarHidden
{
    return YES;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
	return UIInterfaceOrientationIsLandscape(interfaceOrientation);
}

@end
