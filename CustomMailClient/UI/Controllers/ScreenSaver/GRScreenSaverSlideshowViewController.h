//
//  GRScreenSaverSlideshowViewController.h
//  GrannyApp
//
//  Created by Eugene Pavlyuk on 8/7/12.
//  Copyright (c) 2012 Home. All rights reserved.
//

#import "GRScreenSaverBaseViewController.h"

@interface GRScreenSaverSlideshowViewController : GRScreenSaverBaseViewController <UIScrollViewDelegate>
{
    UIScrollView     *scrollView;
    ALAssetsLibrary *library;
    NSMutableArray   *assets;
    NSInteger         selectedPhotoIndex;
    NSInteger         currentPage;
    
    CGFloat           superWidth;
    CGFloat           superHeight;
    BOOL              scrollFromRotation;
    BOOL              isSliderOn;
    
    NSTimer          *timer;
    BOOL              assetsLoaded;
}

@property (nonatomic, retain) IBOutlet UIScrollView *scrollView;
@property (nonatomic, retain) NSMutableArray   *assets;
@property (nonatomic, assign) NSInteger   selectedPhotoIndex;

@end
