//
//  GRScreenSaverImageViewController.h
//  GrannyApp
//
//  Created by Eugene Pavlyuk on 8/7/12.
//  Copyright (c) 2012 Home. All rights reserved.
//

#import "GRScreenSaverBaseViewController.h"

@interface GRScreenSaverImageViewController : GRScreenSaverBaseViewController

@property (nonatomic, retain) IBOutlet UIImageView *imageView;

@end
