//
//  GRScreenSaverImageViewController.m
//  GrannyApp
//
//  Created by Eugene Pavlyuk on 8/7/12.
//  Copyright (c) 2012 Home. All rights reserved.
//

#import "GRScreenSaverImageViewController.h"
#import "DataManager.h"
#import "Settings.h"

@interface GRScreenSaverImageViewController ()

@end

@implementation GRScreenSaverImageViewController

@synthesize imageView;

- (BOOL)prefersStatusBarHidden
{
    return YES;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    
    self.screenName = @"Screen Saver Photo Screen";
    
    Settings *settings = [[DataManager sharedInstance] getEntityWithName:kSettingsEntityName];
    
    if ([settings.linkToImage length])
    {        
        UIImage *image = [UIImage imageWithContentsOfFile:settings.linkToImage];
        
        self.imageView.image = image;
    }
}

- (void)releaseViews
{
    self.imageView = nil;
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    
    [self releaseViews];
}

- (void)dealloc
{
    [self releaseViews];
    [super dealloc];
}

@end
