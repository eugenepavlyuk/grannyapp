//
//  GRScreenSaverClockViewController.h
//  GrannyApp
//
//  Created by Eugene Pavlyuk on 8/7/12.
//  Copyright (c) 2012 Home. All rights reserved.
//

#import "GRScreenSaverBaseViewController.h"
#import "AlarmManagerDelegate.h"

@interface GRScreenSaverClockViewController : GRScreenSaverBaseViewController <AlarmManagerDelegate>

@property (nonatomic, retain) IBOutlet UIView *watchContainer;
@property (nonatomic, retain) IBOutlet UIImageView *hourArrow;
@property (nonatomic, retain) IBOutlet UIImageView *minuteArrow;
@property (nonatomic, retain) IBOutlet UIImageView *secondArrow;

@end
