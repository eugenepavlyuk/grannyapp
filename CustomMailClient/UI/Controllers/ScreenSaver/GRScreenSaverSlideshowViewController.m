//
//  GRScreenSaverSlideshowViewController.m
//  GrannyApp
//
//  Created by Eugene Pavlyuk on 8/7/12.
//  Copyright (c) 2012 Home. All rights reserved.
//

#import "GRScreenSaverSlideshowViewController.h"
#import "GRPhotoView.h"
#import "Settings.h"
#import "DataManager.h"

#define LEFT_PADDING 10.0f

@interface GRScreenSaverSlideshowViewController ()

@end

@implementation GRScreenSaverSlideshowViewController

@synthesize assets;
@synthesize scrollView;
@synthesize selectedPhotoIndex;

- (void)releaseViews
{
    self.scrollView = nil;
}

- (BOOL)prefersStatusBarHidden
{
    return YES;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    
    self.screenName = @"Screen Saver Slideshow Screen";
    
    self.scrollView.backgroundColor = [UIColor colorWithRed:0 green:0 blue:0 alpha:0.6f];
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    
    [self releaseViews];
}

- (void)startLoadingPhotoFromGallery 
{
//#if TARGET_IPHONE_SIMULATOR
//#else
    void (^assetEnumerator)(ALAsset *, NSUInteger, BOOL *) = ^(ALAsset *result, NSUInteger index, BOOL *stop) {
        if(result != NULL) {
            //            NSLog(@"See Asset: %@", result);
            [assets addObject:result];
        }
    };
    
    void (^assetGroupEnumerator)(ALAssetsGroup *, BOOL *) =  ^(ALAssetsGroup *group, BOOL *stop) {
    	if(group != nil) {
            [group enumerateAssetsUsingBlock:assetEnumerator];
        } else {
            [self assetsLoaded];
        }
    };
    
    assets = [[NSMutableArray alloc] init];
    library = [[ALAssetsLibrary alloc] init];
    [library enumerateGroupsWithTypes:ALAssetsGroupSavedPhotos
                           usingBlock:assetGroupEnumerator
                         failureBlock: ^(NSError *error) {
                             NSLog(@"Failure");
                         }];
//#endif
    
}

- (void)assetsLoaded 
{
    assetsLoaded = YES;
    
    currentPage = selectedPhotoIndex;
    [self loadScrollViewWithPage:currentPage];
    [self adjustToInterfaceOrientation:[[UIApplication sharedApplication] statusBarOrientation]];
    
    [self sliderOnBtnPressed:nil];
}

- (void)viewWillAppear:(BOOL)animated 
{
    [super viewWillAppear:animated];
    
    [self startLoadingPhotoFromGallery];
}

- (void)viewWillDisappear:(BOOL)animated 
{    
    [super viewWillDisappear:animated];  
}

- (void)dealloc
{
    [self releaseViews];
    
    [assets release];
    
    [super dealloc];
}

- (void)willRotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)duration {
    [self adjustToInterfaceOrientation:toInterfaceOrientation];
}

- (void)adjustToInterfaceRotation:(UIInterfaceOrientation)orientation {
    
    //    self.photoContentView.frame = CGRectMake(0, 0, superWidth, superHeight);
    if (assetsLoaded) 
    {
        [self assetsLoaded];
    }
    
    self.scrollView.contentSize = CGSizeMake(self.scrollView.frame.size.width*[self.assets count], self.scrollView.frame.size.height);
    [self reloadDisplayedPhotoViews];
    [self.scrollView setContentOffset:CGPointMake(scrollView.frame.size.width*currentPage, 0) animated:NO];
}

- (void)adjustToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation {
    scrollFromRotation = YES;
    
    //    self.scrollView.frame = CGRectMake(0, 0, superWidth, superHeight);
    self.scrollView.contentSize = CGSizeMake(self.scrollView.frame.size.width*[self.assets count], self.scrollView.frame.size.height);
    [self reloadDisplayedPhotoViews];
    [self.scrollView setContentOffset:CGPointMake(scrollView.frame.size.width*currentPage, 0) animated:NO];
}

- (void)reloadDisplayedPhotoViews {
    CGRect frame = self.scrollView.frame;
    for (GRPhotoView * pView in [self.scrollView subviews]) {
        NSInteger page = pView.tag-100;
        if (page>=0) {
            frame.origin.x = frame.size.width * page;
            frame.origin.y = 0;
            pView.frame = frame;
        }
    }
}


#pragma mark - UIScrollViewDelegate

- (void)loadScrollViewWithPage:(int)page {
    
    if (page < 0)
        return;
    if (page >= [self.assets count])
        return;
    
    GRPhotoView *photoView = (GRPhotoView *)[self.scrollView viewWithTag:page+100];
    
    if (photoView == nil) 
    {
        GRPhotoView *photoView = [[GRPhotoView alloc] initWithAsset:[assets objectAtIndex:page] frame:self.scrollView.frame];
        CGRect frame = self.scrollView.frame;
        frame.origin.x = frame.size.width * page;
        frame.origin.y = 0;
        photoView.frame = frame;
        photoView.tag = page+100;
        [self.scrollView addSubview:photoView];
        [photoView release];
    }
}


- (void)scrollViewDidScroll:(UIScrollView *)sender {
    if (!scrollFromRotation) {
        CGFloat pageWidth = scrollView.frame.size.width;
        int page = floor((scrollView.contentOffset.x - pageWidth / 2) / pageWidth) + 1;
        
        NSInteger direction;
        if (page > currentPage) {
            direction = 1;
        }
        if (page < currentPage) {
            direction = 0;
        }
        currentPage = page;
        [self removeNotVisibleViews:page direction:direction];
        
        //NSLog(@"%i", currentPage);
        // load the visible page and the page on either side of it (to avoid flashes when the user starts scrolling)
        [self loadScrollViewWithPage:page - 1];
        [self loadScrollViewWithPage:page];
        [self loadScrollViewWithPage:page + 1];
    }
}

- (void)removeNotVisibleViews:(NSInteger) page direction:(NSInteger)direction {
    GRPhotoView *photoView = nil;
    switch (direction) {
        case 0:
            photoView = (GRPhotoView *)[self.scrollView viewWithTag:page+2+100];
            break;
        case 1:
            photoView = (GRPhotoView *)[self.scrollView viewWithTag:page-2+100];
            break;
            
        default:
            break;
    }
    if (photoView) {
        [photoView removeFromSuperview];
    }
}

- (void)scrollViewWillBeginDragging:(UIScrollView *)scrollView {
    scrollFromRotation = NO;
}

- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView {
}

#pragma mark - Actions

- (void)sliderOnBtnPressed:(id)sender 
{
    if (!isSliderOn) {
        isSliderOn = YES;
        [self runSlider];
    } else {
        isSliderOn = NO;
        [self stopSlider];
    }
}


#pragma mark - Slider 

- (void)runSlider {
    
    Settings *settings = [[DataManager sharedInstance] getEntityWithName:kSettingsEntityName];
    
    NSInteger time = [settings.slidingTimeSeconds intValue] + [settings.slidingTimeMinutes intValue] * 60;
    
    timer = [NSTimer scheduledTimerWithTimeInterval:time
                                             target:self
                                           selector:@selector(moveSlide)
                                           userInfo:nil
                                            repeats:YES];
}

- (void)moveSlide {
    scrollFromRotation = NO;
    currentPage++;
    
    if (currentPage >= [assets count])
    {
        currentPage = 0;
    }
    
    [self.scrollView setContentOffset:CGPointMake(scrollView.frame.size.width*currentPage, 0) animated:YES];
    //[self loadScrollViewWithPage:currentPage];
}

- (void)stopSlider {
    if (timer) {
        [timer invalidate];
        timer = nil;
    }
}

@end
