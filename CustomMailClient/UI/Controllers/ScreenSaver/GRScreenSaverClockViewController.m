//
//  GRScreenSaverClockViewController.m
//  GrannyApp
//
//  Created by Eugene Pavlyuk on 8/7/12.
//  Copyright (c) 2012 Home. All rights reserved.
//

#import "GRScreenSaverClockViewController.h"
#import "AlarmManager.h"

@interface GRScreenSaverClockViewController ()

@end

@implementation GRScreenSaverClockViewController

@synthesize watchContainer;

@synthesize hourArrow;
@synthesize minuteArrow;
@synthesize secondArrow;

- (BOOL)prefersStatusBarHidden
{
    return YES;
}

- (void)releaseViews
{
    self.secondArrow = nil;
    self.hourArrow = nil;
    self.minuteArrow = nil;
    self.watchContainer = nil;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    
    self.screenName = @"Screen Saver Clock Screen";
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    
    [self releaseViews];
}

- (void)dealloc
{
    [self releaseViews];
    [super dealloc];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    [self.navigationController setNavigationBarHidden:YES animated:YES];
    [self adjustToInterfaceOrientation:[[UIApplication sharedApplication] statusBarOrientation]];
    
    AlarmManager *alarmManager = [AlarmManager sharedInstance];
    
    [alarmManager addWatcher:self];
    
    [self timeChangedInAlarmManager:alarmManager];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    
    AlarmManager *alarmManager = [AlarmManager sharedInstance];
    
    [alarmManager removeWatcher:self];
}

- (void)timeChangedInAlarmManager:(AlarmManager*)alarmManager
{			 
    NSDateFormatter *hTimeFormat = [[[NSDateFormatter alloc] init] autorelease];
	[hTimeFormat setDateFormat:@"HH"];
	NSDateFormatter *mTimeFormat = [[[NSDateFormatter alloc] init] autorelease];
	[mTimeFormat setDateFormat:@"mm"];
    NSDateFormatter *sTimeFormat = [[[NSDateFormatter alloc] init] autorelease];
	[sTimeFormat setDateFormat:@"ss"];
    
    NSDate *now = [NSDate date];
   	
	NSString *hTime = [hTimeFormat stringFromDate:now];
	NSString *mTime = [mTimeFormat stringFromDate:now];
    NSString *sTime = [sTimeFormat stringFromDate:now];
    
    int hours = [hTime intValue];
    
    if (hours > 11)
    {
        hours -= 12;
    }
    
    int sec = [sTime intValue];
    int min = [mTime intValue];
    
    float secondAngel = (sec * 6 * M_PI) / 180;
    float minuteAngel = (min * 6 * M_PI) / 180 + (sec * 6 * M_PI) / (180 * 60);
    float hourAngel = (hours * 30 * M_PI) / 180 + (sec + min * 60 * 30 * M_PI) / (180 * 3600);
    
    [UIView beginAnimations:@"rotate" context:nil];    
    self.secondArrow.transform = CGAffineTransformMakeRotation(secondAngel);
    self.minuteArrow.transform = CGAffineTransformMakeRotation(minuteAngel);
    self.hourArrow.transform = CGAffineTransformMakeRotation(hourAngel);
    [UIView commitAnimations];
}

- (void)willRotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)duration
{
    [self adjustToInterfaceOrientation:toInterfaceOrientation];
}

- (void)didRotateFromInterfaceOrientation:(UIInterfaceOrientation)fromInterfaceOrientation
{
    //[self adjustToInterfaceOrientation:[[UIApplication sharedApplication] statusBarOrientation]];
}

- (void)adjustToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation 
{

}

@end
