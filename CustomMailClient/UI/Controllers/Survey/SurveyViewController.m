//
//  SurveyViewController.m
//  GrannyApp
//
//  Created by Eugene Pavluk on 4/23/14.
//
//

#import "SurveyViewController.h"

@interface SurveyViewController ()

@end

static NSString *const surveyUrl = @"https://www.surveymonkey.com/s/YWC98JY";

@implementation SurveyViewController

- (BOOL)prefersStatusBarHidden
{
    return YES;
}

- (void)dealloc
{
    [super dealloc];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.screenName = @"Survey Screen";
    
    UIImage *buttonBackground = [[UIImage imageNamed:@"games_header"] resizableImageWithCapInsets:UIEdgeInsetsMake(0, 20, 0, 20)];
    
    headerImageView.image = buttonBackground;
    
    [webView loadRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:surveyUrl]]];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
}

- (IBAction)homeButtonTapped
{
    [self.navigationController popToRootViewControllerAnimated:YES];
}

- (IBAction)backButtonTapped
{
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)openInSafariButtonTapped
{
    if ([[UIApplication sharedApplication] canOpenURL:[NSURL URLWithString:surveyUrl]])
    {
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:surveyUrl]];
    }
}

@end
