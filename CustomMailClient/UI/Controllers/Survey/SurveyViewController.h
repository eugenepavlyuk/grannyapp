//
//  SurveyViewController.h
//  GrannyApp
//
//  Created by Eugene Pavluk on 4/23/14.
//
//

#import "GAITrackedViewController.h"

@interface SurveyViewController : GAITrackedViewController
{
    IBOutlet UIImageView *headerImageView;
    IBOutlet UIWebView *webView;
}

@end
