//
//  GRAlarmListViewController.h
//  GrannyApp
//
//  Created by Eugene Pavlyuk on 8/8/12.
//  Copyright (c) 2012 Home. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "GRWeekView.h" 
#import "CalendarViewControllerDelegate.h"

@class CalendarView;
@class Settings;

@interface GRAlarmListViewController : GAITrackedViewController <UITableViewDelegate, UITableViewDataSource, MAWeekViewDataSource, MAWeekViewDelegate>
{
    NSMutableArray *alarms;
    
    sortingType typeOfSorting;
    
    Settings *settings;
}

@property (nonatomic, assign) UIViewController *parentController;

@property (nonatomic, retain) IBOutlet UIView *tableContentView;
@property (nonatomic, retain) IBOutlet UITableView *settingsTableView;

@property (nonatomic, retain) IBOutlet UIView *weekContentView;
@property (nonatomic, retain) IBOutlet GRWeekView *weekView;

@property (nonatomic, retain) IBOutlet UIView *monthContentView;
@property (nonatomic, retain) IBOutlet CalendarView *calendarView;

@property (nonatomic, retain) IBOutlet UIButton *addButton;

@property (nonatomic, retain) IBOutlet UIImageView *headerImageView;

@property (nonatomic, retain) IBOutlet UIButton *listButton;
@property (nonatomic, retain) IBOutlet UIButton *monthButton;
@property (nonatomic, retain) IBOutlet UIButton *weekButton;

- (IBAction)sortButtonTapped:(id)sender;

@end
