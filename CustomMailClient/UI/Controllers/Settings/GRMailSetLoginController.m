//
//  GRMailSetLoginController.m
//  GrannyApp
//
//  Created by Ievgen Pavliuk on 5/10/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "GRMailSetLoginController.h"
#import "GRMailAccountManager.h"

@interface GRMailSetLoginController ()


@end

@implementation GRMailSetLoginController
@synthesize lTitle;
@synthesize tUserName;
@synthesize tPassword;
@synthesize tAccountName;
@synthesize bValidate;

#pragma mark - GRMailServiceDelegate

- (void)mailRequestFailed:(NSError *)error {
    bValidate.enabled = YES;
    [bValidate setTitle:NSLocalizedString(@"Validate", @"Validate") forState:UIControlStateNormal];
    [[AppDelegate getInstance].window displayErrorMessageWithString:[error localizedDescription]];
}

- (void)mailConnectionSucceed {
    bValidate.enabled = YES;
    [bValidate setTitle:NSLocalizedString(@"Validate", @"Validate") forState:UIControlStateNormal];
    [[GRMailAccountManager manager] createAccount:mailSettings];
    [[AppDelegate getInstance].window displayInfoMessageWithString:NSLocalizedString(@"Account created successfully", @"Account created successfully")];
//    for (UIViewController *c in [self.navigationController viewControllers]) {
//        if ([c isKindOfClass:GRMailSetupController.class]) {
//            [self.navigationController popToViewController:c animated:YES];
//            break;
//        }
//    }
}

#pragma mark - Initialization

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)dealloc {
    [lTitle release];
    [tUserName release];
    [tPassword release];
    [mailSettings release];
    [tAccountName release];
    [bValidate release];
    [super dealloc];
}

- (BOOL)prefersStatusBarHidden
{
    return YES;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.screenName = @"Mail Set Login Screen";
    
    self.navigationItem.title = NSLocalizedString(@"Create an mail account", @"Create an mail account");
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [GRMailServiceManager shared].delegate = self;
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return UIInterfaceOrientationIsLandscape(interfaceOrientation);
}

#pragma mark - UITextFieldDelegate

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    return YES;
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    [textField resignFirstResponder];
    return YES;
}

- (void)textFieldDidEndEditing:(UITextField *)textField {
}

- (void)textFieldDidBeginEditing:(UITextField *)textField {
    
}


#pragma mark - Actions methods

- (IBAction)validateBtnTapped:(id)sender
{
    bValidate.enabled = NO;
    [bValidate setTitle:NSLocalizedString(@"Connecting...", @"Connecting...") forState:UIControlStateNormal];
    mailSettings = [[[GRMailAccountManager manager] getCurrentMailSettings] retain];
    [mailSettings setObject:tUserName.text forKey:@"userName"];
    [mailSettings setObject:tPassword.text forKey:@"password"];
    [mailSettings setObject:tAccountName.text forKey:@"accountName"];
}

@end
