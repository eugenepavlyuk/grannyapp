//
//  GRSettingsiPadController.h
//  GrannyApp
//
//  Created by Ievgen Pavliuk on 7/27/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "GAITrackedViewController.h"

@interface GRSettingsiPadController : GAITrackedViewController <UITableViewDelegate, UITableViewDataSource>
{
    UITableView      *_contentTable;
    NSMutableArray   *tableData;
    NSMutableArray   *_viewControllers;
    
    NSInteger         selectedIndex;
    
    UIView           *_contentView;
}

@property (nonatomic, retain) IBOutlet UITableView *contentTable;
@property (nonatomic, retain) IBOutlet UIView *contentView;
@property (nonatomic, retain) IBOutlet UIButton *addButton;
@property (nonatomic, retain) IBOutlet UIButton *addRadioStationButton;
@property (nonatomic, retain) IBOutlet UIButton *editRadioStationButton;
@property (nonatomic, retain) IBOutlet UIButton *editOkRadioStationButton;
@property (nonatomic, retain) IBOutlet UIButton *editMailButton;
@property (nonatomic, retain) IBOutlet UIButton *editOkMailButton;
@property (nonatomic, retain) IBOutlet UIButton *addMailButton;
@property (nonatomic, retain) IBOutlet UILabel *navigationLabel;
@property (nonatomic, retain) IBOutlet UIButton *addTVStationButton;
@property (nonatomic, retain) IBOutlet UIButton *editTVStationButton;
@property (nonatomic, retain) IBOutlet UIButton *editOkTVStationButton;
@property NSInteger         selectedIndex;

- (IBAction)homeBtnTapped:(id)sender;
- (IBAction)addBtnTapped:(id)sender;
- (IBAction)addRadioStationBtnTapped:(id)sender;
- (IBAction)editRadioStationBtnTapped:(id)sender;
- (IBAction)editMailBtnTapped:(id)sender;
- (IBAction)okMailBtnTapped:(id)sender;
- (IBAction)addMailBtnTapped:(id)sender;
- (IBAction)editTVStationBtnTapped:(id)sender;
- (IBAction)addTVStationBtnTapped:(id)sender;
- (IBAction)editOkTVStationBtnTapped:(id)sender;

- (IBAction)instructionsButtonTapped;
- (IBAction)surveyButtonTapped;

- (void)displaySelectedViewController;

- (void)restoreHeaderToDefault;
- (void)updateHeaderForRadioStationsList;
- (void)updateHeaderForRadioSearchMethod;
- (void)updateHeaderForMailAccountsList;
- (void)updateHeaderForMailAccountsEdit;
- (void)updateHeaderForTVChannelsList;
- (void)updateHeaderForTVChannelsEdit;
- (void)updateHeaderForTVChannelSearchMethod;

@end
