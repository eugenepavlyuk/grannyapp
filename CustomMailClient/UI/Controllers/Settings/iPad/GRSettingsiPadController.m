//
//  GRSettingsiPadController.m
//  GrannyApp
//
//  Created by Ievgen Pavliuk on 7/27/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "GRSettingsiPadController.h"
#import "GRResource.h"
#import "GRSettingsMenuCell.h"
#import "GRMailSettingsAccountsController.h"
#import "GRContactSettingsViewController.h"
#import "GREventSettingsViewController.h"
#import "GRRadioSettingsStationsController.h"
#import "GRPhotoSettingsViewController.h"
#import "GRScreenSaverSettingsViewController.h"
#import "GREventListSettingsViewController.h"
#import "GRAlarmListViewController.h"
#import "GRSOSSettingsViewController.h"
#import "GRRadioListStationsController.h"
#import "GRMedicineAlarmsViewController.h"
#import "IntroductionViewController.h"
#import "SurveyViewController.h"

@interface GRSettingsiPadController ()
{
    UINavigationController *currentController;
    GREventListSettingsViewController *eventListSettingsViewController;
    GRAlarmListViewController *alarmListViewController;
    GRScreenSaverSettingsViewController *screenSaverSettingsViewController;
    GRContactSettingsViewController *contactSettingsViewController;
    GREventSettingsViewController *eventSettingsViewController;
    GRSOSSettingsViewController *SOSSettingsViewController;
    GRPhotoSettingsViewController *photoSettingsViewController;
    GRMedicineAlarmsViewController *medicineAlarmsViewController;
}

@end

@implementation GRSettingsiPadController

@synthesize selectedIndex;

#pragma mark - Initialization

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    
    if (self) 
    {
        selectedIndex = 0;
    }
    
    return self;
}

- (BOOL)prefersStatusBarHidden
{
    return YES;
}

- (void)dealloc 
{
    [screenSaverSettingsViewController release];
    [contactSettingsViewController release];
    [alarmListViewController release];
    [eventSettingsViewController release];
    [eventListSettingsViewController release];
    [SOSSettingsViewController release];
    [photoSettingsViewController release];
    [medicineAlarmsViewController release];
    [currentController release];
    [tableData release];
    [_viewControllers release];
    self.contentTable = nil;
    self.contentView = nil;
    self.addButton = nil;
    self.navigationLabel = nil;
    self.addRadioStationButton = nil;
    self.editRadioStationButton = nil;
    self.editOkRadioStationButton = nil;
    self.editMailButton = nil;
    self.editOkMailButton = nil;
    self.addMailButton = nil;
    self.addTVStationButton = nil;
    self.editTVStationButton = nil;
    self.editOkTVStationButton = nil;
    [super dealloc];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.screenName = @"Settings Screen";
    
    [self displaySelectedViewController];
    
    tableData = [[NSMutableArray alloc] init];
    [tableData addObject:NSLocalizedString(@"Home", @"Home")];
    [tableData addObject:NSLocalizedString(@"Email", @"Email")];
    [tableData addObject:NSLocalizedString(@"Date&Time", @"Date&Time")];
    [tableData addObject:NSLocalizedString(@"Events", @"Events")];
    [tableData addObject:NSLocalizedString(@"Alarms", @"Alarms")];
    [tableData addObject:NSLocalizedString(@"Medicine Alerts", @"Medicine Alerts")];
    [tableData addObject:NSLocalizedString(@"Radio", @"Radio")];
    [tableData addObject:NSLocalizedString(@"Contacts", @"Contacts")];
    [tableData addObject:NSLocalizedString(@"Photo", @"Photo")];
    //[tableData addObject:NSLocalizedString(@"TV", @"TV")];
    [tableData addObject:NSLocalizedString(@"Emergency", @"Emergency")];
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];    
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    self.addButton = nil;
    self.navigationLabel = nil;
    self.addRadioStationButton = nil;
    self.editRadioStationButton = nil;
    self.editOkRadioStationButton = nil;
    currentController = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return UIInterfaceOrientationIsLandscape(interfaceOrientation);
}

#pragma mark - Actions methods

- (IBAction)homeBtnTapped:(id)sender 
{
    [self.navigationController popViewControllerAnimated:YES];
//    [self.navigationController popToRootViewControllerAnimated:YES];
//    [[AppDelegate getInstance].window onMainController];
}

- (IBAction)addBtnTapped:(id)sender
{
    [[NSNotificationCenter defaultCenter] postNotificationName:@"AddButtonTappedNotification" object:nil];
}

- (IBAction)addRadioStationBtnTapped:(id)sender 
{
    [self updateHeaderForRadioEdit];
    UINavigationController *navController = currentController;
    UIViewController *vc = [navController topViewController];
    
    if ([vc isKindOfClass:GRRadioListStationsController.class]) 
    {
        [(GRRadioListStationsController *)vc onSelectAddSearchMethodController];
    }
}


- (IBAction)editRadioStationBtnTapped:(id)sender
{
    [self updateHeaderForRadioEdit];
    UINavigationController *navController = currentController;
    UIViewController *vc = [navController topViewController];
    if ([vc isKindOfClass:GRRadioListStationsController.class]) {
        [(GRRadioListStationsController *)vc editBtnPressed];
    }
}


- (IBAction)editOkRadioStationBtnTapped:(id)sender {
    [self restoreHeaderToDefault];
    [self updateHeaderForRadioStationsList];
    UINavigationController *navController = currentController;
    UIViewController *vc = [navController topViewController];
    if ([vc isKindOfClass:GRRadioListStationsController.class])
    {
        [(GRRadioListStationsController *)vc okBtnPressed];
    }
    else
    {
        self.addRadioStationButton.hidden = YES;
        self.editRadioStationButton.hidden = YES;
        [vc.navigationController popToRootViewControllerAnimated:YES];
    }
}


- (IBAction)editMailBtnTapped:(id)sender {
    [self updateHeaderForMailAccountsEdit];
    if (selectedIndex == 1) {
        UINavigationController *mailNavController = currentController;
        UIViewController *topViewController = [mailNavController topViewController];
        if ([topViewController isKindOfClass:GRMailSettingsAccountsController.class]) {
            [(GRMailSettingsAccountsController *)topViewController editOn];
        }
    }
}

- (IBAction)okMailBtnTapped:(id)sender {
    [self updateHeaderForMailAccountsList];
    if (selectedIndex == 1) {
        UINavigationController *mailNavController = currentController;
        UIViewController *topViewController = [mailNavController topViewController];
        if ([topViewController isKindOfClass:GRMailSettingsAccountsController.class]) {
            [(GRMailSettingsAccountsController *)topViewController editOff];
        }
    }
}

- (IBAction)addMailBtnTapped:(id)sender {
    [self restoreHeaderToDefault];
    self.navigationLabel.text = NSLocalizedString(@"Settings: Email", nil);
    if (selectedIndex == 1) {
        UINavigationController *mailNavController = currentController;
        UIViewController *topViewController = [mailNavController topViewController];
        if ([topViewController isKindOfClass:GRMailSettingsAccountsController.class]) {
            [(GRMailSettingsAccountsController *)topViewController onAccountTypesController];
        }
    }
}

- (IBAction)editTVStationBtnTapped:(id)sender
{

}

- (IBAction)addTVStationBtnTapped:(id)sender
{

}

- (IBAction)editOkTVStationBtnTapped:(id)sender {

}

- (IBAction)instructionsButtonTapped
{
    IntroductionViewController *introductionViewController = [[[IntroductionViewController alloc] init] autorelease];
    
    [self presentViewController:introductionViewController animated:YES completion:NULL];
}

- (IBAction)surveyButtonTapped
{
    SurveyViewController *surveyViewController = [[[SurveyViewController alloc] init] autorelease];
    
    [self.navigationController pushViewController:surveyViewController animated:NULL];
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [tableData count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *nibName = @"GRSettingsMenuCell_iPad";
    static NSString *CellIdentifier = @"GRSettingsMenuCell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:nil];
    
    if (cell == nil)
    {
        cell = [GRResource tableCellFromNib:nibName owner:self.contentTable cellId:CellIdentifier];
    }
    
    [(GRSettingsMenuCell *)cell updateForIndex:[indexPath row]];
    
    if (selectedIndex == [indexPath row])
    {
        [(GRSettingsMenuCell *)cell markAsSelected];
    }
    else
    {
        [(GRSettingsMenuCell *)cell markAsUnSelected];
    }
    
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (IS_IPAD)
    {
        return 100.0f;
    }
    else
    {
        return 49.0f;
    }
}

#pragma mark - Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath 
{
    [self restoreHeaderToDefault];
    selectedIndex = [indexPath row];
    [self.contentTable reloadData];
    [self displaySelectedViewController];
    
    if (indexPath.row == 5 || indexPath.row == 4 || indexPath.row == 3 || indexPath.row == 7)
    {
        self.addButton.hidden = NO;
    }
    else
    {
        self.addButton.hidden = YES;
    }
    
    switch (indexPath.row) 
    {
        case 0:
            self.navigationLabel.text = NSLocalizedString(@"Settings: Home", @"Settings: Home");
            break;
        case 1:
            self.navigationLabel.text = NSLocalizedString(@"Settings: Email", @"Settings: Email");
            break;
        case 2:
            self.navigationLabel.text = NSLocalizedString(@"Settings: Date&Time", @"Settings: Date&Time");
            break;
        case 3:
            self.navigationLabel.text = NSLocalizedString(@"Settings: Event", @"Settings: Event");
            break;
        case 4:
            self.navigationLabel.text = NSLocalizedString(@"Settings: Protected Alarms", @"Settings: Protected Alarms");
            break;
        
        case 5:
            self.navigationLabel.text = NSLocalizedString(@"Settings: Medicine Alarms", @"Settings: Medicine Alarms");
            break;
            
        case 6:
            self.navigationLabel.text = NSLocalizedString(@"Settings: Radio", @"Settings: Radio");
            break;
        case 7:
            self.navigationLabel.text = NSLocalizedString(@"Settings: Contacts", @"Settings: Contacts");
            break;
            
        case 8:
            self.navigationLabel.text = NSLocalizedString(@"Settings: Photo", @"Settings: Photo");
            break;
            
//        case 9:
//            self.navigationLabel.text = NSLocalizedString(@"Settings: TV", @"Settings: TV");
//            break;
            
//        case 10:
        case 9:
            self.navigationLabel.text = NSLocalizedString(@"Settings: Emergency", @"Settings: Emergency");
            break;

        default:
            self.navigationLabel.text = NSLocalizedString(@"Settings", @"Settings");
            break;
    }
}


#pragma mark - Controller helper

- (void)displaySelectedViewController
{
    [self restoreHeaderToDefault];
    
    for (UIView *v in [self.contentView subviews])
    {
        if (v.tag == 111)
        {
            [v removeFromSuperview];
        }
    }

    if (currentController)
    {
        [currentController popToRootViewControllerAnimated:NO];
        
        [currentController release];
        currentController = nil;
    }
    
    
    switch (selectedIndex)
    {
        case 0:
            self.navigationLabel.text = NSLocalizedString(@"Settings: Home", @"Settings: Home");
            break;
        case 1:
            self.navigationLabel.text = NSLocalizedString(@"Settings: Email", @"Settings: Email");
            break;
        case 2:
            self.navigationLabel.text = NSLocalizedString(@"Settings: Date&Time", @"Settings: Date&Time");
            break;
        case 3:
            self.navigationLabel.text = NSLocalizedString(@"Settings: Event", @"Settings: Event");
            break;
        case 4:
            self.navigationLabel.text = NSLocalizedString(@"Settings: Protected Alarms", @"Settings: Protected Alarms");
            break;
            
        case 5:
            self.navigationLabel.text = NSLocalizedString(@"Settings: Medicine Alarms", @"Settings: Medicine Alarms");
            break;
            
        case 6:
            self.navigationLabel.text = NSLocalizedString(@"Settings: Radio", @"Settings: Radio");
            break;
        case 7:
            self.navigationLabel.text = NSLocalizedString(@"Settings: Contacts", @"Settings: Contacts");
            break;
            
        case 8:
            self.navigationLabel.text = NSLocalizedString(@"Settings: Photo", @"Settings: Photo");
            break;
            
            //        case 9:
            //            self.navigationLabel.text = NSLocalizedString(@"Settings: TV", @"Settings: TV");
            //            break;
            
            //        case 10:
        case 9:
            self.navigationLabel.text = NSLocalizedString(@"Settings: Emergency", @"Settings: Emergency");
            break;
            
        default:
            self.navigationLabel.text = NSLocalizedString(@"Settings", @"Settings");
            break;
    }
    
    switch (selectedIndex)
    {
        case 0:
        {
            if (screenSaverSettingsViewController)
            {
                [screenSaverSettingsViewController release];
                screenSaverSettingsViewController = nil;
            }
            screenSaverSettingsViewController = [[GRScreenSaverSettingsViewController alloc] init];
            screenSaverSettingsViewController.view.tag = 111;
            screenSaverSettingsViewController.view.frame = CGRectMake(0, 0, self.contentView.frame.size.width, self.contentView.frame.size.height);
            screenSaverSettingsViewController.parentController = self;
            [self.contentView addSubview:screenSaverSettingsViewController.view];
            break;
        }
        case 1:
        {
            GRMailSettingsAccountsController *mailSettingsController = [[[GRMailSettingsAccountsController alloc] init] autorelease];
            mailSettingsController.view.frame = CGRectMake(0, 0, self.contentView.frame.size.width, self.contentView.frame.size.height);
            mailSettingsController.view.tag = 111;
            mailSettingsController.parentController = self;
            
            currentController = [[UINavigationController alloc] initWithRootViewController:mailSettingsController];
            currentController.view.tag = 111;
            [currentController setNavigationBarHidden:YES];
            currentController.view.frame = CGRectMake(0, 0, self.contentView.frame.size.width, self.contentView.frame.size.height);
            
            [self.contentView addSubview:currentController.view];
            break;
        }
        case 2:
        {
            if (eventSettingsViewController)
            {
                [eventSettingsViewController release];
                eventSettingsViewController = nil;
            }
            
            eventSettingsViewController = [[GREventSettingsViewController alloc] init];
            eventSettingsViewController.view.tag = 111;
            eventSettingsViewController.view.frame = CGRectMake(0, 0, self.contentView.frame.size.width, self.contentView.frame.size.height);
            
            [self.contentView addSubview:eventSettingsViewController.view];
            break;
        }
        case 3:
        {
            if (eventListSettingsViewController)
            {
                [eventListSettingsViewController release];
                eventListSettingsViewController = nil;
            }
            eventListSettingsViewController = [[GREventListSettingsViewController alloc] init];
            eventListSettingsViewController.view.tag = 111;
            eventListSettingsViewController.parentController = self;
            eventListSettingsViewController.view.frame = CGRectMake(0, 0, self.contentView.frame.size.width, self.contentView.frame.size.height);
            [self.contentView addSubview:eventListSettingsViewController.view];
            break;
        }
        case 4:
        {
            if (alarmListViewController)
            {
                [alarmListViewController release];
                alarmListViewController = nil;
            }
            alarmListViewController = [[GRAlarmListViewController alloc] init];
            alarmListViewController.view.tag = 111;
            alarmListViewController.parentController = self;
            alarmListViewController.view.frame = CGRectMake(0, 0, self.contentView.frame.size.width, self.contentView.frame.size.height);
            [self.contentView addSubview:alarmListViewController.view];
            break;
        }
        
        case 5:
        {
            if (medicineAlarmsViewController)
            {
                [medicineAlarmsViewController release];
                medicineAlarmsViewController = nil;
            }
            medicineAlarmsViewController = [[GRMedicineAlarmsViewController alloc] init];
            medicineAlarmsViewController.view.tag = 111;
            medicineAlarmsViewController.parentController = self;
            medicineAlarmsViewController.view.frame = CGRectMake(0, 0, self.contentView.frame.size.width, self.contentView.frame.size.height);
            [self.contentView addSubview:medicineAlarmsViewController.view];
            break;
        }
            
        case 6:
        {
            GRRadioSettingsStationsController *radioStationsController = [[[GRRadioSettingsStationsController alloc] init] autorelease];
            radioStationsController.parentController = self;
            radioStationsController.view.frame = CGRectMake(0, 0, self.contentView.frame.size.width, self.contentView.frame.size.height);
            
            currentController = [[UINavigationController alloc] initWithRootViewController:radioStationsController];
            currentController.view.tag = 111;
            [currentController setNavigationBarHidden:YES];
            currentController.view.frame = CGRectMake(0, 0, self.contentView.frame.size.width, self.contentView.frame.size.height);
            
            [self.contentView addSubview:currentController.view];
            break;
        }
        case 7:
        {
            if (contactSettingsViewController)
            {
                [contactSettingsViewController release];
                contactSettingsViewController = nil;
            }
            
            contactSettingsViewController = [[GRContactSettingsViewController alloc] init];
            contactSettingsViewController.view.tag = 111;
            contactSettingsViewController.view.frame = CGRectMake(0, 0, self.contentView.frame.size.width, self.contentView.frame.size.height);
            [self.contentView addSubview:contactSettingsViewController.view];
            break;
        }
        case 8:
        {
            if (photoSettingsViewController)
            {
                [photoSettingsViewController release];
                photoSettingsViewController = nil;
            }
            
            photoSettingsViewController = [[GRPhotoSettingsViewController alloc] init];
            photoSettingsViewController.view.tag = 111;
            photoSettingsViewController.view.frame = CGRectMake(0, 0, self.contentView.frame.size.width, self.contentView.frame.size.height);
            [self.contentView addSubview:photoSettingsViewController.view];
            break;
        }
//        case 9:
//        {
//            GRTVChannelSettingsController *tvSettingsChannelController = [[[GRTVChannelSettingsController alloc] initWithNibName:@"GRTVChannelSettingsController_iPad" bundle:nil] autorelease];
//            tvSettingsChannelController.view.frame = CGRectMake(0, 0, self.contentView.frame.size.width, self.contentView.frame.size.height);
//            tvSettingsChannelController.parentController = self;
//            
//            currentController = [[UINavigationController alloc] initWithRootViewController:tvSettingsChannelController];
//            currentController.view.tag = 111;
//            [currentController setNavigationBarHidden:YES];
//            currentController.view.frame = CGRectMake(0, 0, self.contentView.frame.size.width, self.contentView.frame.size.height);
//            
//            [self.contentView addSubview:currentController.view];
//            break;
//        }
            
//        case 10:
        case 9:
        {
            GRSOSSettingsViewController *sosSettingsViewController = [[[GRSOSSettingsViewController alloc] init] autorelease];
            sosSettingsViewController.view.frame = CGRectMake(0, 0, self.contentView.frame.size.width, self.contentView.frame.size.height);
//            sosSettingsViewController.parentController = self;
            
            currentController = [[UINavigationController alloc] initWithRootViewController:sosSettingsViewController];
            currentController.view.tag = 111;
            [currentController setNavigationBarHidden:YES];
            currentController.view.frame = CGRectMake(0, 0, self.contentView.frame.size.width, self.contentView.frame.size.height);
            
            [self.contentView addSubview:currentController.view];
            break;
        }
    }
}

#pragma mark - Update Header for selected sections

- (void)restoreHeaderToDefault {
//    self.navigationLabel.text = NSLocalizedString(@"Settings", @"Settings");
    self.addRadioStationButton.hidden = YES;
    self.editRadioStationButton.hidden = YES;
    self.editOkRadioStationButton.hidden = YES;
    self.editMailButton.hidden = YES;
    self.editOkMailButton.hidden = YES;
    self.addMailButton.hidden = YES;
    self.editOkTVStationButton.hidden = YES;
    self.addTVStationButton.hidden = YES;
    self.editTVStationButton.hidden = YES;

}


- (void)updateHeaderForRadioStationsList
{
    [self restoreHeaderToDefault];
    self.addRadioStationButton.hidden = NO;
    self.editRadioStationButton.hidden = NO;
//    self.navigationLabel.text = NSLocalizedString(@"Settings: Radio", @"Settings: Radio");
}

- (void)updateHeaderForRadioSearchMethod
{
    [self restoreHeaderToDefault];
//    self.navigationLabel.text = NSLocalizedString(@"Settings: Radio", @"Settings: Radio");
}

- (void)updateHeaderForRadioEdit
{
    [self restoreHeaderToDefault];
//    self.navigationLabel.text = NSLocalizedString(@"Settings: Radio", @"Settings: Radio");
    self.editOkRadioStationButton.hidden = NO;
}

- (void)updateHeaderForMailAccountsList {
    [self restoreHeaderToDefault];
  //  self.navigationLabel.text = NSLocalizedString(@"Settings: Email", nil);
    self.editMailButton.hidden = NO;
    self.addMailButton.hidden = NO;
}

- (void)updateHeaderForMailAccountsEdit {
    [self restoreHeaderToDefault];
//    self.navigationLabel.text = NSLocalizedString(@"Settings: Email", nil);
    self.editMailButton.hidden = YES;
    self.addMailButton.hidden = YES;
    self.editOkMailButton.hidden = NO;
}

- (void)updateHeaderForTVChannelsList
{
    [self restoreHeaderToDefault];
//    self.navigationLabel.text = NSLocalizedString(@"Settings: TV", @"Settings: TV");
    self.addTVStationButton.hidden = NO;
    self.editTVStationButton.hidden = NO;
    self.editOkTVStationButton.hidden = YES;
    
}
- (void)updateHeaderForTVChannelSearchMethod
{
    [self restoreHeaderToDefault];
//    self.navigationLabel.text = NSLocalizedString(@"Settings: TV", @"Settings: TV");
}

- (void)updateHeaderForTVChannelsEdit
{
    [self restoreHeaderToDefault];
//    self.navigationLabel.text = NSLocalizedString(@"Settings: TV", @"Settings: TV");
    self.editOkTVStationButton.hidden = NO;
}

@end
