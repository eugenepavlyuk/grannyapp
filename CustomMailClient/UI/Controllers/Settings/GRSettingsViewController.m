//
//  GRSettingsViewController.m
//  GrannyApp
//
//  Created by Eugene Pavlyuk on 6/8/12.
//  Copyright (c) 2012 Home. All rights reserved.
//

#import "GRSettingsViewController.h"
#import "GRContactSettingsViewController.h"

@interface GRSettingsViewController ()

@end

@implementation GRSettingsViewController

@synthesize settingsTableView;
@synthesize containerView;

- (id)initWithNib
{
    return [self initWithNibName:@"GRSettingsViewController" bundle:nil];
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    [self.navigationController setNavigationBarHidden:YES];
}

- (BOOL)prefersStatusBarHidden
{
    return YES;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    
    self.screenName = @"Settings Screen";
    
    self.settingsTableView.layer.borderWidth = 1.f;
    self.settingsTableView.layer.borderColor = [UIColor lightGrayColor].CGColor;
    self.settingsTableView.layer.cornerRadius = 10.f;
    
    self.containerView.layer.shadowColor = [UIColor blackColor].CGColor;
    self.containerView.layer.shadowRadius = 3.f;
    self.containerView.layer.shadowOpacity = 0.6f;
    self.containerView.layer.shadowOffset = CGSizeMake(0, 0);
    self.containerView.layer.borderWidth = 1.f;
    self.containerView.layer.borderColor = [UIColor lightGrayColor].CGColor;
    self.containerView.layer.cornerRadius = 10.f;
    
    self.settingsTableView.clipsToBounds = YES;
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    self.settingsTableView = nil;
    self.containerView = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return UIInterfaceOrientationIsLandscape(interfaceOrientation);
}

- (void)dealloc
{
    self.settingsTableView = nil;
    self.containerView = nil;

    [super dealloc];
}

- (IBAction)backButtonTapped
{
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark - UITableViewDelegate's methods

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 50;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    if (indexPath.row == 3)
    {
        GRContactSettingsViewController *settingsController = [[GRContactSettingsViewController alloc] initWithNibName:@"GRContactSettingsViewController" bundle:nil];
        [self.navigationController pushViewController:settingsController animated:YES];
        [settingsController release];
    }
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

#pragma mark - UITableViewDataSource's methods

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
//    if (section == 0)
//    {
//        return 2;
//    }
    
    return 4;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"cellIdentifier"];
    
    if (!cell)
    {
        cell = [[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"cellIdentifier"] autorelease];
    }
    
    switch (indexPath.row) 
    {
        case 0:
            cell.textLabel.text = NSLocalizedString(@"Email", @"Email");
            break;

        case 1:
            cell.textLabel.text = NSLocalizedString(@"Events", @"Events");
            break;

        case 2:
            cell.textLabel.text = NSLocalizedString(@"Radio", @"Radio");
            break;

        case 3:
            cell.textLabel.text = NSLocalizedString(@"Contacts", @"Contacts");
            break;
            
        default:
            break;
    }
    
    return cell;
}

@end
