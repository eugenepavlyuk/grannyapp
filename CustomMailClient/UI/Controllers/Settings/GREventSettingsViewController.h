//
//  GREventSettingsViewController.h
//  GrannyApp
//
//  Created by Eugene Pavlyuk on 6/16/12.
//  Copyright (c) 2012 Home. All rights reserved.
//

#import <UIKit/UIKit.h>

@class STSegmentedControl;
@class Settings;

@interface GREventSettingsViewController : GAITrackedViewController <UITableViewDelegate, UITableViewDataSource>
{
    Settings *settings;
}

@property (nonatomic, retain) IBOutlet UITableView *settingsTableView;

@property (nonatomic, retain) IBOutlet UITableViewCell *weekTableViewCell;
@property (nonatomic, retain) IBOutlet UITableViewCell *timeTableViewCell;
@property (nonatomic, retain) IBOutlet UITableViewCell *mailTableViewCell;

@property (nonatomic, retain) IBOutlet UITableViewCell *snoozeTableViewCell;

@property (nonatomic, retain) IBOutlet UISegmentedControl *timeSegment;

@property (nonatomic, retain) STSegmentedControl *timeCustomSegment;

@property (nonatomic, retain) IBOutlet UISlider *snoozeSlider;
@property (nonatomic, retain) IBOutlet UILabel *snoozeTime;

@property (nonatomic, retain) IBOutlet UIButton *mondayButton;
@property (nonatomic, retain) IBOutlet UIButton *sundayButton;

- (IBAction)segmentValueChanged:(id)sender;
- (IBAction)backButtonTapped;
- (IBAction)sliderValueChanged;

@end
