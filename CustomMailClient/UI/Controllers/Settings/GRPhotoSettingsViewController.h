//
//  GRPhotoSettingsViewController.h
//  GrannyApp
//
//  Created by Eugene Pavlyuk on 8/3/12.
//  Copyright (c) 2012 Home. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "GRPickerView.h"
#import "GRMinuteSecondsPickerView.h"

@class Settings;

@interface GRPhotoSlidingTimePickerView : GRMinuteSecondsPickerView

@end


@interface GRPhotoSettingsViewController : GAITrackedViewController <UITableViewDelegate, UITableViewDataSource, UITextFieldDelegate>
{
    Settings *settings;    
}

@property (nonatomic, retain) IBOutlet UITableView *settingsTableView;
@property (nonatomic, retain) IBOutlet UITableViewCell *stepperTableViewCell;

@property (nonatomic, retain) IBOutlet GRPhotoSlidingTimePickerView *timePickerView;

- (IBAction)backButtonTapped;

@end
