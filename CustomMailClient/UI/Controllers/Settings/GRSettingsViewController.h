//
//  GRSettingsViewController.h
//  GrannyApp
//
//  Created by Eugene Pavlyuk on 6/8/12.
//  Copyright (c) 2012 Home. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface GRSettingsViewController : GAITrackedViewController 
{

}

@property (nonatomic, retain) IBOutlet UITableView *settingsTableView;
@property (nonatomic, retain) IBOutlet UIView *containerView;

- (id)initWithNib;

- (IBAction)backButtonTapped;

@end
