//
//  GRContactSettingsViewController.m
//  GrannyApp
//
//  Created by Eugene Pavlyuk on 6/13/12.
//  Copyright (c) 2012 Home. All rights reserved.
//

#import "GRContactSettingsViewController.h"
#import "GRNewContactViewController.h"
#import "GRContact.h"
#import "Favorite.h"
#import "DataManager.h"
#import "ContactTableViewCell.h"
#import "TPKeyboardAvoidingScrollView.h"
#import "GRPeopleManager.h"
#import "MBProgressHUD.h"
#import "GRAlertViewController.h"
#import "WYPopoverController.h"

@interface GRContactSettingsViewController () <GRContactCardDelegate, UITableViewDelegate, UITableViewDataSource, AlertViewDelegate, ContactTableViewCellDelegate, WYPopoverControllerDelegate>

@property (nonatomic, retain) WYPopoverController *alertPopoverController;

@end

@implementation GRContactSettingsViewController

@synthesize contactsTableView;
@synthesize contactNewViewController;
@synthesize spinnerView;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    
    if (self) 
    {
        [[NSNotificationCenter defaultCenter] addObserver:self 
                                                 selector:@selector(applicationDidBecomeActive:) 
                                                     name:UIApplicationDidBecomeActiveNotification 
                                                   object:nil];
        
        [[NSNotificationCenter defaultCenter] addObserver:self 
                                                 selector:@selector(addButtonTapped:) 
                                                     name:@"AddButtonTappedNotification" 
                                                   object:nil];
        
        [[NSNotificationCenter defaultCenter] addObserver:self
                                                 selector:@selector(resyncingContactsNotification:)
                                                     name:kResyncingContactsNotification
                                                   object:nil];
        
        [[NSNotificationCenter defaultCenter] addObserver:self
                                                 selector:@selector(syncingContactsFinishedNotification:)
                                                     name:kSyncingContactsFinishedNotification
                                                   object:nil];
    }
    
    return self;
}

- (void)resyncingContactsNotification:(NSNotification*)notification
{
    [self.contactsTableView reloadData];
}

- (void)syncingContactsFinishedNotification:(NSNotification*)notification
{
    [self.contactsTableView reloadData];
}

- (BOOL)prefersStatusBarHidden
{
    return YES;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.screenName = @"Contacts Settings Screen";
    
    self.contactsTableView.clipsToBounds = YES;
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    
    hud.labelText = NSLocalizedString(@"Sorting...", @"Sorting...");
    
    hud.mode = MBProgressHUDModeIndeterminate;
    
    [[GRPeopleManager sharedManager] updateContactsWithCompletionBlock:^{
       [self.contactsTableView reloadData];
        
        [MBProgressHUD hideHUDForView:self.view animated:YES];
    }];
}

- (IBAction)homeButtonTapped
{
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)addContactButtonTapped
{
    [self addButtonTapped:nil];
}

- (void)addButtonTapped:(NSNotification*)note
{
    self.contactNewViewController = [[[GRNewContactViewController alloc] initWithNibName:@"GRNewSettingsContactViewController_iPad" bundle:nil] autorelease];
    contactNewViewController.isSettings = YES;
    contactNewViewController.contact = nil;
    contactNewViewController.delegate = self;
    
    TPKeyboardAvoidingScrollView *scrollView = (TPKeyboardAvoidingScrollView*)(contactNewViewController.view);
    
    scrollView.scrollEnabled = NO;
    
    [[NSNotificationCenter defaultCenter] removeObserver:contactNewViewController.view];
    [self.view.superview.superview addSubview:[contactNewViewController view]];
}

- (void)reloadTableView
{
    [self.spinnerView stopAnimating];
    [self.contactsTableView reloadData];
}

- (void)removeFromFavorites:(NSInteger)recId
{
    // check if contact is favorite
    Favorite *favoriteTable = [[DataManager sharedInstance] getEntityWithName:@"Favorite"];
    
    NSMutableArray *favorites = [NSMutableArray arrayWithArray:favoriteTable.contacts];
    
    if ([favorites count])
    {
        for (NSString *favorite in favorites)
        {
            if (recId == [favorite intValue])
            {
                [favorites removeObject:favorite];
                break;
            }
        }
    }
    
    favoriteTable.contacts = favorites;
    
    NSMutableDictionary *emails = [NSMutableDictionary dictionaryWithDictionary:favoriteTable.emails];
    
    if ([emails count])
    {
        NSString *key = [NSString stringWithFormat:@"%i", recId];
        
        NSString *value = [emails objectForKey:key];
        
        if (value)
        {
            [emails removeObjectForKey:key];
        }
    }
    
    favoriteTable.contacts = favorites;
    favoriteTable.emails = emails;
    
    [[DataManager sharedInstance] save];
}

- (void)addToFavorites:(NSInteger)recId
{
    // check if contact is favorite
    Favorite *favoriteTable = [[DataManager sharedInstance] getEntityWithName:@"Favorite"];
    
    NSArray *array = favoriteTable.contacts;
    
    NSMutableArray *favorites = [NSMutableArray array];
    [favorites addObjectsFromArray:array];
    
    NSString *rec = [NSString stringWithFormat:@"%i", recId];
    
    [favorites addObject:rec];
    
    favoriteTable.contacts = favorites;
    [[DataManager sharedInstance] save];
}

- (void)applicationDidBecomeActive:(NSNotification*)notification
{
    [self.spinnerView startAnimating];
    
    [[GRPeopleManager sharedManager] updateContactsWithCompletionBlock:^{
        [self reloadTableView];
    }];
}

- (void)releaseViews
{
    self.contactsTableView = nil;
    self.spinnerView = nil;
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    [self releaseViews];
}

- (void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
    [self releaseViews];
    self.contactNewViewController = nil;
    
    [self.alertPopoverController dismissPopoverAnimated:YES];
    self.alertPopoverController = nil;
    
    [super dealloc];
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return UIInterfaceOrientationIsLandscape(interfaceOrientation);
}

- (void)setEditing:(BOOL)editing animated:(BOOL)animated
{
    [super setEditing:editing animated:animated];
    
    [self.contactsTableView setEditing:editing animated:animated];
}

#pragma mark - UITableViewDelegate's methods

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    GRContact *contact = [[GRPeopleManager sharedManager].contacts objectAtIndex:indexPath.row];
    
    self.contactNewViewController = [[[GRNewContactViewController alloc] initWithNibName:@"GRNewSettingsContactViewController_iPad" bundle:nil] autorelease];
    self.contactNewViewController.isSettings = YES;
    contactNewViewController.contact = contact;
    contactNewViewController.delegate = self;
    
    TPKeyboardAvoidingScrollView *scrollView = (TPKeyboardAvoidingScrollView*)(contactNewViewController.view);
    
    scrollView.scrollEnabled = NO;
    
    [[NSNotificationCenter defaultCenter] removeObserver:contactNewViewController.view];
    
    [self.view.superview.superview addSubview:[contactNewViewController view]];
}

- (UITableViewCellEditingStyle)tableView:(UITableView *)tableView editingStyleForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return UITableViewCellEditingStyleDelete;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath 
{
    CGFloat height = [ContactTableViewCell cellHeight];
    
    GRContact *contact = [[GRPeopleManager sharedManager].contacts objectAtIndex:indexPath.row];
    
    NSInteger numberOfEmails = 0;
    
    if ([contact.email1 length])
    {
        numberOfEmails++;
    }
    
    if ([contact.email2 length])
    {
        numberOfEmails++;
    }
    
    if ([contact.email3 length])
    {
        numberOfEmails++;
    }
    
    if ([contact.email4 length])
    {
        numberOfEmails++;
    }
    
    if ([contact.email5 length])
    {
        numberOfEmails++;
    }
    
    if (numberOfEmails > 1)
    {
        numberOfEmails--;
    }
    
    height += (numberOfEmails * 25);
    
    return height;
}

#pragma mark - UITableViewDataSource's methods

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 80.f;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    UIView *headerView = [[[UIView alloc] initWithFrame:CGRectMake(0, 0, tableView.bounds.size.width, 80.f)] autorelease];
    
    headerView.backgroundColor = [UIColor whiteColor];
    
    UILabel *label = [[[UILabel alloc] initWithFrame:CGRectInset(headerView.bounds, 40, 0)] autorelease];
    
    [headerView addSubview:label];
    label.backgroundColor = [UIColor clearColor];
    label.font = [UIFont fontWithName:@"Helvetica-Oblique" size:18];
    label.textColor = rgb(0xee5015);
    label.numberOfLines = 2;
    label.text = NSLocalizedString(@"For better user experience select seven favorite contacts by clicking the star icons", @"For better user experience select seven favorite contacts by clicking the star icons");
    
    UIView *separator = [[UIView alloc] initWithFrame:CGRectMake(0, headerView.bounds.size.height - 1, tableView.bounds.size.width, 1)];
    separator.backgroundColor = RGBCOLOR(190, 190, 190);
    separator.autoresizingMask = UIViewAutoresizingFlexibleTopMargin | UIViewAutoresizingFlexibleWidth;
    [headerView addSubview:separator];
    
    return headerView;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [[GRPeopleManager sharedManager].contacts count];
}

- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    DeleteAlertView *alertView = [[[DeleteAlertView alloc] initWithTitle:NSLocalizedString(@"The contact will be deleted from your device", @"The contact will be deleted from your device")
                                                                 message:nil
                                                                delegate:self
                                                       cancelButtonTitle:nil
                                                       otherButtonTitles:
                                   NSLocalizedString(@"CANCEL", @"CANCEL"),
                                   NSLocalizedString(@"DELETE", @"DELETE"), nil] autorelease];
    alertView.tag = indexPath.row;
    [alertView show];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{            
    ContactTableViewCell *contactTableViewCell = (ContactTableViewCell*)[tableView dequeueReusableCellWithIdentifier:[ContactTableViewCell getReuseIdentifier]];
    
    if (!contactTableViewCell)
    {
        contactTableViewCell = (ContactTableViewCell*)[[[NSBundle mainBundle] loadNibNamed:@"ContactTableViewCell" owner:nil options:nil] lastObject];
    }
    
    contactTableViewCell.indexPath = indexPath;
    
    GRContact *contact = [[GRPeopleManager sharedManager].contacts objectAtIndex:indexPath.row];
    
    contactTableViewCell.delegate = self;
    [contactTableViewCell adaptToContact:contact];
    
    return contactTableViewCell;
}

#pragma mark - AlertViewDelegate's methods

- (void)alertView:(AlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if ([alertView isKindOfClass:[DeleteAlertView class]])
    {
        if (buttonIndex == 1)
        {
            GRContact *contact = [[GRPeopleManager sharedManager].contacts objectAtIndex:alertView.tag];
            
            if ([contact isFavorite] || [[contact selectedEmail] length])
            {
                [self removeFromFavorites:[contact.recId integerValue]];
            }
            
            [[GRPeopleManager sharedManager] removeContactFromAddressBook:contact withCompletionBlock:^{
               [contactsTableView reloadData];
            }];
        }
    }
    else
    {
    }
}

#pragma mark - ContactTableViewCellDelegate's methods

- (void)contactTableViewCell:(ContactTableViewCell*)cell didTapFavoriteButtonAtIndexPath:(NSIndexPath*)indexPath
{
    Favorite *favoriteTable = [[DataManager sharedInstance] getEntityWithName:@"Favorite"];
    
    NSArray *array = favoriteTable.contacts;
    
    GRContact *contact = [[GRPeopleManager sharedManager].contacts objectAtIndex:indexPath.row];
    
    if ([contact isFavorite])
    {
        contact.favorite = NO;
        
        NSMutableDictionary *emails = [NSMutableDictionary dictionaryWithDictionary:favoriteTable.emails];
        
        [emails removeObjectForKey:[contact.recId stringValue]];
        
        favoriteTable.emails = emails;
        
        contact.selectedEmail = nil;
        
        [self removeFromFavorites:[contact.recId integerValue]];
    }
    else
    {
        if ([array count] >= MAX_FAVORITES_COUNT)
        {
            WYPopoverBackgroundView *popoverAppearance = [WYPopoverBackgroundView appearance];
            
            [popoverAppearance setOverlayColor:[UIColor colorWithWhite:0.1f alpha:0.6f]];
            [popoverAppearance setOuterCornerRadius:0];
            [popoverAppearance setOuterShadowBlurRadius:0];
            [popoverAppearance setOuterShadowColor:[UIColor clearColor]];
            [popoverAppearance setOuterShadowOffset:CGSizeZero];
            
            [popoverAppearance setGlossShadowColor:[UIColor clearColor]];
            [popoverAppearance setGlossShadowOffset:CGSizeMake(0, 0)];
            
            [popoverAppearance setBorderWidth:1];
            [popoverAppearance setArrowHeight:0];
            [popoverAppearance setArrowBase:0];
            
            [popoverAppearance setInnerCornerRadius:0];
            [popoverAppearance setInnerShadowBlurRadius:0];
            [popoverAppearance setInnerShadowColor:[UIColor clearColor]];
            [popoverAppearance setInnerShadowOffset:CGSizeMake(0, 0)];
            
            [popoverAppearance setFillTopColor:[UIColor whiteColor]];
            [popoverAppearance setFillBottomColor:[UIColor whiteColor]];
            [popoverAppearance setOuterStrokeColor:[UIColor clearColor]];
            [popoverAppearance setInnerStrokeColor:[UIColor clearColor]];
            
            
            GRAlertViewController *contentViewController = [[[GRAlertViewController alloc] init] autorelease];
            
            contentViewController.contentSizeForViewInPopover = CGSizeMake(324, 175);
            
            [contentViewController view];
            
            contentViewController.messageLabel.text = NSLocalizedString(@"You can only select up to \n7 favorite contacts", @"You can only select up to \n7 favorite contacts");
            
            contentViewController.messageLabel.font = [UIFont fontWithName:@"Helvetica" size:18];
            
            [contentViewController.okButton setBackgroundImage:nil forState:UIControlStateNormal];
            contentViewController.okButton.frame = CGRectMake(0, contentViewController.okButton.frame.origin.y + 30, 50, 28);
            contentViewController.okButton.center = CGPointMake(contentViewController.view.bounds.size.width / 2, contentViewController.okButton.center.y);
            contentViewController.okButton.layer.borderWidth = 1.f;
            contentViewController.okButton.layer.borderColor = RGBCOLOR(190, 190, 190).CGColor;
            [contentViewController.okButton setTitleColor:RGBCOLOR(96, 96, 96) forState:UIControlStateNormal];
            contentViewController.okButton.titleLabel.font = [UIFont fontWithName:@"Helvetica" size:14];
            
            [contentViewController.okButton addTarget:self action:@selector(popoverButtonTapped:) forControlEvents:UIControlEventTouchUpInside];
            
            self.alertPopoverController = [[[WYPopoverController alloc] initWithContentViewController:contentViewController] autorelease];
            self.alertPopoverController.delegate = self;
            
            [self.alertPopoverController presentPopoverAsDialogAnimated:YES];
            
            return ;
        }
        
        [self addToFavorites:[contact.recId integerValue]];
        contact.favorite = YES;
    }
    
    [self.contactsTableView reloadRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationNone];
}

- (void)popoverButtonTapped:(UIButton*)button
{
    [self.alertPopoverController dismissPopoverAnimated:YES];
    self.alertPopoverController = nil;
}

- (void)contactTableViewCell:(ContactTableViewCell*)cell
              didSelectEmail:(NSString*)emailString
           inCellAtIndexPath:(NSIndexPath*)indexPath
{    
    Favorite *favoriteTable = [[DataManager sharedInstance] getEntityWithName:@"Favorite"];
    
    GRContact *contact = [[GRPeopleManager sharedManager].contacts objectAtIndex:indexPath.row];
    
    if ([contact isFavorite])
    {
        NSMutableDictionary *emails = [NSMutableDictionary dictionaryWithDictionary:favoriteTable.emails];
        
        [emails setObject:emailString forKey:[contact.recId stringValue]];
        
        favoriteTable.emails = emails;
        
        contact.selectedEmail = emailString;
        [[GRPeopleManager sharedManager] updateContactSelectedMailForRecId:contact.recId selectedMail:emailString];
        
        [[DataManager sharedInstance] save];
    }
    else
    {
        contact.selectedEmail = nil;
    }
    
    [self.contactsTableView reloadRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationNone];
}

#pragma mark - GRContactCardDelegate's methods

- (void)popContactCard:(UIViewController*)contactCard
{
    [contactCard.view removeFromSuperview];
    
    if (contactCard == self.contactNewViewController)
    {
        self.contactNewViewController = nil;
        
        [self.spinnerView startAnimating];
        
        [[GRPeopleManager sharedManager] updateContactsWithCompletionBlock:^{
            [self reloadTableView];
        }];
    }
}

#pragma mark - WYPopoverControllerDelegate's methods

- (BOOL)popoverControllerShouldDismissPopover:(WYPopoverController *)popoverController
{
    return YES;
}

- (void)popoverControllerDidDismissPopover:(WYPopoverController *)popoverController
{
    self.alertPopoverController = nil;
}

@end
