//
//  GRContactSettingsViewController.h
//  GrannyApp
//
//  Created by Eugene Pavlyuk on 6/13/12.
//  Copyright (c) 2012 Home. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AlertView.h"
#import "ContactTableViewCellDelegate.h"
#import "GRContactCardDelegate.h"

@class GRNewContactViewController;

@interface GRContactSettingsViewController : GAITrackedViewController
{
}

@property (nonatomic, retain) IBOutlet UITableView *contactsTableView;
@property (nonatomic, retain) IBOutlet UIActivityIndicatorView *spinnerView;

@property (nonatomic, retain) GRNewContactViewController *contactNewViewController;

- (IBAction)homeButtonTapped;
- (IBAction)addContactButtonTapped;

@end
