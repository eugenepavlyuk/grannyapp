//
//  GRPhotoSettingsViewController.m
//  GrannyApp
//
//  Created by Eugene Pavlyuk on 8/3/12.
//  Copyright (c) 2012 Home. All rights reserved.
//

#import "GRPhotoSettingsViewController.h"
#import "Settings.h"
#import "DataManager.h"


@implementation GRPhotoSlidingTimePickerView

- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component
{
    NSInteger second = [self selectedRowInComponent:1] % 60;
    NSInteger minutes = [self selectedRowInComponent:0] % 60;
    
    if (second == 0 && minutes == 0)
    {
        [self selectRow: row + 1
            inComponent: 1
               animated: NO];
    }
}

@end

@interface GRPhotoSettingsViewController ()

@property (nonatomic, retain) Settings *settings;

@end

@implementation GRPhotoSettingsViewController

@synthesize settingsTableView;
@synthesize stepperTableViewCell;
@synthesize settings;

- (BOOL)prefersStatusBarHidden
{
    return YES;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    
    self.screenName = @"Photo Settings Screen";
    
    if (!self.settings)
    {
        self.settings = [[DataManager sharedInstance] getEntityWithName:kSettingsEntityName];        
    }
    
    [self.timePickerView setMinute:[self.settings.slidingTimeMinutes intValue]];
    [self.timePickerView setSecond:[self.settings.slidingTimeSeconds intValue]];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    
    self.settings.slidingTimeMinutes = @([self.timePickerView getMinute]);
    self.settings.slidingTimeSeconds = @([self.timePickerView getSecond]);
    
    [[DataManager sharedInstance] save];
}

- (void)releaseViews
{
    self.settingsTableView = nil;
    self.stepperTableViewCell = nil;
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    
    [self releaseViews];
}

- (void)dealloc
{
    [self releaseViews];
    self.settings = nil;
    
    [super dealloc];
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
	return UIInterfaceOrientationIsLandscape(interfaceOrientation);
}


- (IBAction)backButtonTapped
{
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark - UITableViewDelegate's methods

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath 
{
    return 252;
}

#pragma mark - UITableViewDataSource's methods

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{    
    return 1;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{  
    return self.stepperTableViewCell;
}

#pragma mark - UITextFieldDelegate's methods

- (void)textFieldDidBeginEditing:(UITextField *)textField
{
    
}

- (void)textFieldDidEndEditing:(UITextField *)textField
{
    self.settings.slidingTimeSeconds = [NSNumber numberWithInt:[textField.text intValue]];
    
    [[DataManager sharedInstance] save];
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    if(textField.text.length - range.length + string.length > 2)
    {
        return NO;
    }

    NSString *template = @"0123456789";
    
    if ([string length] > 0)
    {
        NSRange range = [template rangeOfString:string];
        
        if (range.location != NSNotFound)
        {
            return YES;
        }
        
        return NO;
    }
    
    return YES;
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    
    self.settings.slidingTimeSeconds = [NSNumber numberWithInt:[textField.text intValue]];
    
    [[DataManager sharedInstance] save];
    
    return YES;
}

@end
