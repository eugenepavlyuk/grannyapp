//
//  GRScreenSaverSettingsViewController.m
//  GrannyApp
//
//  Created by Eugene Pavlyuk on 8/5/12.
//  Copyright (c) 2012 Home. All rights reserved.
//

#import "GRScreenSaverSettingsViewController.h"
#import "Settings.h"
#import "DataManager.h"
#import "MICheckBox.h"
#import "UIImage+Border.h"
#import "BDGridCell.h"

@interface GRScreenSaverSettingsViewController () <MICheckBoxDelegate>

@property (nonatomic, retain) Settings *settings;

@end

@implementation GRScreenSaverSettingsViewController
{
    NSMutableArray *radioButtons;

    ALAssetsLibrary *library;
}

@synthesize settingsTableView;
@synthesize personalMessageTableViewCell;
@synthesize screenSaverTableViewCell;
@synthesize durationTableViewCell;
@synthesize clockTableViewCell;
@synthesize photoslideshowTableViewCell;
@synthesize imageTableViewCell;
@synthesize sliderTableViewCell;
@synthesize photoImageView;
@synthesize settings;
@synthesize durationSlider;
@synthesize durationLabel;
@synthesize parentController;
@synthesize fldPersonalMessage;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    
    if (self) 
    {
        radioButtons = [[NSMutableArray array] retain];
    }
    
    return self;
}

- (BOOL)prefersStatusBarHidden
{
    return YES;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    
    self.screenName = @"Screensaver Settings Screen";
    
    if (!self.settings)
    {
        self.settings = [[DataManager sharedInstance] getEntityWithName:kSettingsEntityName];        
    }
    
    if (IS_IPAD)
    {
        [self.durationSlider setThumbImage:[UIImage imageNamed:@"screensaver_slider_btn"] forState:UIControlStateNormal];
        [self.durationSlider setMinimumTrackImage:[[UIImage imageNamed:@"screensaver_slider_progress"] resizableImageWithCapInsets:UIEdgeInsetsMake(0, 10, 0, 10)] forState:UIControlStateNormal];
        [self.durationSlider setMaximumTrackImage:[[UIImage imageNamed:@"screensaver_slider_progress"] resizableImageWithCapInsets:UIEdgeInsetsMake(0, 10, 0, 10)] forState:UIControlStateNormal];
    }
    
    self.durationSlider.value = [settings.duration intValue];
    self.durationLabel.text = [NSString stringWithFormat:@"%i min", [settings.duration intValue]];
    
    CGRect boxRect;
    
    if (IS_IPAD)
    {
        boxRect = CGRectMake(13, 14, 150, 42);
    }
    else
    {
        boxRect = CGRectMake(13, 1, 150, 42);
    }
    
//    MICheckBox *radioCheckBox = [[[MICheckBox alloc]initWithFrame:boxRect] autorelease];
//    radioCheckBox.delegate = self;
//    radioCheckBox.tag = 0;
//    radioCheckBox.isChecked = [self.settings.radioOn boolValue];
    
    MICheckBox *clockCheckBox = [[[MICheckBox alloc]initWithFrame:boxRect] autorelease];
    clockCheckBox.delegate = self;
    clockCheckBox.tag = 0;
    clockCheckBox.isChecked = [self.settings.clockOn boolValue];

    MICheckBox *slideCheckBox = [[[MICheckBox alloc]initWithFrame:boxRect] autorelease];
    slideCheckBox.delegate = self;
    slideCheckBox.tag = 1;
    slideCheckBox.isChecked = [self.settings.photoSliderOn boolValue];
    
    MICheckBox *imageCheckBox = [[[MICheckBox alloc]initWithFrame:boxRect] autorelease];
    imageCheckBox.delegate = self;
    imageCheckBox.tag = 2;
    imageCheckBox.isChecked = [self.settings.imageOn boolValue];
    
//    [radioButtons addObject:radioCheckBox];
    [radioButtons addObject:clockCheckBox];
    [radioButtons addObject:slideCheckBox];
    [radioButtons addObject:imageCheckBox];
    
    if (![settings.screenSaverState boolValue])
    {        
        [self disableAllButtons];
    }
    else
    {
        [self enableAllButtons];
    }
    
    self.photoImageView.layer.cornerRadius = 10.f;
    self.photoImageView.clipsToBounds = YES;
    self.photoImageView.layer.borderWidth = 1.f;
    self.photoImageView.layer.borderColor = RGBCOLOR(121, 121, 121).CGColor;
    
    [self loadImage];
    
    self.photoContentView.minimumPadding = 5.f;
    
    [self startLoadingPhotoFromGallery];
}

- (void)startLoadingPhotoFromGallery
{
    //#if TARGET_IPHONE_SIMULATOR
    //#else
    void (^assetEnumerator)(ALAsset *, NSUInteger, BOOL *) = ^(ALAsset *result, NSUInteger index, BOOL *stop) {
        if(result != NULL) {
            //            NSLog(@"See Asset: %@", result);
            [assets addObject:result];
        }
    };
    
    void (^assetGroupEnumerator)(ALAssetsGroup *, BOOL *) =  ^(ALAssetsGroup *group, BOOL *stop) {
    	if(group != nil) {
            [group enumerateAssetsUsingBlock:assetEnumerator];
        } else {
            [self.photoContentView reloadData];
        }
    };
    
    assets = [[NSMutableArray alloc] init];
    library = [[ALAssetsLibrary alloc] init];
    [library enumerateGroupsWithTypes:ALAssetsGroupSavedPhotos
                           usingBlock:assetGroupEnumerator
                         failureBlock: ^(NSError *error) {
                             NSLog(@"Failure");
                         }];
    //#endif
    
}

- (void)loadImage
{
    if ([self.settings.linkToImage length])
    {        
        [image release];
        image = nil;
        
        image = [[UIImage imageWithContentsOfFile:self.settings.linkToImage] retain];
        
        self.photoImageView.image = image;
    }
}

- (void)disableAllButtons
{
    self.photoslideshowTableViewCell.hidden = YES;
    self.clockTableViewCell.hidden = YES;
    self.imageTableViewCell.hidden = YES;
    self.durationTableViewCell.hidden = YES;
//    for (UIButton *button in radioButtons)
//    {
//        button.alpha = 0.6f;
//        [button setUserInteractionEnabled:NO];
//    }
}

- (void)enableAllButtons
{
    self.photoslideshowTableViewCell.hidden = NO;
    self.clockTableViewCell.hidden = NO;
    self.imageTableViewCell.hidden = NO;
    self.durationTableViewCell.hidden = NO;
//    for (UIButton *button in radioButtons)
//    {
//        button.alpha = 1.f;
//        [button setUserInteractionEnabled:YES];
//    }    
}

- (void)uncheckAllButtons
{
    for (MICheckBox *button in radioButtons)
    {
        button.isChecked = NO;
    }
}

- (IBAction)backButtonTapped
{
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)checkButtonTapped:(MICheckBox*)button
{
    NSInteger index = button.tag;
    
    BOOL isChecked = button.isChecked;
    
    if (!isChecked)
    {
        button.isChecked = YES;
        return ;
    }
    
    [self uncheckAllButtons];

    button.isChecked = YES;

    switch (index)
    {
//        case 0:
//        {
//            settings.radioOn = [NSNumber numberWithBool:YES];
//            settings.clockOn = [NSNumber numberWithBool:NO];
//            settings.photoSliderOn = [NSNumber numberWithBool:NO];
//            settings.imageOn = [NSNumber numberWithBool:NO];
//        }; break ;
            
        case 0:
        {
            settings.radioOn = [NSNumber numberWithBool:NO];
            settings.clockOn = [NSNumber numberWithBool:YES];
            settings.photoSliderOn = [NSNumber numberWithBool:NO];
            settings.imageOn = [NSNumber numberWithBool:NO];
        }; break ;

        case 1:
        {
            settings.radioOn = [NSNumber numberWithBool:NO];
            settings.clockOn = [NSNumber numberWithBool:NO];
            settings.photoSliderOn = [NSNumber numberWithBool:YES];
            settings.imageOn = [NSNumber numberWithBool:NO];
        }; break ;

        case 2:
        {
            settings.radioOn = [NSNumber numberWithBool:NO];
            settings.clockOn = [NSNumber numberWithBool:NO];
            settings.photoSliderOn = [NSNumber numberWithBool:NO];
            settings.imageOn = [NSNumber numberWithBool:YES];
        }; break ;
    }
    
    [[DataManager sharedInstance] save];
}

- (IBAction)valueChanged:(UIButton*)sender
{
    if (sender == self.useSSButtonOff)
    {
        self.useSSButtonOn.selected = NO;
        self.useSSButtonOff.selected = YES;
        settings.screenSaverState = [NSNumber numberWithBool:NO];
        
        [self disableAllButtons];
    }
    else
    {
        self.useSSButtonOn.selected = YES;
        self.useSSButtonOff.selected = NO;
        settings.screenSaverState = [NSNumber numberWithBool:YES];
        
        [self enableAllButtons];
    }
        
    [[DataManager sharedInstance] save];
    
    //[[NSNotificationCenter defaultCenter] postNotificationName:@"ScreenSaverStateChangedNotification" object:nil];
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    [self releaseViews];
}

- (void)dealloc
{
    [self releaseViews];
    self.settings = nil;
    [radioButtons release];
    [fldPersonalMessage release];
    
    [image release];
    image = nil;
    
    [assets release];
    
    [super dealloc];
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    if (IS_IPAD)
    {
        return UIInterfaceOrientationIsLandscape(interfaceOrientation);
    }
    else
    {
        return YES;
    }
}

- (void)releaseViews
{
    self.photoImageView = nil;
    self.settingsTableView = nil;
    self.useSSButtonOn = nil;
    self.useSSButtonOff = nil;
    self.screenSaverTableViewCell = nil;
    self.durationTableViewCell = nil;
    self.clockTableViewCell = nil;
    self.photoslideshowTableViewCell = nil;
    self.sliderTableViewCell = nil;
    self.durationSlider = nil;
    self.durationLabel = nil;
    self.addPhotoPopover = nil;
    self.photoContentView = nil;
}

- (void)imagePicker
{
    [self.parentController.view addSubview:self.addPhotoPopover];
}

- (IBAction)hidePhotoPopoverButtonTapped
{
    [self.addPhotoPopover removeFromSuperview];
}

- (void)returnImageToParent:(UIImage*)theImage
{
    NSDate *date = [NSDate date];
    
    NSString *iconName = [[[NSString stringWithFormat:@"%f", [date timeIntervalSince1970]] stringByReplacingOccurrencesOfString:@"." withString:@"_"] stringByAppendingPathExtension:@"png"];
    
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES);
    NSString *dirPath = [paths objectAtIndex:0];
    
    dirPath = [dirPath stringByAppendingPathComponent:iconName];
    
    NSData *data = UIImagePNGRepresentation(theImage);
    
    [[NSFileManager defaultManager] createFileAtPath:dirPath contents:data attributes:nil];
    
    self.settings.linkToImage = dirPath;
    
    [[DataManager sharedInstance] save];
    
    [self loadImage];
}

- (IBAction)sliderValueChanged:(UISlider*)slider
{
    self.settings.duration = [NSNumber numberWithInt:slider.value];
    [[DataManager sharedInstance] save];
    
    self.durationLabel.text = [NSString stringWithFormat:@"%i min", [settings.duration intValue]];
}

#pragma mark - UITableViewDelegate's methods

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    if (indexPath.row == 5)
    {
        [self imagePicker];
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath 
{
    if (indexPath.row == 0)
    {
        return 88.f;
    }
    else if (indexPath.row == 5)
    {
        return 135.f;
    }
    else
    {
        return 72.f;
    }
    
    if (IS_IPAD)
    {
        return 70;
    }
    else 
    {
        return 44;
    }
}

#pragma mark - UITableViewDataSource's methods

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 6;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.row == 0)
    {
        if (self.settings && [self.settings.personalMessage length])
            self.fldPersonalMessage.text = self.settings.personalMessage;
        
        return self.personalMessageTableViewCell;
    }

    if (indexPath.row == 1)
    {
        if ([self.settings.screenSaverState boolValue] == YES)
        {
            self.useSSButtonOn.selected = YES;
            self.useSSButtonOff.selected = NO;
        }
        else
        {
            self.useSSButtonOn.selected = NO;
            self.useSSButtonOff.selected = YES;
        }
        
        return self.screenSaverTableViewCell;
    }
    else if (indexPath.row == 2)
    {
        self.durationSlider.value = [self.settings.duration intValue];
        
        return durationTableViewCell;
    }
    else if (indexPath.row == 3)
    {
        UIButton *button = [radioButtons objectAtIndex:indexPath.row - 3];
        
        [button removeFromSuperview];
        
        [self.clockTableViewCell.contentView addSubview:button];
        
        return clockTableViewCell;
    }
    else if (indexPath.row == 4)
    {
        UIButton *button = [radioButtons objectAtIndex:indexPath.row - 3];
        
        [button removeFromSuperview];
        
        [self.photoslideshowTableViewCell.contentView addSubview:button];

        return self.photoslideshowTableViewCell;
    }
    else
    {
        UIButton *button = [radioButtons objectAtIndex:indexPath.row - 3];
        
        [button removeFromSuperview];
        
        [self.imageTableViewCell.contentView addSubview:button];

        return self.imageTableViewCell;
    }
    
    return nil;
}

#pragma mark - UITextFieldDelegate
- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    if (textField.text.length > 45)
        return NO;
    return YES;
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    self.settings.personalMessage = textField.text;
    return YES;
}

- (void)textFieldDidEndEditing:(UITextField *)textField
{
    self.settings.personalMessage = textField.text;
    [textField resignFirstResponder];
}

#pragma mark - BDGridViewDataSource's methods

- (NSUInteger)gridViewCountOfCells:(BDGridView *)gridView
{
    return [assets count];
}

- (CGSize)gridViewSizeOfCell:(BDGridView *)gridView
{
    if (IS_IPAD)
    {
        CGFloat width = 126;
        
        return CGSizeMake(width, width);
    }
    else
    {
        CGFloat width = (gridView.bounds.size.width - 3 * gridView.minimumPadding)/ 4;
        
        return CGSizeMake(width, width);
    }
    
    return CGSizeZero;
}

- (BDGridCell *)gridView:(BDGridView *)gridView cellForIndex:(NSUInteger)index
{
    ALAsset *asset = [assets objectAtIndex:index];
    UIImage *pImage = [[UIImage imageWithCGImage:[asset aspectRatioThumbnail]] imageWithBorderWidth:0 andColor:[UIColor whiteColor].CGColor];
    
    PhotoCell *cell = (PhotoCell*)[gridView dequeueCell];
    
    if (!cell)
    {
        cell = [[[PhotoCell alloc] initCellWithRoundedCorners] autorelease];
    }
    
    cell.photoImageView.image = pImage;
    cell.layer.shadowOpacity = 0.5f;
    
    cell.layer.borderWidth = 1.f;
    cell.layer.borderColor = RGBCOLOR(121, 121, 121).CGColor;
    
    return cell;
}

#pragma mark - BDGridViewDelegate's methods

- (void)gridView:(BDGridView *)gridView didTapCell:(BDGridCell *)cell
{
    ALAsset *asset = [assets objectAtIndex:cell.index];
    
    UIImage *theImage = [UIImage imageWithCGImage:[[asset defaultRepresentation] fullScreenImage]];
    
    [self returnImageToParent:theImage];
    
    [self.addPhotoPopover removeFromSuperview];
}

@end
