//
//  GRScreenSaverSettingsViewController.h
//  GrannyApp
//
//  Created by Eugene Pavlyuk on 8/5/12.
//  Copyright (c) 2012 Home. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BDGridView.h"

@class Settings;

@interface GRScreenSaverSettingsViewController : GAITrackedViewController <UITableViewDelegate, UITableViewDataSource, UITextFieldDelegate, BDGridViewDataSource, BDGridViewDelegate>
{
    Settings *settings;
    
    UIImage *image;
    
    NSMutableArray *assets;
}

@property (nonatomic, retain) IBOutlet UITableView *settingsTableView;

@property (nonatomic, retain) IBOutlet UITableViewCell *personalMessageTableViewCell;
@property (nonatomic, retain) IBOutlet UITableViewCell *sliderTableViewCell;
@property (nonatomic, retain) IBOutlet UITableViewCell *screenSaverTableViewCell;
@property (nonatomic, retain) IBOutlet UITableViewCell *durationTableViewCell;
@property (nonatomic, retain) IBOutlet UITableViewCell *clockTableViewCell;
@property (nonatomic, retain) IBOutlet UITableViewCell *photoslideshowTableViewCell;
@property (nonatomic, retain) IBOutlet UITableViewCell *imageTableViewCell;
@property (nonatomic, retain) IBOutlet UISlider *durationSlider;
@property (nonatomic, retain) IBOutlet UIImageView *photoImageView;
@property (nonatomic, retain) IBOutlet UILabel *durationLabel;
@property (nonatomic, retain) IBOutlet UITextField *fldPersonalMessage;

@property (nonatomic, retain) IBOutlet UIButton *useSSButtonOn;
@property (nonatomic, retain) IBOutlet UIButton *useSSButtonOff;

@property (nonatomic, assign) UIViewController *parentController;

@property (nonatomic, retain) IBOutlet BDGridView *photoContentView;
@property (nonatomic, retain) IBOutlet UIView *addPhotoPopover;

- (IBAction)sliderValueChanged:(UISlider*)slider;
- (IBAction)backButtonTapped;
- (IBAction)hidePhotoPopoverButtonTapped;

@end
