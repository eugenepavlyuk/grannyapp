//
//  GRMailSetLoginController.h
//  GrannyApp
//
//  Created by Ievgen Pavliuk on 5/10/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "GRMailServiceManager.h"

@interface GRMailSetLoginController : GAITrackedViewController <UITextFieldDelegate, GRMailServiceDelegate> {

    UITextField *tUserName;
    UITextField *tPassword;
    UITextField *tAccountName;
    
    UILabel     *lTitle;
    
    NSMutableDictionary *mailSettings;
    
    UIButton            *bValidate;
}
@property (nonatomic, retain) IBOutlet UITextField *tUserName;
@property (nonatomic, retain) IBOutlet UITextField *tPassword;
@property (nonatomic, retain) IBOutlet UITextField *tAccountName;
@property (nonatomic, retain) IBOutlet UILabel *lTitle;
@property (nonatomic, retain) IBOutlet UIButton *bValidate;


- (IBAction)validateBtnTapped:(id)sender;
@end
