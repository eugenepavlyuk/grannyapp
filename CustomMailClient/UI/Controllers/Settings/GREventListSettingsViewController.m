//
//  GREventListSettingsViewController.m
//  GrannyApp
//
//  Created by Eugene Pavlyuk on 8/8/12.
//  Copyright (c) 2012 Home. All rights reserved.
//

#import "GREventListSettingsViewController.h"
#import "GRNewProtectedEventViewController.h"
#import "Event.h"
#import "DataManager.h"
#import "AlarmManager.h"
#import "ProtectedEventTableViewCell.h"
#import "NSString+Additions.h"
#import <QuartzCore/QuartzCore.h>
#import "CalendarView.h"
#import "CalendarMonth.h"
#import "Settings.h"
#import "GRHourView.h"
#import "GRHourCell.h"
#import "GRHeaderCell.h"
#import "GRWeekdayBarView.h"
#import "GridView.h"
#import "GREventView.h"
#import "GRDayHeader.h"
#import "GRDayCell.h"

@interface GREventListSettingsViewController () <GREventInputDelegate>

@property (nonatomic, retain) GRNewProtectedEventViewController *eventController;
@property (nonatomic, retain) NSMutableArray *events;

@property (nonatomic, assign) sortingType typeOfSorting;
@property (nonatomic, retain) Settings *settings;

@end

@implementation GREventListSettingsViewController

@synthesize settingsTableView;
@synthesize parentController;

@synthesize events;

@synthesize weekView;
@synthesize tableContentView;
@synthesize weekContentView;
@synthesize calendarView;
@synthesize monthContentView;
@synthesize addButton;
@synthesize settings;

@synthesize typeOfSorting;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    
    if (self) 
    {
        typeOfSorting = ST_List;
        
        [[NSNotificationCenter defaultCenter] addObserver:self
                                                 selector:@selector(applicationDidBecomeActive:)
                                                     name:UIApplicationDidBecomeActiveNotification
                                                   object:nil];
    }
    
    return self;
}

- (BOOL)prefersStatusBarHidden
{
    return YES;
}

- (void)applicationDidBecomeActive:(NSNotification*)note
{
    self.weekView.week = [NSDate date];
    [self.calendarView reloadData];
}

- (Settings*)settings
{
    if (!settings)
    {
        self.settings = [[DataManager sharedInstance] getEntityWithName:kSettingsEntityName];
    }
    
    return settings;
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    [[NSNotificationCenter defaultCenter] addObserver:self 
                                             selector:@selector(addButtonTapped:) 
                                                 name:@"AddButtonTappedNotification" 
                                               object:nil];
    
    [self.settingsTableView reloadData];
//    [self.calendarView.calendarView refresh];
//    [self.calendarView reloadData];
    
    [self.calendarView performSelector:@selector(changeMonthToCurrent) withObject:nil afterDelay:0.1f];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:@"AddButtonTappedNotification"
                                                  object:nil];
}


- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.screenName = @"Event List in Settings Screen";
    
    [[GRHourView appearance] setYOffset:@(30)];
    [[GRHourView appearance] setTextFont:[UIFont fontWithName:@"Helvetica" size:14]];
    [[GRHourCell appearance] setBorderColor:[UIColor clearColor]];
    [[GRHeaderCell appearance] setTitleFont:[UIFont fontWithName:@"Helvetica" size:14]];
    
    [[GRDayHeader appearance] setTitleFont:[UIFont fontWithName:@"Helvetica" size:18]];
    
    [[GREventView appearance] setLockImage:[UIImage imageNamed:@"alarm_settings_lock_icon"]];
    [[GREventView appearance] setEventImage:[UIImage imageNamed:@"alarm_settings_alarm_icon"]];
    
    [[GRDayCell appearance] setPreviousMonthDayColor:RGBCOLOR(177, 177, 177)];
    [[GRDayCell appearance] setCurrentMonthDayColor:RGBCOLOR(102, 102, 102)];
//    [[GRDayCell appearance] setTodayDayColor:RGBCOLOR(127, 127, 127)];
    [[GRDayCell appearance] setTodayDayTitleColor:[UIColor blackColor]];
    //[[GRDayCell appearance] setTodayBackgroundDayColor:RGBCOLOR(232, 246, 246)];
    //[[GRDayCell appearance] setEventBackgroundColor:RGBCOLOR(226, 230, 254)];
    //[[GRDayCell appearance] setEventProtectedBackgroundColor:RGBACOLOR(226, 230, 254, 0.8f)];
    [[GRDayCell appearance] setEventTitleColor:RGBCOLOR(96, 96, 96)];
    [[GRDayCell appearance] setDayFont:[UIFont fontWithName:@"Helvetica-Bold" size:21]];
    
    [[GRDayCell appearance] setLockImage:[UIImage imageNamed:@"alarm_settings_lock_icon"]];
    [[GRDayCell appearance] setEventImage:[UIImage imageNamed:@"alarm_settings_alarm_icon"]];
    
//    self.weekView.weekdayBarView.todayBackgroundColor = RGBCOLOR(226, 231, 254);
//    self.weekView.gridView.todayBackgroundColor = RGBCOLOR(226, 231, 254);
    self.weekView.colorForEvent = RGBCOLOR(226, 231, 254);
    self.weekView.weekdayBarView.todayColor = RGBCOLOR(47, 77, 250);
    self.weekView.settings = self.settings;
    self.weekView.week = [NSDate date];
    
    self.todayButton.hidden = YES;
    self.listButton.selected = YES;
    
    self.calendarView.calendarViewControllerDelegate = self;
    
    [[NSNotificationCenter defaultCenter] addObserver:self 
                                             selector:@selector(updateListNotification:)
                                                 name:@"UpdateEventsListNotification" 
                                               object:nil];
    
    self.events = [NSMutableArray array];
    [self updateListNotification:nil];
}

- (IBAction)sortButtonTapped:(id)sender
{
    UIButton *button = (UIButton*)sender;
    
    self.typeOfSorting = button.tag;
    
    [self updateListNotification:nil];
}

- (void)updateListNotification:(NSNotification*)notification
{
    [self.events removeAllObjects];
    
    NSMutableArray *allEvents = [NSMutableArray arrayWithArray:[[DataManager sharedInstance] allRowsForEntity:kEventEntityName]];
    
    [self.events addObjectsFromArray:allEvents];
    
    [self.settingsTableView  reloadData];
    [self.weekView reloadData];
    [self.calendarView reloadData];
}

- (void)addButtonTapped:(NSNotification*)notification
{
    self.eventController = [[[GRNewProtectedEventViewController alloc] init] autorelease];
    self.eventController.inputDelegate = self;
    self.eventController.allowEdit = YES;
    [self.eventController view].frame = self.parentController.view.bounds;
    [self.parentController.view addSubview:[self.eventController view]];
}

- (IBAction)addButtonTapped
{
    self.eventController = [[[GRNewProtectedEventViewController alloc] init] autorelease];
    self.eventController.inputDelegate = self;
    self.eventController.allowEdit = YES;
    
    [self.navigationController pushViewController:self.eventController animated:YES];
}

- (IBAction)homeButtonTapped:(id)sender
{
    [self.navigationController popToRootViewControllerAnimated:YES];
}

- (IBAction)backButtonTapped:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)todayButtonTapped:(id)sender
{
    if (typeOfSorting == ST_List)
    {
        
    }
    else if (typeOfSorting == ST_WEEKLY)
    {
        [self.weekView changeWeekToday];
    }
    else
    {
        [self.calendarView changeMonthToCurrent];
    }
}

- (void)releaseViews
{
    self.settingsTableView = nil;
    self.addButton = nil;
    self.tableContentView = nil;
    self.weekContentView = nil;
    self.weekView = nil;
    self.weekButton = nil;
    self.monthButton = nil;
    self.listButton = nil;
    self.todayButton = nil;
    self.calendarView = nil;
    self.monthContentView = nil;
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    
    [self releaseViews];
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    //    return YES;
    
    return UIInterfaceOrientationIsLandscape(interfaceOrientation);
}

- (void)dealloc
{
    [self releaseViews];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self];
    self.events = nil;
    self.settings = nil;
    self.eventController = nil;
    
    [super dealloc];
}

- (void)setTypeOfSorting:(sortingType)nTypeOfSorting
{
    typeOfSorting = nTypeOfSorting;
    
    if (typeOfSorting == ST_List)
    {
        self.tableContentView.hidden = NO;
        self.weekContentView.hidden = YES;
        self.monthContentView.hidden = YES;
        
        self.todayButton.hidden = YES;
        self.listButton.selected = YES;
        self.monthButton.selected = NO;
        self.weekButton.selected = NO;
    }
    else if (typeOfSorting == ST_WEEKLY)
    {
        self.tableContentView.hidden = YES;
        self.weekContentView.hidden = NO;
        [weekView scrollTo7Hours];
        self.monthContentView.hidden = YES;
        
        self.todayButton.hidden = NO;
        
        self.listButton.selected = NO;
        self.monthButton.selected = NO;
        self.weekButton.selected = YES;
    }
    else
    {
        self.monthContentView.hidden = NO;
        self.weekContentView.hidden = YES;
        self.tableContentView.hidden = YES;
        [self.calendarView setup];
        
        self.todayButton.hidden = NO;
        
        self.listButton.selected = NO;
        self.monthButton.selected = YES;
        self.weekButton.selected = NO;
    }
}

#pragma mark - UITableViewDelegate's methods

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (IS_IPAD)
    {
        return 85.f;
    }
    
    return 44.f;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    Event *event = [self.events objectAtIndex:indexPath.section];
    
    self.eventController = [[[GRNewProtectedEventViewController alloc] init] autorelease];
    self.eventController.inputDelegate = self;
    self.eventController.allowEdit = YES;
    
    self.eventController.currentEvent = event;
    
    if (IS_IPAD)
    {
        [self.eventController view].frame = self.parentController.view.bounds;
        [self.parentController.view addSubview:[self.eventController view]];
    }
    else
    {
        [self.navigationController pushViewController:self.eventController animated:YES];
    }
}

#pragma mark - UITableViewDataSource methods

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return [self.events count];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 1;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    ProtectedEventTableViewCell *cell = (ProtectedEventTableViewCell*)[tableView dequeueReusableCellWithIdentifier:@"ProtectedEventTableViewCellIdentifier"];
    
    if (!cell)
    {
        if (IS_IPAD)
        {
            cell = [[[NSBundle mainBundle] loadNibNamed:@"ProtectedEventTableViewCell" owner:nil options:nil] lastObject];
        }
        else
        {
            cell = [[[NSBundle mainBundle] loadNibNamed:@"EventTableViewCell_iphone" owner:nil options:nil] lastObject];
        }
    }
    
    UIImageView *imageView = [[[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 9, 24)] autorelease];
    imageView.image = [UIImage imageNamed:@"event_arrow"];
    cell.accessoryView = imageView;
    
    Event *event = [self.events objectAtIndex:indexPath.section];
    
    cell.protectedIcon.hidden = ![event.persistent boolValue];
    
    if (!cell.protectedIcon.hidden)
    {
        cell.timeLabel.font = [UIFont fontWithName:@"Helvetica-Oblique" size:18];
    }
    else
    {
        cell.timeLabel.font = [UIFont fontWithName:@"Helvetica-BoldOblique" size:18];
    }
    
    cell.alarmIcon.hidden = !cell.protectedIcon.hidden;
    cell.protectedIcon.image = [UIImage imageNamed:@"settings_protected_calendar_lock"];
    cell.titleLabel.frame = CGRectMake(80, cell.titleLabel.frame.origin.y, tableView.frame.size.width, cell.titleLabel.frame.size.height);
    
//    NSString *dateTime = @"";
    
    NSString *time = @"";
    
    NSDateFormatter *dateFormatter = [[[NSDateFormatter alloc] init] autorelease];
    
    if ([settings.timeFormat intValue])
    {
        [dateFormatter setDateFormat:@"HH:mm"];
    }
    else
    {
        [dateFormatter setDateFormat:@"hh:mm a"];
    }
    
    time = [dateFormatter stringFromDate:event.date];
    
//    switch ([event.type intValue])
//    {
//        case 0:
//        {
//            dateTime = [NSString stringWithFormat:@"Daily at %@", time];
//        }; break;
//            
//        case 1:
//        {
//            NSDateFormatter *df = [[[NSDateFormatter alloc] init] autorelease];
//            
//            [df setDateFormat:@"EEEE"];
//            
//            NSString *day = [df stringFromDate:event.date];
//                        
//            dateTime = [NSString stringWithFormat:@"every %@ at %@", day, time];
//            
//        };    break;
//            
//        case 2:
//        {
//            NSDateFormatter *df = [[[NSDateFormatter alloc] init] autorelease];
//            
//            [df setDateFormat:@"d"];
//            
//            NSInteger dayInt = [[df stringFromDate:event.date] intValue];
//            
//            NSString *day = [NSString ordinalNumberFormat:[NSNumber numberWithInt:dayInt]];
//                        
//            dateTime = [NSString stringWithFormat:@"on the %@ of every month, at %@", day, time];
//            
//        }; break;
//            
//        case 4:
//        {
//            NSDateFormatter *df = [[[NSDateFormatter alloc] init] autorelease];
//            
//            [df setDateFormat:@"dd, MMM yyyy, "];
//            
//            dateTime = [df stringFromDate:event.date];
//            
//            dateTime = [NSString stringWithFormat:@"On %@%@", dateTime, time];
//        }; break;
//            
//        case 3:
//        {
//            NSDateFormatter *df = [[[NSDateFormatter alloc] init] autorelease];
//            
//            [df setDateFormat:@"dd, MMMM, yyyy"];
//            
//            NSString *date = [df stringFromDate:event.date];
//            
//            dateTime = [NSString stringWithFormat:@"on %@ at %@", date, time];
//        };    break;
//    }
//    
//    cell.titleLabel.text = [NSString stringWithFormat:@"%@ %@", event.name, dateTime];
//    
    NSString *repeatStr = nil;
    if (event.type)
    {
        switch ([event.type intValue])
        {
            case ET_Once:
                repeatStr = NSLocalizedString(@"Once", @"Once");
                break;
                
            case ET_Daily:
                repeatStr = NSLocalizedString(@"Daily", @"Daily");
                break;
                
            case ET_Weekly:
                repeatStr = NSLocalizedString(@"Weekly", @"Weekly");
                break;
                
            case ET_Monthly:
                repeatStr = NSLocalizedString(@"Monthly", @"Monthly");
                break;
                
            case ET_Annually:
                repeatStr = NSLocalizedString(@"Annually", @"Annually");
                break;
        }
    }
    
    cell.titleLabel.text = [NSString stringWithFormat:@"%@  /  %@  /  %@", event.name, repeatStr, time];
    
    return cell;
}

#pragma mark - MAWeekViewDelegate's methods

- (void)weekView:(GRWeekView *)weekView eventTapped:(EventBase *)event
{
	self.eventController = [[[GRNewProtectedEventViewController alloc] init] autorelease];
    self.eventController.inputDelegate = self;
    self.eventController.allowEdit = YES;
    
    self.eventController.currentEvent = event;
    
    [self.eventController view].frame = self.parentController.view.bounds;
    [self.parentController.view addSubview:[self.eventController view]];
}

#pragma mark - MAWeekViewDataSource's methods

- (NSArray *)weekView:(GRWeekView *)weekView eventsForDate:(NSDate *)startDate
{
    NSCalendar *calendar = [AppDelegate getInstance].globalCalendar;
    
    NSDateComponents *components = [calendar components:DATE_COMPONENTS fromDate:startDate];
    
    NSInteger year = [components year];
    NSInteger month = [components month];
    NSInteger day = [components day];
    NSInteger weekDay = components.weekday;
    
    NSMutableArray *eventsArray = [NSMutableArray array];
    
    for (Event *event in self.events)
    {
        NSDateComponents *eventComponents = [calendar components:DATE_COMPONENTS fromDate:event.date];
        
        NSInteger yearEvent = [eventComponents year];
        NSInteger monthEvent = [eventComponents month];
        NSInteger dayEvent = [eventComponents day];
        NSInteger weekDayEvent = eventComponents.weekday;
        
        if ([event.type integerValue] == ET_Once)
        {
            if ((yearEvent == year) &&
                (month == monthEvent) &&
                (day == dayEvent))
            {
                [eventsArray addObject:event];
            }
        }
        else if ([event.type integerValue] == ET_Daily)
        {
            [eventsArray addObject:event];
        }
        else if ([event.type integerValue] == ET_Weekly)
        {
            if (weekDay == weekDayEvent)
            {
                [eventsArray addObject:event];
            }
        }
        else if ([event.type integerValue] == ET_Monthly)
        {
            if (day == dayEvent)
            {
                [eventsArray addObject:event];
            }
        }
        else if ([event.type integerValue] == ET_Annually)
        {
            if ((month == monthEvent) &&
                (day == dayEvent))
            {
                [eventsArray addObject:event];
            }
        }
    }
    
    return eventsArray;
}

#pragma mark - CalendarViewControllerDelegate's methods

- (void)calendarViewController:(CalendarViewController *)aCalendarViewController dateDidChange:(NSDate *)aDate
{
	NSLog(@"Date set to: %@", aDate);
}

- (void)calendarViewController:(CalendarViewController *)aCalendarViewController didSelectEvent:(Event*)event
{
    self.eventController = [[[GRNewProtectedEventViewController alloc] init] autorelease];
    self.eventController.inputDelegate = self;
    self.eventController.allowEdit = YES;
    self.eventController.currentEvent = event;
    
    [self.eventController view].frame = self.parentController.view.bounds;
    [self.parentController.view addSubview:[self.eventController view]];
}

- (NSArray*)eventsForDate:(NSDate*)date
{
    NSCalendar *calendar = [AppDelegate getInstance].globalCalendar;
    
    NSDateComponents *components = [calendar components:DATE_COMPONENTS fromDate:date];
    
    NSInteger year = [components year];
    NSInteger month = [components month];
    NSInteger day = [components day];
    NSInteger weekDay = components.weekday;
    
    NSMutableArray *eventsArray = [NSMutableArray array];
    
    for (Event *event in self.events)
    {
        NSDateComponents *eventComponents = [calendar components:DATE_COMPONENTS fromDate:event.date];
        
        NSInteger yearEvent = [eventComponents year];
        NSInteger monthEvent = [eventComponents month];
        NSInteger dayEvent = [eventComponents day];
        NSInteger weekDayEvent = eventComponents.weekday;
        
        if ([event.type integerValue] == ET_Once)
        {
            if ((yearEvent == year) &&
                (month == monthEvent) &&
                (day == dayEvent))
            {
                [eventsArray addObject:event];
            }
        }
        else if ([event.type integerValue] == ET_Daily)
        {
            [eventsArray addObject:event];
        }
        else if ([event.type integerValue] == ET_Weekly)
        {
            if (weekDay == weekDayEvent)
            {
                [eventsArray addObject:event];
            }
        }
        else if ([event.type integerValue] == ET_Monthly)
        {
            if (day == dayEvent)
            {
                [eventsArray addObject:event];
            }
        }
        else if ([event.type integerValue] == ET_Annually)
        {
            if ((month == monthEvent) &&
                (day == dayEvent))
            {
                [eventsArray addObject:event];
            }
        }
    }
    
    return eventsArray;
}

#pragma mark - GREventInputDelegate's methods

- (void)commonEventInputViewControllerDidFinish:(GRCommonEventInputViewController*)commonEventInputViewController
{
    if (IS_IPAD)
    {
        [commonEventInputViewController.view removeFromSuperview];
    }
    else
    {
        [commonEventInputViewController.navigationController popViewControllerAnimated:YES];
    }
}

@end
