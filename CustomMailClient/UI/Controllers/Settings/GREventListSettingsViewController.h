//
//  GREventListSettingsViewController.h
//  GrannyApp
//
//  Created by Eugene Pavlyuk on 8/8/12.
//  Copyright (c) 2012 Home. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "GRWeekView.h" 
#import "CalendarViewControllerDelegate.h"

@class CalendarView;
@class Settings;

@interface GREventListSettingsViewController : GAITrackedViewController <UITableViewDelegate, UITableViewDataSource, MAWeekViewDataSource, MAWeekViewDelegate, CalendarViewControllerDelegate>
{
    NSMutableArray *events;
    
    sortingType typeOfSorting;
    
    Settings *settings;
}

@property (nonatomic, assign) UIViewController *parentController;

@property (nonatomic, retain) IBOutlet UITableView *settingsTableView;
@property (nonatomic, retain) IBOutlet UIView *tableContentView;

@property (nonatomic, retain) IBOutlet GRWeekView *weekView;
@property (nonatomic, retain) IBOutlet UIView *weekContentView;

@property (nonatomic, retain) IBOutlet UIView *monthContentView;
@property (nonatomic, retain) IBOutlet CalendarView *calendarView;

@property (nonatomic, retain) IBOutlet UIButton *addButton;

@property (nonatomic, retain) IBOutlet UIButton *listButton;
@property (nonatomic, retain) IBOutlet UIButton *monthButton;
@property (nonatomic, retain) IBOutlet UIButton *weekButton;
@property (nonatomic, retain) IBOutlet UIButton *todayButton;

- (IBAction)sortButtonTapped:(id)sender;
- (IBAction)addButtonTapped;
- (IBAction)homeButtonTapped:(id)sender;
- (IBAction)backButtonTapped:(id)sender;
- (IBAction)todayButtonTapped:(id)sender;

@end
