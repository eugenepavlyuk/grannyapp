//
//  GREventSettingsViewController.m
//  GrannyApp
//
//  Created by Eugene Pavlyuk on 6/16/12.
//  Copyright (c) 2012 Home. All rights reserved.
//

#import "GREventSettingsViewController.h"
#import "Settings.h"
#import "DataManager.h"
#import "AlarmManager.h"
#import "STSegmentedControl.h"

@interface GREventSettingsViewController ()

@property (nonatomic, retain) Settings *settings;

@end

@implementation GREventSettingsViewController

@synthesize settingsTableView;

@synthesize weekTableViewCell;
@synthesize timeTableViewCell;
@synthesize mailTableViewCell;

@synthesize timeSegment;

@synthesize timeCustomSegment;

@synthesize settings;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    
    if (self)
    {
        // Custom initialization
    }
    
    return self;
}

- (BOOL)prefersStatusBarHidden
{
    return YES;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    
    self.screenName = @"Date Settings Screen";
    
    if (!self.settings)
    {
        self.settings = [[DataManager sharedInstance] getEntityWithName:kSettingsEntityName];        
    }
    
    NSArray *objects2 = [NSArray arrayWithObjects:@"12", @"24", nil];
	
	timeCustomSegment = [[STSegmentedControl alloc] initWithItems:objects2];
	timeCustomSegment.frame = CGRectMake(10, 20, 135, 40);
    timeCustomSegment.tag = 100;
	[timeCustomSegment addTarget:self action:@selector(valueChanged:) forControlEvents:UIControlEventValueChanged];
	timeCustomSegment.autoresizingMask = self.timeSegment.autoresizingMask;
    
    [self.snoozeSlider setThumbImage:[UIImage imageNamed:@"screensaver_slider_btn"] forState:UIControlStateNormal];
    [self.snoozeSlider setMinimumTrackImage:[[UIImage imageNamed:@"screensaver_slider_progress"] resizableImageWithCapInsets:UIEdgeInsetsMake(0, 10, 0, 10)] forState:UIControlStateNormal];
    [self.snoozeSlider setMaximumTrackImage:[[UIImage imageNamed:@"screensaver_slider_progress"] resizableImageWithCapInsets:UIEdgeInsetsMake(0, 10, 0, 10)] forState:UIControlStateNormal];
    
    self.snoozeTime.text = [NSString stringWithFormat:@"%d", [self.settings.snoozeDuration integerValue]];
    self.snoozeSlider.value = [self.settings.snoozeDuration integerValue];
}

- (IBAction)valueChanged:(UIControl*)sender
{
    if (sender == self.mondayButton)
    {
        self.mondayButton.selected = YES;
        self.sundayButton.selected = NO;
        
        settings.weedStartsFrom = @(0);
    }
    else if (sender == self.sundayButton)
    {
        self.mondayButton.selected = NO;
        self.sundayButton.selected = YES;
        
        settings.weedStartsFrom = @(1);
    }
    else
    {
        STSegmentedControl *control = (STSegmentedControl*)sender;
        
        if (control.tag == 100)
        {
            settings.timeFormat = [NSNumber numberWithInt:control.selectedSegmentIndex];
        }
    }
    
    [[NSNotificationCenter defaultCenter] postNotificationName:@"SettingsChangedNotification" object:nil];
    
    [[DataManager sharedInstance] save];
}

- (void)releaseViews
{
    self.mondayButton = nil;
    self.sundayButton = nil;
    self.timeCustomSegment = nil;
    
    self.timeSegment = nil;
    
    self.weekTableViewCell = nil;
    self.timeTableViewCell = nil;
    self.mailTableViewCell = nil;
    self.snoozeTableViewCell = nil;
    self.settingsTableView = nil;
    self.snoozeSlider = nil;
    self.snoozeTime = nil;
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
        
    [self releaseViews];
}

- (void)dealloc
{
    [self releaseViews];
    self.settings = nil;
    
    [super dealloc];
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return UIInterfaceOrientationIsLandscape(interfaceOrientation);
}

- (IBAction)backButtonTapped
{
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)segmentValueChanged:(id)sender
{
    UISegmentedControl *segment = (UISegmentedControl*)sender;
    
    if (segment.tag == 1)
    {
        settings.weedStartsFrom = [NSNumber numberWithInt:segment.selectedSegmentIndex];
    }
    else
    {
        settings.timeFormat = [NSNumber numberWithInt:segment.selectedSegmentIndex];
    }
    
    [[NSNotificationCenter defaultCenter] postNotificationName:@"SettingsChangedNotification" object:nil];
    
    [[DataManager sharedInstance] save];
}

- (IBAction)sliderValueChanged
{
    settings.snoozeDuration = @((int)(self.snoozeSlider.value));
    self.snoozeTime.text = [NSString stringWithFormat:@"%d min", (int)(self.snoozeSlider.value)];
    
    [[AlarmManager sharedInstance] updateAllEvents];
    
    [[DataManager sharedInstance] save];
}

#pragma mark - UITableViewDelegate's methods

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath 
{
    if (indexPath.section == 0)
    {
        if (indexPath.row == 0)
        {
            return 84.f;
        }
    }
    
    return 70;
}


#pragma mark - UITableViewDataSource's methods

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 2;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.section == 0)
    {        
        if (indexPath.row == 0)
        {
            if ([self.settings.weedStartsFrom intValue] == 0)
            {
                self.mondayButton.selected = YES;
                self.sundayButton.selected = NO;
            }
            else
            {
                self.mondayButton.selected = NO;
                self.sundayButton.selected = YES;
            }
            
            return self.weekTableViewCell;
        }
        else if (indexPath.row == 1)
        {
            return self.snoozeTableViewCell;
        }
        else
        {
            [self.timeCustomSegment removeFromSuperview];
            self.timeSegment.hidden = YES;
            self.timeCustomSegment.frame = self.timeSegment.frame;
            [self.timeTableViewCell addSubview:self.timeCustomSegment];

            
            self.timeCustomSegment.selectedSegmentIndex = [self.settings.timeFormat intValue];
//            self.timeSegment.selectedSegmentIndex = [self.settings.timeFormat intValue];
            
            return self.timeTableViewCell;
        }
    }
    else
    {
        return self.mailTableViewCell;
    }
    
    return nil;
}

#pragma mark - UIAlertViewDelegate's methods

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
//    if (buttonIndex == alertView.cancelButtonIndex)
//    {
//        GRContact *contact = [self.contacts objectAtIndex:alertView.tag];
//        
//        [self removeContactFromAddressBook:contact.record];
//        
//        [self.contacts removeObjectAtIndex:alertView.tag];
//    }
//    
//    [contactsTableView reloadData];
}


@end
