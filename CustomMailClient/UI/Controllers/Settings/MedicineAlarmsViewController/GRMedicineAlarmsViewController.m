//
//  MedicineAlarmsViewController.m
//  GrannyApp
//
//  Created by Eugene Pavluk on 9/24/13.
//
//

#import "GRMedicineAlarmsViewController.h"
#import "GRNewMedicineAlertViewController.h"
#import "ProtectedEventTableViewCell.h"
#import "MedicineAlarm.h"
#import "DataManager.h"
#import "AlarmManager.h"
#import "EventTableViewCell.h"
#import "NSString+Additions.h"
#import <QuartzCore/QuartzCore.h>
#import "CalendarView.h"
#import "CalendarMonth.h"
#import "Settings.h"
#import "GRHourView.h"
#import "GRHourCell.h"
#import "GRHeaderCell.h"
#import "GRWeekdayBarView.h"
#import "GridView.h"
#import "GREventView.h"
#import "GRDayHeader.h"
#import "GRDayCell.h"

@interface GRMedicineAlarmsViewController () <GREventInputDelegate,UITableViewDelegate, UITableViewDataSource, MAWeekViewDataSource, MAWeekViewDelegate, CalendarViewControllerDelegate>

@property (nonatomic, retain) GRNewMedicineAlertViewController *eventController;

@property (nonatomic, retain) NSMutableArray *alarms;

@property (nonatomic, assign) sortingType typeOfSorting;
@property (nonatomic, retain) Settings *settings;

@end

@implementation GRMedicineAlarmsViewController

@synthesize settingsTableView;
@synthesize parentController;
@synthesize eventController;
@synthesize alarms;

@synthesize weekView;
@synthesize tableContentView;
@synthesize weekContentView;
@synthesize calendarView;
@synthesize monthContentView;
@synthesize addButton;
@synthesize settings;

@synthesize typeOfSorting;

@synthesize listButton;
@synthesize weekButton;
@synthesize monthButton;

- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder:aDecoder];
    
    if (self)
    {
        typeOfSorting = ST_List;
        
        [[NSNotificationCenter defaultCenter] addObserver:self
                                                 selector:@selector(applicationDidBecomeActive:)
                                                     name:UIApplicationDidBecomeActiveNotification
                                                   object:nil];
    }
    
    return self;
}

- (void)applicationDidBecomeActive:(NSNotification*)note
{
    self.weekView.week = [NSDate date];
    [self.calendarView reloadData];
}

- (Settings*)settings
{
    if (!settings)
    {
        self.settings = [[DataManager sharedInstance] getEntityWithName:kSettingsEntityName];
    }
    
    return settings;
}

- (BOOL)prefersStatusBarHidden
{
    return YES;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    
    self.screenName = @"Medicine Alarm List in Settings Screen";
    
    [[GRHourView appearance] setYOffset:@(30)];
    [[GRHourView appearance] setTextFont:[UIFont fontWithName:@"Helvetica" size:14]];
    [[GRHourCell appearance] setBorderColor:[UIColor clearColor]];
    [[GRHeaderCell appearance] setTitleFont:[UIFont fontWithName:@"Helvetica" size:14]];
    
    [[GREventView appearance] setLockImage:[UIImage imageNamed:@"alarm_settings_lock_icon"]];
    [[GREventView appearance] setEventImage:[UIImage imageNamed:@"alarm_settings_alarm_icon"]];
    
    [[GRDayHeader appearance] setTitleFont:[UIFont fontWithName:@"Helvetica" size:18]];
    
//    [[GRDayCell appearance] setPreviousMonthDayColor:RGBCOLOR(177, 177, 177)];
//    [[GRDayCell appearance] setCurrentMonthDayColor:RGBCOLOR(102, 102, 102)];
//    [[GRDayCell appearance] setTodayDayColor:RGBCOLOR(127, 127, 127)];
    [[GRDayCell appearance] setTodayDayTitleColor:[UIColor blackColor]];
//    [[GRDayCell appearance] setEventBackgroundColor:RGBCOLOR(226, 230, 254)];
//    [[GRDayCell appearance] setEventTitleColor:RGBCOLOR(96, 96, 96)];
//    [[GRDayCell appearance] setDayFont:[UIFont fontWithName:@"Helvetica-Bold" size:21]];
//    [[GRDayCell appearance] setLockImage:[UIImage imageNamed:@"settings_calendar_lock_icon"]];
    
//    self.weekView.weekdayBarView.todayBackgroundColor = RGBCOLOR(226, 231, 254);
//    self.weekView.gridView.todayBackgroundColor = RGBCOLOR(226, 231, 254);
    self.weekView.colorForEvent = RGBCOLOR(226, 231, 254);
    self.weekView.weekdayBarView.todayColor = RGBCOLOR(47, 77, 250);
    self.listButton.selected = YES;
    
    self.weekView.settings = self.settings;
    self.weekView.week = [NSDate date];
    
    self.calendarView.calendarViewControllerDelegate = self;
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(updateListNotification:)
                                                 name:@"UpdateEventsListNotification"
                                               object:nil];
    
    self.alarms = [NSMutableArray array];
    [self updateListNotification:nil];
}

- (void)updateListNotification:(NSNotification*)notification
{
    [self.alarms removeAllObjects];
    
    NSMutableArray *allEvents = [NSMutableArray arrayWithArray:[[DataManager sharedInstance] allRowsForEntity:kMedicineAlarmEntityName]];
    
    [self.alarms addObjectsFromArray:allEvents];
    
    [self.settingsTableView reloadData];
    [self.weekView reloadData];
    [self.calendarView reloadData];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(addButtonTapped:)
                                                 name:@"AddButtonTappedNotification"
                                               object:nil];
    
    if (typeOfSorting == ST_List)
    {
        [self.settingsTableView reloadData];
    }
    else if (typeOfSorting == ST_MONTLY)
    {
        [self.calendarView.calendarView refresh];
        [self.calendarView reloadData];
    }
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:@"AddButtonTappedNotification"
                                                  object:nil];
}

- (void)addButtonTapped:(NSNotification*)notification
{
    if (IS_IPAD)
    {
        self.eventController = [[[GRNewMedicineAlertViewController alloc] init] autorelease];
    }
    else
    {
        self.eventController = [[[GRNewMedicineAlertViewController alloc] init] autorelease];
    }
    
    self.eventController.inputDelegate = self;

    self.eventController.allowEdit = YES;
    [eventController view].frame = self.parentController.view.bounds;
    [self.parentController.view addSubview:[eventController view]];
}

- (IBAction)homeButtonTapped:(id)sender
{
    [self.navigationController popToRootViewControllerAnimated:YES];
}

- (IBAction)backButtonTapped:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)addButtonTapped
{
    self.eventController = [[[GRNewMedicineAlertViewController alloc] init] autorelease];
    self.eventController.inputDelegate = self;
    self.eventController.allowEdit = YES;
    
    [self.navigationController pushViewController:self.eventController animated:YES];
}

- (void)releaseViews
{
    self.settingsTableView = nil;
    self.addButton = nil;
    self.tableContentView = nil;
    self.weekContentView = nil;
    self.weekView = nil;
    self.weekButton = nil;
    self.monthButton = nil;
    self.listButton = nil;
    self.calendarView = nil;
    self.monthContentView = nil;
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    
    [self releaseViews];
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    //    return YES;
    
    return UIInterfaceOrientationIsLandscape(interfaceOrientation);
}

- (void)dealloc
{
    [self releaseViews];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self];
    self.alarms = nil;
    
    self.eventController = nil;
    self.settings = nil;
    
    [super dealloc];
}

- (void)setTypeOfSorting:(sortingType)nTypeOfSorting
{
    typeOfSorting = nTypeOfSorting;
    
    if (typeOfSorting == ST_List)
    {
        self.tableContentView.hidden = NO;
        self.weekContentView.hidden = YES;
        self.monthContentView.hidden = YES;
    }
    else if (typeOfSorting == ST_WEEKLY)
    {
        self.tableContentView.hidden = YES;
        self.weekContentView.hidden = NO;
        [weekView scrollTo7Hours];
        self.monthContentView.hidden = YES;
    }
    else
    {
        self.monthContentView.hidden = NO;
        self.weekContentView.hidden = YES;
        self.tableContentView.hidden = YES;
        [self.calendarView setup];
    }
}

- (IBAction)sortButtonTapped:(id)sender
{
    UIButton *button = (UIButton*)sender;
    
    if (button == self.listButton)
    {
        self.listButton.selected = YES;
        self.weekButton.selected = NO;
    }
    else
    {
        self.listButton.selected = NO;
        self.weekButton.selected = YES;
    }
    
    self.typeOfSorting = button.tag;
    
    [self updateListNotification:nil];
}

#pragma mark - UITableViewDelegate's methods

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (IS_IPAD)
    {
        return 85.f;
    }
    
    return 44.f;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    MedicineAlarm *alarm = [self.alarms objectAtIndex:indexPath.section];
    
    if (IS_IPAD)
    {
        self.eventController = [[[GRNewMedicineAlertViewController alloc] init] autorelease];
    }
    else
    {
        self.eventController = [[[GRNewMedicineAlertViewController alloc] init] autorelease];
    }
    
    self.eventController.currentEvent = alarm;
    self.eventController.allowEdit = YES;
    self.eventController.inputDelegate = self;
    
    if (IS_IPAD)
    {
        [eventController view].frame = self.parentController.view.bounds;
        [self.parentController.view addSubview:[eventController view]];
    }
    else
    {
        [self.navigationController pushViewController:self.eventController animated:YES];
    }
}

#pragma mark - UITableViewDataSource methods

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return [self.alarms count];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 1;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    ProtectedEventTableViewCell *cell = (ProtectedEventTableViewCell*)[tableView dequeueReusableCellWithIdentifier:@"ProtectedEventTableViewCellIdentifier"];
    
    if (!cell)
    {
        if (IS_IPAD)
        {
            cell = [[[NSBundle mainBundle] loadNibNamed:@"ProtectedEventTableViewCell" owner:nil options:nil] lastObject];
        }
        else
        {
            cell = [[[NSBundle mainBundle] loadNibNamed:@"EventTableViewCell_iphone" owner:nil options:nil] lastObject];
        }
    }
    
    UIImageView *imageView = [[[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 9, 24)] autorelease];
    imageView.image = [UIImage imageNamed:@"event_arrow"];
    cell.accessoryView = imageView;
    
    MedicineAlarm *event = [self.alarms objectAtIndex:indexPath.section];
    
    cell.protectedIcon.hidden = ![event.persistent boolValue];
    
    if (!cell.protectedIcon.hidden)
    {
        cell.timeLabel.font = [UIFont fontWithName:@"Helvetica-Oblique" size:18];
    }
    else
    {
        cell.timeLabel.font = [UIFont fontWithName:@"Helvetica-BoldOblique" size:18];
    }
    
    cell.titleLabel.frame = CGRectMake(100, cell.titleLabel.frame.origin.y, tableView.frame.size.width, cell.titleLabel.frame.size.height);
    
    cell.alarmIcon.hidden = NO;
    
    if ([event.medicineType intValue] == 1)
    {
        cell.alarmIcon.image = [UIImage imageNamed:@"pill_icon_h"];
        cell.alarmIcon.contentMode = UIViewContentModeCenter;
    }
    else if ([event.medicineType intValue] == 2)
    {
        cell.alarmIcon.image = [UIImage imageNamed:@"bottle_icon_h"];
        cell.alarmIcon.contentMode = UIViewContentModeCenter;
    }
    else if ([event.medicineType intValue] == 3)
    {
        cell.alarmIcon.image = [UIImage imageNamed:@"medicine_chest_icon_h"];
        cell.alarmIcon.contentMode = UIViewContentModeCenter;
    }
    else if ([event.medicineType intValue] == 4)
    {
        NSString *iconName = [[[NSString stringWithFormat:@"%f", [event.eventId floatValue]] stringByReplacingOccurrencesOfString:@"." withString:@"_"]stringByAppendingPathExtension:@"png"];
        
        NSArray *paths = NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES);
        NSString *dirPath = [paths objectAtIndex:0];
        
        dirPath = [dirPath stringByAppendingPathComponent:iconName];
        
        cell.alarmIcon.image = [UIImage imageWithContentsOfFile:dirPath];
        cell.alarmIcon.contentMode = UIViewContentModeScaleToFill;
    }
    
    NSString *time = @"";
    
    NSDateFormatter *dateFormatter = [[[NSDateFormatter alloc] init] autorelease];
    
    if ([settings.timeFormat intValue])
    {
        [dateFormatter setDateFormat:@"HH:mm"];
    }
    else
    {
        [dateFormatter setDateFormat:@"hh:mm a"];
    }
    
    time = [dateFormatter stringFromDate:event.date];
    
    //    switch ([event.type intValue])
    //    {
    //        case 0:
    //        {
    //            dateTime = [NSString stringWithFormat:@"Daily at %@", time];
    //        }; break;
    //
    //        case 1:
    //        {
    //            NSDateFormatter *df = [[[NSDateFormatter alloc] init] autorelease];
    //
    //            [df setDateFormat:@"EEEE"];
    //
    //            NSString *day = [df stringFromDate:event.date];
    //
    //            dateTime = [NSString stringWithFormat:@"every %@ at %@", day, time];
    //
    //        };    break;
    //
    //        case 2:
    //        {
    //            NSDateFormatter *df = [[[NSDateFormatter alloc] init] autorelease];
    //
    //            [df setDateFormat:@"d"];
    //
    //            NSInteger dayInt = [[df stringFromDate:event.date] intValue];
    //
    //            NSString *day = [NSString ordinalNumberFormat:[NSNumber numberWithInt:dayInt]];
    //
    //            dateTime = [NSString stringWithFormat:@"on the %@ of every month, at %@", day, time];
    //
    //        }; break;
    //
    //        case 4:
    //        {
    //            NSDateFormatter *df = [[[NSDateFormatter alloc] init] autorelease];
    //
    //            [df setDateFormat:@"dd, MMM yyyy, "];
    //
    //            dateTime = [df stringFromDate:event.date];
    //
    //            dateTime = [NSString stringWithFormat:@"On %@%@", dateTime, time];
    //        }; break;
    //
    //        case 3:
    //        {
    //            NSDateFormatter *df = [[[NSDateFormatter alloc] init] autorelease];
    //
    //            [df setDateFormat:@"dd, MMMM, yyyy"];
    //
    //            NSString *date = [df stringFromDate:event.date];
    //
    //            dateTime = [NSString stringWithFormat:@"on %@ at %@", date, time];
    //        };    break;
    //    }
    //
    //    cell.titleLabel.text = [NSString stringWithFormat:@"%@ %@", event.name, dateTime];
    
    NSString *repeatStr = nil;
    
    if (event.type)
    {
        switch ([event.type intValue])
        {
            case ET_Daily:
                repeatStr = NSLocalizedString(@"Daily", @"Daily");
                break;
                
            case ET_None:
                repeatStr = NSLocalizedString(@"Never", @"Never");
                break;
                
            default:
            {
                NSMutableArray *weekdays = event.weekDays;
                
                NSMutableString *weekdaysString = [NSMutableString string];
                
                for (NSNumber *weekday in weekdays)
                {
                    switch ([weekday intValue])
                    {
                        case MT_Monday:
                            [weekdaysString appendString:@"Monday"];
                            break;
                            
                        case MT_Tuesday:
                            [weekdaysString appendString:@"Tuesday"];
                            break;
                            
                        case MT_Wednesday:
                            [weekdaysString appendString:@"Wednesday"];
                            break;
                            
                        case MT_Thursday:
                            [weekdaysString appendString:@"Thursday"];
                            break;
                            
                        case MT_Friday:
                            [weekdaysString appendString:@"Friday"];
                            break;
                            
                        case MT_Saturday:
                            [weekdaysString appendString:@"Saturday"];
                            break;
                            
                        case MT_Sunday:
                            [weekdaysString appendString:@"Sunday"];
                            break;
                            
                        default:
                            break;
                    }
                    
                    if (weekday != [weekdays lastObject])
                    {
                        [weekdaysString appendString:@", "];
                    }
                }
                
                repeatStr = weekdaysString;
                
            }    break;
        }
    }
    
    cell.titleLabel.textColor = RGBCOLOR(96, 96, 96);
    cell.titleLabel.text = [NSString stringWithFormat:@"%@   /   %@   /   %@", time, repeatStr, event.name];
    
    return cell;
}

#pragma mark - MAWeekViewDelegate's methods

- (void)weekView:(GRWeekView *)weekView eventTapped:(EventBase *)event
{
	if (IS_IPAD)
    {
        self.eventController = [[[GRNewMedicineAlertViewController alloc] init] autorelease];
    }
    else
    {
        self.eventController = [[[GRNewMedicineAlertViewController alloc] init] autorelease];
    }
    
    self.eventController.currentEvent = (MedicineAlarm*)event;
    self.eventController.allowEdit = YES;
    self.eventController.inputDelegate = self;
    
    if (IS_IPAD)
    {
        [eventController view].frame = self.parentController.view.bounds;
        [self.parentController.view addSubview:[eventController view]];
    }
    else
    {
        [self.navigationController pushViewController:self.eventController animated:YES];
    }
}

#pragma mark - MAWeekViewDataSource's methods

- (NSArray *)weekView:(GRWeekView *)weekView eventsForDate:(NSDate *)startDate
{
    static NSCalendar *calendar = nil;
    
    if (!calendar)
    {
        calendar = [[NSCalendar alloc] initWithCalendarIdentifier:NSGregorianCalendar];
    }
    
    NSDateComponents *components = [calendar components:DATE_COMPONENTS fromDate:startDate];
    NSInteger weekDay = [components weekday] == 1 ? 7 : [components weekday] - 1;
    
    NSMutableArray *eventsArray = [NSMutableArray array];
    
    for (MedicineAlarm *event in self.alarms)
    {
        if ([event.type integerValue] == ET_Daily)
        {
            [eventsArray addObject:event];
        }
        else if ([event.type integerValue] == ET_Weekly)
        {
            if ([event.weekDays containsObject:@(weekDay)])
            {
                [eventsArray addObject:event];
            }
        }
    }
    
    return eventsArray;
}

#pragma mark - CalendarViewControllerDelegate's methods

- (void)calendarViewController:(CalendarViewController *)aCalendarViewController dateDidChange:(NSDate *)aDate
{
	NSLog(@"Date set to: %@", aDate);
}

- (void)calendarViewController:(CalendarViewController *)aCalendarViewController didSelectEvent:(Event*)event
{
    self.eventController = [[[GRNewMedicineAlertViewController alloc] init] autorelease];
    
    self.eventController.currentEvent = (MedicineAlarm*)event;
    self.eventController.allowEdit = YES;
    self.eventController.inputDelegate = self;
    
    if (IS_IPAD)
    {
        [eventController view].frame = self.parentController.view.bounds;
        [self.parentController.view addSubview:[eventController view]];
    }
    else
    {
        [self.navigationController pushViewController:self.eventController animated:YES];
    }
}

- (NSArray*)eventsForDate:(NSDate*)date
{
    NSCalendar *calendar = [AppDelegate getInstance].globalCalendar;
    
    NSDateComponents *components = [calendar components:DATE_COMPONENTS fromDate:date];
    
    NSInteger year = [components year];
    NSInteger month = [components month];
    NSInteger day = [components day];
    NSInteger weekDay = components.weekday;
    
    NSMutableArray *eventsArray = [NSMutableArray array];
    
    for (MedicineAlarm *event in self.alarms)
    {
        NSDateComponents *eventComponents = [calendar components:DATE_COMPONENTS fromDate:event.date];
        
        NSInteger yearEvent = [eventComponents year];
        NSInteger monthEvent = [eventComponents month];
        NSInteger dayEvent = [eventComponents day];
        NSInteger weekDayEvent = eventComponents.weekday;
        
        if ([event.type integerValue] == ET_Once)
        {
            if ((yearEvent == year) &&
                (month == monthEvent) &&
                (day == dayEvent))
            {
                [eventsArray addObject:event];
            }
        }
        else if ([event.type integerValue] == ET_Daily)
        {
            [eventsArray addObject:event];
        }
        else if ([event.type integerValue] == ET_Weekly)
        {
            if (weekDay == weekDayEvent)
            {
                [eventsArray addObject:event];
            }
        }
        else if ([event.type integerValue] == ET_Monthly)
        {
            if (day == dayEvent)
            {
                [eventsArray addObject:event];
            }
        }
        else if ([event.type integerValue] == ET_Annually)
        {
            if ((month == monthEvent) &&
                (day == dayEvent))
            {
                [eventsArray addObject:event];
            }
        }
    }
    
    return eventsArray;
}

#pragma mark - GREventInputDelegate

- (void)commonEventInputViewControllerDidFinish:(GRCommonEventInputViewController*)commonEventInputViewController
{
    if (IS_IPAD)
    {
        [commonEventInputViewController.view removeFromSuperview];
    }
    else
    {
        [self.eventController.navigationController popViewControllerAnimated:YES];
    }
    
    self.eventController = nil;
}

@end
