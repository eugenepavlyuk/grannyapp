//
//  MedicineAlarmsViewController.h
//  GrannyApp
//
//  Created by Eugene Pavluk on 9/24/13.
//
//

#import "GAITrackedViewController.h"
#import "GRWeekView.h"
#import "CalendarViewControllerDelegate.h"

@class CalendarView;
@class Settings;


@interface GRMedicineAlarmsViewController : GAITrackedViewController 
{
    NSMutableArray *alarms;
    
    sortingType typeOfSorting;
    
    Settings *settings;
}

@property (nonatomic, assign) UIViewController *parentController;

@property (nonatomic, retain) IBOutlet UIView *tableContentView;
@property (nonatomic, retain) IBOutlet UITableView *settingsTableView;

@property (nonatomic, retain) IBOutlet UIView *weekContentView;
@property (nonatomic, retain) IBOutlet GRWeekView *weekView;

@property (nonatomic, retain) IBOutlet UIView *monthContentView;
@property (nonatomic, retain) IBOutlet CalendarView *calendarView;

@property (nonatomic, retain) IBOutlet UIButton *addButton;

@property (nonatomic, retain) IBOutlet UIButton *listButton;
@property (nonatomic, retain) IBOutlet UIButton *monthButton;
@property (nonatomic, retain) IBOutlet UIButton *weekButton;

- (IBAction)sortButtonTapped:(id)sender;

@end
