//
//  GRContactAlertViewController.h
//  GrannyApp
//
//  Created by Eugene Pavluk on 12/14/13.
//
//

#import <UIKit/UIKit.h>

@interface GRAlertViewController : UIViewController

@property (nonatomic, retain) IBOutlet UILabel *messageLabel;
@property (nonatomic, retain) IBOutlet UIButton *okButton;

@end
