//
//  GRAlarmViewController.h
//  Granny
//
//  Created by Eugene Pavlyuk on 21.03.12.
//  Copyright __MyCompanyName__ 2012. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AlarmManagerDelegate.h"

@interface GRAlarmViewController : GAITrackedViewController <AlarmManagerDelegate>
{		

}

@property (nonatomic, retain) IBOutlet UIView *watchContainer;
@property (nonatomic, retain) IBOutlet UIButton *plusButton;
@property (nonatomic, retain) IBOutlet UIButton *showAlarmsButton;

@property (nonatomic, retain) IBOutlet UIView *alarmslistPopover;
@property (nonatomic, retain) IBOutlet UIView *addAlarmPopover;

@property (nonatomic, retain) IBOutlet UIImageView *hourArrow;
@property (nonatomic, retain) IBOutlet UIImageView *minuteArrow;
@property (nonatomic, retain) IBOutlet UIImageView *secondArrow;

@property (nonatomic, retain) IBOutlet UIImageView *headerImageView;

- (IBAction)backButtonTapped;
- (IBAction)setAlarmButtonTapped;
- (IBAction)showEvents;
- (IBAction)showWeekButtonTapped;
- (IBAction)showMonthButtonTapped;

- (IBAction)showAlarmsListPopoverButtonTapped;
- (IBAction)addAlarmPopoverButtonTapped;
- (IBAction)cancelPopoverButtonTapped;

@end

