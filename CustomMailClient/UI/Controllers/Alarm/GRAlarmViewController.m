//
//  GRAlarmViewController.m
//  Granny
//
//  Created by Eugene Pavlyuk on 21.03.12.
//  Copyright __MyCompanyName__ 2012. All rights reserved.
//


#import "GRAlarmViewController.h"
#import "GRAlarmsListViewController.h"
#import "GRSettingsViewController.h"
#import "AlarmManager.h"
#import "GRNewAlarmViewController.h"
#import "GREventInputDelegate.h"

@interface GRAlarmViewController() <GREventInputDelegate>

@property (nonatomic, retain) GRNewAlarmViewController *eventNewController;

@end


@implementation GRAlarmViewController

@synthesize watchContainer;
@synthesize plusButton;
@synthesize showAlarmsButton;

@synthesize hourArrow;
@synthesize minuteArrow;

@synthesize headerImageView;

- (BOOL)prefersStatusBarHidden
{
    return YES;
}

- (IBAction)setAlarmButtonTapped
{
    [UIView animateWithDuration:0.3f animations:^{
        self.addAlarmPopover.alpha = 0.f;
    }];
    
    self.plusButton.selected = NO;
    
    self.eventNewController = [[[GRNewAlarmViewController alloc] init] autorelease];
    
    self.eventNewController.inputDelegate = self;
    
    self.eventNewController.view.frame = self.view.bounds;
    [self.view addSubview:self.eventNewController.view];
}

- (IBAction)showEvents
{
    self.showAlarmsButton.selected = NO;
    
    [UIView animateWithDuration:0.3f animations:^{
       self.alarmslistPopover.alpha = 0.f; 
    }];
    
    GRAlarmsListViewController *eventsListViewController = [[[GRAlarmsListViewController alloc] init] autorelease];
    
    [self.navigationController pushViewController:eventsListViewController animated:YES];
}

- (IBAction)showWeekButtonTapped
{
    self.showAlarmsButton.selected = NO;
    
    [UIView animateWithDuration:0.3f animations:^{
        self.alarmslistPopover.alpha = 0;
    }];
    
    GRAlarmsListViewController *eventsListViewController = [[[GRAlarmsListViewController alloc] init] autorelease];
    
    [self.navigationController pushViewController:eventsListViewController animated:YES];
    
    [eventsListViewController setTypeOfSorting:ST_WEEKLY];
}

- (IBAction)showMonthButtonTapped
{
    self.showAlarmsButton.selected = NO;
    
    [UIView animateWithDuration:0.3f animations:^{
        self.alarmslistPopover.alpha = 0;
    }];
    
    GRAlarmsListViewController *eventsListViewController = [[[GRAlarmsListViewController alloc] init] autorelease];
    
    [self.navigationController pushViewController:eventsListViewController animated:YES];
    
    [eventsListViewController setTypeOfSorting:ST_MONTLY];
}

#pragma mark - life cycle

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    [self adjustToInterfaceOrientation:[[UIApplication sharedApplication] statusBarOrientation]];
    
    AlarmManager *alarmManager = [AlarmManager sharedInstance];
    
    [alarmManager addWatcher:self];
    
    [self timeChangedInAlarmManager:alarmManager];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    
    AlarmManager *alarmManager = [AlarmManager sharedInstance];
    
    [alarmManager removeWatcher:self];
}


- (void)viewDidLoad 
{
	[super viewDidLoad];

    self.screenName = @"Alarm Screen";
    
    self.watchContainer.frame = CGRectMake((int)((self.view.frame.size.width - self.watchContainer.frame.size.width)/2), self.headerImageView.frame.size.height + self.headerImageView.frame.origin.y + (int)((self.view.frame.size.height - self.watchContainer.frame.size.height - self.headerImageView.frame.size.height - self.headerImageView.frame.origin.y)/2), self.watchContainer.frame.size.width, self.watchContainer.frame.size.height);
    
    self.headerImageView.image = [[UIImage imageNamed:@"alarm_header_background"] resizableImageWithCapInsets:UIEdgeInsetsMake(0, 10, 0, 10)];
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation 
{
    if (!IS_IPAD)
    {
        return YES;
    }
    
    return UIInterfaceOrientationIsLandscape(interfaceOrientation);
}

- (void)releaseViews
{
    self.watchContainer = nil;
    self.plusButton = nil;
    self.showAlarmsButton = nil;
    
    self.hourArrow = nil;
    self.minuteArrow = nil;
    
    self.headerImageView = nil;
    self.alarmslistPopover = nil;
    self.addAlarmPopover = nil;
}

- (void)viewDidUnload 
{
    [super viewDidUnload];
    	
	[self releaseViews];
}

- (IBAction)showAlarmsListPopoverButtonTapped
{
    self.showAlarmsButton.selected = YES;
    
    [UIView animateWithDuration:0.3f animations:^{
        self.alarmslistPopover.alpha = 1.f;
    }];
}

- (IBAction)addAlarmPopoverButtonTapped
{
    self.plusButton.selected = YES;
    
    [UIView animateWithDuration:0.3f animations:^{
        self.addAlarmPopover.alpha = 1.f;
    }];
}

- (IBAction)cancelPopoverButtonTapped
{
    self.showAlarmsButton.selected = NO;
    self.plusButton.selected = NO;
    
    [UIView animateWithDuration:0.3f animations:^{
        self.alarmslistPopover.alpha = 0.f;
    }];
    
    [UIView animateWithDuration:0.3f animations:^{
        self.addAlarmPopover.alpha = 0.f;
    }];
}

- (IBAction)backButtonTapped
{
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)dealloc 
{
    [self releaseViews];
    self.eventNewController = nil;
    
    [super dealloc];
}

- (void)willRotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)duration
{
    [self adjustToInterfaceOrientation:toInterfaceOrientation];
}

- (void)didRotateFromInterfaceOrientation:(UIInterfaceOrientation)fromInterfaceOrientation
{
    //[self adjustToInterfaceOrientation:[[UIApplication sharedApplication] statusBarOrientation]];
}

- (void)adjustToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation 
{
    if (!IS_IPAD)
    {

    }
}

#pragma mark - AlarmManagerDelegate's methods

- (void)timeChangedInAlarmManager:(AlarmManager*)alarmManager
{
    NSDateFormatter *hTimeFormat = [[[NSDateFormatter alloc] init] autorelease];
	[hTimeFormat setDateFormat:@"HH"];
	NSDateFormatter *mTimeFormat = [[[NSDateFormatter alloc] init] autorelease];
	[mTimeFormat setDateFormat:@"mm"];
    NSDateFormatter *sTimeFormat = [[[NSDateFormatter alloc] init] autorelease];
	[sTimeFormat setDateFormat:@"ss"];
    
    NSDate *now = [NSDate date];
   	
	NSString *hTime = [hTimeFormat stringFromDate:now];
	NSString *mTime = [mTimeFormat stringFromDate:now];
    NSString *sTime = [sTimeFormat stringFromDate:now];
    
    int hours = [hTime intValue];
    
    if (hours > 11)
    {
        hours -= 12;
    }
    
    int sec = [sTime intValue];
    int min = [mTime intValue];
    
    float minuteAngel = (min * 6 * M_PI) / 180 + (sec * 6 * M_PI) / (180 * 60);
    float hourAngel = (hours * 30 * M_PI) / 180 + (sec + min * 60 * 30 * M_PI) / (180 * 3600);
    
    [UIView beginAnimations:@"rotate" context:nil];
    self.minuteArrow.transform = CGAffineTransformMakeRotation(minuteAngel);
    self.hourArrow.transform = CGAffineTransformMakeRotation(hourAngel);
    [UIView commitAnimations];
}

#pragma mark - GREventInputDelegate's methods

- (void)commonEventInputViewControllerDidFinish:(GRCommonEventInputViewController*)commonEventInputViewController
{
    if (IS_IPAD)
    {
        [commonEventInputViewController.view removeFromSuperview];
    }
    else
    {
        [commonEventInputViewController.navigationController popViewControllerAnimated:YES];
    }
    
    self.eventNewController = nil;
}

@end
