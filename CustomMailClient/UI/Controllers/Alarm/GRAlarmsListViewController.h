//
//  GRAlarmsListViewController
//  Granny
//
//  Created by Eugene Pavlyuk on 4/2/12.
//  Copyright (c) 2012 Home. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "GRWeekView.h" 
#import "CalendarViewControllerDelegate.h"

@class CalendarView;
@class Settings;

@interface GRAlarmsListViewController : GAITrackedViewController <UITableViewDelegate, UITableViewDataSource, MAWeekViewDataSource, MAWeekViewDelegate>
{
    NSMutableArray *events;
    
    sortingType typeOfSorting;
    
    Settings *settings;
}

@property (nonatomic, retain) IBOutlet UITableView *eventsTableView;
@property (nonatomic, retain) IBOutlet UIView *tableContentView;

@property (nonatomic, retain) IBOutlet GRWeekView *weekView;
@property (nonatomic, retain) IBOutlet UIView *weekContentView;

@property (nonatomic, retain) IBOutlet UIView *monthContentView;
@property (nonatomic, retain) IBOutlet CalendarView *calendarView;

@property (nonatomic, retain) IBOutlet UIButton *addButton;

@property (nonatomic, retain) IBOutlet UIImageView *footerImageView;
@property (nonatomic, retain) IBOutlet UIImageView *headerImageView;

@property (nonatomic, retain) IBOutlet UIButton *listButton;
@property (nonatomic, retain) IBOutlet UIButton *monthButton;
@property (nonatomic, retain) IBOutlet UIButton *weekButton;

@property (nonatomic, retain) IBOutlet UILabel *titleLabel;

@property (nonatomic, assign) sortingType typeOfSorting;

- (IBAction)backButtonTapped;
- (IBAction)homeButtonTapped;
- (IBAction)sortButtonTapped:(id)sender;
- (IBAction)addNewEventButtonTapped:(id)sender;

@end
