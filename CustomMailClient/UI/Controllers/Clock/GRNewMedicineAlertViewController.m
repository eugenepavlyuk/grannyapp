//
//  GRNewMedicineAlertViewController.m
//  GrannyApp
//
//  Created by Eugene Pavluk on 9/25/13.
//
//

#import "GRNewMedicineAlertViewController.h"
#import "DataManager.h"
#import "MedicineAlarm.h"
#import "Settings.h"
#import "GRDatePickerView.h"
#import "GRTimePickerView.h"
#import "BaseEventTableViewCell.h"
#import "GRCommonAlertView.h"
#import "BDGridCell.h"
#import "GRPhotoPreview.h"
#import "RecurrenceTableViewCell.h"
#import "WYPopoverController.h"
#import "NSDate+Calendar.h"
#import "Contants.h"

@interface GRNewMedicineAlertViewController () <AlertViewDelegate, WYPopoverControllerDelegate>

@property (nonatomic, retain) MedicineAlarm *currentEvent;

@end

@implementation GRNewMedicineAlertViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    
    if (self)
    {
        self.allowEdit = NO;
    }
    
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    
    self.screenName = @"Medicine Alarm Details Screen";
    
    self.monthDayPickerView.bShowYears = NO;
    self.photoContentView.minimumPadding = 30;
    
    self.hourMinutePickerView.componentWidth = 100.f;
    
    [self.hourMinutePickerView reset];
    
    if (self.currentEvent)
    {
        if (self.currentEvent.weekDays)
        {
            [eventInfo setObject:[NSMutableArray arrayWithArray:self.currentEvent.weekDays] forKey:kWeekdaysKey];
        }
        else
        {
            [eventInfo setObject:[NSMutableArray array] forKey:kWeekdaysKey];
        }
        
        if ([self.currentEvent.type integerValue] == ET_Weekly)
        {
            [eventInfo setObject:@(ET_Weekly) forKey:kEventTypeKey];
            [self setTypeOfEvent:@(-2)];
        }
        else
        {
            [self setTypeOfEvent:self.currentEvent.type];
        }
        
        [self setNameOfEvent:self.currentEvent.name];
        [self setIconOfEvent:self.currentEvent.eventId];
        [self setMedicineTypeOfEvent:self.currentEvent.medicineType];
        
        [eventInfo setObject:self.currentEvent.hour forKey:kHourTimeKey];
        [eventInfo setObject:self.currentEvent.minute forKey:kMinuteTimeKey];
                
        [eventInfo setObject:@(NO) forKey:kHasSnoozeTimeKey];
        [eventInfo setObject:@(NO) forKey:kHasNotificationKey];
        
        [eventInfo setObject:self.currentEvent.eventId forKey:kEventIdKey];
    }
    else
    {
        [self setTypeOfEvent:[NSNumber numberWithInt:ET_Daily]];
        [self setMedicineTypeOfEvent:@(1)];
        
        NSNumber *hour   = @([[NSDate date] hour]);
        NSNumber *minute = @([[NSDate date] minute]);
        
        [eventInfo setObject:hour forKey:kHourTimeKey];
        [eventInfo setObject:minute forKey:kMinuteTimeKey];
        
        [eventInfo setObject:@(NO) forKey:kHasSnoozeTimeKey];
        [eventInfo setObject:@(NO) forKey:kHasNotificationKey];
    }
    
    self.deleteButton.hidden = YES;
    
    if (self.currentEvent)
    {
        self.titleTableViewCell.userInteractionEnabled = NO;
        self.photoTableViewCell.userInteractionEnabled = NO;
        self.startDateTableViewCell.userInteractionEnabled = NO;
        self.repeatTableViewCell.userInteractionEnabled = NO;
        self.browseButton.userInteractionEnabled = NO;
        
        self.pillButton.userInteractionEnabled = NO;
        self.bottleButton.userInteractionEnabled = NO;
        self.medicineChestButton.userInteractionEnabled = NO;
    }
    else
    {
        self.editButton.hidden = YES;
        self.titleTableViewCell.userInteractionEnabled = YES;
        self.photoTableViewCell.userInteractionEnabled = YES;
        self.startDateTableViewCell.userInteractionEnabled = YES;
        self.repeatTableViewCell.userInteractionEnabled = YES;
        self.browseButton.userInteractionEnabled = YES;
        
        self.pillButton.userInteractionEnabled = YES;
        self.bottleButton.userInteractionEnabled = YES;
        self.medicineChestButton.userInteractionEnabled = YES;
    }
    
    self.titleTableViewCell.backgroundImageView.image = nil;
    self.startDateTableViewCell.backgroundImageView.image = nil;
    self.repeatTableViewCell.backgroundImageView.image = nil;
    self.photoTableViewCell.backgroundImageView.image = nil;
    
    self.photoTableViewCell.separatorImageView.hidden = YES;
    
    self.dateStartLabel.text = [self setBeginDate];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
}

- (void)releaseViews
{
    self.hourMinutePickerView = nil;
    self.monthDayPickerView = nil;
    
    self.pillButton = nil;
    self.bottleButton = nil;
    self.medicineChestButton = nil;
    self.browseButton = nil;
}

- (void)dealloc
{
    [super dealloc];
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
	return UIInterfaceOrientationIsLandscape(interfaceOrientation);
}

- (void)edit
{
    self.titleTableViewCell.userInteractionEnabled = YES;
    self.photoTableViewCell.userInteractionEnabled = YES;
    self.startDateTableViewCell.userInteractionEnabled = YES;
    self.repeatTableViewCell.userInteractionEnabled = YES;
    self.browseButton.userInteractionEnabled = YES;
    self.pillButton.userInteractionEnabled = YES;
    self.bottleButton.userInteractionEnabled = YES;
    self.medicineChestButton.userInteractionEnabled = YES;
    
    self.deleteButton.hidden = NO;
    self.editButton.hidden = YES;
    
    self.xButton.selected = NO;
}

- (IBAction)editButtonTapped
{
    if (self.currentEvent)
    {
        if (self.allowEdit)
        {
            [self edit];
        }
        else
        {
            InfoAlertView *alertView = [[[InfoAlertView alloc] initWithTitle:NSLocalizedString(@"\nThis event is protected", @"\nThis event is protected")
                                                                     message:nil
                                                                    delegate:self
                                                           cancelButtonTitle:nil
                                                           otherButtonTitles:NSLocalizedString(@"OK", @"OK"), nil] autorelease];
            
            [alertView show];
        }
        
        return ;
    }
    else
    {
        [self edit];
    }
    
    [super editButtonTapped];
}

- (IBAction)deleteButtonTapped
{
    if (self.currentEvent)
    {
        if (self.allowEdit)
        {
            [self deleteEvent];
            
            [self.inputDelegate commonEventInputViewControllerDidFinish:self];
        }
        else
        {
            InfoAlertView *alertView = [[[InfoAlertView alloc] initWithTitle:NSLocalizedString(@"\nThis event is protected", @"\nThis event is protected")
                                                                     message:nil
                                                                    delegate:self
                                                           cancelButtonTitle:nil
                                                           otherButtonTitles:NSLocalizedString(@"OK", @"OK"), nil] autorelease];
            
            [alertView show];
        }
        
        return ;
    }
    
    [self.inputDelegate commonEventInputViewControllerDidFinish:self];
    
    [super deleteButtonTapped];
}

- (IBAction)xButtonTapped
{
    if (self.datePopover.superview)
    {
        return ;
    }
    
    if (self.addPhotoPopover.superview)
    {
        return ;
    }
    
    if (self.recurrencePopover.superview)
    {
        return ;
    }
    
    [self.datePopover removeFromSuperview];
    [self.addPhotoPopover removeFromSuperview];
    [self.recurrencePopover removeFromSuperview];
    
    [super xButtonTapped];
}

- (IBAction)browseButtonTapped
{
    if (self.datePopover.superview)
    {
        return ;
    }
    
    if (self.addPhotoPopover.superview)
    {
        return ;
    }
    
    if (self.recurrencePopover.superview)
    {
        return ;
    }
    
    [self.currentTextField resignFirstResponder];
    self.currentTextField = nil;
    
    [self.datePopover removeFromSuperview];
    [self.addPhotoPopover removeFromSuperview];
    [self.recurrencePopover removeFromSuperview];
    
    [self photoButtonTapped];
}

- (IBAction)medicineButtonTapped:(UIButton*)sender
{
    [self setMedicineTypeOfEvent:@(sender.tag)];
}

- (void)photoButtonTapped
{
    if (self.datePopover.superview)
    {
        return ;
    }
    
    if (self.addPhotoPopover.superview)
    {
        return ;
    }
    
    if (self.recurrencePopover.superview)
    {
        return ;
    }
    
//    if ([self.currentEvent.persistent boolValue])
//    {
        if (!self.allowEdit)
        {
            InfoAlertView *alertView = [[[InfoAlertView alloc] initWithTitle:NSLocalizedString(@"\nThis event is protected", @"\nThis event is protected")
                                                                     message:nil
                                                                    delegate:self
                                                           cancelButtonTitle:nil
                                                           otherButtonTitles:NSLocalizedString(@"OK", @"OK"), nil] autorelease];
            
            [alertView show];
            
            return ;
        }
//    }
    
    somethingChanged = YES;
    
    if (IS_IPAD)
    {
        self.addPhotoPopover.frame = CGRectMake(0, 0, self.addPhotoPopover.frame.size.width, self.addPhotoPopover.frame.size.height);
        
        [self.view addSubview:self.addPhotoPopover];
    }
    else
    {
        self.addPhotoPopover.frame = CGRectMake(0, 0, self.addPhotoPopover.frame.size.width, self.addPhotoPopover.frame.size.height);
        
        [self.infoContainer addSubview:self.addPhotoPopover];
    }
    
    [self.currentTextField resignFirstResponder];
    [self.datePopover removeFromSuperview];
    [self.recurrencePopover removeFromSuperview];
    
    self.xButton.selected = NO;
}

- (IBAction)okDateButtonTapped
{
    NSInteger HH;
    NSInteger mm;
    
    HH = [self.hourMinutePickerView getHour];
    mm = [self.hourMinutePickerView getMinute];
    
    [eventInfo setObject:[NSNumber numberWithInt:HH] forKey:kHourTimeKey];
    [eventInfo setObject:[NSNumber numberWithInt:mm] forKey:kMinuteTimeKey];
    
    self.dateStartLabel.text = [self setBeginDate];
    
    [self.datePopover removeFromSuperview];
}

- (IBAction)reccurenceButtonTapped:(UIButton*)button
{
    [self setTypeOfEvent:[NSNumber numberWithInt:button.tag]];
}

- (void)continueSaving
{
    [self saveEvent];
}

- (void)deleteEvent
{
    if (image)
    {
        NSString *iconName = [[[NSString stringWithFormat:@"%f", [self.currentEvent.eventId floatValue]] stringByReplacingOccurrencesOfString:@"." withString:@"_"] stringByAppendingPathExtension:@"png"];
        
        NSArray *paths = NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES);
        NSString *dirPath = [paths objectAtIndex:0];
        
        dirPath = [dirPath stringByAppendingPathComponent:iconName];
        
        NSURL *url = [NSURL URLWithString:dirPath];
        
        if ([[NSFileManager defaultManager] removeItemAtURL:url error:nil])
        {
            NSLog(@"image was removed");
        }
    }
    
    [[UIApplication sharedApplication] cancelLocalNotificationsForEventId:self.currentEvent.eventId];
    
    [self.currentEvent removeEvent];
    
    [[DataManager sharedInstance] removeEntityModel:self.currentEvent];
    
    [[NSNotificationCenter defaultCenter] postNotificationName:@"UpdateEventsListNotification" object:nil];
}

- (void)save
{
    MedicineAlarm *event = nil;
    
    if (self.currentEvent)
    {
        event = self.currentEvent;
    }
    else
    {
        event = [[DataManager sharedInstance] createEntityWithName:kMedicineAlarmEntityName];
        event.persistent = @(NO);
    }
    
    event.eventId = [NSNumber numberWithDouble:[[NSDate date] timeIntervalSince1970]];
    
    NSDate *startDate = [NSDate dateWithYear:[[NSDate date] year]
                                       month:[[NSDate date] month]
                                         day:[[NSDate date] day]
                                        hour:[[eventInfo objectForKey:kHourTimeKey] integerValue]
                                      minute:[[eventInfo objectForKey:kMinuteTimeKey] integerValue]
                                      second:0];
    
    event.date = startDate;
    
    event.hour   = [eventInfo objectForKey:kHourTimeKey];
    event.minute = [eventInfo objectForKey:kMinuteTimeKey];
    
    event.hasNotification = [eventInfo objectForKey:kHasNotificationKey];
    event.hasSnoozeTime = @(YES);
    event.medicineType = [eventInfo objectForKey:kMedicineTypeKey];
    
    event.snoozeDate = nil;
    
    event.name = [eventInfo objectForKey:kNameKey];
    event.eventId = [NSNumber numberWithDouble:[[NSDate date] timeIntervalSince1970]];
    
    NSNumber *eventType = [eventInfo objectForKey:kEventTypeKey];
    
    event.type = eventType;
    event.weekDays = [eventInfo objectForKey:kWeekdaysKey];
    
    if (image)
    {
        NSString *iconName = [[[NSString stringWithFormat:@"%f", [event.eventId floatValue]] stringByReplacingOccurrencesOfString:@"." withString:@"_"] stringByAppendingPathExtension:@"png"];
        
        NSArray *paths = NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES);
        NSString *dirPath = [paths objectAtIndex:0];
        
        dirPath = [dirPath stringByAppendingPathComponent:iconName];
        
        //NSURL *url = [NSURL URLWithString:dirPath];
        
        NSData *data = UIImagePNGRepresentation(image);
        
        [[NSFileManager defaultManager] createFileAtPath:dirPath contents:data attributes:nil];
    }
    else
    {
        NSString *iconName = [[[NSString stringWithFormat:@"%f", [event.eventId floatValue]] stringByReplacingOccurrencesOfString:@"." withString:@"_"] stringByAppendingPathExtension:@"png"];
        
        NSArray *paths = NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES);
        NSString *dirPath = [paths objectAtIndex:0];
        
        dirPath = [dirPath stringByAppendingPathComponent:iconName];
        
        [[NSFileManager defaultManager] removeItemAtPath:dirPath error:nil];
    }
    
    event.expired = [NSNumber numberWithBool:NO];
    
    [[DataManager sharedInstance] save];
    
    [event scheduleLocalNotifications];
    
    [[NSNotificationCenter defaultCenter] postNotificationName:@"UpdateEventsListNotification" object:nil];
    
    [self.inputDelegate commonEventInputViewControllerDidFinish:self];
}

- (void)saveEvent
{
    [self save];
}

- (IBAction)saveButtonTapped
{
    [super saveButtonTapped];
    
    [self.datePopover removeFromSuperview];
    [self.addPhotoPopover removeFromSuperview];
    [self.recurrencePopover removeFromSuperview];
    
    if (![self.titleTextField.text length])
    {
        [eventInfo setObject:@"" forKey:kNameKey];
    }
    else
    {
        [eventInfo setObject:self.titleTextField.text forKey:kNameKey];
    }
    
    [self continueSaving];
}

- (void)beginButtonTapped
{
    if (self.datePopover.superview)
    {
        return ;
    }
    
    if (self.addPhotoPopover.superview)
    {
        return ;
    }
    
    if (self.recurrencePopover.superview)
    {
        return ;
    }
    
    somethingChanged = YES;
    
    self.datePopover.tag = startDatePopoverTag;
    
    [self.addPhotoPopover removeFromSuperview];
    [self.currentTextField resignFirstResponder];
    [self.recurrencePopover removeFromSuperview];
    
    self.xButton.selected = NO;
    
    if (!IS_IPAD)
    {
        [self.infoContainer addSubview:self.datePopover];
    }
    else
    {
        [self.view addSubview:self.datePopover];
    }
    
    if (IS_IPAD)
    {
        self.datePopover.center = CGPointMake(self.view.frame.size.width / 2, self.view.frame.size.height / 2);
    }
    else
    {
        self.datePopover.frame = CGRectMake(0, 0, self.datePopover.frame.size.width, self.datePopover.frame.size.height);
    }
    
    NSInteger hh = [[eventInfo objectForKey:kHourTimeKey] intValue];
    NSInteger mm = [[eventInfo objectForKey:kMinuteTimeKey] intValue];
    
    [self.hourMinutePickerView setHour:hh];
    [self.hourMinutePickerView setMinute:mm];
    
    [self.monthDayPickerView selectToday];
    [self.hourMinutePickerView selectToday];
}

- (void)recurrenceButtonTapped
{
    if (self.datePopover.superview)
    {
        return ;
    }
    
    if (self.addPhotoPopover.superview)
    {
        return ;
    }
    
    if (self.recurrencePopover.superview)
    {
        return ;
    }
    
    somethingChanged = YES;
    
    [self.datePopover removeFromSuperview];
    [self.addPhotoPopover removeFromSuperview];
    [self.currentTextField resignFirstResponder];
    
    if (IS_IPAD)
    {
        [self.view addSubview:self.recurrencePopover];
    }
    else
    {
        [self.infoContainer addSubview:self.recurrencePopover];
    }
    
    self.xButton.selected = NO;
    
    if (IS_IPAD)
    {

        
    }
    else
    {
        self.recurrencePopover.frame = CGRectMake(self.view.bounds.size.width - self.recurrencePopover.bounds.size.width,
                                                  self.view.bounds.size.height - self.recurrencePopover.bounds.size.height,
                                                  self.recurrencePopover.frame.size.width,
                                                  self.recurrencePopover.frame.size.height);
    }
}

- (void)setTypeOfEvent:(NSNumber*)type
{
    switch ([type intValue])
    {
        case ET_Daily:
            [eventInfo setObject:type forKey:kEventTypeKey];
            [eventInfo setObject:[NSMutableArray arrayWithObjects:@(MT_Monday), @(MT_Tuesday), @(MT_Wednesday), @(MT_Thursday), @(MT_Friday), @(MT_Saturday), @(MT_Sunday), nil] forKey:kWeekdaysKey];
            self.recurrenceLabel.text = NSLocalizedString(@"Daily", @"Daily");
            break;
            
        case ET_None:
            [eventInfo setObject:type forKey:kEventTypeKey];
            [eventInfo setObject:[NSMutableArray array] forKey:kWeekdaysKey];
            self.recurrenceLabel.text = NSLocalizedString(@"Never", @"Never");
            break;
            
        case -2:
            
            break;
            
        default:
        {
            NSMutableArray *weekdays = [eventInfo objectForKey:kWeekdaysKey];
            
            if ([type intValue] > 0)
            {
                if ([weekdays containsObject:type])
                {
                    [weekdays removeObject:type];
                }
                else
                {
                    [weekdays addObject:type];
                }
            }
            
            if ([weekdays count] == DAYS_IN_WEEK)
            {
                [weekdays removeAllObjects];
                [eventInfo setObject:@(ET_Daily) forKey:kEventTypeKey];
                type = @(ET_Daily);
            }
            else if ([weekdays count] == 0)
            {
                [eventInfo setObject:@(ET_None) forKey:kEventTypeKey];
                type = @(ET_None);
            }
            else
            {
                [eventInfo setObject:@(ET_Weekly) forKey:kEventTypeKey];
            }
        
            [weekdays sortUsingSelector:@selector(compare:)];
        }
        break;
    }
    
    switch ([type intValue])
    {
        case ET_Daily:
            self.recurrenceLabel.text = NSLocalizedString(@"Daily", @"Daily");
            break;
            
        case ET_None:
            self.recurrenceLabel.text = NSLocalizedString(@"Never", @"Never");
            break;
            
        default:
        {
            NSMutableArray *weekdays = [eventInfo objectForKey:kWeekdaysKey];
            
            NSMutableString *weekdaysString = [NSMutableString string];
            
            for (NSNumber *weekday in weekdays)
            {
                switch ([weekday intValue])
                {
                    case MT_Monday:
                        [weekdaysString appendString:@"Monday"];
                        break;
                        
                    case MT_Tuesday:
                        [weekdaysString appendString:@"Tuesday"];
                        break;
                        
                    case MT_Wednesday:
                        [weekdaysString appendString:@"Wednesday"];
                        break;
                        
                    case MT_Thursday:
                        [weekdaysString appendString:@"Thursday"];
                        break;
                        
                    case MT_Friday:
                        [weekdaysString appendString:@"Friday"];
                        break;
                        
                    case MT_Saturday:
                        [weekdaysString appendString:@"Saturday"];
                        break;
                        
                    case MT_Sunday:
                        [weekdaysString appendString:@"Sunday"];
                        break;
                        
                    default:
                        break;
                }
                
                if (weekday != [weekdays lastObject])
                {
                    [weekdaysString appendString:@", "];
                }
            }
            
            self.recurrenceLabel.text = weekdaysString;
        }
        break;
    }
    
    [self.recurrenceTableView reloadRowsAtIndexPaths:self.recurrenceTableView.indexPathsForVisibleRows withRowAnimation:UITableViewRowAnimationNone];
}

- (void)setIconOfEvent:(NSNumber*)eventId
{
    NSString *iconName = [[[NSString stringWithFormat:@"%f", [eventId floatValue]] stringByReplacingOccurrencesOfString:@"." withString:@"_"]stringByAppendingPathExtension:@"png"];
    
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES);
    NSString *dirPath = [paths objectAtIndex:0];
    
    dirPath = [dirPath stringByAppendingPathComponent:iconName];
    
    //NSURL *url = [NSURL URLWithString:dirPath];
    
    [image release];
    image = nil;
    
    image = [[UIImage imageWithContentsOfFile:dirPath] retain];//[[UIImage imageWithData:data] retain];
    
    if (image)
    {
        [icon removeFromSuperview];
        
        CGRect iconRect;
        
        if (IS_IPAD)
        {
            iconRect = CGRectMake(740, 10, 60, 60);
        }
        else
        {
            iconRect = CGRectMake(self.view.bounds.size.width - 60, 100, 40 , 40);
        }
        
        icon = [[[GRPhotoPreview alloc] initWithImage:image frame:iconRect] autorelease];
        icon.autoresizingMask = UIViewAutoresizingFlexibleLeftMargin | UIViewAutoresizingFlexibleBottomMargin | UIViewAutoresizingFlexibleTopMargin;
        icon.imageView.contentMode = UIViewContentModeScaleAspectFill;
        
        icon.layer.cornerRadius = 8.f;
        icon.imageView.layer.cornerRadius = 8.f;
        icon.layer.shadowOffset = CGSizeZero;
        
        UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
        button.frame = icon.bounds;
        [button addTarget:self action:@selector(browseButtonTapped) forControlEvents:UIControlEventTouchUpInside];
        [icon addSubview:button];
        
        if (IS_IPAD)
        {
            [self.photoTableViewCell.contentView addSubview:icon];
        }
        else
        {
            [self.infoContainer addSubview:icon];
        }
        
        self.browseButton.hidden = YES;
    }
    else
    {
        self.browseButton.hidden = NO;
    }
}

- (void)setMedicineTypeOfEvent:(NSNumber*)medicineType
{
    switch ([medicineType intValue]) {
        case 1:
            self.bottleButton.selected = NO;
            self.medicineChestButton.selected = NO;
            self.pillButton.selected = YES;
            break;
            
        case 2:
            self.bottleButton.selected = YES;
            self.medicineChestButton.selected = NO;
            self.pillButton.selected = NO;
            break;
            
        case 3:
            self.bottleButton.selected = NO;
            self.medicineChestButton.selected = YES;
            self.pillButton.selected = NO;
            break;
            
        case 4:
            self.bottleButton.selected = NO;
            self.medicineChestButton.selected = NO;
            self.pillButton.selected = NO;
            break;
            
        default:
            break;
    }
    
    [eventInfo setObject:medicineType forKey:@"medicineType"];
}

- (NSString*)setBeginDate
{
//    NSString *day = [[eventInfo objectForKey:kDayKey] stringValue];
//    NSString *year = [[eventInfo objectForKey:kYearKey] stringValue];
//    NSString *month = @"";
    NSInteger hour = [[eventInfo objectForKey:kHourTimeKey] intValue];
    NSInteger minute = [[eventInfo objectForKey:kMinuteTimeKey] intValue];
    
//    NSInteger theYear = [year intValue];
    
//    NSDateFormatter *dateFormatter = [[NSDateFormatter new] autorelease];
//    NSArray *months = [dateFormatter standaloneMonthSymbols];
//    
//    NSInteger index = [[eventInfo objectForKey:kMonthKey] intValue] - 1;
//    
//    month = [months objectAtIndex:index];
    
    NSString *time = @"";
    
    //    if (![settings.timeFormat intValue])
    //    {
    //        if (hour >= 12)
    //        {
    //            time = @"PM";
    //        }
    //        else
    //        {
    //            time = @"AM";
    //        }
    //
    //        if (hour > 12)
    //        {
    //            hour -= 12;
    //        }
    //
    //        if (hour == 0)
    //        {
    //            hour = 12;
    //        }
    //    }
    
    return [NSString stringWithFormat:@"%02i:%02i %@", hour, minute, time];
}

#pragma mark - UITextFieldDelegate's methods

- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField
{
    if (self.datePopover.superview)
    {
        return NO;
    }
    
    if (self.addPhotoPopover.superview)
    {
        return NO;
    }
    
    if (self.recurrencePopover.superview)
    {
        return NO;
    }
    
    self.xButton.selected = NO;
    
    [self.datePopover removeFromSuperview];
    [self.addPhotoPopover removeFromSuperview];
    [self.recurrencePopover removeFromSuperview];
    
    self.currentTextField = textField;
    
    return YES;
}

#pragma mark - BDGridViewDataSource's methods

- (CGSize)gridViewSizeOfCell:(BDGridView *)gridView
{
    if (IS_IPAD)
    {
        CGFloat width = 100;
        
        return CGSizeMake(width, width);
    }
    else
    {
        CGFloat width = (gridView.bounds.size.width - 3 * gridView.minimumPadding)/ 4;
        
        return CGSizeMake(width, width);
    }
    
    return CGSizeZero;
}

#pragma mark - BDGridViewDelegate's methods

- (void)gridView:(BDGridView *)gridView didTapCell:(BDGridCell *)cell
{
    ALAsset *asset = [assets objectAtIndex:cell.index];
    
    if (image)
    {
        [image release];
        image = nil;
    }
    
    image = [UIImage imageWithCGImage:[[asset defaultRepresentation] fullScreenImage]];
    
    [image retain];
    
    [icon removeFromSuperview];
    
    CGRect iconRect;
    
    if (IS_IPAD)
    {
        iconRect = CGRectMake(740, 10, 60, 60);
    }
    else
    {
        iconRect = CGRectMake(self.view.bounds.size.width - 60, 10, 40 , 40);
    }
    
    icon = [[[GRPhotoPreview alloc] initWithImage:image frame:iconRect] autorelease];
    icon.autoresizingMask = UIViewAutoresizingFlexibleLeftMargin  | UIViewAutoresizingFlexibleBottomMargin | UIViewAutoresizingFlexibleTopMargin;
    icon.imageView.contentMode = UIViewContentModeScaleAspectFill;
    
    icon.layer.cornerRadius = 8.f;
    icon.imageView.layer.cornerRadius = 8.f;
    icon.layer.shadowOffset = CGSizeZero;
    
    UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
    button.frame = icon.bounds;
    [button addTarget:self action:@selector(browseButtonTapped) forControlEvents:UIControlEventTouchUpInside];
    [icon addSubview:button];
    
    if (IS_IPAD)
    {
        [self.photoTableViewCell.contentView addSubview:icon];
    }
    else
    {
        [self.infoContainer addSubview:icon];
    }
    
    [self.addPhotoPopover removeFromSuperview];
    
    [self setMedicineTypeOfEvent:@(4)];
    
    self.browseButton.hidden = YES;
}

#pragma mark - UITableViewDataSource's methods

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (tableView == self.recurrenceTableView)
    {
        return 9;
    }
    
    return 4;
}

- (void)selectCell:(RecurrenceTableViewCell*)recurrenceTableViewCell
{
    recurrenceTableViewCell.checkmarkImageView.hidden = NO;
    recurrenceTableViewCell.backgroundView = [[UIImageView alloc] initWithImage:[[UIImage imageNamed:@"settings_calendar_selected_cell"] resizableImageWithCapInsets:UIEdgeInsetsMake(10, 10, 10, 10)]];
    recurrenceTableViewCell.titleLabel.textColor = RGBCOLOR(7, 11, 35);
    recurrenceTableViewCell.titleLabel.font = [UIFont fontWithName:@"Helvetica-Bold" size:18];
}

- (void)deselectCell:(RecurrenceTableViewCell*)recurrenceTableViewCell
{
    recurrenceTableViewCell.checkmarkImageView.hidden = YES;
    recurrenceTableViewCell.backgroundView = nil;
    recurrenceTableViewCell.titleLabel.textColor = RGBCOLOR(96, 96, 96);
    recurrenceTableViewCell.titleLabel.font = [UIFont fontWithName:@"Helvetica" size:18];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (tableView == self.recurrenceTableView)
    {
        RecurrenceTableViewCell *recurrenceTableViewCell = (RecurrenceTableViewCell*)[tableView dequeueReusableCellWithIdentifier:@"RecurrenceTableViewCellIdentifier"];
        
        recurrenceTableViewCell.checkmarkImageView.image = [UIImage imageNamed:@"settings_calendar_checkmark_icon"];
        
        switch (indexPath.row) {
            case 0:
                recurrenceTableViewCell.titleLabel.text = NSLocalizedString(@"Never", @"Never");
                break;
                
            case 1:
                recurrenceTableViewCell.titleLabel.text = NSLocalizedString(@"Daily", @"Daily");
                break;
                
            case 2:
                recurrenceTableViewCell.titleLabel.text = NSLocalizedString(@"Monday", @"Monday");
                break;
                
            case 3:
                recurrenceTableViewCell.titleLabel.text = NSLocalizedString(@"Tuesday", @"Tuesday");
                break;
                
            case 4:
                recurrenceTableViewCell.titleLabel.text = NSLocalizedString(@"Wednesday", @"Wednesday");
                break;
                
            case 5:
                recurrenceTableViewCell.titleLabel.text = NSLocalizedString(@"Thursday", @"Thursday");
                break;
                
            case 6:
                recurrenceTableViewCell.titleLabel.text = NSLocalizedString(@"Friday", @"Friday");
                break;
                
            case 7:
                recurrenceTableViewCell.titleLabel.text = NSLocalizedString(@"Saturday", @"Saturday");
                break;
                
            case 8:
                recurrenceTableViewCell.titleLabel.text = NSLocalizedString(@"Sunday", @"Sunday");
                break;
                
            default:
                break;
        }
        
        NSInteger type = [[eventInfo objectForKey:kEventTypeKey] integerValue];
        
        [self deselectCell:recurrenceTableViewCell];
        
        switch (type)
        {
            case ET_Daily:
            {
                if (indexPath.row == 1)
                {
                    [self selectCell:recurrenceTableViewCell];
                }
            } break ;

            case ET_None:
            {
                if (indexPath.row == 0)
                {
                    [self selectCell:recurrenceTableViewCell];
                }

            } break ;
                
            default:
            {
                NSMutableArray *weekdays = [eventInfo objectForKey:kWeekdaysKey];

                for (NSNumber *weekday in weekdays)
                {
                    switch ([weekday integerValue])
                    {
                        case MT_Monday:
                            if (indexPath.row == 2)
                            {
                                [self selectCell:recurrenceTableViewCell];
                            }
                            break;

                        case MT_Tuesday:
                            if (indexPath.row == 3)
                            {
                                [self selectCell:recurrenceTableViewCell];
                            }

                            break;

                        case MT_Wednesday:
                            if (indexPath.row == 4)
                            {
                                [self selectCell:recurrenceTableViewCell];
                            }

                            break;

                        case MT_Thursday:
                            if (indexPath.row == 5)
                            {
                                [self selectCell:recurrenceTableViewCell];
                            }

                            break;

                        case MT_Friday:
                            if (indexPath.row == 6)
                            {
                                [self selectCell:recurrenceTableViewCell];
                            }

                            break;

                        case MT_Saturday:
                            if (indexPath.row == 7)
                            {
                                [self selectCell:recurrenceTableViewCell];
                            }

                            break;
                            
                        case MT_Sunday:
                            if (indexPath.row == 8)
                            {
                                [self selectCell:recurrenceTableViewCell];
                            }

                            break;
                            
                        default:
                            break;
                    }
                }
                
                
            }   break;
        }
        
        return recurrenceTableViewCell;
    }
    
    switch (indexPath.row)
    {
        case 0:
            return self.startDateTableViewCell;
            
        case 1:
            return self.titleTableViewCell;
            
        case 2:
            return self.repeatTableViewCell;
            
        case 3:
            return self.photoTableViewCell;
            
        default:
            break;
    }
    
    return nil;
}

#pragma mark - UITableViewDelegate's methods

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    if (tableView == self.recurrenceTableView)
    {
        if (indexPath.row == 0)
        {
            [self setTypeOfEvent:@(ET_None)];
        }
        else if (indexPath.row == 1)
        {
            [self setTypeOfEvent:@(ET_Daily)];
        }
        else
        {
            [self setTypeOfEvent:@(indexPath.row - 1)];
        }
        
        return;
    }
    
    switch (indexPath.row)
    {
        case 0:
            [self beginButtonTapped];
            break;
            
        case 2:
            [self recurrenceButtonTapped];
            break;
            
        default:
            break;
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (tableView == self.recurrenceTableView)
    {
        return 75;
    }
    
    return 90.f;
}

@end
