//
//  GRNewProtectedAlarmViewController.m
//  GrannyApp
//
//  Created by Eugene Pavlyuk on 7/16/12.
//  Copyright (c) 2012 Home. All rights reserved.
//

#import "GRNewProtectedAlarmViewController.h"
#import "DataManager.h"
#import "Alarm.h"
#import "Settings.h"
#import "GRDatePickerView.h"
#import "GRTimePickerView.h"
#import "STSegmentedControl.h"
#import "BaseEventTableViewCell.h"
#import "GRCommonAlertView.h"
#import "BDGridCell.h"
#import "GRPhotoPreview.h"
#import "WYPopoverController.h"
#import "GRAlertViewController.h"
#import "RecurrenceTableViewCell.h"
#import "NSDate+Calendar.h"

@interface GRNewProtectedAlarmViewController () <AlertViewDelegate, WYPopoverControllerDelegate>

@property (nonatomic, retain) Alarm *currentEvent;

@end

@implementation GRNewProtectedAlarmViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    
    if (self) 
    {
        self.allowEdit = NO;
    }
    
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    
    self.screenName = @"Event/Alarm Details Screen";
    
    self.photoContentView.minimumPadding = 30;
    
    self.hourMinutePickerView.componentWidth = 100.f;
    
    [self.hourMinutePickerView reset];
    
    if (self.currentEvent)
    {
        [self setTypeOfEvent:self.currentEvent.type];
        [self setNameOfEvent:self.currentEvent.name];
        [self setIconOfEvent:self.currentEvent.eventId];
        
        [eventInfo setObject:self.currentEvent.month forKey:kMonthKey];
        [eventInfo setObject:self.currentEvent.day forKey:kDayKey];
        [eventInfo setObject:self.currentEvent.year forKey:kYearKey];
        [eventInfo setObject:self.currentEvent.hour forKey:kHourTimeKey];
        [eventInfo setObject:self.currentEvent.minute forKey:kMinuteTimeKey];
                
        if ([self.currentEvent.hasSnoozeTime boolValue])
        {
            [eventInfo setObject:@(YES) forKey:kHasSnoozeTimeKey];
            self.onButton.selected = YES;
            self.offButton.selected = NO;
        }
        else
        {
            [eventInfo setObject:@(NO) forKey:kHasSnoozeTimeKey];
            self.onButton.selected = NO;
            self.offButton.selected = YES;
        }
        
        [eventInfo setObject:@(NO) forKey:kHasNotificationKey];
        
        [eventInfo setObject:self.currentEvent.eventId forKey:kEventIdKey];
    }
    else
    {
        [self setTypeOfEvent:[NSNumber numberWithInt:ET_Once]];
        
        NSNumber *year   = @([[NSDate date] year]);
        NSNumber *month  = @([[NSDate date] month]);
        NSNumber *day    = @([[NSDate date] day]);
        NSNumber *hour   = @([[NSDate date] hour]);
        NSNumber *minute = @([[NSDate date] minute]);
        
        [eventInfo setObject:year forKey:kYearKey];
        [eventInfo setObject:month forKey:kMonthKey];
        [eventInfo setObject:day forKey:kDayKey];
        [eventInfo setObject:hour forKey:kHourTimeKey];
        [eventInfo setObject:minute forKey:kMinuteTimeKey];
        
        [eventInfo setObject:@(NO) forKey:kHasSnoozeTimeKey];
        [eventInfo setObject:@(NO) forKey:kHasNotificationKey];
        
        self.onButton.selected = NO;
        self.offButton.selected = YES;
    }
  
    self.deleteButton.hidden = YES;
    
    if (self.currentEvent)
    {
        self.titleTableViewCell.userInteractionEnabled = NO;
        self.photoTableViewCell.userInteractionEnabled = NO;
        self.startDateTableViewCell.userInteractionEnabled = NO;
        self.protectTableViewCell.userInteractionEnabled = NO;
        self.snoozeTableViewCell.userInteractionEnabled = NO;
        self.repeatTableViewCell.userInteractionEnabled = NO;
    }
    else
    {
        self.editButton.hidden = YES;
        self.titleTableViewCell.userInteractionEnabled = YES;
        self.photoTableViewCell.userInteractionEnabled = YES;
        self.startDateTableViewCell.userInteractionEnabled = YES;
        self.protectTableViewCell.userInteractionEnabled = YES;
        self.snoozeTableViewCell.userInteractionEnabled = YES;
        self.repeatTableViewCell.userInteractionEnabled = YES;
    }
    
    self.photoTableViewCell.separatorImageView.hidden = YES;
    
    self.titleTableViewCell.backgroundImageView.image = nil;
    self.photoTableViewCell.backgroundImageView.image = nil;
    self.startDateTableViewCell.backgroundImageView.image = nil;
    self.repeatTableViewCell.backgroundImageView.image = nil;
    self.snoozeTableViewCell.backgroundImageView.image = nil;
    
    self.titleTableViewCell.separatorOffset = CGRectMake(0, 0, 0, 2);
    self.startDateTableViewCell.separatorOffset = CGRectMake(0, 0, 0, 2);
    self.protectTableViewCell.separatorOffset = CGRectMake(0, 0, 0, 2);
    self.repeatTableViewCell.separatorOffset = CGRectMake(0, 0, 0, 2);
    self.snoozeTableViewCell.separatorOffset = CGRectMake(0, 0, 0, 2);
    
    self.dateStartLabel.text = [self setBeginDate];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];

}

- (void)releaseViews
{
    [super releaseViews];
    
    self.offButton = nil;
    self.onButton = nil;
    
    self.protectTableViewCell = nil;
    self.snoozeTableViewCell = nil;
}

- (void)dealloc
{
    [super dealloc];
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
	return UIInterfaceOrientationIsLandscape(interfaceOrientation);
}

- (void)edit
{
    self.titleTableViewCell.userInteractionEnabled = YES;
    self.photoTableViewCell.userInteractionEnabled = YES;
    self.startDateTableViewCell.userInteractionEnabled = YES;
    self.protectTableViewCell.userInteractionEnabled = YES;
    self.snoozeTableViewCell.userInteractionEnabled = YES;
    self.repeatTableViewCell.userInteractionEnabled = YES;
    
    self.deleteButton.hidden = NO;
    self.editButton.hidden = YES;
    
    self.xButton.selected = NO;
}

- (IBAction)editButtonTapped
{
    if (self.currentEvent)
    {
        if ([self.currentEvent.persistent boolValue])
        {
            if (self.allowEdit)
            {
                [self edit];
            }
            else
            {
                InfoAlertView *alertView = [[[InfoAlertView alloc] initWithTitle:NSLocalizedString(@"\nThis event is protected", @"\nThis event is protected")
                                                                         message:nil
                                                                        delegate:self
                                                               cancelButtonTitle:nil
                                                               otherButtonTitles:NSLocalizedString(@"OK", @"OK"), nil] autorelease];
                
                [alertView show];
            }
            
            return ;
        }
        else
        {
            [self edit];
        }
    }
    else
    {
        [self edit];
    }
    
    [super editButtonTapped];
}

- (IBAction)deleteButtonTapped
{
    if (self.currentEvent)
    {
        [self deleteEvent];
    }
    
    [self.inputDelegate commonEventInputViewControllerDidFinish:self];
    
    [super deleteButtonTapped];
}

- (IBAction)xButtonTapped
{
    if (self.datePopover.superview)
    {
        return ;
    }
    
    if (self.addPhotoPopover.superview)
    {
        return ;
    }
    
    if (self.recurrencePopover.superview)
    {
        return ;
    }
    
    [self.datePopover removeFromSuperview];
    [self.addPhotoPopover removeFromSuperview];
    [self.recurrencePopover removeFromSuperview];
    
    [super xButtonTapped];
}

- (void)photoButtonTapped
{
    if (self.datePopover.superview)
    {
        return ;
    }
    
    if (self.addPhotoPopover.superview)
    {
        return ;
    }
    
    if (self.recurrencePopover.superview)
    {
        return ;
    }
    
    if ([self.currentEvent.persistent boolValue])
    {
        if (!self.allowEdit)
        {
            InfoAlertView *alertView = [[[InfoAlertView alloc] initWithTitle:NSLocalizedString(@"\nThis event is protected", @"\nThis event is protected")
                                                                     message:nil
                                                                    delegate:self
                                                           cancelButtonTitle:nil
                                                           otherButtonTitles:NSLocalizedString(@"OK", @"OK"), nil] autorelease];
            
            [alertView show];
            
            return ;
        }
    }
    
    somethingChanged = YES;
    
    if (IS_IPAD)
    {
        self.addPhotoPopover.frame = CGRectMake(0, 0, self.addPhotoPopover.frame.size.width, self.addPhotoPopover.frame.size.height);
        
        [self.view addSubview:self.addPhotoPopover];
    }
    else
    {
        self.addPhotoPopover.frame = CGRectMake(0, 0, self.addPhotoPopover.frame.size.width, self.addPhotoPopover.frame.size.height);
        
        [self.infoContainer addSubview:self.addPhotoPopover];
    }
    
    [self.currentTextField resignFirstResponder];
    [self.datePopover removeFromSuperview];
    [self.recurrencePopover removeFromSuperview];
    
    self.xButton.selected = NO;
}

- (IBAction)okDateButtonTapped
{
    NSInteger HH;
    NSInteger mm;
    NSInteger day;
    NSInteger month;
    NSInteger year;
    
    HH = [self.hourMinutePickerView getHour];
    mm = [self.hourMinutePickerView getMinute];
    
    day = [self.monthDayPickerView getDay] + 1;
    month = [self.monthDayPickerView getMonth] + 1;
    year = [self.monthDayPickerView getYear];

    [eventInfo setObject:@(HH) forKey:kHourTimeKey];
    [eventInfo setObject:@(mm) forKey:kMinuteTimeKey];
    
    [eventInfo setObject:@(day) forKey:kDayKey];
    [eventInfo setObject:@(month) forKey:kMonthKey];
    [eventInfo setObject:@(year) forKey:kYearKey];
        
    self.dateStartLabel.text = [self setBeginDate];
    
    [self.datePopover removeFromSuperview];
}

- (IBAction)reccurenceButtonTapped:(UIButton*)button
{
    [self setTypeOfEvent:[NSNumber numberWithInt:button.tag]];
}

- (void)continueSaving
{
    [self saveEvent];
}

- (void)deleteEvent
{
    if (image)
    {
        NSString *iconName = [[[NSString stringWithFormat:@"%f", [self.currentEvent.eventId floatValue]] stringByReplacingOccurrencesOfString:@"." withString:@"_"] stringByAppendingPathExtension:@"png"];
        
        NSArray *paths = NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES);
        NSString *dirPath = [paths objectAtIndex:0];
        
        dirPath = [dirPath stringByAppendingPathComponent:iconName];
        
        NSURL *url = [NSURL URLWithString:dirPath];
        
        if ([[NSFileManager defaultManager] removeItemAtURL:url error:nil])
        {
            NSLog(@"image was removed");
        }
    }
    
    [[UIApplication sharedApplication] cancelLocalNotificationsForEventId:self.currentEvent.eventId];
    
    [[DataManager sharedInstance] removeEntityModel:self.currentEvent];
    
    [[NSNotificationCenter defaultCenter] postNotificationName:@"UpdateEventsListNotification" object:nil];
}

- (void)save
{
    Alarm *event = nil;
    
    if (self.currentEvent)
    {
        event = self.currentEvent;
    }
    else
    {
        event = (Alarm*)[[DataManager sharedInstance] createEntityWithName:kAlarmEntityName];
        event.persistent = @(YES);
    }
    
    event.eventId = [NSNumber numberWithDouble:[[NSDate date] timeIntervalSince1970]];
    
    NSDate *startDate = [NSDate dateWithYear:[[eventInfo objectForKey:kYearKey] integerValue]
                                       month:[[eventInfo objectForKey:kMonthKey] integerValue]
                                         day:[[eventInfo objectForKey:kDayKey] integerValue]
                                        hour:[[eventInfo objectForKey:kHourTimeKey] integerValue]
                                      minute:[[eventInfo objectForKey:kMinuteTimeKey] integerValue]
                                      second:0];
    
    event.date = startDate;
    
    event.year   = [eventInfo objectForKey:kYearKey];
    event.month  = [eventInfo objectForKey:kMonthKey];
    event.day    = [eventInfo objectForKey:kDayKey];
    event.hour   = [eventInfo objectForKey:kHourTimeKey];
    event.minute = [eventInfo objectForKey:kMinuteTimeKey];
    
    event.hasNotification = [eventInfo objectForKey:kHasNotificationKey];
    event.hasSnoozeTime = [eventInfo objectForKey:kHasSnoozeTimeKey];
    
    event.snoozeDate = nil;
    
    event.name = [eventInfo objectForKey:kNameKey];
    
    NSNumber *eventType = [eventInfo objectForKey:kEventTypeKey];
    
    event.type = eventType;
    
    if (image)
    {
        NSString *iconName = [[[NSString stringWithFormat:@"%f", [event.eventId floatValue]] stringByReplacingOccurrencesOfString:@"." withString:@"_"] stringByAppendingPathExtension:@"png"];
        
        NSArray *paths = NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES);
        NSString *dirPath = [paths objectAtIndex:0];
        
        dirPath = [dirPath stringByAppendingPathComponent:iconName];
        
        //NSURL *url = [NSURL URLWithString:dirPath];
        
        NSData *data = UIImagePNGRepresentation(image);
        
        [[NSFileManager defaultManager] createFileAtPath:dirPath contents:data attributes:nil];
    }
    else
    {
        NSString *iconName = [[[NSString stringWithFormat:@"%f", [event.eventId floatValue]] stringByReplacingOccurrencesOfString:@"." withString:@"_"] stringByAppendingPathExtension:@"png"];
        
        NSArray *paths = NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES);
        NSString *dirPath = [paths objectAtIndex:0];
        
        dirPath = [dirPath stringByAppendingPathComponent:iconName];
        
        [[NSFileManager defaultManager] removeItemAtPath:dirPath error:nil];
    }
    
    event.expired = [NSNumber numberWithBool:NO];
    
    [[DataManager sharedInstance] save];
    
    [event scheduleLocalNotifications];
    
    [[NSNotificationCenter defaultCenter] postNotificationName:@"UpdateEventsListNotification" object:nil];
    
    [self.inputDelegate commonEventInputViewControllerDidFinish:self];
}

- (void)saveEvent
{
    [self save];
}

- (IBAction)saveButtonTapped
{
    [super saveButtonTapped];
    
    [self.datePopover removeFromSuperview];
    [self.addPhotoPopover removeFromSuperview];
    [self.recurrencePopover removeFromSuperview];
    
    if (![self.titleTextField.text length])
    {
        [eventInfo setObject:@"" forKey:kNameKey];
    }
    else
    {
        [eventInfo setObject:self.titleTextField.text forKey:kNameKey];
    }
    
    [self continueSaving];
}

- (void)beginButtonTapped
{
    if (self.datePopover.superview)
    {
        return ;
    }
    
    if (self.addPhotoPopover.superview)
    {
        return ;
    }
    
    if (self.recurrencePopover.superview)
    {
        return ;
    }
    
    if ([self.currentEvent.persistent boolValue])
    {
        if (!self.allowEdit)
        {
            InfoAlertView *alertView = [[[InfoAlertView alloc] initWithTitle:NSLocalizedString(@"\nThis event is protected", @"\nThis event is protected")
                                                                     message:nil
                                                                    delegate:self
                                                           cancelButtonTitle:nil
                                                           otherButtonTitles:NSLocalizedString(@"OK", @"OK"), nil] autorelease];
            
            [alertView show];
            
            return ;
        }
    }
    
    somethingChanged = YES;
    
    self.datePopover.tag = 666;
    
    [self.addPhotoPopover removeFromSuperview];
    [self.currentTextField resignFirstResponder];
    [self.recurrencePopover removeFromSuperview];
    
    self.xButton.selected = NO;
    
    if (!IS_IPAD)
    {
        [self.infoContainer addSubview:self.datePopover];
    }
    else
    {
        [self.view addSubview:self.datePopover];
    }
    
    self.datePopover.center = CGPointMake(self.view.frame.size.width / 2, self.view.frame.size.height / 2);
    
    NSInteger hh = [[eventInfo objectForKey:kHourTimeKey] intValue];
    NSInteger mm = [[eventInfo objectForKey:kMinuteTimeKey] intValue];
    NSInteger dd = [[eventInfo objectForKey:kDayKey] intValue];
    NSInteger MM = [[eventInfo objectForKey:kMonthKey] intValue];
    NSInteger YY = [[eventInfo objectForKey:kYearKey] intValue];
    
    [self.monthDayPickerView setYear:YY];
    [self.monthDayPickerView setMonth:MM-1];
    [self.monthDayPickerView setDay:dd-1];
    
    [self.hourMinutePickerView setHour:hh];
    [self.hourMinutePickerView setMinute:mm];
    
    [self.monthDayPickerView selectToday];
    [self.hourMinutePickerView selectToday];
}

- (void)endButtonTapped
{

}

- (void)recurrenceButtonTapped
{    
    if (self.datePopover.superview)
    {
        return ;
    }
    
    if (self.addPhotoPopover.superview)
    {
        return ;
    }
    
    if (self.recurrencePopover.superview)
    {
        return ;
    }
    
    if ([self.currentEvent.persistent boolValue])
    {
        if (!self.allowEdit)
        {
            InfoAlertView *alertView = [[[InfoAlertView alloc] initWithTitle:NSLocalizedString(@"\nThis event is protected", @"\nThis event is protected")
                                                                     message:nil
                                                                    delegate:self
                                                           cancelButtonTitle:nil
                                                           otherButtonTitles:NSLocalizedString(@"OK", @"OK"), nil] autorelease];
            
            [alertView show];
            
            return ;
        }
    }
    
    somethingChanged = YES;
    
    [self.datePopover removeFromSuperview];
    [self.addPhotoPopover removeFromSuperview];
    [self.currentTextField resignFirstResponder];
    
    if (IS_IPAD)
    {
        [self.view addSubview:self.recurrencePopover];
    }
    else
    {
        [self.infoContainer addSubview:self.recurrencePopover];
    }
    
    self.xButton.selected = NO;
    
    if (IS_IPAD)
    {

    }
    else
    {
        self.recurrencePopover.frame = CGRectMake(self.view.bounds.size.width - self.recurrencePopover.bounds.size.width,
                                                  self.view.bounds.size.height - self.recurrencePopover.bounds.size.height,
                                                  self.recurrencePopover.frame.size.width,
                                                  self.recurrencePopover.frame.size.height);
    }
}

- (IBAction)delaySegmentValueChanged:(UIButton*)button
{
    somethingChanged = YES;
    
    if (self.onButton == button)
    {
        self.onButton.selected = YES;
        self.offButton.selected = NO;
        [eventInfo setObject:@(YES) forKey:kHasSnoozeTimeKey];
    }
    else
    {
        self.onButton.selected = NO;
        self.offButton.selected = YES;
        [eventInfo setObject:@(NO) forKey:kHasSnoozeTimeKey];
    }
}

- (void)setTypeOfEvent:(NSNumber*)type
{
    if (type)
    {
        [eventInfo setObject:type forKey:kEventTypeKey];
        
        switch ([type intValue])
        {
            case ET_Once:
                self.recurrenceLabel.text = NSLocalizedString(@"Never", @"Never");
                break;
                
            case ET_Daily:
                self.recurrenceLabel.text = NSLocalizedString(@"Daily", @"Daily");
                break;
                
            case ET_Weekly:
                self.recurrenceLabel.text = NSLocalizedString(@"Weekly", @"Weekly");
                break;
                
            case ET_Monthly:
                self.recurrenceLabel.text = NSLocalizedString(@"Monthly", @"Monthly");
                break;
                
            case ET_Annually:
                self.recurrenceLabel.text = NSLocalizedString(@"Annually", @"Annually");
                break;
        }
    }
    
    [self.recurrenceTableView reloadRowsAtIndexPaths:self.recurrenceTableView.indexPathsForVisibleRows withRowAnimation:UITableViewRowAnimationNone];
}

- (void)setIconOfEvent:(NSNumber*)eventId
{
    NSString *iconName = [[[NSString stringWithFormat:@"%f", [eventId floatValue]] stringByReplacingOccurrencesOfString:@"." withString:@"_"]stringByAppendingPathExtension:@"png"];
    
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES);
    NSString *dirPath = [paths objectAtIndex:0];
    
    dirPath = [dirPath stringByAppendingPathComponent:iconName];
    
    //NSURL *url = [NSURL URLWithString:dirPath];
    
    [image release];
    image = nil;
    
    image = [[UIImage imageWithContentsOfFile:dirPath] retain];//[[UIImage imageWithData:data] retain];
    
    if (image)
    {
        [icon removeFromSuperview];
        
        CGRect iconRect;
        
        if (IS_IPAD)
        {
                iconRect = CGRectMake(730, 7, 70, 70);
        }
        else
        {
            iconRect = CGRectMake(self.view.bounds.size.width - 60, 100, 40 , 40);
        }
        
        icon = [[[GRPhotoPreview alloc] initWithImage:image frame:iconRect] autorelease];
        icon.autoresizingMask = UIViewAutoresizingFlexibleLeftMargin | UIViewAutoresizingFlexibleBottomMargin | UIViewAutoresizingFlexibleTopMargin;
        icon.imageView.contentMode = UIViewContentModeScaleAspectFill;
        
        if (IS_IPAD)
        {
            [self.photoTableViewCell.contentView addSubview:icon];
        }
        else
        {
            [self.infoContainer addSubview:icon];
        } 
    }
}

- (NSString*)setBeginDate
{
    NSString *day = [[eventInfo objectForKey:kDayKey] stringValue];
    NSString *year = [[eventInfo objectForKey:kYearKey] stringValue];
    NSString *month = @"";
    NSInteger hour = [[eventInfo objectForKey:kHourTimeKey] intValue];
    NSInteger minute = [[eventInfo objectForKey:kMinuteTimeKey] intValue];
    
    NSInteger theYear = [year intValue];
 
    NSDateFormatter *dateFormatter = [[NSDateFormatter new] autorelease];
    NSArray *months = [dateFormatter standaloneMonthSymbols];
    
    NSInteger index = [[eventInfo objectForKey:kMonthKey] intValue] - 1;
    
    month = [months objectAtIndex:index];
    
    NSString *time = @"";
    
//    if (![settings.timeFormat intValue])
//    {
//        if (hour >= 12)
//        {
//            time = @"PM";
//        }
//        else
//        {
//            time = @"AM";
//        }
//        
//        if (hour > 12)
//        {
//            hour -= 12;
//        }
//        
//        if (hour == 0)
//        {
//            hour = 12;
//        }
//    }
    
    return [NSString stringWithFormat:@"%@ %@   /   %i   /   %02i:%02i %@", month, day, theYear, hour, minute, time];
}

#pragma mark - UITextFieldDelegate's methods

- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField
{
    if (self.datePopover.superview)
    {
        return NO;
    }
    
    if (self.addPhotoPopover.superview)
    {
        return NO;
    }
    
    if (self.recurrencePopover.superview)
    {
        return NO;
    }
    
    self.xButton.selected = NO;
    
    [self.datePopover removeFromSuperview];
    [self.addPhotoPopover removeFromSuperview];
    [self.recurrencePopover removeFromSuperview];
    
    if ([self.currentEvent.persistent boolValue])
    {
        if (!self.allowEdit)
        {
            InfoAlertView *alertView = [[[InfoAlertView alloc] initWithTitle:NSLocalizedString(@"\nThis event is protected", @"\nThis event is protected")
                                                                     message:nil
                                                                    delegate:self
                                                           cancelButtonTitle:nil
                                                           otherButtonTitles:NSLocalizedString(@"OK", @"OK"), nil] autorelease];
            
            [alertView show];
            
            return NO;
        }
    }
    
    self.currentTextField = textField;
    
    return YES;
}

#pragma mark - BDGridViewDataSource's methods

- (CGSize)gridViewSizeOfCell:(BDGridView *)gridView
{
    if (IS_IPAD)
    {
        CGFloat width = 100;
        
        return CGSizeMake(width, width);
    }
    else
    {
        CGFloat width = (gridView.bounds.size.width - 3 * gridView.minimumPadding)/ 4;
        
        return CGSizeMake(width, width);
    }
    
    return CGSizeZero;
}

#pragma mark - BDGridViewDelegate's methods

- (void)gridView:(BDGridView *)gridView didTapCell:(BDGridCell *)cell
{
    ALAsset *asset = [assets objectAtIndex:cell.index];
    
    if (image)
    {
        [image release];
        image = nil;
    }
    
    image = [UIImage imageWithCGImage:[[asset defaultRepresentation] fullScreenImage]];
    
    [image retain];
    
    [icon removeFromSuperview];
    
    CGRect iconRect;
    
    if (IS_IPAD)
    {
            iconRect = CGRectMake(753, 20, 70, 70);
    }
    else
    {
        iconRect = CGRectMake(self.view.bounds.size.width - 60, 10, 40 , 40);
    }
    
    icon = [[[GRPhotoPreview alloc] initWithImage:image frame:iconRect] autorelease];
    icon.autoresizingMask = UIViewAutoresizingFlexibleLeftMargin  | UIViewAutoresizingFlexibleBottomMargin | UIViewAutoresizingFlexibleTopMargin;
    icon.imageView.contentMode = UIViewContentModeScaleAspectFill;
    
    if (IS_IPAD)
    {
        [self.photoTableViewCell.contentView addSubview:icon];
    }
    else
    {
        [self.infoContainer addSubview:icon];
    }
    
    [self.addPhotoPopover removeFromSuperview];
}

#pragma mark - UITableViewDataSource's methods

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (tableView == self.recurrenceTableView)
    {
        return 9;
    }
    
    return 5;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (tableView == self.recurrenceTableView)
    {
        RecurrenceTableViewCell *recurrenceTableViewCell = (RecurrenceTableViewCell*)[tableView dequeueReusableCellWithIdentifier:@"RecurrenceTableViewCellIdentifier"];
        
        switch (indexPath.row)
        {
            case ET_Daily:
                recurrenceTableViewCell.titleLabel.text = NSLocalizedString(@"Daily", @"Daily");
                break;
                
            case ET_Weekly:
                recurrenceTableViewCell.titleLabel.text = NSLocalizedString(@"Weekly", @"Weekly");
                break;
                
            case ET_Monthly:
                recurrenceTableViewCell.titleLabel.text = NSLocalizedString(@"Monthly", @"Monthly");
                break;
                
            case ET_Annually:
                recurrenceTableViewCell.titleLabel.text = NSLocalizedString(@"Annually", @"Annually");
                break;
                
            case ET_Once:
                recurrenceTableViewCell.titleLabel.text = NSLocalizedString(@"Never", @"Never");
                break;
        }
        
        recurrenceTableViewCell.checkmarkImageView.image = [UIImage imageNamed:@"settings_calendar_checkmark_icon"];
        
        if ([[eventInfo objectForKey:kEventTypeKey] intValue] == indexPath.row)
        {
            recurrenceTableViewCell.checkmarkImageView.hidden = NO;
            recurrenceTableViewCell.backgroundView = [[UIImageView alloc] initWithImage:[[UIImage imageNamed:@"settings_calendar_selected_cell"] resizableImageWithCapInsets:UIEdgeInsetsMake(10, 10, 10, 10)]];
            recurrenceTableViewCell.titleLabel.textColor = RGBCOLOR(7, 11, 35);
            recurrenceTableViewCell.titleLabel.font = [UIFont fontWithName:@"Helvetica-Bold" size:18];
        }
        else
        {
            recurrenceTableViewCell.checkmarkImageView.hidden = YES;
            recurrenceTableViewCell.backgroundView = nil;
            recurrenceTableViewCell.titleLabel.textColor = RGBCOLOR(96, 96, 96);
            recurrenceTableViewCell.titleLabel.font = [UIFont fontWithName:@"Helvetica" size:18];
        }
        
        return recurrenceTableViewCell;
    }
    
    switch (indexPath.row)
    {
        case 0:
            return self.startDateTableViewCell;
            break;
        
        case 1:
            return self.titleTableViewCell;
            break;
            
        case 2:
            return self.repeatTableViewCell;
            break;
            
        case 3:
            return self.snoozeTableViewCell;
            break;
            
        case 4:
            return self.photoTableViewCell;
            break;
            
//                case 5:
//                    return self.protectTableViewCell;
//                    break;
            
        default:
            break;
    }
    
    return nil;
}

#pragma mark - UITableViewDelegate's methods

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    if (tableView == self.recurrenceTableView)
    {
        [self setTypeOfEvent:@(indexPath.row)];
        
        return;
    }
    
    switch (indexPath.row)
    {
        case 4:
            [self photoButtonTapped];
            break;
            
        case 0:
            [self beginButtonTapped];
            break;
            
        case 2:
            [self recurrenceButtonTapped];
            break;
            
        default:
            break;
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (tableView == self.recurrenceTableView)
    {
        return 92;
    }
    
    if (indexPath.row == 4)
    {
        return 100.f;
    }
    
    return 80.f;
}

@end

