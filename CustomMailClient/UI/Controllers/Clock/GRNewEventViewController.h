//
//  GRNewEventViewController.h
//  GrannyApp
//
//  Created by Eugene Pavlyuk on 7/16/12.
//  Copyright (c) 2012 Home. All rights reserved.
//

#import "GRCommonEventInputViewController.h"
#import "AlertView.h"

@class GRDatePickerView;
@class GRTimePickerView;
@class STSegmentedControl;

@interface GRNewEventViewController : GRCommonEventInputViewController

@property (nonatomic, retain) IBOutlet UILabel *dateEndLabel;

@property (nonatomic, retain) IBOutlet GRDatePickerView *monthDayPickerView;
@property (nonatomic, retain) IBOutlet GRTimePickerView *hourMinutePickerView;;

@property (nonatomic, retain) IBOutlet STSegmentedControl *delaySegmentControl;

@property (nonatomic, retain) IBOutlet BaseEventTableViewCell *snoozeTableViewCell;
@property (nonatomic, retain) IBOutlet BaseEventTableViewCell *endDateTableViewCell;

- (void)beginButtonTapped;
- (void)endButtonTapped;
- (IBAction)okDateButtonTapped;

- (IBAction)delaySegmentValueChanged;

@end
