//
//  GRNewAlarmViewController.m
//  GrannyApp
//
//  Created by Eugene Pavlyuk on 7/16/12.
//  Copyright (c) 2012 Home. All rights reserved.
//

#import "GRNewAlarmViewController.h"
#import "DataManager.h"
#import "Alarm.h"
#import "Settings.h"
#import "GRDatePickerView.h"
#import "GRTimePickerView.h"
#import "STSegmentedControl.h"
#import "BaseEventTableViewCell.h"
#import "GRCommonAlertView.h"
#import "BDGridCell.h"
#import "GRPhotoPreview.h"
#import "WYPopoverController.h"
#import "RecurrenceTableViewCell.h"
#import "GRAlertViewController.h"
#import "UIImage+Border.h"
#import "NSDate+Calendar.h"

@interface GRNewAlarmViewController () <AlertViewDelegate, WYPopoverControllerDelegate>

@property (nonatomic, retain) Alarm *currentEvent;

@end

@implementation GRNewAlarmViewController

- (void)setupPopoverAppearanceForTitleAlert
{
    WYPopoverBackgroundView *popoverAppearance = [WYPopoverBackgroundView appearance];
    
    [popoverAppearance setOverlayColor:[UIColor colorWithWhite:0.1f alpha:0.6f]];
    [popoverAppearance setOuterCornerRadius:10];
    [popoverAppearance setOuterShadowBlurRadius:5];
    [popoverAppearance setOuterShadowColor:[UIColor colorWithWhite:0.1f alpha:0.6f]];
    [popoverAppearance setOuterShadowOffset:CGSizeMake(5, 5)];
    
    [popoverAppearance setGlossShadowColor:[UIColor clearColor]];
    [popoverAppearance setGlossShadowOffset:CGSizeMake(0, 0)];
    
    [popoverAppearance setBorderWidth:0];
    [popoverAppearance setArrowHeight:30];
    [popoverAppearance setArrowBase:35];
    
    [popoverAppearance setInnerCornerRadius:0];
    [popoverAppearance setInnerShadowBlurRadius:0];
    [popoverAppearance setInnerShadowColor:[UIColor clearColor]];
    [popoverAppearance setInnerShadowOffset:CGSizeMake(0, 0)];
    
    [popoverAppearance setFillTopColor:[UIColor whiteColor]];
    [popoverAppearance setFillBottomColor:[UIColor whiteColor]];
    [popoverAppearance setOuterStrokeColor:[UIColor clearColor]];
    [popoverAppearance setInnerStrokeColor:[UIColor clearColor]];
}

- (void)setupPopoverAppearanceForProtectedAlert
{
    WYPopoverBackgroundView *popoverAppearance = [WYPopoverBackgroundView appearance];
    
    [popoverAppearance setOverlayColor:[UIColor colorWithWhite:0.1f alpha:0.6f]];
    [popoverAppearance setOuterCornerRadius:10];
    [popoverAppearance setOuterShadowBlurRadius:5];
    [popoverAppearance setOuterShadowColor:[UIColor colorWithWhite:0.1f alpha:0.6f]];
    [popoverAppearance setOuterShadowOffset:CGSizeMake(5, 5)];
    
    [popoverAppearance setGlossShadowColor:[UIColor clearColor]];
    [popoverAppearance setGlossShadowOffset:CGSizeMake(0, 0)];
    
    [popoverAppearance setBorderWidth:0];
    [popoverAppearance setArrowHeight:0];
    [popoverAppearance setArrowBase:0];
    
    [popoverAppearance setInnerCornerRadius:0];
    [popoverAppearance setInnerShadowBlurRadius:0];
    [popoverAppearance setInnerShadowColor:[UIColor clearColor]];
    [popoverAppearance setInnerShadowOffset:CGSizeMake(0, 0)];
    
    [popoverAppearance setFillTopColor:[UIColor whiteColor]];
    [popoverAppearance setFillBottomColor:[UIColor whiteColor]];
    [popoverAppearance setOuterStrokeColor:[UIColor clearColor]];
    [popoverAppearance setInnerStrokeColor:[UIColor clearColor]];
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    
    if (self) 
    {
        self.allowEdit = NO;
    }
    
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    
    self.screenName = @"Event/Alarm Details Screen";
    
    self.dateContainerView.layer.shadowOpacity = 0.6f;
    self.dateContainerView.layer.shadowColor = [UIColor blackColor].CGColor;
    self.dateContainerView.layer.shadowOffset = CGSizeMake(5, 5);
    
    self.hourMinutePickerView.componentWidth = 100.f;
    
    [self.hourMinutePickerView reset];
    
    self.delaySegmentControl.segments = [[@[@"ON", @"OFF"] mutableCopy] autorelease];
    self.delaySegmentControl.font = [UIFont fontWithName:@"Helvetica" size:30];
    
    self.delaySegmentControl.type = stSegmentAlarmType;
    
    [self.delaySegmentControl initImages];
    [self.delaySegmentControl setNeedsDisplay];
    
    if (self.currentEvent)
    {
        [self setTypeOfEvent:self.currentEvent.type];
        [self setNameOfEvent:self.currentEvent.name];
        [self setIconOfEvent:self.currentEvent.eventId];
        
        [eventInfo setObject:self.currentEvent.month forKey:kMonthKey];
        [eventInfo setObject:self.currentEvent.day forKey:kDayKey];
        [eventInfo setObject:self.currentEvent.year forKey:kYearKey];
        [eventInfo setObject:self.currentEvent.hour forKey:kHourTimeKey];
        [eventInfo setObject:self.currentEvent.minute forKey:kMinuteTimeKey];
        
//        NSTimeInterval timeInterval;
        
            if ([self.currentEvent.hasSnoozeTime boolValue])
            {
                self.delaySegmentControl.selectedSegmentIndex = 0;
                [eventInfo setObject:@(YES) forKey:kHasSnoozeTimeKey];
                
//                timeInterval = 0;
            }
            else
            {
                self.delaySegmentControl.selectedSegmentIndex = 1;
                [eventInfo setObject:@(NO) forKey:kHasSnoozeTimeKey];
                
//                timeInterval = 0;
            }
            
            [eventInfo setObject:@(NO) forKey:kHasNotificationKey];
        
        [eventInfo setObject:self.currentEvent.eventId forKey:kEventIdKey];
    }
    else
    {
        [self setTypeOfEvent:@(ET_Once)];
        
        NSNumber *year   = @([[NSDate date] year]);
        NSNumber *month  = @([[NSDate date] month]);
        NSNumber *day    = @([[NSDate date] day]);
        NSNumber *hour   = @([[NSDate date] hour]);
        NSNumber *minute = @([[NSDate date] minute]);
        
        [eventInfo setObject:year forKey:kYearKey];
        [eventInfo setObject:month forKey:kMonthKey];
        [eventInfo setObject:day forKey:kDayKey];
        [eventInfo setObject:hour forKey:kHourTimeKey];
        [eventInfo setObject:minute forKey:kMinuteTimeKey];
        
        [eventInfo setObject:@(NO) forKey:kHasSnoozeTimeKey];
        [eventInfo setObject:@(NO) forKey:kHasNotificationKey];
        
        self.delaySegmentControl.selectedSegmentIndex = 1;
    }
  
    self.deleteButton.hidden = YES;
    
    if (self.currentEvent)
    {
        self.titleTableViewCell.userInteractionEnabled = NO;
        self.photoTableViewCell.userInteractionEnabled = NO;
        self.startDateTableViewCell.userInteractionEnabled = NO;
        self.snoozeTableViewCell.userInteractionEnabled = NO;
        self.repeatTableViewCell.userInteractionEnabled = NO;
        
        if ([self.currentEvent.persistent boolValue])
        {
            self.editButton.hidden = YES;
            self.okButton.hidden = YES;
        }
        
        if ([self.currentEvent.persistent boolValue])
        {
            self.titleLabel.text = NSLocalizedString(@"Protected Alarm", @"Protected Alarm");
        }
        else
        {
            self.titleLabel.text = NSLocalizedString(@"Alarm", @"Alarm");
        }
    }
    else
    {
        self.editButton.hidden = YES;
        self.titleTableViewCell.userInteractionEnabled = YES;
        self.photoTableViewCell.userInteractionEnabled = YES;
        self.startDateTableViewCell.userInteractionEnabled = YES;
        self.snoozeTableViewCell.userInteractionEnabled = YES;
        self.repeatTableViewCell.userInteractionEnabled = YES;
        
        self.titleLabel.text = NSLocalizedString(@"New Alarm", @"New Alarm");
    }
    
    self.photoTableViewCell.backgroundImageView.image = nil;
    self.photoTableViewCell.separatorImageView.hidden = YES;
        
    self.titleTableViewCell.backgroundImageView.image = nil;
    self.startDateTableViewCell.backgroundImageView.image = nil;
    self.repeatTableViewCell.backgroundImageView.image = nil;
    self.snoozeTableViewCell.backgroundImageView.image = nil;
    self.photoTableViewCell.backgroundImageView.image = nil;
    
    self.titleTableViewCell.separatorOffset = CGRectMake(0, 0, 0, 2);
    self.startDateTableViewCell.separatorOffset = CGRectMake(0, 0, 0, 2);
    self.repeatTableViewCell.separatorOffset = CGRectMake(0, 0, 0, 2);
    self.snoozeTableViewCell.separatorOffset = CGRectMake(0, 0, 0, 2);
    
    self.dateStartLabel.text = [self setBeginDate];
}

- (void)releaseViews
{
    [super releaseViews];
    
    self.delaySegmentControl = nil;    
    self.snoozeTableViewCell = nil;
}

- (void)dealloc
{
    [super dealloc];
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
	return UIInterfaceOrientationIsLandscape(interfaceOrientation);
}

- (void)edit
{
    self.titleTableViewCell.userInteractionEnabled = YES;
    self.photoTableViewCell.userInteractionEnabled = YES;
    self.startDateTableViewCell.userInteractionEnabled = YES;
    self.snoozeTableViewCell.userInteractionEnabled = YES;
    self.repeatTableViewCell.userInteractionEnabled = YES;
    
    self.deleteButton.hidden = NO;
    self.editButton.hidden = YES;
    
    self.xButton.selected = NO;
}

- (void)showProtectedAlert
{
    [self setupPopoverAppearanceForProtectedAlert];
    
    GRAlertViewController *contentViewController = [[[GRAlertViewController alloc] init] autorelease];
    contentViewController.contentSizeForViewInPopover = CGSizeMake(512, 270);
    
    [contentViewController view];
    
    contentViewController.messageLabel.text = NSLocalizedString(@"This is a \n protected alarm", @"This is a \n protected alarm");
    
    [contentViewController.okButton addTarget:self action:@selector(popoverButtonTapped:) forControlEvents:UIControlEventTouchUpInside];
    
    self.alertPopoverController = [[[WYPopoverController alloc] initWithContentViewController:contentViewController] autorelease];
    self.alertPopoverController.delegate = self;
    
    CGRect rect = CGRectMake(self.view.bounds.size.width / 2, self.view.bounds.size.height / 2 + 100, 5, 5);
    
    [self.alertPopoverController presentPopoverFromRect:rect inView:self.view permittedArrowDirections:WYPopoverArrowDirectionNone animated:YES];
}

- (IBAction)editButtonTapped
{
    if (self.currentEvent)
    {
        if ([self.currentEvent.persistent boolValue])
        {
            if (self.allowEdit)
            {
                [self edit];
            }
            else
            {
                [self showProtectedAlert];
            }
            
            return ;
        }
        else
        {
            [self edit];
        }
    }
    else
    {
        [self edit];
    }
    
    [super editButtonTapped];
}

- (IBAction)deleteButtonTapped
{
    if (self.currentEvent)
    {
        if ([self.currentEvent.persistent boolValue])
        {
            if (self.allowEdit)
            {
                DeleteAlertView *alertView = [[[DeleteAlertView alloc] initWithTitle:NSLocalizedString(@"This is a \n protected alarm", @"This is a \n protected alarm")
                                                                             message:nil
                                                                            delegate:self
                                                                   cancelButtonTitle:nil
                                                                   otherButtonTitles:
                                               NSLocalizedString(@"GO BACK", @"GO BACK"),
                                               NSLocalizedString(@"OK", @"OK"), nil] autorelease];
                
                alertView.tag = 669;
                [alertView show];
            }
            else
            {
                InfoAlertView *alertView = [[[InfoAlertView alloc] initWithTitle:NSLocalizedString(@"This is a \n protected alarm", @"This is a \n protected alarm")
                                                                         message:nil
                                                                        delegate:self
                                                               cancelButtonTitle:nil
                                                               otherButtonTitles:NSLocalizedString(@"OK", @"OK"), nil] autorelease];
                
                [alertView show];
            }
            
            return ;
        }
        else
        {
            [self deleteEvent];
        }
    }
    
    [self.inputDelegate commonEventInputViewControllerDidFinish:self];
    
    [super deleteButtonTapped];
}

- (IBAction)xButtonTapped
{
    if (self.datePopover.superview)
    {
        return ;
    }
    
    if (self.addPhotoPopover.superview)
    {
        return ;
    }
    
    if (self.recurrencePopover.superview)
    {
        return ;
    }
    
    [self.datePopover removeFromSuperview];
    [self.addPhotoPopover removeFromSuperview];
    [self.recurrencePopover removeFromSuperview];
    
    [super xButtonTapped];
}

- (void)photoButtonTapped
{
    if (self.datePopover.superview)
    {
        return ;
    }
    
    if (self.addPhotoPopover.superview)
    {
        return ;
    }
    
    if (self.recurrencePopover.superview)
    {
        return ;
    }
    
    if ([self.currentEvent.persistent boolValue])
    {
        if (!self.allowEdit)
        {
            InfoAlertView *alertView = [[[InfoAlertView alloc] initWithTitle:NSLocalizedString(@"This is a \n protected alarm", @"This is a \n protected alarm")
                                                                     message:nil
                                                                    delegate:self
                                                           cancelButtonTitle:nil
                                                           otherButtonTitles:NSLocalizedString(@"OK", @"OK"), nil] autorelease];
            
            [alertView show];
            
            return ;
        }
    }
    
    somethingChanged = YES;
    
    if (IS_IPAD)
    {
        self.addPhotoPopover.frame = CGRectMake(0, 0, self.addPhotoPopover.frame.size.width, self.addPhotoPopover.frame.size.height);
        
        [self.view addSubview:self.addPhotoPopover];
    }
    else
    {
        self.addPhotoPopover.frame = CGRectMake(0, 0, self.addPhotoPopover.frame.size.width, self.addPhotoPopover.frame.size.height);
        
        [self.infoContainer addSubview:self.addPhotoPopover];
    }
    
    [self.currentTextField resignFirstResponder];
    [self.datePopover removeFromSuperview];
    [self.recurrencePopover removeFromSuperview];
    
    self.xButton.selected = NO;
}

- (IBAction)okDateButtonTapped
{
    NSInteger HH;
    NSInteger mm;
    NSInteger day;
    NSInteger month;
    NSInteger year;
    
    HH = [self.hourMinutePickerView getHour];
    mm = [self.hourMinutePickerView getMinute];
    
    day = [self.monthDayPickerView getDay];
    month = [self.monthDayPickerView getMonth];
    year = [self.monthDayPickerView getYear];
    
    [eventInfo setObject:[NSNumber numberWithInt:HH] forKey:kHourTimeKey];
    [eventInfo setObject:[NSNumber numberWithInt:mm] forKey:kMinuteTimeKey];
    
    [eventInfo setObject:[NSNumber numberWithInt:(day + 1)] forKey:kDayKey];
    [eventInfo setObject:[NSNumber numberWithInt:(month + 1)] forKey:kMonthKey];
    [eventInfo setObject:[NSNumber numberWithInt:year] forKey:kYearKey];
    
    self.dateStartLabel.text = [self setBeginDate];
    
    [self.datePopover removeFromSuperview];
}

- (IBAction)reccurenceButtonTapped:(UIButton*)button
{
    [self setTypeOfEvent:[NSNumber numberWithInt:button.tag]];
}

- (void)continueSaving
{
    [self saveEvent];
}

- (void)deleteEvent
{
    if (image)
    {
        NSString *iconName = [[[NSString stringWithFormat:@"%f", [self.currentEvent.eventId floatValue]] stringByReplacingOccurrencesOfString:@"." withString:@"_"] stringByAppendingPathExtension:@"png"];
        
        NSArray *paths = NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES);
        NSString *dirPath = [paths objectAtIndex:0];
        
        dirPath = [dirPath stringByAppendingPathComponent:iconName];
        
        NSURL *url = [NSURL URLWithString:dirPath];
        
        if ([[NSFileManager defaultManager] removeItemAtURL:url error:nil])
        {
            NSLog(@"image was removed");
        }
    }
    
    [[UIApplication sharedApplication] cancelLocalNotificationsForEventId:self.currentEvent.eventId];
    
    [[DataManager sharedInstance] removeEntityModel:self.currentEvent];
    
    [[NSNotificationCenter defaultCenter] postNotificationName:@"UpdateEventsListNotification" object:nil];
}

- (void)save
{
    Alarm *event = nil;
    
    if (self.currentEvent)
    {
        event = self.currentEvent;
    }
    else
    {
        event = (Alarm*)[[DataManager sharedInstance] createEntityWithName:kAlarmEntityName];
        event.persistent = @(NO);
    }
    
    event.eventId = [NSNumber numberWithDouble:[[NSDate date] timeIntervalSince1970]];
    
    NSDate *startDate = [NSDate dateWithYear:[[eventInfo objectForKey:kYearKey] integerValue]
                                       month:[[eventInfo objectForKey:kMonthKey] integerValue]
                                         day:[[eventInfo objectForKey:kDayKey] integerValue]
                                        hour:[[eventInfo objectForKey:kHourTimeKey] integerValue]
                                      minute:[[eventInfo objectForKey:kMinuteTimeKey] integerValue]
                                      second:0];
    
    event.date = startDate;
    
    event.year   = [eventInfo objectForKey:kYearKey];
    event.month  = [eventInfo objectForKey:kMonthKey];
    event.day    = [eventInfo objectForKey:kDayKey];
    event.hour   = [eventInfo objectForKey:kHourTimeKey];
    event.minute = [eventInfo objectForKey:kMinuteTimeKey];
    
    event.hasNotification = [eventInfo objectForKey:kHasNotificationKey];
    event.hasSnoozeTime = [eventInfo objectForKey:kHasSnoozeTimeKey];
    
    event.snoozeDate = nil;
    
    event.name = [eventInfo objectForKey:kNameKey];
    
    NSNumber *eventType = [eventInfo objectForKey:kEventTypeKey];
    
    event.type = eventType;
    
    if (image)
    {
        NSString *iconName = [[[NSString stringWithFormat:@"%f", [event.eventId floatValue]] stringByReplacingOccurrencesOfString:@"." withString:@"_"] stringByAppendingPathExtension:@"png"];
        
        NSArray *paths = NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES);
        NSString *dirPath = [paths objectAtIndex:0];
        
        dirPath = [dirPath stringByAppendingPathComponent:iconName];
        
        //NSURL *url = [NSURL URLWithString:dirPath];
        
        NSData *data = UIImagePNGRepresentation(image);
        
        [[NSFileManager defaultManager] createFileAtPath:dirPath contents:data attributes:nil];
    }
    else
    {
        NSString *iconName = [[[NSString stringWithFormat:@"%f", [event.eventId floatValue]] stringByReplacingOccurrencesOfString:@"." withString:@"_"] stringByAppendingPathExtension:@"png"];
        
        NSArray *paths = NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES);
        NSString *dirPath = [paths objectAtIndex:0];
        
        dirPath = [dirPath stringByAppendingPathComponent:iconName];
      
        [[NSFileManager defaultManager] removeItemAtPath:dirPath error:nil];
    }
    
    event.expired = [NSNumber numberWithBool:NO];
    
    [[DataManager sharedInstance] save];
    
    [event scheduleLocalNotifications];
    
    [[NSNotificationCenter defaultCenter] postNotificationName:@"UpdateEventsListNotification" object:nil];
    
    [self.inputDelegate commonEventInputViewControllerDidFinish:self];
}

- (void)saveEvent
{
    [self save];
}

- (IBAction)saveButtonTapped
{
    [super saveButtonTapped];
    
    [self.datePopover removeFromSuperview];
    [self.addPhotoPopover removeFromSuperview];
    [self.recurrencePopover removeFromSuperview];
    
    if (![self.titleTextField.text length])
    {
        [eventInfo setObject:@"" forKey:kNameKey];
    }
    else
    {
        [eventInfo setObject:self.titleTextField.text forKey:kNameKey];
    }
    
    [self continueSaving];
}

- (void)beginButtonTapped
{
    if (self.datePopover.superview)
    {
        return ;
    }
    
    if (self.addPhotoPopover.superview)
    {
        return ;
    }
    
    if (self.recurrencePopover.superview)
    {
        return ;
    }
    
    if ([self.currentEvent.persistent boolValue])
    {
        if (!self.allowEdit)
        {
            [self showProtectedAlert];
            
            return ;
        }
    }
    
    somethingChanged = YES;
    
    self.datePopover.tag = 666;
    
    [self.addPhotoPopover removeFromSuperview];
    [self.currentTextField resignFirstResponder];
    [self.recurrencePopover removeFromSuperview];
    
    self.xButton.selected = NO;
    
    if (!IS_IPAD)
    {
        [self.infoContainer addSubview:self.datePopover];
    }
    else
    {
        [self.view addSubview:self.datePopover];
    }
    
    if (IS_IPAD)
    {
        self.datePopover.center = CGPointMake(self.view.frame.size.width / 2, self.view.frame.size.height / 2);
    }
    else
    {
        self.datePopover.frame = CGRectMake(0, 0, self.datePopover.frame.size.width, self.datePopover.frame.size.height);
    }
    
    NSInteger hh = [[eventInfo objectForKey:kHourTimeKey] intValue];
    NSInteger mm = [[eventInfo objectForKey:kMinuteTimeKey] intValue];
    NSInteger dd = [[eventInfo objectForKey:kDayKey] intValue];
    NSInteger MM = [[eventInfo objectForKey:kMonthKey] intValue];
    NSInteger YY = [[eventInfo objectForKey:kYearKey] intValue];
    
    [self.monthDayPickerView setYear:YY];
    [self.monthDayPickerView setMonth:MM-1];
    [self.monthDayPickerView setDay:dd-1];
    
    [self.hourMinutePickerView setHour:hh];
    [self.hourMinutePickerView setMinute:mm];
    
    [self.monthDayPickerView selectToday];
    [self.hourMinutePickerView selectToday];
}

- (void)recurrenceButtonTapped
{    
    if (self.datePopover.superview)
    {
        return ;
    }
    
    if (self.addPhotoPopover.superview)
    {
        return ;
    }
    
    if (self.recurrencePopover.superview)
    {
        return ;
    }
        
    if ([self.currentEvent.persistent boolValue])
    {
        if (!self.allowEdit)
        {
            [self showProtectedAlert];
            
            return ;
        }
    }
    
    somethingChanged = YES;
    
    [self.datePopover removeFromSuperview];
    [self.addPhotoPopover removeFromSuperview];
    [self.currentTextField resignFirstResponder];
    
    if (IS_IPAD)
    {
        [self.view addSubview:self.recurrencePopover];
    }
    else
    {
        [self.infoContainer addSubview:self.recurrencePopover];
    }
    
    self.xButton.selected = NO;
    
    if (IS_IPAD)
    {
    }
    else
    {
        self.recurrencePopover.frame = CGRectMake(self.view.bounds.size.width - self.recurrencePopover.bounds.size.width,
                                                  self.view.bounds.size.height - self.recurrencePopover.bounds.size.height,
                                                  self.recurrencePopover.frame.size.width,
                                                  self.recurrencePopover.frame.size.height);
    }
}

- (void)popoverButtonTapped:(UIButton*)button
{
    [self.alertPopoverController dismissPopoverAnimated:YES];
    self.alertPopoverController = nil;
}

- (IBAction)delaySegmentValueChanged
{
    somethingChanged = YES;
    
    if (self.delaySegmentControl.selectedSegmentIndex == 0)
    {
        [eventInfo setObject:@(YES) forKey:kHasSnoozeTimeKey];
    }
    else
    {
        [eventInfo setObject:@(NO) forKey:kHasSnoozeTimeKey];
    }
}

- (void)setTypeOfEvent:(NSNumber*)type
{
    if (type)
    {
        [eventInfo setObject:type forKey:kEventTypeKey];
        
        switch ([type intValue])
        {
            case ET_Once:
                self.recurrenceLabel.text = NSLocalizedString(@"Don't repeat", @"Don't repeat");
                break;
                
            case ET_Daily:
                self.recurrenceLabel.text = NSLocalizedString(@"Daily", @"Daily");
                break;
                
            case ET_Weekly:
                self.recurrenceLabel.text = NSLocalizedString(@"Weekly", @"Weekly");
                break;
                
            case ET_Monthly:
                self.recurrenceLabel.text = NSLocalizedString(@"Monthly", @"Monthly");
                break;
                
            case ET_Annually:
                self.recurrenceLabel.text = NSLocalizedString(@"Annually", @"Annually");
                break;
        }
    }
    
    [self.recurrenceTableView reloadRowsAtIndexPaths:self.recurrenceTableView.indexPathsForVisibleRows withRowAnimation:UITableViewRowAnimationNone];
}

- (void)setIconOfEvent:(NSNumber*)eventId
{
    NSString *iconName = [[[NSString stringWithFormat:@"%f", [eventId floatValue]] stringByReplacingOccurrencesOfString:@"." withString:@"_"]stringByAppendingPathExtension:@"png"];
    
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES);
    NSString *dirPath = [paths objectAtIndex:0];
    
    dirPath = [dirPath stringByAppendingPathComponent:iconName];
    
    //NSURL *url = [NSURL URLWithString:dirPath];
    
    [image release];
    image = nil;
    
    image = [[UIImage imageWithContentsOfFile:dirPath] retain];//[[UIImage imageWithData:data] retain];
    
    if (image)
    {
        [icon removeFromSuperview];
        
        CGRect iconRect;
        
        if (IS_IPAD)
        {
            iconRect = CGRectMake(650, 25, 150, 150);
        }
        else
        {
            iconRect = CGRectMake(self.view.bounds.size.width - 60, 100, 40 , 40);
        }
        
        icon = [[[GRPhotoPreview alloc] initWithImage:image frame:iconRect] autorelease];
        icon.autoresizingMask = UIViewAutoresizingFlexibleLeftMargin;
        icon.imageView.contentMode = UIViewContentModeScaleAspectFill;
        
        icon.layer.cornerRadius = 20.f;
        icon.layer.borderWidth = 1.f;
        icon.layer.borderColor = RGBCOLOR(128, 128, 128).CGColor;
        icon.layer.shadowRadius = 0.f;
        icon.layer.shadowColor = [UIColor clearColor].CGColor;
        icon.layer.shadowOpacity = 0.f;
        
        icon.imageView.layer.cornerRadius = 20.f;
        
        if (IS_IPAD)
        {
            [self.photoTableViewCell.contentView addSubview:icon];
        }
        else
        {
            [self.infoContainer addSubview:icon];
        } 
    }
}

- (NSString*)setBeginDate
{
    NSString *day = [[eventInfo objectForKey:kDayKey] stringValue];
    NSString *year = [[eventInfo objectForKey:kYearKey] stringValue];
    NSString *month = @"";
    NSInteger hour = [[eventInfo objectForKey:kHourTimeKey] intValue];
    NSInteger minute = [[eventInfo objectForKey:kMinuteTimeKey] intValue];
    
    NSInteger theYear = [year intValue];
 
    NSDateFormatter *dateFormatter = [[NSDateFormatter new] autorelease];
    NSArray *months = [dateFormatter standaloneMonthSymbols];
    
    NSInteger index = [[eventInfo objectForKey:kMonthKey] intValue] - 1;
    
    month = [months objectAtIndex:index];
    
    NSString *time = @"";
    
//    if (![settings.timeFormat intValue])
//    {
//        if (hour >= 12)
//        {
//            time = @"PM";
//        }
//        else
//        {
//            time = @"AM";
//        }
//        
//        if (hour > 12)
//        {
//            hour -= 12;
//        }
//        
//        if (hour == 0)
//        {
//            hour = 12;
//        }
//    }
    
    return [NSString stringWithFormat:@"%@ %@ · %i · %02i:%02i %@", month, day, theYear, hour, minute, time];
}

#pragma mark - UITextFieldDelegate's methods

- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField
{
    if (self.datePopover.superview)
    {
        return NO;
    }
    
    if (self.addPhotoPopover.superview)
    {
        return NO;
    }
    
    if (self.recurrencePopover.superview)
    {
        return NO;
    }
    
    self.xButton.selected = NO;
    
    [self.datePopover removeFromSuperview];
    [self.addPhotoPopover removeFromSuperview];
    [self.recurrencePopover removeFromSuperview];
    
    if ([self.currentEvent.persistent boolValue])
    {
        if (!self.allowEdit)
        {
            [self showProtectedAlert];
            
            return NO;
        }
    }
    
    self.currentTextField = textField;
    
    return YES;
}

#pragma mark - AlertViewDelegate's methods

- (void)alertView:(AlertView *)alertView didDismissWithButtonIndex:(NSInteger)buttonIndex
{
    if ([alertView isKindOfClass:[DeleteAlertView class]])
    {
        if (alertView.tag == 669)
        {
            if (buttonIndex == 1)
            {
                [self deleteEvent];
                
                [self.inputDelegate commonEventInputViewControllerDidFinish:self];
            }
        }
    }
}


#pragma mark - BDGridViewDataSource's methods

- (CGSize)gridViewSizeOfCell:(BDGridView *)gridView
{
    if (IS_IPAD)
    {
        CGFloat width = 140;
        
        return CGSizeMake(width, width);
    }
    else
    {
        CGFloat width = (gridView.bounds.size.width - 3 * gridView.minimumPadding)/ 4;
        
        return CGSizeMake(width, width);
    }
    
    return CGSizeZero;
}

#pragma mark - BDGridViewDelegate's methods

- (BDGridCell *)gridView:(BDGridView *)gridView cellForIndex:(NSUInteger)index
{
    ALAsset *asset = [assets objectAtIndex:index];
    UIImage *pImage = [[UIImage imageWithCGImage:[asset aspectRatioThumbnail]] imageWithBorderWidth:0 andColor:[UIColor whiteColor].CGColor];
    
    PhotoCell *cell = (PhotoCell*)[gridView dequeueCell];
    
    if (!cell)
    {
        cell = [[[PhotoCell alloc] initCellWithRoundedCorners] autorelease];
    }
    
    cell.photoImageView.image = pImage;
    
    cell.layer.shadowOpacity = 0.f;
    cell.layer.cornerRadius = 20.f;
    cell.layer.shadowOffset = CGSizeZero;
    cell.photoImageView.layer.cornerRadius = 20.f;
    cell.layer.borderWidth = 1.f;
    cell.layer.borderColor = RGBCOLOR(150, 148, 186).CGColor;
    
    return cell;
}

- (void)gridView:(BDGridView *)gridView didTapCell:(BDGridCell *)cell
{
    if (cell.isSelected)
    {
        [gridView unselectAllCells];
        
        if (image)
        {
            [image release];
            image = nil;
        }
        
        [icon removeFromSuperview];
        
        icon = nil;
    }
    else
    {
        [gridView unselectAllCells];
        
        [gridView selectCell:cell];
        
        ALAsset *asset = [assets objectAtIndex:cell.index];
        
        if (image)
        {
            [image release];
            image = nil;
        }
        
        image = [UIImage imageWithCGImage:[[asset defaultRepresentation] fullScreenImage]];
        
        [image retain];
        
        [icon removeFromSuperview];
        
        CGRect iconRect;
        
        if (IS_IPAD)
        {
            iconRect = CGRectMake(710, 25, 150, 150);
        }
        else
        {
            iconRect = CGRectMake(self.view.bounds.size.width - 60, 100, 40 , 40);
        }
        
        icon = [[[GRPhotoPreview alloc] initWithImage:image frame:iconRect] autorelease];
        icon.autoresizingMask = UIViewAutoresizingFlexibleLeftMargin;
        icon.imageView.contentMode = UIViewContentModeScaleAspectFill;
        
        icon.layer.cornerRadius = 20.f;
        icon.layer.borderWidth = 1.f;
        icon.layer.borderColor = RGBCOLOR(128, 128, 128).CGColor;
        icon.layer.shadowRadius = 0.f;
        icon.layer.shadowColor = [UIColor clearColor].CGColor;
        icon.layer.shadowOpacity = 0.f;
        
        icon.imageView.layer.cornerRadius = 20.f;
        
        if (IS_IPAD)
        {
            [self.photoTableViewCell.contentView addSubview:icon];
        }
        else
        {
            [self.infoContainer addSubview:icon];
        }
    }
}

#pragma mark - UITableViewDataSource's methods

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (tableView == self.recurrenceTableView)
    {
        return 5;
    }
    
    return 5;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (tableView == self.recurrenceTableView)
    {
        RecurrenceTableViewCell *recurrenceTableViewCell = (RecurrenceTableViewCell*)[tableView dequeueReusableCellWithIdentifier:@"RecurrenceTableViewCellIdentifier"];
        
        switch (indexPath.row)
        {
            case ET_Daily:
                recurrenceTableViewCell.titleLabel.text = NSLocalizedString(@"Daily", @"Daily");
                break;
                
            case ET_Weekly:
                recurrenceTableViewCell.titleLabel.text = NSLocalizedString(@"Weekly", @"Weekly");
                break;
                
            case ET_Monthly:
                recurrenceTableViewCell.titleLabel.text = NSLocalizedString(@"Monthly", @"Monthly");
                break;
                
            case ET_Annually:
                recurrenceTableViewCell.titleLabel.text = NSLocalizedString(@"Annually", @"Annually");
                break;
                
            case ET_Once:
                recurrenceTableViewCell.titleLabel.text = NSLocalizedString(@"Don't repeat", @"Don't repeat");
                break;
        }
        
        if ([[eventInfo objectForKey:kEventTypeKey] intValue] == indexPath.row)
        {
            recurrenceTableViewCell.checkmarkImageView.hidden = NO;
            recurrenceTableViewCell.backgroundView = [[UIImageView alloc] initWithImage:[[UIImage imageNamed:@"calendar_selected_cell"] resizableImageWithCapInsets:UIEdgeInsetsMake(10, 10, 10, 10)]];
            recurrenceTableViewCell.titleLabel.textColor = [UIColor whiteColor];
        }
        else
        {
            recurrenceTableViewCell.checkmarkImageView.hidden = YES;
            recurrenceTableViewCell.backgroundView = nil;
            recurrenceTableViewCell.titleLabel.textColor = RGBCOLOR(151, 151, 151);
        }
        
        return recurrenceTableViewCell;
    }
    
    switch (indexPath.row)
    {
        case 0:
            return self.startDateTableViewCell;
            break;
            
        case 1:
            return self.titleTableViewCell;
            break;
            
        case 2:
            return self.repeatTableViewCell;
            break;
            
        case 3:
            return self.snoozeTableViewCell;
            break;
            
        case 4:
            return self.photoTableViewCell;
            break;
                                
        default:
            break;
    }
    
    return nil;
}

#pragma mark - UITableViewDelegate's methods

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    if (tableView == self.recurrenceTableView)
    {
        [self setTypeOfEvent:@(indexPath.row)];
        
        return ;
    }
    
    switch (indexPath.row)
    {
        case 4:
            [self photoButtonTapped];
            break;
            
        case 0:
            [self beginButtonTapped];
            break;
            
        case 2:
            [self recurrenceButtonTapped];
            break;
            
        default:
            break;
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (tableView == self.recurrenceTableView)
    {
        return 75;
    }
    
    if (indexPath.row == 4)
    {
        return 200.f;
    }
    
    return 80;
}

@end

