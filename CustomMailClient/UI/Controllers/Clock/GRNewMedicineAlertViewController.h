//
//  GRNewMedicineAlertViewController.h
//  GrannyApp
//
//  Created by Eugene Pavluk on 9/25/13.
//
//

#import "GRCommonEventInputViewController.h"
#import "AlertView.h"

@class GRDatePickerView;
@class GRTimePickerView;

@interface GRNewMedicineAlertViewController : GRCommonEventInputViewController

@property (nonatomic, retain) IBOutlet GRDatePickerView *monthDayPickerView;
@property (nonatomic, retain) IBOutlet GRTimePickerView *hourMinutePickerView;

@property (nonatomic, retain) IBOutlet UIButton *browseButton;
@property (nonatomic, retain) IBOutlet UIButton *pillButton;
@property (nonatomic, retain) IBOutlet UIButton *bottleButton;
@property (nonatomic, retain) IBOutlet UIButton *medicineChestButton;

- (IBAction)browseButtonTapped;
- (void)beginButtonTapped;
- (IBAction)okDateButtonTapped;
- (IBAction)reccurenceButtonTapped:(UIButton*)button;
- (IBAction)medicineButtonTapped:(UIButton*)sender;

@end
