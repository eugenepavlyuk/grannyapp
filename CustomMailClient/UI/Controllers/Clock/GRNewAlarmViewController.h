//
//  GRNewAlarmViewController.h
//  GrannyApp
//
//  Created by Eugene Pavlyuk on 7/16/12.
//  Copyright (c) 2012 Home. All rights reserved.
//

#import "GRCommonEventInputViewController.h"
#import "AlertView.h"

@class GRDatePickerView;
@class GRTimePickerView;
@class STSegmentedControl;

@interface GRNewAlarmViewController : GRCommonEventInputViewController

@property (nonatomic, retain) IBOutlet GRDatePickerView *monthDayPickerView;
@property (nonatomic, retain) IBOutlet GRTimePickerView *hourMinutePickerView;

@property (nonatomic, retain) IBOutlet UIView *timePickerContainer;

@property (nonatomic, retain) IBOutlet STSegmentedControl *delaySegmentControl;

@property (nonatomic, retain) IBOutlet BaseEventTableViewCell *snoozeTableViewCell;

- (void)photoButtonTapped;
- (void)beginButtonTapped;
- (IBAction)okDateButtonTapped;
- (IBAction)reccurenceButtonTapped:(UIButton*)button;

- (IBAction)delaySegmentValueChanged;

@end
