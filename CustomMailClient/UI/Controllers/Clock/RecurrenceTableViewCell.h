//
//  RecurrenceTableViewCell.h
//  GrannyApp
//
//  Created by Eugene Pavluk on 12/13/13.
//
//

#import <UIKit/UIKit.h>

@interface RecurrenceTableViewCell : UITableViewCell

@property (nonatomic, retain) IBOutlet UILabel *titleLabel;
@property (nonatomic, retain) IBOutlet UIImageView *checkmarkImageView;

@end
