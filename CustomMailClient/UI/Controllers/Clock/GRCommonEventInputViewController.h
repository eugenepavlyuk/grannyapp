//
//  GRCommonEventInputViewController.h
//  GrannyApp
//
//  Created by Eugene Pavluk on 12/3/13.
//
//

#import "GAITrackedViewController.h"
#import "GREventInputDelegate.h"
#import "BDGridView.h"

@class EventBase;
@class Settings;
@class BaseEventTableViewCell;
@class GRPhotoPreview;
@class WYPopoverController;

@interface GRCommonEventInputViewController : GAITrackedViewController <BDGridViewDataSource, BDGridViewDelegate, UITableViewDataSource, UITableViewDelegate>
{
    UITextField *currentTextField;
    
    Settings *settings;
    
    BOOL somethingChanged;
    
    NSMutableDictionary *eventInfo;
    
    NSMutableArray *assets;
    
    UIImage     *image;
    GRPhotoPreview *icon;
}

@property (nonatomic, retain) EventBase *currentEvent;

@property (nonatomic, retain) WYPopoverController *alertPopoverController;

@property (nonatomic, retain) NSMutableDictionary *eventInfo;
@property (nonatomic, assign) id<GREventInputDelegate> inputDelegate;
@property (nonatomic, retain) UITextField *currentTextField;
@property (nonatomic, retain) Settings *settings;

@property (nonatomic, assign) BOOL allowEdit;

@property (nonatomic, retain) IBOutlet BDGridView *photoContentView;
@property (nonatomic, retain) IBOutlet UIView *infoContainer;
@property (nonatomic, retain) IBOutlet UIView *headerView;
@property (nonatomic, assign) IBOutlet UIImageView *headerImageView;
@property (nonatomic, retain) IBOutlet UIView *addPhotoPopover;
@property (nonatomic, retain) IBOutlet UIView *datePopover;
@property (nonatomic, retain) IBOutlet UIView *recurrencePopover;

@property (nonatomic, retain) IBOutlet UITableView *mainTableView;
@property (nonatomic, retain) IBOutlet UITableView *recurrenceTableView;
@property (nonatomic, retain) IBOutlet UITextField *titleTextField;
@property (nonatomic, retain) IBOutlet UILabel *dateStartLabel;
@property (nonatomic, retain) IBOutlet UILabel *titleLabel;
@property (nonatomic, retain) IBOutlet UIView *dateContainerView;
@property (nonatomic, retain) IBOutlet UILabel *recurrenceLabel;
@property (nonatomic, retain) IBOutlet UILabel *dateDialogTitleLabel;

@property (nonatomic, retain) IBOutlet UIButton *xButton;
@property (nonatomic, retain) IBOutlet UIButton *editButton;
@property (nonatomic, retain) IBOutlet UIButton *deleteButton;
@property (nonatomic, retain) IBOutlet UIButton *okButton;

@property (nonatomic, retain) IBOutlet BaseEventTableViewCell *titleTableViewCell;

@property (nonatomic, retain) IBOutlet BaseEventTableViewCell *startDateTableViewCell;

@property (nonatomic, retain) IBOutlet BaseEventTableViewCell *photoTableViewCell;
@property (nonatomic, retain) IBOutlet BaseEventTableViewCell *repeatTableViewCell;

- (void)setNameOfEvent:(NSString*)name;

- (void)releaseViews;

- (IBAction)xButtonTapped;
- (IBAction)okButtonTapped;
- (IBAction)saveButtonTapped;
- (IBAction)deleteButtonTapped;
- (IBAction)editButtonTapped;
- (IBAction)okRecurrenceButtonTapped;
- (void)recurrenceButtonTapped;
- (void)photoButtonTapped;

@end
