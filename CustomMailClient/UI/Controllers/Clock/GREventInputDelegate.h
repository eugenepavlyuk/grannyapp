//
//  GREventInputDelegate.h
//  GrannyApp
//
//  Created by Eugene Pavluk on 12/3/13.
//
//

#import <Foundation/Foundation.h>

@class GRCommonEventInputViewController;

@protocol GREventInputDelegate <NSObject>

- (void)commonEventInputViewControllerDidFinish:(GRCommonEventInputViewController*)commonEventInputViewController;

@end
