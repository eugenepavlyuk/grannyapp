//
//  GRAlarmSetupViewController.h
//  Granny
//
//  Created by Eugene Pavlyuk on 22.03.12.
//  Copyright 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "GRPickerView.h"

@class Clock;
@class Settings;

@interface GRAlarmSetupViewController : UIViewController <AFPickerViewDataSource, AFPickerViewDelegate>
{
    GRPickerView *leftPickerView;
    GRPickerView *centerPickerView;
    GRPickerView *rightPickerView;
    
    Clock *clock;
    Settings *settings;
}

@property (nonatomic, retain) IBOutlet UILabel *whatTimeLabel;

@property (nonatomic, retain) IBOutlet UIView *containerView;
@property (nonatomic, retain) IBOutlet UIView *pickersContainerView;

@property (nonatomic, retain) IBOutlet UIImageView *topShelfImageView;

@property (nonatomic, retain) IBOutlet UIButton *turnButton;

@property (nonatomic, retain) IBOutlet UIImageView *footerImageView;
@property (nonatomic, retain) IBOutlet UIImageView *headerImageView;

- (id)initWithNib;

- (IBAction)backButtonTapped;
- (IBAction)saveButtonTapped;
- (IBAction)homeButtonTapped;
- (IBAction)settingsButtonTapped;

@end
