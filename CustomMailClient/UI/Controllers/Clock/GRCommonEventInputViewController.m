//
//  GRCommonEventInputViewController.m
//  GrannyApp
//
//  Created by Eugene Pavluk on 12/3/13.
//
//

#import "GRCommonEventInputViewController.h"
#import "DataManager.h"
#import "Settings.h"
#import "BDGridCell.h"
#import "UIImage+Border.h"
#import "GRPhotoPreview.h"
#import "BaseEventTableViewCell.h"
#import "Event.h"
#import "WYPopoverController.h"

@interface GRCommonEventInputViewController () <UITextFieldDelegate>
{
    ALAssetsLibrary *library;
}

@end

@implementation GRCommonEventInputViewController

@synthesize currentTextField;
@synthesize settings;
@synthesize eventInfo;
@synthesize allowEdit;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    
    if (self)
    {
        eventInfo = [[NSMutableDictionary dictionary] retain];
        somethingChanged = NO;
    }
    
    return self;
}

- (BOOL)prefersStatusBarHidden
{
    return YES;
}

- (void)startLoadingPhotoFromGallery
{
    //#if TARGET_IPHONE_SIMULATOR
    //#else
    void (^assetEnumerator)(ALAsset *, NSUInteger, BOOL *) = ^(ALAsset *result, NSUInteger index, BOOL *stop) {
        if(result != NULL) {
            //            NSLog(@"See Asset: %@", result);
            [assets addObject:result];
        }
    };
    
    void (^assetGroupEnumerator)(ALAssetsGroup *, BOOL *) =  ^(ALAssetsGroup *group, BOOL *stop) {
    	if(group != nil) {
            [group enumerateAssetsUsingBlock:assetEnumerator];
        } else {
            [self.photoContentView reloadData];
        }
    };
    
    assets = [[NSMutableArray alloc] init];
    library = [[ALAssetsLibrary alloc] init];
    [library enumerateGroupsWithTypes:ALAssetsGroupSavedPhotos
                           usingBlock:assetGroupEnumerator
                         failureBlock: ^(NSError *error) {
                             NSLog(@"Failure");
                         }];
    //#endif
    
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    
    self.photoContentView.minimumPadding = 5.f;
    
    [self startLoadingPhotoFromGallery];
    
    [self.recurrenceTableView registerNib:[UINib nibWithNibName:@"RecurrenceTableViewCell" bundle:[NSBundle mainBundle]] forCellReuseIdentifier:@"RecurrenceTableViewCellIdentifier"];
}

- (void)releaseViews
{
    self.datePopover = nil;
    self.dateContainerView = nil;
    self.currentTextField = nil;
    self.titleTextField = nil;
    self.mainTableView = nil;
    self.infoContainer = nil;
    self.dateStartLabel = nil;
    self.photoContentView = nil;
    self.recurrenceLabel = nil;
    self.dateDialogTitleLabel = nil;
    self.titleLabel = nil;
    
    self.editButton = nil;
    self.deleteButton = nil;
    self.xButton = nil;
    self.okButton = nil;
    
    self.addPhotoPopover = nil;
    self.recurrencePopover = nil;
    
    self.titleTableViewCell = nil;
    self.startDateTableViewCell = nil;
    self.photoTableViewCell = nil;
}

- (Settings*)settings
{
    if (!settings)
    {
        self.settings = [[DataManager sharedInstance] getEntityWithName:kSettingsEntityName];
    }
    
    return settings;
}

- (void)setNameOfEvent:(NSString*)name
{
    if (name)
    {
        self.titleTextField.text = name;
        
        [eventInfo setObject:name forKey:kNameKey];
    }
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    
    [self releaseViews];
}

- (void)dealloc
{
    self.currentEvent = nil;
    
    [self releaseViews];
    
    [assets release];
    
    self.settings = nil;
    self.eventInfo = nil;
    
    if (image)
    {
        [image release];
        image = nil;
    }
    
    [library release];
    
    [super dealloc];
}

- (void)recurrenceButtonTapped
{
    
}

- (IBAction)saveButtonTapped
{
    [self.currentTextField resignFirstResponder];
    
    self.xButton.selected = NO;
}

- (IBAction)xButtonTapped
{
    [self.currentTextField resignFirstResponder];
    self.currentTextField = nil;
    
    self.xButton.selected = NO;
    
    [self.addPhotoPopover removeFromSuperview];
    
    [self.inputDelegate commonEventInputViewControllerDidFinish:self];
}

- (void)photoButtonTapped
{

}

- (IBAction)okButtonTapped
{
    [self.addPhotoPopover removeFromSuperview];
}

- (IBAction)okRecurrenceButtonTapped
{
    [self.recurrencePopover removeFromSuperview];
}

- (IBAction)editButtonTapped
{
    
}

- (IBAction)deleteButtonTapped
{
    
}

#pragma mark - UITableViewDelegate's methods

- (void)tableView:(UITableView *)tableView
  willDisplayCell:(UITableViewCell *)cell
forRowAtIndexPath:(NSIndexPath *)indexPath
{
    [cell setBackgroundColor:[UIColor clearColor]];
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    if (tableView == self.recurrenceTableView)
    {
        return nil;
    }
    
    return self.headerView;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    if (tableView == self.recurrenceTableView)
    {
        return 0;
    }
    
    return 80;
}

#pragma mark - UITableViewDataSource's methods

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 0;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return nil;
}

#pragma mark - BDGridViewDataSource's methods

- (NSUInteger)gridViewCountOfCells:(BDGridView *)gridView
{
    return [assets count];
}

- (CGSize)gridViewSizeOfCell:(BDGridView *)gridView
{
    if (IS_IPAD)
    {
        CGFloat width = 140;
        
        return CGSizeMake(width, width);
    }
    else
    {
        CGFloat width = (gridView.bounds.size.width - 3 * gridView.minimumPadding)/ 4;
        
        return CGSizeMake(width, width);
    }
    
    return CGSizeZero;
}

- (BDGridCell *)gridView:(BDGridView *)gridView cellForIndex:(NSUInteger)index
{
    ALAsset *asset = [assets objectAtIndex:index];
    UIImage *pImage = [[UIImage imageWithCGImage:[asset aspectRatioThumbnail]] imageWithBorderWidth:0 andColor:[UIColor whiteColor].CGColor];
    
    PhotoCell *cell = (PhotoCell*)[gridView dequeueCell];
    
    if (!cell)
    {
        cell = [[[PhotoCell alloc] initCellWithRoundedCorners] autorelease];
    }
    
    cell.photoImageView.image = pImage;
    
    return cell;
}

#pragma mark - BDGridViewDelegate's methods

- (void)gridView:(BDGridView *)gridView didTapCell:(BDGridCell *)cell
{
    
}

#pragma mark - UITextFieldDelegate's methods

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    somethingChanged = YES;
    
    return YES;
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [self.currentTextField resignFirstResponder];
    return YES;
}

#pragma mark - WYPopoverControllerDelegate's methods

- (BOOL)popoverControllerShouldDismissPopover:(WYPopoverController *)popoverController
{
    return YES;
}

- (void)popoverControllerDidDismissPopover:(WYPopoverController *)popoverController
{
    self.alertPopoverController = nil;
}

@end
