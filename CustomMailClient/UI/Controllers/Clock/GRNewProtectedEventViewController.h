//
//  GRNewProtectedEventViewController.h
//  GrannyApp
//
//  Created by Eugene Pavlyuk on 7/16/12.
//  Copyright (c) 2012 Home. All rights reserved.
//

#import "GRCommonEventInputViewController.h"
#import "AlertView.h"

@class GRDatePickerView;
@class GRTimePickerView;

@interface GRNewProtectedEventViewController : GRCommonEventInputViewController

@property (nonatomic, retain) IBOutlet UIImageView *datePopoverImageView;

@property (nonatomic, retain) IBOutlet UILabel *dateEndLabel;

@property (nonatomic, retain) IBOutlet GRDatePickerView *monthDayPickerView;
@property (nonatomic, retain) IBOutlet GRTimePickerView *hourMinutePickerView;

@property (nonatomic, retain) IBOutlet UIView *timePickerContainer;

@property (nonatomic, retain) IBOutlet BaseEventTableViewCell *endDateTableViewCell;
@property (nonatomic, retain) IBOutlet BaseEventTableViewCell *snoozeTableViewCell;

@property (nonatomic, retain) IBOutlet UIButton *onButton;
@property (nonatomic, retain) IBOutlet UIButton *offButton;

- (void)photoButtonTapped;
- (void)beginButtonTapped;
- (void)endButtonTapped;
- (IBAction)okDateButtonTapped;
- (IBAction)reccurenceButtonTapped:(UIButton*)button;

- (IBAction)delaySegmentValueChanged:(UIButton*)button;

@end
