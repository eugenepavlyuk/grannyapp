//
//  GRGamesController.m
//  GrannyApp
//
//  Created by Ievgen Pavliuk on 5/14/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "GRGamesController.h"
#import "SolitaireViewController.h"
#import <QuartzCore/QuartzCore.h>


#define LEFT_PADDING 10

@implementation GRPreviewButton

- (void)layoutSubviews
{
    [super layoutSubviews];
    
    if (!self.imageView.image)
    {
        self.titleLabel.frame = self.bounds;
    }
    else
    {
        self.titleLabel.frame = CGRectMake(0, self.bounds.size.height * 0.6f, self.bounds.size.width, self.bounds.size.height * 0.4f);
        
        if (IS_IPAD)
        {
            self.imageView.frame = CGRectMake(0, 0, 125, 100);
            self.imageView.center = CGPointMake(self.bounds.size.width / 2, 80);
        }
        else
        {
            self.imageView.frame = CGRectMake(0, 0, self.bounds.size.width, self.bounds.size.height * 0.6f);
        }
    }
}

@end

@implementation GRGamePreview

@synthesize index;
@synthesize delegate;
@synthesize previewButton;

- (id)initWithFrame:(CGRect)frame
{
    if ((self = [super initWithFrame:frame])) 
    {
        self.userInteractionEnabled = YES;
        self.clipsToBounds = NO;
        
        self.backgroundColor = [UIColor whiteColor];
        
        self.previewButton = [GRPreviewButton buttonWithType:UIButtonTypeCustom];
        self.previewButton.frame = self.bounds;
        self.previewButton.userInteractionEnabled = NO;
        self.previewButton.clipsToBounds = YES;
        self.previewButton.layer.cornerRadius = 25.f;
        self.previewButton.titleLabel.numberOfLines = 2.f;
        self.previewButton.titleLabel.font = [UIFont fontWithName:@"Helvetica-Bold" size:30];
        self.previewButton.imageView.contentMode = UIViewContentModeScaleAspectFit;
        self.previewButton.titleLabel.textAlignment = NSTextAlignmentCenter;
        
        [self addSubview:self.previewButton];
        
        self.layer.cornerRadius = 25.f;
        self.layer.borderWidth = 1.f;
        self.layer.borderColor = RGBCOLOR(54, 66, 158).CGColor;
        self.layer.shadowOpacity = 0.4f;
        self.layer.shadowColor = [UIColor blackColor].CGColor;
        self.layer.shadowRadius = 5.f;
        self.layer.shadowOffset = CGSizeMake(2, 0);
    }
    
    return self;
}

- (void)dealloc 
{
    delegate = nil;
    self.previewButton = nil;
    [super dealloc];
}

- (void)layoutSubviews
{
    [super layoutSubviews];
    
    self.previewButton.frame = self.bounds;
}

#pragma mark - UserInteraction

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    self.backgroundColor = RGBCOLOR(245, 145, 30);
    self.previewButton.selected = YES;
}

- (void)touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event 
{
    self.backgroundColor = [UIColor clearColor];
    self.previewButton.selected = NO;
    
    if ([delegate respondsToSelector:@selector(gamePreviewSelectedWithIndex:)])
    {
        [delegate performSelector:@selector(gamePreviewSelectedWithIndex:) withObject:[NSNumber numberWithInt:self.index]];
    }
}

- (void)touchesCancelled:(NSSet *)touches withEvent:(UIEvent *)event
{
    self.backgroundColor = [UIColor clearColor];
    self.previewButton.selected = NO;
}

@end


@interface GRGamesController ()

@end

@implementation GRGamesController

@synthesize gamesContentView;
@synthesize headerImageView;


- (BOOL)prefersStatusBarHidden
{
    return YES;
}

- (void)dealloc 
{
    [gamesContentView release];
    [tableData release];
    [gameTitles release];
    
    self.headerImageView = nil;
    
    [super dealloc];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.screenName = @"Games Screen";
    
    if (!tableData)
    {
        tableData = [[NSMutableArray alloc] init];
        gameTitles = [[NSMutableArray alloc] init];
        
        [tableData addObject:@"games_salitaire"];
        [gameTitles addObject:NSLocalizedString(@"Solitaire", @"Solitaire")];
        
        NSInteger numberBlocks = (IS_IPAD) ? 7 : 4;
        
        for (int i = 0; i < numberBlocks; i++)
        {
            [tableData addObject:@""];
            [gameTitles addObject:@""];
        }
    }
    
    UIImage *buttonBackground = nil;
    
    if (IS_IPAD)
    {
        buttonBackground = [[UIImage imageNamed:@"games_header"] resizableImageWithCapInsets:UIEdgeInsetsMake(0, 20, 0, 20)];
    }
    else
    {
        buttonBackground = [[UIImage imageNamed:@"games_header"] stretchableImageWithLeftCapWidth:60 topCapHeight:0];
    }
    
    self.headerImageView.image = buttonBackground;
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    
    self.gamesContentView = nil;
    self.headerImageView = nil;
}

- (void)viewWillAppear:(BOOL)animated 
{
    [super viewWillAppear:animated];
    
    [self.navigationController setNavigationBarHidden:YES animated:NO];
    
    [self adjustToInterfaceRotation:[[UIApplication sharedApplication] statusBarOrientation]];
}

- (void)adjustToInterfaceRotation:(UIInterfaceOrientation)orientation 
{
    if(orientation == UIInterfaceOrientationLandscapeLeft ||orientation == UIInterfaceOrientationLandscapeRight) 
    {
        if (IS_IPAD) 
        {
            itemsAtRow = 4;
            //            superWidth = 1024;
            //            superHeight = 768;
        } 
        else 
        {
            itemsAtRow = 6;
            //            superWidth = 480;
            //            superHeight = 267;
        }
    } 
    else 
    {
        if (IS_IPAD) 
        {
            itemsAtRow = 3;
            //            superWidth = 768;
            //            superHeight = 960;
        } 
        else 
        {
            itemsAtRow = 4;
            //            superWidth = 320;
            //            superHeight = 416;
        }
    }
    
//    self.gamesContentView.frame = CGRectMake(self.gamesContentView.frame.origin.x, self.gamesContentView.frame.origin.y, self.view.bounds.size.width, self.view.bounds.size.height - self.headerImageView.bounds.size.height);
    
    [self gamesLoaded];
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return UIInterfaceOrientationIsLandscape(interfaceOrientation);;
}

- (void)willRotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)duration 
{
    //[self adjustToInterfaceRotation:toInterfaceOrientation];
}

- (void)didRotateFromInterfaceOrientation:(UIInterfaceOrientation)fromInterfaceOrientation
{
    [self adjustToInterfaceRotation:[[UIApplication sharedApplication] statusBarOrientation]];
}

#pragma mark - GRGamePreviewDelegate

- (void)gamePreviewSelectedWithIndex:(NSNumber*)index 
{
    if ([index intValue] == 0)
    {
        SolitaireViewController *sController = [[SolitaireViewController alloc] init];
        
        [self.navigationController pushViewController:sController animated:YES];
        [sController release];
    }
}

- (void)gamesLoaded
{
    [gamesContentView.subviews makeObjectsPerformSelector:@selector(removeFromSuperview)];
    
    CGFloat itemSize = (self.gamesContentView.frame.size.width - LEFT_PADDING * itemsAtRow - (itemsAtRow - 1) * 20) / itemsAtRow;
    
    NSInteger numberRows = ([tableData count] + itemsAtRow) / itemsAtRow;
    
    CGFloat contentSizeHeight = (LEFT_PADDING + itemSize) * numberRows;
    
    self.gamesContentView.contentSize = CGSizeMake(self.gamesContentView.frame.size.width,  contentSizeHeight);
    
    for (int i = 0; i < numberRows; i++)
    {
        for (int j = 0; j < itemsAtRow; j++)
        {
            NSInteger index = i * itemsAtRow + j;
            
            if (index >= [tableData count])
            {
                break;
            }
            
            CGFloat posX = 0.0f;
            CGFloat posY = 0.0f;
            
            posY = (LEFT_PADDING + itemSize + 20) * i;
            
            posX = (LEFT_PADDING + itemSize + 20) * j;
            
            NSString *asset = [tableData objectAtIndex:index];
            NSString *title = [gameTitles objectAtIndex:index];
            
            GRGamePreview *preview;
            
            preview = [[GRGamePreview alloc] initWithFrame:CGRectMake(posX, posY, itemSize, itemSize)];
            preview.index = index;
            
            if ([asset length])
            {
                UIImage *pImage = [UIImage imageNamed:asset];
                
                NSString *highlightedString = [NSString stringWithFormat:@"%@_h", asset];
                
                UIImage *pImageH = [UIImage imageNamed:highlightedString];
                
                [preview.previewButton setImage:pImage forState:UIControlStateNormal];
                [preview.previewButton setImage:pImageH forState:UIControlStateSelected];
                [preview.previewButton setTitle:title forState:UIControlStateNormal];
                [preview.previewButton setTitleColor:RGBCOLOR(253, 163, 54) forState:UIControlStateNormal];
                [preview.previewButton setTitleColor:RGBCOLOR(255, 255, 255) forState:UIControlStateSelected];
            }
            else
            {
                [preview.previewButton setImage:nil forState:UIControlStateNormal];
                [preview.previewButton setTitle:NSLocalizedString(@"Coming Soon", @"Coming Soon") forState:UIControlStateNormal];
                [preview.previewButton setTitleColor:RGBCOLOR(204, 204, 204) forState:UIControlStateNormal];
                [preview.previewButton setTitleColor:RGBCOLOR(255, 255, 255) forState:UIControlStateSelected];
                
                preview.userInteractionEnabled = NO;
            }
            
            preview.delegate = self;
            [self.gamesContentView addSubview:preview];
            [preview release];
            preview = nil;
        }
    }
}

- (IBAction)homeButtonTapped
{
    [self.navigationController popToRootViewControllerAnimated:YES];
}

- (IBAction)backButtonTapped
{
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)nextButtonTapped
{	    
    
}

@end
