//
//  GRGamesController.h
//  GrannyApp
//
//  Created by Ievgen Pavliuk on 5/14/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol GRGamePreviewDelegate <NSObject>

- (void)gamePreviewSelectedWithIndex:(NSNumber*)index;

@end


@interface GRPreviewButton : UIButton

@end


@interface GRGamePreview : UIView 
{    
    NSInteger index;
    id<GRGamePreviewDelegate> delegate;
}

@property (nonatomic, assign) NSInteger index;
@property (nonatomic, assign) id<GRGamePreviewDelegate> delegate;
@property (nonatomic, retain) GRPreviewButton *previewButton;

@end



@interface GRGamesController : GAITrackedViewController <UIScrollViewDelegate, GRGamePreviewDelegate> 
{
    UIScrollView *gamesContentView;
    NSMutableArray  *tableData;
    NSMutableArray  *gameTitles;
    
    NSInteger         itemsAtRow;
}

@property (nonatomic, retain) IBOutlet UIScrollView *gamesContentView;
@property (nonatomic, retain) IBOutlet UIImageView *headerImageView;

@end
