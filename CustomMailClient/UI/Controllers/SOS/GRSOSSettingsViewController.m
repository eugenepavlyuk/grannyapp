//
//  GRSOSSettingsViewController.m
//  GrannyApp
//
//  Created by Eugene Pavluk on 12/27/12.
//
//

#import "GRSOSSettingsViewController.h"
#import "Settings.h"
#import "DataManager.h"
#import "BDGridCell.h"
#import "UIImage+Border.h"

@implementation PhotoButton

- (void)awakeFromNib
{
    [super awakeFromNib];
    
    self.clipsToBounds = YES;
    self.layer.cornerRadius = 15.f;
}

- (void)layoutSubviews
{
    [super layoutSubviews];
    
    self.imageView.frame = CGRectInset(self.bounds, 25, 25);
    //    self.imageView.center = CGPointMake(self.bounds.size.width/2, self.imageView.center.y + 20);
    self.imageView.clipsToBounds = YES;
    self.imageView.contentMode = UIViewContentModeScaleAspectFill;
}

@end

@interface GRSOSSettingsViewController ()

@end

#define LEFT_PADDING 10.0f

@implementation GRSOSSettingsViewController
{
    Settings *settings;
    
    CGFloat           superWidth;
    CGFloat           superHeight;
    
    CGFloat           thumbSize;
    BOOL              assetsLoaded;
    
    NSInteger         itemsAtRow;
    
    BOOL isHome;
    BOOL isDoctor;
    BOOL isHospital;
    BOOL isContact;
}

@synthesize doctorButton;
@synthesize contactButton;
@synthesize hospitalButton;
@synthesize homeButton;
@synthesize photoButton;

@synthesize addPhotoPopover;
@synthesize photoContentView;

@synthesize doctorButtonLabel;
@synthesize homeButtonLabel;
@synthesize contactButtonLabel;

@synthesize nameTextField;
@synthesize numberTextField;

- (BOOL)prefersStatusBarHidden
{
    return YES;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    
    self.screenName = @"SOS Settings Screen";
    
    if (!settings)
    {
        settings = [[[DataManager sharedInstance] getEntityWithName:kSettingsEntityName] retain];
    }
    
    UIButton *nameButton = [UIButton buttonWithType:UIButtonTypeCustom];
    nameButton.frame = CGRectMake(0, 0, 207, self.nameTextField.bounds.size.height);
    [nameButton addTarget:self action:@selector(activateNameTextField) forControlEvents:UIControlEventTouchUpInside];
    
    self.nameTextField.leftView = nameButton;
    self.nameTextField.leftViewMode = UITextFieldViewModeAlways;
    
    UIButton *numberButton = [UIButton buttonWithType:UIButtonTypeCustom];
    numberButton.frame = CGRectMake(0, 0, 207, self.numberTextField.bounds.size.height);
    [numberButton addTarget:self action:@selector(activateNumberTextField) forControlEvents:UIControlEventTouchUpInside];
    
    self.numberTextField.leftView = numberButton;
    self.numberTextField.leftViewMode = UITextFieldViewModeAlways;
    
    self.photoContentView.minimumPadding = 30.f;
    
    self.doctorButtonLabel.text = settings.doctor_name.length ? settings.doctor_name : NSLocalizedString(@"Add doctor", @"Add doctor");
    self.homeButtonLabel.text = settings.home_name.length ? settings.home_name : NSLocalizedString(@"Add home", @"Add home");
    self.contactButtonLabel.text = settings.contact_name.length ? settings.contact_name : NSLocalizedString(@"Add contact", @"Add contact");
    self.hospitalButtonLabel.text = settings.hospital_name.length ? settings.hospital_name : NSLocalizedString(@"Add hospital", @"Add hospital");
    
    self.contactButton.selected = settings.contact_name.length > 0;
    
    self.doctorButton.selected = settings.doctor_name.length > 0;
    
    self.homeButton.selected = settings.home_name.length > 0;
    
    self.hospitalButton.selected = settings.hospital_name.length > 0;
    
    self.nameTextField.text = settings.doctor_name;
    self.numberTextField.text = settings.doctor_phone;
    
    isDoctor = YES;
    [self setIconOfContact:@"home"];
    [self setIconOfContact:@"contact"];
    [self setIconOfContact:@"hospital"];
    [self setIconOfContact:@"doctor"];
    
//    [self adjustToInterfaceRotation:[[UIApplication sharedApplication] statusBarOrientation]];
    [self startLoadingPhotoFromGallery];
}

- (void)activateNameTextField
{
    [self.nameTextField becomeFirstResponder];
}

- (void)activateNumberTextField
{
    [self.numberTextField becomeFirstResponder];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    
    [[DataManager sharedInstance] save];
}

- (void)releaseViews
{
    self.doctorButton = nil;
    self.contactButton = nil;
    self.homeButton = nil;
    self.photoButton = nil;
    self.hospitalButton = nil;
    
    self.nameTextField = nil;
    self.numberTextField = nil;
    
    self.contactButtonLabel = nil;
    self.homeButtonLabel = nil;
    self.doctorButtonLabel = nil;
    self.hospitalButtonLabel = nil;
    
    self.addPhotoPopover = nil;
    self.photoContentView = nil;
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    
    [self releaseViews];
}

- (void)dealloc
{
    [self releaseViews];
    [settings release];
    
    [assets release];
    
    [super dealloc];
}

- (void)setIconOfContact:(NSString*)name
{
    NSString *iconName = [name stringByAppendingPathExtension:@"png"];
    
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES);
    NSString *dirPath = [paths objectAtIndex:0];
    
    dirPath = [dirPath stringByAppendingPathComponent:iconName];
    
    UIImage *image = [UIImage imageWithContentsOfFile:dirPath];
    
    [icon removeFromSuperview];
    
    icon = nil;
    
    if (image)
    {
        icon = [[[GRPhotoPreview alloc] initWithImage:image frame:self.photoButton.frame] autorelease];
        icon.clipsToBounds = YES;
        icon.imageView.contentMode = UIViewContentModeScaleAspectFill;
        icon.autoresizingMask = UIViewAutoresizingFlexibleLeftMargin | UIViewAutoresizingFlexibleBottomMargin | UIViewAutoresizingFlexibleTopMargin;
        
        [self.photoButton.superview insertSubview:icon aboveSubview:self.photoButton];
        
        [self.photoButton setImage:nil forState:UIControlStateNormal];
        
        if ([name isEqualToString:@"home"])
        {
            [self.homeButton setImage:image forState:UIControlStateNormal];
            self.homeButton.imageView.contentMode = UIViewContentModeScaleAspectFill;
        }
        else if ([name isEqualToString:@"doctor"])
        {
            [self.doctorButton setImage:image forState:UIControlStateNormal];
            self.doctorButton.imageView.contentMode = UIViewContentModeScaleAspectFill;
        }
        else if ([name isEqualToString:@"contact"])
        {
            [self.contactButton setImage:image forState:UIControlStateNormal];
            self.contactButton.imageView.contentMode = UIViewContentModeScaleAspectFill;
        }
        else if ([name isEqualToString:@"hospital"])
        {
            [self.hospitalButton setImage:image forState:UIControlStateNormal];
            self.hospitalButton.imageView.contentMode = UIViewContentModeScaleAspectFill;
        }
    }
    else
    {
        if ([name isEqualToString:@"home"])
        {
            if (isHome)
            {
                [self.photoButton setImage:[UIImage imageNamed:@"sos_home"] forState:UIControlStateNormal];
            }
            
            [self.homeButton setImage:[UIImage imageNamed:@"sos_home"] forState:UIControlStateNormal];
            
            self.homeButton.imageView.contentMode = UIViewContentModeCenter;
        }
        else if ([name isEqualToString:@"doctor"])
        {
            if (isDoctor)
            {
                [self.photoButton setImage:[UIImage imageNamed:@"sos_doctor"] forState:UIControlStateNormal];
            }
            
            [self.doctorButton setImage:[UIImage imageNamed:@"sos_doctor"] forState:UIControlStateNormal];
            
            self.doctorButton.imageView.contentMode = UIViewContentModeCenter;
        }
        else if ([name isEqualToString:@"contact"])
        {
            if (isContact)
            {
                [self.photoButton setImage:[UIImage imageNamed:@"sos_contact"] forState:UIControlStateNormal];
            }
            
            [self.contactButton setImage:[UIImage imageNamed:@"sos_contact"] forState:UIControlStateNormal];
            
            self.contactButton.imageView.contentMode = UIViewContentModeCenter;
        }
        else if ([name isEqualToString:@"hospital"])
        {
            if (isHospital)
            {
                [self.photoButton setImage:[UIImage imageNamed:@"sos_hospital"] forState:UIControlStateNormal];
            }
            
            [self.hospitalButton setImage:[UIImage imageNamed:@"sos_hospital"] forState:UIControlStateNormal];
            
            self.hospitalButton.imageView.contentMode = UIViewContentModeCenter;
        }
    }
}

//- (void)photoPreviewSelectedWithIndex:(NSInteger)index
//{
//    ALAsset *asset = [assets objectAtIndex:index];
//    
//    UIImage *image = [UIImage imageWithCGImage:[asset aspectRatioThumbnail]];
//    
//    if (image)
//    {
//        NSString *iconName = nil;
//        
//        if (isDoctor)
//        {
//            iconName = [@"doctor" stringByAppendingPathExtension:@"png"];
//        }
//        else if (isHome)
//        {
//            iconName = [@"home" stringByAppendingPathExtension:@"png"];
//        }
//        else if (isContact)
//        {
//            iconName = [@"contact" stringByAppendingPathExtension:@"png"];
//        }
//        else if (isHospital)
//        {
//            iconName = [@"hospital" stringByAppendingPathExtension:@"png"];
//        }
//        
//        NSArray *paths = NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES);
//        NSString *dirPath = [paths objectAtIndex:0];
//        
//        dirPath = [dirPath stringByAppendingPathComponent:iconName];
//        
//        NSData *data = UIImagePNGRepresentation(image);
//        
//        [[NSFileManager defaultManager] createFileAtPath:dirPath contents:data attributes:nil];
//    }
//    
//    NSString *iconName = nil;
//    
//    if (isDoctor)
//    {
//        iconName = @"doctor";
//    }
//    else if (isHome)
//    {
//        iconName = @"home";
//    }
//    else if (isContact)
//    {
//        iconName = @"contact";
//    }
//    else if (isHospital)
//    {
//        iconName = @"hospital";
//    }
//    
//    [self setIconOfContact:iconName];
//    
//    [self.addPhotoPopover removeFromSuperview];
//}

- (void)startLoadingPhotoFromGallery
{
    //#if TARGET_IPHONE_SIMULATOR
    //#else
    void (^assetEnumerator)(ALAsset *, NSUInteger, BOOL *) = ^(ALAsset *result, NSUInteger index, BOOL *stop) {
        if(result != NULL) {
            //            NSLog(@"See Asset: %@", result);
            [assets addObject:result];
        }
    };
    
    void (^assetGroupEnumerator)(ALAssetsGroup *, BOOL *) =  ^(ALAssetsGroup *group, BOOL *stop) {
    	if(group != nil) {
            [group enumerateAssetsUsingBlock:assetEnumerator];
        } else {
            [self.photoContentView reloadData];
        }
    };
    
    assets = [[NSMutableArray alloc] init];
    library = [[ALAssetsLibrary alloc] init];
    [library enumerateGroupsWithTypes:ALAssetsGroupSavedPhotos
                           usingBlock:assetGroupEnumerator
                         failureBlock: ^(NSError *error) {
                             NSLog(@"Failure");
                         }];
    //#endif
    
}

#pragma mark - BDGridViewDataSource's methods

- (NSUInteger)gridViewCountOfCells:(BDGridView *)gridView
{
    return [assets count];
}

- (CGSize)gridViewSizeOfCell:(BDGridView *)gridView
{
    if (IS_IPAD)
    {
        CGFloat width = 100;
        
        return CGSizeMake(width, width);
    }
    else
    {
        //        if(orientation == UIInterfaceOrientationLandscapeLeft || orientation == UIInterfaceOrientationLandscapeRight)
        //        {
        //        CGFloat width = (gridView.bounds.size.width - 3 * gridView.minimumPadding)/ 4;
        //            return CGSizeMake(width, width);
        //        }
        //        else
        //        {
        CGFloat width = (gridView.bounds.size.width - 3 * gridView.minimumPadding)/ 4;
        
        return CGSizeMake(width, width);
        //        }
    }
    
    return CGSizeZero;
}

- (BDGridCell *)gridView:(BDGridView *)gridView cellForIndex:(NSUInteger)index
{
    ALAsset *asset = [assets objectAtIndex:index];
    UIImage *pImage = nil;
    
    if ([[[UIDevice currentDevice] systemVersion] floatValue] >= 5.f)
    {
        pImage = [[UIImage imageWithCGImage:[asset aspectRatioThumbnail]] imageWithBorderWidth:0 andColor:[UIColor whiteColor].CGColor];
    }
    else
    {
        pImage = [[UIImage imageWithCGImage:[asset thumbnail]] imageWithBorderWidth:0 andColor:[UIColor whiteColor].CGColor];
    }
    
    PhotoCell *cell = (PhotoCell*)[gridView dequeueCell];
    
    if (!cell)
    {
        cell = [[[PhotoCell alloc] initCellWithRoundedCorners] autorelease];
    }
    
    cell.photoImageView.image = pImage;
    
    return cell;
}

#pragma mark - BDGridViewDelegate's methods

- (void)gridView:(BDGridView *)gridView didTapCell:(BDGridCell *)cell
{
    ALAsset *asset = [assets objectAtIndex:cell.index];
    
    UIImage *image = [UIImage imageWithCGImage:[asset aspectRatioThumbnail]];
    
    if (image)
    {
        NSString *iconName = nil;
        
        if (isDoctor)
        {
            iconName = [@"doctor" stringByAppendingPathExtension:@"png"];
        }
        else if (isHome)
        {
            iconName = [@"home" stringByAppendingPathExtension:@"png"];
        }
        else if (isContact)
        {
            iconName = [@"contact" stringByAppendingPathExtension:@"png"];
        }
        else if (isHospital)
        {
            iconName = [@"hospital" stringByAppendingPathExtension:@"png"];
        }
        
        NSArray *paths = NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES);
        NSString *dirPath = [paths objectAtIndex:0];
        
        dirPath = [dirPath stringByAppendingPathComponent:iconName];
        
        NSData *data = UIImagePNGRepresentation(image);
        
        [[NSFileManager defaultManager] createFileAtPath:dirPath contents:data attributes:nil];
    }
    
    NSString *iconName = nil;
    
    if (isDoctor)
    {
        iconName = @"doctor";
    }
    else if (isHome)
    {
        iconName = @"home";
    }
    else if (isContact)
    {
        iconName = @"contact";
    }
    else if (isHospital)
    {
        iconName = @"hospital";
    }
    
    [self setIconOfContact:iconName];
    
    [self.addPhotoPopover removeFromSuperview];
}

//- (void)assetsLoaded
//{
//    NSInteger limit = itemsAtRow;
//    NSInteger rad = 0;
//    CGFloat itemSize = (self.photoContentView.frame.size.width - LEFT_PADDING*itemsAtRow - LEFT_PADDING)/itemsAtRow;
//    int ost = [assets count]%itemsAtRow;
//    if (ost > 1) {
//        ost = 1;
//    }
//    
//    CGFloat contentSizeHeight = LEFT_PADDING + (LEFT_PADDING+itemSize)*([assets count]/itemsAtRow) + (LEFT_PADDING+itemSize)*ost;
//    self.photoContentView.contentSize = CGSizeMake(self.photoContentView.frame.size.width,  contentSizeHeight);
//    
//    for (int i=1; i<[assets count]+1; i++) {
//        CGFloat posX = 0.0f;
//        CGFloat posY = 0.0f;
//        if (i >= limit+1) {
//            limit = limit + itemsAtRow;
//            rad = rad+1;
//        }
//        NSInteger cellQueue = 0;
//        if (rad == 0) {
//            cellQueue = i-1;
//        } else  {
//            cellQueue = i - ((rad) * itemsAtRow)-1;
//        }
//        
//        if (rad == 0) {
//            posY = LEFT_PADDING;
//        } else {
//            posY = LEFT_PADDING +(LEFT_PADDING + itemSize)*rad;
//        }
//        if (cellQueue == 0) {
//            posX = LEFT_PADDING;
//        } else {
//            posX = LEFT_PADDING +(LEFT_PADDING + itemSize)*cellQueue;
//        }
//        
//        
//        ALAsset *asset = [assets objectAtIndex:i-1];
//        GRPhotoPreview *preview;
//        if (assetsLoaded) {
//            preview = [[self.photoContentView subviews] objectAtIndex:i-1+2];
//            preview.frame = CGRectMake(posX, posY, itemSize, itemSize);
//        } else {
//            //ALAssetRepresentation *respresentation = [asset defaultRepresentation];
//            //UIImage *pImage = [UIImage imageWithCGImage:[respresentation fullScreenImage]];
//            
//            UIImage *pImage = nil;
//            
//            if ([[[UIDevice currentDevice] systemVersion] floatValue] >= 5.f)
//            {
//                pImage = [UIImage imageWithCGImage:[asset aspectRatioThumbnail]];
//            }
//            else
//            {
//                pImage = [UIImage imageWithCGImage:[asset thumbnail]];
//            }
//            
//            preview = [[GRPhotoPreview alloc] initWithImage:pImage frame:CGRectMake(posX, posY, itemSize, itemSize)];
//            
//            preview.backgroundColor = [UIColor colorWithRed:0 green:0 blue:0 alpha:0.6f];
//            preview.index = i-1;
//            preview.delegate = self;
//            [self.photoContentView addSubview:preview];
//            [preview release];
//            preview = nil;
//        }
//    }
//    
//    assetsLoaded = YES;
//}

//- (void)adjustToInterfaceRotation:(UIInterfaceOrientation)orientation
//{
//    if(orientation == UIInterfaceOrientationLandscapeLeft ||orientation == UIInterfaceOrientationLandscapeRight) {
//        if (IS_IPAD) {
//            itemsAtRow = 5;
//            //            superWidth = 1024;
//            //            superHeight = 768;
//        } else {
//            itemsAtRow = 6;
//            //            superWidth = 480;
//            //            superHeight = 267;
//        }
//    } else {
//        if (IS_IPAD) {
//            itemsAtRow = 3;
//            //            superWidth = 768;
//            //            superHeight = 960;
//        } else {
//            itemsAtRow = 4;
//            //            superWidth = 320;
//            //            superHeight = 416;
//        }
//    }
//    
//    //    self.photoContentView.frame = CGRectMake(0, 0, superWidth, superHeight);
//    if (assetsLoaded)
//    {
//        [self assetsLoaded];
//    }
//}

- (IBAction)doctorButtonTapped
{
    [self.addPhotoPopover removeFromSuperview];
    [self.numberTextField resignFirstResponder];
    [self.nameTextField resignFirstResponder];
 
    isHome = NO;
    isDoctor = YES;
    isHospital = NO;
    isContact = NO;
    
    self.contactButton.selected = settings.contact_name.length > 0;
    self.doctorButton.selected = YES;
    self.homeButton.selected = settings.home_name.length > 0;
    self.hospitalButton.selected = settings.hospital_name.length > 0;
    
    self.nameTextField.text = settings.doctor_name;
    self.numberTextField.text = settings.doctor_phone;
    
    [self setIconOfContact:@"doctor"];
}

- (IBAction)homeButtonTapped
{
    [self.addPhotoPopover removeFromSuperview];
    [self.numberTextField resignFirstResponder];
    [self.nameTextField resignFirstResponder];
 
    isHome = YES;
    isDoctor = NO;
    isHospital = NO;
    isContact = NO;
    
    self.contactButton.selected = settings.contact_name.length > 0;
    self.doctorButton.selected = settings.doctor_name.length > 0;
    self.homeButton.selected = YES;
    self.hospitalButton.selected = settings.hospital_name.length > 0;
    
    self.nameTextField.text = settings.home_name;
    self.numberTextField.text = settings.home_phone;
    
    [self setIconOfContact:@"home"];
}

- (IBAction)contactButtonTapped
{
    [self.addPhotoPopover removeFromSuperview];
    [self.numberTextField resignFirstResponder];
    [self.nameTextField resignFirstResponder];
 
    isHome = NO;
    isDoctor = NO;
    isHospital = NO;
    isContact = YES;
    
    self.contactButton.selected = YES;
    self.doctorButton.selected = settings.doctor_name.length > 0;
    self.homeButton.selected = settings.home_name.length > 0;
    self.hospitalButton.selected = settings.hospital_name.length > 0;
    
    self.nameTextField.text = settings.contact_name;
    self.numberTextField.text = settings.contact_phone;
    
    [self setIconOfContact:@"contact"];
}

- (IBAction)hospitalButtonTapped
{
    [self.addPhotoPopover removeFromSuperview];
    [self.numberTextField resignFirstResponder];
    [self.nameTextField resignFirstResponder];
 
    isHome = NO;
    isDoctor = NO;
    isHospital = YES;
    isContact = NO;
    
    self.contactButton.selected = settings.contact_name.length > 0;
    self.doctorButton.selected = settings.doctor_name.length > 0;
    self.homeButton.selected = settings.home_name.length > 0;
    self.hospitalButton.selected = YES;
    
    self.nameTextField.text = settings.hospital_name;
    self.numberTextField.text = settings.hospital_phone;
    
    [self setIconOfContact:@"hospital"];
}

- (IBAction)okButtonTapped
{
    [self.addPhotoPopover removeFromSuperview];
}

- (IBAction)cancelButtonTapped
{
    NSString *iconName = nil;
    
    if (isDoctor)
    {
        iconName = @"doctor";
    }
    else if (isHome)
    {
        iconName = @"home";
    }
    else if (isContact)
    {
        iconName = @"contact";
    }
    else if (isHospital)
    {
        iconName = @"hospital";
    }
    
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES);
    NSString *dirPath = [paths objectAtIndex:0];
    
    dirPath = [dirPath stringByAppendingPathComponent:iconName];
    dirPath = [dirPath stringByAppendingPathExtension:@"png"];
    
    [[NSFileManager defaultManager] removeItemAtPath:dirPath error:nil];
    
    [self setIconOfContact:iconName];
}

- (IBAction)removeNameButtonTapped
{
    self.nameTextField.text = @"";
}

- (IBAction)removeNumberButtonTapped
{
    self.numberTextField.text = @"";
}

- (IBAction)removeSosButtonTapped
{
    self.nameTextField.text = @"";
    self.numberTextField.text = @"";
    
    [icon removeFromSuperview];
    icon = nil;
    
    if (isContact)
    {
        settings.contact_name = @"";
        settings.contact_phone = @"";
        
        self.contactButtonLabel.text = NSLocalizedString(@"Add contact", @"Add contact");
        self.contactButton.selected = NO;
    }
    else if (isHospital)
    {
        settings.hospital_name = @"";
        settings.hospital_phone = @"";
        
        self.hospitalButtonLabel.text = NSLocalizedString(@"Add hospital", @"Add hospital");
        self.homeButton.selected = NO;
    }
    else if (isHome)
    {
        settings.home_name = @"";
        settings.home_phone = @"";
        
        self.homeButtonLabel.text = NSLocalizedString(@"Add home", @"Add home");
        self.homeButton.selected = NO;
    }
    else if (isDoctor)
    {
        settings.doctor_name = @"";
        settings.doctor_phone = @"";
        
        self.doctorButtonLabel.text = NSLocalizedString(@"Add doctor", @"Add doctor");
        self.doctorButton.selected = NO;
    }
    
    [self cancelButtonTapped];
}

- (IBAction)photoButtonTapped
{
    [[AppDelegate getInstance].window.rootViewController.view addSubview:self.addPhotoPopover];

    if (IS_IPAD)
    {
        //self.addPhotoPopover.frame = CGRectMake(130, 200, self.addPhotoPopover.frame.size.width, self.addPhotoPopover.frame.size.height);
        
        self.addPhotoPopover.frame = CGRectMake(0, 0, self.addPhotoPopover.frame.size.width, self.addPhotoPopover.frame.size.height);
    }

    [self.nameTextField resignFirstResponder];
    [self.numberTextField resignFirstResponder];
}
#pragma mark - UItextFieldDelegate

- (void)textFieldDidBeginEditing:(UITextField *)textField
{
    [self.addPhotoPopover removeFromSuperview];
}

- (void)textFieldDidEndEditing:(UITextField *)textField
{
    if (isContact)
    {
        if (textField == self.nameTextField)
        {
            settings.contact_name = self.nameTextField.text;
            
            self.contactButtonLabel.text = settings.contact_name.length ? settings.contact_name : NSLocalizedString(@"Add contact", @"Add contact");
        }
        else
        {
            settings.contact_phone = self.numberTextField.text;
        }
    }
    else if (isHome)
    {
        if (textField == self.nameTextField)
        {
            settings.home_name = self.nameTextField.text;
            
            self.homeButtonLabel.text = settings.home_name.length ? settings.home_name : NSLocalizedString(@"Add home", @"Add home");
        }
        else
        {
            settings.home_phone = self.numberTextField.text;
        }
    }
    else if (isDoctor)
    {
        if (textField == self.nameTextField)
        {
            settings.doctor_name = self.nameTextField.text;
            
            self.doctorButtonLabel.text = settings.doctor_name.length ? settings.doctor_name : NSLocalizedString(@"Add doctor", @"Add doctor");
        }
        else
        {
            settings.doctor_phone = self.numberTextField.text;
        }
    }
    else if (isHospital)
    {
        if (textField == self.nameTextField)
        {
            settings.hospital_name = self.nameTextField.text;
            
            self.hospitalButtonLabel.text = settings.hospital_name.length ? settings.hospital_name : NSLocalizedString(@"Add hospital", @"Add hospital");
        }
        else
        {
            settings.hospital_phone = self.numberTextField.text;
        }
    }
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    if (numberTextField == textField)
    {
        if(textField.text.length - range.length + string.length > 20)
        {
            return NO;
        }
        
        NSString *template = @"0123456789)(+-#";
        
        if ([string length] > 0)
        {
            NSRange range = [template rangeOfString:string];
            
            if (range.location != NSNotFound)
            {
                return YES;
            }
            
            return NO;
        }
    }
    
    return YES;
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    
    if (isContact)
    {
        if (textField == self.nameTextField)
        {
            settings.contact_name = self.nameTextField.text;
            
            self.contactButtonLabel.text = settings.contact_name.length ? settings.contact_name : NSLocalizedString(@"Add contact", @"Add contact");
        }
        else
        {
            settings.contact_phone = self.numberTextField.text;
        }
    }
    else if (isHome)
    {
        if (textField == self.nameTextField)
        {
            settings.home_name = self.nameTextField.text;
            
            self.homeButtonLabel.text = settings.home_name.length ? settings.home_name : NSLocalizedString(@"Add home", @"Add home");
        }
        else
        {
            settings.home_phone = self.numberTextField.text;
        }
    }
    else if (isDoctor)
    {
        if (textField == self.nameTextField)
        {
            settings.doctor_name = self.nameTextField.text;
            
            self.doctorButtonLabel.text = settings.doctor_name.length ? settings.doctor_name : NSLocalizedString(@"Add doctor", @"Add doctor");
        }
        else
        {
            settings.doctor_phone = self.numberTextField.text;
        }
    }
    else if (isHospital)
    {
        if (textField == self.nameTextField)
        {
            settings.hospital_name = self.nameTextField.text;
            
            self.hospitalButtonLabel.text = settings.hospital_name.length ? settings.hospital_name : NSLocalizedString(@"Add hospital", @"Add hospital");
        }
        else
        {
            settings.hospital_phone = self.numberTextField.text;
        }
    }
    
    return YES;
}

@end
