//
//  SOSViewControllerProtocol.h
//  GrannyApp
//
//  Created by Eugene Pavluk on 12/19/12.
//
//

#import <Foundation/Foundation.h>

@protocol SOSViewControllerProtocol <NSObject>

- (void) hideSOSScreen;

@end
