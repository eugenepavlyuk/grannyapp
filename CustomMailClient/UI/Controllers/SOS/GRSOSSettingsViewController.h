//
//  GRSOSSettingsViewController.h
//  GrannyApp
//
//  Created by Eugene Pavluk on 12/27/12.
//
//

#import <UIKit/UIKit.h>
#import "BDGridView.h"
#import "GRPhotoPreview.h"
#import "SOSButton.h"

@interface PhotoButton : UIButton



@end

@interface GRSOSSettingsViewController : GAITrackedViewController <UITextFieldDelegate, BDGridViewDataSource, BDGridViewDelegate>
{
    GRPhotoPreview *icon;
    
    NSMutableArray *assets;
//    NSMutableArray *photoViews;
    ALAssetsLibrary *library;
}

@property (nonatomic, retain) IBOutlet SOSButton *doctorButton;
@property (nonatomic, retain) IBOutlet SOSButton *homeButton;
@property (nonatomic, retain) IBOutlet SOSButton *contactButton;
@property (nonatomic, retain) IBOutlet SOSButton *hospitalButton;
@property (nonatomic, retain) IBOutlet UIButton *photoButton;

@property (nonatomic, retain) IBOutlet UILabel *doctorButtonLabel;
@property (nonatomic, retain) IBOutlet UILabel *homeButtonLabel;
@property (nonatomic, retain) IBOutlet UILabel *contactButtonLabel;
@property (nonatomic, retain) IBOutlet UILabel *hospitalButtonLabel;

@property (nonatomic, retain) IBOutlet UITextField *nameTextField;
@property (nonatomic, retain) IBOutlet UITextField *numberTextField;

@property (nonatomic, retain) IBOutlet UIView *addPhotoPopover;
@property (nonatomic, retain) IBOutlet BDGridView *photoContentView;

- (IBAction)doctorButtonTapped;
- (IBAction)homeButtonTapped;
- (IBAction)contactButtonTapped;
- (IBAction)hospitalButtonTapped;

- (IBAction)photoButtonTapped;
- (IBAction)cancelButtonTapped;
- (IBAction)okButtonTapped;

- (IBAction)removeNameButtonTapped;
- (IBAction)removeNumberButtonTapped;
- (IBAction)removeSosButtonTapped;

@end
