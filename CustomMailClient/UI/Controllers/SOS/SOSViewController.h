//
//  SOSViewController.h
//  GrannyApp
//
//  Created by Eugene Pavluk on 12/19/12.
//
//

#import <UIKit/UIKit.h>
#import "SOSViewControllerProtocol.h"
#import "GRCommonAlertView.h"

@interface SOSViewController : GAITrackedViewController <GRCommonAlertDelegate>

@property (nonatomic, retain) IBOutlet UIButton *cancelButton;
@property (nonatomic, retain) IBOutlet UIButton *doctorButton;
@property (nonatomic, retain) IBOutlet UIButton *homeButton;
@property (nonatomic, retain) IBOutlet UIButton *contactButton;
@property (nonatomic, retain) IBOutlet UIButton *hospitalButton;

@property (nonatomic, retain) IBOutlet UILabel *homeButtonLabel;
@property (nonatomic, retain) IBOutlet UILabel *doctorButtonLabel;
@property (nonatomic, retain) IBOutlet UILabel *contactButtonLabel;
@property (nonatomic, retain) IBOutlet UILabel *hospitalButtonLabel;

@property (nonatomic, assign) NSObject<SOSViewControllerProtocol> *delegate;

- (IBAction)cancelButtonTapped;
- (IBAction)buttonTapped:(UIButton*)button;

@end
