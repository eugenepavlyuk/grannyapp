//
//  SOSButton.m
//  GrannyApp
//
//  Created by Eugene Pavluk on 1/7/14.
//
//

#import "SOSButton.h"

@implementation SOSButton

- (void)awakeFromNib
{
    [super awakeFromNib];
    
    self.layer.cornerRadius = 25.f;
    self.clipsToBounds = YES;
}

- (void)layoutSubviews
{
    [super layoutSubviews];
    
    self.imageView.frame = CGRectMake(0, 0, 80, 80);
    self.imageView.center = CGPointMake(self.bounds.size.width/2, self.bounds.size.height/2);
    self.imageView.clipsToBounds = YES;
}

@end
