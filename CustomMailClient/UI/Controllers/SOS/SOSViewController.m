//
//  SOSViewController.m
//  GrannyApp
//
//  Created by Eugene Pavluk on 12/19/12.
//
//

#import "SOSViewController.h"
#import "Settings.h"
#import "DataManager.h"
#import "AlertView.h"
#import "WYPopoverController.h"
#import "GRAlertViewController.h"

@interface SOSViewController () <WYPopoverControllerDelegate>

@property (nonatomic, retain) WYPopoverController *alertPopoverController;

@end

@implementation SOSViewController
{
    Settings *settings;
    NSMutableArray *buttons;
    NSMutableArray *labels;
}

@synthesize delegate;
@synthesize cancelButton;
@synthesize doctorButton;
@synthesize homeButton;
@synthesize contactButton;
@synthesize homeButtonLabel;
@synthesize doctorButtonLabel;
@synthesize contactButtonLabel;
@synthesize hospitalButtonLabel;
@synthesize hospitalButton;

- (BOOL)prefersStatusBarHidden
{
    return YES;
}

- (void)setupPopoverAppearance
{
    WYPopoverBackgroundView *popoverAppearance = [WYPopoverBackgroundView appearance];
    
    [popoverAppearance setOverlayColor:[UIColor colorWithWhite:0.1f alpha:0.6f]];
    [popoverAppearance setOuterCornerRadius:0];
    [popoverAppearance setOuterShadowBlurRadius:4];
    [popoverAppearance setOuterShadowColor:[UIColor colorWithWhite:0.3f alpha:0.6f]];
    [popoverAppearance setOuterShadowOffset:CGSizeMake(4, 4)];
    
    [popoverAppearance setGlossShadowColor:[UIColor clearColor]];
    [popoverAppearance setGlossShadowOffset:CGSizeMake(0, 0)];
    
    [popoverAppearance setBorderWidth:0];
    [popoverAppearance setArrowHeight:30];
    [popoverAppearance setArrowBase:35];
    
    [popoverAppearance setInnerCornerRadius:0];
    [popoverAppearance setInnerShadowBlurRadius:0];
    [popoverAppearance setInnerShadowColor:[UIColor clearColor]];
    [popoverAppearance setInnerShadowOffset:CGSizeMake(0, 0)];
    
    [popoverAppearance setFillTopColor:[UIColor whiteColor]];
    [popoverAppearance setFillBottomColor:[UIColor whiteColor]];
    [popoverAppearance setOuterStrokeColor:[UIColor whiteColor]];
    [popoverAppearance setInnerStrokeColor:[UIColor whiteColor]];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    
    self.screenName = @"SOS Screen";
    
    buttons = [@[] mutableCopy];
    labels = [@[] mutableCopy];
    
    if (!settings)
    {
        settings = [[[DataManager sharedInstance] getEntityWithName:kSettingsEntityName] retain];
    }
    
    self.doctorButton.hidden = YES;
    self.homeButton.hidden = YES;
    self.contactButton.hidden = YES;
    self.hospitalButton.hidden = YES;
    self.doctorButtonLabel.hidden = YES;
    self.hospitalButtonLabel.hidden = YES;
    self.homeButtonLabel.hidden = YES;
    self.contactButtonLabel.hidden = YES;
    
    self.doctorButtonLabel.text = settings.doctor_name;
    self.homeButtonLabel.text = settings.home_name;
    self.contactButtonLabel.text = settings.contact_name;
    self.hospitalButtonLabel.text = settings.hospital_name;
    
    [self setIconOfContact:@"doctor"];
    [self setIconOfContact:@"home"];
    [self setIconOfContact:@"contact"];
    [self setIconOfContact:@"hospital"];
    
    if ([settings.doctor_phone length])
    {
        [buttons addObject:self.doctorButton];
        [labels addObject:self.doctorButtonLabel];
    }
    
    if ([settings.home_phone length])
    {
        [buttons addObject:self.homeButton];
        [labels addObject:self.homeButtonLabel];
    }
    
    if ([settings.contact_phone length])
    {
        [buttons addObject:self.contactButton];
        [labels addObject:self.contactButtonLabel];
    }
    
    if ([settings.hospital_phone length])
    {
        [buttons addObject:self.hospitalButton];
        [labels addObject:hospitalButtonLabel];
    }
    
    if ([buttons count])
    {
        float width = [buttons count] * self.hospitalButton.bounds.size.width + 20 * ([buttons count] - 1);
        
        float xcenter = ((float)1024) / 2 - width / 2 + ((float)self.hospitalButton.bounds.size.width) / 2;
        
        int i = 0;
        
        for (UIButton *button in buttons)
        {
            button.center = CGPointMake(xcenter, button.center.y);
            
            UILabel *label = labels[i];
            
            label.center = CGPointMake(xcenter, label.center.y);
            
            i++;
            
            button.hidden = NO;
            label.hidden = NO;
            
            xcenter += 20;
            xcenter += self.hospitalButton.bounds.size.width;
        }
        
        self.cancelButton.hidden = NO;
    }
    else
    {
        self.cancelButton.hidden = YES;
    }
}

- (void)setIconOfContact:(NSString*)name
{
    NSString *iconName = [name stringByAppendingPathExtension:@"png"];
    
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES);
    NSString *dirPath = [paths objectAtIndex:0];
    
    dirPath = [dirPath stringByAppendingPathComponent:iconName];
    
    UIImage *image = [UIImage imageWithContentsOfFile:dirPath];
        
    if (image)
    {
        if ([name isEqualToString:@"home"])
        {
            [self.homeButton setImage:image forState:UIControlStateNormal];
            self.homeButton.imageView.contentMode = UIViewContentModeScaleAspectFill;
        }
        else if ([name isEqualToString:@"doctor"])
        {
            [self.doctorButton setImage:image forState:UIControlStateNormal];
            self.doctorButton.imageView.contentMode = UIViewContentModeScaleAspectFill;
        }
        else if ([name isEqualToString:@"contact"])
        {
            [self.contactButton setImage:image forState:UIControlStateNormal];
            self.contactButton.imageView.contentMode = UIViewContentModeScaleAspectFill;
        }
        else if ([name isEqualToString:@"hospital"])
        {
            [self.hospitalButton setImage:image forState:UIControlStateNormal];
            self.hospitalButton.imageView.contentMode = UIViewContentModeScaleAspectFill;
        }
    }
    else
    {
        if ([name isEqualToString:@"home"])
        {
            [self.homeButton setImage:[UIImage imageNamed:@"sos_home"] forState:UIControlStateNormal];
            self.homeButton.imageView.contentMode = UIViewContentModeCenter;
        }
        else if ([name isEqualToString:@"doctor"])
        {
            [self.doctorButton setImage:[UIImage imageNamed:@"sos_doctor"] forState:UIControlStateNormal];
            self.doctorButton.imageView.contentMode = UIViewContentModeCenter;
        }
        else if ([name isEqualToString:@"contact"])
        {
            [self.contactButton setImage:[UIImage imageNamed:@"sos_contact"] forState:UIControlStateNormal];
            self.contactButton.imageView.contentMode = UIViewContentModeCenter;
        }
        else if ([name isEqualToString:@"hospital"])
        {
            [self.hospitalButton setImage:[UIImage imageNamed:@"sos_hospital"] forState:UIControlStateNormal];
            self.hospitalButton.imageView.contentMode = UIViewContentModeCenter;
        }
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)releaseViews
{
    self.cancelButton = nil;
    self.contactButton = nil;
    self.homeButton = nil;
    self.doctorButton = nil;
    self.contactButtonLabel = nil;
    self.homeButtonLabel = nil;
    self.doctorButtonLabel = nil;
    self.hospitalButton = nil;
    self.hospitalButtonLabel = nil;
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    
    [self releaseViews];
}

- (void)dealloc
{
    [self releaseViews];
    [settings release];
    
    [self.alertPopoverController dismissPopoverAnimated:NO];
    self.alertPopoverController = nil;
    
    [super dealloc];
}

- (IBAction)cancelButtonTapped
{
    [self.delegate hideSOSScreen];
}

- (IBAction)buttonTapped:(UIButton*)button
{
    if (IS_IPAD)
    {
        GRAlertViewController *contentViewController = [[[GRAlertViewController alloc] init] autorelease];
        contentViewController.contentSizeForViewInPopover = CGSizeMake(512, 270);
        
        [contentViewController view];
        
        contentViewController.messageLabel.text = NSLocalizedString(@"There is no telephone connection", @"There is no telephone connection");
        
        [contentViewController.okButton addTarget:self action:@selector(popoverButtonTapped:) forControlEvents:UIControlEventTouchUpInside];
        
        self.alertPopoverController = [[[WYPopoverController alloc] initWithContentViewController:contentViewController] autorelease];
        self.alertPopoverController.delegate = self;
        
        [self.alertPopoverController presentPopoverAsDialogAnimated:YES];
        
        return ;
    }
    
    NSString *phone = nil;
    
    if (button == self.doctorButton)
    {
        phone = settings.doctor_phone;
    }
    else if (button == self.homeButton)
    {
        phone = settings.home_phone;
    }
    else if (button == self.contactButton)
    {
        phone = settings.contact_phone;
    }
    else
    {
        phone = settings.hospital_phone;
    }
    
    if (![phone length])
    {
        InfoAlertView *alertView = [[[InfoAlertView alloc] initWithTitle:NSLocalizedString(@"\nPhone number is absent\n", @"Phone number is absent")
                                                                 message:nil
                                                                delegate:self
                                                       cancelButtonTitle:nil
                                                       otherButtonTitles:NSLocalizedString(@"OK", @"OK"), nil] autorelease];
        
        alertView.tag = 666;
        
        [alertView show];
    }
    else
    {
        NSString* urlString = [NSString stringWithFormat:@"tel://%@", phone];
        
        urlString = [urlString stringByReplacingOccurrencesOfString:@"(" withString:@""];
        urlString = [urlString stringByReplacingOccurrencesOfString:@")" withString:@""];
        urlString = [urlString stringByReplacingOccurrencesOfString:@" " withString:@""];
        
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:urlString]];
    }
}

- (void)popoverButtonTapped:(UIButton*)button
{
    [self.alertPopoverController dismissPopoverAnimated:YES];
    self.alertPopoverController = nil;
    
    [self.delegate hideSOSScreen];
}

#pragma mark - GRCommonAlertDelegate
- (void)commonAlert:(id)alertView buttonWithIndex:(NSInteger)index
{
//    if (index == 0)
//    {
//        [self.delegate hideSOSScreen];
//        GRSettingsiPadController *contr = [[[GRSettingsiPadController alloc] initWithNibName:@"GRSettingsiPadController" bundle:nil] autorelease];
//        contr.selectedIndex = 9;
//        [(UINavigationController *)[AppDelegate getInstance].window.rootViewController pushViewController:contr animated:YES];
//    }
//    else
        [self.delegate hideSOSScreen];
}

#pragma mark - WYPopoverControllerDelegate's methods

- (BOOL)popoverControllerShouldDismissPopover:(WYPopoverController *)popoverController
{
    return YES;
}

- (void)popoverControllerDidDismissPopover:(WYPopoverController *)popoverController
{
    self.alertPopoverController = nil;
}

@end
