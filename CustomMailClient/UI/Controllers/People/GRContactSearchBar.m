//
//  GRContactSearchBar.m
//  GrannyApp
//
//  Created by admin on 26/11/13.
//
//

#import "GRContactSearchBar.h"

NSString *const GRContactSearchBarDidCancelNotification = @"GRContactSearchBarDidCancelNotification";

@implementation GRContactSearchBar

- (void)awakeFromNib {
    [super awakeFromNib];
    
    UIImage *image = [UIImage imageNamed:@"people_cancel_btn"];
    
    UIView *rightView = [[[UIView alloc] initWithFrame:CGRectMake(0, 0, image.size.width*1.4, image.size.height)] autorelease];
    
    UIButton *cancelBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    cancelBtn.frame = CGRectMake(0, 0, image.size.width, image.size.height);
    [cancelBtn setBackgroundImage:image forState:UIControlStateNormal];
    [cancelBtn addTarget:self action:@selector(cancelSearch) forControlEvents:UIControlEventTouchUpInside];
    [rightView addSubview:cancelBtn];
    self.rightViewMode = UITextFieldViewModeAlways;
    self.rightView = rightView;
    
    self.leftViewMode = UITextFieldViewModeAlways;
    self.leftView = [[[UIImageView alloc] initWithImage:[UIImage imageNamed:@"people_magnifying_glass"]] autorelease];
    self.leftView.frame = CGRectMake(0, 0, 60, 52);
    self.leftView.backgroundColor = [UIColor clearColor];
    self.leftView.contentMode = UIViewContentModeScaleAspectFit;
    
    self.background = [[UIImage imageNamed:@"people_area"] resizableImageWithCapInsets:UIEdgeInsetsMake(20, 100, 20, 100)];
}

- (void) cancelSearch
{
    [[NSNotificationCenter defaultCenter] postNotificationName:GRContactSearchBarDidCancelNotification object:nil];
    [self setText:@""];
    [self resignFirstResponder];
}

@end
