//
//  GRContactContainerView.m
//  GrannyApp
//
//  Created by Eugene Pavluk on 11/28/13.
//
//

#import "GRContactContainerView.h"

@implementation GRContactContainerView

- (void)awakeFromNib
{
    [super awakeFromNib];
}

- (void)dealloc
{
    self.titleLabel = nil;
    self.container = nil;
    self.coverImageView = nil;
    self.coverButton = nil;
    self.topSeparatorView = nil;
    self.bottomSeparatorView = nil;
    [super dealloc];
}

@end
