//
//  GRContactTableViewCell.m
//  GrannyApp
//
//  Created by admin on 26/11/13.
//
//

#import "GRContactTableViewCell.h"
#import "GRContactView.h"

@implementation GRContactTableViewCell

@synthesize titleLabel;
@synthesize delegate;
@synthesize numberOfChildren;
@synthesize childSize;
@synthesize indexPath;

- (void)awakeFromNib
{
    [super awakeFromNib];
    
    self.backgroundColor = [UIColor clearColor];
    self.contentView.backgroundColor = [UIColor clearColor];
}

+ (CGFloat)cellHeight
{
    if (IS_IPAD)
    {
        return 78;
    }
    
    return 50;
}

- (void)dealloc
{
    self.titleLabel = nil;
    self.indexPath = nil;
    [super dealloc];
}

+ (NSString*)reuseIdentifier
{
    return @"GRContactTableViewCellIdentifier";
}

- (void)layoutContent
{
    for (int i = 0; i < numberOfChildren; i++)
    {
        GRContactView *contactView = nil;
        
        if ([delegate respondsToSelector:@selector(viewForContactAtIndexPath:withIndex:)])
        {
            contactView = [delegate performSelector:@selector(viewForContactAtIndexPath:withIndex:) withObject:self.indexPath withObject:[NSNumber numberWithInt:i]];
            contactView.coverButton.tag = i;
        }
        
        [contactView.coverButton addTarget:self
                                    action:@selector(coverButtonTapped:)
                          forControlEvents:UIControlEventTouchUpInside];
        
        [self.contentView addSubview:contactView];
    }
}

- (void)coverButtonTapped:(id)sender
{
    UIButton *button = (UIButton*)sender;
    
    [delegate willSelectView:(GRContactContainerView*)[[button superview] superview]atIndexPath:self.indexPath andIndex:[NSNumber numberWithInt:button.tag]];
    
    [delegate contactTappedWithIndexPath:indexPath andIndex:[NSNumber numberWithInt:button.tag]];
}

@end
