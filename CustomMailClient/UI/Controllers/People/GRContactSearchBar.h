//
//  GRContactSearchBar.h
//  GrannyApp
//
//  Created by admin on 26/11/13.
//
//

#import <UIKit/UIKit.h>

extern NSString *const GRContactSearchBarDidCancelNotification;

@interface GRContactSearchBar : UITextField

@end
