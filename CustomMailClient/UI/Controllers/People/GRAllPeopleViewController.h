//
//  GRAllPeopleViewController.h
//  GrannyApp
//
//  Created by Eugene Pavlyuk on 5/22/12.
//  Copyright (c) 2012 Home. All rights reserved.
//

#import "SOSViewControllerProtocol.h"
#import "GRContactTableViewCell.h"
#import "GRContactCardDelegate.h"

@class GRContactViewController;
@class GRNewContactViewController;
@class GRContactSearchBar;

@interface GRAllPeopleViewController : GAITrackedViewController <UITableViewDelegate, UITableViewDataSource, UITextFieldDelegate, SOSViewControllerProtocol, GRContactCellDelegate, GRContactCardDelegate>
{
    BOOL keyboardIsShowing;
}

@property (nonatomic, retain) IBOutlet UITableView *gridView;
@property (nonatomic, retain) IBOutlet GRContactSearchBar *searchBar;
@property (nonatomic, retain) IBOutlet UIActivityIndicatorView *spinnerView;
@property (nonatomic, retain) IBOutlet UIImageView *headerImageView;
@property (nonatomic, retain) IBOutlet UILabel *titleLabel;

@property (nonatomic, retain) GRContactViewController *contactViewController;
@property (nonatomic, retain) GRNewContactViewController *contactNewViewController;

- (IBAction)homeButtonTapped;
- (IBAction)addButtonTapped;

@end
