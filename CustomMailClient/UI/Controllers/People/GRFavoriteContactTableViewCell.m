//
//  GRFavoriteContactTableViewCell.m
//  GrannyApp
//
//  Created by admin on 26/11/13.
//
//

#import "GRFavoriteContactTableViewCell.h"

@implementation GRFavoriteContactTableViewCell

+ (CGFloat)cellHeight
{
    if (IS_IPAD)
    {
        return 230;
    }
    
    return 75;
}

- (void)dealloc
{
    self.titleLabel = nil;
    [super dealloc];
}

+ (NSString*)reuseIdentifier
{
    return @"GRFavoriteContactTableViewCellIdentifier";
}

@end
