//
//  GRContactView.m
//  GrannyApp
//
//  Created by admin on 26/11/13.
//
//

#import "GRContactView.h"

@implementation GRContactView

@synthesize titleLabel;
@synthesize container;
@synthesize coverImageView;
@synthesize coverButton;

- (void)awakeFromNib
{
    [super awakeFromNib];
}

- (void)dealloc
{
    self.titleLabel = nil;
    self.container = nil;
    self.coverImageView = nil;
    self.coverButton = nil;
    [super dealloc];
}

@end
