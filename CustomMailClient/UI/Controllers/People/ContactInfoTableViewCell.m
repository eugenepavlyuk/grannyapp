//
//  ContactInfoTableViewCell.m
//  GrannyApp
//
//  Created by Eugene Pavluk on 3/22/13.
//
//

#import "ContactInfoTableViewCell.h"

@implementation ContactInfoTableViewCell

- (void)awakeFromNib
{    
    _xOffsetForInfoField = 200;
    
    self.titleLabel.textAlignment = NSTextAlignmentLeft;
    self.infoTextField.textAlignment = NSTextAlignmentLeft;
    self.subTitleLabel.textAlignment = NSTextAlignmentLeft;
}

- (void)dealloc
{
    self.titleLabel = nil;
    self.infoTextField = nil;
    self.subTitleLabel = nil;
    [super dealloc];
}

- (void)layoutSubviews
{
    [super layoutSubviews];
    
    if (![self.subTitleLabel.text length])
    {
        self.infoTextField.frame = CGRectMake(_xOffsetForInfoField, 0, self.bounds.size.width - _xOffsetForInfoField, 40);
    }
    else
    {
        CGSize size = [self.subTitleLabel.text sizeWithFont:self.subTitleLabel.font constrainedToSize:CGSizeMake(_xOffsetForInfoField, 40)];
        
        self.subTitleLabel.frame = CGRectMake(_xOffsetForInfoField, 0, size.width + 5, 40);
        self.infoTextField.frame = CGRectMake(_xOffsetForInfoField + size.width + 5, 0, self.bounds.size.width - _xOffsetForInfoField - size.width - 5, 40);
    }
}

- (void)setXOffsetForInfoField:(NSInteger)xOffsetForInfoField
{
    _xOffsetForInfoField = xOffsetForInfoField;
    
    [self setNeedsLayout];
}

@end
