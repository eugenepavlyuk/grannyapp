//
//  GRNewContactViewController.m
//  GrannyApp
//
//  Created by Eugene Pavlyuk on 7/12/12.
//  Copyright (c) 2012 Home. All rights reserved.
//

#import "GRNewContactViewController.h"
#import "GRContact.h"
#import "BDGridCell.h"
#import "UIImage+Border.h"
#import "GRPeopleManager.h"
#import "Favorite.h"
#import "DataManager.h"
#import "ContactInfoTableViewCell.h"
#import "WYPopoverController.h"
#import "GRAlertViewController.h"


@interface GRNewContactViewController () <WYPopoverControllerDelegate>

@property (nonatomic, retain) MFMailComposeViewController *mailController;

@property (nonatomic, copy) NSString *street;
@property (nonatomic, copy) NSString *city;
@property (nonatomic, copy) NSString *zip;
@property (nonatomic, copy) NSString *country;
@property (nonatomic, copy) NSString *name;
@property (nonatomic, copy) NSString *lastname;
@property (nonatomic, copy) NSString *phone;
@property (nonatomic, copy) NSString *iphone;
@property (nonatomic, copy) NSString *homePhone;
@property (nonatomic, copy) NSString *workPhone;
@property (nonatomic, copy) NSString *mainPhone;
@property (nonatomic, copy) NSString *faxPhoneHome;
@property (nonatomic, copy) NSString *faxPhoneWork;
@property (nonatomic, copy) NSString *faxPhoneOther;
@property (nonatomic, copy) NSString *Pager;
@property (nonatomic, copy) NSString *skype;

@property (nonatomic, copy) NSString *email;
@property (nonatomic, copy) NSString *email1;
@property (nonatomic, copy) NSString *email2;
@property (nonatomic, copy) NSString *email3;
@property (nonatomic, copy) NSString *email4;
@property (nonatomic, copy) NSString *email5;

@property (nonatomic, copy) NSString *fullname;

@property (nonatomic, retain) UITextField *currentTextField;

@property (nonatomic, retain) WYPopoverController *alertPopoverController;

@end

#define kMaxPhoneDigitsCount 20
#define kMaxEmailSymbolsCount 30
#define kMaxZipDigitsCount 8
#define kMaxTextSymbolsCount 30

@implementation GRNewContactViewController

@synthesize backButton;
@synthesize contact;
@synthesize infoImageView;
@synthesize addPhotoButton;
@synthesize street;
@synthesize city;
@synthesize zip;
@synthesize country;
@synthesize phone;
@synthesize skype;
@synthesize fullname;

@synthesize email;
@synthesize email1;
@synthesize email2;
@synthesize email3;
@synthesize email4;
@synthesize email5;

@synthesize name;
@synthesize lastname;
@synthesize infoContainer;
@synthesize imageView;
@synthesize addPhotoPopover;
@synthesize photoView;
@synthesize photoContentView;
@synthesize mailController;
@synthesize currentTextField;
@synthesize photoContainerView;

@synthesize delegate;

- (BOOL)prefersStatusBarHidden
{
    return YES;
}

- (void)setupPopoverAppearance
{
    WYPopoverBackgroundView *popoverAppearance = [WYPopoverBackgroundView appearance];
    
    if (!self.isSettings)
    {
        [popoverAppearance setOverlayColor:[UIColor colorWithWhite:0.1f alpha:0.6f]];
        [popoverAppearance setOuterCornerRadius:20];
        [popoverAppearance setOuterShadowBlurRadius:4];
        [popoverAppearance setOuterShadowColor:[UIColor colorWithWhite:0.3f alpha:0.6f]];
        [popoverAppearance setOuterShadowOffset:CGSizeMake(4, 4)];
        
        [popoverAppearance setGlossShadowColor:[UIColor clearColor]];
        [popoverAppearance setGlossShadowOffset:CGSizeMake(0, 0)];
        
        [popoverAppearance setBorderWidth:0];
        [popoverAppearance setArrowHeight:30];
        [popoverAppearance setArrowBase:35];
        
        [popoverAppearance setInnerCornerRadius:20];
        [popoverAppearance setInnerShadowBlurRadius:0];
        [popoverAppearance setInnerShadowColor:[UIColor clearColor]];
        [popoverAppearance setInnerShadowOffset:CGSizeMake(0, 0)];
        
        [popoverAppearance setFillTopColor:[UIColor whiteColor]];
        [popoverAppearance setFillBottomColor:[UIColor whiteColor]];
        [popoverAppearance setOuterStrokeColor:[UIColor clearColor]];
        [popoverAppearance setInnerStrokeColor:[UIColor clearColor]];
    }
    else
    {
        [popoverAppearance setOverlayColor:[UIColor colorWithWhite:0.1f alpha:0.6f]];
        [popoverAppearance setOuterCornerRadius:0];
        [popoverAppearance setOuterShadowBlurRadius:0];
        [popoverAppearance setOuterShadowColor:[UIColor clearColor]];
        [popoverAppearance setOuterShadowOffset:CGSizeMake(0, 0)];
        
        [popoverAppearance setGlossShadowColor:[UIColor clearColor]];
        [popoverAppearance setGlossShadowOffset:CGSizeMake(0, 0)];
        
        [popoverAppearance setBorderWidth:0];
        [popoverAppearance setArrowHeight:18];
        [popoverAppearance setArrowBase:23];
        
        [popoverAppearance setInnerCornerRadius:0];
        [popoverAppearance setInnerShadowBlurRadius:0];
        [popoverAppearance setInnerShadowColor:[UIColor clearColor]];
        [popoverAppearance setInnerShadowOffset:CGSizeMake(0, 0)];
        
        [popoverAppearance setFillTopColor:[UIColor whiteColor]];
        [popoverAppearance setFillBottomColor:[UIColor whiteColor]];
        [popoverAppearance setOuterStrokeColor:[UIColor clearColor]];
        [popoverAppearance setInnerStrokeColor:[UIColor clearColor]];
    }
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    
    if (self) 
    {
        self.allowEditing = NO;
        
        [[NSNotificationCenter defaultCenter] addObserver:self
                                                 selector:@selector(resyncingContactsNotification:)
                                                     name:kResyncingContactsNotification
                                                   object:nil];
    }
    
    return self;
}

- (void)resyncingContactsNotification:(NSNotification*)notification
{
    [self.currentTextField resignFirstResponder];
    self.currentTextField = nil;
    
    [delegate popContactCard:self];
}

- (void)setAllowEditing:(BOOL)bAllowEditing
{
    _allowEditing = bAllowEditing;
    
    [self.tableView reloadData];
    
    self.addPhotoButton.userInteractionEnabled = _allowEditing;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    
    [self setupPopoverAppearance];
    
    self.screenName = @"Contact Screen";
    
    if (IS_IPAD)
    {
        self.addPhotoLabel.font = kARSMaquetteFontWithSize(24);
    }
    else
    {
        self.addPhotoLabel.font = kARSMaquetteFontWithSize(14);
    }
    
    if (self.contact.photourl)
    {
        self.imageView.image = [UIImage imageWithContentsOfFile:self.contact.photourl];
        
        [icon release];
        icon = [UIImage imageWithContentsOfFile:contact.photourl];
        [icon retain];
        
//        self.photoContainerView.hidden = NO;
//        self.photoView.hidden = NO;
//        self.imageView.hidden = NO;
//        
//        self.tableView.frame = CGRectMake(CGRectGetMaxX(self.photoContainerView.frame) + 20, self.tableView.frame.origin.y, CGRectGetWidth(self.infoImageView.frame) - CGRectGetMaxX(self.photoContainerView.frame) - 20, self.tableView.frame.size.height);
    }
//    else
//    {
//        self.photoContainerView.hidden = YES;
//        self.photoView.hidden = YES;
//        self.imageView.hidden = YES;
//        
//        self.tableView.frame = CGRectMake(30, self.tableView.frame.origin.y, CGRectGetWidth(self.infoImageView.frame) - 30, self.tableView.frame.size.height);
//    }
    
    NSString *firstNameStr = @"";
    NSString *lastNameStr = @"";
    
    if (contact.firstname)
    {
        firstNameStr = contact.firstname;
    }
    
    if (contact.lastname)
    {
        lastNameStr = contact.lastname;
    }
    
    NSString *fullnameStr = [NSString stringWithFormat:@"%@ %@", firstNameStr, lastNameStr];
    
    self.fullname = fullnameStr;
    self.lastname = contact.lastname;
    self.name = contact.firstname;
    
    self.phone = contact.phoneMobile;
    
    self.email = contact.email;
    self.email1 = contact.email1;
    self.email2 = contact.email2;
    self.email3 = contact.email3;
    self.email4 = contact.email4;
    self.email5 = contact.email5;
    
    self.skype = contact.phoneSkype;
    
    self.street = contact.street;

    self.city = contact.city;
    
    self.zip = contact.postCode;
    
    self.country = contact.country;
    
    if (!self.isSettings)
    {
        self.tableView.contentInset = UIEdgeInsetsMake(60, 0, 0, 0);
        
        UIImage *headerBackground = nil;
        
        if (IS_IPAD)
        {
            headerBackground = [[UIImage imageNamed:@"people_header_background"] resizableImageWithCapInsets:UIEdgeInsetsMake(0, 40, 0, 40)];
        }
        else
        {
            headerBackground = [[UIImage imageNamed:@"people_header_background"] stretchableImageWithLeftCapWidth:36 topCapHeight:0];
        }
        
        self.infoImageView.image = headerBackground;
        
        self.photoContainerView.layer.cornerRadius = 25.f;
        self.imageView.layer.cornerRadius = 25.f;
        self.photoView.layer.cornerRadius = 25.f;
        self.photoContainerView.layer.borderColor = RGBCOLOR(220, 54, 0).CGColor;
    }
    else
    {
        if (!self.contact)
        {
            self.backButton.hidden = NO;
        }
        
        self.tableView.contentInset = UIEdgeInsetsMake(20, 0, 0, 0);
        
        self.photoContainerView.layer.cornerRadius = 25.f;
        self.imageView.layer.cornerRadius = 25.f;
        self.photoContainerView.layer.shadowOffset = CGSizeMake(2, 2);
        self.photoContainerView.layer.shadowRadius = 5.f;
        self.photoContainerView.layer.shadowOpacity = 0.5f;
        self.photoContainerView.clipsToBounds = NO;
        self.photoContainerView.layer.shadowColor = [UIColor blackColor].CGColor;
        self.photoView.layer.cornerRadius = 25.f;
        self.photoContainerView.layer.borderColor = RGBCOLOR(140, 140, 140).CGColor;
    }
    
    self.photoContainerView.layer.borderWidth = 1.f;
    
    self.photoContentView.minimumPadding = 5.f;
    
    [self startLoadingPhotoFromGallery];
    
    self.deleteButton.hidden = YES;
    
    if (self.contact)
    {
        self.editButton.hidden = NO;
        
        self.addPhotoButton.userInteractionEnabled = NO;
    }
    else
    {
        self.editButton.hidden = YES;
        
        self.addPhotoButton.userInteractionEnabled = YES;
        
        self.allowEditing = YES;
    }
}


- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
}


- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    
    self.infoImageView = nil;
    self.infoContainer = nil;
    self.imageView = nil;
    self.photoContainerView = nil;
    self.photoView = nil;
    self.addPhotoPopover = nil;
    self.photoContentView = nil;
    self.currentTextField = nil;
    self.backButton = nil;
    self.addPhotoButton = nil;
    self.addPhotoLabel = nil;
    self.headerLabel = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    //    return YES;
    
    return UIInterfaceOrientationIsLandscape(interfaceOrientation);
}

- (void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
    self.backButton = nil;
    self.currentTextField = nil;
    self.contact = nil;
    self.infoImageView = nil;
    self.infoContainer = nil;
    self.imageView = nil;
    self.addPhotoPopover = nil;
    self.mailController = nil;
    self.photoContentView = nil;
    self.photoContainerView = nil;
    self.photoView = nil;
    self.addPhotoButton = nil;
    self.addPhotoLabel = nil;
    self.headerLabel = nil;
    self.street = nil;
    self.city = nil;
    self.zip = nil;
    self.country = nil;
    self.name = nil;
    self.lastname = nil;
    self.phone = nil;
    self.skype = nil;
    
    self.email = nil;
    self.email1 = nil;
    self.email2 = nil;
    self.email3 = nil;
    self.email4 = nil;
    self.email5 = nil;
    
    if (icon)
    {
        [icon release];
        icon = nil;
    }
    
    [self.alertPopoverController dismissPopoverAnimated:NO];
    self.alertPopoverController = nil;
    
    [super dealloc];
}

- (IBAction)editButtonTapped
{
//    ActionAlertView *alertView = [[[ActionAlertView alloc] initWithTitle:NSLocalizedString(@"What number would you like to call?", @"What number would you like to call?")
//                                                                 message:nil 
//                                                                delegate:self 
//                                                       cancelButtonTitle:nil 
//                                                       otherButtonTitles:NSLocalizedString(@"Home", @"Home"), 
//                                   NSLocalizedString(@"office", @"office"), 
//                                   NSLocalizedString(@"Mobile", @"Mobile"), 
//                                   NSLocalizedString(@"SKYPE", @"SKYPE"), nil] autorelease];
//    alertView.tag = 1;
//    [alertView show];
    
    self.editButton.hidden = YES;
    self.deleteButton.hidden = NO;
    
    self.allowEditing = YES;
}

- (IBAction)deleteButtonTapped
{
    if ([contact isFavorite])
    {
        Favorite *favoriteTable = [[DataManager sharedInstance] getEntityWithName:@"Favorite"];
        
        NSMutableArray *favorites = [NSMutableArray arrayWithArray:favoriteTable.contacts];
        
        if (favorites && [favorites count])
        {
            for (NSString *favorite in favorites)
            {
                if ([contact.recId intValue] == [favorite intValue])
                {
                    [favorites removeObject:favorite];
                    break;
                }
            }
        }
        
        NSMutableDictionary *emails = [NSMutableDictionary dictionaryWithDictionary:favoriteTable.emails];
        
        if ([emails count])
        {
            NSString *key = [contact.recId stringValue];
            
            NSString *value = [emails objectForKey:key];
            
            if (value)
            {
                [emails removeObjectForKey:key];
            }
        }
        
        favoriteTable.contacts = favorites;
        favoriteTable.emails = emails;
        [[DataManager sharedInstance] save];
    }
    
    [[GRPeopleManager sharedManager] removeContactFromAddressBook:contact withCompletionBlock:^{
        
        [delegate popContactCard:self];
    }];
}

- (IBAction)deleteDraftButtonTapped
{
    [delegate popContactCard:self];
}

- (BOOL)validateEmailAdress:(NSString*)candidate
{
	NSString *emailRegEx = @"[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}"; 
	NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegEx]; 
	return [emailTest evaluateWithObject:[candidate lowercaseString]];
}

- (void)popoverButtonTapped:(UIButton*)button
{
    [self.alertPopoverController dismissPopoverAnimated:YES];
    self.alertPopoverController = nil;
}

- (IBAction)saveDraftButtonTapped
{
    [self.currentTextField resignFirstResponder];
 
    if (!self.editButton.hidden)
    {
        [delegate popContactCard:self];
        
        return ;
    }
    
    if ([self.email length] && ![self validateEmailAdress:self.email])
    {
        ContactInfoTableViewCell *cell = (ContactInfoTableViewCell*)[self.tableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:2]];
        
        GRAlertViewController *contentViewController = [[[GRAlertViewController alloc] init] autorelease];
        
        if (self.isSettings)
        {
            contentViewController.contentSizeForViewInPopover = CGSizeMake(325, 175);
        }
        else
        {
            contentViewController.contentSizeForViewInPopover = CGSizeMake(512, 270);
        }

        [contentViewController view];
        
        if (!self.isSettings)
        {
            contentViewController.messageLabel.text = NSLocalizedString(@"e-mail is incorrect.\nPlease type it again.\n", @"e-mail is incorrect.\nPlease type it again.\n");
        }
        else
        {
            contentViewController.messageLabel.text = NSLocalizedString(@"Please enter a correct \n email address here", @"Please enter a correct \n email address here");
        }
        
        if (self.isSettings)
        {
            contentViewController.messageLabel.font = [UIFont fontWithName:@"Helvetica" size:18];
            
            [contentViewController.okButton setBackgroundImage:nil forState:UIControlStateNormal];
            contentViewController.okButton.frame = CGRectMake(0, contentViewController.okButton.frame.origin.y + 30, 50, 28);
            contentViewController.okButton.center = CGPointMake(contentViewController.view.bounds.size.width / 2, contentViewController.okButton.center.y);
            contentViewController.okButton.layer.borderWidth = 1.f;
            contentViewController.okButton.layer.borderColor = RGBCOLOR(190, 190, 190).CGColor;
            [contentViewController.okButton setTitleColor:RGBCOLOR(96, 96, 96) forState:UIControlStateNormal];
            contentViewController.okButton.titleLabel.font = [UIFont fontWithName:@"Helvetica" size:14];
        }
        
        [contentViewController.okButton addTarget:self action:@selector(popoverButtonTapped:) forControlEvents:UIControlEventTouchUpInside];
        
        self.alertPopoverController = [[[WYPopoverController alloc] initWithContentViewController:contentViewController] autorelease];
        self.alertPopoverController.delegate = self;
        
        if (!self.isSettings)
        {
            CGRect textRect = CGRectOffset(cell.infoTextField.frame, -180, 0);
            
            CGRect rect = [cell convertRect:textRect toView:self.view];
            
            [self.alertPopoverController presentPopoverFromRect:rect inView:self.view permittedArrowDirections:WYPopoverArrowDirectionUp animated:YES];
        }
        else
        {
            [self.alertPopoverController presentPopoverAsDialogAnimated:YES];
        }
        
        return ;
    }
    
    NSMutableArray *array = [NSMutableArray arrayWithArray:[self.fullname componentsSeparatedByCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]]];
    
    self.lastname = [array lastObject];
    [array removeLastObject];
    
    self.name = [array lastObject];
    [array removeLastObject];
    
    if (![self.name length] && ![self.lastname length])
    {
        ContactInfoTableViewCell *cell = (ContactInfoTableViewCell*)[self.tableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0]];
        
        GRAlertViewController *contentViewController = [[[GRAlertViewController alloc] init] autorelease];
        
        
        if (self.isSettings)
        {
            contentViewController.contentSizeForViewInPopover = CGSizeMake(325, 175);
        }
        else
        {
            contentViewController.contentSizeForViewInPopover = CGSizeMake(512, 270);
        }
        
        [contentViewController view];
        
        if (!self.isSettings)
        {
            contentViewController.messageLabel.text = NSLocalizedString(@"Please type \n a name", @"Please type \n a name");
//            contentViewController.messageLabel.text = NSLocalizedString(@"Please select a recipient", @"Please select a recipient");
        }
        else
        {
            contentViewController.messageLabel.text = NSLocalizedString(@"Please type a name", @"Please type a name");
        }
        
        if (self.isSettings)
        {
            contentViewController.messageLabel.font = [UIFont fontWithName:@"Helvetica" size:18];
            
            [contentViewController.okButton setBackgroundImage:nil forState:UIControlStateNormal];
            contentViewController.okButton.frame = CGRectMake(0, contentViewController.okButton.frame.origin.y + 30, 50, 28);
            contentViewController.okButton.center = CGPointMake(contentViewController.view.bounds.size.width / 2, contentViewController.okButton.center.y);
            contentViewController.okButton.layer.borderWidth = 1.f;
            contentViewController.okButton.layer.borderColor = RGBCOLOR(190, 190, 190).CGColor;
            [contentViewController.okButton setTitleColor:RGBCOLOR(96, 96, 96) forState:UIControlStateNormal];
            contentViewController.okButton.titleLabel.font = [UIFont fontWithName:@"Helvetica" size:14];
        }
        else
        {
            contentViewController.messageLabel.font = [UIFont fontWithName:@"Helvetica-Bold" size:48];
            contentViewController.messageLabel.frame = CGRectMake(contentViewController.messageLabel.frame.origin.x, contentViewController.messageLabel.frame.origin.y + 20, contentViewController.messageLabel.frame.size.width, contentViewController.messageLabel.frame.size.height);
        }
        
        [contentViewController.okButton addTarget:self action:@selector(popoverButtonTapped:) forControlEvents:UIControlEventTouchUpInside];
        
        self.alertPopoverController = [[[WYPopoverController alloc] initWithContentViewController:contentViewController] autorelease];
        self.alertPopoverController.delegate = self;
        
        CGRect textRect = CGRectOffset(cell.infoTextField.frame, -180, 0);
        
        CGRect rect = [cell convertRect:textRect toView:self.view];
        
        [self.alertPopoverController presentPopoverFromRect:rect inView:self.view permittedArrowDirections:WYPopoverArrowDirectionUp animated:YES];
        
        return ;
    }
    
    __block NSMutableDictionary *info = [@{} mutableCopy];
    
    [info setObjectIfNotNil:self.phone forKey:@"mobilePhone"];
    [info setObjectIfNotNil:self.iphone forKey:@"mobileiPhone"];
    [info setObjectIfNotNil:self.homePhone forKey:@"mobileHome"];
    [info setObjectIfNotNil:self.workPhone forKey:@"mobileWork"];
    [info setObjectIfNotNil:self.mainPhone forKey:@"mobileMain"];
    [info setObjectIfNotNil:self.faxPhoneHome forKey:@"faxHome"];
    [info setObjectIfNotNil:self.faxPhoneWork forKey:@"faxWork"];
    [info setObjectIfNotNil:self.faxPhoneOther forKey:@"faxOther"];
    [info setObjectIfNotNil:self.Pager forKey:@"pager"];
    [info setObjectIfNotNil:self.name forKey:@"name"];
    
    [info setObjectIfNotNil:self.email forKey:@"email"];
    [info setObjectIfNotNil:self.email1 forKey:@"email1"];
    [info setObjectIfNotNil:self.email2 forKey:@"email2"];
    [info setObjectIfNotNil:self.email3 forKey:@"email3"];
    [info setObjectIfNotNil:self.email4 forKey:@"email4"];
    [info setObjectIfNotNil:self.email5 forKey:@"email5"];
    
    [info setObjectIfNotNil:self.lastname forKey:@"lastname"];
    [info setObjectIfNotNil:self.street forKey:@"street"];
    [info setObjectIfNotNil:self.zip forKey:@"zip"];
    [info setObjectIfNotNil:self.city forKey:@"city"];
    [info setObjectIfNotNil:self.country forKey:@"country"];
    [info setObjectIfNotNil:self.skype forKey:@"skype"];
    [info setObjectIfNotNil:@"" forKey:@"icon"];
    [info setObjectIfNotNil:contact.recId forKey:@"recId"];
    [info setObjectIfNotNil:@(self.favorite) forKey:@"favorite"];
    
    if (icon)
    {
        NSData *imgData = UIImageJPEGRepresentation(icon, 1.f);
        
        [info setObjectIfNotNil:imgData forKey:@"iconData"];
    }
    
    [[GRPeopleManager sharedManager] createContactWithInfo:info withCompletionBlock:^(NSInteger recId){
        
        if (contact)
        {
            contact.phoneMobile = self.phone;
            contact.phoneiPhone = self.iphone;
            contact.phoneHome = self.homePhone;
            contact.phoneWork = self.workPhone;
            contact.phoneMain = self.mainPhone;
            contact.phoneHomeFax = self.faxPhoneHome;
            contact.phoneWorkFax = self.faxPhoneWork;
            contact.phoneOtherFax = self.faxPhoneOther;
            contact.phonePager = self.Pager;
            
            contact.firstname = self.name;
            contact.lastname = self.lastname;
            
            NSString *firstNameStr = @"";
            NSString *lastNameStr = @"";
            
            if (contact.firstname)
            {
                firstNameStr = contact.firstname;
            }
            
            if (contact.lastname)
            {
                lastNameStr = contact.lastname;
            }
            
            NSString *fullnameStr = [NSString stringWithFormat:@"%@ %@", firstNameStr, lastNameStr];
            
            self.fullname = fullnameStr;
            
            contact.email = self.email;
            contact.email1 = self.email1;
            contact.email2 = self.email2;
            contact.email3 = self.email3;
            contact.email4 = self.email4;
            contact.email5 = self.email5;
            
            contact.street = self.street;
            contact.postCode = self.zip;
            contact.city = self.city;
            contact.country = self.country;
            contact.phoneSkype = self.skype;
            
            if (icon)
            {
                NSArray *paths = NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES);
                NSString *dirPath = [paths objectAtIndex:0];
                NSString *prevdirPath = [paths objectAtIndex:0];
                
                NSString *fileName = [[NSString stringWithFormat:@"%i", [contact.recId integerValue]] stringByAppendingPathExtension:@"png"];
                NSString *prevFileName = [[NSString stringWithFormat:@"preview%i", [contact.recId integerValue]] stringByAppendingPathExtension:@"png"];
                
                dirPath = [dirPath stringByAppendingPathComponent:fileName];
                prevdirPath = [prevdirPath stringByAppendingPathComponent:prevFileName];
                
                contact.photourl = dirPath;
                contact.photoPreviewUrl = prevdirPath;
            }
        }
        else
        {
            if (self.favorite)
            {
                Favorite *favoriteTable = [[DataManager sharedInstance] getEntityWithName:@"Favorite"];
                
                NSMutableArray *favorites = [favoriteTable.contacts mutableCopy];
                
                if (!favorites)
                {
                    favorites = [[NSMutableArray array] retain];
                }
                
                NSString *rec = [NSString stringWithFormat:@"%i", recId];
                
                [favorites addObject:rec];
                
                favoriteTable.contacts = favorites;
                [[DataManager sharedInstance] save];
                [favorites release];
            }
        }
        
        [delegate popContactCard:self];
        
        [info release];
        info = nil;
    } withFailedBlock:^{
        
        [delegate popContactCard:self];
        
        [info release];
        info = nil;
    }];
}

- (IBAction)backButtonTapped
{
    [self.currentTextField resignFirstResponder];
    self.currentTextField = nil;
    
    [delegate popContactCard:self];
}

- (IBAction)okButtonTapped
{
    [self.addPhotoPopover removeFromSuperview];
}

- (IBAction)addPhotoButtonTapped
{
    if (IS_IPAD)
    {
        if (self.isSettings)
        {
            self.addPhotoPopover.frame = CGRectMake(0, 0, self.addPhotoPopover.frame.size.width, self.addPhotoPopover.frame.size.height);
            
            [self.view addSubview:self.addPhotoPopover];
        }
        else
        {
            [self.view addSubview:self.addPhotoPopover];
//            [self.infoContainer addSubview:self.addPhotoPopover];
            
//            self.addPhotoPopover.frame = CGRectMake(100, -10, self.addPhotoPopover.frame.size.width, self.addPhotoPopover.frame.size.height);
        }
    }
    else
    {
        //self.addPhotoPopover.frame = CGRectMake(40, 0, self.addPhotoPopover.frame.size.width, self.addPhotoPopover.frame.size.height);
        self.addPhotoButton.center = CGPointMake(self.view.bounds.size.width / 2, self.view.bounds.size.height / 2);
    }
    
    [self.currentTextField resignFirstResponder];
}

- (void)startLoadingPhotoFromGallery
{
//#if TARGET_IPHONE_SIMULATOR
//#else
    void (^assetEnumerator)(ALAsset *, NSUInteger, BOOL *) = ^(ALAsset *result, NSUInteger index, BOOL *stop) {
        if(result != NULL) {
            [assets addObject:result];
        }
    };
    
    void (^assetGroupEnumerator)(ALAssetsGroup *, BOOL *) =  ^(ALAssetsGroup *group, BOOL *stop) {
    	if(group != nil) {
            [group enumerateAssetsUsingBlock:assetEnumerator];
        } else {
            [self.photoContentView reloadData];
        }
    };
    
    assets = [[NSMutableArray alloc] init];
    library = [[ALAssetsLibrary alloc] init];
    [library enumerateGroupsWithTypes:ALAssetsGroupSavedPhotos
                           usingBlock:assetGroupEnumerator
                         failureBlock: ^(NSError *error) {
                             NSLog(@"Failure");
                         }];
//#endif
    
}

#pragma mark -
#pragma mark MFMailComposeViewControllerDelegate's methods

- (void)mailComposeController:(MFMailComposeViewController *)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError *)error
{
    [self.mailController dismissViewControllerAnimated:YES completion:^{
        
    }];
}

#pragma mark - AlertViewDelegate's methods

- (void)alertView:(AlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (alertView.tag == 1)
    {
        if (buttonIndex == 0)
        {
            if (IS_IPAD)
            {
                [[[[UIAlertView alloc] initWithTitle:@"" 
                                             message:NSLocalizedString(@"This device doesn't support telephone calls", @"This device doesn't support telephone calls")
                                            delegate:nil 
                                   cancelButtonTitle:NSLocalizedString(@"OK", @"OK") 
                                   otherButtonTitles:nil] autorelease] show];
                
            }
        }
        else if (buttonIndex == 1)
        {
            [[[[UIAlertView alloc] initWithTitle:@"" 
                                         message:NSLocalizedString(@"This device doesn't support telephone calls", @"This device doesn't support telephone calls")
                                        delegate:nil 
                               cancelButtonTitle:NSLocalizedString(@"OK", @"OK") 
                               otherButtonTitles:nil] autorelease] show];
            
        }
        else if (buttonIndex == 2)
        {
            [[[[UIAlertView alloc] initWithTitle:@"" 
                                         message:NSLocalizedString(@"This device doesn't support telephone calls", @"This device doesn't support telephone calls") 
                                        delegate:nil 
                               cancelButtonTitle:NSLocalizedString(@"OK", @"OK") 
                               otherButtonTitles:nil] autorelease] show];
            
        }
        else if (buttonIndex == 3)
        {
            if ([contact.phoneSkype length])
            {
                NSString* urlString = [NSString stringWithFormat:@"skype://%@?call", contact.phoneSkype];
                
                [[UIApplication sharedApplication] openURL:[NSURL URLWithString:urlString]];
            }
            else
            {
                [[[[UIAlertView alloc] initWithTitle:@"" 
                                             message:NSLocalizedString(@"Skype id is absent", @"Skype id is absent") 
                                            delegate:nil 
                                   cancelButtonTitle:NSLocalizedString(@"OK", @"OK") 
                                   otherButtonTitles:nil] autorelease] show];
            }
        }
    }
    else
    {
        [[[[UIAlertView alloc] initWithTitle:@"" 
                                     message:NSLocalizedString(@"Hasn't been implemented yet.", @"Hasn't been implemented yet.") 
                                    delegate:nil 
                           cancelButtonTitle:NSLocalizedString(@"OK", @"OK") 
                           otherButtonTitles:nil] autorelease] show];
    }
}

- (void)alertViewCancel:(AlertView *)alertView
{
    
}

- (void)willPresentAlertView:(AlertView *)alertView
{
    
}

- (void)didPresentAlertView:(AlertView *)alertView
{
    
}

- (void)alertView:(AlertView *)alertView willDismissWithButtonIndex:(NSInteger)buttonIndex
{
    
}

- (void)alertView:(AlertView *)alertView didDismissWithButtonIndex:(NSInteger)buttonIndex
{
    
}

#pragma mark - UITextFieldDelegate's methods

- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField
{
    self.currentTextField = textField;
    return YES;
}

- (void)textFieldDidBeginEditing:(UITextField *)textField
{
    
}

- (BOOL)textFieldShouldEndEditing:(UITextField *)textField
{
    return YES;
}

- (void)textFieldDidEndEditing:(UITextField *)textField
{
    NSInteger section = [textField tag] / 10;
    NSInteger row = textField.tag - section * 10;
    
    if (section == 4)
    {
        switch (row)
        {
            case 0:         //street
                self.street = textField.text;
                break;
                
            case 1:         //city
                self.city = textField.text;
                break;
                
            case 2:         //zip
                self.zip = textField.text;
                break;
                
            case 3:         //country
                self.country = textField.text;
                break;
        }
    }
    else if (section == 1)
    {
        switch (row)
        {
            case 0:         //phone
                self.phone = textField.text;
                break;
                
            case 1:         //iPhone
                self.iphone = textField.text;
                break;
                
            case 2:         //Home
                self.homePhone = textField.text;
                break;
                
            case 3:         //Work
                self.workPhone = textField.text;
                break;
                
            case 4:         //Main
                self.mainPhone = textField.text;
                break;
                
            case 5:         //Fax Home
                self.faxPhoneHome = textField.text;
                break;
                
            case 6:         //Fax Work
                self.faxPhoneWork = textField.text;
                break;
                
            case 7:         //Fax Other
                self.faxPhoneOther = textField.text;
                break;
                
            case 8:         //Fax Other
                self.faxPhoneOther = textField.text;
                break;
        }
    }
    else if (section == 2)
    {
        switch (row)
        {
            case 0:         //email
                self.email = textField.text;
                break;
                
            case 1:         //email
                self.email1 = textField.text;
                break;
                
            case 2:         //email
                self.email2 = textField.text;
                break;
                
            case 3:         //email
                self.email3 = textField.text;
                break;
                
            case 4:         //email
                self.email4 = textField.text;
                break;
                
            case 5:         //email
                self.email5 = textField.text;
                break;
        }
    }
    else if (section == 3)
    {
        switch (row)
        {
            case 0:         //skype
                self.skype = textField.text;
                break;
        }
    }
    else if (section == 0)
    {
        switch (row)
        {
            case 0:         //name
                self.fullname = textField.text;
                break;
        }
    }
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    NSInteger section = [textField tag] / 10;
    NSInteger row = textField.tag - section * 10;
    
    if (section == 1)
    {
        if(textField.text.length - range.length + string.length > kMaxPhoneDigitsCount) 
        {
            return NO;
        }
        
        NSString *template = @"0123456789)(+-#";
        
        if ([string length] > 0) 
        {
            NSRange range = [template rangeOfString:string];
            
            if (range.location != NSNotFound)
            {
                return YES;
            }
            
            return NO;
        }
    }
    else if ((section == 4 && (row == 0 || row == 1 || row == 3)) || (section == 0) || (section == 3))
    {
        if(textField.text.length - range.length + string.length > kMaxTextSymbolsCount) 
        {
            return NO;
        }        
    }
    else if (section == 2)
    {
        if(textField.text.length - range.length + string.length > kMaxEmailSymbolsCount) 
        {
            return NO;
        } 
    }
    else if (section == 4 && row == 2)
    {
        if(textField.text.length - range.length + string.length > kMaxZipDigitsCount) 
        {
            return NO;
        }
        
//        NSString *template = @"0123456789";
//        
//        if ([string length] > 0) 
//        {
//            NSRange range = [template rangeOfString:string];
//            
//            if (range.location != NSNotFound)
//            {
//                return YES;
//            }
//            
//            return NO;
//        }
    }
    
	return YES;
}

- (BOOL)textFieldShouldClear:(UITextField *)textField
{
    return YES;
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [self.currentTextField resignFirstResponder];
    return YES;
}

#pragma mark - BDGridViewDataSource's methods

- (NSUInteger)gridViewCountOfCells:(BDGridView *)gridView
{
    return [assets count];
}

- (CGSize)gridViewSizeOfCell:(BDGridView *)gridView
{
    if (IS_IPAD)
    {
        CGFloat width = (gridView.bounds.size.width - 4 * gridView.minimumPadding)/ 5;
        
        return CGSizeMake(width, width);
    }
    else
    {
        CGFloat width = (gridView.bounds.size.width - 2 * gridView.minimumPadding)/ 3;
        
        return CGSizeMake(width, width);
    }
    
    return CGSizeZero;
}

- (BDGridCell *)gridView:(BDGridView *)gridView cellForIndex:(NSUInteger)index
{
    ALAsset *asset = [assets objectAtIndex:index];
    UIImage *pImage = nil;
    
    if ([[[UIDevice currentDevice] systemVersion] floatValue] >= 5.f)
    {
        pImage = [[UIImage imageWithCGImage:[asset aspectRatioThumbnail]] imageWithBorderWidth:3 andColor:[UIColor whiteColor].CGColor];
    }
    else
    {
        pImage = [[UIImage imageWithCGImage:[asset thumbnail]] imageWithBorderWidth:3 andColor:[UIColor whiteColor].CGColor];
    }
    
    PhotoCell *cell = (PhotoCell*)[gridView dequeueCell];
    
    if (!cell)
    {
        cell = [[[PhotoCell alloc] initCellWithRoundedCorners] autorelease];
    }
    
    cell.photoImageView.image = pImage;
    
    if (!self.isSettings)
    {
        cell.layer.shadowOpacity = 0.f;
        cell.layer.cornerRadius = 20.f;
        cell.layer.shadowOffset = CGSizeZero;
        cell.photoImageView.layer.cornerRadius = 20.f;
        cell.layer.borderWidth = 1.f;
        cell.layer.borderColor = RGBCOLOR(156, 160, 156).CGColor;
    }
    
    return cell;
}

#pragma mark - BDGridViewDelegate's methods

- (void)gridView:(BDGridView *)gridView didTapCell:(BDGridCell *)cell
{
    if (cell.isSelected)
    {
        [gridView unselectAllCells];
        
        if (icon)
        {
            [icon release];
            icon = nil;
        }
        
        imageView.image = nil;
    }
    else
    {
        [gridView unselectAllCells];
        [gridView selectCell:cell];
        
        ALAsset *asset = [assets objectAtIndex:cell.index];
        
        if (icon)
        {
            [icon release];
            icon = nil;
        }
        
        icon = [UIImage imageWithCGImage:[[asset defaultRepresentation] fullScreenImage]];
        
        [icon retain];
        
        imageView.image = icon;
    }
    
//    [self.addPhotoPopover removeFromSuperview];
}


#pragma mark - UITableViewDataSource's methods

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (section == 4)
    {
        return 4;
    }
    else if (section == 1)
    {
        return 9;
    }
    else if (section == 2)
    {
        return 6;
    }
    
    return 1;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 5;
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
    NSInteger height = [self heightForFooterInSection:section];
    
    return height;
}

- (CGFloat)heightForFooterInSection:(NSInteger)section
{
//    if (section == 1)
//    {
//        if (![self.contact.phoneMobile length] &&
//            ![self.contact.phoneiPhone length] &&
//            ![self.contact.phoneHome length] &&
//            ![self.contact.phoneWork length] &&
//            ![self.contact.phoneMain length] &&
//            ![self.contact.phoneHomeFax length] &&
//            ![self.contact.phoneWorkFax length] &&
//            ![self.contact.phoneOtherFax length] &&
//            ![self.contact.phonePager length])
//        {
//            return 0;
//        }
//    }
//    
//    if (section == 2)
//    {
//        if (![self.contact.email length] &&
//            ![self.contact.email1 length] &&
//            ![self.contact.email2 length] &&
//            ![self.contact.email3 length] &&
//            ![self.contact.email4 length] &&
//            ![self.contact.email5 length])
//        {
//            return 0;
//        }
//    }
//    
//    if (section == 3)
//    {
//        if (![self.contact.phoneSkype length])
//        {
//            return 0;
//        }
//    }
//    
//    if (section == 4)
//    {
//        if (![self.contact.street length]
//            && ![self.contact.city length]
//            && ![self.contact.postCode length]
//            && ![self.contact.country length])
//        {
//            return 0;
//        }
//    }
    
    if (section == 4 && self.isSettings)
    {
        return 0.f;
    }
    
    return 20.f;
}

- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section
{
    if (section == 4 && self.isSettings)
    {
        return nil;
    }
    
    NSInteger height = [self heightForFooterInSection:section];
    
    UIView *headerView = [[[UIView alloc] initWithFrame:CGRectMake(0, 0, tableView.bounds.size.width, height)] autorelease];
    
    headerView.backgroundColor = [UIColor clearColor];
    
    UIView *separator = [[[UIView alloc] initWithFrame:CGRectMake(0, 0, self.tableView.bounds.size.width, 1)] autorelease];
    
    separator.backgroundColor = RGBCOLOR(190, 190, 190);
    
    separator.autoresizingMask = UIViewAutoresizingFlexibleWidth;
    
    [headerView addSubview:separator];
    
    separator.center = CGPointMake(headerView.bounds.size.width / 2, 10);
    
    return headerView;
}

- (UITableViewCell *)tableView:(UITableView *)tableView
         cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    ContactInfoTableViewCell *cell = (ContactInfoTableViewCell*)[tableView dequeueReusableCellWithIdentifier:@"Cell"];
    
    if (!cell)
    {
        cell = (ContactInfoTableViewCell*)[[[NSBundle mainBundle] loadNibNamed:@"ContactInfoTableViewCell" owner:nil options:nil] lastObject];
    }
    
    if (self.isSettings)
    {
        cell.xOffsetForInfoField = 100;
    }
    
    cell.infoTextField.delegate = self;
    cell.infoTextField.tag = indexPath.section * 10 + indexPath.row;
    
    cell.infoTextField.userInteractionEnabled = self.allowEditing;
    
    if (self.isSettings)
    {
        cell.infoTextField.font = [UIFont fontWithName:@"Helvetica" size:18];
        cell.titleLabel.font = [UIFont fontWithName:@"Helvetica" size:18];
        
        cell.titleLabel.textColor = RGBCOLOR(96, 96, 96);
    }
    else
    {
        cell.infoTextField.font = [UIFont fontWithName:@"Helvetica-Bold" size:38];
        cell.titleLabel.font = [UIFont fontWithName:@"Helvetica-Bold" size:38];
        cell.subTitleLabel.font = [UIFont fontWithName:@"Helvetica-Bold" size:38];
        
        cell.titleLabel.textColor = RGBCOLOR(151, 151, 151);
        cell.subTitleLabel.textColor = RGBCOLOR(151, 151, 151);
        cell.infoTextField.textColor = RGBCOLOR(66, 66, 66);
    }
    
    if (indexPath.section == 0)
    {
        if (indexPath.row == 0)
        {
            if (self.isSettings)
            {
                cell.titleLabel.text = NSLocalizedString(@"Name :", @"Name :");
            }
            else
            {
                cell.titleLabel.text = NSLocalizedString(@"Name:", @"Name:");
            }
        }
        
        NSString *firstName = @"";
        NSString *lastName = @"";
        
        if (contact.firstname)
        {
            firstName = contact.firstname;
        }
        
        if (contact.lastname)
        {
            lastName = contact.lastname;
        }
        
        NSString *fullnameStr = [NSString stringWithFormat:@"%@ %@", firstName, lastName];
        
        fullnameStr = [fullnameStr stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
        
        cell.infoTextField.text = fullnameStr;
//        cell.infoTextField.placeholder = NSLocalizedString(@"Name", @"Name");
    }
    
    if (indexPath.section == 1)
    {
        switch (indexPath.row)
        {
            case 0:
                
                if (![contact.phoneMobile length] &&
                    ![contact.phoneiPhone length] &&
                    ![contact.phoneHome length] &&
                    ![contact.phoneWork length] &&
                    ![contact.phoneMain length] &&
                    ![contact.phoneHomeFax length] &&
                    ![contact.phoneWorkFax length] &&
                    ![contact.phoneOtherFax length] &&
                    ![contact.phonePager length])
                {
                    if (self.isSettings)
                    {
                        cell.titleLabel.text = NSLocalizedString(@"Phone :", @"Phone :");
                    }
                    else
                    {
                        cell.titleLabel.text = NSLocalizedString(@"Phone:", @"Phone:");
                    }
                }
                else
                {
                    cell.titleLabel.text = [self.contact titleForPhoneSectionForPhone:self.contact.phoneMobile];
                }
                
//                cell.infoTextField.placeholder = NSLocalizedString(@"Phone", @"Phone");
                cell.infoTextField.text = self.contact.phoneMobile;
                break;
                
            case 1:
                cell.titleLabel.text = [self.contact titleForPhoneSectionForPhone:self.contact.phoneiPhone];
//                cell.infoTextField.placeholder = NSLocalizedString(@"iPhone", @"iPhone");
                cell.infoTextField.text = self.contact.phoneiPhone;
                break;
                
            case 2:
                cell.titleLabel.text = [self.contact titleForPhoneSectionForPhone:self.contact.phoneHome];
//                cell.infoTextField.placeholder = NSLocalizedString(@"Home", @"Home");
                cell.infoTextField.text = self.contact.phoneHome;
                break;
                
            case 3:
                cell.titleLabel.text = [self.contact titleForPhoneSectionForPhone:self.contact.phoneWork];
//                cell.infoTextField.placeholder = NSLocalizedString(@"Work", @"Work");
                cell.infoTextField.text = self.contact.phoneWork;
                break;
                
            case 4:
                cell.titleLabel.text = [self.contact titleForPhoneSectionForPhone:self.contact.phoneMain];
//                cell.infoTextField.placeholder = NSLocalizedString(@"Main", @"Main");
                cell.infoTextField.text = self.contact.phoneMain;
                break;
                
            case 5:
                cell.titleLabel.text = [self.contact titleForPhoneSectionForPhone:self.contact.phoneHomeFax];
//                cell.infoTextField.placeholder = NSLocalizedString(@"Fax Home", @"Fax Home");
                cell.infoTextField.text = self.contact.phoneHomeFax;
                break;
                
            case 6:
                cell.titleLabel.text = [self.contact titleForPhoneSectionForPhone:self.contact.phoneWorkFax];
//                cell.infoTextField.placeholder = NSLocalizedString(@"Fax Work", @"Fax Work");
                cell.infoTextField.text = self.contact.phoneWorkFax;
                break;
                
            case 7:
                cell.titleLabel.text = [self.contact titleForPhoneSectionForPhone:self.contact.phoneOtherFax];
//                cell.infoTextField.placeholder = NSLocalizedString(@"Fax Other", @"Fax Other");
                cell.infoTextField.text = self.contact.phoneOtherFax;
                break;
                
            case 8:
                cell.titleLabel.text = [self.contact titleForPhoneSectionForPhone:self.contact.phonePager];
//                cell.infoTextField.placeholder = NSLocalizedString(@"Pager", @"Pager");
                cell.infoTextField.text = self.contact.phonePager;
                break;
                
            default:
                break;
        }
    }
    
    if (indexPath.section == 2)
    {
        if (indexPath.row == 0)
        {
            if (self.isSettings)
            {
                cell.titleLabel.text = NSLocalizedString(@"Email :", @"Email :");
            }
            else
            {
                cell.titleLabel.text = NSLocalizedString(@"Email:", @"Email:");
            }
        }
        
//        cell.infoTextField.placeholder = NSLocalizedString(@"Email", @"Email");
        
        switch (indexPath.row)
        {
            case 0:
                cell.infoTextField.text = self.contact.email;
                break;
                
            case 1:
                cell.infoTextField.text = self.contact.email1;
                break;
                
            case 2:
                cell.infoTextField.text = self.contact.email2;
                break;
                
            case 3:
                cell.infoTextField.text = self.contact.email3;
                break;
                
            case 4:
                cell.infoTextField.text = self.contact.email4;
                break;
                
            case 5:
                cell.infoTextField.text = self.contact.email5;
                break;
                
            default:
                break;
        }
    }
    
    if (indexPath.section == 3)
    {
        if (indexPath.row == 0)
        {
            if (self.isSettings)
            {
                cell.titleLabel.text = NSLocalizedString(@"Skype :", @"Skype :");
            }
            else
            {
                cell.titleLabel.text = NSLocalizedString(@"Skype:", @"Skype:");
            }
        }
        
        cell.infoTextField.text = self.contact.phoneSkype;
        cell.infoTextField.placeholder = NSLocalizedString(@"To use Skype please enter the address here", @"To use Skype please enter the address here");
    }
    
    if (indexPath.section == 4)
    {
        if (indexPath.row == 0)
        {
            if (self.isSettings)
            {
                cell.titleLabel.text = NSLocalizedString(@"Address :", @"Address :");
            }
            else
            {
                cell.titleLabel.text = NSLocalizedString(@"Address:", @"Address:");
            }
            
            cell.infoTextField.text = self.contact.street;
            cell.infoTextField.placeholder = @"";
        }
        else if (indexPath.row == 1)
        {
            if (self.isSettings)
            {
                cell.titleLabel.text = NSLocalizedString(@"City :", @"City :");
            }
            else
            {
                cell.titleLabel.text = NSLocalizedString(@"City:", @"City:");
            }
            
            cell.infoTextField.text = self.contact.city;
            cell.infoTextField.placeholder = @"";
        }
        else if (indexPath.row == 2)
        {
            if (self.isSettings)
            {
                cell.titleLabel.text = NSLocalizedString(@"Zip :", @"Zip :");
            }
            else
            {
                cell.titleLabel.text = NSLocalizedString(@"Zip:", @"Zip:");
            }
            
            cell.infoTextField.text = self.contact.postCode;
            cell.infoTextField.placeholder = @"";
        }
        else if (indexPath.row == 3)
        {
            if (self.isSettings)
            {
                cell.titleLabel.text = NSLocalizedString(@"Country :", @"Country :");
            }
            else
            {
                cell.titleLabel.text = NSLocalizedString(@"Country:", @"Country:");
            }
            
            cell.infoTextField.text = self.contact.country;
            cell.infoTextField.placeholder = @"";
        }
    }
    
    return cell;
}

#pragma mark - UITableViewDelegate's methods

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    ContactInfoTableViewCell *cell = (ContactInfoTableViewCell*)[tableView cellForRowAtIndexPath:indexPath];
    
    [cell.infoTextField becomeFirstResponder];
    
//    if (indexPath.section == 1)
//    {
//        [self phoneButtonTappedWithIndexPath:indexPath];
//    }
//    else if (indexPath.section == 2)
//    {
//        [self emailButtonTapped];
//    }
//    else if (indexPath.section == 3)
//    {
//        [self skypeButtonTapped];
//    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.section == 1)
    {
        if (indexPath.row == 0)
        {
            if (![contact.phoneMobile length] &&
                ![contact.phoneiPhone length] &&
                ![contact.phoneHome length] &&
                ![contact.phoneWork length] &&
                ![contact.phoneMain length] &&
                ![contact.phoneHomeFax length] &&
                ![contact.phoneWorkFax length] &&
                ![contact.phoneOtherFax length] &&
                ![contact.phonePager length])
            {
                if (self.isSettings)
                {
                    return 30;
                }
                return 40;
            }
            else if (![contact.phoneMobile length])
            {
                return 0;
            }
        }
        else if (indexPath.row == 1)
        {
            if (![contact.phoneiPhone length])
            {
                return 0;
            }
        }
        else if (indexPath.row == 2)
        {
            if (![contact.phoneHome length])
            {
                return 0;
            }
        }
        else if (indexPath.row == 3)
        {
            if (![contact.phoneWork length])
            {
                return 0;
            }
        }
        else if (indexPath.row == 4)
        {
            if (![contact.phoneMain length])
            {
                return 0;
            }
        }
        else if (indexPath.row == 5)
        {
            if (![contact.phoneHomeFax length])
            {
                return 0;
            }
        }
        else if (indexPath.row == 6)
        {
            if (![contact.phoneWorkFax length])
            {
                return 0;
            }
        }
        else if (indexPath.row == 7)
        {
            if (![contact.phoneOtherFax length])
            {
                return 0;
            }
        }
        else if (indexPath.row == 8)
        {
            if (![contact.phonePager length])
            {
                return 0;
            }
        }
        
        //return 85;
    }
    
    if (indexPath.section == 2)
    {
        if (indexPath.row == 0)
        {
            if (![contact.email length] &&
                ![contact.email1 length] &&
                ![contact.email2 length] &&
                ![contact.email3 length] &&
                ![contact.email4 length] &&
                ![contact.email5 length])
            {
                if (self.isSettings)
                {
                    return 30;
                }
                
                return 40;
            }
            else if (![contact.email length])
            {
                return 0;
            }
        }
        else if (indexPath.row == 1)
        {
            if (![contact.email1 length])
            {
                return 0;
            }
        }
        else if (indexPath.row == 2)
        {
            if (![contact.email2 length])
            {
                return 0;
            }
        }
        else if (indexPath.row == 3)
        {
            if (![contact.email3 length])
            {
                return 0;
            }
        }
        else if (indexPath.row == 4)
        {
            if (![contact.email4 length])
            {
                return 0;
            }
        }
        else if (indexPath.row == 5)
        {
            if (![contact.email5 length])
            {
                return 0;
            }
        }
    }
    
//    if (indexPath.section == 4)
//    {
//        if (indexPath.row == 0)
//        {
//            //            if (![contact.street length])
//            //            {
//            //                return 0;
//            //            }
//        }
//        else if (indexPath.row == 1)
//        {
//            if (![contact.city length])
//            {
//                return 0;
//            }
//        }
//        else if (indexPath.row == 2)
//        {
//            if (![contact.postCode length])
//            {
//                return 0;
//            }
//        }
//        else if (indexPath.row == 3)
//        {
//            if (![contact.country length])
//            {
//                return 0;
//            }
//        }
//    }
    
    if (!self.isSettings)
    {
        return 45.f;
    }
    else
    {
        return 35.f;
    }
}

#pragma mark - WYPopoverControllerDelegate's methods

- (BOOL)popoverControllerShouldDismissPopover:(WYPopoverController *)popoverController
{
    return YES;
}

- (void)popoverControllerDidDismissPopover:(WYPopoverController *)popoverController
{
    self.alertPopoverController = nil;
}

@end
