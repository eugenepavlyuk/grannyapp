//
//  EmailActionView.h
//  GrannyApp
//
//  Created by Eugene Pavluk on 1/30/14.
//
//

#import <UIKit/UIKit.h>

@interface EmailActionView : UIView

@property (nonatomic, retain) IBOutlet UIButton *readButton;
@property (nonatomic, retain) IBOutlet UIButton *writeButton;
@property (nonatomic, retain) IBOutlet UIButton *cancelButton;

@end
