//
//  GRContactContainerView.h
//  GrannyApp
//
//  Created by Eugene Pavluk on 11/28/13.
//
//

#import <UIKit/UIKit.h>

@interface GRContactContainerView : UIView

@property (nonatomic, retain) IBOutlet UIView *container;
@property (nonatomic, retain) IBOutlet UILabel *titleLabel;
@property (nonatomic, retain) IBOutlet UIImageView *coverImageView;
@property (nonatomic, retain) IBOutlet UIButton *coverButton;

@property (nonatomic, retain) IBOutlet UIView *topSeparatorView;
@property (nonatomic, retain) IBOutlet UIView *bottomSeparatorView;

@end
