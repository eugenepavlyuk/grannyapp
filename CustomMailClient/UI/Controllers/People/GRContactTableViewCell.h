//
//  GRContactTableViewCell.h
//  GrannyApp
//
//  Created by admin on 26/11/13.
//
//

@class GRContactContainerView;

@protocol GRContactCellDelegate <NSObject>

- (void)willSelectView:(GRContactContainerView*)contactView
           atIndexPath:(NSIndexPath*)indexPath
              andIndex:(NSNumber*)index;

- (UIView*)viewForContactAtIndexPath:(NSIndexPath*)indexPath
                           withIndex:(NSNumber*)index;

- (void)contactTappedWithIndexPath:(NSIndexPath*)indexPath andIndex:(NSNumber*)index;

@end

@interface GRContactTableViewCell : UITableViewCell
{
    
}

@property (nonatomic, retain) IBOutlet UILabel *titleLabel;
@property (nonatomic, retain) NSIndexPath *indexPath;
@property (nonatomic, assign) NSObject<GRContactCellDelegate> *delegate;

@property (nonatomic, assign) NSInteger numberOfChildren;
@property (nonatomic, assign) CGSize childSize;

+ (CGFloat)cellHeight;
+ (NSString*)reuseIdentifier;

- (void)layoutContent;

@end
