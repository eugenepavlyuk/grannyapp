//
//  GRContactViewController.h
//  GrannyApp
//
//  Created by Eugene Pavlyuk on 7/12/12.
//  Copyright (c) 2012 Home. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MessageUI/MFMailComposeViewController.h>
#import "AlertView.h"
#import "GRCommonAlertView.h"
#import "GRContactCardDelegate.h"

@class GRContact;

@interface GRContactViewController : GAITrackedViewController <MFMailComposeViewControllerDelegate, AlertViewDelegate, GRCommonAlertDelegate, UITableViewDataSource, UITableViewDelegate>

@property (nonatomic, retain) GRContact *contact;

@property (nonatomic, retain) IBOutlet UIImageView *infoImageView;

@property (nonatomic, retain) IBOutlet UIImageView *imageView;

@property (nonatomic, retain) IBOutlet UITableView *tableView;

@property (nonatomic, retain) IBOutlet UIView *photoContainerView;
@property (nonatomic, retain) IBOutlet UIView *photoView;
@property (nonatomic, retain) IBOutlet UIView *infoContainerView;

@property (nonatomic, assign) UIViewController<GRContactCardDelegate> *delegate;

- (IBAction)callButtonTapped;
- (IBAction)mailButtonTapped;
- (IBAction)backButtonTapped;
- (IBAction)homeButtonTapped;

- (IBAction)skypeButtonTapped;

@end
