//
//  GRAllPeopleViewController.m
//  GrannyApp
//
//  Created by Eugene Pavlyuk on 5/22/12.
//  Copyright (c) 2012 Home. All rights reserved.
//

#import "GRAllPeopleViewController.h"
#import "GRContactViewController.h"
#import "GRNewContactViewController.h"
#import "BDGridCell.h"
#import "GRContact.h"
#import "Favorite.h"
#import "DataManager.h"
#import "GRPeopleManager.h"
#import <QuartzCore/QuartzCore.h>
#import "MBProgressHUD.h"
#import "SOSViewController.h"
#import "GRFavoriteContactTableViewCell.h"
#import "GRContactView.h"
#import "GRFavoriteContactView.h"
#import "GRContactSearchBar.h"
#import "Settings.h"
#import "WYPopoverController.h"
#import "GRAlertViewController.h"

@interface GRAllPeopleViewController () <GRContactCellDelegate, WYPopoverControllerDelegate>

@property (nonatomic, retain) NSMutableArray *favoriteSearchContacts;
@property (nonatomic, retain) NSMutableArray *otherSearchContacts;

@property (nonatomic, retain) NSMutableArray *searchPeople;
@property (nonatomic, retain) Settings *settings;

@property (nonatomic, retain) SOSViewController *sosViewController;
@property (nonatomic, retain) GRContactContainerView *selectedContactView;

@property (nonatomic, retain) WYPopoverController *alertPopoverController;

@end

@implementation GRAllPeopleViewController
{
    BOOL isSearchActive;
}

@synthesize spinnerView;
@synthesize gridView;
@synthesize searchBar;
@synthesize searchPeople;

@synthesize favoriteSearchContacts;
@synthesize otherSearchContacts;

@synthesize headerImageView;

@synthesize contactViewController;
@synthesize contactNewViewController;

- (BOOL)prefersStatusBarHidden
{
    return YES;
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    
    if (self) 
    {
        isSearchActive = NO;
        
        self.searchPeople = [NSMutableArray array];
        self.favoriteSearchContacts = [NSMutableArray array];
        self.otherSearchContacts = [NSMutableArray array];
        
        [[NSNotificationCenter defaultCenter] addObserver:self 
                                                 selector:@selector(applicationDidBecomeActive:) 
                                                     name:UIApplicationDidBecomeActiveNotification 
                                                   object:nil];
        
        [[NSNotificationCenter defaultCenter] addObserver:self
                                                 selector:@selector(resyncingContactsNotification:)
                                                     name:kResyncingContactsNotification
                                                   object:nil];
        
        [[NSNotificationCenter defaultCenter] addObserver:self
                                                 selector:@selector(syncingContactsFinishedNotification:)
                                                     name:kSyncingContactsFinishedNotification
                                                   object:nil];
        
        [[NSNotificationCenter defaultCenter] addObserver:self
                                                 selector:@selector(contactSearchBarDidCancelNotification:)
                                                     name:GRContactSearchBarDidCancelNotification
                                                   object:nil];
    }
    
    return self;
}

- (Settings*)settings
{
    if (!_settings)
    {
        self.settings = [[DataManager sharedInstance] getEntityWithName:kSettingsEntityName];
    }
    
    return _settings;
}

- (void)resyncingContactsNotification:(NSNotification*)notification
{
    [self reloadTableView];
}

- (void)syncingContactsFinishedNotification:(NSNotification*)notification
{
    [self reloadTableView];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    
    self.screenName = @"People Screen";
    
    if ([[UIDevice currentDevice].systemVersion floatValue] >= 7.f)
    {
        [gridView setTintColor:[UIColor blackColor]];
    }
    
    [gridView reloadData];
    
    UIImage *headerBackground = nil;
    
    if (IS_IPAD)
    {
        headerBackground = [[UIImage imageNamed:@"people_header_background"] resizableImageWithCapInsets:UIEdgeInsetsMake(0, 40, 0, 40)];
    }
    else
    {
        headerBackground = [[UIImage imageNamed:@"people_header_background"] stretchableImageWithLeftCapWidth:36 topCapHeight:0];
    }
    
    self.headerImageView.image = headerBackground;
}

- (void)reloadTableView
{
    [self.spinnerView stopAnimating];
    [self.gridView reloadData];
}

- (void)applicationDidBecomeActive:(NSNotification*)notification
{
    [self.spinnerView startAnimating];
    
    [[GRPeopleManager sharedManager] updateContactsWithCompletionBlock:^{
        [self reloadTableView];
    }];
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    
    self.gridView = nil;
    self.searchBar = nil;
    
    self.headerImageView = nil;
    self.titleLabel = nil;
    self.spinnerView = nil;
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];

    MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    hud.userInteractionEnabled = NO;
    hud.labelText = NSLocalizedString(@"Sorting...", @"Sorting...");
    
    hud.mode = MBProgressHUDModeIndeterminate;
    
    [[GRPeopleManager sharedManager] updateContactsWithCompletionBlock:^{
        [self.gridView reloadData];
        
        [MBProgressHUD hideHUDForView:self.view animated:YES];
    }];
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return YES;
}

- (void)didRotateFromInterfaceOrientation:(UIInterfaceOrientation)fromInterfaceOrientation
{
    [super didRotateFromInterfaceOrientation:fromInterfaceOrientation];

    [self.gridView reloadRowsAtIndexPaths:[self.gridView indexPathsForVisibleRows] withRowAnimation:UITableViewRowAnimationNone];
}

- (void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
    self.gridView = nil;
    self.searchBar = nil;
    
    self.searchPeople = nil;
    self.titleLabel = nil;
    self.spinnerView = nil;
    self.headerImageView = nil;
    self.contactViewController = nil;
    self.contactNewViewController = nil;
    self.selectedContactView = nil;
    self.settings = nil;
    
    [self.alertPopoverController dismissPopoverAnimated:YES];
    self.alertPopoverController = nil;
    
    [super dealloc];
}

- (IBAction)sosButtonTapped:(UIButton*)button
{
    if (IS_IPAD)
    {
        InfoAlertView *alertView = [[[InfoAlertView alloc] initWithTitle:NSLocalizedString(@"This device doesn't support telephone calls", @"This device doesn't support telephone calls")
                                                                 message:nil
                                                                delegate:self
                                                       cancelButtonTitle:nil
                                                       otherButtonTitles:NSLocalizedString(@"OK", @"OK"), nil] autorelease];
        
        alertView.tag = 666;
        
        [alertView show];
        
        return ;
    }
}

- (void)contactSearchBarDidCancelNotification:(NSNotification *)note
{
    isSearchActive = NO;
    [gridView reloadData];
}

- (IBAction)homeButtonTapped
{
    [self.navigationController popToRootViewControllerAnimated:YES];
}

- (IBAction)searchButtonTapped
{
    [self.searchBar becomeFirstResponder];
}

- (IBAction)addButtonTapped
{
    self.contactNewViewController = [[[GRNewContactViewController alloc] init] autorelease];
    contactNewViewController.contact = nil;
    contactNewViewController.delegate = self;
    
    contactNewViewController.allowEditing = NO;
    contactNewViewController.isSettings = NO;
    
    if (IS_IPAD)
    {
        [contactNewViewController view].frame = self.view.bounds;
        [self.view addSubview:[contactNewViewController view]];
    }
    else
    {
        [self.navigationController pushViewController:self.contactNewViewController animated:YES];
    }
}

- (NSArray*)getContactsWithText:(NSString*)text
{
    NSMutableArray *array = [NSMutableArray array];
    
    NSInteger count = [[[GRPeopleManager sharedManager] contacts] count];
    
    for (int i = 0; i < count; i++)
    {
        GRContact *record = [[[GRPeopleManager sharedManager] contacts] objectAtIndex:i];
                
        NSString *firstnameStr = @"";
        NSString *lastnameStr = @"";
        
        if (record.firstname)
        {
            firstnameStr = record.firstname;
        }
        
        if (record.lastname)
        {
            lastnameStr = record.lastname;
        }
        
        NSString *fullname = [NSString stringWithFormat:@"%@ %@", firstnameStr, lastnameStr];
        
        if ([fullname rangeOfString:text options:NSCaseInsensitiveSearch].location != NSNotFound)
        {
            [array addObject:record];
        }
    }
    
    return array;
}

- (void)showEmergencyAlert
{
    WYPopoverBackgroundView *popoverAppearance = [WYPopoverBackgroundView appearance];
    
    [popoverAppearance setOverlayColor:[UIColor colorWithWhite:0.1f alpha:0.6f]];
    [popoverAppearance setOuterCornerRadius:0];
    [popoverAppearance setOuterShadowBlurRadius:0];
    [popoverAppearance setOuterShadowColor:[UIColor clearColor]];
    [popoverAppearance setOuterShadowOffset:CGSizeZero];
    
    [popoverAppearance setGlossShadowColor:[UIColor clearColor]];
    [popoverAppearance setGlossShadowOffset:CGSizeMake(0, 0)];
    
    [popoverAppearance setBorderWidth:1];
    [popoverAppearance setArrowHeight:0];
    [popoverAppearance setArrowBase:0];
    
    [popoverAppearance setInnerCornerRadius:0];
    [popoverAppearance setInnerShadowBlurRadius:0];
    [popoverAppearance setInnerShadowColor:[UIColor clearColor]];
    [popoverAppearance setInnerShadowOffset:CGSizeMake(0, 0)];
    
    [popoverAppearance setFillTopColor:[UIColor whiteColor]];
    [popoverAppearance setFillBottomColor:[UIColor whiteColor]];
    [popoverAppearance setOuterStrokeColor:[UIColor clearColor]];
    [popoverAppearance setInnerStrokeColor:[UIColor clearColor]];
    
    
    GRAlertViewController *contentViewController = [[[GRAlertViewController alloc] init] autorelease];
    
    contentViewController.contentSizeForViewInPopover = CGSizeMake(324, 175);
    
    [contentViewController view];
    
    contentViewController.messageLabel.text = NSLocalizedString(@"Emergency numbers \nneed to be set", @"Emergency numbers \nneed to be set");
    
    contentViewController.messageLabel.font = [UIFont fontWithName:@"Helvetica" size:18];
    
    [contentViewController.okButton setBackgroundImage:nil forState:UIControlStateNormal];
    contentViewController.okButton.frame = CGRectMake(0, contentViewController.okButton.frame.origin.y + 30, 50, 28);
    contentViewController.okButton.center = CGPointMake(contentViewController.view.bounds.size.width / 2, contentViewController.okButton.center.y);
    contentViewController.okButton.layer.borderWidth = 1.f;
    contentViewController.okButton.layer.borderColor = RGBCOLOR(190, 190, 190).CGColor;
    [contentViewController.okButton setTitleColor:RGBCOLOR(96, 96, 96) forState:UIControlStateNormal];
    contentViewController.okButton.titleLabel.font = [UIFont fontWithName:@"Helvetica" size:14];
    
    [contentViewController.okButton addTarget:self action:@selector(popoverButtonTapped:) forControlEvents:UIControlEventTouchUpInside];
    
    self.alertPopoverController = [[[WYPopoverController alloc] initWithContentViewController:contentViewController] autorelease];
    self.alertPopoverController.delegate = self;
    
    [self.alertPopoverController presentPopoverAsDialogAnimated:YES];
}

- (void)popoverButtonTapped:(UIButton*)button
{
    [self.alertPopoverController dismissPopoverAnimated:YES];
    self.alertPopoverController = nil;
}

#pragma mark - UITextFieldDelegate's methods

- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField
{
    if (!isSearchActive)
    {
        [self performSelector:@selector(updateView) withObject:nil afterDelay:0.1f];
    }
    
    return YES;
}

- (void)textFieldDidBeginEditing:(UITextField *)textField
{
    isSearchActive = YES;
}

- (BOOL)textFieldShouldEndEditing:(UITextField *)textField
{
    return YES;
}

- (void)textFieldDidEndEditing:(UITextField *)textField
{
    
}

- (void)updateView
{
    [self.searchPeople removeAllObjects];
    [self.searchPeople addObjectsFromArray:[self getContactsWithText:self.searchBar.text]];
    
    [self.favoriteSearchContacts removeAllObjects];
    [self.otherSearchContacts removeAllObjects];
    
    for (GRContact *contact in searchPeople)
    {
//        if (contact.isFavorite)
//        {
//            [self.favoriteSearchContacts addObject:contact];
//        }
//        else
        {
            [self.otherSearchContacts addObject:contact];
        }
    }
    
    [gridView reloadData];
    
    [searchBar becomeFirstResponder];
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    [self performSelector:@selector(updateView) withObject:nil afterDelay:0.1f];
    
    return YES;
}

- (BOOL)textFieldShouldClear:(UITextField *)textField
{
    return YES;
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    
    return YES;
}

#pragma mark - UITableViewDataSource's methods

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 2;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    NSInteger modul = 0;
    
    if (section == 0)
    {
        if (IS_IPAD)
        {
            modul = 4;
        }
        else 
        {
            if (UIInterfaceOrientationIsLandscape([UIApplication sharedApplication].statusBarOrientation))
            {
                modul = 4;
            }
            else
            {
                modul = 4;
            }
        }
        
        if (isSearchActive)
        {
            return 0;//([self.favoriteSearchContacts count] + 1 + (modul - 1)) / modul;
        }
        else
        {
            //return ([[GRPeopleManager sharedManager].favoriteContacts count] + 1 + (modul - 1)) / modul;
            
            return 2;
        }
    }
    else
    {
        if (IS_IPAD)
        {
            modul = 2;
        }
        else
        {
            if (UIInterfaceOrientationIsLandscape([UIApplication sharedApplication].statusBarOrientation))
            {
                modul = 2;
            }
            else
            {
                modul = 1;
            }
        }
        
        
        if (isSearchActive)
        {
            return ([self.otherSearchContacts count] + (modul - 1)) / modul;
        }
        else
        {
            return ([[GRPeopleManager sharedManager].otherContacts count] + (modul - 1)) / modul;
        }
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    if (section == 0)
    {
        if (!isSearchActive)
        {
            if (IS_IPAD)
            {
                return 50;
            }
            else
            {
                return 50;
            }
        }
        else
        {
            return 0;
        }
    }
    
    if (!isSearchActive)
    {
        return 40;
    }
    else
    {
        return 0;
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
    if (section == 0)
    {
        if (!isSearchActive)
        {
            if (IS_IPAD)
            {
                return 105;
            }
            else
            {
                return 105;
            }
        }
        else
        {
            return 105;
        }
    }
    
    return 0;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
//    if (section == 0)
//    {
//        if ([[GRPeopleManager sharedManager].favoriteContacts count])
//        {
            UIView *headerView = [[[UIView alloc] init] autorelease];
            
            headerView.backgroundColor = [UIColor clearColor];
            
            return headerView;
//        }
//        else
//        {
//            return nil;
//        }
//    }
//    
//    return nil;
}

- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section
{
    if (section == 0)
    {
//        if ([[GRPeopleManager sharedManager].favoriteContacts count])
//        {
            NSInteger height = 105;
        
            if (IS_IPAD)
            {
                height = 105;
            }
            
            UIView *headerView = [[[UIView alloc] initWithFrame:CGRectMake(0, 0, tableView.bounds.size.width, height)] autorelease];
            
            headerView.backgroundColor = [UIColor clearColor];
        
            [headerView addSubview:self.searchBar];
        
            self.searchBar.center = CGPointMake((int)headerView.bounds.size.width / 2, (int)headerView.bounds.size.height / 2);
        
            return headerView;
//        }
//        else
//        {
//            return nil;
//        }
    }
    
    return nil;
}

- (UITableViewCell *)tableView:(UITableView *)tableView
         cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    GRContactTableViewCell *contactTableViewCell = nil;
    
    NSInteger number = 0;
    NSInteger modul = 0;
    
    if (indexPath.section == 0)
    {
        contactTableViewCell = [tableView dequeueReusableCellWithIdentifier:[GRFavoriteContactTableViewCell reuseIdentifier]];
        
        if (!contactTableViewCell)
        {
            contactTableViewCell = [[[NSBundle mainBundle] loadNibNamed:@"GRFavoriteContactTableViewCell" owner:nil options:nil] lastObject];
        }
                
//        if (isSearchActive)
//        {
//            number = [self.favoriteSearchContacts count];
//        }
//        else
//        {
            number = 8;//[[GRPeopleManager sharedManager].favoriteContacts count] + 1;
//        }
        
        if (IS_IPAD)
        {
            modul = 4;
        }
        else 
        {
            if (UIInterfaceOrientationIsLandscape([UIApplication sharedApplication].statusBarOrientation))
            {
                modul = 4;
            }
            else
            {
                modul = 4;
            }
        }
    }
    else
    {
        contactTableViewCell = [tableView dequeueReusableCellWithIdentifier:[GRContactTableViewCell reuseIdentifier]];
        
        if (!contactTableViewCell)
        {
            contactTableViewCell = [[[NSBundle mainBundle] loadNibNamed:@"GRContactTableViewCell" owner:nil options:nil] lastObject];
        }
        
        if (isSearchActive)
        {
            number = [self.otherSearchContacts count];
        }
        else
        {
            number = [[GRPeopleManager sharedManager].otherContacts count];
        }
        
        if (IS_IPAD)
        {
            modul = 2;
        }
        else
        {
            if (UIInterfaceOrientationIsLandscape([UIApplication sharedApplication].statusBarOrientation))
            {
                modul = 2;
            }
            else
            {
                modul = 1;
            }
        }
    }

    contactTableViewCell.backgroundColor = [UIColor clearColor];
    
    if (number - (indexPath.row + 1) * modul >= 0)
    {
        number = modul;
    }
    else
    {
        number = number % modul;
    }
    
    contactTableViewCell.indexPath = indexPath;
    
    [contactTableViewCell.contentView.subviews makeObjectsPerformSelector:@selector(removeFromSuperview)];
    
    contactTableViewCell.delegate = self;
    
    contactTableViewCell.numberOfChildren = number;
    
    [contactTableViewCell layoutContent];
    
    return contactTableViewCell;
}

#pragma mark - UITableViewDelegate's methods

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.section == 0)
    {
        return [GRFavoriteContactTableViewCell cellHeight];
    }
    
    return [GRContactTableViewCell cellHeight];
}

#pragma mark - GRContactCellDelegate's methods

- (UIView*)viewForContactAtIndexPath:(NSIndexPath*)indexPath withIndex:(NSNumber*)index
{
    NSInteger i = [index intValue];
    
    NSInteger objectIndex = 0;
    
    GRContact *contact = nil;
    
    GRContactContainerView *contactView = nil;
    
    NSInteger modul;
    
    if (indexPath.section == 0)
    {
        if (IS_IPAD)
        {
            modul = 4;
        }
        else 
        {
            if (IS_IPAD)
            {
                modul = 4;
            }
            else
            {
                if (UIInterfaceOrientationIsLandscape([UIApplication sharedApplication].statusBarOrientation))
                {
                    modul = 4;
                }
                else
                {
                    modul = 4;
                }
            }
        }
        
        objectIndex = indexPath.row * modul + i;
        
        CGSize size = CGSizeMake([GRFavoriteContactTableViewCell cellHeight], [GRFavoriteContactTableViewCell cellHeight]);
        
        contactView = [[[NSBundle mainBundle] loadNibNamed:@"GRFavoriteContactView" owner:nil options:nil] lastObject];
        
        contactView.frame = CGRectMake(20 + i * size.width + i * 20, 0, size.width, size.height);
        
//        if ([self.searchBar.text length])
//        {
//            contact = [self.favoriteSearchContacts objectAtIndex:objectIndex];
//        }
//        else
//        {
        if ([[GRPeopleManager sharedManager].favoriteContacts count] > objectIndex)
        {
            contact = [[GRPeopleManager sharedManager].favoriteContacts objectAtIndex:objectIndex];
        }
        else
        {
            contact = nil;
        }
//        }
        
        
        if (contact)
        {
            UIImage *image = [UIImage imageWithContentsOfFile:contact.photourl];
            
            contactView.coverButton.userInteractionEnabled = YES;
            
            if (!image)
            {
                contactView.titleLabel.hidden = NO;
                
                contactView.coverImageView.image = nil;
                
                [contactView.coverButton setImage:nil forState:UIControlStateNormal];
            }
            else
            {
                contactView.titleLabel.hidden = YES;
                contactView.coverImageView.image = image;
                
                [contactView.coverButton setImage:nil forState:UIControlStateNormal];
            }
        }
        else if (objectIndex == 7)
        {
            contactView.titleLabel.hidden = NO;
            
            UIImage *background = [UIImage imageNamed:@"people_sos"];
            
            contactView.coverImageView.image = background;
            contactView.coverImageView.contentMode = UIViewContentModeCenter;
            contactView.coverButton.userInteractionEnabled = YES;
            
            [contactView.coverButton setImage:nil forState:UIControlStateNormal];
        }
        else
        {
            contactView.titleLabel.hidden = NO;
            
            contactView.coverImageView.image = nil;
            contactView.coverButton.userInteractionEnabled = NO;
            
            [contactView.coverButton setImage:[UIImage imageNamed:@"people_add_name_icon"] forState:UIControlStateNormal];
        }
    }
    else
    {
        if (IS_IPAD)
        {
            modul = 2;
        }
        else
        {
            if (UIInterfaceOrientationIsLandscape([UIApplication sharedApplication].statusBarOrientation))
            {
                modul = 2;
            }
            else
            {
                modul = 1;
            }
        }
        
        objectIndex = indexPath.row * modul + i;
        
        CGSize size = CGSizeMake(self.gridView.bounds.size.width / modul, [GRContactTableViewCell cellHeight]);
        
        contactView = [[[NSBundle mainBundle] loadNibNamed:@"GRContactContainerView" owner:nil options:nil] lastObject];
        
        contactView.frame = CGRectMake(i * size.width, 0, size.width, size.height);
        
        if (indexPath.row == 0)
        {
            contactView.topSeparatorView.hidden = NO;
        }
        else
        {
            contactView.topSeparatorView.hidden = YES;
        }
        
        if (isSearchActive)
        {
            contact = [self.otherSearchContacts objectAtIndex:objectIndex];
        }
        else
        {
            contact = [[GRPeopleManager sharedManager].otherContacts objectAtIndex:objectIndex];
        }
        
        contactView.coverImageView.image = nil;
        
        contactView.titleLabel.hidden = NO;
    }
    
    NSMutableString *name = [NSMutableString string];
    
    if ([contact.firstname length])
    {
        [name appendString:contact.firstname];
    }
    
    if ([contact.lastname length])
    {
        if ([name length])
        {
            [name appendString:@" "];
        }
        
        [name appendString:contact.lastname];
    }
    
    if (indexPath.section == 0)
    {
        if (!contact && objectIndex != 7)
        {
            contactView.titleLabel.text = @"";
            contactView.titleLabel.textColor = RGBCOLOR(175, 169, 162);
        }
        else
        {
            contactView.titleLabel.text = name;
            contactView.titleLabel.textColor = RGBCOLOR(113, 112, 112);
        }
    }
    else
    {
        contactView.titleLabel.text = name;
        contactView.titleLabel.textColor = RGBCOLOR(167, 167, 167);
    }
    
    return contactView;
}

- (void)contactTappedWithIndexPath:(NSIndexPath*)indexPath andIndex:(NSNumber*)index
{    
    NSInteger i = [index intValue];
    
    NSInteger objectIndex = 0;
    
    GRContact *contact = nil;
    
    NSInteger modul;
    
    if (indexPath.section == 0)
    {
        if (IS_IPAD)
        {
            modul = 4;
        }
        else 
        {
            if (IS_IPAD)
            {
                modul = 4;
            }
            else
            {
                if (UIInterfaceOrientationIsLandscape([UIApplication sharedApplication].statusBarOrientation))
                {
                    modul = 4;
                }
                else
                {
                    modul = 4;
                }
            }
        }
        
        objectIndex = indexPath.row * modul + i;
        
//        if ([self.searchBar.text length])
//        {
//            contact = [self.favoriteSearchContacts objectAtIndex:objectIndex];
//        }
//        else
//        {
        if ([[GRPeopleManager sharedManager].favoriteContacts count] > objectIndex)
        {
            contact = [[GRPeopleManager sharedManager].favoriteContacts objectAtIndex:objectIndex];
        }
        else
        {
            contact = nil;
        }
//        }
    }
    else
    {
        if (IS_IPAD)
        {
            modul = 2;
        }
        else
        {
            if (UIInterfaceOrientationIsLandscape([UIApplication sharedApplication].statusBarOrientation))
            {
                modul = 2;
            }
            else
            {
                modul = 1;
            }
        }
        
        objectIndex = indexPath.row * modul + i;
        
        if (isSearchActive)
        {
            contact = [self.otherSearchContacts objectAtIndex:objectIndex];
        }
        else
        {
            contact = [[GRPeopleManager sharedManager].otherContacts objectAtIndex:objectIndex];
        }
    }

    if (IS_IPAD)
    {
        if (contact)
        {
            self.contactViewController = [[[GRContactViewController alloc] initWithNibName:@"GRContactViewController" bundle:nil] autorelease];
            contactViewController.contact = contact;
            contactViewController.delegate = self;
            [contactViewController view].frame = self.view.bounds;
            [self.view addSubview:[contactViewController view]];
        }
        else if (objectIndex == 7)
        {
            //self.sosView.hidden = NO;
            if (![self.settings.doctor_phone length] &&
                ![self.settings.home_phone length] &&
                ![self.settings.contact_phone length] &&
                ![self.settings.hospital_phone length])
            {
                [self showEmergencyAlert];
                
                return ;
            }
            
            
            self.sosViewController = [[[SOSViewController alloc] init] autorelease];
            
            self.sosViewController.delegate = self;
            
            [self.view addSubview:self.sosViewController.view];
            self.sosViewController.view.frame = self.view.bounds;
        }
        else
        {
            
//            GRSettingsiPadController *contr = [[[GRSettingsiPadController alloc] initWithNibName:@"GRSettingsiPadController" bundle:nil] autorelease];
//            contr.selectedIndex = 6;
//            [self.navigationController pushViewController:contr animated:YES];
//            [contr performSelector:@selector(tableView:didSelectRowAtIndexPath:) withObject:nil withObject:[NSIndexPath indexPathForRow:contr.selectedIndex inSection:0]];
            
            
            
//            self.contactNewViewController = [[[GRNewContactViewController alloc] init] autorelease];
//            contactNewViewController.contact = nil;
//            contactNewViewController.delegate = self;
//            contactNewViewController.favorite = YES;
//            
//            [contactNewViewController view].frame = self.view.bounds;
//            [self.view addSubview:[contactNewViewController view]];
        }
    }
    else
    {
        if (contact)
        {
            self.contactViewController = [[[GRContactViewController alloc] init] autorelease];
            contactViewController.contact = contact;
            contactViewController.delegate = self;
            
            [self.navigationController pushViewController:self.contactViewController animated:YES];
        }
        else if (objectIndex == 7)
        {
            
        }
        else
        {
            self.contactNewViewController = [[[GRNewContactViewController alloc] init] autorelease];
            contactNewViewController.contact = nil;
            contactNewViewController.delegate = self;
            contactNewViewController.favorite = YES;
            
            [self.navigationController pushViewController:self.contactNewViewController animated:YES];
        }
    }
    
    [self.searchBar resignFirstResponder];
}

- (void)willSelectView:(GRContactContainerView*)contactView
           atIndexPath:(NSIndexPath*)indexPath
              andIndex:(NSNumber*)index
{
    
    
    if (self.selectedContactView)
    {
        self.selectedContactView.titleLabel.textColor = RGBCOLOR(167, 167, 167);
        self.selectedContactView.coverImageView.image = nil;
        
        self.selectedContactView = nil;
    }
    
    if (indexPath.section != 0)
    {
        self.selectedContactView = contactView;
        self.selectedContactView.titleLabel.textColor = [UIColor whiteColor];
        self.selectedContactView.coverImageView.image = [[UIImage imageNamed:@"people_selected_contact_background"] resizableImageWithCapInsets:UIEdgeInsetsMake(0, 20, 0, 20)];
    }
}

- (void) hideSOSScreen
{
    [self.sosViewController.view removeFromSuperview];
    self.sosViewController = nil;
}

#pragma mark - GRContactCardDelegate's methods

- (void)popContactCard:(UIViewController*)contactCard
{
    if (IS_IPAD)
    {
        [contactCard.view removeFromSuperview];
    }
    
    if (contactCard == self.contactNewViewController)
    {
        if (!IS_IPAD)
        {
            [self.contactNewViewController.navigationController popViewControllerAnimated:YES];
        }
        
        [self.spinnerView startAnimating];
        
        [[GRPeopleManager sharedManager] updateContactsWithCompletionBlock:^{
            [self reloadTableView];
        }];
        
        self.contactNewViewController = nil;
    }
    else if (contactCard == self.contactViewController)
    {
        if (!IS_IPAD)
        {
            [self.contactViewController.navigationController popViewControllerAnimated:YES];
        }
        
        self.contactViewController = nil;
    }
}

#pragma mark - WYPopoverControllerDelegate's methods

- (BOOL)popoverControllerShouldDismissPopover:(WYPopoverController *)popoverController
{
    return YES;
}

- (void)popoverControllerDidDismissPopover:(WYPopoverController *)popoverController
{
    self.alertPopoverController = nil;
}

@end
