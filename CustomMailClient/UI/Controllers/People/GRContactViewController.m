//
//  GRContactViewController.m
//  GrannyApp
//
//  Created by Eugene Pavlyuk on 7/12/12.
//  Copyright (c) 2012 Home. All rights reserved.
//

#import "GRContactViewController.h"
#import "GRContact.h"
#import "GRNewMessageController.h"
#import "GRMailAccountManager.h"
#import "TMailAccount.h"
#import "GRMailSetupController.h"
#import "GRMailInboxListController.h"
#import "GRMessageReadController.h"
#import "GRMailDataManager.h"
#import "ContactInfoTableViewCell.h"
#import "WYPopoverController.h"
#import "EmailActionView.h"

@interface GRContactViewController () <WYPopoverControllerDelegate>
{
    GRNewMessageController  *newMessageController;
    GRMessageReadController *messageReadController;
    GRCommonAlertView *mailAlert;
}

@property (nonatomic, retain) MFMailComposeViewController *mailController;

@property (nonatomic, retain) WYPopoverController *alertPopoverController;

@property (nonatomic, copy) NSString *emailSelected;

@end

@implementation GRContactViewController

@synthesize delegate;

- (void)setupPopoverAppearance
{
    WYPopoverBackgroundView *popoverAppearance = [WYPopoverBackgroundView appearance];
    
    [popoverAppearance setOverlayColor:[UIColor colorWithWhite:0.1f alpha:0.6f]];
    [popoverAppearance setOuterCornerRadius:20];
    [popoverAppearance setOuterShadowBlurRadius:4];
    [popoverAppearance setOuterShadowColor:[UIColor colorWithWhite:0.3f alpha:0.6f]];
    [popoverAppearance setOuterShadowOffset:CGSizeMake(4, 4)];
    
    [popoverAppearance setGlossShadowColor:[UIColor clearColor]];
    [popoverAppearance setGlossShadowOffset:CGSizeMake(0, 0)];
    
    [popoverAppearance setBorderWidth:0];
    [popoverAppearance setArrowHeight:0];
    [popoverAppearance setArrowBase:0];
    
    [popoverAppearance setInnerCornerRadius:20];
    [popoverAppearance setInnerShadowBlurRadius:0];
    [popoverAppearance setInnerShadowColor:[UIColor clearColor]];
    [popoverAppearance setInnerShadowOffset:CGSizeMake(0, 0)];
    
    [popoverAppearance setFillTopColor:[UIColor whiteColor]];
    [popoverAppearance setFillBottomColor:[UIColor whiteColor]];
    [popoverAppearance setOuterStrokeColor:[UIColor clearColor]];
    [popoverAppearance setInnerStrokeColor:[UIColor clearColor]];
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    
    if (self) 
    {
        [[NSNotificationCenter defaultCenter] addObserver:self
                                                 selector:@selector(resyncingContactsNotification:)
                                                     name:kResyncingContactsNotification
                                                   object:nil];
    }
    
    return self;
}

- (void)resyncingContactsNotification:(NSNotification*)notification
{
    [delegate popContactCard:self];
}

- (BOOL)prefersStatusBarHidden
{
    return YES;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    
    self.screenName = @"Contact Screen";
    
    self.tableView.contentInset = UIEdgeInsetsMake(60, 0, 0, 0);
    
    if (self.contact.photourl)
    {
        self.imageView.image = [UIImage imageWithContentsOfFile:self.contact.photourl];
        
        self.photoContainerView.hidden = NO;
        self.photoView.hidden = NO;
        self.imageView.hidden = NO;
        
        self.tableView.frame = CGRectMake(CGRectGetMaxX(self.photoContainerView.frame) + 20, self.tableView.frame.origin.y, CGRectGetWidth(self.infoContainerView.frame) - CGRectGetMaxX(self.photoContainerView.frame) - 20, self.tableView.frame.size.height);
    }
    else
    {
        self.photoContainerView.hidden = YES;
        self.photoView.hidden = YES;
        self.imageView.hidden = YES;
        
        self.tableView.frame = CGRectMake(30, self.tableView.frame.origin.y, CGRectGetWidth(self.infoContainerView.frame) - 30, self.tableView.frame.size.height);
    }
    
    UIImage *headerBackground = nil;
    
    if (IS_IPAD)
    {
        headerBackground = [[UIImage imageNamed:@"people_header_background"] resizableImageWithCapInsets:UIEdgeInsetsMake(0, 40, 0, 40)];
    }
    else
    {
        headerBackground = [[UIImage imageNamed:@"people_header_background"] stretchableImageWithLeftCapWidth:36 topCapHeight:0];
    }
    
    self.infoImageView.image = headerBackground;
    
    self.photoContainerView.layer.cornerRadius = 25.f;
    self.imageView.layer.cornerRadius = 25.f;
    self.photoView.layer.cornerRadius = 25.f;
    self.photoContainerView.layer.borderWidth = 1.f;
    self.photoContainerView.layer.borderColor = RGBCOLOR(220, 54, 0).CGColor;
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
}


- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    
    self.infoImageView = nil;
    
    self.imageView = nil;
    self.infoContainerView = nil;
    self.photoContainerView = nil;
    self.photoView = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    if (IS_IPAD)
    {
        return UIInterfaceOrientationIsLandscape(interfaceOrientation);
    }
    
    return YES;
}

- (void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
    
    self.contact = nil;
    self.emailSelected = nil;
    
    self.infoImageView = nil;
    
    self.imageView = nil;
    self.mailController = nil;

    self.infoContainerView = nil;
    self.photoContainerView = nil;
    self.photoView = nil;
    
    [self.alertPopoverController dismissPopoverAnimated:NO];
    self.alertPopoverController = nil;
    
    [super dealloc];
}

- (IBAction)callButtonTapped
{    
    ActionAlertView *alertView = [[[ActionAlertView alloc] initWithTitle:NSLocalizedString(@"What number would you like to call?", @"What number would you like to call?")
                                                                 message:nil 
                                                                delegate:self 
                                                       cancelButtonTitle:nil 
                                                       otherButtonTitles:
                                   NSLocalizedString(@"Mobile", @"Mobile"), 
                                   NSLocalizedString(@"SKYPE", @"SKYPE"), nil] autorelease];
    alertView.tag = 1;
    [alertView show];
}

- (IBAction)phoneButtonTappedWithIndexPath:(NSIndexPath*)indexPath
{
    if (IS_IPAD)
    {
//        InfoAlertView *alertView = [[[InfoAlertView alloc] initWithTitle:NSLocalizedString(@"This device doesn't support telephone calls", @"This device doesn't support telephone calls")
//                                                                 message:nil
//                                                                delegate:self
//                                                       cancelButtonTitle:nil
//                                                       otherButtonTitles:NSLocalizedString(@"OK", @"OK"), nil] autorelease];
//        
//        alertView.tag = 666;
//        
//        [alertView show];
        
        [[[GRCommonAlertView alloc] initContactAlertWhenCallsNotAvailableWithAlert:NSLocalizedString(@"Phone calls do not work on this device right now", @"Phone calls do not work on this device right now") inView:self.view] autorelease];
    }
    else
    {
        NSString *phone = nil;
        
        if (indexPath.row == 0)
        {
            phone = self.contact.phoneMobile;
        }
        else if (indexPath.row == 1)
        {
            phone = self.contact.phoneiPhone;
        }
        else if (indexPath.row == 2)
        {
            phone = self.contact.phoneHome;
        }
        else if (indexPath.row == 3)
        {
            phone = self.contact.phoneWork;
        }
        else if (indexPath.row == 4)
        {
            phone = self.contact.phoneMain;
        }
        else if (indexPath.row == 5)
        {
            phone = self.contact.phoneHomeFax;
        }
        else if (indexPath.row == 6)
        {
            phone = self.contact.phoneWorkFax;
        }
        else if (indexPath.row == 7)
        {
            phone = self.contact.phoneOtherFax;
        }
        else if (indexPath.row == 8)
        {
            phone = self.contact.phonePager;
        }
        
        if (![phone length])
        {
            InfoAlertView *alertView = [[[InfoAlertView alloc] initWithTitle:NSLocalizedString(@"\nPhone number is absent\n", @"Phone number is absent")
                                                                     message:nil
                                                                    delegate:self
                                                           cancelButtonTitle:nil
                                                           otherButtonTitles:NSLocalizedString(@"OK", @"OK"), nil] autorelease];
            
            alertView.tag = 666;
            
            [alertView show];
        }
        else
        {
            NSString* urlString = [NSString stringWithFormat:@"tel://%@", phone];
            urlString = [urlString stringByReplacingOccurrencesOfString:@"(" withString:@""];
            urlString = [urlString stringByReplacingOccurrencesOfString:@")" withString:@""];
            urlString = [urlString stringByReplacingOccurrencesOfString:@" " withString:@""];
            [[UIApplication sharedApplication] openURL:[NSURL URLWithString:urlString]];
        }
    }
}

- (IBAction)skypeButtonTapped
{
    if (![_contact.phoneSkype length])
    {
        InfoAlertView *alertView = [[[InfoAlertView alloc] initWithTitle:NSLocalizedString(@"\nSkype ID is absent\n", @"Skype ID is absent")
                                                                 message:nil
                                                                delegate:self
                                                       cancelButtonTitle:nil
                                                       otherButtonTitles:NSLocalizedString(@"OK", @"OK"), nil] autorelease];
        
        alertView.tag = 666;
        
        [alertView show];
    }
    else
    {
        NSString* urlString = [NSString stringWithFormat:@"skype://%@?call", _contact.phoneSkype];
        
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:urlString]];
    }

}

- (void)cancelButtonTapped
{
    [self.alertPopoverController dismissPopoverAnimated:NO];
    self.alertPopoverController = nil;
}

- (void)readButtonTapped
{
    NSArray *accounts = [[GRMailAccountManager manager] getAccounts];
    if (accounts.count == 0)
    {
        GRCommonAlertView *alert = [[[GRCommonAlertView alloc] initCommonAlert:NSLocalizedString(@"Please add an \nemail account", @"Please add an \nemail account") inView:self.view] autorelease];
        alert.delegate = self;
        return;
    }

    if ([accounts count] == 1) {
            TMailAccount *account = [accounts objectAtIndex:0];
        
            GRMailInboxListController *inboxController = [[GRMailInboxListController alloc] init];
            inboxController.mailAccount = account;
        
            GRContact *c = [[_contact copy] autorelease];
            c.email = self.emailSelected;
        
            inboxController.fromContact = c;
            [delegate.navigationController setNavigationBarHidden:YES];
            [delegate.navigationController pushViewController:inboxController animated:YES];
            [inboxController release];
        } else {
            GRMailSetupController *mailSetupController = [[GRMailSetupController alloc] init];
            
            GRContact *c = [[_contact copy] autorelease];
            c.email = self.emailSelected;
            
            mailSetupController.fromContact = c;
            [delegate.navigationController setNavigationBarHidden:YES];
            [delegate.navigationController pushViewController:mailSetupController animated:YES];
            [mailSetupController release];
            
        }
    
    [self.alertPopoverController dismissPopoverAnimated:NO];
    self.alertPopoverController = nil;
}

- (void)writeButtonTapped
{
    NSArray *accounts = [[GRMailAccountManager manager] getAccounts];
    
    if (accounts.count == 0)
    {
        GRCommonAlertView *alert = [[[GRCommonAlertView alloc] initCommonAlert:NSLocalizedString(@"Please add an \nemail account", @"Please add an \nemail account") inView:self.view] autorelease];
        alert.delegate = self;
        return;
    }
    
    TMailAccount *mailAccount = [accounts objectAtIndex:0];
    
    if (newMessageController) {
        [newMessageController release];
        newMessageController = nil;
    }
    [[GRMailServiceManager shared] setupMailAccount:mailAccount];
    newMessageController = [[GRNewMessageController alloc] init];
    newMessageController.delegate = self;
    
    GRContact *c = [[_contact copy] autorelease];
    c.email = self.emailSelected;
    c.selectedEmail = nil;
    
    newMessageController.selectedToContacts = [NSMutableArray arrayWithObject:c];
    
    if (IS_IPAD)
        [self.view addSubview:newMessageController.view];
    else
        [self.delegate.navigationController pushViewController:newMessageController animated:YES];
    
    [newMessageController reloadSubviews];
    
    [self.alertPopoverController dismissPopoverAnimated:NO];
    self.alertPopoverController = nil;
}

- (IBAction)emailButtonTappedWithIndexPath:(NSIndexPath*)indexPath
{
    NSString *email = nil;
    
    if (indexPath.row == 0)
    {
        email = _contact.email;
    }
    else if (indexPath.row == 1)
    {
        email = _contact.email1;
    }
    else if (indexPath.row == 2)
    {
        email = _contact.email2;
    }
    else if (indexPath.row == 3)
    {
        email = _contact.email3;
    }
    else if (indexPath.row == 4)
    {
        email = _contact.email4;
    }
    else if (indexPath.row == 5)
    {
        email = _contact.email5;
    }
        
    if ([email length])
    {
        self.emailSelected = email;
        
        [self setupPopoverAppearance];
                
        UIViewController *contentViewController = [[[UIViewController alloc] init] autorelease];
        
        contentViewController.contentSizeForViewInPopover = CGSizeMake(510, 400);
        
        EmailActionView *emailActionView = [[[NSBundle mainBundle] loadNibNamed:@"EmailActionView" owner:nil options:nil] lastObject];
        
        [[contentViewController view] addSubview:emailActionView];
        
        [emailActionView.cancelButton addTarget:self action:@selector(cancelButtonTapped) forControlEvents:UIControlEventTouchUpInside];
        
        [emailActionView.readButton addTarget:self action:@selector(readButtonTapped) forControlEvents:UIControlEventTouchUpInside];
        
        [emailActionView.writeButton addTarget:self action:@selector(writeButtonTapped) forControlEvents:UIControlEventTouchUpInside];
        
        self.alertPopoverController = [[[WYPopoverController alloc] initWithContentViewController:contentViewController] autorelease];
        self.alertPopoverController.delegate = self;
        
        [self.alertPopoverController presentPopoverAsDialogAnimated:YES];
    }
    else
    {
        NSString *message = [NSString stringWithFormat:@"There is no email set to %@", _contact.fullContactName];
        
        [[[GRCommonAlertView alloc] initNoEmailAlert:message inView:self.view] autorelease];
    }
}

- (IBAction)backButtonTapped
{
    [delegate popContactCard:self];
}

- (IBAction)homeButtonTapped
{
    [delegate popContactCard:self];
    //[delegate.navigationController popToRootViewControllerAnimated:YES];
}

- (IBAction)mailButtonTapped
{    
    ActionAlertView *alertView = [[[ActionAlertView alloc] initWithTitle:NSLocalizedString(@"What would you\nlike to do?", @"What would you\nlike to do?")
                                                                 message:nil 
                                                                delegate:self 
                                                       cancelButtonTitle:nil 
                                                       otherButtonTitles:NSLocalizedString(@"Read email messages", @"Read email messages"), 
                                   NSLocalizedString(@"Write an email", @"Write an email"), nil] autorelease];
    [alertView show];
}

#pragma mark -
#pragma mark MFMailComposeViewControllerDelegate's methods

- (void)mailComposeController:(MFMailComposeViewController *)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError *)error
{
    [self.mailController dismissViewControllerAnimated:YES completion:^{
        
    }];
}

#pragma mark - AlertViewDelegate's methods

- (void)alertView:(AlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (alertView.tag == 1)
    {
        if (buttonIndex == 0)
        {
            if (IS_IPAD)
            {                

            }
            else
            {
                if ([_contact.phoneMobile length])
                {
                    NSString* urlString = [NSString stringWithFormat:@"tel://%@", _contact.phoneMobile];
                    
                    urlString = [urlString stringByReplacingOccurrencesOfString:@"(" withString:@""];
                    urlString = [urlString stringByReplacingOccurrencesOfString:@")" withString:@""];
                    urlString = [urlString stringByReplacingOccurrencesOfString:@" " withString:@""];
                    
                    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:urlString]];
                }
            }
        }
        else if (buttonIndex == 1)
        {
            if ([_contact.phoneSkype length])
            {
                NSString* urlString = [NSString stringWithFormat:@"skype://%@?call", _contact.phoneSkype];
                
                [[UIApplication sharedApplication] openURL:[NSURL URLWithString:urlString]];
            }
        }
    }
    else if (alertView.tag == 666)
    {
        
    }
    else
    {
        NSArray *accounts = [[GRMailAccountManager manager] getAccounts];
        if (buttonIndex == 1)
        {
            if (accounts.count > 0)
            {
                if (newMessageController) {
                    [newMessageController release];
                    newMessageController = nil;
                }
                
                newMessageController = [[GRNewMessageController alloc] init];
                newMessageController.delegate = self;
                newMessageController.selectedToContacts = [NSMutableArray arrayWithObject:_contact];
                
                if (IS_IPAD)
                    [self.view addSubview:newMessageController.view];
                else
                    [self.delegate.navigationController pushViewController:newMessageController animated:YES];
                
                [newMessageController reloadSubviews];
            }
            else
            {
                GRCommonAlertView *alert = [[[GRCommonAlertView alloc] initCommonAlert:NSLocalizedString(@"Please add an \nemail account", @"Please add an \nemail account") inView:self.view] autorelease];
                alert.delegate = self;
            }
        }
        else
        {
            [GRMailDataManager sharedManager].fromContact = YES;
            [GRMailDataManager sharedManager].contact = _contact;
            
            if (accounts.count > 0)
            {
                TMailAccount *account = [accounts objectAtIndex:0];
                
                GRMailInboxListController *inboxController = [[GRMailInboxListController alloc] init];
                inboxController.mailAccount = account;
                [delegate.navigationController setNavigationBarHidden:YES];
                [delegate.navigationController pushViewController:inboxController animated:YES];
                [inboxController release];
            }
            else
            {
                GRCommonAlertView *alert = [[[GRCommonAlertView alloc] initCommonAlert:NSLocalizedString(@"Please add an \nemail account", @"Please add an \nemail account") inView:self.view] autorelease];
                alert.delegate = self;
            }
        }
    }
}

- (void)alertViewCancel:(AlertView *)alertView
{
    
}

- (void)willPresentAlertView:(AlertView *)alertView
{
    
}

- (void)didPresentAlertView:(AlertView *)alertView
{
    
}

- (void)alertView:(AlertView *)alertView willDismissWithButtonIndex:(NSInteger)buttonIndex
{
    
}

- (void)alertView:(AlertView *)alertView didDismissWithButtonIndex:(NSInteger)buttonIndex
{
    if (alertView.tag == 1)
    {
        if (buttonIndex == 0)
        {
            if (IS_IPAD)
            {                
                InfoAlertView *alertView = [[[InfoAlertView alloc] initWithTitle:NSLocalizedString(@"This device doesn't support telephone calls", @"This device doesn't support telephone calls")
                                                                         message:nil 
                                                                        delegate:self 
                                                               cancelButtonTitle:nil 
                                                               otherButtonTitles:NSLocalizedString(@"OK", @"OK"), nil] autorelease];
                
                alertView.tag = 666;
                
                [alertView show];
            }
            else
            {
                if (![_contact.phoneMobile length])
                {
                    InfoAlertView *alertView = [[[InfoAlertView alloc] initWithTitle:NSLocalizedString(@"\nPhone number is absent\n", @"Phone number is absent")
                                                                             message:nil 
                                                                            delegate:self 
                                                                   cancelButtonTitle:nil 
                                                                   otherButtonTitles:NSLocalizedString(@"OK", @"OK"), nil] autorelease];
                    
                    alertView.tag = 666;
                    
                    [alertView show];
                }
            }
        }
        else if (buttonIndex == 1)
        {
            if (![_contact.phoneSkype length])
            {
                InfoAlertView *alertView = [[[InfoAlertView alloc] initWithTitle:NSLocalizedString(@"\nSkype ID is absent\n", @"Skype ID is absent")
                                                                         message:nil 
                                                                        delegate:self 
                                                               cancelButtonTitle:nil 
                                                               otherButtonTitles:NSLocalizedString(@"OK", @"OK"), nil] autorelease];
                
                alertView.tag = 666;
                
                [alertView show];
            }
        }
    }
    else if (alertView.tag == 666)
    {
        
    }
    else
    {

    }
}

- (void)commonAlert:(id)alertView buttonWithIndex:(NSInteger)index
{
        if (index == 666)
        {
            return ;
        }
        
        UINavigationController *navController = delegate.navigationController;
        [delegate.navigationController popToRootViewControllerAnimated:NO];
        
            GRSettingsiPadController *contr = [[[GRSettingsiPadController alloc] initWithNibName:@"GRSettingsiPadController" bundle:nil] autorelease];
            [navController pushViewController:contr animated:YES];
}

#pragma mark - UITableViewDataSource's methods

- (CGFloat)heightForFooterInSection:(NSInteger)section
{
    if (section == 1)
    {
        if (![self.contact.phoneMobile length] &&
            ![self.contact.phoneiPhone length] &&
            ![self.contact.phoneHome length] &&
            ![self.contact.phoneWork length] &&
            ![self.contact.phoneMain length] &&
            ![self.contact.phoneHomeFax length] &&
            ![self.contact.phoneWorkFax length] &&
            ![self.contact.phoneOtherFax length] &&
            ![self.contact.phonePager length])
        {
            return 0;
        }
    }
    
    if (section == 2)
    {
        if (![self.contact.email length] &&
            ![self.contact.email1 length] &&
            ![self.contact.email2 length] &&
            ![self.contact.email3 length] &&
            ![self.contact.email4 length] &&
            ![self.contact.email5 length])
        {
            return 0;
        }
    }
    
    if (section == 3)
    {
        if (![_contact.phoneSkype length])
        {
            return 0;
        }
    }
    
    if (section == 4)
    {
        if (![_contact.street length]
            && ![_contact.city length]
            && ![_contact.postCode length]
            && ![_contact.country length])
        {
            return 0;
        }
    }
    
    return 20.f;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (section == 4)
    {
        return 4;
    }
    else if (section == 1)
    {
        return 9;
    }
    else if (section == 2)
    {
        return 6;
    }
    
    return 1;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 5;
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
    NSInteger height = [self heightForFooterInSection:section];
    
    return height;
}

- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section
{
    NSInteger height = [self heightForFooterInSection:section];
    
    UIView *headerView = [[[UIView alloc] initWithFrame:CGRectMake(0, 0, tableView.bounds.size.width, height)] autorelease];
    
    headerView.backgroundColor = [UIColor clearColor];
    
    UIView *separator = [[[UIView alloc] initWithFrame:CGRectMake(0, 0, self.tableView.bounds.size.width, 1)] autorelease];
    
    separator.backgroundColor = RGBCOLOR(190, 190, 190);
    
    separator.autoresizingMask = UIViewAutoresizingFlexibleWidth;
    
    [headerView addSubview:separator];
    
    separator.center = CGPointMake(headerView.bounds.size.width / 2, 10);
    
    return headerView;
}

- (UITableViewCell *)tableView:(UITableView *)tableView
         cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    ContactInfoTableViewCell *cell = (ContactInfoTableViewCell*)[tableView dequeueReusableCellWithIdentifier:@"Cell"];
    
    if (!cell)
    {
        cell = (ContactInfoTableViewCell*)[[[NSBundle mainBundle] loadNibNamed:@"ContactInfoTableViewCell" owner:nil options:nil] lastObject];
    }
    
    cell.subTitleLabel.text = @"";
    
    cell.infoTextField.font = [UIFont fontWithName:@"Helvetica-Bold" size:38];
    cell.titleLabel.font = [UIFont fontWithName:@"Helvetica-Bold" size:38];
    cell.subTitleLabel.font = [UIFont fontWithName:@"Helvetica-Bold" size:38];
    
    cell.titleLabel.textColor = RGBCOLOR(151, 151, 151);
    cell.subTitleLabel.textColor = RGBCOLOR(151, 151, 151);
    cell.infoTextField.textColor = RGBCOLOR(66, 66, 66);
        
    cell.infoTextField.userInteractionEnabled = NO;
    
    // fix for hebrew
    cell.titleLabel.textAlignment = NSTextAlignmentLeft;
    
    if (indexPath.section == 0)
    {
        if (indexPath.row == 0)
        {
            cell.titleLabel.text = NSLocalizedString(@"Name:", @"Name:");
        }
        
        NSString *firstName = @"";
        NSString *lastName = @"";
        
        if (_contact.firstname)
        {
            firstName = _contact.firstname;
        }
        
        if (_contact.lastname)
        {
            lastName = _contact.lastname;
        }
        
        NSString *fullnameStr = [NSString stringWithFormat:@"%@ %@", firstName, lastName];
        
        cell.infoTextField.text = fullnameStr;
//        cell.infoTextField.placeholder = NSLocalizedString(@"Name", @"Name");
    }
    
    if (indexPath.section == 1)
    {
        switch (indexPath.row)
        {
            case 0:
                cell.titleLabel.text = [self.contact titleForPhoneSectionForPhone:self.contact.phoneMobile];
                cell.infoTextField.text = [self.contact.phoneMobile phoneNumberWithSpaces];
                cell.subTitleLabel.text = NSLocalizedString(@"Mobile", @"Mobile");
                break;
                
            case 1:
                cell.titleLabel.text = [self.contact titleForPhoneSectionForPhone:self.contact.phoneiPhone];
                cell.infoTextField.text = [self.contact.phoneiPhone phoneNumberWithSpaces];
                cell.subTitleLabel.text = NSLocalizedString(@"iPhone", @"iPhone");
                break;
                
            case 2:
                cell.titleLabel.text = [self.contact titleForPhoneSectionForPhone:self.contact.phoneHome];
                cell.infoTextField.text = [self.contact.phoneHome phoneNumberWithSpaces];
                cell.subTitleLabel.text = NSLocalizedString(@"Home", @"Home");
                break;
                
            case 3:
                cell.titleLabel.text = [self.contact titleForPhoneSectionForPhone:self.contact.phoneWork];
                cell.infoTextField.text = [self.contact.phoneWork phoneNumberWithSpaces];
                cell.subTitleLabel.text = NSLocalizedString(@"Work", @"Work");
                break;
                
            case 4:
                cell.titleLabel.text = [self.contact titleForPhoneSectionForPhone:self.contact.phoneMain];
                cell.infoTextField.text = [self.contact.phoneMain phoneNumberWithSpaces];
                cell.subTitleLabel.text = NSLocalizedString(@"Main", @"Main");
                break;
                
            case 5:
                cell.titleLabel.text = [self.contact titleForPhoneSectionForPhone:self.contact.phoneHomeFax];
                cell.infoTextField.text = [self.contact.phoneHomeFax phoneNumberWithSpaces];
                cell.subTitleLabel.text = NSLocalizedString(@"Fax Home", @"Fax Home");
                break;
                
            case 6:
                cell.titleLabel.text = [self.contact titleForPhoneSectionForPhone:self.contact.phoneWorkFax];
                cell.infoTextField.text = [self.contact.phoneWorkFax phoneNumberWithSpaces];
                cell.subTitleLabel.text = NSLocalizedString(@"Fax Work", @"Fax Work");
                break;
                
            case 7:
                cell.titleLabel.text = [self.contact titleForPhoneSectionForPhone:self.contact.phoneOtherFax];
                cell.infoTextField.text = [self.contact.phoneOtherFax phoneNumberWithSpaces];
                cell.subTitleLabel.text = NSLocalizedString(@"Fax Other", @"Fax Other");
                break;
                
            case 8:
                cell.titleLabel.text = [self.contact titleForPhoneSectionForPhone:self.contact.phonePager];
                cell.infoTextField.text = [self.contact.phonePager phoneNumberWithSpaces];
                cell.subTitleLabel.text = NSLocalizedString(@"Pager", @"Pager");
                break;
                
            default:
                break;
        }
    }
    
    if (indexPath.section == 2)
    {        
        switch (indexPath.row)
        {
            case 0:
                cell.titleLabel.text = [self.contact titleForEmailSectionForEmail:self.contact.email];
                cell.infoTextField.text = self.contact.email;
                break;
                
            case 1:
                cell.titleLabel.text = [self.contact titleForEmailSectionForEmail:self.contact.email1];
                cell.infoTextField.text = self.contact.email1;
                break;
                
            case 2:
                cell.titleLabel.text = [self.contact titleForEmailSectionForEmail:self.contact.email2];
                cell.infoTextField.text = self.contact.email2;
                break;
                
            case 3:
                cell.titleLabel.text = [self.contact titleForEmailSectionForEmail:self.contact.email3];
                cell.infoTextField.text = self.contact.email3;
                break;
                
            case 4:
                cell.titleLabel.text = [self.contact titleForEmailSectionForEmail:self.contact.email4];
                cell.infoTextField.text = self.contact.email4;
                break;
                
            case 5:
                cell.titleLabel.text = [self.contact titleForEmailSectionForEmail:self.contact.email5];
                cell.infoTextField.text = self.contact.email5;
                break;
                
            default:
                break;
        }
    }
    
    if (indexPath.section == 3)
    {
        if (indexPath.row == 0)
        {
            cell.titleLabel.text = NSLocalizedString(@"Skype:", @"Skype:");
        }
        
        cell.infoTextField.text = self.contact.phoneSkype;
//        cell.infoTextField.placeholder = NSLocalizedString(@"Skype", @"Skype");
    }
    
    if (indexPath.section == 4)
    {
        if (indexPath.row == 0)
        {
            cell.titleLabel.text = NSLocalizedString(@"Address:", @"Address:");
            cell.infoTextField.text = self.contact.street;
//            cell.infoTextField.placeholder = NSLocalizedString(@"Address", @"Address");
        }
        else if (indexPath.row == 1)
        {
            cell.titleLabel.text = NSLocalizedString(@"City:", @"City:");
            cell.infoTextField.text = self.contact.city;
//            cell.infoTextField.placeholder = NSLocalizedString(@"City", @"City");
        }
        else if (indexPath.row == 2)
        {
            cell.titleLabel.text = NSLocalizedString(@"Zip:", @"Zip:");
            cell.infoTextField.text = self.contact.postCode;
//            cell.infoTextField.placeholder = NSLocalizedString(@"Zip", @"Zip");
        }
        else if (indexPath.row == 3)
        {
            cell.titleLabel.text = NSLocalizedString(@"Country:", @"Country:");
            cell.infoTextField.text = self.contact.country;
//            cell.infoTextField.placeholder = NSLocalizedString(@"Country", @"Country");
        }
    }
    
    [cell setNeedsLayout];
    
    return cell;
}

#pragma mark - UITableViewDelegate's methods

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    if (indexPath.section == 1)
    {
        [self phoneButtonTappedWithIndexPath:indexPath];
    }
    else if (indexPath.section == 2)
    {
        [self emailButtonTappedWithIndexPath:indexPath];
    }
    else if (indexPath.section == 3)
    {
        [self skypeButtonTapped];
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.section == 1)
    {
        if (indexPath.row == 0)
        {
//            if (![contact.phoneMobile length] &&
//                ![contact.phoneiPhone length] &&
//                ![contact.phoneHome length] &&
//                ![contact.phoneWork length] &&
//                ![contact.phoneMain length] &&
//                ![contact.phoneHomeFax length] &&
//                ![contact.phoneWorkFax length] &&
//                ![contact.phoneOtherFax length] &&
//                ![contact.phonePager length])
//            {
//                return 40;
//            }
//            else
                if (![_contact.phoneMobile length])
            {
                return 0;
            }
        }
        else if (indexPath.row == 1)
        {
            if (![_contact.phoneiPhone length])
            {
                return 0;
            }
        }
        else if (indexPath.row == 2)
        {
            if (![_contact.phoneHome length])
            {
                return 0;
            }
        }
        else if (indexPath.row == 3)
        {
            if (![_contact.phoneWork length])
            {
                return 0;
            }
        }
        else if (indexPath.row == 4)
        {
            if (![_contact.phoneMain length])
            {
                return 0;
            }
        }
        else if (indexPath.row == 5)
        {
            if (![_contact.phoneHomeFax length])
            {
                return 0;
            }
        }
        else if (indexPath.row == 6)
        {
            if (![_contact.phoneWorkFax length])
            {
                return 0;
            }
        }
        else if (indexPath.row == 7)
        {
            if (![_contact.phoneOtherFax length])
            {
                return 0;
            }
        }
        else if (indexPath.row == 8)
        {
            if (![_contact.phonePager length])
            {
                return 0;
            }
        }
        
        //return 85;
    }
    
    if (indexPath.section == 2)
    {
        if (indexPath.row == 0)
        {
//            if (![contact.email length] &&
//                ![contact.email1 length] &&
//                ![contact.email2 length] &&
//                ![contact.email3 length] &&
//                ![contact.email4 length] &&
//                ![contact.email5 length])
//            {
//                return 40;
//            }
//            else
                if (![_contact.email length])
            {
                return 0;
            }
        }
        else if (indexPath.row == 1)
        {
            if (![_contact.email1 length])
            {
                return 0;
            }
        }
        else if (indexPath.row == 2)
        {
            if (![_contact.email2 length])
            {
                return 0;
            }
        }
        else if (indexPath.row == 3)
        {
            if (![_contact.email3 length])
            {
                return 0;
            }
        }
        else if (indexPath.row == 4)
        {
            if (![_contact.email4 length])
            {
                return 0;
            }
        }
        else if (indexPath.row == 5)
        {
            if (![_contact.email5 length])
            {
                return 0;
            }
        }
    }
    
    if (indexPath.section == 3)
    {
        if (![_contact.phoneSkype length])
        {
            return 0;
        }
    }
    
    if (indexPath.section == 4)
    {
        if (indexPath.row == 0)
        {
            if (![_contact.street length])
            {
                return 0;
            }
        }
        else if (indexPath.row == 1)
        {
            if (![_contact.city length])
            {
                return 0;
            }
        }
        else if (indexPath.row == 2)
        {
            if (![_contact.postCode length])
            {
                return 0;
            }
        }
        else if (indexPath.row == 3)
        {
            if (![_contact.country length])
            {
                return 0;
            }
        }
    }
    
    return 45.f;
}

#pragma mark - WYPopoverControllerDelegate's methods

- (BOOL)popoverControllerShouldDismissPopover:(WYPopoverController *)popoverController
{
    return YES;
}

- (void)popoverControllerDidDismissPopover:(WYPopoverController *)popoverController
{
    self.alertPopoverController = nil;
}

@end
