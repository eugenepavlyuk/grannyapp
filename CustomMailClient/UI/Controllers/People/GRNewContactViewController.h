//
//  GRNewContactViewController.h
//  GrannyApp
//
//  Created by Eugene Pavlyuk on 7/12/12.
//  Copyright (c) 2012 Home. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MessageUI/MFMailComposeViewController.h>
#import "AlertView.h"
#import "BDGridView.h"
#import "GRContactCardDelegate.h"

@class GRContact;


@interface GRNewContactViewController : GAITrackedViewController <MFMailComposeViewControllerDelegate, AlertViewDelegate, UITextFieldDelegate, BDGridViewDataSource, BDGridViewDelegate, UITableViewDataSource, UITableViewDelegate>
{
    NSString *fullname;
    NSString *name;
    NSString *lastname;
    NSString *street;
    NSString *city;
    NSString *zip;
    NSString *country;
    
    NSString *phone;
    NSString *skype;
    NSString *email;
    
    UITextField *currentTextField;
    
    NSMutableArray *assets;
    ALAssetsLibrary *library;
    
    BDGridView *photoContentView;
    
    UIImage *icon;
}

@property (nonatomic, retain) GRContact *contact;
@property (nonatomic, assign) BOOL isSettings;

@property (nonatomic, retain) IBOutlet UITableView *tableView;

@property (nonatomic, retain) IBOutlet UIImageView *infoImageView;
@property (nonatomic, retain) IBOutlet UILabel *addPhotoLabel;
@property (nonatomic, retain) IBOutlet UIImageView *imageView;
@property (nonatomic, retain) IBOutlet UILabel *headerLabel;

@property (nonatomic, retain) IBOutlet UIView *photoContainerView;
@property (nonatomic, retain) IBOutlet UIView *photoView;
@property (nonatomic, retain) IBOutlet UIButton *backButton;
@property (nonatomic, retain) IBOutlet UIButton *addPhotoButton;

@property (nonatomic, assign) UIViewController<GRContactCardDelegate> *delegate;

@property (nonatomic, retain) IBOutlet UIView *infoContainer;
@property (nonatomic, retain) IBOutlet UIView *addPhotoPopover;
@property (nonatomic, retain) IBOutlet BDGridView *photoContentView;

@property (nonatomic, assign) BOOL favorite;
@property (nonatomic, assign) BOOL allowEditing;

@property (nonatomic, retain) IBOutlet UIButton *editButton;
@property (nonatomic, retain) IBOutlet UIButton *deleteButton;


- (IBAction)backButtonTapped;
- (IBAction)okButtonTapped;
- (IBAction)deleteDraftButtonTapped;
- (IBAction)saveDraftButtonTapped;
- (IBAction)addPhotoButtonTapped;

- (IBAction)deleteButtonTapped;
- (IBAction)editButtonTapped;

- (void)startLoadingPhotoFromGallery;

@end
