//
//  EmailActionView.m
//  GrannyApp
//
//  Created by Eugene Pavluk on 1/30/14.
//
//

#import "EmailActionView.h"

@implementation EmailActionView

- (void)dealloc
{
    self.readButton = nil;
    self.writeButton = nil;
    self.cancelButton = nil;
    
    [super dealloc];
}

@end
