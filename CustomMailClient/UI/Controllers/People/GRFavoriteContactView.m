//
//  GRFavoriteContactView.m
//  GrannyApp
//
//  Created by admin on 26/11/13.
//
//

#import "GRFavoriteContactView.h"

@implementation GRFavoriteContactView

- (void)awakeFromNib
{
    [super awakeFromNib];
    
    self.container.layer.cornerRadius = 25.f;
    self.container.layer.borderColor = RGBCOLOR(220, 54, 0).CGColor;
    self.container.layer.borderWidth = 1.f;
    
    self.coverImageView.layer.cornerRadius = 25.f;
    
    //self.coverButton.imageEdgeInsets = UIEdgeInsetsMake(0, 0, 20, 0);
}

- (void)layoutSubviews
{
    [super layoutSubviews];
    
    if (!IS_IPAD)
    {
        self.titleLabel.frame = CGRectMake(0, 20, self.bounds.size.width - 10, 40);
    }
    else
    {
        self.titleLabel.frame = CGRectMake(5, 5, self.bounds.size.width - 20, self.bounds.size.height - 10);
    }
}

@end
