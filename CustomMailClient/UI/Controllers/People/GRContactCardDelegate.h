//
//  GRContactCardDelegate.h
//  GrannyApp
//
//  Created by Eugene Pavluk on 11/26/13.
//
//

#import <Foundation/Foundation.h>

@protocol GRContactCardDelegate <NSObject>

- (void)popContactCard:(UIViewController*)contactCard;

@end
