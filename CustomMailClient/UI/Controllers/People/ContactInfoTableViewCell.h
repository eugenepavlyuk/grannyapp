//
//  ContactInfoTableViewCell.h
//  GrannyApp
//
//  Created by Eugene Pavluk on 3/22/13.
//
//

#import <UIKit/UIKit.h>

@interface ContactInfoTableViewCell : UITableViewCell

@property (nonatomic, retain) IBOutlet UILabel *titleLabel;
@property (nonatomic, retain) IBOutlet UILabel *subTitleLabel;
@property (nonatomic, retain) IBOutlet UITextField *infoTextField;

@property (nonatomic, assign) NSInteger xOffsetForInfoField;

@end
