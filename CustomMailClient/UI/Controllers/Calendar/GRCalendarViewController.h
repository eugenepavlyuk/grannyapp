//
//  GRCalendarViewController.h
//  Granny
//
//  Created by Eugene Pavlyuk on 21.03.12.
//  Copyright __MyCompanyName__ 2012. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AlarmManagerDelegate.h"

@interface GRCalendarViewController : GAITrackedViewController <AlarmManagerDelegate>
{		

}

@property (nonatomic, retain) IBOutlet UIView *dateContainer;
@property (nonatomic, retain) IBOutlet UILabel *weekDayLabel;
@property (nonatomic, retain) IBOutlet UILabel *dayLabel;
@property (nonatomic, retain) IBOutlet UILabel *monthLabel;

@property (nonatomic, retain) IBOutlet UIButton *plusButton;
@property (nonatomic, retain) IBOutlet UIButton *showEventsButton;

@property (nonatomic, retain) IBOutlet UIView *eventslistPopover;
@property (nonatomic, retain) IBOutlet UIView *addEventPopover;
@property (nonatomic, retain) IBOutlet UIImageView *headerImageView;

- (IBAction)backButtonTapped;
- (IBAction)setAlarmButtonTapped;
- (IBAction)showEventListButtonTapped;
- (IBAction)showWeekButtonTapped;
- (IBAction)showMonthButtonTapped;

- (IBAction)showEventsListPopoverButtonTapped;
- (IBAction)addEventPopoverButtonTapped;
- (IBAction)cancelPopoverButtonTapped;

@end

