//
//  GRCalendarListViewController.m
//  Granny
//
//  Created by Eugene Pavlyuk on 4/2/12.
//  Copyright (c) 2012 Home. All rights reserved.
//

#import "GRCalendarListViewController.h"
#import "EventTableViewCell.h"
#import "GRNewEventViewController.h"
#import "Event.h"
#import "AlarmManager.h"
#import "NSString+Additions.h"
#import "CalendarView.h"
#import "CalendarMonth.h"
#import "CalendarLogic.h"
#import "Settings.h"
#import "DataManager.h"
#import <QuartzCore/QuartzCore.h>
#import "GRWeekdayBarView.h"
#import "GridView.h"
#import "GRHourView.h"
#import "GRHourCell.h"
#import "GRHeaderCell.h"
#import "GREventView.h"
#import "GRDayHeader.h"
#import "GRDayCell.h"

@interface GRCalendarListViewController () <GREventInputDelegate>

@property (nonatomic, retain) NSMutableArray *events;
@property (nonatomic, retain) Settings *settings;

@property (nonatomic, retain) NSDateFormatter *dateFormatter;

@property (nonatomic, retain) GRNewEventViewController *eventNewController;

@end


@implementation GRCalendarListViewController

@synthesize eventsTableView;
@synthesize weekView;
@synthesize tableContentView;
@synthesize weekContentView;
@synthesize calendarView;
@synthesize monthContentView;
@synthesize events;
@synthesize settings;

@synthesize typeOfSorting;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    
    if (self) 
    {
        self.dateFormatter = [[[NSDateFormatter alloc] init] autorelease];
        
        self.typeOfSorting = ST_List;
        
        [[NSNotificationCenter defaultCenter] addObserver:self
                                                 selector:@selector(applicationDidBecomeActive:)
                                                     name:UIApplicationDidBecomeActiveNotification
                                                   object:nil];
    }
    
    return self;
}

- (BOOL)prefersStatusBarHidden
{
    return YES;
}

- (void)applicationDidBecomeActive:(NSNotification*)note
{
    self.weekView.week = [NSDate date];
    [self.calendarView reloadData];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    
    self.screenName = @"Calendar List Screen";
    
    if (!self.settings)
    {
        self.settings = [[DataManager sharedInstance] getEntityWithName:kSettingsEntityName];
    }
    
    
    [[GRHourView appearance] setYOffset:@(0)];
    [[GRHourView appearance] setTextFont:[UIFont fontWithName:@"Helvetica-Bold" size:IS_IPAD ? 30 : 16]];
    [[GRHourCell appearance] setBorderColor:RGBCOLOR(190, 190, 190)];
    [[GRHeaderCell appearance] setTitleFont:[UIFont fontWithName:@"Helvetica-Bold" size:IS_IPAD ? 30 : 16]];    
    [[GREventView appearance] setLockImage:[UIImage imageNamed:@"calendar_lock_icon_small"]];
    [[GREventView appearance] setEventImage:[UIImage imageNamed:@"calendar_alarm_month"]];
    
    [[GRDayHeader appearance] setTitleFont:[UIFont fontWithName:@"Helvetica-Bold" size:30]];
    
    [[GRDayCell appearance] setTodayDayTitleColor:RGBCOLOR(25, 163, 163)];
    [[GRDayCell appearance] setTodayBackgroundDayColor:RGBACOLOR(239, 249, 249, 248.f/255.f)];
    [[GRDayCell appearance] setPreviousMonthDayColor:rgb(0x929292)];
    [[GRDayCell appearance] setCurrentMonthDayColor:rgb(0x424242)];
    [[GRDayCell appearance] setNextMonthDayColor:rgb(0x8cd1d1)];
    [[GRDayCell appearance] setTodayDayColor:RGBCOLOR(17, 159, 159)];
    [[GRDayCell appearance] setEventBackgroundColor:RGBACOLOR(209, 237, 237, 0.8f)];
    [[GRDayCell appearance] setEventProtectedBackgroundColor:RGBACOLOR(172, 222, 222, 1.0f)];
    [[GRDayCell appearance] setEventTitleColor:[UIColor blackColor]];
    [[GRDayCell appearance] setDayFont:[UIFont fontWithName:@"Helvetica-Bold" size:30]];
    [[GRDayCell appearance] setLockImage:[UIImage imageNamed:@"calendar_lock_icon_small"]];
    [[GRDayCell appearance] setEventImage:[UIImage imageNamed:@"calendar_alarm_month"]];
    
    self.weekView.settings = self.settings;
    self.weekView.week = [NSDate date];
    self.weekView.weekdayBarView.todayBackgroundColor = RGBACOLOR(239, 249, 249, 248.f/255.f);
    self.weekView.weekdayBarView.todayColor = RGBCOLOR(25, 163, 163);
    self.weekView.gridView.todayBackgroundColor = RGBACOLOR(239, 249, 249, 248.f/255.f);
    self.weekView.colorForEvent = RGBACOLOR(186, 227, 227, 0.7f);
    //[self.calendarView setup];
    self.calendarView.calendarViewControllerDelegate = self;
    
    [[NSNotificationCenter defaultCenter] addObserver:self 
                                             selector:@selector(updateListNotification:)
                                                 name:@"UpdateEventsListNotification" 
                                               object:nil];
    
    self.events = [NSMutableArray array];
    
    [self updateListNotification:nil];
    
    UIImage *headerBackground = nil;
    
    if (IS_IPAD)
    {
        headerBackground = [[UIImage imageNamed:@"calendar_header"] resizableImageWithCapInsets:UIEdgeInsetsMake(0, 10, 0, 10)];
    }
    else
    {
        headerBackground = [[UIImage imageNamed:@"calendar_header"] stretchableImageWithLeftCapWidth:80 topCapHeight:0];
    }
    
    self.headerImageView.image = headerBackground;
    
    ((UIButton *)[self.view viewWithTag:1000]).selected = YES;
    
    self.todayButton.hidden = YES;
}

- (void)updateListNotification:(NSNotification*)notification
{
    [self.events removeAllObjects];
    
    NSMutableArray *allEvents = [NSMutableArray arrayWithArray:[[DataManager sharedInstance] allRowsForEntity:kEventEntityName]];
    
    NSSortDescriptor *descriptor = [[[NSSortDescriptor alloc] initWithKey:@"date" ascending:YES] autorelease];
    
    [allEvents sortUsingDescriptors:@[descriptor]];
    
    [self.events addObjectsFromArray:allEvents];
    
    [self.eventsTableView  reloadData];
    [self.weekView reloadData];
    [self.calendarView reloadData];
}

- (void)releaseViews
{
    self.eventsTableView = nil;
    self.calendarView = nil;
    self.addButton = nil;
    self.monthContentView = nil;
    self.tableContentView = nil;
    self.weekContentView = nil;
    self.headerImageView = nil;
    self.weekView = nil;
    self.weekButton = nil;
    self.monthButton = nil;
    self.listButton = nil;
    self.titleLabel = nil;
    self.leftButton = nil;
    self.rightButton = nil;
    self.todayButton = nil;
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    
    [self releaseViews];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self 
                                                 name:@"UpdateEventsListNotification" 
                                               object:nil];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    [self setTypeOfSorting:typeOfSorting];
    [self.calendarView performSelector:@selector(changeMonthToCurrent) withObject:nil afterDelay:0.1f];
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];

//    [self.calendarView changeMonthToCurrent];
}

- (void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
    [self releaseViews];
    self.events = nil;
    self.settings = nil;
    self.eventNewController = nil;
    [super dealloc];
}

- (void)setTypeOfSorting:(sortingType)nTypeOfSorting
{
    typeOfSorting = nTypeOfSorting;
    
    [self.dateFormatter setDateFormat:@"MMMM"];
    
    if (typeOfSorting == ST_List)
    {
        self.tableContentView.hidden = NO;
        self.weekContentView.hidden = YES;
        self.monthContentView.hidden = YES;
        
        self.titleLabel.text = NSLocalizedString(@"Calendar : Event List", @"Calendar : Event List");
        
        self.listButton.selected = YES;
        self.weekButton.selected = NO;
        self.monthButton.selected = NO;
        
        self.leftButton.hidden = YES;
        self.rightButton.hidden = YES;
        
        self.todayButton.hidden = YES;
    }
    else if (typeOfSorting == ST_WEEKLY)
    {
        self.tableContentView.hidden = YES;
        self.weekContentView.hidden = NO;
        [weekView scrollTo7Hours];
        self.monthContentView.hidden = YES;
        
        self.titleLabel.text = [self.dateFormatter stringFromDate:self.weekView.week];
        
        self.weekButton.selected = YES;
        self.monthButton.selected = NO;
        self.listButton.selected = NO;
        
        self.leftButton.hidden = NO;
        self.rightButton.hidden = NO;
        
        self.todayButton.hidden = NO;
    }
    else
    {
        [self.calendarView setup];
        
        self.monthContentView.hidden = NO;
        self.weekContentView.hidden = YES;
        self.tableContentView.hidden = YES;
        
        self.titleLabel.text = [self.dateFormatter stringFromDate:self.calendarView.calendarLogic.referenceDate];
        self.monthButton.selected = YES;
        self.weekButton.selected = NO;
        self.listButton.selected = NO;
        
        self.leftButton.hidden = NO;
        self.rightButton.hidden = NO;
        
        self.todayButton.hidden = NO;
    }
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return UIInterfaceOrientationIsLandscape(interfaceOrientation);
}

- (IBAction)addNewEventButtonTapped:(id)sender
{
    self.eventNewController = [[[GRNewEventViewController alloc] init] autorelease];
    self.eventNewController.inputDelegate = self;
    self.eventNewController.allowEdit = YES;
    [self.eventNewController view].frame = self.view.bounds;
    [self.view addSubview:[self.eventNewController view]];
}

- (IBAction)backButtonTapped
{
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)homeButtonTapped
{
    [self.navigationController popToRootViewControllerAnimated:YES];
}

- (IBAction)sortButtonTapped:(id)sender
{
    UIButton *button = (UIButton*)sender;
    for (int i = 1000; i < 1003; i++)
    {
        if (button.tag == i)
            button.selected = YES;
        else
            ((UIButton *)[self.view viewWithTag:i]).selected = NO;
    }
    
    self.typeOfSorting = button.tag-1000;

    [self updateListNotification:nil];
}

- (IBAction)leftScrollButtonTapped
{
    if (typeOfSorting == ST_List)
    {

    }
    else if (typeOfSorting == ST_WEEKLY)
    {
        [self.weekView changeWeekLeft];
    }
    else
    {
        [self.calendarView.calendarLogic selectPreviousMonth];
    }
}

- (IBAction)rightScrollButtonTapped
{
    if (typeOfSorting == ST_List)
    {
        
    }
    else if (typeOfSorting == ST_WEEKLY)
    {
        [self.weekView changeWeekRight];
    }
    else
    {
        [self.calendarView.calendarLogic selectNextMonth];
    }
}

- (IBAction)todayButtonTapped:(id)sender
{
    if (typeOfSorting == ST_List)
    {
        
    }
    else if (typeOfSorting == ST_WEEKLY)
    {
        [self.weekView changeWeekToday];
    }
    else
    {
        [self.calendarView changeMonthToCurrent];
    }
}

#pragma mark - UITableViewDelegate's methods

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (IS_IPAD)
    {
        return 77.f;
    }
    
    return 44.f;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    self.eventNewController = [[[GRNewEventViewController alloc] init] autorelease];
    self.eventNewController.inputDelegate = self;
    
    Event *event = [self.events objectAtIndex:indexPath.section];
    
    self.eventNewController.currentEvent = event;
    self.eventNewController.allowEdit = ![event.persistent boolValue];
    
    [self.eventNewController view].frame = self.view.bounds;
    [self.view addSubview:[self.eventNewController view]];
}

#pragma mark - UITableViewDataSource methods

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return [self.events count];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 1;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    EventTableViewCell *cell = (EventTableViewCell*)[tableView dequeueReusableCellWithIdentifier:@"EventTableViewCellIdentifier"];
    
    if (!cell)
    {
        if (IS_IPAD)
        {
            cell = [[[NSBundle mainBundle] loadNibNamed:@"EventTableViewCell" owner:nil options:nil] lastObject];
        }
        else
        {
            cell = [[[NSBundle mainBundle] loadNibNamed:@"EventTableViewCell_iphone" owner:nil options:nil] lastObject];
        }
    }
    
    Event *event = [self.events objectAtIndex:indexPath.section];
    
    if ([event.persistent boolValue])
    {
        cell.protectedIcon.hidden = NO;
        cell.alarmIcon.hidden = YES;
//        cell.titleLabel.textColor = RGBCOLOR(220, 54, 0);
//        cell.timeLabel.textColor = RGBCOLOR(220, 54, 0);
//        cell.repeatLabel.textColor = RGBCOLOR(220, 54, 0);
    }
    else
    {
        cell.protectedIcon.hidden = YES;
        cell.alarmIcon.hidden = NO;
//        cell.titleLabel.textColor = RGBCOLOR(96, 96, 96);
//        cell.timeLabel.textColor = RGBCOLOR(96, 96, 96);
//        cell.repeatLabel.textColor = RGBCOLOR(96, 96, 96);
    }
    
    cell.titleLabel.textColor = RGBCOLOR(96, 96, 96);
    cell.timeLabel.textColor = RGBCOLOR(96, 96, 96);
    cell.repeatLabel.textColor = RGBCOLOR(96, 96, 96);
    
    NSString *dateTime = @"";
    
    NSString *time = @"";
    
    if ([settings.timeFormat intValue])
    {
        [self.dateFormatter setDateFormat:@"HH:mm"];
    }
    else
    {
        [self.dateFormatter setDateFormat:@"hh:mm a"];
    }
    
    time = [self.dateFormatter stringFromDate:event.date];
    
    [self.dateFormatter setDateFormat:@"MMM. dd"];
    
    dateTime = [self.dateFormatter stringFromDate:event.date];
    dateTime = [NSString stringWithFormat:@"%@ • %@", dateTime, time];
//    switch ([event.type intValue])
//    {
//        case 0:
//        {
//            dateTime = [NSString stringWithFormat:@"Daily at %@", time];
//        }; break;
//            
//        case 1:
//        {
//            NSDateFormatter *df = [[[NSDateFormatter alloc] init] autorelease];
//            
//            [df setDateFormat:@"EEEE"];
//            
//            NSString *day = [df stringFromDate:event.date];
//            
//            dateTime = [NSString stringWithFormat:@"every %@ at %@", day, time];
//            
//        };    break;
//            
//        case 2:
//        {
//            NSDateFormatter *df = [[[NSDateFormatter alloc] init] autorelease];
//            
//            [df setDateFormat:@"d"];
//            
//            NSInteger dayInt = [[df stringFromDate:event.date] intValue];
//            
//            NSString *day = [NSString ordinalNumberFormat:[NSNumber numberWithInt:dayInt]];
//            
//            dateTime = [NSString stringWithFormat:@"on the %@ of every month, at %@", day, time];
//            
//        }; break;
//            
//        case 4:
//        {
//            NSDateFormatter *df = [[[NSDateFormatter alloc] init] autorelease];
//            
//            [df setDateFormat:@"dd, MMM yyyy, "];
//            
//            dateTime = [df stringFromDate:event.date];
//            
//            dateTime = [NSString stringWithFormat:@"On %@%@", dateTime, time];
//        }; break;
//            
//        case 3:
//        {
//            NSDateFormatter *df = [[[NSDateFormatter alloc] init] autorelease];
//            
//            [df setDateFormat:@"dd, MMMM, yyyy"];
//            
//            NSString *date = [df stringFromDate:event.date];
//            
//            dateTime = [NSString stringWithFormat:@"on %@ at %@", date, time];
//        };    break;
//    }
    
    cell.titleLabel.text = [NSString stringWithFormat:@"%@", event.name];
    cell.timeLabel.text = [NSString stringWithFormat:@"%@", dateTime];
    cell.timeLabel.hidden = NO;
    cell.repeatLabel.hidden = NO;
    
    if (event.type)
    {
        switch ([event.type intValue])
        {
            case ET_Once:
                cell.repeatLabel.text = NSLocalizedString(@"Once", @"Once");
                break;
                
            case ET_Daily:
                cell.repeatLabel.text = NSLocalizedString(@"Daily", @"Daily");
                break;
                
            case ET_Weekly:
                cell.repeatLabel.text = NSLocalizedString(@"Weekly", @"Weekly");
                break;
                
            case ET_Monthly:
                cell.repeatLabel.text = NSLocalizedString(@"Monthly", @"Monthly");
                break;
                
            case ET_Annually:
                cell.repeatLabel.text = NSLocalizedString(@"Annually", @"Annually");
                break;
        }
    }
    
    return cell;
}

#pragma mark - MAWeekViewDelegate's methods

- (void)weekView:(GRWeekView *)weekView eventTapped:(EventBase *)event
{
	self.eventNewController = [[[GRNewEventViewController alloc] init] autorelease];
    self.eventNewController.inputDelegate = self;
    
    self.eventNewController.currentEvent = event;
    self.eventNewController.allowEdit = ![event.persistent boolValue];
    
    [self.eventNewController view].frame = self.view.bounds;
    [self.view addSubview:[self.eventNewController view]];
}

#pragma mark - MAWeekViewDataSource's methods

- (NSArray *)weekView:(GRWeekView *)weekView eventsForDate:(NSDate *)startDate
{
    if (typeOfSorting == ST_WEEKLY)
    {
        [self.dateFormatter setDateFormat:@"MMMM"];
        
        self.titleLabel.text = [self.dateFormatter stringFromDate:startDate];
    }
    
    NSCalendar *calendar = [AppDelegate getInstance].globalCalendar;
    
    NSDateComponents *components = [calendar components:DATE_COMPONENTS fromDate:startDate];
    
    NSInteger year = [components year];
    NSInteger month = [components month];
    NSInteger day = [components day];
    NSInteger weekDay = components.weekday;
    
    NSMutableArray *eventsArray = [NSMutableArray array];
    
    for (Event *event in self.events)
    {
        NSDateComponents *eventComponents = [calendar components:DATE_COMPONENTS fromDate:event.date];
        
        NSInteger yearEvent = [eventComponents year];
        NSInteger monthEvent = [eventComponents month];
        NSInteger dayEvent = [eventComponents day];
        NSInteger weekDayEvent = eventComponents.weekday;
        
        if ([event.type integerValue] == ET_Once)
        {
            if ((yearEvent == year) &&
                (month == monthEvent) &&
                (day == dayEvent))
            {
                [eventsArray addObject:event];
            }
        }
        else if ([event.type integerValue] == ET_Daily)
        {
            [eventsArray addObject:event];
        }
        else if ([event.type integerValue] == ET_Weekly)
        {
            if (weekDay == weekDayEvent)
            {
                [eventsArray addObject:event];
            }
        }
        else if ([event.type integerValue] == ET_Monthly)
        {
            if (day == dayEvent)
            {
                [eventsArray addObject:event];
            }
        }
        else if ([event.type integerValue] == ET_Annually)
        {
            if ((month == monthEvent) &&
                (day == dayEvent))
            {
                [eventsArray addObject:event];
            }
        }
    }
    
    return eventsArray;
}

#pragma mark - CalendarViewControllerDelegate's methods

- (void)calendarViewController:(CalendarViewController *)aCalendarViewController dateDidChange:(NSDate *)aDate
{
    if (typeOfSorting == ST_MONTLY)
    {        
        [self.dateFormatter setDateFormat:@"MMMM"];
        
        self.titleLabel.text = [self.dateFormatter stringFromDate:self.calendarView.calendarLogic.referenceDate];
    }
}

- (void)calendarViewController:(CalendarViewController *)aCalendarViewController didSelectEvent:(Event*)event
{
    self.eventNewController = [[[GRNewEventViewController alloc] init] autorelease];
    self.eventNewController.inputDelegate = self;
    
    self.eventNewController.currentEvent = event;
    self.eventNewController.allowEdit = ![event.persistent boolValue];
    
    [self.eventNewController view].frame = self.view.bounds;
    [self.view addSubview:[self.eventNewController view]];
}

- (NSArray*)eventsForDate:(NSDate*)date
{
    NSCalendar *calendar = [AppDelegate getInstance].globalCalendar;
    
    NSDateComponents *components = [calendar components:DATE_COMPONENTS fromDate:date];
    
    NSInteger year = [components year];
    NSInteger month = [components month];
    NSInteger day = [components day];
    NSInteger weekDay = components.weekday;
    
    NSMutableArray *eventsArray = [NSMutableArray array];
    
    for (Event *event in self.events)
    {
        NSDateComponents *eventComponents = [calendar components:DATE_COMPONENTS fromDate:event.date];
        
        NSInteger yearEvent = [eventComponents year];
        NSInteger monthEvent = [eventComponents month];
        NSInteger dayEvent = [eventComponents day];
        NSInteger weekDayEvent = eventComponents.weekday;
        
        if ([event.type integerValue] == ET_Once)
        {
            if ((yearEvent == year) &&
                (month == monthEvent) &&
                (day == dayEvent))
            {
                [eventsArray addObject:event];
            }
        }
        else if ([event.type integerValue] == ET_Daily)
        {
            [eventsArray addObject:event];
        }
        else if ([event.type integerValue] == ET_Weekly)
        {
            if (weekDay == weekDayEvent)
            {
                [eventsArray addObject:event];
            }
        }
        else if ([event.type integerValue] == ET_Monthly)
        {
            if (day == dayEvent)
            {
                [eventsArray addObject:event];
            }
        }
        else if ([event.type integerValue] == ET_Annually)
        {
            if ((month == monthEvent) &&
                (day == dayEvent))
            {
                [eventsArray addObject:event];
            }
        }
    }
    
    return eventsArray;
}

#pragma mark - GREventInputDelegate's methods

- (void)commonEventInputViewControllerDidFinish:(GRCommonEventInputViewController*)commonEventInputViewController
{
    [commonEventInputViewController.view removeFromSuperview];
    
    self.eventNewController = nil;
}

@end
