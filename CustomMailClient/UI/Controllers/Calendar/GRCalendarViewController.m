//
//  GRCalendarViewController.m
//  Granny
//
//  Created by Eugene Pavlyuk on 21.03.12.
//  Copyright __MyCompanyName__ 2012. All rights reserved.
//


#import "GRCalendarViewController.h"
#import "GRCalendarListViewController.h"
#import "GRSettingsViewController.h"
#import "AlarmManager.h"
#import "GRNewEventViewController.h"

@interface GRCalendarViewController() <GREventInputDelegate>

@property (nonatomic, retain) GRNewEventViewController *eventNewController;

@end


@implementation GRCalendarViewController

@synthesize dateContainer;
@synthesize weekDayLabel;
@synthesize monthLabel;
@synthesize dayLabel;
@synthesize headerImageView;

- (BOOL)prefersStatusBarHidden
{
    return YES;
}

- (IBAction)setAlarmButtonTapped
{
    self.plusButton.selected = NO;
    
    [UIView animateWithDuration:0.3f animations:^{
        self.addEventPopover.alpha = 0;
    }];
    
    self.eventNewController = [[[GRNewEventViewController alloc] init] autorelease];
    self.eventNewController.inputDelegate = self;
    
    if (IS_IPAD)
    {
        [self.eventNewController view].frame = self.view.bounds;
        [self.view addSubview:[self.eventNewController view]];
    }
    else
    {
        [self.navigationController pushViewController:self.eventNewController animated:YES];
    }
}

- (IBAction)showEventListButtonTapped
{
    self.showEventsButton.selected = NO;
    
    [UIView animateWithDuration:0.3f animations:^{
        self.eventslistPopover.alpha = 0;
    }];
    
    GRCalendarListViewController *calendarListViewController = [[[GRCalendarListViewController alloc] init] autorelease];
    
    [self.navigationController pushViewController:calendarListViewController animated:YES];
}

- (IBAction)showWeekButtonTapped
{
    self.showEventsButton.selected = NO;
    
    [UIView animateWithDuration:0.3f animations:^{
        self.eventslistPopover.alpha = 0;
    }];
    
    GRCalendarListViewController *calendarListViewController = [[[GRCalendarListViewController alloc] init] autorelease];
    
    [self.navigationController pushViewController:calendarListViewController animated:YES];
    
    [calendarListViewController setTypeOfSorting:ST_WEEKLY];
}

- (IBAction)showMonthButtonTapped
{
    self.showEventsButton.selected = NO;
    
    [UIView animateWithDuration:0.3f animations:^{
        self.eventslistPopover.alpha = 0;
    }];
    
    GRCalendarListViewController *calendarListViewController = [[[GRCalendarListViewController alloc] init] autorelease];
    
    [self.navigationController pushViewController:calendarListViewController animated:YES];
    
    [calendarListViewController setTypeOfSorting:ST_MONTLY];
}

- (IBAction)showEventsListPopoverButtonTapped
{
    [UIView animateWithDuration:0.3f animations:^{
        self.eventslistPopover.alpha = 1;
    }];
    
    self.showEventsButton.selected = YES;
}

- (IBAction)addEventPopoverButtonTapped
{
    [UIView animateWithDuration:0.3f animations:^{
        self.addEventPopover.alpha = 1;
    }];
    
    self.plusButton.selected = YES;
}

- (IBAction)cancelPopoverButtonTapped
{
    self.plusButton.selected = NO;
    self.showEventsButton.selected = NO;
    
    [UIView animateWithDuration:0.3f animations:^{
        self.eventslistPopover.alpha = 0.f;
        self.addEventPopover.alpha = 0.f;
    }];
}

- (void)timeChangedInAlarmManager:(AlarmManager*)alarmManager
{
    NSDateFormatter *dTimeFormat = [[[NSDateFormatter alloc] init] autorelease];
	[dTimeFormat setDateFormat:@"dd"];
    
    NSDateFormatter *monthTimeFormat = [[[NSDateFormatter alloc] init] autorelease];
	[monthTimeFormat setDateFormat:@"MMMM"];
    NSDateFormatter *wTimeFormat = [[[NSDateFormatter alloc] init] autorelease];
	[wTimeFormat setDateFormat:@"EEEE"];
    
    NSDate *now = [NSDate date];
   	
    NSString *dTime = [dTimeFormat stringFromDate:now];
    NSString *monthTime = [monthTimeFormat stringFromDate:now];
    NSString *weekDayTime = [wTimeFormat stringFromDate:now];
    
//    NSDateFormatter *sTimeFormat = [[[NSDateFormatter alloc] init] autorelease];
//	[sTimeFormat setDateFormat:@"ss"];
//    NSString *sTime = [sTimeFormat stringFromDate:now];
    
    self.weekDayLabel.text = [weekDayTime capitalizedString];
    
    NSDictionary *attributes = @{NSForegroundColorAttributeName : RGBCOLOR(204, 204, 204),
                                 NSStrokeWidthAttributeName : @(-0.5f),
                                 NSStrokeColorAttributeName : RGBCOLOR(0, 153, 153),
                                 NSFontAttributeName : self.dayLabel.font,
                                 NSKernAttributeName : @(-20)};
    
    NSMutableAttributedString *attributedString = [[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@"%i", [[dTime uppercaseString] intValue]] attributes:attributes];
    
    self.dayLabel.attributedText = attributedString;
    self.monthLabel.text = [monthTime capitalizedString];
}

#pragma mark - life cycle

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    [self adjustToInterfaceOrientation:[[UIApplication sharedApplication] statusBarOrientation]];
    
    AlarmManager *alarmManager = [AlarmManager sharedInstance];
    
    [alarmManager addWatcher:self];
        
    [self timeChangedInAlarmManager:alarmManager];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    
    AlarmManager *alarmManager = [AlarmManager sharedInstance];
    
    [alarmManager removeWatcher:self];
}

// Implement viewDidLoad to do additional setup after loading the view, typically from a nib.
- (void)viewDidLoad 
{
	[super viewDidLoad];
    
    self.screenName = @"Calendar Screen";
    
    UIImage *headerBackground = nil;
    
    if (IS_IPAD)
    {
        headerBackground = [[UIImage imageNamed:@"calendar_header"] resizableImageWithCapInsets:UIEdgeInsetsMake(0, 10, 0, 10)];
    }
    else
    {
        headerBackground = [[UIImage imageNamed:@"calendar_header"] stretchableImageWithLeftCapWidth:80 topCapHeight:0];
    }
    
    self.headerImageView.image = headerBackground;
    
    self.dateContainer.frame = CGRectMake((int)((self.view.frame.size.width - self.dateContainer.frame.size.width)/2), self.headerImageView.frame.size.height + self.headerImageView.frame.origin.y + (int)((self.view.frame.size.height - self.dateContainer.frame.size.height - self.headerImageView.frame.size.height - self.headerImageView.frame.origin.y)/2), self.dateContainer.frame.size.width, self.dateContainer.frame.size.height);
}

// Override to allow orientations other than the default portrait orientation.
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation 
{
    if (!IS_IPAD)
    {
        return YES;
    }
    
    return UIInterfaceOrientationIsLandscape(interfaceOrientation);
}

- (void)releaseViews
{
    self.dateContainer = nil;
    self.weekDayLabel = nil;
    self.dayLabel = nil;
    self.monthLabel = nil;
    
    self.eventslistPopover = nil;
    self.addEventPopover = nil;
    self.headerImageView = nil;
}

- (void)viewDidUnload 
{
    [super viewDidUnload];
    	
	[self releaseViews];
}

- (IBAction)backButtonTapped
{
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)dealloc 
{
    [self releaseViews];
    self.eventNewController = nil;
    [super dealloc];
}

- (void)willRotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)duration
{
    [self adjustToInterfaceOrientation:toInterfaceOrientation];
}

- (void)didRotateFromInterfaceOrientation:(UIInterfaceOrientation)fromInterfaceOrientation
{
    //[self adjustToInterfaceOrientation:[[UIApplication sharedApplication] statusBarOrientation]];
}

- (void)adjustToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation 
{
    if (IS_IPAD) 
    {

    }
    else 
    {
    }
}

#pragma mark - GREventInputDelegate's methods

- (void)commonEventInputViewControllerDidFinish:(GRCommonEventInputViewController*)commonEventInputViewController
{
    if (IS_IPAD)
    {
        [commonEventInputViewController.view removeFromSuperview];
    }
    else
    {
        [commonEventInputViewController.navigationController popViewControllerAnimated:YES];
    }
    
    self.eventNewController = nil;
}

@end
