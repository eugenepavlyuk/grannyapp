//
//  Introduction.h
//  GrannyApp
//
//  Created by Eugene Pavluk on 3/29/14.
//
//

#import <UIKit/UIKit.h>

@interface IntroductionViewController : GAITrackedViewController
{
    IBOutlet UICollectionView *collectionView;
}

@end
