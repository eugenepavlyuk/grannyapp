//
//  IntroductionCollectionViewCell.m
//  GrannyApp
//
//  Created by Eugene Pavluk on 3/29/14.
//
//

#import "IntroductionCollectionViewCell.h"

@implementation IntroductionCollectionViewCell

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)dealloc
{
    self.introductionImageView = nil;
    [super dealloc];
}

@end
