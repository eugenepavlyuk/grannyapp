//
//  Introduction.m
//  GrannyApp
//
//  Created by Eugene Pavluk on 3/29/14.
//
//

#import "IntroductionViewController.h"
#import "IntroductionCollectionViewCell.h"

@interface IntroductionViewController ()

@end

@implementation IntroductionViewController

- (NSUInteger)supportedInterfaceOrientations
{
    return UIInterfaceOrientationMaskLandscape;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation
{
    return UIInterfaceOrientationIsLandscape(toInterfaceOrientation);
}

- (BOOL)prefersStatusBarHidden
{
    return YES;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    [collectionView registerNib:[UINib nibWithNibName:@"IntroductionCollectionViewCell" bundle:[NSBundle mainBundle]]
     forCellWithReuseIdentifier:@"IntroductionCollectionViewCellIdentifier"];
}

- (void)dealloc
{
    [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"userSawIntroduction"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    
    [super dealloc];
}

- (void)emailButtonTapped
{
    if ([[UIApplication sharedApplication] canOpenURL:[NSURL URLWithString:@"mailto:clients@fruitmenapps.com"]])
    {
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"mailto:clients@fruitmenapps.com"]];
    }
}

#pragma mark - UIScrollViewDelegate's delegates

- (void)scrollViewDidScroll: (UIScrollView*)scrollView
{
    float scrollViewWidth = scrollView.frame.size.width;
    float scrollContentSizeWidth = scrollView.contentSize.width;
    float scrollOffset = scrollView.contentOffset.x;
    
    if (scrollOffset == 0)
    {
        // then we are at the top
    }
    else if (scrollOffset + scrollViewWidth > scrollContentSizeWidth)
    {
        [self dismissViewControllerAnimated:YES completion:NULL];
    }
}

#pragma mark - UICollectionViewDataSource's methods

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return 12;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)aCollectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    IntroductionCollectionViewCell *introductionCollectionViewCell = [aCollectionView dequeueReusableCellWithReuseIdentifier:@"IntroductionCollectionViewCellIdentifier" forIndexPath:indexPath];
    
    introductionCollectionViewCell.introductionImageView.image = [UIImage imageNamed:[NSString stringWithFormat:@"introduction_%li", (long)indexPath.item + 1]];
        
    return introductionCollectionViewCell;
}

@end
