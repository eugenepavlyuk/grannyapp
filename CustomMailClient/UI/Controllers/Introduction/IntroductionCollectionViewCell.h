//
//  IntroductionCollectionViewCell.h
//  GrannyApp
//
//  Created by Eugene Pavluk on 3/29/14.
//
//

#import <UIKit/UIKit.h>

@interface IntroductionCollectionViewCell : UICollectionViewCell

@property (nonatomic, retain) IBOutlet UIImageView *introductionImageView;
@property (nonatomic, retain) IBOutlet UIButton *emailButton;

@end
