//
//  GRMailSetupController.h
//  GrannyApp
//
//  Created by Ievgen Pavliuk on 4/12/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "GRCommonAlertView.h"
#import "GRContact.h"

@interface GRMailSetupController : GAITrackedViewController <UITableViewDelegate, UITableViewDataSource, GRCommonAlertDelegate>  {
    
    UITableView      *contentTable;
    NSMutableArray   *tableData;
    GRContact        *fromContact;
}
@property (nonatomic, retain) IBOutlet UITableView      *contentTable;
@property (nonatomic, retain) GRContact        *fromContact;

- (void)updateData;
- (IBAction)backBtnTapped:(id)sender;
@end
