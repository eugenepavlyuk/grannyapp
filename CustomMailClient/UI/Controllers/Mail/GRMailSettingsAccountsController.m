//
//  GRMailSettingsAccountsController.m
//  GrannyApp
//
//  Created by Ievgen Pavliuk on 7/30/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "GRMailSettingsAccountsController.h"
#import "TMailAccount.h"
#import "GRMailAccountManager.h"
#import "GRMailSettingsAccountTypeController.h"
#import "GRSettingsiPadController.h"
#import "GRMailSettingsNewAccountController.h"
#import "WYPopoverController.h"

@interface GRMailSettingsAccountsController () <WYPopoverControllerDelegate>

@property (nonatomic, retain) WYPopoverController *alertPopoverController;

@end

@implementation GRMailSettingsAccountsController

@synthesize contentTable;
@synthesize parentController;
@synthesize addButton;
@synthesize editButton;
@synthesize okButton;

#pragma mark - update Data

- (void)updateData
{
    if (tableData)
    {
        [tableData retain];
        tableData = nil;
    }
    NSArray *accounts = [[GRMailAccountManager manager] getAccounts];
    tableData = [[NSMutableArray alloc] initWithArray:accounts];
    [self.contentTable reloadData];
    
    if (!IS_IPAD)
    {
        if (accounts.count > 0)
            editButton.hidden = NO;
        else
        {
            editButton.hidden = YES;
            okButton.hidden = YES;
        }
        
        addButton.hidden = NO;
    }
    else
    {
        if (accounts.count == 0)
            [self onAccountTypesController];
    }
}

- (void)dealloc
{
    self.addButton = nil;
    self.editButton = nil;
    self.okButton = nil;
    [contentTable release];
    [tableData release];
    [newAccountController release];
    [newOtherAccountController release];
    parentController = nil;
    [[NSNotificationCenter defaultCenter] removeObserver: self ];
    
    [self.alertPopoverController dismissPopoverAnimated:YES];
    self.alertPopoverController = nil;
    
    [super dealloc];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.screenName = @"Mail Settings Accounts Screen";
    
    [self updateData];
    
    [[NSNotificationCenter defaultCenter] removeObserver: self ];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(newAccountCreated:)
                                                 name:@"newAccountCreated"
                                               object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(newMailAccountController:)
                                                 name:@"newMailAccountController"
                                               object:nil];

    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(newOtherMailAccountController:)
                                                 name:@"newOtherMailAccountController"
                                               object:nil];
    
    if (!IS_IPAD)
    {
        if ([[GRMailAccountManager manager] getAccounts].count == 0)
            [self onAccountTypesController];
    }
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    isEditOn = NO;
    [(GRSettingsiPadController *)parentController updateHeaderForMailAccountsList];

    if (!IS_IPAD)
        [self willRotateToInterfaceOrientation:[UIApplication sharedApplication].statusBarOrientation duration:0];
    else
        [self updateData];
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
}

-(void)willRotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)duration
{

}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    if (IS_IPAD)
        return UIInterfaceOrientationIsLandscape(interfaceOrientation);
    else
        return (interfaceOrientation != UIInterfaceOrientationPortraitUpsideDown);
}


#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [tableData count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:nil];
    if (cell == nil) {
        cell = [[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier] autorelease];
        
    }
    
    cell.contentView.backgroundColor = [UIColor clearColor];
    
    CGFloat cellWidth = self.contentTable.frame.size.width;
    CGFloat cellHeight = [self tableView:tableView heightForRowAtIndexPath:indexPath];
    UIImageView *separator = [[[UIImageView alloc] init] autorelease];
    separator.frame = CGRectMake(0, cellHeight-1, cellWidth, 1);
    separator.backgroundColor = RGBCOLOR(179, 179, 179);
    [cell.contentView addSubview:separator];
    
    TMailAccount *mailAccount = [tableData objectAtIndex:[indexPath row]];
    UIImageView *iconView = nil;
    if ([mailAccount.type isEqualToString:@"gmail"]) {
        iconView = [[[UIImageView alloc] initWithImage:[UIImage imageNamed:@"Settings_MailIconGmail"]] autorelease];
    }
    if ([mailAccount.type isEqualToString:@"other"]) {
        
    }
    if (!IS_IPAD) {
        iconView.frame = CGRectMake(0, 0, iconView.frame.size.width/2, iconView.frame.size.height/2);
    }
    iconView.frame = CGRectMake(30, cellHeight/2 - iconView.frame.size.height/2, iconView.frame.size.width, iconView.frame.size.height);
    [cell.contentView addSubview:iconView];
    
    UILabel *lTitle = [[[UILabel alloc] initWithFrame:CGRectMake(80, 0, cellWidth-80, cellHeight)] autorelease];
    lTitle.backgroundColor = [UIColor clearColor];
    lTitle.font = [UIFont fontWithName:@"Helvetica" size:18];
    if (!IS_IPAD) {
        lTitle.font = [UIFont fontWithName:@"Helvetica-Bold" size:14];
        lTitle.frame = CGRectMake(40, 0, cellWidth-40, lTitle.frame.size.height);
    }
    [cell.contentView addSubview:lTitle];
    lTitle.textColor = RGBCOLOR(96, 96, 96);
    lTitle.text = mailAccount.accountName;
    
    if (isEditOn)
    {
        UIButton *bDelete = [UIButton buttonWithType:UIButtonTypeCustom];
        [bDelete setImage:[UIImage imageNamed:@"Settings_Radio_DeleteBtn.png"] forState:UIControlStateNormal];
        [bDelete setImage:[UIImage imageNamed:@"Settings_Radio_DeleteBtnOn.png"] forState:UIControlStateHighlighted];
        [bDelete addTarget:self action:@selector(deleteBtnPressed:) forControlEvents:UIControlEventTouchUpInside];
        if (IS_IPAD) {
            bDelete.frame = CGRectMake(0, 0, 47, 47);
        } else {
            bDelete.frame = CGRectMake(0, 0, 24, 24);
        }
        bDelete.frame = CGRectMake(cellWidth-bDelete.frame.size.width-20, cellHeight/2 - bDelete.frame.size.height/2, bDelete.frame.size.width, bDelete.frame.size.height);
        bDelete.tag = [indexPath row];
        [cell.contentView addSubview:bDelete];
    }

    cell.selectedBackgroundView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"mail_selected_cell_background"]];
    
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    CGFloat height = 0.0f;
    if (IS_IPAD) {
        if ([indexPath row] == 0) {
            height = 98;
        } else {
            height = 98;
        }
    } else {
        if ([indexPath row] == 0) {
            height = 50;
        } else {
            height = 49;
        }
    }
    return height;
}


#pragma mark - Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    TMailAccount *account = [tableData objectAtIndex:[indexPath row]];
    
    if ([account.type isEqualToString:@"other"])
    {
        if (newOtherAccountController) {
            [newOtherAccountController release];
            newOtherAccountController = nil;
        }
        
        newOtherAccountController = [[GRMailSettingsNewOtherAccountController alloc] init];
        newOtherAccountController.mailAccount = account;
        newOtherAccountController.delegate = self;
        newOtherAccountController.view.backgroundColor = [[UIColor blackColor] colorWithAlphaComponent:0.7f];
        [((UIViewController *)parentController).view addSubview:newOtherAccountController.view];
        [(GRSettingsiPadController *)parentController updateHeaderForMailAccountsList];
    }
    else
    {
        if (newAccountController) {
            [newAccountController release];
            newAccountController = nil;
        }
        newAccountController = [[GRMailSettingsNewAccountController alloc] init];
        newAccountController.mailAccount = account;
        newAccountController.delegate = self;
        newAccountController.view.backgroundColor = [[UIColor blackColor] colorWithAlphaComponent:0.7f];
        [((UIViewController *)parentController).view addSubview:newAccountController.view];
    }
}

#pragma mark - Helpers

- (void)onAccountTypesController
{
    [(GRSettingsiPadController *)parentController restoreHeaderToDefault];

    GRMailSettingsAccountTypeController *typeController = [[GRMailSettingsAccountTypeController alloc] init];
    typeController.view.frame = self.view.frame;
    [self.navigationController pushViewController:typeController animated:YES];
    [typeController release];
}

- (void)editOn
{
    if (!IS_IPAD)
    {
        okButton.hidden = NO;
        addButton.hidden = YES;
    }
    isEditOn = YES;
    [self.contentTable reloadData];
}

- (void)editOff
{
    if (!IS_IPAD)
    {
        okButton.hidden = YES;
        addButton.hidden = NO;
    }
    isEditOn = NO;
    [self.contentTable reloadData];
}


- (void)deleteBtnPressed:(id)sender
{
    selectedAccountIndex = ((UIButton *) sender).tag;
    
    WYPopoverBackgroundView *popoverAppearance = [WYPopoverBackgroundView appearance];
    
    [popoverAppearance setOverlayColor:[UIColor colorWithWhite:0.1f alpha:0.6f]];
    [popoverAppearance setOuterCornerRadius:0];
    [popoverAppearance setOuterShadowBlurRadius:0];
    [popoverAppearance setOuterShadowColor:[UIColor clearColor]];
    [popoverAppearance setOuterShadowOffset:CGSizeZero];
    
    [popoverAppearance setGlossShadowColor:[UIColor clearColor]];
    [popoverAppearance setGlossShadowOffset:CGSizeMake(0, 0)];
    
    [popoverAppearance setBorderWidth:1];
    [popoverAppearance setArrowHeight:0];
    [popoverAppearance setArrowBase:0];
    
    [popoverAppearance setInnerCornerRadius:0];
    [popoverAppearance setInnerShadowBlurRadius:0];
    [popoverAppearance setInnerShadowColor:[UIColor clearColor]];
    [popoverAppearance setInnerShadowOffset:CGSizeMake(0, 0)];
    
    [popoverAppearance setFillTopColor:[UIColor whiteColor]];
    [popoverAppearance setFillBottomColor:[UIColor whiteColor]];
    [popoverAppearance setOuterStrokeColor:[UIColor clearColor]];
    [popoverAppearance setInnerStrokeColor:[UIColor clearColor]];
    
    UIViewController *contentViewController = [[[UIViewController alloc] init] autorelease];
    contentViewController.contentSizeForViewInPopover = CGSizeMake(427, 180);
    
    UIView *deleteAccountAlertView = [[[NSBundle mainBundle] loadNibNamed:@"DeleteAccountAlertView" owner:self options:nil] lastObject];
    
    [[contentViewController view] addSubview:deleteAccountAlertView];
    
    
    self.alertPopoverController = [[[WYPopoverController alloc] initWithContentViewController:contentViewController] autorelease];
    self.alertPopoverController.delegate = self;
    
    [self.alertPopoverController presentPopoverAsDialogAnimated:YES];
}

- (IBAction)deleteAccountCancelButtonTapped
{
    [self.alertPopoverController dismissPopoverAnimated:YES];
    self.alertPopoverController = nil;
}

- (IBAction)deleteAccountDeleteButtonTapped
{
    [self.alertPopoverController dismissPopoverAnimated:YES];
    self.alertPopoverController = nil;
    
    TMailAccount *account = [tableData objectAtIndex:selectedAccountIndex];
    [[GRMailAccountManager manager] removeAccount:account];
    [self updateData];
}

#pragma mark - WYPopoverControllerDelegate's methods

- (BOOL)popoverControllerShouldDismissPopover:(WYPopoverController *)popoverController
{
    return YES;
}

- (void)popoverControllerDidDismissPopover:(WYPopoverController *)popoverController
{
    self.alertPopoverController = nil;
}

#pragma mark - Notifications

- (void)newAccountCreated:(id)notification
{
    [self updateData];
}

- (void)newMailAccountController:(id)notification
{
    if (newAccountController) {
        [newAccountController release];
        newAccountController = nil;
    }
    
    newAccountController = [[GRMailSettingsNewAccountController alloc] init];
    newAccountController.delegate = self;
    newAccountController.view.backgroundColor = [[UIColor blackColor] colorWithAlphaComponent:0.7f];
    [((UIViewController *)parentController).view addSubview:newAccountController.view];
}

- (void)newOtherMailAccountController:(id)notification
{
    if (newOtherAccountController) {
        [newOtherAccountController release];
        newOtherAccountController = nil;
    }
    
    newOtherAccountController = [[GRMailSettingsNewOtherAccountController alloc] init];
    newOtherAccountController.view.backgroundColor = [[UIColor blackColor] colorWithAlphaComponent:0.7f];
    [((UIViewController *)parentController).view addSubview:newOtherAccountController.view];
    [(GRSettingsiPadController *)parentController updateHeaderForMailAccountsList];
}

#pragma mark - GRCommonAlertDelegate

- (void)commonAlert:(id)alertView buttonWithIndex:(NSInteger)index
{
    if (index == 1)
    {
        TMailAccount *account = [tableData objectAtIndex:selectedAccountIndex];
        [[GRMailAccountManager manager] removeAccount:account];
        [self updateData];
    }
}

#pragma mark - Actions methods

- (IBAction)homeBtnTapped:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)editMailBtnTapped:(id)sender
{
    [self editOn];
}

- (IBAction)okMailBtnTapped:(id)sender
{
    [self editOff];
}

- (IBAction)addMailBtnTapped:(id)sender
{
    [self onAccountTypesController];
}

#pragma mark - GRMailSettingsNewAccountController delegate

- (void)onModalCancel:(id)sender {
    if ([[self.navigationController viewControllers] count] > 1) {
        [(GRSettingsiPadController *)parentController restoreHeaderToDefault];
    }
}

- (void)onModalDone:(id)sender {
    [self.navigationController popToRootViewControllerAnimated:NO];
}



@end
