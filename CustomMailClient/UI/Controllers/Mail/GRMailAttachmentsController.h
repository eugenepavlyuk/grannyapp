//
//  GRMailAttachmentsController.h
//  GrannyApp
//
//  Created by Ievgen Pavliuk on 6/1/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface GRMailAttachmentsController : GAITrackedViewController <UITableViewDelegate, UITableViewDataSource> {
    
    UITableView *contentTable;
}
@property (nonatomic, retain) IBOutlet UITableView *contentTable;

@end
