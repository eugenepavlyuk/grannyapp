//
//  GRMailSettingsAccountTypeController.m
//  GrannyApp
//
//  Created by Ievgen Pavliuk on 8/7/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "GRMailSettingsAccountTypeController.h"

@interface GRMailSettingsAccountTypeController ()

@end

@implementation GRMailSettingsAccountTypeController

#pragma mark - Initialization

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    if (!IS_IPAD)
        [self willRotateToInterfaceOrientation:[UIApplication sharedApplication].statusBarOrientation duration:0];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.screenName = @"Mail Settings Account Type Screen";
    
    dialogContainerView.layer.borderWidth = 1.f;
    dialogContainerView.layer.borderColor = RGBCOLOR(66, 66, 66).CGColor;
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
}

-(void)willRotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)duration
{

}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    if (IS_IPAD)
        return UIInterfaceOrientationIsLandscape(interfaceOrientation);
    else
        return (interfaceOrientation != UIInterfaceOrientationPortraitUpsideDown);
}


#pragma mark - Actions mathods

- (IBAction)gmailBtnTapped:(id)sender
{
    if (IS_IPAD)
        [self.navigationController popToRootViewControllerAnimated:YES];
    else
        [self.navigationController popViewControllerAnimated:NO];
    NSNotificationCenter *notificationCenter = [NSNotificationCenter defaultCenter];
    [notificationCenter postNotificationName:@"newMailAccountController"
                                      object:nil
                                    userInfo:nil];
}

- (IBAction)otherBtnTapped:(id)sender
{
    if (IS_IPAD)
        [self.navigationController popToRootViewControllerAnimated:YES];
    else
        [self.navigationController popViewControllerAnimated:NO];
    NSNotificationCenter *notificationCenter = [NSNotificationCenter defaultCenter];
    [notificationCenter postNotificationName:@"newOtherMailAccountController"
                                      object:nil
                                    userInfo:nil];
}

- (IBAction)homeBtnTapped:(id)sender
{
//    if (IS_IPAD)
        [self.navigationController popToRootViewControllerAnimated:YES];
//    else
//        [self.navigationController popViewControllerAnimated:NO];
}

@end
