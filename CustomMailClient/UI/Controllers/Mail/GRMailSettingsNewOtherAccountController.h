//
//  GRMailSettingsNewOtherAccountController.h
//  GrannyApp
//
//  Created by Ievgen Pavliuk on 8/7/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "GRMailServiceManager.h"
#import "GRMailBaseViewController.h"

@interface GRMailSettingsNewOtherAccountController : GRMailBaseViewController <UITextFieldDelegate, GRMailServiceDelegate> {
    
    UIView         *contentView;
    UITextField    *tAccountName;
    UITextField    *tUserName;
    UITextField    *tPassword;
    UITextField    *tImapUrl;
    UITextField    *tImapPort;
    UITextField    *tSmtpUrl;
    UITextField    *tSmtpPort;
    
    UIButton       *clearButton;
    UIButton       *startTLSButton;
    UIButton       *tlsButton;
    
    UITextField    *currentField;
    
    
    CGFloat animateY;
    CGFloat originY;
    NSMutableDictionary *mailSettings;
}

@property (nonatomic, retain) IBOutlet UIView  *contentView;
@property (nonatomic, retain) IBOutlet UITextField  *tAccountName;
@property (nonatomic, retain) IBOutlet UITextField  *tUserName;
@property (nonatomic, retain) IBOutlet UITextField  *tPassword;
@property (nonatomic, retain) IBOutlet UITextField  *tImapUrl;
@property (nonatomic, retain) IBOutlet UITextField  *tImapPort;
@property (nonatomic, retain) IBOutlet UITextField  *tSmtpUrl;
@property (nonatomic, retain) IBOutlet UITextField  *tSmtpPort;

@property (nonatomic, retain) IBOutlet UIButton  *clearButton;
@property (nonatomic, retain) IBOutlet UIButton  *startTLSButton;
@property (nonatomic, retain) IBOutlet UIButton  *tlsButton;

@property (nonatomic, assign) id delegate;
@property (nonatomic, retain) TMailAccount *mailAccount;

- (IBAction)okBtnTapped:(id)sender;
- (IBAction)cancelBtnTapped:(id)sender;
- (IBAction)connectionButtonTapped:(UIButton*)button;

- (void)animateViewUp;
- (void)restoreView;

@end
