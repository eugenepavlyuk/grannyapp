//
//  GRMailBaseViewController.m
//  GrannyApp
//
//  Created by Ievgen Pavliuk on 7/9/13.
//
//

#import "GRMailBaseViewController.h"

@interface GRMailBaseViewController ()

@end

@implementation GRMailBaseViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)dealloc {
    [GRMailServiceManager shared].delegate = nil;
    [super dealloc];
}

- (BOOL)prefersStatusBarHidden
{
    return YES;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
}

- (void)viewDidAppear:(BOOL)animated {
    [GRMailServiceManager shared].delegate = self;
    [super viewDidAppear:animated];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - GRMailServiceManager delegate

- (void)mailActionFailed:(NSError *)error {
}

- (void)mailActionSucceed:(NSInteger)operationCode {
}


@end
