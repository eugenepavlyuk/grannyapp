//
//  GRMessageReadController.h
//  GrannyApp
//
//  Created by Ievgen Pavliuk on 4/17/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "GRMailBaseViewController.h"

@class GRMailInboxListController;
@class TMailMessageBase;
@class TMailAccount;

@interface GRMessageReadController : GRMailBaseViewController

@property (nonatomic, retain) IBOutlet UIScrollView  *contentScrollView;
@property (nonatomic, retain) IBOutlet UILabel       *senderEmail;
@property (nonatomic, retain) IBOutlet UILabel       *receiverEmail;
@property (nonatomic, retain) IBOutlet UILabel       *subjectField;
@property (nonatomic, retain) IBOutlet UIButton      *deleteEmail;
@property (nonatomic, retain) IBOutlet UIButton      *reactionBtn;
@property (nonatomic, retain) IBOutlet UICollectionView       *attachmentsCollection;
@property (nonatomic, retain) IBOutlet UIWebView *messageWebView;
@property (nonatomic, retain) IBOutlet UIActivityIndicatorView       *activitiIndicator;
@property (nonatomic, retain) IBOutlet UIView *popupDeleteView;
@property (nonatomic, retain) IBOutlet UIView *popupReplyView;
@property (retain, nonatomic) IBOutlet UIView *headerView;

@property (nonatomic, retain) TMailMessageBase *messageModel;
@property (nonatomic, assign) GRMailInboxListController * masterController;
@property (nonatomic, assign) NSInteger       folderIndex;



- (IBAction)backBtnTapped:(id)sender;
- (IBAction)reactionBtnTapped:(id)sender;
- (IBAction)deleteBtnTapped:(id)sender;
- (IBAction)onDelete;
- (IBAction)onCancel;

- (void)updateRecieverName;
- (void)updateSubjectField;
- (void)updateSenderName;
- (CGSize)getSizeForMessageBody:(NSString *)bodyText;

- (IBAction)replyPopupReplyClicked;
- (IBAction)replyPopupReplyAllClicked;
- (IBAction)replyPopupForwardClicked;
- (IBAction)replyPopupPrintClicked;
- (IBAction)replyPopupCancelClicked;
@end
