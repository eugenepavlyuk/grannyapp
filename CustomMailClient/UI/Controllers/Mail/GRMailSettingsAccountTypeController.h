//
//  GRMailSettingsAccountTypeController.h
//  GrannyApp
//
//  Created by Ievgen Pavliuk on 8/7/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "GRMailBaseViewController.h"

@interface GRMailSettingsAccountTypeController : GRMailBaseViewController
{
    IBOutlet UIView *dialogContainerView;
}

- (IBAction)gmailBtnTapped:(id)sender;
- (IBAction)otherBtnTapped:(id)sender;
- (IBAction)homeBtnTapped:(id)sender;

@end
