//
//  GRMailInboxListController.h
//  GrannyApp
//
//  Created by Ievgen Pavliuk on 4/13/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "GRMailServiceManager.h"
#import "TMailAccount.h"
#import "GRMailBaseViewController.h"
#import "GRNewMessageController.h"
#import "GRContact.h"
#import "DataManager.h"

@class GRFolderButton;

@interface GRMailInboxListController : GRMailBaseViewController <UITableViewDataSource, UITableViewDelegate, GRMailServiceDelegate> {
    NSMutableArray   *tableData;
    UIView           *loadingView;
    UILabel          *lLoadingInfo;
    UIButton         *composeBtn;
    UIActivityIndicatorView *indicatorView;
    
    
    
    TMailAccount     *mailAccount;
    GRContact        *fromContact;
    BOOL              isLoaded;
    
    NSInteger         messagesTotal;
    NSInteger         messageNextIndex;
    NSInteger         fetchLimit;
    NSInteger         folderIndex;
    
    BOOL              navBarShouldHide;
    BOOL              isMoreMessages;
    BOOL              isInProcess;
    
    GRNewMessageController  *newMessageController;
    NSTimer *timer;
    
    UIRefreshControl *refreshControl;
}
@property (nonatomic, retain) IBOutlet UITableView *contentTable;
@property (nonatomic, retain) IBOutlet UIView *loadingView;
@property (nonatomic, retain) IBOutlet UILabel *lLoadingInfo;
@property (nonatomic, retain) IBOutlet UILabel *sendingMessageLabel;
@property (nonatomic, retain) IBOutlet UIButton *composeBtn;

@property (nonatomic, retain) IBOutlet GRFolderButton *inboxBtn;
@property (nonatomic, retain) IBOutlet GRFolderButton *draftBtn;
@property (nonatomic, retain) IBOutlet GRFolderButton *sentBtn;
@property (nonatomic, retain) IBOutlet GRFolderButton *trashBtn;
@property (nonatomic, retain) IBOutlet UIActivityIndicatorView *indicatorView;
@property (retain, nonatomic) IBOutlet UIView *headerView;

@property (nonatomic, retain) TMailAccount     *mailAccount;
@property (nonatomic, retain) GRContact        *fromContact;

- (IBAction)onSendMsgController:(id)sender;
- (IBAction)backBtnTapped:(id)sender;
- (void)requestMail:(BOOL)loadMore;
- (void) reloadTableDataWithTypeFolder:(int)typeFolder;

- (void)mailActionSucceed:(NSInteger)operationCode;

- (IBAction)inboxBtnTapped:(id)sender;
- (IBAction)draftBtnTapped:(id)sender;
- (IBAction)sentBtnTapped:(id)sender;
- (IBAction)trashBtnTapped:(id)sender;

- (void)allOperationsCompleted;
- (void)onModalDone;
- (void)readControllerClosed;
@end
