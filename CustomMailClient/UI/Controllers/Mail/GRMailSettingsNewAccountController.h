//
//  GRMailSettingsNewAccountController.h
//  GrannyApp
//
//  Created by Ievgen Pavliuk on 7/30/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "GRCommonAlertView.h"
#import "GRMailBaseViewController.h"
#import "TMailAccount.h"

@interface GRMailSettingsNewAccountController : GRMailBaseViewController <UITextFieldDelegate, GRCommonAlertDelegate> {
    
    UITextField *tAccountName;
    UITextField *tUserName;
    UITextField *tPassword;
    
    NSMutableDictionary *mailSettings;
    
    UIView       *contentView;
    
    CGFloat        originY;
    NSInteger      alertMessageIndex;
    id             delegate;
    BOOL           isCancel;
    BOOL           isDone;
    
    TMailAccount *mailAccount;

}
@property (nonatomic, retain) IBOutlet UITextField *tAccountName;
@property (nonatomic, retain) IBOutlet UITextField *tUserName;
@property (nonatomic, retain) IBOutlet UITextField *tPassword;
@property (nonatomic, retain) IBOutlet UIView *contentView;
@property (nonatomic, assign) id delegate;
@property (nonatomic, retain) TMailAccount *mailAccount;

- (IBAction)okBtnTapped:(id)sender;
- (IBAction)cancelBtnTapped:(id)sender;

- (void)animateContentViewUp;
- (void)restoreContentView;
@end
