//
//  GRMailSettingsAccountsController.h
//  GrannyApp
//
//  Created by Ievgen Pavliuk on 7/30/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "GRMailSettingsNewAccountController.h"
#import "GRMailSettingsNewOtherAccountController.h"
#import "GRCommonAlertView.h"
#import "GRMailBaseViewController.h"

@interface GRMailSettingsAccountsController : GRMailBaseViewController <UITableViewDelegate, UITableViewDataSource, GRCommonAlertDelegate> {
    
    UITableView      *contentTable;
    NSMutableArray   *tableData;

    GRMailSettingsNewAccountController       *newAccountController; 
    GRMailSettingsNewOtherAccountController  *newOtherAccountController;
    
    UIView *backView;
    id parentController;
    
    NSInteger selectedAccountIndex;
    
    BOOL  isEditOn;
}
@property (nonatomic, retain) IBOutlet UITableView *contentTable;
@property (nonatomic, retain) IBOutlet UIButton *addButton;
@property (nonatomic, retain) IBOutlet UIButton *okButton;
@property (nonatomic, retain) IBOutlet UIButton *editButton;
@property (nonatomic, assign) id parentController;

- (void)updateData;
- (void)onAccountTypesController;
- (void)editOn;
- (void)editOff;

- (IBAction)homeBtnTapped:(id)sender;
- (IBAction)editMailBtnTapped:(id)sender;
- (IBAction)okMailBtnTapped:(id)sender;
- (IBAction)addMailBtnTapped:(id)sender;
- (IBAction)deleteAccountCancelButtonTapped;
- (IBAction)deleteAccountDeleteButtonTapped;

@end
