//
//  GRMessageReadController.m
//  GrannyApp
//
//  Created by Ievgen Pavliuk on 4/17/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "GRMessageReadController.h"
#import "GRMailAttachmentsController.h"
#import "GRMailDataManager.h"
#import "TMessageAttachment.h"
#import "GRPeopleManager.h"
#import "GRContact.h"
#import "GRAttachmentPdfController.h"
#import "MFDocumentManager.h"
#import "GRMailAccountManager.h"
#import "GRResource.h"
#import "GRMailInboxListController.h"
#import "GRInternetViewController.h"
#import "GRMailAttachemntCell.h"
#import "TMailMessageBase.h"
#import "GRNewMessageController.h"
#import "GRMailAttachmentView.h"

#define ATTACHMENT_HEIGHT 72
#define ATTACHMENT_HEIGHT_iPHONE 20

@interface GRMessageReadController () <UIPrintInteractionControllerDelegate, UIWebViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout>
{
    
    BOOL           isReactionDisplayed;
    BOOL           isRendredOnce;
    
    GRNewMessageController  *newMessageController;
    
    UIPrintInteractionController *printInteraction;
        
    NSMutableArray *attachmentsArray;
}
@end

@implementation GRMessageReadController

#pragma mark - GRMailServiceDelegate

- (void)mailActionSucceed:(NSInteger)operationCode {
    switch (operationCode) {
        case MAIL_FETCH_OP:
            [self refreshMessageContent];
            break;
        case MAIL_ALL_OPERATIONS_DONE:
            [_masterController allOperationsCompleted];
            break;
            
        default:
            break;
    }
}

- (void)mailActionFailed:(NSError *)error {
    GRCommonAlertView *alert = [[[GRCommonAlertView alloc] initCommonAlert:[error localizedDescription] inView:self.view] autorelease];
    alert.delegate = nil;
}

#pragma mark - Mail Server api calls 

- (void)fetchMessage {
    if (![[YLReachability reachabilityForInternetConnection] isReachable])
    {
        self.activitiIndicator.hidden = YES;
        GRCommonAlertView *alert = [[[GRCommonAlertView alloc] initCommonAlert:NSLocalizedString(@"No Internet connection.", @"No Internet connection.") inView:self.view] autorelease];
        alert.delegate = nil;
        return;
    }

    [GRMailServiceManager shared].delegate = self;

    if ([_messageModel.isFetched integerValue] == 1) {
        // display local message
        [self refreshMessageContent];
    } else {
        self.activitiIndicator.hidden = NO;
        NSInteger uid = [_messageModel.uid integerValue];
        [[GRMailServiceManager shared] mailActionFetchMessageWithUID:uid folderIndex:self.folderIndex];
    }
}

#pragma mark - Initialization
- (void) dealloc
{
    _masterController = nil;
    [_receiverEmail release];
    [_subjectField release];
    [_contentScrollView release];
    [_senderEmail release];
    [_messageModel release];
    [attachmentsArray release];
    [_attachmentsCollection release];
    [_activitiIndicator release];
    [_headerView release];
    [_popupDeleteView release];
    [_popupReplyView release];
    
    [_messageWebView stopLoading];
    _messageWebView.delegate = nil;
    [_messageWebView release];
    
    [super dealloc];
}

#pragma mark - Layouts helpers

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.screenName = NSLocalizedString(@"Mail Message Screen", @"Mail Message Screen Title");
    self.view.backgroundColor = [[UIColor blackColor] colorWithAlphaComponent:0.8f];
    self.headerView.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"email_msg_header_bkg"]];
    self.messageWebView.scrollView.scrollEnabled = NO;
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    [self updateSenderName];
    [self updateRecieverName];
    [self updateSubjectField];
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    if (!isRendredOnce) {
        isRendredOnce = YES;
        [self fetchMessage];
    }
}

-(void)willRotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)duration
{
    if (!IS_IPAD)
    {
        if( UIInterfaceOrientationIsLandscape(toInterfaceOrientation) )
        {
            [[NSBundle mainBundle] loadNibNamed: [NSString stringWithFormat:@"%@", NSStringFromClass([self class])]
                                          owner: self
                                        options: nil];
            [self viewDidLoad];
        }
        else
        {
            [[NSBundle mainBundle] loadNibNamed: [NSString stringWithFormat:@"%@-portrait", NSStringFromClass([self class])]
                                          owner: self
                                        options: nil];
            [self viewDidLoad];
        }
    }
}
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    if (IS_IPAD)
        return UIInterfaceOrientationIsLandscape(interfaceOrientation);
    else
        return (interfaceOrientation != UIInterfaceOrientationPortraitUpsideDown);
}


#pragma mark - Action methods
- (IBAction)deleteBtnTapped:(id)sender
{
    if (!_popupDeleteView)
    {
        if (isReactionDisplayed)
        {
            self.reactionBtn.selected = NO;
            isReactionDisplayed = NO;
            [self.popupReplyView removeFromSuperview];
        }
        
        if (IS_IPAD)
        {
            CGRect coverRect = CGRectMake(62, 73, 900, 675);
            
            [[NSBundle mainBundle] loadNibNamed:@"GRMailPopupDelete" owner:self options:nil];
            self.popupDeleteView.frame = coverRect;
            _popupDeleteView.backgroundColor = [UIColor colorWithWhite:0 alpha:0.73f];
            [self.view addSubview:_popupDeleteView];
        }
    }
}
- (IBAction)backBtnTapped:(id)sender
{
    self.deleteEmail.selected = NO;
    if (IS_IPAD) {
        [_masterController onModalDone];
        [self.view removeFromSuperview];
    }
    else
        [self.navigationController popViewControllerAnimated:YES];
    [_masterController readControllerClosed];
    [[GRMailServiceManager shared] resetFetching];
}

- (IBAction)reactionBtnTapped:(id)sender
{
    [self onCancel];
    if (!isReactionDisplayed)
    {
        CGRect coverRect = CGRectMake(62, 73, 900, 675);
        
        isReactionDisplayed = YES;
        [[NSBundle mainBundle] loadNibNamed:@"GRMailPopupReply" owner:self options:nil];
        self.popupReplyView.frame = coverRect;
        self.popupReplyView.backgroundColor = [UIColor colorWithWhite:0 alpha:0.73f];
        [self.view addSubview:self.popupReplyView];
    }
    else
    {
        isReactionDisplayed = NO;
        [self.popupReplyView removeFromSuperview];
    }
}

- (IBAction)onCancel
{
    if (_popupDeleteView)
    {
        [_popupDeleteView removeFromSuperview];
        self.popupDeleteView = nil;
    }
}
- (IBAction)onDelete {
    NSInteger uid = [self.messageModel.uid integerValue];
    
    [[GRMailServiceManager shared] moveMailFromFolder:self.folderIndex toFolder:MAIL_FOLDER_TRASH withUid:uid];
    self.deleteEmail.selected = NO;
    if (IS_IPAD) {
        if ([_masterController respondsToSelector:@selector(onModalDone)]) {
            [_masterController performSelector:@selector(onModalDone)];
        }
        [self.view removeFromSuperview];
    }
}

#pragma mark - Content management
- (void)refreshMessageContent {
    self.activitiIndicator.hidden = YES;
    [self locateAttachments];
    [self loadHTML];
}

- (void)loadHTML {
    NSString *htmlText = self.messageModel.messageBodyHTML;;
    BOOL isInlineAttachments = [self.messageModel.isInlineAttachments boolValue];
    
    if (isInlineAttachments) {
        NSURL *cacheURL = [NSURL URLWithString:[self getLocalCachedUrl]];
        NSString *updHtml = [self removeInlineAttachementsDivs:htmlText];
        [self.messageWebView loadHTMLString:updHtml baseURL:cacheURL];
    } else {
        [self.messageWebView loadHTMLString:htmlText baseURL:nil];
    }
    
    self.contentScrollView.contentSize = CGSizeMake(self.contentScrollView.frame.size.width, CGRectGetMaxY(self.messageWebView.frame));
}

- (NSString *)getLocalCachedUrl {
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSCachesDirectory,
                                                         NSUserDomainMask, YES);
    NSString *cacheDirectory = [NSString stringWithString:[paths objectAtIndex:0]];
    
    NSMutableString *path = [[[cacheDirectory stringByAppendingPathComponent:self.messageModel.messageID] mutableCopy] autorelease];
    
    [path appendString:@"/"];
    NSRange renge= NSMakeRange(0, path.length);
    [path replaceOccurrencesOfString:@"/" withString:@"//" options:NSCaseInsensitiveSearch range:renge];
    renge= NSMakeRange(0, path.length);
    [path replaceOccurrencesOfString:@" " withString:@"%20" options:NSCaseInsensitiveSearch range:renge];
    [path insertString:@"file://" atIndex:0];

    return path;
}

#pragma mark - UICollectionViewDataSource

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return [attachmentsArray count];
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView
                  cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *identifier = @"GRMailAttachemntCell";
    
    GRMailAttachemntCell *collectionCell =
    [collectionView dequeueReusableCellWithReuseIdentifier:identifier
                                              forIndexPath:indexPath];
    
    NSDictionary *item = [attachmentsArray objectAtIndex:indexPath.row];
    [collectionCell updateForItem:item];
    
    return collectionCell;
}

#pragma mark - UICollectionViewDelegate

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    NSDictionary *item = [attachmentsArray objectAtIndex:indexPath.row];
    NSString *mimeType = [item objectForKey:MimeTypeKey];
    NSString *filePath = [item objectForKey:PathKey];
    if ([mimeType rangeOfString:@"pdf" options:NSCaseInsensitiveSearch].location != NSNotFound) {
        if ([[NSFileManager defaultManager] fileExistsAtPath:filePath])
        {
            NSData *pdf = [NSData dataWithContentsOfFile:filePath];
            CGDataProviderRef provider = CGDataProviderCreateWithCFData((CFDataRef)pdf);
            MFDocumentManager *docManager = [[MFDocumentManager alloc] initWithDataProvider:provider];
            if (docManager) {
                GRAttachmentPdfController *pdfController = [[GRAttachmentPdfController alloc] initWithDocumentManager:docManager];
                [(UIViewController *)_masterController presentViewController:pdfController animated:YES completion:^{
                    
                }];
            }
            
        }
        else
        {
            GRCommonAlertView *alert = [[[GRCommonAlertView alloc] initCommonAlert:NSLocalizedString(@"Cover not found", @"Cover not found") inView:self.view] autorelease];
            alert.delegate = nil;
        }
    } else if ([mimeType rangeOfString:@"word" options:NSCaseInsensitiveSearch].location != NSNotFound ||
               [mimeType rangeOfString:@"excel" options:NSCaseInsensitiveSearch].location != NSNotFound || [mimeType rangeOfString:@"ppt" options:NSCaseInsensitiveSearch].location != NSNotFound) {
        NSString *nibName = UIInterfaceOrientationIsLandscape([UIApplication sharedApplication].statusBarOrientation) ? @"GRMailAttachmentView" : @"GRMailAttachmentView-portrait";
        if (IS_IPAD)
        {
            nibName = @"GRMailAttachmentView_iPad";
        }
        GRMailAttachmentView *attachmentView = (GRMailAttachmentView *)[GRResource viewFromNib:nibName owner:nil];
        [attachmentView loadDocument:filePath];
        attachmentView.saveBtn.hidden = YES;
        [self.view addSubview:attachmentView];
    } else if ([mimeType rangeOfString:@"image" options:NSCaseInsensitiveSearch].location != NSNotFound) {
        NSString *nibName = UIInterfaceOrientationIsLandscape([UIApplication sharedApplication].statusBarOrientation) ? @"GRMailAttachmentView" : @"GRMailAttachmentView-portrait";
        if (IS_IPAD) {
            nibName = @"GRMailAttachmentView_iPad";
        }
        GRMailAttachmentView *attachmentView = (GRMailAttachmentView *)[GRResource viewFromNib:nibName owner:nil];
        [attachmentView loadImage:filePath];
        attachmentView.saveBtn.hidden = [GRMailServiceManager shared].currentFolder == 2;
        [self.view addSubview:attachmentView];
    }

}


#pragma mark - Helpers
- (NSString *)removeInlineAttachementsDivs:(NSString *)html {
    
    NSString *lookUpString = nil;
    
    NSScanner *theScanner = [NSScanner scannerWithString:html];
    theScanner.charactersToBeSkipped = NULL;
    NSInteger index = 0;
    while (![theScanner isAtEnd]) {
        NSCharacterSet *charset = [NSCharacterSet characterSetWithCharactersInString:@"/>"];
        [theScanner scanUpToString:@"<img" intoString:nil];
        [theScanner scanUpToCharactersFromSet:charset intoString:&lookUpString];
        if (lookUpString != nil && lookUpString.length != 0) {
            lookUpString = [lookUpString stringByAppendingFormat:@"/>"];
            if ([html rangeOfString:lookUpString].location != NSNotFound) {
                NSString *imgName = [self getFileNameFromAttachmentsByIndex:index];
                NSInteger width = [self getWidthForAttachmentByIndex:index];
                if (width > 430) {
                    width =430;
                }

                NSString *replacedString = [NSString stringWithFormat:@"<img width=\"%i\" src=\"%@\" />", width, imgName];
                html = [html stringByReplacingOccurrencesOfString:lookUpString withString:replacedString];
                index++;
            }
        }
    }
    return html;
}

#pragma mark - Layout helpers
- (void)locateAttachmentCollection {
    if ([attachmentsArray count] != 0 ) {
        [self.attachmentsCollection setHidden:NO];
        [self.attachmentsCollection setBackgroundColor:[UIColor whiteColor]];
        [self.attachmentsCollection setAllowsSelection:YES];
        [self.attachmentsCollection registerNib:[UINib nibWithNibName:@"GRMailAttachemntCell_iPad" bundle:nil] forCellWithReuseIdentifier:@"GRMailAttachemntCell"];
        [self.attachmentsCollection reloadData];
        
        // update height
        CGRect frame = self.attachmentsCollection.frame;
        frame.size.height = self.attachmentsCollection.collectionViewLayout.collectionViewContentSize.height;
        self.attachmentsCollection.frame = frame;
        
        // position web view
        frame = self.messageWebView.frame;
        frame.origin.y = CGRectGetMaxY(self.attachmentsCollection.frame) + 16;
        self.messageWebView.frame = frame;
    } else {
        CGRect attColFrame = self.attachmentsCollection.frame;
        attColFrame.size.height = 0;
        self.attachmentsCollection.frame = attColFrame;
        [self.attachmentsCollection setHidden:YES];
    }
}

- (void)updateSenderName
{
    NSString *sender = self.messageModel.senderName? self.messageModel.senderName : self.messageModel.senderEmail;
    self.senderEmail.text = sender;
}

- (void)locateAttachments {
    if (!attachmentsArray) {
        attachmentsArray = [[NSMutableArray alloc] init];
    }
    NSInteger uid = _messageModel.uid.integerValue;
    
    NSArray *attachments = [[GRMailDataManager sharedManager] getAttachmentsForMessageID:uid];
    for (TMessageAttachment *atm in attachments) {
        NSDictionary *item = [NSDictionary dictionaryWithObjectsAndKeys:atm.contentType, MimeTypeKey, atm.fileName, FileNameKey, atm.filePath, PathKey, nil];
        [attachmentsArray addObject:item];
    }
    [self locateAttachmentCollection];

}
 
- (void)updateRecieverName
{
    if (self.folderIndex == 2)
    {
        NSArray *tos = [NSKeyedUnarchiver unarchiveObjectWithData:self.messageModel.tos];
        NSString *toString = @"";
        
        int i = 0;
        
        for (NSString *email in tos)
        {
            if (i > 1)
            {
                toString = [toString stringByAppendingString:NSLocalizedString(@"and others", @"and others")];
                break ;
            }
            
            GRContact *contact = [[GRPeopleManager sharedManager] findContactByEmail:email];
            
            if (contact)
            {
                toString = [toString stringByAppendingFormat:@"%@", contact.fullname];
            }
            else
            {
                toString = [toString stringByAppendingFormat:@"%@", email];
            }
            
            if (i != [tos count] - 1)
            {
                toString = [toString stringByAppendingString:@", "];
            }
            
            i++;
        }
        
        self.receiverEmail.text = [NSString stringWithFormat:@"%@", toString];
    }
    else
    {
        NSString *receiverEmail = [GRMailServiceManager shared].currentAccount.userName;
        
        NSString *receiverName = [[GRPeopleManager sharedManager] findContactByEmail:receiverEmail].fullname;
        
        self.receiverEmail.text = receiverName?:receiverEmail;
    }
}

- (void)updateSubjectField
{
    NSString *subject = self.messageModel.subject;

    self.subjectField.text = subject;
}

- (CGSize)getSizeForMessageBody:(NSString *)bodyText
{
    CGFloat fontSize = 0;
    CGFloat maxWidth = 820.0f;
    if (IS_IPAD)
    {
        fontSize = 26;
    }
    else
    {
        fontSize = 14;
        maxWidth = 400.0f;
    }

    CGSize maximumLabelSize = CGSizeMake(maxWidth, 10000);
    CGSize expectedLabelSize = [bodyText sizeWithFont: [UIFont fontWithName:@"Helvetica" size:fontSize]
                                    constrainedToSize:maximumLabelSize];
    return expectedLabelSize;
}

#pragma mark - GRMailself.popupReplyViewDelegate

-(IBAction)replyPopupReplyClicked
{
    if (newMessageController) {
        [newMessageController release];
        newMessageController = nil;
    }

    newMessageController = [[GRNewMessageController alloc] init];
    newMessageController.operationIndex = 1;
    newMessageController.messageModel = self.messageModel;
    newMessageController.folderIndex = self.folderIndex;
    if (IS_IPAD)
        [self.view addSubview:newMessageController.view];
    else
        [self.navigationController pushViewController:newMessageController animated:YES];
    [newMessageController reloadSubviews];
    
    self.reactionBtn.selected = NO;
    isReactionDisplayed = NO;
    [self.popupReplyView removeFromSuperview];
}

- (IBAction)replyPopupReplyAllClicked
{
    if (newMessageController) {
        [newMessageController release];
        newMessageController = nil;
    }
    NSString *senderMail = _messageModel.senderEmail;
    NSString *senderName = _messageModel.senderName;
    NSMutableArray *ccContacts = [NSMutableArray array];
    NSArray *toContacts = [NSKeyedUnarchiver unarchiveObjectWithData:_messageModel.tos];
    
    for (int i = 0; i < toContacts.count; i++)
    {
        NSString *sMail = [toContacts objectAtIndex:i];
        if (![sMail isEqualToString:[GRMailServiceManager shared].currentAccount.userName]) {
            GRContact *contact = [[GRPeopleManager sharedManager] findContactByEmail:[toContacts objectAtIndex:i]];
            if (!contact)
            {
                contact = [[[GRContact alloc] init] autorelease];
                contact.email = [toContacts objectAtIndex:i];
            }
            [ccContacts addObject:contact];
        }
    }

    GRContact *contact = [[GRPeopleManager sharedManager] findContactByEmail:senderMail];
    if (contact)
    {
        if (contact.photourl && [[NSFileManager defaultManager] fileExistsAtPath:contact.photourl]) {
//            self.message.senderPhotoUrl = contact.photoPreviewUrl;
        }
    }
    else
    {
        contact = [[[GRContact alloc] init] autorelease];
        contact.email = senderMail;
        contact.firstname = senderName;
    }
    newMessageController = [[GRNewMessageController alloc] init];
    newMessageController.operationIndex = 2;
    newMessageController.messageModel = self.messageModel;
    newMessageController.view.backgroundColor = [[UIColor blackColor] colorWithAlphaComponent:0.7f];
    newMessageController.selectedToContacts = [NSMutableArray arrayWithObject:contact];
    if (ccContacts.count > 0)
        newMessageController.selectedBccContacts = [NSMutableArray arrayWithArray:ccContacts];
    if (IS_IPAD)
        [self.view addSubview:newMessageController.view];
    else
        [self.navigationController pushViewController:newMessageController animated:YES];
    [newMessageController reloadSubviews];
    
    self.reactionBtn.selected = NO;
    isReactionDisplayed = NO;
    [self.popupReplyView removeFromSuperview];
}


- (IBAction)replyPopupForwardClicked
{
    if (newMessageController) {
        [newMessageController release];
        newMessageController = nil;
    }
    newMessageController = [[GRNewMessageController alloc] init];
    newMessageController.messageModel = self.messageModel;
    newMessageController.operationIndex = 3;
    newMessageController.view.backgroundColor = [[UIColor blackColor] colorWithAlphaComponent:0.7f];
    if (IS_IPAD)
        [self.view addSubview:newMessageController.view];
    else
        [self.navigationController pushViewController:newMessageController animated:YES];
    [newMessageController reloadSubviews];
    
    self.reactionBtn.selected = NO;
    isReactionDisplayed = NO;
    [self.popupReplyView removeFromSuperview];
}


- (IBAction)replyPopupPrintClicked {
    self.reactionBtn.selected = NO;
    isReactionDisplayed = NO;
    [self.popupReplyView removeFromSuperview];
    
    Class printInteractionController = NSClassFromString(@"UIPrintInteractionController");
    
    if ((printInteractionController != nil) && [printInteractionController isPrintingAvailable])
    {
        printInteraction = [printInteractionController sharedPrintController];
        printInteraction.delegate = self;
        
        UIPrintInfo *printInfo = [NSClassFromString(@"UIPrintInfo") printInfo];
        
        printInfo.duplex = UIPrintInfoDuplexLongEdge;
        printInfo.outputType = UIPrintInfoOutputGeneral;
        printInfo.jobName = self.messageModel.subject?:@"";
        
        printInteraction.printInfo = printInfo;
        printInteraction.showsPageRange = YES;
        
        UIViewPrintFormatter *formatter = [self.view viewPrintFormatter];
        printInteraction.printFormatter = formatter;
        
        [printInteraction presentAnimated:YES completionHandler:^(UIPrintInteractionController *printInteractionController, BOOL completed, NSError *error) {
            
        }];
    }
}
- (IBAction)replyPopupCancelClicked
{
    self.reactionBtn.selected = NO;
    isReactionDisplayed = NO;
    [self.popupReplyView removeFromSuperview];
}

#pragma mark - UIWebView Delegates

- (BOOL)webView:(UIWebView *)webView shouldStartLoadWithRequest:(NSURLRequest *)request navigationType:(UIWebViewNavigationType)navigationType {
    if ([[request URL].absoluteString rangeOfString:@"http" options:NSCaseInsensitiveSearch].location != NSNotFound) {
        GRInternetViewController *internetViewController = [[GRInternetViewController alloc] init];
        internetViewController.externalLink = [request URL].absoluteString;
        [[AppDelegate getInstance].window.navController pushViewController:internetViewController animated:YES];
        [internetViewController release];

        return NO;
        
    } else {
        return YES;
    }

}

- (NSURLRequest *)webView:(UIWebView *)sender resource:(id)identifier willSendRequest:(NSURLRequest *)request redirectResponse:(NSURLResponse *)redirectResponse fromDataSource:(id)dataSource
{
    if ([[[request URL] scheme] isEqualToString:@"x-mailcore-msgviewloaded"]) {
        [self loadImages:sender];
    }

	return request;
}

- (void)loadImages:(UIWebView *)webView {
    NSString * result = [webView stringByEvaluatingJavaScriptFromString:@"findCIDImageURL()"];
	NSData * data = [result dataUsingEncoding:NSUTF8StringEncoding];
	NSError *error = nil;
	NSArray * imagesURLStrings = [NSJSONSerialization JSONObjectWithData:data options:0 error:&error];
    NSInteger index = 0;
    for(NSString * urlString in imagesURLStrings) {
        NSString * filePath = [self getPathFromAttachemntsByIndex:index];
        if (!filePath) {
            continue;
        }
        NSDictionary * args = @{ @"URLKey": urlString, @"LocalPathKey": filePath };
        NSString * jsonString = [self _jsonEscapedStringFromDictionary:args];
        NSString * replaceScript = [NSString stringWithFormat:@"replaceImageSrc(%@)", jsonString];
        [webView stringByEvaluatingJavaScriptFromString:replaceScript];
        index++;
    }

}

- (NSString *) _jsonEscapedStringFromDictionary:(NSDictionary *)dictionary
{
	NSData * json = [NSJSONSerialization dataWithJSONObject:dictionary options:0 error:nil];
	NSString * jsonString = [[NSString alloc] initWithData:json encoding:NSUTF8StringEncoding];
	return jsonString;
}

- (NSString *)getPathFromAttachemntsByIndex:(NSInteger)index {
    NSString  *filePath = nil;
    NSInteger uid = _messageModel.uid.integerValue;

    NSArray *attachments = [[GRMailDataManager sharedManager] getAttachmentsForMessageID:uid];
    if (index < [attachments count]) {
        TMessageAttachment *attachement = [attachments objectAtIndex:index];
        filePath = attachement.filePath;
    }
    return filePath;
}

- (NSString *)getFileNameFromAttachmentsByIndex:(NSInteger)index {
    NSString  *fileName = nil;
    NSInteger uid = _messageModel.uid.integerValue;
    
    NSArray *attachments = [[GRMailDataManager sharedManager] getAttachmentsForMessageID:uid];
    if (index < [attachments count]) {
        TMessageAttachment *attachement = [attachments objectAtIndex:index];
        if ([attachement.contentType rangeOfString:@"image"].location != NSNotFound) {
            fileName = [attachement.filePath lastPathComponent];
        }
    }
    return fileName;
    
}

- (NSInteger)getWidthForAttachmentByIndex:(NSInteger)index {
    NSInteger width = 0;
    NSInteger uid = _messageModel.uid.integerValue;
    
    NSArray *attachments = [[GRMailDataManager sharedManager] getAttachmentsForMessageID:uid];
    if (index < [attachments count]) {
        TMessageAttachment *attachement = [attachments objectAtIndex:index];
        if ([attachement.contentType rangeOfString:@"image"].location != NSNotFound) {
            NSString *path = attachement.filePath;
            NSData *data = [NSData dataWithContentsOfFile:path];
            UIImage *image = [UIImage imageWithData:data];
            if (image) {
                width = image.size.width;
            }
        }
    }
    
    
    return width;
}

- (void)webViewDidFinishLoad:(UIWebView *)aWebView {
    NSString* js =
    @"var meta = document.createElement('meta'); " \
    "meta.setAttribute( 'name', 'viewport' ); " \
    "meta.setAttribute( 'content', 'width = device-width, user-scalable = no' ); " \
    "document.getElementsByTagName('head')[0].appendChild(meta)";
    [aWebView stringByEvaluatingJavaScriptFromString: js];
    
    NSString *jsString = [[NSString alloc] initWithFormat:@"document.getElementsByTagName('body')[0].style.webkitTextSizeAdjust= '%d%%'",
                          150];
    [aWebView stringByEvaluatingJavaScriptFromString:jsString];
    [jsString release];
    
    CGRect frame = aWebView.frame;
    frame.size.height = 1;
    aWebView.frame = frame;
    CGSize fittingSize = [aWebView sizeThatFits:CGSizeZero];
    frame.size.height = fittingSize.height;
    aWebView.frame = frame;
    CGFloat maxWebViewY = CGRectGetMaxY(aWebView.frame);
    self.contentScrollView.contentSize = CGSizeMake(self.contentScrollView.frame.size.width, maxWebViewY);
}


#pragma mark -
#pragma mark UIPrintInteractionControllerDelegate

- (void)printInteractionControllerDidDismissPrinterOptions:(UIPrintInteractionController *)printInteractionController
{
    //NSLog(@"printInteractionControllerDidDismissPrinterOptions");
    printInteraction = nil;
}
@end
