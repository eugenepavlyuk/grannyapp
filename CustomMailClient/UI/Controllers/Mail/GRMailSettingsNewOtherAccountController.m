//
//  GRMailSettingsNewOtherAccountController.m
//  GrannyApp
//
//  Created by Ievgen Pavliuk on 8/7/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "GRMailSettingsNewOtherAccountController.h"
#import "GRMailAccountManager.h"
#import "WYPopoverController.h"
#import "GRAlertViewController.h"

@interface GRMailSettingsNewOtherAccountController () <WYPopoverControllerDelegate>

@property (nonatomic, retain) WYPopoverController *alertPopoverController;

@end

@implementation GRMailSettingsNewOtherAccountController

@synthesize contentView;
@synthesize tAccountName;
@synthesize tUserName;
@synthesize tPassword;
@synthesize tImapUrl;
@synthesize tImapPort;
@synthesize tSmtpUrl;
@synthesize tSmtpPort;

@synthesize clearButton;
@synthesize startTLSButton;
@synthesize tlsButton;

#pragma mark - GRMailServiceDelegate

- (void)mailActionSucceed:(NSInteger)operationCode {
    switch (operationCode) {
        case MAIL_VERIFICATION_OP:
            [[AppDelegate getInstance].window unBlockUI];
            [[GRMailAccountManager manager] createAccount:mailSettings];
            NSNotificationCenter *notificationCenter = [NSNotificationCenter defaultCenter];
            [notificationCenter postNotificationName:@"newAccountCreated"
                                              object:nil
                                            userInfo:nil];
            
            WYPopoverBackgroundView *popoverAppearance = [WYPopoverBackgroundView appearance];
            
            [popoverAppearance setOverlayColor:[UIColor colorWithWhite:0.1f alpha:0.6f]];
            [popoverAppearance setOuterCornerRadius:0];
            [popoverAppearance setOuterShadowBlurRadius:0];
            [popoverAppearance setOuterShadowColor:[UIColor clearColor]];
            [popoverAppearance setOuterShadowOffset:CGSizeZero];
            
            [popoverAppearance setGlossShadowColor:[UIColor clearColor]];
            [popoverAppearance setGlossShadowOffset:CGSizeMake(0, 0)];
            
            [popoverAppearance setBorderWidth:1];
            [popoverAppearance setArrowHeight:0];
            [popoverAppearance setArrowBase:0];
            
            [popoverAppearance setInnerCornerRadius:0];
            [popoverAppearance setInnerShadowBlurRadius:0];
            [popoverAppearance setInnerShadowColor:[UIColor clearColor]];
            [popoverAppearance setInnerShadowOffset:CGSizeMake(0, 0)];
            
            [popoverAppearance setFillTopColor:[UIColor whiteColor]];
            [popoverAppearance setFillBottomColor:[UIColor whiteColor]];
            [popoverAppearance setOuterStrokeColor:[UIColor clearColor]];
            [popoverAppearance setInnerStrokeColor:[UIColor clearColor]];
            
            
            GRAlertViewController *contentViewController = [[[GRAlertViewController alloc] init] autorelease];
            
            contentViewController.contentSizeForViewInPopover = CGSizeMake(324, 175);
            
            [contentViewController view];
            
            contentViewController.messageLabel.text = NSLocalizedString(@"The account was \nsuccessfully added", @"The account was \nsuccessfully added");
            
            contentViewController.messageLabel.font = [UIFont fontWithName:@"Helvetica" size:18];
            
            [contentViewController.okButton setBackgroundImage:nil forState:UIControlStateNormal];
            contentViewController.okButton.frame = CGRectMake(0, contentViewController.okButton.frame.origin.y + 30, 50, 28);
            contentViewController.okButton.center = CGPointMake(contentViewController.view.bounds.size.width / 2, contentViewController.okButton.center.y);
            contentViewController.okButton.layer.borderWidth = 1.f;
            contentViewController.okButton.layer.borderColor = RGBCOLOR(190, 190, 190).CGColor;
            [contentViewController.okButton setTitleColor:RGBCOLOR(96, 96, 96) forState:UIControlStateNormal];
            contentViewController.okButton.titleLabel.font = [UIFont fontWithName:@"Helvetica" size:14];
            
            [contentViewController.okButton addTarget:self action:@selector(popoverButtonTapped:) forControlEvents:UIControlEventTouchUpInside];
            
            self.alertPopoverController = [[[WYPopoverController alloc] initWithContentViewController:contentViewController] autorelease];
            self.alertPopoverController.delegate = self;
            
            [self.alertPopoverController presentPopoverAsDialogAnimated:YES];
            
            break;
            
        default:
            break;
    }
}

- (void)incorrectUsernameButtonTapped:(UIButton*)button
{
    [self.alertPopoverController dismissPopoverAnimated:YES];
    self.alertPopoverController = nil;
}

- (IBAction)connectionButtonTapped:(UIButton*)button
{
    if (button == self.clearButton)
    {
        self.clearButton.selected = YES;
        self.startTLSButton.selected = NO;
        self.tlsButton.selected = NO;
    }
    else if (button == self.startTLSButton)
    {
        self.clearButton.selected = NO;
        self.startTLSButton.selected = YES;
        self.tlsButton.selected = NO;
    }
    else
    {
        self.clearButton.selected = NO;
        self.startTLSButton.selected = NO;
        self.tlsButton.selected = YES;
    }
}

- (void)popoverButtonTapped:(UIButton*)button
{
    [self.alertPopoverController dismissPopoverAnimated:YES];
    self.alertPopoverController = nil;
    
    if (IS_IPAD)
        [self.view removeFromSuperview];
    else
        [self.navigationController popViewControllerAnimated:YES];
}

- (void)mailActionFailed:(NSError *)error
{
    [[AppDelegate getInstance].window unBlockUI];
    NSNotificationCenter *notificationCenter = [NSNotificationCenter defaultCenter];
    [notificationCenter postNotificationName:@"newAccountCreated"
                                      object:nil
                                    userInfo:nil];
    
    WYPopoverBackgroundView *popoverAppearance = [WYPopoverBackgroundView appearance];
    
    [popoverAppearance setOverlayColor:[UIColor colorWithWhite:0.1f alpha:0.6f]];
    [popoverAppearance setOuterCornerRadius:0];
    [popoverAppearance setOuterShadowBlurRadius:0];
    [popoverAppearance setOuterShadowColor:[UIColor clearColor]];
    [popoverAppearance setOuterShadowOffset:CGSizeZero];
    
    [popoverAppearance setGlossShadowColor:[UIColor clearColor]];
    [popoverAppearance setGlossShadowOffset:CGSizeMake(0, 0)];
    
    [popoverAppearance setBorderWidth:1];
    [popoverAppearance setArrowHeight:0];
    [popoverAppearance setArrowBase:0];
    
    [popoverAppearance setInnerCornerRadius:0];
    [popoverAppearance setInnerShadowBlurRadius:0];
    [popoverAppearance setInnerShadowColor:[UIColor clearColor]];
    [popoverAppearance setInnerShadowOffset:CGSizeMake(0, 0)];
    
    [popoverAppearance setFillTopColor:[UIColor whiteColor]];
    [popoverAppearance setFillBottomColor:[UIColor whiteColor]];
    [popoverAppearance setOuterStrokeColor:[UIColor clearColor]];
    [popoverAppearance setInnerStrokeColor:[UIColor clearColor]];
    
    
    GRAlertViewController *contentViewController = [[[GRAlertViewController alloc] init] autorelease];
    
    contentViewController.contentSizeForViewInPopover = CGSizeMake(324, 175);
    
    [contentViewController view];
    
    contentViewController.messageLabel.text = NSLocalizedString(@"Some information  \n is incorrect. Please try again.", @"Some information \n is incorrect. Please try again.");
    
    contentViewController.messageLabel.font = [UIFont fontWithName:@"Helvetica" size:18];
    
    [contentViewController.okButton setBackgroundImage:nil forState:UIControlStateNormal];
    contentViewController.okButton.frame = CGRectMake(0, contentViewController.okButton.frame.origin.y + 30, 50, 28);
    contentViewController.okButton.center = CGPointMake(contentViewController.view.bounds.size.width / 2, contentViewController.okButton.center.y);
    contentViewController.okButton.layer.borderWidth = 1.f;
    contentViewController.okButton.layer.borderColor = RGBCOLOR(190, 190, 190).CGColor;
    [contentViewController.okButton setTitleColor:RGBCOLOR(96, 96, 96) forState:UIControlStateNormal];
    contentViewController.okButton.titleLabel.font = [UIFont fontWithName:@"Helvetica" size:14];
    
    [contentViewController.okButton addTarget:self action:@selector(incorrectUsernameButtonTapped:) forControlEvents:UIControlEventTouchUpInside];
    
    self.alertPopoverController = [[[WYPopoverController alloc] initWithContentViewController:contentViewController] autorelease];
    self.alertPopoverController.delegate = self;
    
    [self.alertPopoverController presentPopoverAsDialogAnimated:YES];
}

#pragma mark - Initialization

- (void)dealloc {
    [contentView release];
    [tAccountName release];
    [tUserName release];
    [tPassword release];
    [tImapUrl release];
    [tImapPort release];
    [tSmtpUrl release];
    [tSmtpPort release];
    [mailSettings release];
    
    self.clearButton = nil;
    self.startTLSButton = nil;
    self.tlsButton = nil;
    
    self.mailAccount = nil;
    
    [self.alertPopoverController dismissPopoverAnimated:YES];
    self.alertPopoverController = nil;
    
    [super dealloc];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    if (!IS_IPAD)
        [self willRotateToInterfaceOrientation:[UIApplication sharedApplication].statusBarOrientation duration:0];
}


- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [GRMailServiceManager shared].delegate = self;
    
    self.screenName = @"Mail Settings New Other Account Screen";
    
    if(self.mailAccount)
    {
        self.tAccountName.text = self.mailAccount.accountName;
        self.tUserName.text = self.mailAccount.userName;
        self.tPassword.text = self.mailAccount.password;
        self.tSmtpPort.text = [self.mailAccount.smtpPort stringValue];
        self.tSmtpUrl.text = self.mailAccount.smtpServer;
        self.tImapPort.text = [self.mailAccount.port stringValue];
        self.tImapUrl.text = self.mailAccount.server;
        
        if ([self.mailAccount.connectionType integerValue] == 0)
        {
            self.clearButton.selected = YES;
            self.startTLSButton.selected = NO;
            self.tlsButton.selected = NO;
        }
        else if ([self.mailAccount.connectionType integerValue] == 1)
        {
            self.clearButton.selected = NO;
            self.startTLSButton.selected = YES;
            self.tlsButton.selected = NO;
        }
        else
        {
            self.clearButton.selected = NO;
            self.startTLSButton.selected = NO;
            self.tlsButton.selected = YES;
        }
    }
    else
    {
        self.clearButton.selected = NO;
        self.startTLSButton.selected = NO;
        self.tlsButton.selected = YES;
    }
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
}

-(void)willRotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)duration
{
    if( UIInterfaceOrientationIsLandscape(toInterfaceOrientation) )
    {
        [[NSBundle mainBundle] loadNibNamed: [NSString stringWithFormat:@"%@", NSStringFromClass([self class])]
                                      owner: self
                                    options: nil];
        [self viewDidLoad];
    }
    else
    {
        [[NSBundle mainBundle] loadNibNamed: [NSString stringWithFormat:@"%@-portrait", NSStringFromClass([self class])]
                                      owner: self
                                    options: nil];
        [self viewDidLoad];
    }
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    if (IS_IPAD)
        return UIInterfaceOrientationIsLandscape(interfaceOrientation);
    else
        return (interfaceOrientation != UIInterfaceOrientationPortraitUpsideDown);
}

#pragma mark - Actions methods

- (BOOL)validateEmailAdress:(NSString*)candidate
{
	NSString *emailRegEx = @"[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}";
	NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegEx];
	return [emailTest evaluateWithObject:[candidate lowercaseString]];
}

- (BOOL)validateEmail
{
    if (![self validateEmailAdress:self.tUserName.text]) {
        [[AppDelegate getInstance] displayAlertWithMessage:@"Wrong username"];
        return NO;
    }
    return YES;
}

- (IBAction)okBtnTapped:(id)sender
{
    if (![self validateEmail])
    {
        return;
    }
    
    NSNumber *connectionType = nil;
    
    if (self.clearButton.isSelected)
    {
        connectionType = @(0);
    }
    else if (self.startTLSButton.isSelected)
    {
        connectionType = @(1);
    }
    else
    {
        connectionType = @(2);
    }
    
    NSArray *keys = [NSArray arrayWithObjects:@"type", @"server", @"port", @"smtpServer", @"smtpPort", @"userName", @"password", @"accountName", @"connectionType", nil];
    NSArray *objects = [NSArray arrayWithObjects: @"other", self.tImapUrl.text, [NSNumber numberWithInt:[self.tImapPort.text intValue]], self.tSmtpUrl.text, [NSNumber numberWithInt:[self.tSmtpPort.text intValue]], self.tUserName.text, self.tPassword.text, self.tAccountName.text, connectionType, nil];
   
    mailSettings = [[NSMutableDictionary alloc] initWithObjects:objects forKeys:keys];
    [GRMailServiceManager shared].delegate = self;
    [[GRMailServiceManager shared] mailActionVerification:mailSettings];
    [[AppDelegate getInstance].window blockUIWithMessage:NSLocalizedString(@"Validating account", @"Validating account") isBlack:YES];
    [currentField resignFirstResponder];
}

- (IBAction)cancelBtnTapped:(id)sender
{
    if (IS_IPAD)
        [self.view removeFromSuperview];
    else
        [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark - UITextFieldDelegate

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    [textField resignFirstResponder];
    return YES;
}

- (void)textFieldDidEndEditing:(UITextField *)textField {
    [self restoreView];
}

- (void)textFieldDidBeginEditing:(UITextField *)textField {
    currentField = textField;
    if (IS_IPAD) {
        switch (textField.tag) {
            case 11:
                animateY = -24.0f;
                break;
            case 12:
                animateY = -80.0f;
                break;
                
            default:
                animateY = self.contentView.frame.origin.y;
                break;
        }
    } else {
        switch (textField.tag) {
            case 11:
                animateY = -2.0f;
                break;
            case 12:
                animateY = -36.0f;
                break;
            case 13:
                animateY = -72.0f;
                break;
            case 14:
                animateY = -106.0f;
                break;
                
            default:
                animateY = self.contentView.frame.origin.y;
                break;
        }
    }
    [self animateViewUp];
}

- (void)animateViewUp {
    originY = self.contentView.frame.origin.y;
    CGRect frame = self.contentView.frame;
    frame.origin.y = animateY;
    [UIView beginAnimations:nil context:NULL];
	[UIView setAnimationDuration:0.3];
	[UIView setAnimationDelegate:self];
	self.contentView.frame = frame;
	[UIView commitAnimations];
}

- (void)restoreView {
    CGRect frame = self.contentView.frame;
    frame.origin.y = originY;
    [UIView beginAnimations:nil context:NULL];
	[UIView setAnimationDuration:0.3];
	[UIView setAnimationDelegate:self];
	self.contentView.frame = frame;
	[UIView commitAnimations];
}

#pragma mark - WYPopoverControllerDelegate's methods

- (BOOL)popoverControllerShouldDismissPopover:(WYPopoverController *)popoverController
{
    return YES;
}

- (void)popoverControllerDidDismissPopover:(WYPopoverController *)popoverController
{
    self.alertPopoverController = nil;
}

@end
