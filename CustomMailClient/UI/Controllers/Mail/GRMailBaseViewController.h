//
//  GRMailBaseViewController.h
//  GrannyApp
//
//  Created by Ievgen Pavliuk on 7/9/13.
//
//

#import <UIKit/UIKit.h>
#import "GRMailServiceManager.h"

@interface GRMailBaseViewController : GAITrackedViewController <GRMailServiceDelegate> {
    
}

@end
