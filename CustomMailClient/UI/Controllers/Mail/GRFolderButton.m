//
//  GRFolderButton.m
//  GrannyApp
//
//  Created by admin on 04/12/13.
//
//

#import "GRFolderButton.h"

@implementation GRFolderButton

- (void)awakeFromNib
{
    [super awakeFromNib];
    
    self.imageView.contentMode = UIViewContentModeCenter;
    self.titleLabel.numberOfLines = 2;
    self.layer.shadowColor = [UIColor blackColor].CGColor;
    self.layer.shadowOpacity = 0.4f;
    self.layer.shadowOffset = CGSizeMake(3, 3);
    self.layer.shadowRadius = 5.f;
    self.titleLabel.textAlignment = NSTextAlignmentCenter;
    self.titleLabel.baselineAdjustment = UIBaselineAdjustmentAlignCenters;
    self.layer.cornerRadius = 12.f;
    self.layer.borderColor = RGBCOLOR(0, 113, 186).CGColor;
    self.layer.borderWidth = 1.f;
    self.titleLabel.font = [UIFont fontWithName:@"Helvetica-Bold" size:30];
    
    UIColor *bkgColorNormal = [UIColor whiteColor];
    [self setBackgroundImage:[[self imageWithColor:bkgColorNormal borderColor:RGBCOLOR(0, 113, 186)] resizableImageWithCapInsets:UIEdgeInsetsMake(15, 15, 15, 15)] forState:UIControlStateNormal];
    
    UIColor *bkgColorHighlighted = rgb(0x7fb8dc);
    [self setBackgroundImage:[[self imageWithColor:bkgColorHighlighted borderColor:RGBCOLOR(0, 113, 186)] resizableImageWithCapInsets:UIEdgeInsetsMake(15, 15, 15, 15)] forState:UIControlStateSelected];
    
    [self setBackgroundImage:[[self imageWithColor:bkgColorHighlighted borderColor:RGBCOLOR(0, 113, 186)] resizableImageWithCapInsets:UIEdgeInsetsMake(15, 15, 15, 15)] forState:UIControlStateHighlighted];
}

- (void)layoutSubviews
{
    [super layoutSubviews];
    
    if ([self.titleLabel.text length])
    {
        self.titleLabel.frame = UIEdgeInsetsInsetRect(self.bounds, UIEdgeInsetsMake(90, 10, 5, 10));
        
        self.imageView.frame = UIEdgeInsetsInsetRect(self.bounds, UIEdgeInsetsMake(5, 10, 30, 10));
    }
}

- (UIImage *)imageWithColor:(UIColor *)color borderColor:(UIColor*)borderColor
{
    CGRect rect = CGRectMake(0.0f, 0.0f, 30.0f, 30.0f);
    UIGraphicsBeginImageContext(rect.size);
    CGContextRef context = UIGraphicsGetCurrentContext();
    
    CGContextSetFillColorWithColor(context, [color CGColor]);
    CGContextSetStrokeColorWithColor(context, borderColor.CGColor);
    CGContextSetLineWidth(context, 1);
    
    UIBezierPath *path = [UIBezierPath bezierPathWithRoundedRect:rect cornerRadius:12.f];
    
    CGContextAddPath(context, path.CGPath);
    CGContextClosePath(context);
    
    CGContextFillPath(context);
    CGContextStrokePath(context);
    
    UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    return image;
}

@end
