//
//  GRMailSetupController.m
//  GrannyApp
//
//  Created by Ievgen Pavliuk on 4/12/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "GRMailSetupController.h"
#import "GRMailInboxListController.h"
#import "GRMailAccountManager.h"
#import "TMailAccount.h"
#import "DataManager.h"
#import "SendingMessage.h"
#import "DBMailAccount.h"

@interface GRMailSetupController ()
{
    int selectedIndex;
}
@end

@implementation GRMailSetupController
@synthesize contentTable;
@synthesize fromContact;

#pragma mark - Update data

- (void)updateData
{
    if (tableData) {
        [tableData retain];
        tableData = nil;
    }
    NSArray *accounts = [[GRMailAccountManager manager] getAccounts];
    tableData = [[NSMutableArray alloc] initWithArray:accounts];
    [self.contentTable reloadData];
}


#pragma mark - Initialization

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)dealloc {
    [contentTable release];
    [tableData release];
    [fromContact release];
    [super dealloc];
}

- (BOOL)prefersStatusBarHidden
{
    return YES;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self.navigationController setNavigationBarHidden:YES];
    self.contentTable.backgroundColor = [UIColor clearColor];
    
    self.screenName = @"Mail Setup Screen";
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self updateData];
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return UIInterfaceOrientationIsLandscape(interfaceOrientation);
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [tableData count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:nil];
    if (cell == nil) {
        cell = [[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier] autorelease];
        
    }
    
    cell.contentView.backgroundColor = [UIColor clearColor];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;    
    
    CGFloat cellWidth = self.contentTable.frame.size.width;
    CGFloat cellHeight = [self tableView:tableView heightForRowAtIndexPath:indexPath];
    UIImageView *separator = [[[UIImageView alloc] init] autorelease];
    separator.frame = CGRectMake(0, cellHeight-1, cellWidth, 1);
    separator.backgroundColor = RGBCOLOR(179, 179, 179);
    [cell.contentView addSubview:separator];
    
    TMailAccount *mailAccount = [tableData objectAtIndex:[indexPath row]];
    UIImageView *iconView = nil;
    if ([mailAccount.type isEqualToString:@"gmail"]) {
        iconView = [[[UIImageView alloc] initWithImage:[UIImage imageNamed:@"Settings_MailIconGmail.png"]] autorelease];
    }
    if ([mailAccount.type isEqualToString:@"other"]) {
        iconView = [[[UIImageView alloc] initWithImage:[UIImage imageNamed:@"Settings_MailIconOther.png"]] autorelease];
    }
    if (!IS_IPAD) {
        iconView.frame = CGRectMake(0, 0, iconView.frame.size.width/2, iconView.frame.size.height/2);
    }

    iconView.frame = CGRectMake(30, cellHeight/2 - iconView.frame.size.height/2, iconView.frame.size.width, iconView.frame.size.height);
    [cell.contentView addSubview:iconView];
    
    UILabel *lTitle = [[[UILabel alloc] initWithFrame:CGRectMake(110, 0, cellWidth-110, cellHeight)] autorelease];
    lTitle.backgroundColor = [UIColor clearColor];
    lTitle.font = [UIFont fontWithName:@"Helvetica-Bold" size:22];
    if (!IS_IPAD) {
        lTitle.font = [UIFont fontWithName:@"Helvetica-Bold" size:14];
        lTitle.frame = CGRectMake(60, 0, cellWidth-40, lTitle.frame.size.height);
    }
    [cell.contentView addSubview:lTitle];
    lTitle.text = mailAccount.accountName;
    
    UIImageView *cellArrow = [[[UIImageView alloc] initWithImage:[UIImage imageNamed:@"Settings_CellArrow.png"]] autorelease];
    cellArrow.frame  = CGRectMake(cellWidth - cellArrow.frame.size.width - 20, cellHeight/2 - cellArrow.frame.size.height/2, cellArrow.frame.size.width, cellArrow.frame.size.height);
    [cell.contentView addSubview:cellArrow];
    
    
    
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    CGFloat height = 0.0f;
    if (IS_IPAD) {
        if ([indexPath row] == 0) {
            height = 98;
        } else {
            height = 98;
        }
    } else {
        if ([indexPath row] == 0) {
            height = 50;
        } else {
            height = 49;
        }
    }
    return height;
}


#pragma mark - Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    selectedIndex = [indexPath row];
    TMailAccount *account = [tableData objectAtIndex:[indexPath row]];
    GRMailInboxListController *inboxController = [[GRMailInboxListController alloc] init];
    inboxController.mailAccount = account;
    if (self.fromContact) {
        inboxController.fromContact = self.fromContact;
    }
    [self.navigationController setNavigationBarHidden:YES];
    [self.navigationController pushViewController:inboxController animated:YES];
    [inboxController release];

    
    NSMutableArray *sendingMessagesArray = [NSMutableArray arrayWithArray:[[DataManager sharedInstance] allRowsForEntity:@"SendingMessage"]];
    BOOL showAlert = NO;
    if (sendingMessagesArray.count)
    {
        SendingMessage *message = [sendingMessagesArray objectAtIndex:0];
        DBMailAccount *mailAccount = [NSKeyedUnarchiver unarchiveObjectWithData:message.account];
        if (![mailAccount.accountName isEqual:account.accountName])
            showAlert = YES;
    }
    
    if (showAlert)
    {
        GRCommonAlertView *alert = [[GRCommonAlertView alloc] initCommonChooseAlert:NSLocalizedString(@"Stop sending current messages?", @"Stop sending current messages?") inView:self.view];
        
        alert.delegate = self;
    }
}

#pragma mark - GRCommonAlertDelegate

- (void)commonAlert:(id)alertView buttonWithIndex:(NSInteger)index
{
    if (index == 0)
    {
        TMailAccount *account = [tableData objectAtIndex:selectedIndex];

        GRMailInboxListController *inboxController = [[GRMailInboxListController alloc] init];
        inboxController.mailAccount = account;
        [self.navigationController pushViewController:inboxController animated:YES];
        [inboxController release];
    }
    else
    {
        
    }
}
#pragma mark - Actions

- (IBAction)backBtnTapped:(id)sender {
    [self.navigationController popToRootViewControllerAnimated:YES];
}

@end
