//
//  GRMailInboxListController.m
//  GrannyApp
//
//  Created by Ievgen Pavliuk on 4/13/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "GRMailInboxListController.h"
#import "GRMailInboxCell.h"
#import "GRResource.h"
#import "TMailMessage.h"
#import "GRMailDataManager.h"
//#import "GRNewMessageController.h"
#import "TMailSentMessage.h"
#import "TMailDraftMessage.h"
#import "GRPeopleManager.h"
#import "TMessageAttachment.h"
//#import "GRMessageListController.h"
#import "GRMailAccountManager.h"
#import "DataManager.h"
#import "GRMessageReadController.h"
#import "GRSMTPMessage.h"
#import "GRFolderButton.h"

#define FETCH_LIMIT 10

@interface GRMailInboxListController ()
{
    BOOL isFirstMessageSended;
    int totalSendedMessages;
    int rowIndex;
    BOOL isFetchingMessage;
}
@end

@implementation GRMailInboxListController

@synthesize contentTable;
@synthesize loadingView;
@synthesize mailAccount;
@synthesize lLoadingInfo;
@synthesize composeBtn;
@synthesize indicatorView;
@synthesize sendingMessageLabel;
@synthesize inboxBtn, draftBtn, sentBtn, trashBtn;
@synthesize fromContact;


#pragma mark - update data

- (void)requestMail:(BOOL)loadMore
{
    
//    if ([GRPeopleManager sharedManager].isBusy) {
//        [self listenContacts];
//        return;
//    }
    if (self.indicatorView.hidden)
        self.indicatorView.hidden = NO;
    
    [GRMailServiceManager shared].delegate = self;
    [GRMailServiceManager shared].currentFolder = folderIndex;
    if (fetchLimit == 0)
    {
        fetchLimit = FETCH_LIMIT;
    }

    self.lLoadingInfo.text = NSLocalizedString(@"Checking mail", @"Checking mail");
    [GRMailServiceManager shared].delegate = self;
    if (!self.fromContact) {
        [[GRMailServiceManager shared] mailActionGetMailsFromFolder:folderIndex isMore:loadMore];
    } else {
        self.indicatorView.hidden = YES;
    }
    if (!loadMore) {
        [self reloadMailData];
    }
}

- (void)checkPendingMessages {
    if ([[YLReachability reachabilityForInternetConnection] isReachable]) {
        if (![GRPeopleManager sharedManager].isBusy) {
            if (![GRMailServiceManager shared].currentAccount) {
                [GRMailServiceManager shared].currentAccount = self.mailAccount;
            }
            NSArray *pendingMessages = [[GRMailDataManager sharedManager] getAllPendingMessagesForAccount:self.mailAccount.accountName];
            for (TPendingMessage *pm in pendingMessages) {
                GRSMTPMessage *sMessage = [[[GRSMTPMessage alloc] init] autorelease];
                sMessage.uid = [pm.uid integerValue];
                sMessage.subject = pm.subject;
                sMessage.plainText = pm.textPlain;
                sMessage.htmlText = pm.textHTML;
                sMessage.tos = [NSKeyedUnarchiver unarchiveObjectWithData:pm.tos];
                sMessage.attachments = [NSKeyedUnarchiver unarchiveObjectWithData:pm.attachments];
                sMessage.senderMail = pm.senderMail;
                [[GRMailServiceManager shared] mailActionSentMessage:sMessage];
            }
            [self performSelector:@selector(updateSendingInfoLabel) withObject:nil afterDelay:0.3f];
        }
    }
}

- (void)reloadMailData {
    if (tableData) {
        [tableData removeAllObjects];
        [tableData release];
        tableData = nil;
    }
    NSArray *messages = nil;
    BOOL isFromContact = NO;
    if (self.fromContact) {
        NSString *contactName = fromContact.selectedEmail ? fromContact.selectedEmail:fromContact.email;

        messages = [[GRMailDataManager sharedManager] getAllInboxMessagesForEmail:contactName accountName:self.mailAccount.accountName];
    } else {
        switch (folderIndex)
        {
            case MAIL_FOLDER_INBOX:
                if ([GRMailDataManager sharedManager].fromContact) {
                    isFromContact = YES;
                }
                
                messages = [[GRMailDataManager sharedManager] getAllMessagesForAccountName:self.mailAccount.accountName];
                break;
            case MAIL_FOLDER_DRAFTS:
                messages = [[GRMailDataManager sharedManager] getAllDraftMessagesForAccountName:self.mailAccount.accountName];
                break;
            case MAIL_FOLDER_SENT:
                messages = [[GRMailDataManager sharedManager] getAllSentMessagesForAccountName:self.mailAccount.accountName];
                break;
            case MAIL_FOLDER_TRASH:
                messages = [[GRMailDataManager sharedManager] getAllTrashMessagesForAccountName:self.mailAccount.accountName];
                break;
        }
    }
    tableData = [[NSMutableArray alloc] initWithArray:messages];
    NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"date" ascending:NO];
    [tableData sortUsingDescriptors:[NSArray arrayWithObject:sortDescriptor]];
    [sortDescriptor release];
    [self.contentTable reloadData];
    [fromContact release];
    fromContact = nil;
}

- (void)listenContacts {
   timer = [NSTimer scheduledTimerWithTimeInterval:0.5f target:self selector:@selector(listen) userInfo:nil repeats:YES];
}

- (void)listen {
    if (![GRPeopleManager sharedManager].isBusy) {
        [timer invalidate];
        timer = nil;
        [self requestMail:NO];
        [self checkPendingMessages];
    }
}

#pragma mark - GRFolderViewDelegate

- (void)mailFolderSelectedWithIndex:(NSInteger)index
{
    if ([[YLReachability reachabilityForInternetConnection] isReachable])
    {        
        isMoreMessages = NO;
        folderIndex = index;
        [self requestMail:NO];
    }
    else
    {
        GRCommonAlertView *alert = [[[GRCommonAlertView alloc] initCommonAlert:NSLocalizedString(@"No Internet connection.", @"No Internet connection.") inView:self.view] autorelease];
        alert.delegate = nil;
        [self reloadMailData];

    }
}

#pragma mark - GRMailServiceDelegate

- (void)mailActionFailed:(NSError *)error {
    [refreshControl endRefreshing];
    NSString *descr =[error localizedDescription];
    if ([descr rangeOfString:@"A stable"].location != NSNotFound) {
        descr = NSLocalizedString(@"No Internet connection.", @"No Internet connection.");
    }
    GRCommonAlertView *alert = [[[GRCommonAlertView alloc] initCommonAlert:descr inView:self.view] autorelease];
    alert.delegate = nil;
    [self performSelector:@selector(updateSendingInfoLabel) withObject:nil afterDelay:1.0f];
}

- (void)mailActionSucceed:(NSInteger)operationCode {
    switch (operationCode) {
        case MAIL_INBOX_OP:
            if (folderIndex == MAIL_FOLDER_INBOX) {
                isMoreMessages = YES;
                [self reloadMailData];
                [refreshControl endRefreshing];
            }
            break;
        case MAIL_DRAFTS_OP:
            if (folderIndex == MAIL_FOLDER_DRAFTS) {
                isMoreMessages = YES;
                [self reloadMailData];
                [refreshControl endRefreshing];
            }
            break;
        case MAIL_SENT_OP:
            if (folderIndex == MAIL_FOLDER_SENT) {
                isMoreMessages = YES;
                [self reloadMailData];
                [refreshControl endRefreshing];
            }
            break;
        case MAIL_TRASH_OP:
            if (folderIndex == MAIL_FOLDER_TRASH) {
                isMoreMessages = YES;
                [self reloadMailData];
                [refreshControl endRefreshing];
            }
            break;
        case MAIL_ALL_OPERATIONS_DONE:
            [self allOperationsCompleted];
            break;
        case MAIL_SEND_OP:
            [self performSelector:@selector(updateSendingInfoLabel) withObject:nil afterDelay:1.0f];
            break;
        case MAIL_MOVE_OP:
            [self reloadMailData];
            break;
        case MAIL_APPEND_OP:
            if (folderIndex == MAIL_FOLDER_DRAFTS) {
                [self requestMail:NO];
            }
            break;
            
        default:
            break;
    }
}


#pragma mark - initialization

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
    self.sendingMessageLabel = nil;
    self.contentTable = nil;
    [tableData release];
    self.loadingView = nil;
    self.mailAccount = nil;
    self.lLoadingInfo = nil;
    self.composeBtn = nil;
//    [messageReadController release];
//    [newMessageController release];
    
    self.inboxBtn = nil;
    self.draftBtn = nil;
    self.sentBtn = nil;
    self.trashBtn = nil;
    
    self.indicatorView = nil;
    self.fromContact = nil;
    [[GRMailServiceManager shared] resetSession];
    [_headerView release];
    [super dealloc];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.screenName = @"Mail Inbox List Screen";
    
    [self.contentTable setBackgroundColor:[UIColor clearColor]];

    [[GRMailServiceManager shared] setupMailAccount:self.mailAccount];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(networkStatusRestored:)
                                                 name:@"networkStatusRestored"
                                               object:nil];
    
    refreshControl = [[UIRefreshControl alloc] init];
    [refreshControl addTarget:self action:@selector(refreshControlValueChanged:) forControlEvents:UIControlEventValueChanged];
    [self.contentTable  addSubview:refreshControl];
    
    self.headerView.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"emails_nav_bkg"]];
}



- (void)refreshControlValueChanged:(UIRefreshControl*)refreshControl
{
    [self requestMail:NO];
}


- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    [self updateSendingInfoLabel];
    
    navBarShouldHide = NO;
    [self willRotateToInterfaceOrientation:[UIApplication sharedApplication].statusBarOrientation duration:0];
    [self.navigationController setNavigationBarHidden:YES];
    
    if (!IS_IPAD)
    {
        if (UIInterfaceOrientationIsPortrait([UIApplication sharedApplication].statusBarOrientation))
        {
            [inboxBtn setBackgroundImage:[UIImage imageNamed:@"Mail_Inbox_Icon_Portrait.png"] forState:UIControlStateNormal];
            [draftBtn setBackgroundImage:[UIImage imageNamed:@"Mail_Draft_Icon_Inactive_Portrait.png"] forState:UIControlStateNormal];
            [sentBtn setBackgroundImage:[UIImage imageNamed:@"Mail_Sent_Icon_Inactive_Portrait.png"] forState:UIControlStateNormal];
            [trashBtn setBackgroundImage:[UIImage imageNamed:@"Mail_Trash_Icon_Inactive_Portrait.png"] forState:UIControlStateNormal];
        }
        else
        {
            [inboxBtn setBackgroundImage:[UIImage imageNamed:@"Mail_Inbox_Icon.png"] forState:UIControlStateNormal];
            [draftBtn setBackgroundImage:[UIImage imageNamed:@"Mail_Draft_Icon_Inactive.png"] forState:UIControlStateNormal];
            [sentBtn setBackgroundImage:[UIImage imageNamed:@"Mail_Sent_Icon_Inactive.png"] forState:UIControlStateNormal];
            [trashBtn setBackgroundImage:[UIImage imageNamed:@"Mail_Trash_Icon_Inactive.png"] forState:UIControlStateNormal];
        }
    }
    
    [self highlightButton:inboxBtn];
        
    if ([[YLReachability reachabilityForInternetConnection] isReachable])
    {
        [GRMailServiceManager shared].delegate = self;
        if (!isLoaded)
        {
            [self requestMail:NO];
            [self updateSendingInfoLabel];
            isLoaded = YES;
            fetchLimit = 0;
        }
        [self checkPendingMessages];
    }
    else
    {
        [self reloadMailData];
        GRCommonAlertView *alert = [[[GRCommonAlertView alloc] initCommonAlert:NSLocalizedString(@"No Internet connection.", @"No Internet connection.") inView:self.view] autorelease];
        alert.delegate = nil;
    }
    self.loadingView.hidden = YES;
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    [GRMailServiceManager shared].delegate = nil;
    [GRMailServiceManager shared].currentFolder = 0;
}

-(void)willRotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)duration
{
    if (!IS_IPAD)
    {
        if( UIInterfaceOrientationIsLandscape(toInterfaceOrientation) )
        {
            [[NSBundle mainBundle] loadNibNamed: [NSString stringWithFormat:@"%@", NSStringFromClass([self class])]
                                          owner: self
                                        options: nil];
            [self viewDidLoad];
        }
        else
        {
            [[NSBundle mainBundle] loadNibNamed: [NSString stringWithFormat:@"%@-portrait", NSStringFromClass([self class])]
                                          owner: self
                                        options: nil];
            [self viewDidLoad];
        }
    }
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    if (IS_IPAD)
        return UIInterfaceOrientationIsLandscape(interfaceOrientation);
    else
        return (interfaceOrientation != UIInterfaceOrientationPortraitUpsideDown);
}

#pragma mark - Table view data source
- (void) reloadTableDataWithTypeFolder:(int)typeFolder
{
    if (tableData) {
        [tableData release];
        tableData = nil;
    }
    
    NSArray *messages = nil;
    switch (typeFolder)
    {
        case MAIL_FOLDER_INBOX:
        {
//            messages = [[GRMailDataManager sharedManager] getAllMessagesForAccountName:self.mailAccount.accountName];
            
            messages = [GRMailDataManager sharedManager].listReadMessages;
            
            tableData = [[NSMutableArray alloc] initWithArray:messages];
            NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"sentDateGMT" ascending:FALSE];
            [tableData sortUsingDescriptors:[NSArray arrayWithObject:sortDescriptor]];
            [sortDescriptor release];
            break;
        }
        case MAIL_FOLDER_DRAFTS:
        {
            messages = [GRMailDataManager sharedManager].listDraftMessages;
            
            tableData = [[NSMutableArray alloc] initWithArray:messages];
            NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"sentDateGMT" ascending:FALSE];
            [tableData sortUsingDescriptors:[NSArray arrayWithObject:sortDescriptor]];
            [sortDescriptor release];
            break;
        }
        case MAIL_FOLDER_SENT:
        {
            messages = [GRMailDataManager sharedManager].listSentMessages;
            
            tableData = [[NSMutableArray alloc] initWithArray:messages];
            NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"sentDateGMT" ascending:FALSE];
            [tableData sortUsingDescriptors:[NSArray arrayWithObject:sortDescriptor]];
            [sortDescriptor release];
            break;
        }
        case MAIL_FOLDER_TRASH:
        {
            messages = [GRMailDataManager sharedManager].listTrashMessages;
            
            tableData = [[NSMutableArray alloc] initWithArray:messages];
            NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"sentDateGMT" ascending:FALSE];
            [tableData sortUsingDescriptors:[NSArray arrayWithObject:sortDescriptor]];
            [sortDescriptor release];
            break;
        }
        default:
            break;
    }
    
    [self.contentTable reloadData];
}
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (isMoreMessages) {
        return [tableData count] + 1;
    } else {
        return [tableData count];
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *nibName = @"GRMailInboxCell";
    if (IS_IPAD) {
        nibName = @"GRMailInboxCell_iPad";
    }
    
    static NSString *CellIdentifier = @"GRMailInboxCell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:nil];
    if (cell == nil) {
        cell = [GRResource tableCellFromNib:nibName owner:contentTable cellId:CellIdentifier];
    }
    if (isMoreMessages && [indexPath row] == [tableData count] && ![GRMailDataManager sharedManager].fromContact) {
        [(GRMailInboxCell *)cell updateMore];
    }
    else
    {
        id message = [tableData objectAtIndex:[indexPath row]];
        switch (folderIndex)
        {
            case MAIL_FOLDER_INBOX:
                [(GRMailInboxCell *)cell update:message];
                break;
            case MAIL_FOLDER_DRAFTS:
                [(GRMailInboxCell *)cell updateForDraft:message];
                break;
            case MAIL_FOLDER_SENT:
                [(GRMailInboxCell *)cell updateForSent:message];
                break;
            case MAIL_FOLDER_TRASH:
                [(GRMailInboxCell *)cell updateForTrash:message];
                break;
                
            default:
                break;
        }
    }
    
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (IS_IPAD) {
        return 108.0f;
    } else {
        return 62.0f;
    }
}


#pragma mark - Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (isFetchingMessage)
        return;
    
    if (isMoreMessages && [indexPath row] == [tableData count]) {
        isMoreMessages = NO;
        [self.contentTable deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationFade];
        [self performSelector:@selector(startRequestMailForMore) withObject:nil afterDelay:1.0f];
    } else {
        TMailMessageBase *messageModel = [tableData objectAtIndex:[indexPath row]];
        
        if (folderIndex == MAIL_FOLDER_DRAFTS) {
            
            TMailDraftMessage *draftModel = [(TMailDraftMessage *)messageModel retain];
            NSMutableArray *ccContacts = [[GRMailDataManager sharedManager] getAllCCContactsForDraftMessage:draftModel];;
            NSMutableArray *toContacts = [[GRMailDataManager sharedManager] getAllToContactsForDraftMessage:draftModel];

            if (newMessageController)
            {
                [newMessageController release];
                newMessageController = nil;
            }
            
            newMessageController = [[GRNewMessageController alloc] init];
            
            newMessageController.selectedToContacts = toContacts;
            newMessageController.selectedBccContacts = ccContacts;

            newMessageController.delegate = self;
            newMessageController.operationIndex = 2;
            newMessageController.folderIndex = folderIndex;
            newMessageController.messageModel = draftModel;
            newMessageController.tSubject.text = draftModel.subject;
            if (IS_IPAD)
                [self.view addSubview:newMessageController.view];
            else
                [self.navigationController pushViewController:newMessageController animated:YES];
            [newMessageController reloadSubviews];
            [draftModel release];

        }
        else
        {
            GRMessageReadController *messageReadController = [[GRMessageReadController alloc] init];
            messageReadController.messageModel = messageModel;
            messageReadController.masterController = self;
            messageReadController.folderIndex = folderIndex;
            
            if (IS_IPAD)
                [self.view addSubview:messageReadController.view];
            else
                [self.navigationController pushViewController:messageReadController animated:YES];
        }

    }
}

- (void)startRequestMailForMore {
    [self requestMail:YES];
}


#pragma mark - Action methods

- (IBAction)onSendMsgController:(id)sender
{
    if ([self.mailAccount.type isEqualToString:@"pop3"])
    {
        [[AppDelegate getInstance].window displayInfoMessageWithString:NSLocalizedString(@"For sending mail\nuse IMAP account.", @"For sending mail\nuse IMAP account.")];
        return;
    }
    
    if (newMessageController)
    {
        [newMessageController release];
        newMessageController = nil;
    }
    
    newMessageController = [[GRNewMessageController alloc] init];
    newMessageController.delegate = self;
    if (IS_IPAD)
        [self.view addSubview:newMessageController.view];
    else
        [self.navigationController pushViewController:newMessageController animated:YES];
}

- (IBAction)backBtnTapped:(id)sender
{
    navBarShouldHide = YES;
    //[[GRMailServiceManager shared] resetSession];
    [self.navigationController popToRootViewControllerAnimated:YES];
}

- (IBAction)inboxBtnTapped:(id)sender
{
    [self mailFolderSelectedWithIndex:0];
    [self highlightButton:sender];
}
- (IBAction)draftBtnTapped:(id)sender
{
    [self mailFolderSelectedWithIndex:1];
    [self highlightButton:sender];
}
- (IBAction)sentBtnTapped:(id)sender
{
    [self mailFolderSelectedWithIndex:2];
    [self highlightButton:sender];
}
- (IBAction)trashBtnTapped:(id)sender
{
    [self mailFolderSelectedWithIndex:3];
    [self highlightButton:sender];
}

- (void)highlightButton:(UIButton *)button {
    inboxBtn.selected = NO;
    draftBtn.selected = NO;
    sentBtn.selected = NO;
    trashBtn.selected = NO;
    
    button.selected = YES;
}

#pragma mark - Notifications actions

- (void)networkStatusRestored:(NSNotification *)notification {
    [self checkPendingMessages];
}

#pragma mark - GRMessageReadController delegate

- (void)readControllerClosed
{
    isFetchingMessage = NO;
    isMoreMessages = YES;
    [self.contentTable reloadData];
}

#pragma mark - UI Helpers

- (void)updateSendingInfoLabel {
    NSInteger sendingCount = [[GRMailServiceManager shared] getSendingOperationsCount];
    if (sendingCount > 0) {
        self.sendingMessageLabel.hidden = NO;
        self.sendingMessageLabel.text = [NSString stringWithFormat:NSLocalizedString(@"sending message 1/%d", @"sending message 1/%d"),sendingCount];
    } else {
        self.sendingMessageLabel.hidden = YES;
    }
}

- (void)callUpdateInfoLabel {
    self.lLoadingInfo.text = [NSString stringWithFormat:NSLocalizedString(@"Download message %i from %i", @"Download message %i from %i"), messageNextIndex, messagesTotal];
}

- (void)onModalDone {
    [GRMailServiceManager shared].delegate = self;
    [self updateSendingInfoLabel];
    if (folderIndex != MAIL_FOLDER_INBOX) {
        [self requestMail:NO];
    }
    [self checkPendingMessages];
    [self reloadMailData];
}

- (void)allOperationsCompleted {
    self.indicatorView.hidden = YES;
}

@end
