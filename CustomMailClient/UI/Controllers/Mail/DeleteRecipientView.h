//
//  DeleteRecipientView.h
//  GrannyApp
//
//  Created by Eugene Pavluk on 1/30/14.
//
//

#import <UIKit/UIKit.h>

@interface DeleteRecipientView : UIView

@property (nonatomic, assign) IBOutlet UIButton *okButton;
@property (nonatomic, assign) IBOutlet UIButton *cancelButton;
@property (nonatomic, assign) IBOutlet UILabel *titleLabel;

@end
