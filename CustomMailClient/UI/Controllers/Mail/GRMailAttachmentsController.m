//
//  GRMailAttachmentsController.m
//  GrannyApp
//
//  Created by Ievgen Pavliuk on 6/1/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "GRMailAttachmentsController.h"
#import "GRResource.h"
#import "GRMailAttachemntCell.h"

@interface GRMailAttachmentsController ()

@end

@implementation GRMailAttachmentsController
@synthesize contentTable;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)dealloc {
    [contentTable release];
    [super dealloc];
}

- (BOOL)prefersStatusBarHidden
{
    return YES;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self.contentTable setBackgroundColor:[UIColor clearColor]];
    
    self.screenName = @"Mail Attachments Screen";
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return UIInterfaceOrientationIsLandscape(interfaceOrientation);
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 20;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *nibName = @"GRMailAttachemntCell";
    
    if (IS_IPAD) 
    {
        nibName = @"GRMailAttachemntCell_iPad";
    }
    
    static NSString *CellIdentifier = @"GRMailAttachemntCell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:nil];
    
    if (cell == nil) 
    {
        cell = [GRResource tableCellFromNib:nibName owner:contentTable cellId:CellIdentifier];
    }
    
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath 
{
    if (IS_IPAD) 
    {
        return 108.0f;
    } 
    else 
    {
        return 64.0f;
    }
}


#pragma mark - Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
}


@end
