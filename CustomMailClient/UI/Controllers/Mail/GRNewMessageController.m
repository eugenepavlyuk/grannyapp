//
//  GRNewMessageController.m
//  GrannyApp
//
//  Created by Ievgen Pavliuk on 7/11/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "GRNewMessageController.h"
#import "GRResource.h"
#import "GRMailDataManager.h"
#import "TMessageAttachment.h"
#import "MFDocumentManager.h"
#import "GRAttachmentPdfController.h"
#import "GRMailAttachmentsController.h"
#import "GRResource.h"
#import "GRMailAttachmentView.h"
#import "GRPeopleManager.h"
#import "GRMailInboxListController.h"
#import "GRMailAccountManager.h"
#import "SendingMessage.h"
#import "DataManager.h"
#import "DBMailAccount.h"
#import "AlertView.h"
#import <AudioToolbox/AudioToolbox.h>
#import "GRMailAttachemntCell.h"
#import "GRMailRecipientView.h"
#import "GRMailAddAttachmentView.h"
#import "TMailAccount.h"
#import "GRMailServiceManager.h"
#import "GRClickableLabel.h"
#import "TMailMessage.h"
#import "GRCommonAlertView.h"
#import "WYPopoverController.h"
#import "DeleteRecipientView.h"

#define ATTACHMENT_HEIGHT 72
#define ATTACHMENT_HEIGHT_iPHONE 20

#define KEYBOARD_HEIGHT_LANDSCAPE_IPAD 352
#define KEYBOARD_HEIGHT_LANDSCAPE_IPHONE 160

static const CGFloat mailNameFontSize = 38.0f;

@implementation UITextView (PasteBoard)

- (BOOL)canBecomeFirstResponder
{
    return YES;
}
- (BOOL)canPerformAction:(SEL)action withSender:(id)sender
{
    UIPasteboard *pasteboard = [UIPasteboard generalPasteboard];
    if ((pasteboard.string && action == @selector(paste:)))
    {
		return YES;
	}
    return NO;
}
- (void)paste:(id)sender
{
    UIPasteboard *pasteboard = [UIPasteboard generalPasteboard];
    self.text = [self.text stringByAppendingFormat:@"%@\n", pasteboard.string];
}

@end

@interface GRNewMessageController () <GRMailRecipientViewDelegate, UIWebViewDelegate, UITextViewDelegate, UITextFieldDelegate, GRMailServiceDelegate, GRMailAddAttachmentViewDelegate, GRClickableLabelDelegate, GRCommonAlertDelegate, GRMailAttachemntCellDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout, WYPopoverControllerDelegate>

@property (nonatomic, retain) IBOutlet UIView *alertDraftView;
@property (nonatomic, retain) UIView *keyboardView;
@property (nonatomic, assign) NSUInteger attachmentIndex;

@property (nonatomic, retain) WYPopoverController *alertPopoverController;

@end

@implementation GRNewMessageController
{
    CGSize keyboardSize;
}

#pragma mark - Initialization

- (void)setupPopoverAppearance
{
    WYPopoverBackgroundView *popoverAppearance = [WYPopoverBackgroundView appearance];
    
    [popoverAppearance setOverlayColor:[UIColor colorWithWhite:0.1f alpha:0.6f]];
    [popoverAppearance setOuterCornerRadius:20];
    [popoverAppearance setOuterShadowBlurRadius:4];
    [popoverAppearance setOuterShadowColor:[UIColor colorWithWhite:0.3f alpha:0.6f]];
    [popoverAppearance setOuterShadowOffset:CGSizeMake(4, 4)];
    
    [popoverAppearance setGlossShadowColor:[UIColor clearColor]];
    [popoverAppearance setGlossShadowOffset:CGSizeMake(0, 0)];
    
    [popoverAppearance setBorderWidth:0];
    [popoverAppearance setArrowHeight:0];
    [popoverAppearance setArrowBase:0];
    
    [popoverAppearance setInnerCornerRadius:20];
    [popoverAppearance setInnerShadowBlurRadius:0];
    [popoverAppearance setInnerShadowColor:[UIColor clearColor]];
    [popoverAppearance setInnerShadowOffset:CGSizeMake(0, 0)];
    
    [popoverAppearance setFillTopColor:[UIColor whiteColor]];
    [popoverAppearance setFillBottomColor:[UIColor whiteColor]];
    [popoverAppearance setOuterStrokeColor:[UIColor clearColor]];
    [popoverAppearance setInnerStrokeColor:[UIColor clearColor]];
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self)
    {
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardDidShow:) name:UIKeyboardDidShowNotification object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillHide:) name:UIKeyboardWillHideNotification object:nil];
        if ([self respondsToSelector:@selector(setAutomaticallyAdjustsScrollViewInsets:)]) {
            self.automaticallyAdjustsScrollViewInsets = NO;
        }
    }
    return self;
}

- (void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
    [GRMailServiceManager shared].delegate = nil;
    _delegate = nil;
    self.selectedBccContacts = nil;
    self.draftMessage = nil;
    self.selectedToContacts = nil;
    self.attachmentData = nil;
    self.attachmentDataBody = nil;
    [_recipientView release];
    [_addAttachmentView release];
    [_selectedContact release];
    [_tSubject release];
    [_tMessage release];
    [_contentScrollView release];
    [_toContentView release];
    [_toContactBtn release];
    [_firstSeparator release];
    [_secondSeparator release];
    [_lCCbCCTitle release];
    [_bccContactBtn release];
    [_toBccContentView release];
    [_lSubjectTitle release];
    [_thirdSeparator release];
    [_totalContacts release];
    [_messageModel release];
    [_sentMessage release];
    [_webMessageView release];
    [_removedAttachemntsIndexes release];
    [_attachmentsCollectionView release];
    [_totalAttachments release];
    [_lBodyPlaceHodler release];
    [_headerView release];
    [_lToTitle release];
    
    [self.alertPopoverController dismissPopoverAnimated:NO];
    self.alertPopoverController = nil;
    
    [super dealloc];
}

- (void)subjectTextFieldTapped:(UIGestureRecognizer*)gestureRecognizer
{
    if (gestureRecognizer.state == UIGestureRecognizerStateBegan)
    {
        [self.tSubject becomeFirstResponder];
    }
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.view.backgroundColor = [UIColor colorWithWhite:0 alpha:0.8f];
    
    self.tSubject.leftViewMode = UITextFieldViewModeAlways;
    self.tSubject.leftView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 210, self.tSubject.bounds.size.height)];
    self.tSubject.leftView.backgroundColor = [UIColor clearColor];
    UITapGestureRecognizer *tapGestureRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(subjectTextFieldTapped:)];
    [self.tSubject.leftView addGestureRecognizer:tapGestureRecognizer];
    
    self.screenName = NSLocalizedString(@"New Mail Screen", @"New message screen");
    self.attachmentIndex = 1;
    
    [GRMailServiceManager shared].delegate = self;
    
    self.headerView.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"email_msg_header_bkg"]];
    
    self.contentScrollView.contentSize = CGSizeMake(self.contentScrollView.frame.size.width, CGRectGetMaxY(self.tMessage.frame));
    
    self.firstSeparator.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"email_separator_wide"]];
    self.secondSeparator.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"email_separator_wide"]];
    self.thirdSeparator.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"email_separator_tight"]];
    
    self.removedAttachemntsIndexes = [[[NSMutableArray alloc] init] autorelease];
    self.webMessageView.hidden = YES;
    
    self.tMessage.editable = NO;
    self.tMessage.scrollEnabled = NO;
    self.tMessage.dataDetectorTypes = UIDataDetectorTypeAll;
    
    self.webMessageView.scrollView.scrollEnabled = NO;
    
    UITapGestureRecognizer *recognizer = [[[UITapGestureRecognizer alloc] initWithTarget:self
                                                                                 action:@selector(editTextRecognizerTabbed:)] autorelease];
    recognizer.numberOfTapsRequired = 1;
    [self.tMessage addGestureRecognizer:recognizer];
    
    _totalAttachments = [[NSMutableArray alloc] init];
    
    [self.attachmentsCollectionView registerNib:[UINib nibWithNibName:@"GRMailAttachemntDeletableCellView" bundle:nil] forCellWithReuseIdentifier:@"GRMailAttachemntCell"];
    
    if(self.messageModel && self.folderIndex != MAIL_FOLDER_DRAFTS)
    {
        self.lBodyPlaceHodler.hidden = YES;
        self.webMessageView.hidden = NO;
        
        self.tMessage.frame = CGRectMake(self.tMessage.frame.origin.x, self.tMessage.frame.origin.y, self.tMessage.frame.size.width, 120);        
        [self buildContent];
    }
    else {
        [self reloadOtherViews];
    }
    
    [self addTapRecognizersToContactViews];
}

-(void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    if(self.messageModel) {
        if (self.folderIndex == MAIL_FOLDER_DRAFTS) {
            TMailDraftMessage *drm = (TMailDraftMessage *)self.messageModel;
            if ([drm.isFetched integerValue] == 1) {
                self.tMessage.text = [self stripTags:drm.messageBodyHTML];
                self.lBodyPlaceHodler.hidden = YES;
                [self adjustTextView];
            } else {
                NSInteger uid = [drm.uid integerValue];
                [GRMailServiceManager shared].delegate = self;
                [[GRMailServiceManager shared] mailActionFetchMessageWithUID:uid folderIndex:MAIL_FOLDER_DRAFTS];
            }
        }
    }
}

-(void)willRotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)duration
{
    if (!IS_IPAD)
    {
        if( UIInterfaceOrientationIsLandscape(toInterfaceOrientation) )
        {
            [[NSBundle mainBundle] loadNibNamed: [NSString stringWithFormat:@"%@", NSStringFromClass([self class])]
                                          owner: self
                                        options: nil];
            [self viewDidLoad];
        }
        else
        {
            [[NSBundle mainBundle] loadNibNamed: [NSString stringWithFormat:@"%@-portrait", NSStringFromClass([self class])]
                                          owner: self
                                        options: nil];
            [self viewDidLoad];
        }
    }
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    if (!IS_IPAD)
        [self willRotateToInterfaceOrientation:[UIApplication sharedApplication].statusBarOrientation duration:0];
}


#pragma mark - Content builders

- (void)buildContent {
    NSString *senderMail = _messageModel.senderEmail;
    NSString *messageSubject = _messageModel.subject;
    NSString *messageHTMLText = _messageModel.messageBodyHTML;
    NSString *senderName = _messageModel.senderName;
    NSDate   *date = _messageModel.date;
    
    if (_operationIndex == 1) {
        GRContact *contact = [[GRPeopleManager sharedManager] findContactByEmail:senderMail];
        if (!contact)
        {
            contact = [[[GRContact alloc] init] autorelease];
            contact.email = senderMail;
            contact.firstname = senderName;
        }
        self.selectedToContacts = [NSMutableArray arrayWithObject:contact];
    }
    NSDateFormatter *df = [[[NSDateFormatter alloc] init] autorelease];
	[df setDateFormat:@"dd.MM.yyyy HH:mm"];
    NSString *senderDate = [df stringFromDate:date];

    self.tSubject.text = [NSString stringWithFormat:@"Re: %@", messageSubject?:@""];
    if (!senderName) {
        senderName = [GRMailServiceManager shared].currentAccount.userName;
    }
    if (self.folderIndex != MAIL_FOLDER_DRAFTS) {
        self.tMessage.text = [NSString stringWithFormat:@"\n%@ from %@\n\n", senderDate, senderName];
        [self adjustTextView];
    }
    [self.webMessageView loadHTMLString:messageHTMLText baseURL:nil];
}

- (void)webViewDidFinishLoad:(UIWebView *)webView {
    
    // initialize viewport
    NSString* js =
    @"var meta = document.createElement('meta'); " \
    "meta.setAttribute( 'name', 'viewport' ); " \
    "meta.setAttribute( 'content', 'width = device-width, user-scalable = no' ); " \
    "document.getElementsByTagName('head')[0].appendChild(meta)";
    [webView stringByEvaluatingJavaScriptFromString: js];
    
    // resize to full content
    CGRect frame = webView.frame;
    frame.size.height = 1;
    webView.frame = frame;
    CGSize fittingSize = [webView sizeThatFits:CGSizeZero];
    frame.size.height = fittingSize.height;
    webView.frame = frame;
    CGFloat maxWebViewY = CGRectGetMaxY(webView.frame);
    
    // update scroll content
    self.contentScrollView.contentSize = CGSizeMake(self.contentScrollView.frame.size.width, maxWebViewY);
}

#pragma mark - Action methods

- (void) editTextRecognizerTabbed:(UITapGestureRecognizer *) aRecognizer;
{
    self.tMessage.dataDetectorTypes = UIDataDetectorTypeNone;
    self.tMessage.editable = YES;
    [self.tMessage becomeFirstResponder];
}

- (void)goBack
{
    self.closeBtn.selected = NO;
    if (IS_IPAD) {
        if ([_delegate respondsToSelector:@selector(onModalDone)]) {
            [_delegate performSelector:@selector(onModalDone)];
        }
        [self.view removeFromSuperview];
    }
    else
        [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)showContactBtnTapped:(id)sender
{
    [self.view endEditing:YES];
    [[NSNotificationCenter defaultCenter] removeObserver:self];
    [self setUserInteractionEnabled:NO];
    
    UIView *blackView = [[[UIView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height)] autorelease];
    blackView.backgroundColor = [UIColor blackColor];
    blackView.alpha = 0.85;
    blackView.tag = 3000;
    [self.view addSubview:blackView];
    
    if (_recipientView)
    {
        [_recipientView removeFromSuperview];
        self.recipientView = nil;
    }
    NSString *nibName = @"GRMailRecipientView";
    if (IS_IPAD)
    {
        nibName = @"GRMailRecipientView_iPad";
    }
    else
    {
        if (UIInterfaceOrientationIsPortrait([UIApplication sharedApplication].statusBarOrientation))
            nibName = @"GRMailRecipientView-portrait";
    }
    _recipientView = [(GRMailRecipientView *)[GRResource viewFromNib:nibName owner:nil] retain];
    _recipientView.delegate = self;
    _recipientView.contactType = GRMailContactViewTo;
    if (_selectedToContacts)
    {
        _recipientView.selectedContacts = [NSMutableArray arrayWithArray:_selectedToContacts];
        [_recipientView createContactView];
    }
    [self.view addSubview:_recipientView];
    
    if (IS_IPAD)
        _recipientView.frame = CGRectMake(170, 95, _recipientView.frame.size.width, _recipientView.frame.size.height);
}

- (IBAction)showBccContactBtnTapped:(id)sender
{
    UIView *blackView = [[[UIView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height)] autorelease];
    blackView.backgroundColor = [UIColor blackColor];
    blackView.alpha = 0.85;
    blackView.tag = 3000;
    [self.view addSubview:blackView];
    
    [self.view endEditing:YES];
    [[NSNotificationCenter defaultCenter] removeObserver:self];
    [self setUserInteractionEnabled:NO];
    if (_recipientView)
    {
        [_recipientView removeFromSuperview];
        self.recipientView = nil;
    }
    NSString *nibName = @"GRMailRecipientView";
    if (IS_IPAD)
    {
        nibName = @"GRMailRecipientView_iPad";
    }
    else
    {
        if (UIInterfaceOrientationIsPortrait([UIApplication sharedApplication].statusBarOrientation))
            nibName = @"GRMailRecipientView-portrait";
    }
    _recipientView = [(GRMailRecipientView *)[GRResource viewFromNib:nibName owner:nil] retain];
    _recipientView.delegate = self;
    _recipientView.contactType = GRMailContactViewBcc;
    if (_selectedBccContacts)
    {
        _recipientView.selectedContacts = [NSMutableArray arrayWithArray:_selectedBccContacts];
        [_recipientView createContactView];
    }
    [self.view addSubview:_recipientView];
    [_recipientView setBckViewImage];
    
    if (IS_IPAD)
        _recipientView.frame = CGRectMake(170, 110, _recipientView.frame.size.width, _recipientView.frame.size.height);
}

- (IBAction)showAttachmentBtnTapped:(id)sender
{
    if (_alertDraftView)
        return;

    [self.view endEditing:YES];
    NSString *nibName = @"GRMailAddAttachmentView";
    if (IS_IPAD)
    {
        nibName = @"GRMailAddAttachmentView_iPad";
    }
    else
    {
        if (UIInterfaceOrientationIsPortrait([UIApplication sharedApplication].statusBarOrientation))
            nibName = @"GRMailAddAttachmentView-portrait";
    }
    
    self.addAttachmentView = (GRMailAddAttachmentView *)[GRResource viewFromNib:nibName owner:nil];
    _addAttachmentView.delegate = self;
    
    [self.view addSubview:_addAttachmentView];
}


- (IBAction)closeBtnTapped:(id)sender
{
    [self.view endEditing:YES];
    [self setUserInteractionEnabled:NO];
    
    if (IS_IPAD)
    {
        if (!_alertDraftView)
        {
            CGRect coverRect = CGRectMake(62, 83, 900, 675);
            
             [[NSBundle mainBundle] loadNibNamed:@"GRMailPopupExitNewMsg" owner:self options:nil];
            _alertDraftView.frame = coverRect;
            _alertDraftView.backgroundColor = [UIColor colorWithWhite:0 alpha:0.73f];
            [self.view addSubview:_alertDraftView];
        }
        else
        {
            [self setUserInteractionEnabled:YES];
            [_alertDraftView removeFromSuperview];
            self.alertDraftView = nil;
        }
    }
    else
    {
        GRCommonAlertView *alert = [[[GRCommonAlertView alloc] initCommonAlertWithText:NSLocalizedString(@"ARE YOU SURE YOU WANT TO DELETE THE MESSAGE?", nil) size:self.view.frame.size] autorelease];
        alert.delegate = self;
    }
}

- (IBAction)sendBtnTapped:(id)sender
{
    if (_alertDraftView)
        return;
    [self.view endEditing:YES];

    if (_selectedToContacts.count > 0 || _selectedBccContacts.count > 0)
    {
        [self buildSentMessage];
        [[GRMailDataManager sharedManager] savePendingMessage:_sentMessage];
        
        if ([[YLReachability reachabilityForInternetConnection] isReachable])
        {
            if (!_attachmentData)
            {
                self.attachmentData = [NSMutableArray array];
            }
            
            self.sendingIndicatorView.hidden = NO;
            [[GRMailServiceManager shared] mailActionSentMessage:_sentMessage];
            
            
            [self goBack];
        }
        else
        {
            GRCommonAlertView *alert = [[[GRCommonAlertView alloc] initCommonAlert:NSLocalizedString(@"There is no internet connection. Your message will be sent later", nil) inView:self.view] autorelease];
            alert.delegate = nil;
        }
    }
    else
    {
        CGPoint pointerPos = [self.view convertPoint:self.toContentView.frame.origin fromView:self.contentScrollView];
        pointerPos.y += self.toContentView.frame.size.height - 25;
        pointerPos.x = self.view.bounds.size.width / 2;
        GRCommonAlertView *alert = [[[GRCommonAlertView alloc] initCommonAlert:NSLocalizedString(@"Please select a recipient", nil) inView:self.view pointerPosition:pointerPos] autorelease];
        alert.delegate = nil;
        
        alert.frame = CGRectMake(0, 0, 1024, 768);
    }
}

- (IBAction)onContinue
{
    self.closeBtn.selected = NO;
    [self setUserInteractionEnabled:YES];
    [_alertDraftView removeFromSuperview];
    self.alertDraftView = nil;
}

- (IBAction)onSaveDraft {
    if (_folderIndex != MAIL_FOLDER_DRAFTS) {
        [self buildSentMessage];
        [[GRMailServiceManager shared] mailActionAppendMessage:_sentMessage toFolderIndex:MAIL_FOLDER_DRAFTS];
    }
    [self goBack];
}

- (IBAction)onDelete {
    if (_folderIndex == MAIL_FOLDER_DRAFTS) {
        TMailDraftMessage *drm = (TMailDraftMessage *)self.messageModel;
        NSInteger uid = [drm.uid integerValue];
        [[GRMailServiceManager shared] moveMailFromFolder:MAIL_FOLDER_DRAFTS toFolder:MAIL_FOLDER_TRASH withUid:uid];
    }
    [self goBack];
}

#pragma mark - GRClickableLabelDelegate

- (void)clickableLabelSelectedWithObject:(id)object
{
    NSString *filePath = (NSString *)object;
    if ([filePath rangeOfString:@"pdf" options:NSCaseInsensitiveSearch].location != NSNotFound)
    {
        if ([[NSFileManager defaultManager] fileExistsAtPath:filePath])
        {
            NSData *pdf = [NSData dataWithContentsOfFile:filePath];
            CGDataProviderRef provider = CGDataProviderCreateWithCFData((CFDataRef)pdf);
            MFDocumentManager *docManager = [[[MFDocumentManager alloc] initWithDataProvider:provider] autorelease];
            if (docManager)
            {
                GRAttachmentPdfController *pdfController = [[[GRAttachmentPdfController alloc] initWithDocumentManager:docManager] autorelease];
                [self presentViewController:pdfController animated:YES completion:^{
                                     
                                 }];
            }
            
        }
        else
        {
            [[AppDelegate getInstance].window  displayErrorMessageWithString:NSLocalizedString(@"Cover not found", @"Cover not found")];
        }
        
    }
    else if ([filePath rangeOfString:@"doc" options:NSCaseInsensitiveSearch].location != NSNotFound ||
        [filePath rangeOfString:@"docx" options:NSCaseInsensitiveSearch].location != NSNotFound)
    {
        NSString *nibName = @"GRMailAttachmentView";
        if (IS_IPAD)
        {
            nibName = @"GRMailAttachmentView_iPad";
        }
        GRMailAttachmentView *attachmentView = (GRMailAttachmentView *)[GRResource viewFromNib:nibName owner:nil];
        [attachmentView loadDocument:filePath];
        attachmentView.saveBtn.hidden = YES;
        [self.view addSubview:attachmentView];
    }
    else
    {
        NSString *nibName = @"GRMailAttachmentView";
        if (IS_IPAD)
        {
            nibName = @"GRMailAttachmentView_iPad";
        }
        GRMailAttachmentView *attachmentView = (GRMailAttachmentView *)[GRResource viewFromNib:nibName owner:nil];
        [attachmentView loadImage:filePath];
        attachmentView.saveBtn.hidden = YES;
        [self.view addSubview:attachmentView];
    }
    
}
#pragma mark - GRMailRecipientViewDelegate

- (void)mailRecipientSelectedWithContact:(NSMutableArray *)contacts
{
    [[self.view viewWithTag:3000] removeFromSuperview];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardDidShow:) name:UIKeyboardDidShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillHide:) name:UIKeyboardWillHideNotification object:nil];
    
    [self keyboardWillHide:nil];
    [self setUserInteractionEnabled:YES];
    if (!_selectedToContacts)
    {
        self.selectedToContacts = [NSMutableArray array];
    }
    
    [_selectedToContacts removeAllObjects];
    [_selectedToContacts addObjectsFromArray:contacts];
    
    [self reloadToContacts];
    [self reloadBCCContacts];
    [self reloadOtherViews];
}


- (void)mailBccRecipientSelectedWithContact:(NSMutableArray *)contacts
{
    [[self.view viewWithTag:3000] removeFromSuperview];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardDidShow:) name:UIKeyboardDidShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillHide:) name:UIKeyboardWillHideNotification object:nil];
    
    [self keyboardWillHide:nil];
    [self setUserInteractionEnabled:YES];
    if (!_selectedBccContacts)
    {
        self.selectedBccContacts = [NSMutableArray array];
    }
    
    [_selectedBccContacts removeAllObjects];
    [_selectedBccContacts addObjectsFromArray:contacts];
    
    [self reloadToContacts];
    [self reloadBCCContacts];
    [self reloadOtherViews];
}

#pragma mark - Layout subviews

- (void)addTapRecognizersToContactViews {
    UITapGestureRecognizer *toTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(toContactViewTapped:)];
    toTap.numberOfTapsRequired = 1;
    toTap.numberOfTouchesRequired = 1;
    [self.toContentView addGestureRecognizer:toTap];
    [self.toContentView setUserInteractionEnabled:YES];
    [toTap release];

    UITapGestureRecognizer *bccTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(bccContactViewTapped:)];
    bccTap.numberOfTapsRequired = 1;
    bccTap.numberOfTouchesRequired = 1;
    [self.toBccContentView addGestureRecognizer:bccTap];
    [self.toBccContentView setUserInteractionEnabled:YES];
    [bccTap release];
}

- (void)locateLocalAttachments:(NSMutableArray *)localAttachments
{
    for (NSDictionary *atm in _totalAttachments) {
        id isLocal = [atm objectForKey:@"local"];
        if (isLocal!=nil && [isLocal isKindOfClass:NSNumber.class]) {
            [_totalAttachments removeObject:atm];
        }
    }

    [_totalAttachments addObjectsFromArray:localAttachments];
    
    [self reloadOtherViews];
}

- (void)locateAttachments
{
    NSArray *msgAttachments = [[GRMailDataManager sharedManager] getAttachmentsForMessageID:[_messageModel.uid integerValue]];
    
    for (TMessageAttachment *atm in msgAttachments) {
        NSDictionary *item = [NSDictionary dictionaryWithObjectsAndKeys:atm.contentType, MimeTypeKey, atm.fileName, FileNameKey, atm.filePath, PathKey, nil];
        [_totalAttachments addObject:item];
    }
}

- (BOOL)isAttachmentIsDeletedWithIndex:(NSInteger)index {
    BOOL flag = NO;
    for (NSNumber *num in _removedAttachemntsIndexes) {
        if ([num integerValue] == index) {
            flag = YES;
            break;
        }
    }
    return flag;
}

- (NSString *)getFileIconName:(NSString *)contentType
{
    NSString *iconName = @"bmp.png";
    if ([contentType rangeOfString:@"image"].location != NSNotFound)
    {
        iconName = @"jpeg.png";
    }
    if ([contentType rangeOfString:@"officedocument"].location != NSNotFound)
    {
        iconName = @"docx mac.png";
    }
    if ([contentType rangeOfString:@"pdf"].location != NSNotFound)
    {
        iconName = @"pdf.png";
    }
    
    return iconName;
}

- (void)reloadSubviews
{
    [self reloadToContacts];
    [self reloadBCCContacts];
    [self locateAttachments];
    [self reloadOtherViews];
}

- (void)reloadToContacts
{
    for (UIView *v in [self.toContentView subviews])
    {
        if (v.tag != 111 && v.tag != 233)
        {
            [v removeFromSuperview];
        }
    }
    
    CGFloat padding = 3.0f;
    
    CGFloat xPos = 210;
    CGFloat yPos = 0;
    
    if (!IS_IPAD)
    {
        padding = 3.0f;
        
        xPos = 10.0f;
        yPos = padding;
    }
    
    CGFloat contentHeight = 90;
    
    for (int i = 0; i < _selectedToContacts.count; i++)
    {
        GRContact *contactObj = [_selectedToContacts objectAtIndex:i];
        
        UILabel *contactLabel = [[[UILabel alloc] init] autorelease];
        contactLabel.frame = CGRectMake(xPos, yPos, 0, 0);
        contactLabel.font = [UIFont fontWithName:@"Helvetica-Bold" size:mailNameFontSize];
        contactLabel.textColor = rgb(0x424242);
        contactLabel.tag = 500+i;
        
        UITapGestureRecognizer *toTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(toContactTapped:)];
        toTap.numberOfTapsRequired = 1;
        toTap.numberOfTouchesRequired = 1;
        [contactLabel addGestureRecognizer:toTap];
        [contactLabel setUserInteractionEnabled:YES];
        [toTap release];

        if (contactObj.fullname)
        {
            contactLabel.text = [NSString stringWithFormat:@"%@,", [contactObj.fullname capitalizedString]];
        }
        else {
            contactLabel.text = contactObj.selectedEmail ? : contactObj.email;
        }
        
        [contactLabel sizeToFit];
        
        if (CGRectGetMaxX(contactLabel.frame) > self.toContentView.frame.size.width)
        {
            xPos = IS_IPAD ? 210 : 10;
            yPos = yPos + CGRectGetHeight(contactLabel.frame) + padding;
            
            CGRect frame = contactLabel.frame;
            frame.origin = CGPointMake(xPos, yPos);
            contactLabel.frame = frame;
        }
        
        xPos = xPos + CGRectGetWidth(contactLabel.frame) + padding;
        
        contentHeight = yPos + padding + CGRectGetHeight(contactLabel.frame);
        
        [self.toContentView addSubview:contactLabel];
    }
    
    CGRect frame = self.toContentView.frame;
    frame.size.height = contentHeight;
    self.toContentView.frame = frame;
    
    if (contentHeight > CGRectGetHeight(self.lToTitle.frame)) {
        self.lToTitle.center = CGPointMake(self.lToTitle.center.x, self.toContentView.center.y);
        
        frame = self.firstSeparator.frame;
        frame.origin.y = CGRectGetMaxY(self.toContentView.frame);
        self.firstSeparator.frame = frame;
    }
    else {
        self.toContentView.center = CGPointMake(self.toContentView.center.x, self.lToTitle.center.y);
        
        frame = self.firstSeparator.frame;
        frame.origin.y = CGRectGetMaxY(self.lToTitle.frame);
        self.firstSeparator.frame = frame;
    }
}


- (void)reloadBCCContacts
{
    CGRect frame;
    frame = self.lCCbCCTitle.frame;
    frame.origin.y = CGRectGetMaxY(self.firstSeparator.frame);
    self.lCCbCCTitle.frame = frame;
    
    frame = self.toBccContentView.frame;
    frame.origin.y = CGRectGetMaxY(self.firstSeparator.frame);
    self.toBccContentView.frame = frame;
    
    frame = self.secondSeparator.frame;
    frame.origin.y = CGRectGetMaxY(self.toBccContentView.frame);
    self.secondSeparator.frame = frame;
    
    for (UIView *v in [self.toBccContentView subviews])
    {
        if (v.tag != 222)
        {
            [v removeFromSuperview];
        }
    }
    
    CGFloat padding = 3.0f;
    
    CGFloat xPos = 210;
    CGFloat yPos = 0;
    
    if (!IS_IPAD)
    {
        padding = 3.0f;
        
        xPos = 10.0f;
        yPos = padding;
    }
    
    CGFloat contentHeight = 90;
    
    for (int i = 0; i < _selectedBccContacts.count; i++)
    {        
        GRContact *contactObj = [_selectedBccContacts objectAtIndex:i];
        
        UILabel *contactLabel = [[UILabel alloc] init];
        contactLabel.frame = CGRectMake(xPos, yPos, 0, 0);
        contactLabel.font = [UIFont fontWithName:@"Helvetica-Bold" size:mailNameFontSize];
        contactLabel.textColor = rgb(0x424242);
        contactLabel.tag = 600+i;
        
        UITapGestureRecognizer *toTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tobccContactTapped:)];
        toTap.numberOfTapsRequired = 1;
        toTap.numberOfTouchesRequired = 1;
        [contactLabel addGestureRecognizer:toTap];
        [contactLabel setUserInteractionEnabled:YES];
        [toTap release];

        if (contactObj.fullname)
        {
            contactLabel.text = [NSString stringWithFormat:@"%@,", [contactObj.fullname capitalizedString]];
        }
        else {
            contactLabel.text = contactObj.selectedEmail ? : contactObj.email;
        }
        
        [contactLabel sizeToFit];
        
        if (CGRectGetMaxX(contactLabel.frame) > self.toContentView.frame.size.width)
        {
            xPos = IS_IPAD? 210 : 10;
            yPos = yPos + CGRectGetHeight(contactLabel.frame) + padding;
            
            CGRect frame = contactLabel.frame;
            frame.origin = CGPointMake(xPos, yPos);
            contactLabel.frame = frame;
        }
        
        xPos = xPos + CGRectGetWidth(contactLabel.frame) + padding;
        
        contentHeight = yPos + padding + CGRectGetHeight(contactLabel.frame);
        
        [self.toBccContentView addSubview:contactLabel];
        [contactLabel release];
    }
    
    frame = self.toBccContentView.frame;
    frame.size.height = contentHeight;
    self.toBccContentView.frame = frame;
    
    if (contentHeight > CGRectGetHeight(self.lCCbCCTitle.frame)) {
        self.lCCbCCTitle.center = CGPointMake(self.lCCbCCTitle.center.x, self.toBccContentView.center.y);
        
        frame = self.secondSeparator.frame;
        frame.origin.y = CGRectGetMaxY(self.toBccContentView.frame);
        self.secondSeparator.frame = frame;
    }
    else {
        self.toBccContentView.center = CGPointMake(self.toBccContentView.center.x, self.lCCbCCTitle.center.y);
        
        frame = self.secondSeparator.frame;
        frame.origin.y = CGRectGetMaxY(self.lCCbCCTitle.frame);
        self.secondSeparator.frame = frame;
    }
}

- (void)reloadAttachmentsCollectionView {
    if (self.totalAttachments.count) {
        [self.attachmentsCollectionView reloadData];
        CGFloat height = self.attachmentsCollectionView.collectionViewLayout.collectionViewContentSize.height;
        self.attachmentsCollectionView.frame = CGRectMake(self.attachmentsCollectionView.frame.origin.x, self.thirdSeparator.frame.origin.y+3, self.attachmentsCollectionView.frame.size.width, height);
    }
    else {
        CGFloat height = 0;
        self.attachmentsCollectionView.frame = CGRectMake(self.attachmentsCollectionView.frame.origin.x, self.thirdSeparator.frame.origin.y+3, self.attachmentsCollectionView.frame.size.width, height);
    }

}


- (void)reloadOtherViews
{
    CGRect frame;
    frame = self.lSubjectTitle.frame;
    frame.origin.y = CGRectGetMaxY(self.secondSeparator.frame);
    self.lSubjectTitle.frame = frame;
    
    frame = self.tSubject.frame;
    frame.origin.y = CGRectGetMaxY(self.secondSeparator.frame);
    self.tSubject.frame = frame;
    
    frame = self.thirdSeparator.frame;
    frame.origin.y = CGRectGetMaxY(self.lSubjectTitle.frame);
    self.thirdSeparator.frame = frame;
    
    [self reloadAttachmentsCollectionView];
    
    frame = self.tMessage.frame;
    frame.origin.y = CGRectGetMaxY(self.attachmentsCollectionView.frame) + 10;
    frame.size.height = self.contentScrollView.bounds.size.height - frame.origin.y;
    self.tMessage.frame = frame;

    frame = self.lBodyPlaceHodler.frame;
    frame.origin = self.tMessage.frame.origin;
    self.lBodyPlaceHodler.frame = frame;
    
    if (self.messageModel) {
        [self updateWebViewPosition];
        self.contentScrollView.contentSize = CGSizeMake(self.contentScrollView.frame.size.width, CGRectGetMaxY(self.webMessageView.frame));
    }
    else {
       self.contentScrollView.contentSize = CGSizeMake(self.contentScrollView.frame.size.width, CGRectGetMaxY(self.tMessage.frame));
    }
}

- (void)updateWebViewPosition
{
    CGRect frame = self.webMessageView.frame;
    frame.origin.y = CGRectGetMaxY(self.tMessage.frame) + 2;
    self.webMessageView.frame = frame;
}

#pragma mark - UITextViewDelegate
- (void)textViewDidBeginEditing:(UITextView *)textView
{
    self.lBodyPlaceHodler.hidden = YES;
}

-(void)handleLandscapeRightWithKeyboardFirField:(id)sender {
    CGSize kbSize = CGSizeMake(352, 768);
    kbSize.width += 40;
    UIView *v = (UIView *)sender;
    
    CGRect fieldRect = v.frame;
    CGRect aRect = v.superview.frame;
    aRect.size.height -= kbSize.width;
    UIScrollView *scrollView = (UIScrollView *)v.superview;
    if (!CGRectContainsPoint(aRect, fieldRect.origin) ) {
        CGPoint scrollPoint = CGPointMake(0.0, kbSize.width - (v.superview.frame.size.height - fieldRect.origin.y));
        [scrollView setContentOffset:scrollPoint animated:YES];
    }
}

- (void)textViewDidEndEditing:(UITextView *)textView;
{
    if (textView.text.length == 0) {
        self.lBodyPlaceHodler.hidden = NO;
    }
    
    self.tMessage.editable = NO;
    self.tMessage.dataDetectorTypes = UIDataDetectorTypeAll;
}

- (void)textViewDidChange:(UITextView *)textView {
    [self adjustTextView];
}

- (void)adjustTextView
{
    CGSize textViewSize = [self.tMessage sizeThatFits:CGSizeMake(self.tMessage.frame.size.width, FLT_MAX)];
    
    if (textViewSize.height < 215)
    {
        textViewSize.height = 215;
    }
    
    CGRect frame = self.tMessage.frame;
    frame.size.height = textViewSize.height;
    self.tMessage.frame = frame;
    
    if (self.messageModel) {
        [self updateWebViewPosition];
        self.contentScrollView.contentSize =
            CGSizeMake(self.contentScrollView.frame.size.width, CGRectGetMaxY(self.webMessageView.frame));
    }
    else {
        self.contentScrollView.contentSize =
        CGSizeMake(self.contentScrollView.frame.size.width, CGRectGetMaxY(self.tMessage.frame));
    }
}

#pragma mark - UITextFieldDelegate

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [self.contentScrollView setContentOffset:CGPointMake(0, 0)];
    
    if (textField == _tSubject)
    {
        dispatch_async(dispatch_get_main_queue(), ^{
          [self editTextRecognizerTabbed:nil];
            
            CGFloat y = CGRectGetMaxY(_tMessage.frame) + keyboardSize.width - _contentScrollView.bounds.size.height - 65;
            [_contentScrollView setContentOffset:CGPointMake(0, y) animated:YES];
            
            UIEdgeInsets contentInsets = UIEdgeInsetsMake(0.0, 0.0, keyboardSize.width + 20, 0.0);
            _contentScrollView.contentInset = contentInsets;
            _contentScrollView.scrollIndicatorInsets = contentInsets;
            
            [self textViewDidChange:_tMessage];
        });
    }
    else
    {
        [textField resignFirstResponder];
    }
    
    return YES;
}

- (void)textFieldDidBeginEditing:(UITextField *)textField
{
    _keyboardView = textField.superview.superview;
}

- (void)keyboardDidChangeFrame:(NSNotification *)note
{
}

- (void)keyboardDidShow:(NSNotification *)note
{
    if (IS_IPAD) {
        
        NSDictionary* info = [note userInfo];
        keyboardSize = [[info objectForKey:UIKeyboardFrameBeginUserInfoKey] CGRectValue].size;
        
        if ([_tSubject isFirstResponder])
        {
            CGFloat y = 80;
            [_contentScrollView setContentOffset:CGPointMake(0, y) animated:YES];
        }
        else if ([_tMessage isFirstResponder]) {
            NSDictionary* info = [note userInfo];
            CGSize kbSize = [[info objectForKey:UIKeyboardFrameBeginUserInfoKey] CGRectValue].size;
            
            CGFloat offset = 64.f;
            
            if ([_tMessage.text length] == 0)
            {
                offset = 160.f;
            }
            
            CGFloat y = CGRectGetMaxY(_tMessage.frame) + kbSize.width - _contentScrollView.bounds.size.height - offset;
            [_contentScrollView setContentOffset:CGPointMake(0, y) animated:YES];
            
            UIEdgeInsets contentInsets = UIEdgeInsetsMake(0.0, 0.0, kbSize.width + 20, 0.0);
            _contentScrollView.contentInset = contentInsets;
            _contentScrollView.scrollIndicatorInsets = contentInsets;
            
            [self textViewDidChange:_tMessage];
        }
    }
    else
    {
        if (_keyboardView.frame.origin.y + _keyboardView.frame.size.height > _contentScrollView.frame.size.height - _contentScrollView.contentOffset.y)
            [_contentScrollView setContentOffset:CGPointMake(_contentScrollView.contentOffset.x, _keyboardView.frame.origin.y + _keyboardView.frame.size.height - _contentScrollView.frame.size.height - self.tSubject.frame.origin.y) animated:YES];
    }
}

- (void) keyboardWillHide:(NSNotification *)note
{
    [_contentScrollView setContentOffset:CGPointMake(0, 0) animated:YES];
    [CATransaction setCompletionBlock:^{
        UIEdgeInsets contentInsets = UIEdgeInsetsZero;
        _contentScrollView.contentInset = contentInsets;
        _contentScrollView.scrollIndicatorInsets = contentInsets;
    }];
}
#pragma mark - GRCommonAlertDelegate
- (void)commonAlert:(id)alertView buttonWithIndex:(NSInteger)index
{
    if (index == 0)
    {
        [self onSaveDraft];
    }
    else
    {
        [self onDelete];
    }
}
#pragma mark - GRMailServiceDelegate

- (void)mailActionSucceed:(NSInteger)operationCode {
    switch (operationCode) {
        case MAIL_FETCH_OP:
            self.tMessage.text = [self stripTags:((TMailDraftMessage *)self.messageModel).messageBodyHTML];
            self.lBodyPlaceHodler.hidden = YES;
            [self adjustTextView];
            break;
            
        default:
            break;
    }
}

- (void)mailActionFailed:(NSError *)error {
    GRCommonAlertView *alert = [[[GRCommonAlertView alloc] initCommonAlert:[error localizedDescription] inView:self.view] autorelease];
    alert.delegate = nil;
}


#pragma mark - GRMailAddAttachmentViewDelegate
- (void)mailAttachemntsSelectedWithImages:(NSArray *)assets {
    if (!assets) return;
    
    for (ALAsset *asset in assets) {
        UIImage *image = [UIImage imageWithCGImage:[[asset defaultRepresentation] fullScreenImage]];
        NSData *imageData = UIImageJPEGRepresentation(image, 1.0);
        
        NSArray *paths = NSSearchPathForDirectoriesInDomains(NSCachesDirectory,  NSUserDomainMask, YES);
        NSString *documentsDirectoryPath = [paths objectAtIndex:0];
        NSString *fileName = [NSString stringWithFormat:@"Attachment%d.png", self.attachmentIndex];
        NSString *filePath = [documentsDirectoryPath  stringByAppendingPathComponent:fileName];
        
        [imageData writeToFile:filePath options:NSDataWritingAtomic error:nil];
        
        NSDictionary *item = [NSDictionary dictionaryWithObjectsAndKeys:
                              @"image", MimeTypeKey,
                              fileName, FileNameKey,
                              filePath, PathKey,
                              [NSNumber numberWithInt:1], @"local",
                              nil];
        
        [_totalAttachments addObject:item];
        self.attachmentIndex++;
    }
    
    [self reloadOtherViews];
}

#pragma mark - Helpers

- (void)buildSentMessage {
    self.sentMessage = [[[GRSMTPMessage alloc] init] autorelease];
    _sentMessage.uid = [self buildLocalUid];
    _sentMessage.tos = [NSMutableArray array];
    _sentMessage.attachments = [NSMutableArray array];
    _sentMessage.senderMail = [GRMailServiceManager shared].currentAccount.userName;
    for (GRContact *c in _selectedToContacts) {
        NSString *contactMail = c.selectedEmail ? c.selectedEmail:c.email;
        [_sentMessage.tos addObject:contactMail];
    }
    for (GRContact *c in _selectedBccContacts) {
        NSString *contactMail = c.selectedEmail ? c.selectedEmail:c.email;
        [_sentMessage.tos addObject:contactMail];
    }
    
    for (NSDictionary *atm in _totalAttachments) {
        [_sentMessage.attachments addObject:atm];
    }
    _sentMessage.subject = self.tSubject.text;
    _sentMessage.plainText = self.tMessage.text;
    if (_messageModel) {
        NSString *htmlText = _messageModel.messageBodyHTML;
        _sentMessage.htmlText = htmlText;
    }
}

- (NSInteger)buildLocalUid {
    NSString *strNum = @"";
    for (int i=0; i<4; i++) {
        NSInteger num = arc4random() % 9;
        strNum = [strNum stringByAppendingFormat:@"%i", num];
    }
    return [strNum integerValue];
}

- (void) setUserInteractionEnabled:(BOOL)enabled
{
    _tSubject.userInteractionEnabled = enabled;
    _tMessage.userInteractionEnabled = enabled;
    
    _toContentView.userInteractionEnabled = enabled;
    _toBccContentView.userInteractionEnabled = enabled;
    
    _contentScrollView.userInteractionEnabled = enabled;
    
    _toContactBtn.userInteractionEnabled = enabled;
    _bccContactBtn.userInteractionEnabled = enabled;
    
    _lCCbCCTitle.userInteractionEnabled = enabled;
    _lSubjectTitle.userInteractionEnabled = enabled;
}
- (void)animateContentViewUp {
    CGRect frame = self.view.frame;
    if (IS_IPAD) {
        frame.origin.y = -240;
    } else {
        frame.origin.y = -160;
    }
    [UIView beginAnimations:nil context:NULL];
	[UIView setAnimationDuration:0.3];
	[UIView setAnimationDelegate:self];
	self.view.frame = frame;
	[UIView commitAnimations];
}

- (void)restoreContentView {
    CGRect frame = self.view.frame;
    frame.origin.y = 0;
    [UIView beginAnimations:nil context:NULL];
	[UIView setAnimationDuration:0.3];
	[UIView setAnimationDelegate:self];
	self.view.frame = frame;
	[UIView commitAnimations];
}

- (BOOL)isToContactExists:(GRContact *)contact {
    BOOL flag = NO;
    for (GRContact *c in _selectedToContacts) {
        if ([c.recId isEqualToNumber:contact.recId]) {
            flag = YES;
            break;
        }
    }
    return flag;
}

- (BOOL)isBccContactExists:(GRContact *)contact {
    BOOL flag = NO;
    for (GRContact *c in _selectedBccContacts) {
        if ([c.recId isEqualToNumber:contact.recId]) {
            flag = YES;
            break;
        }
    }
    return flag;
}

- (BOOL)isTotalContactExists:(GRContact *)contact {
    BOOL flag = NO;
    for (GRContact *c in _totalContacts) {
        if ([c.recId isEqualToNumber:contact.recId]) {
            flag = YES;
            break;
        }
    }
    return flag;
}

- (NSString *)stripTags:(NSString *)str
{
    NSMutableString *html = [NSMutableString stringWithCapacity:[str length]];
    
    NSScanner *scanner = [NSScanner scannerWithString:str];
    scanner.charactersToBeSkipped = NULL;
    NSString *tempText = nil;
    
    while (![scanner isAtEnd])
    {
        [scanner scanUpToString:@"<" intoString:&tempText];
        
        if (tempText != nil)
            [html appendString:tempText];
        
        [scanner scanUpToString:@">" intoString:NULL];
        
        if (![scanner isAtEnd])
            [scanner setScanLocation:[scanner scanLocation] + 1];
        
        tempText = nil;
    }
    
    NSArray *lines = [html componentsSeparatedByString:@"\n\n"];
    NSString *formattedLine = [NSString stringWithFormat:@""];
    for (NSString *line in lines) {
        if (line.length > 0) {
            formattedLine = [formattedLine stringByAppendingFormat:@"%@", line];
        }
    }
    formattedLine = [formattedLine stringByReplacingOccurrencesOfString:@"&nbsp" withString:@" "];
    
    return formattedLine;
}

#pragma mark - Button actions

- (void)removeAttachmentBtnTapped:(id)sender {
    UIButton *btn = (UIButton *)sender;
    if (btn.tag < 700) {
        NSInteger index = btn.tag - 600;
        [_removedAttachemntsIndexes addObject:[NSNumber numberWithInt:index]];
        [self locateAttachments];
    } else {
        NSInteger index = btn.tag - 700;
        NSLog(@"Index %i from %i", index, [_attachments count]);
        if (index < [_attachments count]) {
            [_attachments removeObjectAtIndex:index];
            [self locateLocalAttachments:_attachments];
        }
    }
}


#pragma mark - UICollectionViewDataSource

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return [_totalAttachments count];
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView
                  cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *identifier = @"GRMailAttachemntCell";
    
    GRMailAttachemntCell *collectionCell =
    [collectionView dequeueReusableCellWithReuseIdentifier:identifier
                                              forIndexPath:indexPath];
    
    NSDictionary *item = [_totalAttachments objectAtIndex:indexPath.row];
    [collectionCell updateForItem:item];
    [collectionCell setIndex:indexPath.row];
    [collectionCell setDelegate:self];
    
    return collectionCell;
}

#pragma mark - UICollectionViewDelegate

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    NSDictionary *item = [_totalAttachments objectAtIndex:indexPath.row];
    NSString *mimeType = [item objectForKey:MimeTypeKey];
    NSString *filePath = [item objectForKey:PathKey];
    if ([mimeType rangeOfString:@"pdf" options:NSCaseInsensitiveSearch].location != NSNotFound) {
        if ([[NSFileManager defaultManager] fileExistsAtPath:filePath])
        {
            NSData *pdf = [NSData dataWithContentsOfFile:filePath];
            CGDataProviderRef provider = CGDataProviderCreateWithCFData((CFDataRef)pdf);
            MFDocumentManager *docManager = [[MFDocumentManager alloc] initWithDataProvider:provider];
            if (docManager) {
                GRAttachmentPdfController *pdfController = [[GRAttachmentPdfController alloc] initWithDocumentManager:docManager];
                [(UIViewController *)_delegate presentViewController:pdfController animated:YES completion:^{
                    
                }];
            }
            
        }
        else
        {
            GRCommonAlertView *alert = [[[GRCommonAlertView alloc] initCommonAlert:NSLocalizedString(@"Cover not found", @"Cover not found") inView:self.view] autorelease];
            alert.delegate = nil;
        }
    } else if ([mimeType rangeOfString:@"word" options:NSCaseInsensitiveSearch].location != NSNotFound ||
               [mimeType rangeOfString:@"excel" options:NSCaseInsensitiveSearch].location != NSNotFound || [mimeType rangeOfString:@"ppt" options:NSCaseInsensitiveSearch].location != NSNotFound) {
        NSString *nibName = UIInterfaceOrientationIsLandscape([UIApplication sharedApplication].statusBarOrientation) ? @"GRMailAttachmentView" : @"GRMailAttachmentView-portrait";
        if (IS_IPAD)
        {
            nibName = @"GRMailAttachmentView_iPad";
        }
        GRMailAttachmentView *attachmentView = (GRMailAttachmentView *)[GRResource viewFromNib:nibName owner:nil];
        [attachmentView loadDocument:filePath];
        attachmentView.saveBtn.hidden = YES;
        [self.view addSubview:attachmentView];
    } else if ([mimeType rangeOfString:@"image" options:NSCaseInsensitiveSearch].location != NSNotFound) {
        NSString *nibName = UIInterfaceOrientationIsLandscape([UIApplication sharedApplication].statusBarOrientation) ? @"GRMailAttachmentView" : @"GRMailAttachmentView-portrait";
        if (IS_IPAD) {
            nibName = @"GRMailAttachmentView_iPad";
        }
        GRMailAttachmentView *attachmentView = (GRMailAttachmentView *)[GRResource viewFromNib:nibName owner:nil];
        [attachmentView loadImage:filePath];
        attachmentView.saveBtn.hidden = [GRMailServiceManager shared].currentFolder == 2;
        [self.view addSubview:attachmentView];
    }
    
}


#pragma mark - GRMailAttachemntCellDelegate

- (void)mailAttachmentCellRemoveWithIndex:(NSInteger)index {
    if ([_totalAttachments count] >= index) {
        [_totalAttachments removeObjectAtIndex:index];
    }
    [self reloadOtherViews];
}


#pragma marl - UITapGestureRecognizer 

- (void)toContactViewTapped:(UIGestureRecognizer *)sender {
    [self showContactBtnTapped:nil];
}

- (void)bccContactViewTapped:(UIGestureRecognizer *)sender {
    [self showBccContactBtnTapped:nil];
}

- (void)cancelDeletionButtonTapped
{
    [self.alertPopoverController dismissPopoverAnimated:NO];
    self.alertPopoverController = nil;
}

- (void)okDeletionButtonTapped:(UIButton*)button
{
    NSInteger tag = button.tag;
    if (tag >= 500) {
        NSInteger index = tag-500;
        if (index < [_selectedToContacts count]) {
            [_selectedToContacts removeObjectAtIndex:index];
        }
    }
    if (tag >= 600) {
        NSInteger index = tag-600;
        if (index < [_selectedBccContacts count]) {
            [_selectedBccContacts removeObjectAtIndex:index];
        }
    }
    
    [self reloadSubviews];
    
    [self.alertPopoverController dismissPopoverAnimated:NO];
    self.alertPopoverController = nil;
}

- (void)toContactTapped:(UIGestureRecognizer *)sender {
    
    [self setupPopoverAppearance];
    
    UIViewController *contentViewController = [[[UIViewController alloc] init] autorelease];
    
    contentViewController.contentSizeForViewInPopover = CGSizeMake(600, 300);
    
    DeleteRecipientView *deleteRecipientView = [[[NSBundle mainBundle] loadNibNamed:@"DeleteRecipientView" owner:nil options:nil] lastObject];
    
    [[contentViewController view] addSubview:deleteRecipientView];
    
    [deleteRecipientView.cancelButton addTarget:self action:@selector(cancelDeletionButtonTapped) forControlEvents:UIControlEventTouchUpInside];
    
    [deleteRecipientView.okButton addTarget:self action:@selector(okDeletionButtonTapped:) forControlEvents:UIControlEventTouchUpInside];
    deleteRecipientView.okButton.tag = [sender view].tag;
    
    self.alertPopoverController = [[[WYPopoverController alloc] initWithContentViewController:contentViewController] autorelease];
    self.alertPopoverController.delegate = self;
    
    [self.alertPopoverController presentPopoverAsDialogAnimated:YES];
    
//    DeleteAlertView *alertView = [[[DeleteAlertView alloc] initWithTitle:NSLocalizedString(@"Are you sure you want to delete this name from the recipient list ?", @"Are you sure you want to delete this name from the recipient list ?")
//                                                                 message:nil
//                                                                delegate:self
//                                                       cancelButtonTitle:nil
//                                                       otherButtonTitles:
//                                   NSLocalizedString(@"CANCEL", @"CANCEL"),
//                                   NSLocalizedString(@"DELETE", @"DELETE"), nil] autorelease];
//    
//
//    alertView.tag = [sender view].tag;
//    [alertView show];
}

- (void)tobccContactTapped:(UIGestureRecognizer *)sender {
    
    [self setupPopoverAppearance];
    
    UIViewController *contentViewController = [[[UIViewController alloc] init] autorelease];
    
    contentViewController.contentSizeForViewInPopover = CGSizeMake(600, 300);
    
    DeleteRecipientView *deleteRecipientView = [[[NSBundle mainBundle] loadNibNamed:@"DeleteRecipientView" owner:nil options:nil] lastObject];
    
    [[contentViewController view] addSubview:deleteRecipientView];
    
    [deleteRecipientView.cancelButton addTarget:self action:@selector(cancelDeletionButtonTapped) forControlEvents:UIControlEventTouchUpInside];
    
    [deleteRecipientView.okButton addTarget:self action:@selector(okDeletionButtonTapped:) forControlEvents:UIControlEventTouchUpInside];
    deleteRecipientView.okButton.tag = [sender view].tag;
    
    self.alertPopoverController = [[[WYPopoverController alloc] initWithContentViewController:contentViewController] autorelease];
    self.alertPopoverController.delegate = self;
    
    [self.alertPopoverController presentPopoverAsDialogAnimated:YES];
    
//    DeleteAlertView *alertView = [[[DeleteAlertView alloc] initWithTitle:NSLocalizedString(@"Are you sure you want to delete this name from the recipient list ?", @"Are you sure you want to delete this name from the recipient list ?")
//                                                                 message:nil
//                                                                delegate:self
//                                                       cancelButtonTitle:nil
//                                                       otherButtonTitles:
//                                   NSLocalizedString(@"CANCEL", @"CANCEL"),
//                                   NSLocalizedString(@"DELETE", @"DELETE"), nil] autorelease];
//    
//    
//    alertView.tag = [sender view].tag;
//    [alertView show];
}


#pragma mark - AlertViewDelegate's methods

- (void)alertView:(AlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
//    if ([alertView isKindOfClass:[DeleteAlertView class]])
//    {
//        if (buttonIndex == 1)
//        {
//            NSInteger tag = alertView.tag;
//            if (tag >= 500) {
//                NSInteger index = tag-500;
//                if (index < [_selectedToContacts count]) {
//                    [_selectedToContacts removeObjectAtIndex:index];
//                }
//            }
//            if (tag >= 600) {
//                NSInteger index = tag-600;
//                if (index < [_selectedBccContacts count]) {
//                    [_selectedBccContacts removeObjectAtIndex:index];
//                }
//            }
//
//            [self reloadSubviews];
//            
//        }
//    }
}

#pragma mark - WYPopoverControllerDelegate's methods

- (BOOL)popoverControllerShouldDismissPopover:(WYPopoverController *)popoverController
{
    return YES;
}

- (void)popoverControllerDidDismissPopover:(WYPopoverController *)popoverController
{
    self.alertPopoverController = nil;
}

@end
