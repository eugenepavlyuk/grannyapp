//
//  GRMailSettingsNewAccountController.m
//  GrannyApp
//
//  Created by Ievgen Pavliuk on 7/30/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "GRMailSettingsNewAccountController.h"
#import "GRMailAccountManager.h"
#import "AlertView.h"
#import "WYPopoverController.h"
#import "GRAlertViewController.h"

@interface GRMailSettingsNewAccountController () <WYPopoverControllerDelegate>

@property (nonatomic, retain) WYPopoverController *alertPopoverController;

@end

@implementation GRMailSettingsNewAccountController

@synthesize tPassword;
@synthesize tAccountName;
@synthesize tUserName;
@synthesize contentView;
@synthesize delegate;
@synthesize mailAccount;

#pragma mark - GRMailServiceDelegate

- (void)mailActionSucceed:(NSInteger)operationCode {
    switch (operationCode) {
        case MAIL_VERIFICATION_OP:
            alertMessageIndex = 1;
            
            [[AppDelegate getInstance].window unBlockUI];
            [[GRMailAccountManager manager] createAccount:mailSettings];
            NSNotificationCenter *notificationCenter = [NSNotificationCenter defaultCenter];
            [notificationCenter postNotificationName:@"newAccountCreated"
                                              object:nil
                                            userInfo:nil];
            
            
            WYPopoverBackgroundView *popoverAppearance = [WYPopoverBackgroundView appearance];
            
            [popoverAppearance setOverlayColor:[UIColor colorWithWhite:0.1f alpha:0.6f]];
            [popoverAppearance setOuterCornerRadius:0];
            [popoverAppearance setOuterShadowBlurRadius:0];
            [popoverAppearance setOuterShadowColor:[UIColor clearColor]];
            [popoverAppearance setOuterShadowOffset:CGSizeZero];
            
            [popoverAppearance setGlossShadowColor:[UIColor clearColor]];
            [popoverAppearance setGlossShadowOffset:CGSizeMake(0, 0)];
            
            [popoverAppearance setBorderWidth:1];
            [popoverAppearance setArrowHeight:0];
            [popoverAppearance setArrowBase:0];
            
            [popoverAppearance setInnerCornerRadius:0];
            [popoverAppearance setInnerShadowBlurRadius:0];
            [popoverAppearance setInnerShadowColor:[UIColor clearColor]];
            [popoverAppearance setInnerShadowOffset:CGSizeMake(0, 0)];
            
            [popoverAppearance setFillTopColor:[UIColor whiteColor]];
            [popoverAppearance setFillBottomColor:[UIColor whiteColor]];
            [popoverAppearance setOuterStrokeColor:[UIColor clearColor]];
            [popoverAppearance setInnerStrokeColor:[UIColor clearColor]];

            
            GRAlertViewController *contentViewController = [[[GRAlertViewController alloc] init] autorelease];
            
            contentViewController.contentSizeForViewInPopover = CGSizeMake(324, 175);
            
            [contentViewController view];
            
            contentViewController.messageLabel.text = NSLocalizedString(@"The account was \nsuccessfully added", @"The account was \nsuccessfully added");
            
            contentViewController.messageLabel.font = [UIFont fontWithName:@"Helvetica" size:18];
            
            [contentViewController.okButton setBackgroundImage:nil forState:UIControlStateNormal];
            contentViewController.okButton.frame = CGRectMake(0, contentViewController.okButton.frame.origin.y + 30, 50, 28);
            contentViewController.okButton.center = CGPointMake(contentViewController.view.bounds.size.width / 2, contentViewController.okButton.center.y);
            contentViewController.okButton.layer.borderWidth = 1.f;
            contentViewController.okButton.layer.borderColor = RGBCOLOR(190, 190, 190).CGColor;
            [contentViewController.okButton setTitleColor:RGBCOLOR(96, 96, 96) forState:UIControlStateNormal];
            contentViewController.okButton.titleLabel.font = [UIFont fontWithName:@"Helvetica" size:14];
            
            [contentViewController.okButton addTarget:self action:@selector(popoverButtonTapped:) forControlEvents:UIControlEventTouchUpInside];
            
            self.alertPopoverController = [[[WYPopoverController alloc] initWithContentViewController:contentViewController] autorelease];
            self.alertPopoverController.delegate = self;
            
            [self.alertPopoverController presentPopoverAsDialogAnimated:YES];

            break;
            
        default:
            break;
    }
}

- (void)popoverButtonTapped:(UIButton*)button
{
    isDone = YES;
    [self.view removeFromSuperview];
    [self.alertPopoverController dismissPopoverAnimated:YES];
    self.alertPopoverController = nil;
}

- (void)incorrectUsernameButtonTapped:(UIButton*)button
{
    [self.alertPopoverController dismissPopoverAnimated:YES];
    self.alertPopoverController = nil;
}

- (void)noInternetButtonTapped:(UIButton*)button
{
    [self.alertPopoverController dismissPopoverAnimated:YES];
    self.alertPopoverController = nil;
}

- (void)mailActionFailed:(NSError *)error {
    alertMessageIndex = 2;
    
    [[AppDelegate getInstance].window unBlockUI];
    NSNotificationCenter *notificationCenter = [NSNotificationCenter defaultCenter];
    [notificationCenter postNotificationName:@"newAccountCreated"
                                      object:nil
                                    userInfo:nil];
    
    WYPopoverBackgroundView *popoverAppearance = [WYPopoverBackgroundView appearance];
    
    [popoverAppearance setOverlayColor:[UIColor colorWithWhite:0.1f alpha:0.6f]];
    [popoverAppearance setOuterCornerRadius:0];
    [popoverAppearance setOuterShadowBlurRadius:0];
    [popoverAppearance setOuterShadowColor:[UIColor clearColor]];
    [popoverAppearance setOuterShadowOffset:CGSizeZero];
    
    [popoverAppearance setGlossShadowColor:[UIColor clearColor]];
    [popoverAppearance setGlossShadowOffset:CGSizeMake(0, 0)];
    
    [popoverAppearance setBorderWidth:1];
    [popoverAppearance setArrowHeight:0];
    [popoverAppearance setArrowBase:0];
    
    [popoverAppearance setInnerCornerRadius:0];
    [popoverAppearance setInnerShadowBlurRadius:0];
    [popoverAppearance setInnerShadowColor:[UIColor clearColor]];
    [popoverAppearance setInnerShadowOffset:CGSizeMake(0, 0)];
    
    [popoverAppearance setFillTopColor:[UIColor whiteColor]];
    [popoverAppearance setFillBottomColor:[UIColor whiteColor]];
    [popoverAppearance setOuterStrokeColor:[UIColor clearColor]];
    [popoverAppearance setInnerStrokeColor:[UIColor clearColor]];
    
    
    GRAlertViewController *contentViewController = [[[GRAlertViewController alloc] init] autorelease];
    
    contentViewController.contentSizeForViewInPopover = CGSizeMake(324, 175);
    
    [contentViewController view];
    
    contentViewController.messageLabel.text = NSLocalizedString(@"The username or password \n is incorrect. Please try again.", @"The username or password \n is incorrect. Please try again.");
    
    contentViewController.messageLabel.font = [UIFont fontWithName:@"Helvetica" size:18];
    
    [contentViewController.okButton setBackgroundImage:nil forState:UIControlStateNormal];
    contentViewController.okButton.frame = CGRectMake(0, contentViewController.okButton.frame.origin.y + 30, 50, 28);
    contentViewController.okButton.center = CGPointMake(contentViewController.view.bounds.size.width / 2, contentViewController.okButton.center.y);
    contentViewController.okButton.layer.borderWidth = 1.f;
    contentViewController.okButton.layer.borderColor = RGBCOLOR(190, 190, 190).CGColor;
    [contentViewController.okButton setTitleColor:RGBCOLOR(96, 96, 96) forState:UIControlStateNormal];
    contentViewController.okButton.titleLabel.font = [UIFont fontWithName:@"Helvetica" size:14];
    
    [contentViewController.okButton addTarget:self action:@selector(incorrectUsernameButtonTapped:) forControlEvents:UIControlEventTouchUpInside];
    
    self.alertPopoverController = [[[WYPopoverController alloc] initWithContentViewController:contentViewController] autorelease];
    self.alertPopoverController.delegate = self;
    
    [self.alertPopoverController presentPopoverAsDialogAnimated:YES];
}

#pragma mark - Initialization

- (void)dealloc {
    [tPassword release];
    [tAccountName release];
    [tUserName release];
    [mailSettings release];
    [contentView release];
    [mailAccount release];
    
    [self.alertPopoverController dismissPopoverAnimated:YES];
    self.alertPopoverController = nil;
    
    [super dealloc];
}


- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.screenName = @"Mail Settings New Account Screen";
    
    if(self.mailAccount) {
        self.tAccountName.text = mailAccount.accountName;
        self.tUserName.text = mailAccount.userName;
        self.tPassword.text = mailAccount.password;
    }
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    if (!IS_IPAD)
        [self willRotateToInterfaceOrientation:[UIApplication sharedApplication].statusBarOrientation duration:0];
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
}


- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    if (isCancel) {
        if (delegate && [delegate respondsToSelector:@selector(onModalCancel:)]) {
            [delegate performSelector:@selector(onModalCancel:) withObject:nil];
        }
    }
    if (isDone) {
        if (delegate && [delegate respondsToSelector:@selector(onModalDone:)]) {
            [delegate performSelector:@selector(onModalDone:) withObject:nil];
        }
    }
}



- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
}

-(void)willRotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)duration
{
    if( UIInterfaceOrientationIsLandscape(toInterfaceOrientation) )
    {
        [[NSBundle mainBundle] loadNibNamed: [NSString stringWithFormat:@"%@", NSStringFromClass([self class])]
                                      owner: self
                                    options: nil];
        [self viewDidLoad];
    }
    else
    {
        [[NSBundle mainBundle] loadNibNamed: [NSString stringWithFormat:@"%@-portrait", NSStringFromClass([self class])]
                                      owner: self
                                    options: nil];
        [self viewDidLoad];
    }
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    if (IS_IPAD)
        return UIInterfaceOrientationIsLandscape(interfaceOrientation);
    else
        return (interfaceOrientation != UIInterfaceOrientationPortraitUpsideDown);
}


#pragma mark - UITextFieldDelegate

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    [textField resignFirstResponder];
    if (textField.tag == 111) {
        [self okBtnTapped:nil];
    }
    return YES;
}

- (void)textFieldDidBeginEditing:(UITextField *)textField {
    if (textField.tag == 111) {
        if (!IS_IPAD) {
            [self animateContentViewUp];
        }
    }
}

- (void)textFieldDidEndEditing:(UITextField *)textField {
    if (textField.tag == 111) {
        if (!IS_IPAD) {
            [self restoreContentView];
        }
    }
}

#pragma mark - Actions

- (IBAction)okBtnTapped:(id)sender {
    
    if ([[YLReachability reachabilityForInternetConnection] isReachable]) {
        alertMessageIndex = 0;
        
        [self.tAccountName resignFirstResponder];
        [self.tUserName resignFirstResponder];
        [self.tPassword resignFirstResponder];
        
        if (![self validateAccountName]) {
            return;
        }
        
        NSString *username = tUserName.text;
        
        if (![self validateEmailAdress:tUserName.text])
        {
            NSRange range = [tUserName.text rangeOfString:@"@"];
            
            if (range.location != NSNotFound)
            {
                username = [tUserName.text substringToIndex:range.location];
            }
            
            username = [username stringByAppendingString:@"@gmail.com"];
        }
        
//        if (![self validateEmail])
//        {
//            return;
//        }
        
        [GRMailAccountManager manager].mailType = GRMAIL_TYPE_GMAIL;
        mailSettings = [[[GRMailAccountManager manager] getCurrentMailSettings] retain];
        [mailSettings setObject:username forKey:@"userName"];
        [mailSettings setObject:tPassword.text forKey:@"password"];
        [mailSettings setObject:tAccountName.text forKey:@"accountName"];
        [mailSettings setObject:@(2) forKey:@"connectionType"];
        [GRMailServiceManager shared].delegate = self;
        [[GRMailServiceManager shared] mailActionVerification:mailSettings];
        [[AppDelegate getInstance].window blockUIWithMessage:NSLocalizedString(@"Validating account", @"Validating account") isBlack:YES];
    }
    else
    {
        
        WYPopoverBackgroundView *popoverAppearance = [WYPopoverBackgroundView appearance];
        
        [popoverAppearance setOverlayColor:[UIColor colorWithWhite:0.1f alpha:0.6f]];
        [popoverAppearance setOuterCornerRadius:0];
        [popoverAppearance setOuterShadowBlurRadius:0];
        [popoverAppearance setOuterShadowColor:[UIColor clearColor]];
        [popoverAppearance setOuterShadowOffset:CGSizeZero];
        
        [popoverAppearance setGlossShadowColor:[UIColor clearColor]];
        [popoverAppearance setGlossShadowOffset:CGSizeMake(0, 0)];
        
        [popoverAppearance setBorderWidth:1];
        [popoverAppearance setArrowHeight:0];
        [popoverAppearance setArrowBase:0];
        
        [popoverAppearance setInnerCornerRadius:0];
        [popoverAppearance setInnerShadowBlurRadius:0];
        [popoverAppearance setInnerShadowColor:[UIColor clearColor]];
        [popoverAppearance setInnerShadowOffset:CGSizeMake(0, 0)];
        
        [popoverAppearance setFillTopColor:[UIColor whiteColor]];
        [popoverAppearance setFillBottomColor:[UIColor whiteColor]];
        [popoverAppearance setOuterStrokeColor:[UIColor clearColor]];
        [popoverAppearance setInnerStrokeColor:[UIColor clearColor]];
        
        
        GRAlertViewController *contentViewController = [[[GRAlertViewController alloc] init] autorelease];
        
        contentViewController.contentSizeForViewInPopover = CGSizeMake(324, 175);
        
        [contentViewController view];
        
        contentViewController.messageLabel.text = NSLocalizedString(@"No Internet connection", @"No Internet connection");
        
        contentViewController.messageLabel.font = [UIFont fontWithName:@"Helvetica" size:18];
        
        [contentViewController.okButton setBackgroundImage:nil forState:UIControlStateNormal];
        contentViewController.okButton.frame = CGRectMake(0, contentViewController.okButton.frame.origin.y + 30, 50, 28);
        contentViewController.okButton.center = CGPointMake(contentViewController.view.bounds.size.width / 2, contentViewController.okButton.center.y);
        contentViewController.okButton.layer.borderWidth = 1.f;
        contentViewController.okButton.layer.borderColor = RGBCOLOR(190, 190, 190).CGColor;
        [contentViewController.okButton setTitleColor:RGBCOLOR(96, 96, 96) forState:UIControlStateNormal];
        contentViewController.okButton.titleLabel.font = [UIFont fontWithName:@"Helvetica" size:14];
        
        [contentViewController.okButton addTarget:self action:@selector(noInternetButtonTapped:) forControlEvents:UIControlEventTouchUpInside];
        
        self.alertPopoverController = [[[WYPopoverController alloc] initWithContentViewController:contentViewController] autorelease];
        self.alertPopoverController.delegate = self;
        
        [self.alertPopoverController presentPopoverAsDialogAnimated:YES];
    }
}


- (IBAction)cancelBtnTapped:(id)sender
{
    isCancel = YES;
    if (IS_IPAD)
        [self.view removeFromSuperview];
    else
        [self.navigationController popViewControllerAnimated:YES];
}

- (BOOL)validateAccountName {
    if ([tAccountName.text length] == 0) {
        [[AppDelegate getInstance] displayAlertWithMessage:@"Enter account name"];
        return NO;
    }
    return YES;
}

- (BOOL)validateEmailAdress:(NSString*)candidate
{
	NSString *emailRegEx = @"[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}";
	NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegEx];
	return [emailTest evaluateWithObject:[candidate lowercaseString]];
}

- (BOOL)validateEmail
{
    if (![self validateEmailAdress:tUserName.text]) {
        [[AppDelegate getInstance] displayAlertWithMessage:@"Wrong username"];
        return NO;
    }
    return YES;
}

#pragma mark - Layout helpers

- (void)animateContentViewUp {
    originY = self.contentView.frame.origin.y;
    CGRect frame = self.contentView.frame;
    frame.origin.y = -50.0f;
    [UIView beginAnimations:nil context:NULL];
	[UIView setAnimationDuration:0.3];
	[UIView setAnimationDelegate:self];
	self.contentView.frame = frame;
	[UIView commitAnimations];
}

- (void)restoreContentView {
    CGRect frame = self.contentView.frame;
    frame.origin.y = originY;
    [UIView beginAnimations:nil context:NULL];
	[UIView setAnimationDuration:0.3];
	[UIView setAnimationDelegate:self];
	self.contentView.frame = frame;
	[UIView commitAnimations];
}

#pragma mark - GRCommonAlertDelegate

- (void)commonAlert:(id)alertView buttonWithIndex:(NSInteger)index
{
    if (alertMessageIndex == 1)
    {
        if (IS_IPAD) {
            isDone = YES;
            [self.view removeFromSuperview];
        }
        else {
            [self.navigationController popViewControllerAnimated:YES];
        }
    }
    else
    {
        
    }
}

#pragma mark - AlertViewDelegate's methods

- (void)alertView:(AlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    isDone = YES;
    [self.view removeFromSuperview];
}

#pragma mark - WYPopoverControllerDelegate's methods

- (BOOL)popoverControllerShouldDismissPopover:(WYPopoverController *)popoverController
{
    return YES;
}

- (void)popoverControllerDidDismissPopover:(WYPopoverController *)popoverController
{
    self.alertPopoverController = nil;
}

@end
