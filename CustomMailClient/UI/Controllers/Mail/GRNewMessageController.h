//
//  GRNewMessageController.h
//  GrannyApp
//
//  Created by Ievgen Pavliuk on 7/11/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "GRMailBaseViewController.h"

@class GRMailRecipientView;
@class GRMailAddAttachmentView;

@interface UITextView (PasteBoard)
@end

@interface GRNewMessageController : GRMailBaseViewController

@property (nonatomic, retain) IBOutlet UITextField *tSubject;
@property (nonatomic, retain) IBOutlet UITextView *tMessage;
@property (nonatomic, retain) IBOutlet UIWebView *webMessageView;
@property (nonatomic, retain) IBOutlet UIView *toContentView;
@property (nonatomic, retain) IBOutlet UIView *toBccContentView;
@property (nonatomic, retain) IBOutlet UIScrollView *contentScrollView;
@property (nonatomic, retain) IBOutlet UIButton *closeBtn;
@property (nonatomic, retain) IBOutlet UILabel *lCCbCCTitle;
@property (nonatomic, retain) IBOutlet UILabel *lToTitle;
@property (nonatomic, retain) IBOutlet UILabel *lSubjectTitle;
@property (nonatomic, retain) IBOutlet UILabel *lBodyPlaceHodler;
@property (nonatomic, retain) IBOutlet UILabel *firstSeparator;
@property (nonatomic, retain) IBOutlet UILabel *secondSeparator;
@property (nonatomic, retain) IBOutlet UILabel *thirdSeparator;
@property (nonatomic, retain) IBOutlet UIActivityIndicatorView *sendingIndicatorView;
@property (nonatomic, retain) IBOutlet UICollectionView *attachmentsCollectionView;
@property (nonatomic, retain) IBOutlet UIView *headerView;
@property (nonatomic, retain) UIButton *toContactBtn;
@property (nonatomic, retain) UIButton *bccContactBtn;
@property (nonatomic, retain) GRContact *selectedContact;

@property (nonatomic, retain) NSMutableArray  *savedLocalAttachments;
@property (nonatomic, retain) NSMutableArray  *totalContacts;
@property (nonatomic, retain) NSMutableArray  *totalAttachments;
@property (nonatomic, retain) NSMutableArray *removedAttachemntsIndexes;
@property (nonatomic, retain) GRSMTPMessage *sentMessage;

@property (nonatomic, retain) NSObject *draftMessage;
@property (nonatomic, retain) NSMutableArray  *attachmentData;
@property (nonatomic, retain) NSMutableArray  *attachmentDataBody;
@property (nonatomic, retain) NSMutableArray  *selectedToContacts;
@property (nonatomic, retain) NSMutableArray  *selectedBccContacts;
@property (nonatomic, retain) NSMutableArray *attachments;
@property (nonatomic, retain) TMailMessageBase *messageModel;
@property (nonatomic, assign) NSInteger folderIndex;
@property (nonatomic, assign) NSInteger operationIndex;
@property (nonatomic, assign) id delegate;
@property (nonatomic, retain) GRMailRecipientView *recipientView;
@property (nonatomic, retain) GRMailAddAttachmentView *addAttachmentView;

- (IBAction)showContactBtnTapped:(id)sender;
- (IBAction)showBccContactBtnTapped:(id)sender;
- (IBAction)closeBtnTapped:(id)sender;
- (IBAction)showAttachmentBtnTapped:(id)sender;
- (IBAction)sendBtnTapped:(id)sender;

- (void)animateContentViewUp;
- (void)restoreContentView;

- (void)reloadSubviews;
- (void)reloadToContacts;
- (void)reloadBCCContacts;
- (void)reloadOtherViews;
- (void)locateAttachments;

- (void)mailAttachemntsSelectedWithImages:(NSArray *)assets;

- (BOOL)isToContactExists:(GRContact *)contact;
- (BOOL)isBccContactExists:(GRContact *)contact;
- (BOOL)isTotalContactExists:(GRContact *)contact;

- (IBAction)onContinue;
- (IBAction)onSaveDraft;
- (IBAction)onDelete;

@end
