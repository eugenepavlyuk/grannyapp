//
//  GRAttachmentPdfController.h
//  GrannyApp
//
//  Created by Ievgen Pavliuk on 7/20/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MFDocumentViewController.h"
#import "MFDocumentViewControllerDelegate.h"

@interface GRAttachmentPdfController : MFDocumentViewController <MFDocumentViewControllerDelegate> {
    
}

-(id)initWithDocumentManager:(MFDocumentManager *)aDocumentManager;
@end
