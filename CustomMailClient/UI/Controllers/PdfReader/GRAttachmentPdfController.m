//
//  GRAttachmentPdfController.m
//  GrannyApp
//
//  Created by Ievgen Pavliuk on 7/20/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "GRAttachmentPdfController.h"

@interface GRAttachmentPdfController ()

@end

@implementation GRAttachmentPdfController

-(id)initWithDocumentManager:(MFDocumentManager *)aDocumentManager {
	if((self = [super initWithDocumentManager:aDocumentManager])) {
		[self setDocumentDelegate:self];
        [self setAutoMode:MFDocumentAutoModeOverflow];
        [self setAutomodeOnRotation:YES];
        [self setAutozoomOnPageChange:NO];
        self.padding = 0.0f;
        [self setMode:MFDocumentModeDouble];
	}
	return self;
}

- (BOOL)prefersStatusBarHidden
{
    return YES;
}

- (void)loadView
{
	UIView * aView = nil;    
    
	BOOL isPad = NO;
#ifdef UI_USER_INTERFACE_IDIOM
	isPad = (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad);
#endif
 	if(isPad) {
		aView = [[UIView alloc]initWithFrame:CGRectMake(0, 20 + 44, 768, 1024-20-44)];
	} else {
		aView = [[UIView alloc]initWithFrame:CGRectMake(0, 20 + 44, 320, 480-20-44)];
	}
	
	[aView setAutoresizingMask:UIViewAutoresizingFlexibleWidth|UIViewAutoresizingFlexibleHeight];
	[aView setAutoresizesSubviews:YES];
	
    

    
	// Background color: a nice texture if available, otherwise plain gray.
	
	if ([UIColor respondsToSelector:@selector(scrollViewTexturedBackgroundColor)]) {
		[aView setBackgroundColor:[UIColor colorWithPatternImage:[UIImage imageNamed:@"Main_Background.png"]]];
	} else {
		[aView setBackgroundColor:[UIColor colorWithPatternImage:[UIImage imageNamed:@"Main_Background.png"]]];
	}
	
	[self setView:aView];
	
	[aView release];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [[AppDelegate getInstance].window blockUIWithMessage:@"" isBlack:YES];
    [self performSelector:@selector(hideLockUI) withObject:nil afterDelay:3.0f];
    UIImageView *headerView = [[[UIImageView alloc] initWithImage:[UIImage imageNamed:@"Mail_Header.png"]] autorelease];
    
    if (IS_IPAD) 
    {
        headerView.frame = CGRectMake(1024/2 - headerView.frame.size.width/2, 0, headerView.frame.size.width, headerView.frame.size.height);
    } 
    else 
    {
        headerView.frame = CGRectMake(0, 0, self.view.frame.size.width, 37);
    }
    
    [self.view addSubview:headerView];
    
    UIButton *closeBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    [closeBtn setBackgroundImage:[UIImage imageNamed:@"Mail_CancelBtn.png"] forState:UIControlStateNormal];
    closeBtn.frame = CGRectMake(15, 10, 50, 50);
    
    if (!IS_IPAD) 
    {
        closeBtn.frame = CGRectMake(4, 4, 25, 25);
    }
    
    [closeBtn addTarget:self action:@selector(cloaseBtnTapped:) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:closeBtn];

    UILabel *lTitle;
    
    if (IS_IPAD) 
    {
        lTitle = [[[UILabel alloc] initWithFrame:CGRectMake(0, 10, self.view.frame.size.width, 50)] autorelease];
    } 
    else 
    {
        lTitle = [[[UILabel alloc] initWithFrame:CGRectMake(0, 10, self.view.frame.size.width, 20)] autorelease];
    }
    
    lTitle.backgroundColor = [UIColor clearColor];
    lTitle.font = [UIFont fontWithName:@"Helvetica-Bold" size:32];
    
    if (!IS_IPAD) 
    {
        lTitle.font = [UIFont fontWithName:@"Helvetica-Bold" size:16];
    }
    
    lTitle.textColor = [UIColor whiteColor];
    lTitle.textAlignment = NSTextAlignmentCenter;
    lTitle.text = NSLocalizedString(@"Read attachment document", @"Read attachment document");
    [self.view addSubview:lTitle];
    
    for (UIView *sView in [self.view subviews]) {
        if ([sView isKindOfClass:UIScrollView.class]) {
            sView.frame = CGRectMake(sView.frame.origin.x, sView.frame.origin.y+30, sView.frame.size.width, sView.frame.size.height);
        }
    }
}

- (void)hideLockUI {
    [[AppDelegate getInstance].window unBlockUI];
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return UIInterfaceOrientationIsLandscape(interfaceOrientation);
}

// The nice things about delegate callbacks is that we can use them to update the UI when the internal status of
// the controller changes, rather than query or keep track of it when the user press a button. Just listen for
// the right event and update the UI accordingly.

-(void) documentViewController:(MFDocumentViewController *)dvc didGoToPage:(NSUInteger)page {
}

-(void) documentViewController:(MFDocumentViewController *)dvc didChangeModeTo:(MFDocumentMode)mode automatic:(BOOL)automatically {
}

-(void) documentViewController:(MFDocumentViewController *)dvc didChangeDirectionTo:(MFDocumentDirection)direction {
}

-(void) documentViewController:(MFDocumentViewController *)dvc didChangeLeadTo:(MFDocumentLead)lead {
}

-(void) documentViewController:(MFDocumentViewController *)dvc didReceiveTapOnPage:(NSUInteger)page atPoint:(CGPoint)point {
}


-(void) documentViewController:(MFDocumentViewController *)dvc didReceiveTapAtPoint:(CGPoint)point {
}

- (void)documentViewController:(MFDocumentViewController *)dvc didReceiveRequestToGoToDestinationNamed:(NSString *)destinationName ofFile:(NSString *)fileName{
    
}
- (void)documentViewController:(MFDocumentViewController *)dvc didReceiveRequestToGoToPage:(NSUInteger)pageNumber ofFile:(NSString *)fileName{
}



#pragma mark - Actions

- (void)cloaseBtnTapped:(id)sender
{
    [self dismissViewControllerAnimated:YES completion:^{
        
    }];
}
@end
