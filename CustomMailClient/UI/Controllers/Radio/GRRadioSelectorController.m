//
//  GRRadioSelectorController.m
//  GrannyApp
//
//  Created by Ievgen Pavliuk on 8/1/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "GRRadioSelectorController.h"
#import "GRRadioSettingsResultsController.h"
#import "DataManager.h"
#import "TVChannel.h"

@interface GRRadioSelectorController ()
{
    UIInterfaceOrientation orientation;
    
    NSMutableArray *allChannels;
}

@end

@implementation GRRadioSelectorController

@synthesize pickerView;

#pragma mark - Initialization

- (void)dealloc
{
    self.pickerView = nil;
    [genres release];
    [languages release];
    [countries release];
    
    [allChannels release];
    [super dealloc];
}

- (BOOL)prefersStatusBarHidden
{
    return YES;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.screenName = @"Radio Station Selection Screen";
    
    containerView.layer.borderWidth = 1.f;
    containerView.layer.borderColor = RGBCOLOR(96, 96, 96).CGColor;
    
    if (IS_IPAD)
    {
        self.pickerView.frame = CGRectMake(self.pickerView.frame.origin.x, self.pickerView.frame.origin.y+50, self.pickerView.frame.size.width, self.pickerView.frame.size.height-80);
    }
    else
    {
        isBackgroundHidden = NO;
        if (UIInterfaceOrientationIsPortrait(orientation))
        {
            self.pickerView.frame = CGRectMake(-120, 100, self.pickerView.frame.size.width*2, self.pickerView.frame.size.height);
            self.pickerView.transform = CGAffineTransformScale(CGAffineTransformIdentity, 0.5, 0.5);
//            self.pickerView.transform = CGAffineTransformMakeScale(0.5, 0.5);
        }
        else
        {
            self.pickerView.frame = CGRectMake(-90, 50, self.pickerView.frame.size.width*2, self.pickerView.frame.size.height);
            self.pickerView.transform = CGAffineTransformScale(CGAffineTransformIdentity, 0.5, 0.5);
//            self.pickerView.transform = CGAffineTransformMakeScale(0.5, 0.5);
        }
    }
    
    if (countries)
    {
        [countries release];
        countries = nil;
    }
    if (languages)
    {
        [languages release];
        languages = nil;
    }
    if (genres)
    {
        [genres release];
        genres = nil;
    }
    
    countries = [[NSMutableArray alloc] init];
    languages = [[NSMutableArray alloc] init];
    genres = [[NSMutableArray alloc] init];
    
//    [countries addObject:@"USA"];
//    [countries addObject:@"DENMARK"];
//    [countries addObject:@"ITALY"];
//    [countries addObject:@"GERMANY"];
//
//    [languages addObject:@"ENGLISH"];
//    [languages addObject:@"DANISH"];
//    [languages addObject:@"ITALIAN"];
//    [languages addObject:@"DEUTCH"];
//
//    [genres addObject:@"House"];
//    [genres addObject:@"Jazz"];
//    [genres addObject:@"Rock"];
//    [genres addObject:@"Pop"];
    
    if (!allChannels)
    {
        allChannels = [[[DataManager sharedInstance] allRowsForEntity:@"RadioChannel"] mutableCopy];
    }
    
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0), ^{
        
        for (TVChannel *channel in allChannels)
        {
            BOOL bFound = NO;
         
            if ([[channel.country stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]] length])
            {
                for (NSString *country in countries)
                {
                    if ([country compare:channel.country options:NSCaseInsensitiveSearch] == NSOrderedSame)
                    {
                        bFound = YES;
                        
                        break;
                    }
                }
                
                if (!bFound)
                {
                    [countries addObject:channel.country];
                }
                
                bFound = NO;
            }
            
            if ([[channel.genre stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]] length])
            {
                for (NSString *genre in genres)
                {
                    if ([genre compare:channel.genre options:NSCaseInsensitiveSearch] == NSOrderedSame)
                    {
                        bFound = YES;
                        
                        break;
                    }
                }
                
                if (!bFound)
                {
                    NSString *addedValue = [channel.genre stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];;
                    [genres addObject:addedValue];
                }

                bFound = NO;
            }
            
            if ([[channel.language stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]] length])
            {
                for (NSString *language in languages)
                {
                    if ([language compare:channel.language options:NSCaseInsensitiveSearch] == NSOrderedSame)
                    {
                        bFound = YES;
                        
                        break;
                    }
                }
                
                if (!bFound)
                {
                    [languages addObject:channel.language];
                }
            }
        }
        
        dispatch_async(dispatch_get_main_queue(), ^{
            [pickerView reloadAllComponents];
        });
    });
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    if (!IS_IPAD)
        [self willRotateToInterfaceOrientation:[UIApplication sharedApplication].statusBarOrientation duration:0];
}

- (void)viewDidUnload
{
    [super viewDidUnload];
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    if (IS_IPAD)
        return UIInterfaceOrientationIsLandscape(interfaceOrientation);
    else
        return (interfaceOrientation != UIInterfaceOrientationPortraitUpsideDown);
}

#pragma mark - Actions
- (IBAction)homeBtnTapped:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)okBtnTapped:(id)sender
{
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"language == %@ OR country == %@ OR genre == %@", [self valueForComponent:1 row:[self.pickerView selectedRowInComponent:1]], [self valueForComponent:0 row:[self.pickerView selectedRowInComponent:0]], [self valueForComponent:2 row:[self.pickerView selectedRowInComponent:2]]];
    
    NSMutableArray *array = [allChannels mutableCopy];
    
    [array filterUsingPredicate:predicate];
    
    GRRadioSettingsResultsController *resultsController = [[GRRadioSettingsResultsController alloc] init];
    
    resultsController.tableData = array;
    
    [self.navigationController pushViewController:resultsController animated:YES];
    
    [array release];
    
    [resultsController release];
}

#pragma mark - UIPickerViewDelegate

- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView {
	return 3;
}

- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component {
    
}


- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component {
    NSInteger count = 0;
    switch (component) {
        case 0:
            count = [countries count];
            break;
        case 1:
            count = [languages count];
            break;
        case 2:
            count = [genres count];
            break;
            
        default:
            break;
    }
    return count;
}

- (UIView *)pickerView:(UIPickerView *)pickerView viewForRow:(NSInteger)row forComponent:(NSInteger)component reusingView:(UIView *)view
{
    UIView *rowView = [[[UIView alloc] initWithFrame:view.frame] autorelease];
    
    UILabel *lValue = [[[UILabel alloc] initWithFrame:CGRectMake(20, 18, 150, 24)] autorelease];
    lValue.backgroundColor = [UIColor clearColor];
    lValue.font = [UIFont fontWithName:@"Helvetica-Bold" size:24];
    if (!IS_IPAD)
    {
        lValue.frame = CGRectMake(20, 18, 150, 24);
        lValue.font = [UIFont fontWithName:@"Helvetica-Bold" size:24];
    }
    
    lValue.text = [self valueForComponent:component row:row];
    [rowView addSubview:lValue];
    
//    if (!isBackgroundHidden)
//    {
//        isBackgroundHidden = YES;
//        for (int i = 0; i < self.pickerView.subviews.count; i++)
//        {
//            UIView *v = [self.pickerView.subviews objectAtIndex:i];
//            if (i == 2)
//            {
//                for (UIView *dv in [v subviews])
//                {
//                    [dv removeFromSuperview];
//                }
//                v.backgroundColor = [UIColor greenColor];
//                UIImageView *image = [[[UIImageView alloc] initWithImage:[UIImage imageNamed:@"Settings_PickerComponentLeft.png"]] autorelease];
//                image.frame = CGRectMake(-5, -2, v.frame.size.width+10, v.frame.size.height+4);
//                if (!IS_IPAD)
//                {
//                    image.frame = CGRectMake(-6, 0, v.frame.size.width+10, v.frame.size.height+2);
//                }
//                [v addSubview:image];
//            }
//            
//            if (i == 8)
//            {
//                for (UIView *dv in [v subviews])
//                {
//                    [dv removeFromSuperview];
//                }
//                v.backgroundColor = [UIColor greenColor];
//                UIImageView *image = [[[UIImageView alloc] initWithImage:[UIImage imageNamed:@"Settings_PickerComponentMiddle.png"]] autorelease];
//                image.frame = CGRectMake(0, -2, v.frame.size.width, v.frame.size.height+4);
//                if (!IS_IPAD)
//                {
//                    image.frame = CGRectMake(0, 0, v.frame.size.width+2, v.frame.size.height+2);
//                }
//                [v addSubview:image];
//            }
//            
//            if (i == 14)
//            {
//                for (UIView *dv in [v subviews])
//                {
//                    [dv removeFromSuperview];
//                }
//                v.backgroundColor = [UIColor greenColor];
//                UIImageView *image = [[[UIImageView alloc] initWithImage:[UIImage imageNamed:@"Settings_PickerComponentRight.png"]] autorelease];
//                image.frame = CGRectMake(-4, -2, v.frame.size.width+8, v.frame.size.height+4);
//                if (!IS_IPAD)
//                {
//                    image.frame = CGRectMake(-4, 0, v.frame.size.width+8, v.frame.size.height+2);
//                }
//                [v addSubview:image];
//            }
//            
//            if (i == 6)
//            {
//                v.frame = CGRectMake(v.frame.origin.x, v.frame.origin.y, v.frame.size.width+2, v.frame.size.height);
//                UIImageView *image = [[[UIImageView alloc] initWithImage:[UIImage imageNamed:@"Settings_PickerSelected.png"]] autorelease];
//                image.frame = CGRectMake(0, 0, v.frame.size.width, v.frame.size.height-13);
//                if (!IS_IPAD)
//                {
//                    image.frame = CGRectMake(-2, 0, v.frame.size.width+4, v.frame.size.height-13);
//                }
//                [v insertSubview:image atIndex:0];
//            }
//            if (i == 12)
//            {
//                UIImageView *image = [[[UIImageView alloc] initWithImage:[UIImage imageNamed:@"Settings_PickerSelected.png"]] autorelease];
//                image.frame = CGRectMake(0, 0, v.frame.size.width, v.frame.size.height-13);
//                if (!IS_IPAD)
//                {
//                    image.frame = CGRectMake(0, 0, v.frame.size.width+2, v.frame.size.height-13);
//                }
//                [v insertSubview:image atIndex:0];
//            }
//            if (i == 18)
//            {
//                UIImageView *image = [[[UIImageView alloc] initWithImage:[UIImage imageNamed:@"Settings_PickerSelected.png"]] autorelease];
//                image.frame = CGRectMake(-2, 0, v.frame.size.width+4, v.frame.size.height-13);
//                if (!IS_IPAD) {
//                    image.frame = CGRectMake(-3, 0, v.frame.size.width+6, v.frame.size.height-13);
//                }
//                [v insertSubview:image atIndex:0];
//            }
//                        
//            if (i!=2 && i!=4 && i!=6 
//                && i!=8 &&i!=10 && i!=12 
//                &&  i!=14 && i!=16 && i!=18) {
//                [v setHidden:YES];
//            }
//        }
//    }

    return rowView;
}

- (CGFloat)pickerView:(UIPickerView *)pickerView rowHeightForComponent:(NSInteger)component {
    if (IS_IPAD) {
        return 50.0f;
    } else {
        return 50.0f;
    }
}

//- (NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component {
//	return @"Value";
//}

- (NSString *)valueForComponent:(NSInteger)component row:(NSInteger)row
{
    NSString *value = @"";
    switch (component)
    {
        case 0:
            value = [countries objectAtIndex:row];
            break;
        case 1:
            value = [languages objectAtIndex:row];
            break;
        case 2:
            value = [genres objectAtIndex:row];
            break;
            
        default:
            break;
    }
    
    return [value stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
}

@end
