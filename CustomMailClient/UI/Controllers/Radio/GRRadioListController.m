//
//  GRRadioListController.m
//  Granny
//
//  Created by Ievgen Pavliuk on 3/27/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "GRRadioListController.h"
#import "GRRadioController.h"


@interface GRRadioListController ()

@end

@implementation GRRadioListController
@synthesize contentTable;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (BOOL)prefersStatusBarHidden
{
    return YES;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.screenName = @"Radio List Screen";
    
    self.navigationItem.title = NSLocalizedString(@"Radio Stations", @"Radio Stations");
    
    [self initTableData];
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return UIInterfaceOrientationIsLandscape(interfaceOrientation);
}

- (void)dealloc {
    [contentTable release];
    [super dealloc];
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [tableData count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *CellIdentifier = @"Cell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier] autorelease];
    }

    NSDictionary *radio = [tableData objectAtIndex:[indexPath row]];
    cell.textLabel.text = [radio objectForKey:@"RadioTitle"];
    cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
    
    return cell;
}


#pragma mark - Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    NSDictionary *radioObj = [tableData objectAtIndex:[indexPath row]];
    GRRadioController *radioController = [[GRRadioController alloc] initWithNibName:@"GRRadioController" bundle:nil];
    radioController.radioObj = radioObj;
    [self.navigationController pushViewController:radioController animated:YES];
    [radioController release];
}


#pragma mark - Helpers

- (void)initTableData {
    tableData = [[NSMutableArray alloc] init];
    NSDictionary *radio1 = [NSDictionary dictionaryWithObjects:[NSArray arrayWithObjects:@"MusicRadio", @"http://yp.shoutcast.com/sbin/tunein-station.pls?id=172748", nil] forKeys:[NSArray arrayWithObjects:@"RadioTitle", @"RadioUrl", nil]];

    NSDictionary *radio2 = [NSDictionary dictionaryWithObjects:[NSArray arrayWithObjects:@"Magic FM Romania", @"http://yp.shoutcast.com/sbin/tunein-station.pls?id=69637", nil] forKeys:[NSArray arrayWithObjects:@"RadioTitle", @"RadioUrl", nil]];

    NSDictionary *radio3 = [NSDictionary dictionaryWithObjects:[NSArray arrayWithObjects:@"Radio ZU Live", @"http://yp.shoutcast.com/sbin/tunein-station.pls?id=86114", nil] forKeys:[NSArray arrayWithObjects:@"RadioTitle", @"RadioUrl", nil]];

    NSDictionary *radio4 = [NSDictionary dictionaryWithObjects:[NSArray arrayWithObjects:@"SUPER FM", @"http://yp.shoutcast.com/sbin/tunein-station.pls?id=48959", nil] forKeys:[NSArray arrayWithObjects:@"RadioTitle", @"RadioUrl", nil]];

    NSDictionary *radio5 = [NSDictionary dictionaryWithObjects:[NSArray arrayWithObjects:@"Vocal Trance", @"http://yp.shoutcast.com/sbin/tunein-station.pls?id=1177953", nil] forKeys:[NSArray arrayWithObjects:@"RadioTitle", @"RadioUrl", nil]];

    [tableData addObject:radio1];
    [tableData addObject:radio2];
    [tableData addObject:radio3];
    [tableData addObject:radio4];
    [tableData addObject:radio5];
}


@end
