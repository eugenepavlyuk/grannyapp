//
//  GRRadioSettingsSearchController.m
//  GrannyApp
//
//  Created by Ievgen Pavliuk on 8/1/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "GRRadioSettingsSearchController.h"
#import "GRRadioSettingsResultsController.h"

#import "DataManager.h"
#import "TVChannel.h"

@interface GRRadioSettingsSearchController ()

@end

@implementation GRRadioSettingsSearchController
{
    NSMutableArray *allChannels;
}

@synthesize tSeacrh;
@synthesize backView;

#pragma mark - Initializatoon

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)dealloc {
    [tSeacrh release];
    [backView release];
    [allChannels release];
    [super dealloc];
}

- (BOOL)prefersStatusBarHidden
{
    return YES;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.screenName = @"Radio Station Settings Search Screen";
    
    self.backView.layer.cornerRadius = 10.0f;
    
    if (!allChannels)
    {
        allChannels = [[[DataManager sharedInstance] allRowsForEntity:@"RadioChannel"] mutableCopy];
    }
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self.tSeacrh becomeFirstResponder];
    
    if (!IS_IPAD)
        [self willRotateToInterfaceOrientation:[UIApplication sharedApplication].statusBarOrientation duration:0];
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
}

-(void)willRotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)duration
{
    if( UIInterfaceOrientationIsLandscape(toInterfaceOrientation) )
    {
        [[NSBundle mainBundle] loadNibNamed: [NSString stringWithFormat:@"%@", NSStringFromClass([self class])]
                                      owner: self
                                    options: nil];
        [self viewDidLoad];
    }
    else
    {
        [[NSBundle mainBundle] loadNibNamed: [NSString stringWithFormat:@"%@-portrait", NSStringFromClass([self class])]
                                      owner: self
                                    options: nil];
        [self viewDidLoad];
    }
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    if (IS_IPAD)
        return UIInterfaceOrientationIsLandscape(interfaceOrientation);
    else
        return (interfaceOrientation != UIInterfaceOrientationPortraitUpsideDown);
}

#pragma mark - UITextFieldDelegate

- (BOOL)textFieldShouldReturn:(UITextField *)textField 
{
    [textField resignFirstResponder];
    
    if ([textField.text length] > 0) 
    {
        NSPredicate *predicate = [NSPredicate predicateWithFormat:@"channelTitle CONTAINS[c] %@", textField.text];
        
        NSMutableArray *array = [allChannels mutableCopy];
        
        [array filterUsingPredicate:predicate];
        
        GRRadioSettingsResultsController *resultsController = [[GRRadioSettingsResultsController alloc] init];
        
        resultsController.tableData = array;
        
        [self.navigationController pushViewController:resultsController animated:YES];
        [resultsController release];
    }
    
    return YES;
}

- (void)textFieldDidBeginEditing:(UITextField *)textField 
{
}

- (void)textFieldDidEndEditing:(UITextField *)textField 
{
}

#pragma mark - Action methods
- (IBAction)homeBtnTapped:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}
@end
