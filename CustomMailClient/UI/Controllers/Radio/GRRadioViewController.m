//
//  GRRadioPlayerController.m
//  GrannyApp
//
//  Created by Ievgen Pavliuk on 7/17/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "GRRadioViewController.h"
#import "GRResource.h"
#import "GRRadioDataManager.h"
#import "TRadioStation.h"
#import "GRRadioManager.h"
#import "MBProgressHUD.h"
#import "BDGridView.h"

@interface GRRadioViewController ()
{
    NSTimer *hideVolumeViewTimer;

    UIInterfaceOrientation currentOrientation;
    NSArray *savedStations;
}

@property (nonatomic, retain) NSArray *savedStations;

@end

@implementation GRRadioViewController

@synthesize rightContentScrollView;
@synthesize volumeView;
@synthesize volumeSlider;
@synthesize muteBtn;
@synthesize savedStations;
@synthesize volumeBtn;

#pragma mark - GRServiceDelegate

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    
    if (self)
    {
        
    }
    
    return self;
}

- (void)radioStationsReceivedWithData:(id)data
{
    self.savedStations = [[GRRadioDataManager shared] getAllSavedStations];
    [self reloadStationsWithData:savedStations index:[GRRadioManager sharedManager].currentStationIndex];
}

- (void) radioPlayerInit
{
    CGAffineTransform trans = CGAffineTransformRotate(CGAffineTransformIdentity, M_PI * 0.5);
    self.volumeSlider.transform = trans;
    self.volumeSlider.frame = CGRectMake(self.volumeSlider.frame.origin.x, 20, self.volumeSlider.frame.size.width, 260);
    
    UIImage *stetchLeftTrack = nil;
    UIImage *stetchRightTrack = nil;
    
    if (IS_IPAD)
    {
        self.volumeSlider.frame = CGRectMake(self.volumeSlider.frame.origin.x, 100, self.volumeSlider.frame.size.width, 600);
        
            stetchLeftTrack = [[UIImage imageNamed:@"Radio_Slider_HighlitedBck.png"]
                               resizableImageWithCapInsets:UIEdgeInsetsMake(0, 20, 0, 20)];
            stetchRightTrack = [[UIImage imageNamed:@"Radio_Slider_DefaultBck.png"]
                                resizableImageWithCapInsets:UIEdgeInsetsMake(0, 20, 0, 20)];
        
        [self.volumeSlider setThumbImage:[UIImage imageNamed:@"Radio_Slider_Ball.png"] forState:UIControlStateNormal];
        [self.volumeSlider setThumbImage:[UIImage imageNamed:@"Radio_Slider_Ball.png"] forState:UIControlStateHighlighted];
        [self.volumeSlider setMinimumTrackImage:stetchRightTrack forState:UIControlStateNormal];
        [self.volumeSlider setMaximumTrackImage:stetchLeftTrack forState:UIControlStateNormal];
    }
    else
    {
        self.volumeSlider.frame = CGRectMake(self.volumeSlider.frame.origin.x+1, 55, self.volumeSlider.frame.size.width, 130);
        if ([[UIDevice currentDevice].systemVersion floatValue] >= 5.0)
        {
            stetchLeftTrack = [[UIImage imageNamed:@"Radio_Slider_HighlitedBck_iPhone.png"]
                               resizableImageWithCapInsets:UIEdgeInsetsMake(0, 10, 0, 10)];
            stetchRightTrack = [[UIImage imageNamed:@"Radio_Slider_DefaultBck_iPhone.png"]
                                resizableImageWithCapInsets:UIEdgeInsetsMake(0, 10, 0, 10)];
        }
        else
        {
            stetchLeftTrack = [[UIImage imageNamed:@"Radio_Slider_HighlitedBck_iPhone.png"]
                               stretchableImageWithLeftCapWidth:10.0 topCapHeight:0.0];
            stetchRightTrack = [[UIImage imageNamed:@"Radio_Slider_DefaultBck_iPhone.png"]
                                stretchableImageWithLeftCapWidth:10.0 topCapHeight:0.0];
        }
        [self.volumeSlider setThumbImage:[UIImage imageNamed:@"Radio_Slider_Ball_iPhone.png"] forState:UIControlStateNormal];
        [self.volumeSlider setThumbImage:[UIImage imageNamed:@"Radio_Slider_Ball_iPhone.png"] forState:UIControlStateHighlighted];
        [self.volumeSlider setMinimumTrackImage:stetchRightTrack forState:UIControlStateNormal];
        [self.volumeSlider setMaximumTrackImage:stetchLeftTrack forState:UIControlStateNormal];
    }
    
    [self.volumeSlider setValue:1.0-[GRRadioManager sharedManager].volumeValue animated:YES];
    
    if ([[GRRadioDataManager shared] getAllSavedStations].count == 0)
    {
        GRCommonAlertView *alert = [[[GRCommonAlertView alloc] initCommonAlert:NSLocalizedString(@"Go to settings to select your favourite stations", nil) inView:self.view] autorelease];
        alert.delegate = nil;
    }
}

#pragma mark - Initialization

- (void)dealloc
{
    self.volumeBtn = nil;
    self.savedStations = nil;
    [muteBtn release];
    [rightContentScrollView release];
    [volumeView release];
    [volumeSlider release];
    
    [hideVolumeViewTimer invalidate];
    hideVolumeViewTimer = nil;
    
    [super dealloc];
}

- (BOOL)prefersStatusBarHidden
{
    return YES;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.screenName = @"Radio Station Player Screen";
    
    self.rightContentScrollView.minimumPadding = 20;
    
    [self radioStationsReceivedWithData:nil];
    
    if (![GRRadioManager sharedManager].is3gOn && [[GRRadioManager sharedManager] is3G])
    {
        [[[GRCommonAlertView alloc] initCommonAlert:NSLocalizedString(@"Enable streaming over 3G", @"Enable streaming over 3G") inView:self.view] autorelease];
    }
    else
    {
        [self radioPlayerInit];
    }
    
    UIImage *headerBackground = nil;
    
    if (IS_IPAD)
    {
        headerBackground = [[UIImage imageNamed:@"radio_header_background"] resizableImageWithCapInsets:UIEdgeInsetsMake(0, 40, 0, 40)];
    }
    else
    {
        headerBackground = [[UIImage imageNamed:@"radio_header_background"] stretchableImageWithLeftCapWidth:36 topCapHeight:0];
    }
    
    self.headerView.image = headerBackground;
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self.navigationController setNavigationBarHidden:YES];
    
    if (!IS_IPAD)
        [self willRotateToInterfaceOrientation:[UIApplication sharedApplication].statusBarOrientation duration:0];
    
//    [[UIApplication sharedApplication] beginReceivingRemoteControlEvents];
}

- (void)viewWillDisappear:(BOOL)animated 
{
    [super viewWillDisappear:animated];
    
    if (timer)
    {
        [timer invalidate];
        timer = nil;
    }
}


- (void)viewDidUnload
{
    [super viewDidUnload];
    self.muteBtn = nil;
    // Release any retained subviews of the main view.
}

-(void)willRotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)duration
{
    currentOrientation = toInterfaceOrientation;
    if (!IS_IPAD)
    {
        if( UIInterfaceOrientationIsLandscape(toInterfaceOrientation) )
        {
            [[NSBundle mainBundle] loadNibNamed: [NSString stringWithFormat:@"%@", NSStringFromClass([self class])]
                                          owner: self
                                        options: nil];
            [self viewDidLoad];
        }
        else
        {
            [[NSBundle mainBundle] loadNibNamed: [NSString stringWithFormat:@"%@-portrait", NSStringFromClass([self class])]
                                          owner: self
                                        options: nil];
            [self viewDidLoad];
        }
    }
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    if (IS_IPAD)
        return UIInterfaceOrientationIsLandscape(interfaceOrientation);
    else
        return (interfaceOrientation != UIInterfaceOrientationPortraitUpsideDown);
}


#pragma mark - Actions

- (IBAction)homeBtnTapped:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}


- (IBAction)voulmeBtnTapped:(id)sender
{
    if (isVolumeDisplayed)
    {
        [self hideVolumeView];
    }
    else
    {
        volumeBtn.selected = YES;
        isVolumeDisplayed = YES;
        
        self.volumeView.hidden = NO;
        self.volumeView.alpha = 0;
        
        [UIView animateWithDuration:0.3 animations:^{
            self.volumeView.alpha = 1;
        }];
        
        if (hideVolumeViewTimer)
        {
            [hideVolumeViewTimer invalidate];
            hideVolumeViewTimer = nil;
        }
        
        hideVolumeViewTimer = [NSTimer scheduledTimerWithTimeInterval:8.0 target:self selector:@selector(hideVolumeView) userInfo:nil repeats:NO];
    }
}

- (IBAction) sliderValueChanged:(UISlider *)sender
{
    [GRRadioManager sharedManager].volumeValue = 1.0-[sender value];
    
    [[GRRadioManager sharedManager].radio setVolume:[GRRadioManager sharedManager].volumeValue];
    
    [GRRadioManager sharedManager].mute = NO;
    
    if (hideVolumeViewTimer)
    {
        [hideVolumeViewTimer invalidate];
        hideVolumeViewTimer = nil;
    }
    
    hideVolumeViewTimer = [NSTimer scheduledTimerWithTimeInterval:8.0 target:self selector:@selector(hideVolumeView) userInfo:nil repeats:NO];
}


- (IBAction)muteBtnTapped:(id)sender
{
    muteBtn.selected = YES;
    
    self.mutePopoverView.hidden = NO;
    self.mutePopoverView.alpha = 0;
    
    [UIView animateWithDuration:0.3 animations:^{
        self.mutePopoverView.alpha = 1;
    }];
    
    if (isVolumeDisplayed)
    {
         [self hideVolumeView];
    }
    
    if (IS_IPAD)
    {
        if ([GRRadioManager sharedManager].mute)
        {
            [self.mutePopoverBtn setTitle:NSLocalizedString(@"Unmute sound", nil) forState:UIControlStateNormal];
        }
        else
        {
            [self.mutePopoverBtn setTitle:NSLocalizedString(@"Mute sound", nil) forState:UIControlStateNormal];
        }
    }
}

- (IBAction)onCancel
{
    muteBtn.selected = NO;
    
    [UIView animateWithDuration:0.3 animations:^{
        self.mutePopoverView.alpha = 0;
    } completion:^(BOOL finished) {
        self.mutePopoverView.hidden = YES;
    }];
}

- (IBAction)onMute
{
    if ([GRRadioManager sharedManager].mute)
    {
        [[GRRadioManager sharedManager].radio setVolume:[GRRadioManager sharedManager].volumeValue];

        [GRRadioManager sharedManager].mute = NO;
    }
    else
    {
        [[GRRadioManager sharedManager].radio setVolume:0];
        [GRRadioManager sharedManager].mute = YES;
    }
    
    [self onCancel];
}

- (IBAction)hideVolumeView
{
    volumeBtn.selected = NO;
    isVolumeDisplayed = NO;
    
    [UIView animateWithDuration:0.3 animations:^{
        self.volumeView.alpha = 0;
    } completion:^(BOOL finished) {
        
        self.volumeView.hidden = YES;
        
    }];
    if (hideVolumeViewTimer)
    {
        [hideVolumeViewTimer invalidate];
        hideVolumeViewTimer = nil;
    }
}

#pragma mark - Layout helpers

- (void) reloadStationsWithData:(id)data index:(int)stationIndex
{
    [self.rightContentScrollView reloadData];
}

- (void) updateStationItem:(TRadioStation *)radioItem
{
    [self.rightContentScrollView reloadData];
}

#pragma mark - GRRadioItemDelegate

- (void)radioItemselected:(TRadioStation *)radioItem
{
    if (radioItem)
    {
        if ([GRRadioManager sharedManager].mute)
            [self onMute];

        if (![GRRadioManager sharedManager].isFirstPlaying)
        {
            [GRRadioManager sharedManager].isFirstPlaying = YES;
            [self runPlayerItem:radioItem];
            
            [GRRadioManager sharedManager].currentStationIndex = [savedStations indexOfObject:radioItem];
        }
        else
        {
//            GRRadioItemView *radioItemView = (GRRadioItemView *)[self.rightContentScrollView.subviews objectAtIndex:[GRRadioManager sharedManager].currentStationIndex];
            
//            if (((radioItemView.mode == rimSelected || radioItemView.mode == rimPlay) && [GRRadioManager sharedManager].currentStationIndex != [savedStations indexOfObject:radioItem]) || ([GRRadioManager sharedManager].currentStationIndex == [savedStations indexOfObject:radioItem] && radioItemView.mode == rimSelected))
            
            if ([GRRadioManager sharedManager].currentStationIndex != [savedStations indexOfObject:radioItem])
            {
                [self runPlayerItem:radioItem];
                
                [GRRadioManager sharedManager].currentStationIndex = [savedStations indexOfObject:radioItem];
            }
            else
            {
                [[GRRadioManager sharedManager] stopPlayingRadio];
                
                [GRRadioManager sharedManager].currentStationIndex = -1;
                
                [MBProgressHUD hideAllHUDsForView:self.view animated:NO];
            }
        }
        
        [self updateStationItem:radioItem];
    }
    else
    {
        [self.navigationController popViewControllerAnimated:NO];
        GRSettingsiPadController *contr = [[[GRSettingsiPadController alloc] initWithNibName:@"GRSettingsiPadController" bundle:nil] autorelease];
        contr.selectedIndex = 6;
        [(UINavigationController *)[AppDelegate getInstance].window.rootViewController pushViewController:contr animated:YES];
    }
}


#pragma mark - Player helpers

- (void)runPlayerItem:(TRadioStation *)radioItem
{
    id radioUrl = radioItem.strimUrl;
    
    MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.view animated:NO];
    
    hud.userInteractionEnabled = NO;
    [GRRadioManager sharedManager].radioPlayer = self;
    [[GRRadioManager sharedManager] stopPlayingRadio];
    
    [[GRRadioManager sharedManager] setupRadioStation:radioUrl];
    
    if([GRRadioManager sharedManager].radio)
    {
        [[GRRadioManager sharedManager].radio setDelegate:self];
        [[GRRadioManager sharedManager].radio play];
    }
}

- (void)moviePreloadDidFinish:(id)notification
{
    [self stopTimeOutListening];
}

- (void)moviePlayBackDidFinish:(id)notification
{
    
}

#pragma mark - TimeOut listening

- (void)startTimeOutListening {
    if (!timer) {
        tick = 0;
        timer = [NSTimer scheduledTimerWithTimeInterval:0.1f
                                                 target:self
                                               selector:@selector(listenTimeOut)
                                               userInfo:nil
                                                repeats:YES];
        
    }
    
}

- (void)stopTimeOutListening {
    if (timer) {
        [timer invalidate];
        timer = nil;
        tick = 0;
    }
}

- (void)listenTimeOut
{
    if (tick > 100)
    {
        [self stopTimeOutListening];
        GRCommonAlertView *alert = [[[GRCommonAlertView alloc] initCommonAlert:NSLocalizedString(@"Sorry! Could not connect to station. Try again Later.", nil) inView:self.view] autorelease];
        alert.delegate = nil;
    }
    else
    {
        tick ++;
    }
    NSLog(@"Tracking...");  
}

- (void)displayErrorMessageWithString:(NSString *)error {
   	UIAlertView *alert = [[UIAlertView alloc] initWithTitle: NSLocalizedString(@"Error!", @"Error!")
													message: error
												   delegate: self
										  cancelButtonTitle: NSLocalizedString(@"OK", @"OK")
										  otherButtonTitles: nil];
	[alert show];
	[alert release];
}

#pragma mark GRCommonAlertDelegate

- (void)commonAlert:(id)alertView buttonWithIndex:(NSInteger)index
{
    if (index == 1)
    {
        [GRRadioManager sharedManager].is3gOn = YES;
        [self radioPlayerInit];
    }
    else
    {
        [self.navigationController popViewControllerAnimated:YES];
    }
}

- (BOOL)canBecomeFirstResponder {
    return YES;
}

- (void)remoteControlReceivedWithEvent:(UIEvent *)event {
    switch(event.subtype) {
        case UIEventSubtypeRemoteControlTogglePlayPause:
            if([GRRadioManager sharedManager].radio) {
                if([[GRRadioManager sharedManager].radio isPlaying]) {
                    [[GRRadioManager sharedManager].radio pause];
                } else {
                    [[GRRadioManager sharedManager].radio play];
                }
            }
            break;
        default:
            break;
    }
}

- (void)playButtonTapped
{
    if([GRRadioManager sharedManager].radio == nil)
    {
        return;
    }
    
    if([[GRRadioManager sharedManager].radio isPlaying])
    {
        [[GRRadioManager sharedManager].radio pause];
    }
    else
    {
        [[GRRadioManager sharedManager].radio play];
    }
}

#pragma mark -
#pragma mark YLAudioSessionDelegate

- (void)beginInterruption
{
    if([GRRadioManager sharedManager].radio == nil)
    {
        return;
    }
    
    [[GRRadioManager sharedManager].radio pause];
}

- (void)endInterruptionWithFlags:(NSUInteger)flags {
    if([GRRadioManager sharedManager].radio == nil) {
        return;
    }
    
    if([[GRRadioManager sharedManager].radio isPaused]) {
        [[GRRadioManager sharedManager].radio play];
    }
}

- (void)headphoneUnplugged
{
    if([GRRadioManager sharedManager].radio == nil)
    {
        return;
    }
    
    if([[GRRadioManager sharedManager].radio isPlaying])
    {
        [[GRRadioManager sharedManager].radio pause];
    }
}

#pragma mark -
#pragma mark MMSRadioDelegate Methods

- (void)radioStateChanged:(YLRadio *)radio
{
    YLRadioState state = [[GRRadioManager sharedManager].radio radioState];
    
    if(state == kRadioStateConnecting)
    {

    }
    else if(state == kRadioStateBuffering)
    {
        
    }
    else if(state == kRadioStatePlaying)
    {
        [MBProgressHUD hideAllHUDsForView:self.view animated:NO];
    }
    else if(state == kRadioStateStopped)
    {
        [MBProgressHUD hideAllHUDsForView:self.view animated:NO];
    }
    else if(state == kRadioStateError)
    {
        YLRadioError error = [[GRRadioManager sharedManager].radio radioError];
        
        if(error == kRadioErrorPlaylistMMSStreamDetected)
        {
            // copy url because it will be gone when we release our _radio instance
            NSURL *url = [[[GRRadioManager sharedManager].radio url] copy];
            [GRRadioManager sharedManager].radioPlayer = self;
            [[GRRadioManager sharedManager] stopPlayingRadio];
            
            [[GRRadioManager sharedManager] setupRadioStation:[url absoluteString]];
            
            [url release];
            
            if([GRRadioManager sharedManager].radio)
            {
                [[GRRadioManager sharedManager].radio setDelegate:self];
                [[GRRadioManager sharedManager].radio play];
            }
            
            return;
        }
        
        NSString *errorStr = nil;
        if(error == kRadioErrorAudioQueueBufferCreate)
            errorStr = @"Audio buffers could not be created.";
        else if(error == kRadioErrorAudioQueueCreate)
            errorStr = @"Audio queue could not be created.";
        else if(error == kRadioErrorAudioQueueEnqueue)
            errorStr = @"Audio queue enqueue failed.";
        else if(error == kRadioErrorAudioQueueStart)
            errorStr = @"Audio queue could not be started.";
        else if(error == kRadioErrorFileStreamGetProperty)
            errorStr = @"File stream get property failed.";
        else if(error == kRadioErrorPlaylistParsing)
            errorStr = @"Playlist could not be parsed.";
        else if(error == kRadioErrorDecoding)
            errorStr = @"Audio decoding error.";
        else if(error == kRadioErrorHostNotReachable)
            errorStr = @"Radio host not reachable.";
        else if(error == kRadioErrorNetworkError)
            errorStr = @"No Internet \nconnection";
        else
            errorStr = @"No Internet \nconnection";
        
        [MBProgressHUD hideAllHUDsForView:self.view animated:NO];
        
        [[[GRCommonAlertView alloc] initCommonAlert:errorStr inView:self.view] autorelease];
        
        NSLog(@"%@", radio.url);
    }
}

#pragma mark - BDGridViewDataSource's methods

- (NSUInteger)gridViewCountOfCells:(BDGridView *)gridView
{
    if ([self.savedStations count] < 9)
    {
        return 9;
    }
    else
    {
        return [self.savedStations count];
    }
}

- (CGSize)gridViewSizeOfCell:(BDGridView *)gridView
{
    if (IS_IPAD)
    {
        return CGSizeMake(313, 200);
    }
    else
    {
        CGFloat width = (gridView.bounds.size.width)/ 3;
        
        return CGSizeMake(width, width);
    }
    
    return CGSizeZero;
}

- (BDGridCell *)gridView:(BDGridView *)gridView cellForIndex:(NSUInteger)index
{    
    GRRadioItemView *itemView = (GRRadioItemView*)[gridView dequeueCell];
    
    if (!itemView)
    {
        itemView = (GRRadioItemView *)[GRResource viewFromNib:@"GRRadioItemView" owner:nil];
    }
    
    itemView.delegate = self;
        
    if (index < self.savedStations.count)
    {
        TRadioStation *station = [self.savedStations objectAtIndex:index];
        itemView.mode = rimDefualt;
        [itemView updateWithRadioData:station selected:(index == [GRRadioManager sharedManager].currentStationIndex && [GRRadioManager sharedManager].radio)];
    }
    else
    {
        itemView.mode = rimAdd;
        [itemView updateWithRadioData:nil selected:NO];
    }
    
    return itemView;
}

#pragma mark - BDGridViewDelegate's methods

- (void)gridView:(BDGridView *)gridView didTapCell:(BDGridCell *)cell
{
    if (![GRRadioManager sharedManager].is3gOn && [[GRRadioManager sharedManager] is3G])
    {
        [[[GRCommonAlertView alloc] initCommonAlert:NSLocalizedString(@"Enable streaming over 3G", @"Enable streaming over 3G") inView:self.view] autorelease];
     
        [GRRadioManager sharedManager].currentStationIndex = -1;
        
        [self.rightContentScrollView reloadData];
        
        return ;
    }
    
    TRadioStation *station = nil;
    
    if (cell.index < [self.savedStations count])
    {
        station = [self.savedStations objectAtIndex:cell.index];
    }
    
    [self radioItemselected:station];
}

@end
