//
//  GRRadioSettingsStationsController.m
//  GrannyApp
//
//  Created by Ievgen Pavliuk on 8/1/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "GRRadioSettingsStationsController.h"
#import "GRSettingsiPadController.h"
#import "GRRadioSettingsSearchMethodController.h"
#import "GRRadioDataManager.h"
#import "TRadioStation.h"
#import "GRRadioStationEditController.h"
#import "GRRadioManager.h"
#import "STSegmentedControl.h"
#import "GRRadioListStationsController.h"

@interface GRRadioSettingsStationsController ()

@end

@implementation GRRadioSettingsStationsController

@synthesize parentController;

#pragma mark - Initialization


- (void)dealloc
{
    self.addButton = nil;
    self.editButton = nil;
    self.okButton = nil;
    self.onButton = nil;
    self.offButton = nil;
    parentController = nil;
    self.contentTable = nil;
    [tableData release];
    self.streamingTableViewCell = nil;
    self.channelsTableViewCell = nil;
    [[NSNotificationCenter defaultCenter] removeObserver: self ];
    [super dealloc];
}

- (BOOL)prefersStatusBarHidden
{
    return YES;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.screenName = @"Radio Station Settings Screen";
}

- (void)viewWillAppear:(BOOL)animated 
{
    [super viewWillAppear:animated];
    if (!IS_IPAD)
        [self willRotateToInterfaceOrientation:[UIApplication sharedApplication].statusBarOrientation duration:0];
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    self.streamingTableViewCell = nil;
    self.channelsTableViewCell = nil;
    self.onButton = nil;
    self.offButton = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    if (IS_IPAD)
        return UIInterfaceOrientationIsLandscape(interfaceOrientation);
    else
        return (interfaceOrientation != UIInterfaceOrientationPortraitUpsideDown);
}

- (IBAction)segmentButtonsTapped:(UIButton*)button
{
    if (button == self.onButton)
    {
        self.onButton.selected = YES;
        self.offButton.selected = NO;
    }
    else
    {
        self.onButton.selected = NO;
        self.offButton.selected = YES;
    }
    
    [GRRadioManager sharedManager].is3gOn = self.onButton.selected == YES;
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 2;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.row == 0)
    {
        if ([GRRadioManager sharedManager].is3gOn)
        {
            self.onButton.selected = YES;
            self.offButton.selected = NO;
        }
        else
        {
            self.onButton.selected = NO;
            self.offButton.selected = YES;
        }
        
        return self.streamingTableViewCell;
    }
    
    return self.channelsTableViewCell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.row == 0)
    {
        return 91;
    }
    
    return IS_IPAD ? 73.0f : 40.0f;
}


#pragma mark - Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.row == 1)
    {
        GRRadioListStationsController *radioStationsController = [[[GRRadioListStationsController alloc] init] autorelease];
        radioStationsController.view.frame = self.view.frame;
        radioStationsController.parentController = self;
        [self.navigationController pushViewController:radioStationsController animated:YES];
    }
}

- (void)updateHeaderForRadioStationsList
{
    [(GRSettingsiPadController *)self.parentController updateHeaderForRadioStationsList];
}
- (void)updateHeaderForRadioSearchMethod
{
    [(GRSettingsiPadController *)self.parentController updateHeaderForRadioSearchMethod];
}

#pragma mark - Action methods

- (IBAction)homeBtnTapped:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)addRadioBtnTapped:(id)sender
{
    [self onSelectAddSearchMethodController];
}

- (void)onSelectAddSearchMethodController
{
    if (IS_IPAD)
    {
//        [(GRSettingsiPadController *)self.parentController updateHeaderForRadioSearchMethod];
        
        NSString *nibName = @"GRRadioSettingsSearchMethodController_iPad";
        
        GRRadioSettingsSearchMethodController *searchMethodController = [[[GRRadioSettingsSearchMethodController alloc] initWithNibName:nibName bundle:nil] autorelease];
        searchMethodController.view.frame = self.view.frame;
        [self.navigationController pushViewController:searchMethodController animated:YES];
    }
    else
    {
        GRRadioSettingsSearchMethodController *searchMethodController = [[[GRRadioSettingsSearchMethodController alloc] initWithNibName:@"GRRadioSettingsSearchMethodController" bundle:nil] autorelease];
        [self.navigationController pushViewController:searchMethodController animated:NO];
    }
}

@end
