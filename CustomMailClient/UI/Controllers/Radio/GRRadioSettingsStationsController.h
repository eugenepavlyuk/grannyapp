//
//  GRRadioSettingsStationsController.h
//  GrannyApp
//
//  Created by Ievgen Pavliuk on 8/1/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "GRRadioStationEditController.h"

@interface GRRadioSettingsStationsController : GAITrackedViewController <UITableViewDelegate, UITableViewDataSource>
{
    NSMutableArray *tableData;
    
    id parentController;
    
    NSInteger selectedStationIndex;
    GRRadioStationEditController *editController;
}

@property (nonatomic, retain) IBOutlet UITableView *contentTable;
@property (nonatomic, assign) id parentController;
@property (nonatomic, retain) IBOutlet UIButton *addButton;
@property (nonatomic, retain) IBOutlet UIButton *okButton;
@property (nonatomic, retain) IBOutlet UIButton *editButton;

@property (nonatomic, retain) IBOutlet UITableViewCell *streamingTableViewCell;
@property (nonatomic, retain) IBOutlet UITableViewCell *channelsTableViewCell;

@property (nonatomic, retain) IBOutlet UIButton *onButton;
@property (nonatomic, retain) IBOutlet UIButton *offButton;

- (IBAction)homeBtnTapped:(id)sender;
- (IBAction)addRadioBtnTapped:(id)sender;
- (IBAction)segmentButtonsTapped:(UIButton*)button;

- (void)updateHeaderForRadioStationsList;
- (void)updateHeaderForRadioSearchMethod;

@end
