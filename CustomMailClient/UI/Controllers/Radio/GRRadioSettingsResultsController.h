//
//  GRRadioSettingsResultsController.h
//  GrannyApp
//
//  Created by Ievgen Pavliuk on 8/1/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "GRCommonAlertView.h"

@interface GRRadioSettingsResultsController : GAITrackedViewController <UITableViewDelegate, UITableViewDataSource, GRCommonAlertDelegate> {
    
    UITableView     *contentTable;
     
    NSMutableArray  *tableData;
    NSMutableArray  *genres;
    
    NSInteger        genresCount; 
    NSInteger        transactionCount;
    
    BOOL             isLoaded;
}

@property (nonatomic, retain) IBOutlet UITableView *contentTable;

@property (nonatomic, copy) NSMutableArray *tableData;

- (IBAction)homeBtnTapped:(id)sender;

@end
