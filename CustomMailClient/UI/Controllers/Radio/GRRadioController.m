//
//  GRRadioController.m
//  Granny
//
//  Created by Ievgen Pavliuk on 3/27/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "GRRadioController.h"

@interface GRRadioController ()

@end

@implementation GRRadioController
@synthesize radioObj;
@synthesize loadingView;
@synthesize lRadioTitle;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (BOOL)prefersStatusBarHidden
{
    return YES;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.screenName = @"Radio Screen";
    
    self.navigationItem.title = NSLocalizedString(@"Radio", @"Radio");
    self.lRadioTitle.text = [self.radioObj objectForKey:@"RadioTitle"];
    
    player = [[MPMoviePlayerController alloc] initWithContentURL:[NSURL URLWithString:[self.radioObj objectForKey:@"RadioUrl"]]];
    player.movieSourceType = MPMovieSourceTypeStreaming;
    player.view.hidden = YES;
    [[NSNotificationCenter defaultCenter] addObserver:self 
                                             selector:@selector(moviePreloadDidFinish:) 
                                                 name:MPMoviePlayerLoadStateDidChangeNotification 
                                               object:player];
    
    [[NSNotificationCenter defaultCenter] addObserver:self 
                                             selector:@selector(moviePlayBackDidFinish:) 
                                                 name:MPMoviePlayerPlaybackDidFinishNotification 
                                               object:player];

    [self.view addSubview:player.view];
}

- (void)moviePreloadDidFinish:(id)notification {
    self.loadingView.hidden = YES;
}

- (void)moviePlayBackDidFinish:(id)notification {
}
- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [player play];
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return UIInterfaceOrientationIsLandscape(interfaceOrientation);
}

- (void)dealloc {
    [player release];
    [radioObj release];
    [loadingView release];
    [lRadioTitle release];
    [super dealloc];
}

@end
