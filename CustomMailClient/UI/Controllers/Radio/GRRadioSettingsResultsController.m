//
//  GRRadioSettingsResultsController.m
//  GrannyApp
//
//  Created by Ievgen Pavliuk on 8/1/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "GRRadioSettingsResultsController.h"
#import "GRRadioDataManager.h"
#import "GRCommonAlertView.h"
#import "TRadioStation.h"
#import "TVChannel.h"
#import "DataManager.h"
#import "WYPopoverController.h"

@interface GRRadioSettingsResultsController () <WYPopoverControllerDelegate>
{
    int selectedStationIndex;
}

@property (nonatomic, retain) WYPopoverController *alertPopoverController;

@end

@implementation GRRadioSettingsResultsController

@synthesize contentTable;
@synthesize tableData;

#pragma mark - Initialization

- (void)dealloc
{
    [contentTable release];
    [genres release];
    self.tableData = nil;
    
    [self.alertPopoverController dismissPopoverAnimated:YES];
    self.alertPopoverController = nil;
    
    [super dealloc];
}

- (BOOL)prefersStatusBarHidden
{
    return YES;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.screenName = @"Radio Station Settings Search Results Screen";
    
    self.contentTable.backgroundColor = [UIColor clearColor];
    
    genres = [[NSMutableArray alloc] init];
}

- (void)viewWillAppear:(BOOL)animated 
{
    [super viewWillAppear:animated];
    
//    tableData = [[NSMutableArray alloc] init];
//    
//    for (int i = 0; i < 9; i++)
//    {
//        NSMutableDictionary *dict = [NSMutableDictionary dictionary];
//        [dict setObject:[NSString stringWithFormat:@"Radio %d", i+1] forKey:@"name"];
//        
//        [dict setObject:[NSDictionary dictionaryWithObject:@"http://radio.goha.ru:8000/grind.fm" forKey:@"stream_url"] forKey:@"streams"];
//        
//        [dict setObject:[NSString stringWithFormat:@"%d", i+1] forKey:@"sid"];
//        
//        [tableData addObject:dict];
//    }
    [[AppDelegate getInstance].window unBlockUI];
    [self.contentTable reloadData];
    
    if (!IS_IPAD)
        [self willRotateToInterfaceOrientation:[UIApplication sharedApplication].statusBarOrientation duration:0];
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    if (IS_IPAD)
        return UIInterfaceOrientationIsLandscape(interfaceOrientation);
    else
        return (interfaceOrientation != UIInterfaceOrientationPortraitUpsideDown);
}


#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [tableData count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:nil];
    if (cell == nil) {
        cell = [[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier] autorelease];
        
    }
    
    cell.contentView.backgroundColor = [UIColor clearColor];
    cell.selectedBackgroundView = [[UIImageView alloc] initWithImage:[[UIImage imageNamed:@"settings_radio_cell_selected_background"] resizableImageWithCapInsets:UIEdgeInsetsMake(10, 10, 10, 10)]];    
    
    CGFloat cellWidth = self.contentTable.frame.size.width;
    CGFloat cellHeight = [self tableView:tableView heightForRowAtIndexPath:indexPath];
    UIImageView *separator = [[[UIImageView alloc] initWithImage:[UIImage imageNamed:@"Settings_MenuSeparator"]] autorelease];
    separator.frame = CGRectMake(0, cellHeight-2, cellWidth, 2);
    [cell.contentView addSubview:separator];
    
    UIImageView *logoView = [[[UIImageView alloc] initWithImage:[UIImage imageNamed:@"Settings_Radio_Logo"]] autorelease];
    [cell.contentView addSubview:logoView];
    if (IS_IPAD) {
        logoView.frame = CGRectMake(30, cellHeight/2 - logoView.frame.size.height/2, logoView.frame.size.width, logoView.frame.size.height);
    } else {
        logoView.frame = CGRectMake(10, cellHeight/2 - logoView.frame.size.height/4, logoView.frame.size.width/2, logoView.frame.size.height/2);
    }
    
    UILabel *lTitle = [[[UILabel alloc] initWithFrame:CGRectMake(logoView.frame.origin.x + logoView.frame.size.width + 10, 0, cellWidth, cellHeight)] autorelease];
    lTitle.backgroundColor = [UIColor clearColor];
    lTitle.textColor = RGBCOLOR(96, 96, 96);
    lTitle.font = [UIFont fontWithName:@"Helvetica-Bold" size:IS_IPAD ? 18 : 12];
//    if (IS_IPAD)
//    {
//        lTitle.font = [UIFont fontWithName:@"Helvetica-Bold" size:12];
//        lTitle.frame = CGRectMake(54, 0, cellWidth-100, cellHeight);
//    }
    [cell.contentView addSubview:lTitle];
    
    TVChannel *radio = [tableData objectAtIndex:[indexPath row]];
    id name = radio.channelTitle;//[radio objectForKey:@"name"];
    
    if (name != [NSNull null])
    {
        lTitle.text = (NSString *)name;
    }
    else
    {
        lTitle.text = @"Uknown Station";
    }
    
    id sid = radio.channelID;//[radio objectForKey:@"sid"];
    if (sid != [NSNull null])
    {
        if (![[GRRadioDataManager shared] isStationAxists:(NSString *)sid])
        {
            UIImage *image = [UIImage imageNamed:@"common_blue_plus_btn"];
            UIButton *bAdd = [UIButton buttonWithType:UIButtonTypeCustom];
            [bAdd setImage:image forState:UIControlStateNormal];
            [bAdd setImage:image forState:UIControlStateHighlighted];
            if (IS_IPAD)
            {
                bAdd.frame = CGRectMake(cellWidth - 57 - 10, 9, 75, 50);
            }
            else
            {
                bAdd.frame = CGRectMake(cellWidth - 24 - 10, 4, 37, 25);
            }
            bAdd.tag = [indexPath row];
            [bAdd addTarget:self action:@selector(addRadioStationButtonTapped:) forControlEvents:UIControlEventTouchUpInside];
            [cell.contentView addSubview:bAdd];
        }
        else
        {
            UIImage *image = [UIImage imageNamed:@"Settings_Radio_DeleteBtn"];
            UIButton *bRemove = [UIButton buttonWithType:UIButtonTypeCustom];
            [bRemove setImage:image forState:UIControlStateNormal];
            [bRemove setImage:[UIImage imageNamed:@"Settings_Radio_DeleteBtnOn"] forState:UIControlStateHighlighted];
            if (IS_IPAD)
            {
                bRemove.frame = CGRectMake(cellWidth - 44 - 10, 9, 47, 47);
            }
            else
            {
                bRemove.frame = CGRectMake(cellWidth - 24 - 10, 4, 24, 24);
            }
            bRemove.tag = [indexPath row];
            [bRemove addTarget:self action:@selector(deleteBtnPressed:) forControlEvents:UIControlEventTouchUpInside];
            [cell.contentView addSubview:bRemove];
        }
    }
    
    
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    CGFloat height = 0.0f;
    if (IS_IPAD) {
        height = 70.0f;
    } else {
        height = 35.0f;
    }
    return height;
}


#pragma mark - Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
}

#pragma mark - Action methods

- (IBAction)homeBtnTapped:(id)sender
{
    [self.navigationController popViewControllerAnimated:NO];
}

- (void)addRadioStationButtonTapped:(id)sender
{
//    if ([[GRRadioDataManager shared] getAllSavedStations].count < 9)
//    {
        UIButton *b = (UIButton*)sender;
        NSInteger selectedIndex = b.tag;
        
        TVChannel *radioStation = [tableData objectAtIndex:selectedIndex];
    
        TRadioStation *messageModel = (TRadioStation *)[[DataManager sharedInstance] createEntityWithName:@"TRadioStation"];
    
        messageModel.stationID = radioStation.channelID;
        messageModel.title = radioStation.channelTitle;
        messageModel.strimUrl = radioStation.channelUrl;
        [[DataManager sharedInstance] save];
    
    
//        [[GRRadioDataManager shared] saveRadioStation:radioStation];
        [self.contentTable reloadData];
//    }
//    else
//    {
//        GRCommonAlertView *alert = [[[GRCommonAlertView alloc] initCommonAlert:NSLocalizedString(@"You can add only up to 9 stations", nil) inView:self.view] autorelease];
//        alert.delegate = nil;
//    }
}
- (void)deleteBtnPressed:(id)sender {
    selectedStationIndex = ((UIButton *) sender).tag;
    
    WYPopoverBackgroundView *popoverAppearance = [WYPopoverBackgroundView appearance];
    
    [popoverAppearance setOverlayColor:[UIColor colorWithWhite:0.1f alpha:0.6f]];
    [popoverAppearance setOuterCornerRadius:0];
    [popoverAppearance setOuterShadowBlurRadius:0];
    [popoverAppearance setOuterShadowColor:[UIColor clearColor]];
    [popoverAppearance setOuterShadowOffset:CGSizeZero];
    
    [popoverAppearance setGlossShadowColor:[UIColor clearColor]];
    [popoverAppearance setGlossShadowOffset:CGSizeMake(0, 0)];
    
    [popoverAppearance setBorderWidth:1];
    [popoverAppearance setArrowHeight:0];
    [popoverAppearance setArrowBase:0];
    
    [popoverAppearance setInnerCornerRadius:0];
    [popoverAppearance setInnerShadowBlurRadius:0];
    [popoverAppearance setInnerShadowColor:[UIColor clearColor]];
    [popoverAppearance setInnerShadowOffset:CGSizeMake(0, 0)];
    
    [popoverAppearance setFillTopColor:[UIColor whiteColor]];
    [popoverAppearance setFillBottomColor:[UIColor whiteColor]];
    [popoverAppearance setOuterStrokeColor:[UIColor clearColor]];
    [popoverAppearance setInnerStrokeColor:[UIColor clearColor]];
    
    UIViewController *contentViewController = [[[UIViewController alloc] init] autorelease];
    contentViewController.contentSizeForViewInPopover = CGSizeMake(427, 180);
    
    UIView *deleteAccountAlertView = [[[NSBundle mainBundle] loadNibNamed:@"DeleteRadioAlertView" owner:self options:nil] lastObject];
    
    [[contentViewController view] addSubview:deleteAccountAlertView];
    
    
    self.alertPopoverController = [[[WYPopoverController alloc] initWithContentViewController:contentViewController] autorelease];
    self.alertPopoverController.delegate = self;
    
    [self.alertPopoverController presentPopoverAsDialogAnimated:YES];
}

- (IBAction)deleteStationCancelButtonTapped
{
    [self.alertPopoverController dismissPopoverAnimated:YES];
    self.alertPopoverController = nil;
}

- (IBAction)deleteStationDeleteButtonTapped
{
    [self.alertPopoverController dismissPopoverAnimated:YES];
    self.alertPopoverController = nil;
    
    TVChannel *station = [tableData objectAtIndex:selectedStationIndex];
    [[GRRadioDataManager shared] deleteRaioStationByID:station.channelID];
    [contentTable reloadData];
}

#pragma mark - GRCommonAlertDelegate

- (void)commonAlert:(id)alertView buttonWithIndex:(NSInteger)index
{
    if (index == 1)
    {
        TVChannel *station = [tableData objectAtIndex:selectedStationIndex];
        [[GRRadioDataManager shared] deleteRaioStationByID:station.channelID];
        [contentTable reloadData];
    }
}

#pragma mark - WYPopoverControllerDelegate's methods

- (BOOL)popoverControllerShouldDismissPopover:(WYPopoverController *)popoverController
{
    return YES;
}

- (void)popoverControllerDidDismissPopover:(WYPopoverController *)popoverController
{
    self.alertPopoverController = nil;
}

@end
