//
//  GRRadioStationEditController.m
//  GrannyApp
//
//  Created by Ievgen Pavliuk on 8/2/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "GRRadioStationEditController.h"
#import "GRRadioDataManager.h"
#import "GRAlertViewController.h"
#import "WYPopoverController.h"

@interface GRRadioStationEditController () <WYPopoverControllerDelegate>

@property (nonatomic, retain) WYPopoverController *alertPopoverController;

@end

@implementation GRRadioStationEditController

@synthesize tUrl;
@synthesize tName;
@synthesize station;
@synthesize lTitle;
@synthesize delegate;
@synthesize backView;
@synthesize logoBtn;
@synthesize radioImagePopover;

#pragma mark - Initialization

- (void)dealloc {
    delegate = nil;
    [station release];
    [tUrl release];
    [tName release];
    [lTitle release];
    [backView release];
    [logoBtn release];
    [radioImagePopover release];
    
    [self.alertPopoverController dismissPopoverAnimated:YES];
    self.alertPopoverController = nil;
    
    [super dealloc];
}

- (void)setupAlertAppearance
{
    WYPopoverBackgroundView *popoverAppearance = [WYPopoverBackgroundView appearance];
    
    [popoverAppearance setOverlayColor:[UIColor colorWithWhite:0.1f alpha:0.6f]];
    [popoverAppearance setOuterCornerRadius:0];
    [popoverAppearance setOuterShadowBlurRadius:0];
    [popoverAppearance setOuterShadowColor:[UIColor clearColor]];
    [popoverAppearance setOuterShadowOffset:CGSizeMake(0, 0)];
    
    [popoverAppearance setGlossShadowColor:[UIColor clearColor]];
    [popoverAppearance setGlossShadowOffset:CGSizeMake(0, 0)];
    
    [popoverAppearance setBorderWidth:0];
    [popoverAppearance setArrowHeight:0];
    [popoverAppearance setArrowBase:0];
    
    [popoverAppearance setInnerCornerRadius:0];
    [popoverAppearance setInnerShadowBlurRadius:0];
    [popoverAppearance setInnerShadowColor:[UIColor clearColor]];
    [popoverAppearance setInnerShadowOffset:CGSizeMake(0, 0)];
    
    [popoverAppearance setFillTopColor:[UIColor whiteColor]];
    [popoverAppearance setFillBottomColor:[UIColor whiteColor]];
    [popoverAppearance setOuterStrokeColor:[UIColor clearColor]];
    [popoverAppearance setInnerStrokeColor:[UIColor clearColor]];
}

- (BOOL)prefersStatusBarHidden
{
    return YES;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.screenName = @"Radio Station Edit Screen";
    
    if (self.station) {
        self.lTitle.text = station.title;
        self.tName.text = station.title;
        self.tUrl.text = station.strimUrl;
        if (station.logoPath) {
            ALAssetsLibrary *library = [[ALAssetsLibrary alloc] init];

            [library assetForURL:[NSURL URLWithString:station.logoPath]
                     resultBlock:^(ALAsset *asset) {
                         if (asset) {
                             UIImage *pImage = nil;
                             
                             if ([[[UIDevice currentDevice] systemVersion] floatValue] >= 5.f)
                             {
                                 pImage = [UIImage imageWithCGImage:[asset aspectRatioThumbnail]];
                             }
                             else
                             {
                                 pImage = [UIImage imageWithCGImage:[asset thumbnail]];
                             }
                             
                             if (pImage) {
                                 NSLog(@"Image found");
                                 [self.logoBtn setImage:pImage forState:UIControlStateNormal];
                             }
                             
                         }
                     }
                     failureBlock:^(NSError *error) {
                              NSLog(@"error couldn't get photo");
                    }];
        }
    } else {
        self.lTitle.text = @"Add new station";
    }
    //self.backView.layer.cornerRadius = 10.0f;
    self.radioImagePopover.delegate = self;
    logoImagePath = [[NSString alloc] init];
    
    
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    if (!IS_IPAD)
        [self willRotateToInterfaceOrientation:[UIApplication sharedApplication].statusBarOrientation duration:0];
    isPopoverDisplay = NO;
    self.radioImagePopover.hidden = YES;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    if (IS_IPAD)
        return UIInterfaceOrientationIsLandscape(interfaceOrientation);
    else
        return (interfaceOrientation != UIInterfaceOrientationPortraitUpsideDown);
}

#pragma mark - Actions methods

- (void)popoverButtonTapped:(UIButton*)button
{
    [self.alertPopoverController dismissPopoverAnimated:YES];
    self.alertPopoverController = nil;
    [self cancelBtnPressed:nil];
}

- (IBAction)okEditBtnPressed:(id)sender
{
    [self setupAlertAppearance];
    
    GRAlertViewController *contentViewController = [[[GRAlertViewController alloc] init] autorelease];
    contentViewController.contentSizeForViewInPopover = CGSizeMake(325, 175);
    
    [contentViewController view];
    
    contentViewController.messageLabel.font = [UIFont fontWithName:@"Helvetica" size:18];
    contentViewController.messageLabel.text = NSLocalizedString(@"The station is successfully updated", @"The station is successfully updated");
    
    [contentViewController.okButton addTarget:self action:@selector(popoverButtonTapped:) forControlEvents:UIControlEventTouchUpInside];
    
    [contentViewController.okButton setBackgroundImage:nil forState:UIControlStateNormal];
    contentViewController.okButton.frame = CGRectMake(0, contentViewController.okButton.frame.origin.y + 30, 50, 28);
    contentViewController.okButton.center = CGPointMake(contentViewController.view.bounds.size.width / 2, contentViewController.okButton.center.y);
    contentViewController.okButton.layer.borderWidth = 1.f;
    contentViewController.okButton.layer.borderColor = RGBCOLOR(190, 190, 190).CGColor;
    [contentViewController.okButton setTitleColor:RGBCOLOR(96, 96, 96) forState:UIControlStateNormal];
    contentViewController.okButton.titleLabel.font = [UIFont fontWithName:@"Helvetica" size:14];
    
    
    
    self.alertPopoverController = [[[WYPopoverController alloc] initWithContentViewController:contentViewController] autorelease];
    self.alertPopoverController.delegate = self;
    
    [self.alertPopoverController presentPopoverAsDialogAnimated:YES];
    
    NSString *stationID = self.station.stationID;
    if (!stationID) {
        stationID = [NSString stringWithFormat:@"%f", [[NSDate date] timeIntervalSince1970]];
    }
    
    NSArray *keys = [NSArray arrayWithObjects:@"title", @"url", @"sid", @"logoPath", nil];
    NSArray *objects = [NSArray arrayWithObjects:self.tName.text, self.tUrl.text, stationID, logoImagePath, nil];
    NSDictionary *dict = [NSDictionary dictionaryWithObjects:objects forKeys:keys];
    [[GRRadioDataManager shared] updateRadioStation:dict];
    NSNotificationCenter *notificationCenter = [NSNotificationCenter defaultCenter];
    [notificationCenter postNotificationName:@"stationUpdated"
                                      object:nil
                                    userInfo:nil];
    [self.tName resignFirstResponder];
    [self.tUrl resignFirstResponder];

}

- (IBAction)cancelBtnPressed:(id)sender
{
    if (IS_IPAD)
        [self.view removeFromSuperview];
    else
        [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)logoBtnPressed:(id)sender {
    if (!isPopoverDisplay) {
        self.radioImagePopover.hidden = NO;
        isPopoverDisplay = YES;
    } else {
        self.radioImagePopover.hidden = YES;
        isPopoverDisplay = NO;
    }
}



#pragma mark - UITextFieldDelegate

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    [textField resignFirstResponder];
    return YES;
}

- (void)textFieldDidBeginEditing:(UITextField *)textField {
}

- (void)textFieldDidEndEditing:(UITextField *)textField {
}

#pragma mark - GRCommonAlertDelegate

- (void)commonAlert:(id)alertView buttonWithIndex:(NSInteger)index
{
    [self cancelBtnPressed:nil];
}

#pragma mark - GRRadioImagePopoverDelegate

- (void)imageSelectedWithPath:(NSString *)path image:(UIImage *)image {
    isPopoverDisplay = NO;
    if (logoImagePath) {
        logoImagePath = nil;
    }
    logoImagePath = [path retain];
    [self.logoBtn setBackgroundImage:image forState:UIControlStateNormal];
}

#pragma mark - WYPopoverControllerDelegate's methods

- (BOOL)popoverControllerShouldDismissPopover:(WYPopoverController *)popoverController
{
    return YES;
}

- (void)popoverControllerDidDismissPopover:(WYPopoverController *)popoverController
{
    self.alertPopoverController = nil;
}

@end
