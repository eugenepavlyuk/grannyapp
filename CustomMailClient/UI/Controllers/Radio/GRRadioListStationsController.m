//
//  GRRadioSettingsStationsController.m
//  GrannyApp
//
//  Created by Ievgen Pavliuk on 8/1/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "GRRadioListStationsController.h"
#import "GRSettingsiPadController.h"
#import "GRRadioSettingsSearchMethodController.h"
#import "GRRadioDataManager.h"
#import "TRadioStation.h"
#import "GRRadioStationEditController.h"
#import "GRRadioManager.h"
#import "STSegmentedControl.h"
#import "GRRadioSettingsStationsController.h"
#import "WYPopoverController.h"

@interface GRRadioListStationsController () <WYPopoverControllerDelegate>

@property (nonatomic, retain) WYPopoverController *alertPopoverController;

@end

@implementation GRRadioListStationsController

@synthesize contentTable;
@synthesize parentController;
@synthesize addButton;
@synthesize okButton;
@synthesize editButton;
@synthesize backView;

#pragma mark - update data

- (void)updateData
{
    if (tableData)
    {
        [tableData release];
        tableData = nil;
    }
    NSArray *stations = [[GRRadioDataManager shared] getAllSavedStations];
    tableData = [[NSMutableArray alloc] initWithArray:stations];
    [self.contentTable reloadData];
    
    if (!IS_IPAD)
    {
        if (stations.count > 0)
        {
            editButton.hidden = NO;
        }
        else
        {
            editButton.hidden = YES;
        }
        addButton.hidden = NO;
    }
}


#pragma mark - Initialization

- (void)dealloc
{
    self.addButton = nil;
    self.editButton = nil;
    self.okButton = nil;
    parentController = nil;
    [contentTable release];
    [backView release];
    [tableData release];
    [library release];
    [[NSNotificationCenter defaultCenter] removeObserver: self ];
    
    [self.alertPopoverController dismissPopoverAnimated:YES];
    self.alertPopoverController = nil;
    
    [super dealloc];
}

- (BOOL)prefersStatusBarHidden
{
    return YES;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.screenName = @"Radio List Stations Screen";
    
    self.contentTable.backgroundColor = [UIColor clearColor];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(stationUpdated:)
                                                 name:@"stationUpdated"
                                               object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(displayNewStationController:)
                                                 name:@"newStationController"
                                               object:nil];
    
    library = [[ALAssetsLibrary alloc] init];

    [self updateData];
}

- (void)viewWillAppear:(BOOL)animated 
{
    [super viewWillAppear:animated];
    [(GRRadioSettingsStationsController *)self.parentController updateHeaderForRadioStationsList];
    [self updateData];
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    if (IS_IPAD)
        return UIInterfaceOrientationIsLandscape(interfaceOrientation);
    else
        return (interfaceOrientation != UIInterfaceOrientationPortraitUpsideDown);
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [tableData count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:nil];
    if (cell == nil) {
        cell = [[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier] autorelease];
        
    }
    
    cell.contentView.backgroundColor = [UIColor clearColor];
    cell.selectedBackgroundView = [[UIImageView alloc] initWithImage:[[UIImage imageNamed:@"settings_radio_cell_selected_background"] resizableImageWithCapInsets:UIEdgeInsetsMake(10, 10, 10, 10)]];
    
    CGFloat cellWidth = self.contentTable.frame.size.width;
    CGFloat cellHeight = [self tableView:tableView heightForRowAtIndexPath:indexPath];
    UIImageView *separator = [[[UIImageView alloc] initWithImage:[UIImage imageNamed:@"Settings_MenuSeparator.png"]] autorelease];
    separator.frame = CGRectMake(0, cellHeight-2, cellWidth, 2);
    [cell.contentView addSubview:separator];
    
    UIImageView *logoView = [[[UIImageView alloc] initWithImage:[UIImage imageNamed:@"Settings_Radio_Logo"]] autorelease];
    if (IS_IPAD) {
        logoView.frame = CGRectMake(30, cellHeight/2 - logoView.frame.size.height/2, logoView.frame.size.width, logoView.frame.size.height);
    } else {
        logoView.frame = CGRectMake(15, cellHeight/2 - logoView.frame.size.height/4, logoView.frame.size.width/2, logoView.frame.size.height/2);
    }
    [cell.contentView addSubview:logoView];
    
    UILabel *lTitle = [[[UILabel alloc] initWithFrame:CGRectMake(logoView.frame.origin.x + logoView.frame.size.width + 10, 0, cellWidth, cellHeight)] autorelease];
    lTitle.backgroundColor = [UIColor clearColor];
    lTitle.font = [UIFont fontWithName:@"Helvetica-Bold" size:IS_IPAD ? 18 : 12];
    [cell.contentView addSubview:lTitle];
    lTitle.textColor = RGBCOLOR(96, 96, 96);
    TRadioStation *station = [tableData objectAtIndex:indexPath.row];
    
    if (station.logoPath) {
        NSURL *url = [NSURL URLWithString:station.logoPath];
        [self setupImageForLogoUrl:url imageView:logoView];
    }
    
    lTitle.text = station.title;
    
    if (isEditOn)
    {
        UIButton *bDelete = [UIButton buttonWithType:UIButtonTypeCustom];
        [bDelete setImage:[UIImage imageNamed:@"Settings_Radio_DeleteBtn"] forState:UIControlStateNormal];
        [bDelete setImage:[UIImage imageNamed:@"Settings_Radio_DeleteBtnOn"] forState:UIControlStateHighlighted];
        [bDelete addTarget:self action:@selector(deleteBtnPressed:) forControlEvents:UIControlEventTouchUpInside];
        if (IS_IPAD) {
            bDelete.frame = CGRectMake(680, 9, 47, 47);
        } else {
            bDelete.frame = CGRectMake(cellWidth - 24 - 10, 4, 24, 24);
        }
        bDelete.tag = indexPath.row;
        [cell.contentView addSubview:bDelete];
    }
    
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return IS_IPAD ? 70.0f : 40.0f;
}


#pragma mark - Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    if (isEditOn)
    {
        if (IS_IPAD)
        {
            if (editController) {
                [editController release];
                editController = nil;
            }
            TRadioStation *station = [tableData objectAtIndex:[indexPath row]];
            editController = [[GRRadioStationEditController alloc] init];
            editController.station = station;
            editController.view.frame = (((UIViewController *)((GRRadioSettingsStationsController *)parentController).parentController)).view.frame;
            editController.view.backgroundColor = [[UIColor blackColor] colorWithAlphaComponent:0.7f];
            [(((UIViewController *)((GRRadioSettingsStationsController *)parentController).parentController)).view addSubview:editController.view];
        }
        else
        {
            NSString *nibName = @"GRRadioStationEditController";
            TRadioStation *station = [tableData objectAtIndex:[indexPath row]];
            GRRadioStationEditController *contr = [[[GRRadioStationEditController alloc] initWithNibName:nibName bundle:nil] autorelease];
            contr.station = station;
            [self.navigationController pushViewController:contr animated:YES];
        }
    }
}


#pragma mark - Action methods
- (IBAction)homeBtnTapped:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}
- (IBAction)editRadioBtnTapped:(id)sender
{
    [self editBtnPressed];
}

- (IBAction)okRadioBtnTapped:(id)sender
{
    [self okBtnPressed];
}

- (IBAction)addRadioBtnTapped:(id)sender
{
    [self onSelectAddSearchMethodController];
}

- (void)onSelectAddSearchMethodController
{
    GRRadioSettingsSearchMethodController *searchMethodController = [[[GRRadioSettingsSearchMethodController alloc] init] autorelease];
    searchMethodController.view.frame = self.view.frame;
    [self.navigationController pushViewController:searchMethodController animated:YES];
}


- (void)editBtnPressed
{
    if (!IS_IPAD)
    {
        addButton.hidden = YES;
        okButton.hidden = NO;
    }
    isEditOn = YES;
    [self.contentTable reloadData];
}

- (void)okBtnPressed
{
    if (!IS_IPAD)
    {
        addButton.hidden = NO;
        okButton.hidden = YES;
    }
    isEditOn = NO;
    [self.contentTable reloadData];
}

- (void)deleteBtnPressed:(id)sender {
    selectedStationIndex = ((UIButton *) sender).tag;
    
    WYPopoverBackgroundView *popoverAppearance = [WYPopoverBackgroundView appearance];
    
    [popoverAppearance setOverlayColor:[UIColor colorWithWhite:0.1f alpha:0.6f]];
    [popoverAppearance setOuterCornerRadius:0];
    [popoverAppearance setOuterShadowBlurRadius:0];
    [popoverAppearance setOuterShadowColor:[UIColor clearColor]];
    [popoverAppearance setOuterShadowOffset:CGSizeZero];
    
    [popoverAppearance setGlossShadowColor:[UIColor clearColor]];
    [popoverAppearance setGlossShadowOffset:CGSizeMake(0, 0)];
    
    [popoverAppearance setBorderWidth:1];
    [popoverAppearance setArrowHeight:0];
    [popoverAppearance setArrowBase:0];
    
    [popoverAppearance setInnerCornerRadius:0];
    [popoverAppearance setInnerShadowBlurRadius:0];
    [popoverAppearance setInnerShadowColor:[UIColor clearColor]];
    [popoverAppearance setInnerShadowOffset:CGSizeMake(0, 0)];
    
    [popoverAppearance setFillTopColor:[UIColor whiteColor]];
    [popoverAppearance setFillBottomColor:[UIColor whiteColor]];
    [popoverAppearance setOuterStrokeColor:[UIColor clearColor]];
    [popoverAppearance setInnerStrokeColor:[UIColor clearColor]];
    
    UIViewController *contentViewController = [[[UIViewController alloc] init] autorelease];
    contentViewController.contentSizeForViewInPopover = CGSizeMake(427, 180);
    
    UIView *deleteAccountAlertView = [[[NSBundle mainBundle] loadNibNamed:@"DeleteRadioAlertView" owner:self options:nil] lastObject];
    
    [[contentViewController view] addSubview:deleteAccountAlertView];
    
    
    self.alertPopoverController = [[[WYPopoverController alloc] initWithContentViewController:contentViewController] autorelease];
    self.alertPopoverController.delegate = self;
    
    [self.alertPopoverController presentPopoverAsDialogAnimated:YES];
}

- (IBAction)deleteStationCancelButtonTapped
{
    [self.alertPopoverController dismissPopoverAnimated:YES];
    self.alertPopoverController = nil;
}

- (IBAction)deleteStationDeleteButtonTapped
{
    [self.alertPopoverController dismissPopoverAnimated:YES];
    self.alertPopoverController = nil;
    
    TRadioStation *station = [tableData objectAtIndex:selectedStationIndex];
    [[GRRadioDataManager shared] deleteRaioStationByID:station.stationID];
    [self updateData];
    [self okBtnPressed];
}

- (void)stationUpdated:(id)notification {
    [self updateData];
}

- (void)displayNewStationController:(id)notification
{
    if (IS_IPAD)
    {
         [(GRRadioSettingsStationsController *)self.parentController updateHeaderForRadioSearchMethod];

        if (editController) {
            [editController release];
            editController = nil;
        }
        editController = [[GRRadioStationEditController alloc] init];
        editController.view.frame = (((UIViewController *)((GRRadioSettingsStationsController *)parentController).parentController)).view.frame;
        editController.view.backgroundColor = [[UIColor blackColor] colorWithAlphaComponent:0.7f];
        [(((UIViewController *)((GRRadioSettingsStationsController *)parentController).parentController)).view addSubview:editController.view];
    }
    else
    {
        NSString *nibName = @"GRRadioStationEditController";

        GRRadioStationEditController *contr = [[[GRRadioStationEditController alloc] initWithNibName:nibName bundle:nil] autorelease];
        [self.navigationController pushViewController:contr animated:NO];
    }
}

#pragma mark - GRCommonAlertDelegate

- (void)commonAlert:(id)alertView buttonWithIndex:(NSInteger)index
{
    [self.alertPopoverController dismissPopoverAnimated:YES];
    self.alertPopoverController = nil;
    
    if (index == 1)
    {
        TRadioStation *station = [tableData objectAtIndex:selectedStationIndex];
        [[GRRadioDataManager shared] deleteRaioStationByID:station.stationID];
        [self updateData];
        [self okBtnPressed];
    }
}


#pragma mark - Image assets helpers

- (void)setupImageForLogoUrl:(NSURL *)url imageView:(UIImageView *)logoImageView {
    [library assetForURL:url
             resultBlock:^(ALAsset *asset) {
                 if (asset) {
                     UIImage *pImage = nil;
                     
                     if ([[[UIDevice currentDevice] systemVersion] floatValue] >= 5.f)
                     {
                         pImage = [UIImage imageWithCGImage:[asset aspectRatioThumbnail]];
                     }
                     else
                     {
                         pImage = [UIImage imageWithCGImage:[asset thumbnail]];
                     }
                     
                     if (pImage) {
                         NSLog(@"Image found");
                         [logoImageView setImage:pImage];
                     }
                     
                 }
             }
            failureBlock:^(NSError *error) {
                NSLog(@"error couldn't get photo");
            }];

}

#pragma mark - WYPopoverControllerDelegate's methods

- (BOOL)popoverControllerShouldDismissPopover:(WYPopoverController *)popoverController
{
    return YES;
}

- (void)popoverControllerDidDismissPopover:(WYPopoverController *)popoverController
{
    self.alertPopoverController = nil;
}

@end
