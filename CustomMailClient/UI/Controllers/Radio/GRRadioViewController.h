//
//  GRRadioPlayerController.h
//  GrannyApp
//
//  Created by Ievgen Pavliuk on 7/17/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "GRRadioItemView.h"
#import "TRadioStation.h"
#import "GRCommonAlertView.h"
#import "RadioTunes.h"
#import "BDGridView.h"

@interface GRRadioViewController : GAITrackedViewController <GRRadioItemDelegate, GRCommonAlertDelegate, YLRadioDelegate,YLAudioSessionDelegate, BDGridViewDataSource, BDGridViewDelegate> {
    
    BDGridView      *rightContentScrollView;
    UIView          *volumeView;
    UISlider        *volumeSlider;
    
    NSTimer    *timer;
    NSInteger   tick; 

    BOOL isVolumeDisplayed;
}

@property (nonatomic ,retain) IBOutlet UIView          *volumeView;
@property (nonatomic ,retain) IBOutlet BDGridView    *rightContentScrollView;
@property (nonatomic ,retain) IBOutlet UISlider        *volumeSlider;
@property (nonatomic ,retain) IBOutlet UIButton        *muteBtn;
@property (nonatomic ,retain) IBOutlet UIButton        *mutePopoverBtn;
@property (nonatomic ,retain) IBOutlet UIButton        *volumeBtn;
@property (nonatomic ,retain) IBOutlet UIImageView     *headerView;
@property (nonatomic ,retain) IBOutlet UIView          *mutePopoverView;

@property (nonatomic, retain) NSString *genreID;

- (IBAction)voulmeBtnTapped:(id)sender;
- (IBAction)homeBtnTapped:(id)sender;
- (IBAction)sliderValueChanged:(UISlider *)sender;
- (IBAction)muteBtnTapped:(id)sender;

- (void)runPlayerItem:(TRadioStation *)radioItem;
- (void)startTimeOutListening;
- (void)stopTimeOutListening;

@end
