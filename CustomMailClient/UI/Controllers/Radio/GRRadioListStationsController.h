//
//  GRRadioSettingsStationsController.h
//  GrannyApp
//
//  Created by Ievgen Pavliuk on 8/1/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "GRCommonAlertView.h"
#import "GRRadioStationEditController.h"

@interface GRRadioListStationsController : GAITrackedViewController <UITableViewDelegate, UITableViewDataSource, GRCommonAlertDelegate> {
    
    UITableView *contentTable;
    NSMutableArray *tableData;
    
    id parentController;
    
    BOOL  isEditOn;
    
    NSInteger selectedStationIndex;
    GRRadioStationEditController *editController;
    UIView *backView;
    
    ALAssetsLibrary *library;

}
@property (nonatomic, retain) IBOutlet UITableView *contentTable;
@property (nonatomic, assign) id parentController;
@property (nonatomic, retain) IBOutlet UIButton *addButton;
@property (nonatomic, retain) IBOutlet UIButton *okButton;
@property (nonatomic, retain) IBOutlet UIButton *editButton;
@property (nonatomic, retain) IBOutlet UIView *backView;

- (void)onSelectAddSearchMethodController;
- (void)editBtnPressed;
- (void)okBtnPressed;

- (IBAction)homeBtnTapped:(id)sender;
- (IBAction)editRadioBtnTapped:(id)sender;
- (IBAction)okRadioBtnTapped:(id)sender;
- (IBAction)addRadioBtnTapped:(id)sender;

- (IBAction)deleteStationCancelButtonTapped;
- (IBAction)deleteStationDeleteButtonTapped;

@end
