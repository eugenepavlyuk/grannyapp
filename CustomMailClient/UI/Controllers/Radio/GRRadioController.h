//
//  GRRadioController.h
//  Granny
//
//  Created by Ievgen Pavliuk on 3/27/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MediaPlayer/MediaPlayer.h>

@interface GRRadioController : GAITrackedViewController  {
    MPMoviePlayerController *player;
    
    NSDictionary *radioObj;
    
    UIView   *loadingView;
    UILabel  *lRadioTitle;
}
@property (nonatomic, retain) IBOutlet UIView *loadingView;
@property (nonatomic, retain) IBOutlet UILabel *lRadioTitle;
@property (nonatomic, retain) NSDictionary *radioObj;



@end
