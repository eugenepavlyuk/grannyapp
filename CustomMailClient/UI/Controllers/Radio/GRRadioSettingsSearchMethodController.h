//
//  GRRadioSettingsSearchMethodController.h
//  GrannyApp
//
//  Created by Ievgen Pavliuk on 8/1/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "GRRadioSearchBtnView.h"

@interface GRRadioSettingsSearchMethodController : GAITrackedViewController <GRRadiSearcBtnDelegate> {
    
    UIView    *contentView;
    UILabel   *lTitle;
    UIView    *backView;
}
@property (nonatomic, retain) IBOutlet UIView *contentView;
@property (nonatomic, retain) IBOutlet UILabel *lTitle;
@property (nonatomic, retain) IBOutlet UIView *backView;

- (void)searchBtnTapped:(id)sender;

- (void)listBtnTapped:(id)sender;

- (IBAction)homeBtnTapped:(id)sender;

@end
