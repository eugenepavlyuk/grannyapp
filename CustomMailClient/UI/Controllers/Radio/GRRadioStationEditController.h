//
//  GRRadioStationEditController.h
//  GrannyApp
//
//  Created by Ievgen Pavliuk on 8/2/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "GRCommonAlertView.h"
#import "TRadioStation.h"
#import "GRRadioImagePopover.h"


@interface GRRadioStationEditController : GAITrackedViewController <UITextFieldDelegate, GRCommonAlertDelegate, GRRadioImagePopoverDelegate> {
    
    UITextField *tName;
    UITextField *tUrl;
    
    UILabel     *lTitle;
    UIView      *backView;
    
    
    TRadioStation *station;
    GRRadioImagePopover *radioImagePopover;
    NSString *logoImagePath;
    
    BOOL isPopoverDisplay;
    
    id delegate;
}
@property (nonatomic, assign) id delegate;

@property (nonatomic, retain) IBOutlet UITextField *tName;
@property (nonatomic, retain) IBOutlet UITextField *tUrl;
@property (nonatomic, retain) IBOutlet UILabel *lTitle;
@property (nonatomic, retain) IBOutlet UIView *backView;
@property (nonatomic, retain) IBOutlet UIButton *logoBtn;
@property (nonatomic, retain) IBOutlet GRRadioImagePopover *radioImagePopover;
@property (nonatomic, retain) TRadioStation *station;

- (IBAction)okEditBtnPressed:(id)sender;
- (IBAction)cancelBtnPressed:(id)sender;
- (IBAction)logoBtnPressed:(id)sender;
@end
