//
//  GRRadioSettingsSearchController.h
//  GrannyApp
//
//  Created by Ievgen Pavliuk on 8/1/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface GRRadioSettingsSearchController : GAITrackedViewController <UITextFieldDelegate> {
    
    UITextField *tSeacrh;
    UIView      *backView;
}
@property (nonatomic, retain) IBOutlet UITextField *tSeacrh;
@property (nonatomic, retain) IBOutlet UIView *backView;

- (IBAction)homeBtnTapped:(id)sender;

@end
