//
//  GRRadioSelectorController.h
//  GrannyApp
//
//  Created by Ievgen Pavliuk on 8/1/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface GRRadioSelectorController : GAITrackedViewController <UIPickerViewDelegate, UIPickerViewDataSource> {
    
    UIPickerView *pickerView;
    BOOL          isBackgroundHidden;
    
    NSMutableArray *countries;
    NSMutableArray *languages;
    NSMutableArray *genres;
    IBOutlet UIImageView *containerView;
}

@property (nonatomic, retain) IBOutlet UIPickerView *pickerView;

- (NSString *)valueForComponent:(NSInteger)component row:(NSInteger)row;

- (IBAction)okBtnTapped:(id)sender;
- (IBAction)homeBtnTapped:(id)sender;
@end
