//
//  GRRadioSettingsSearchMethodController.m
//  GrannyApp
//
//  Created by Ievgen Pavliuk on 8/1/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "GRRadioSettingsSearchMethodController.h"
#import "GRRadioSelectorController.h"
#import "GRRadioSettingsSearchController.h"

@interface GRRadioSettingsSearchMethodController ()

@end

@implementation GRRadioSettingsSearchMethodController
@synthesize contentView;
@synthesize lTitle;
@synthesize backView;

#pragma mark - Initialization

- (void)dealloc
{
    [contentView release];
    [lTitle release];
    [backView release];
    [super dealloc];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    if (!IS_IPAD)
        [self willRotateToInterfaceOrientation:[UIApplication sharedApplication].statusBarOrientation duration:0];
}

- (BOOL)prefersStatusBarHidden
{
    return YES;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.screenName = @"Radio Station Settings Search Method Selection Screen";
    
    self.contentView.layer.borderWidth = 1.f;
    self.contentView.layer.borderColor = RGBCOLOR(96, 96, 96).CGColor;
    
    if (IS_IPAD)
    {
        GRRadioSearchBtnView *leftBtnView = [[[GRRadioSearchBtnView alloc] initWithFrame:CGRectMake(2, self.lTitle.frame.origin.y+self.lTitle.frame.size.height, 195, 117) index:0] autorelease];
        leftBtnView.delegate = self;
        [self.contentView addSubview:leftBtnView];
        
        GRRadioSearchBtnView *middleBtnView = [[[GRRadioSearchBtnView alloc] initWithFrame:CGRectMake(leftBtnView.frame.origin.x+leftBtnView.frame.size.width, self.lTitle.frame.origin.y+self.lTitle.frame.size.height, 180, 117) index:1] autorelease];
        middleBtnView.delegate = self;
        [self.contentView addSubview:middleBtnView];
        
        GRRadioSearchBtnView *rightBtnView = [[[GRRadioSearchBtnView alloc] initWithFrame:CGRectMake(middleBtnView.frame.origin.x+middleBtnView.frame.size.width, self.lTitle.frame.origin.y+self.lTitle.frame.size.height, 195, 117) index:2] autorelease];
        rightBtnView.delegate = self;
        [self.contentView addSubview:rightBtnView];
    }
    else
    {
        GRRadioSearchBtnView *leftBtnView = [[[GRRadioSearchBtnView alloc] initWithFrame:CGRectMake(2, self.lTitle.frame.origin.y+self.lTitle.frame.size.height, 195/2, 117/2) index:0] autorelease];
        leftBtnView.delegate = self;
        [self.contentView addSubview:leftBtnView];
        
        GRRadioSearchBtnView *middleBtnView = [[[GRRadioSearchBtnView alloc] initWithFrame:CGRectMake(leftBtnView.frame.origin.x+leftBtnView.frame.size.width, self.lTitle.frame.origin.y+self.lTitle.frame.size.height, 180/2, 117/2) index:1] autorelease];
        middleBtnView.delegate = self;
        [self.contentView addSubview:middleBtnView];
        
        GRRadioSearchBtnView *rightBtnView = [[[GRRadioSearchBtnView alloc] initWithFrame:CGRectMake(middleBtnView.frame.origin.x+middleBtnView.frame.size.width, self.lTitle.frame.origin.y+self.lTitle.frame.size.height, 195/2, 117/2) index:2] autorelease];
        rightBtnView.delegate = self;
        [self.contentView addSubview:rightBtnView];
    }
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    if (IS_IPAD)
        return UIInterfaceOrientationIsLandscape(interfaceOrientation);
    else
        return (interfaceOrientation != UIInterfaceOrientationPortraitUpsideDown);
}


#pragma mark - Action methods
- (IBAction)homeBtnTapped:(id)sender
{
    [self.navigationController popViewControllerAnimated:NO];
}
- (void)searchBtnTapped:(id)sender
{
    GRRadioSettingsSearchController *searchController = [[GRRadioSettingsSearchController alloc] init];
    [self.navigationController pushViewController:searchController animated:YES];
    [searchController release];
}

- (void)listBtnTapped:(id)sender
{
    GRRadioSelectorController *selectorController = [[GRRadioSelectorController alloc] init];
    [self.navigationController pushViewController:selectorController animated:YES];
    [selectorController release];
}

#pragma mark - GRRadiSearcBtnDelegate

- (void)radioSearchBtnSelectedWithIndex:(NSInteger)index
{
    switch (index)
    {
        case 0:
            [self listBtnTapped:nil];
            break;
        case 1:
            [self searchBtnTapped:nil];
            break;
        case 2:
        {
            NSNotificationCenter *notificationCenter = [NSNotificationCenter defaultCenter];
            [notificationCenter postNotificationName:@"newStationController"
                                              object:nil
                                            userInfo:nil];
            if (IS_IPAD)
                [self.navigationController popToRootViewControllerAnimated:NO];
            else
                [self.navigationController popViewControllerAnimated:NO];
            break;
        }
        default:
            break;
    }
}



@end
