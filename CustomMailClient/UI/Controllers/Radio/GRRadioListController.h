//
//  GRRadioListController.h
//  Granny
//
//  Created by Ievgen Pavliuk on 3/27/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface GRRadioListController : GAITrackedViewController <UITableViewDelegate, UITableViewDataSource> {
    
    UITableView *contentTable;
    NSMutableArray *tableData;
}
@property (nonatomic, retain) IBOutlet UITableView *contentTable;

- (void)initTableData;
@end
