//
//  TreeListModel.h
//  EasyApp
//
//  Created by Eugene Pavlyuk on 09.05.11.
//  Copyright 2011 Home. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface HistoryItem : NSObject

@property (nonatomic, assign) BOOL isOpen;
@property (nonatomic, copy) NSString *key;
@property (nonatomic, copy) NSDate *date;
@property (nonatomic, retain) NSMutableArray *children;
@property (nonatomic, retain) NSMutableArray *pages;

@end

@interface TreeListModel : NSObject 
{
    NSInteger cell_count;
}

@property (nonatomic, retain) NSMutableArray *items;
@property (nonatomic, readonly) NSInteger cellCount;

// designated initializer
- (id)initWithHistoryArray:(NSMutableArray *)dict;

// item and item level access
- (id)itemForRowAtIndexPath:(NSIndexPath *)indexPath;
- (NSInteger)levelForRowAtIndexPath:(NSIndexPath *)indexPath;

// open/close state
- (BOOL)isCellOpenForRowAtIndexPath:(NSIndexPath *)indexPath;
- (void)setOpenClose:(BOOL)state forRowAtIndexPath:(NSIndexPath *)indexPath;

@end
