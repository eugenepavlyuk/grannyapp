//
//  TreeListModel.m
//  EasyApp
//
//  Created by Eugene Pavlyuk on 09.05.11.
//  Copyright 2011 Home. All rights reserved.
//

#import "TreeListModel.h"
#import "NSString+SBJSON.h"

@interface TreeListModel ()

@property (nonatomic, retain) NSMutableArray *lookup;
@property (nonatomic, retain) NSMutableArray *level;

- (void)recalculate;
- (int)recalculateWithItem:(id)item andLevel:(int)lvl;

@end

@implementation HistoryItem

@synthesize isOpen;
@synthesize key;
@synthesize children;
@synthesize date;
@synthesize pages;

- (id)init
{
    self = [super init];
    
    if (self)
    {
        self.children = [NSMutableArray array];
        self.pages = [NSMutableArray array];
        
        isOpen = NO;
    }
    
    return self;
}

- (void)dealloc
{
    self.key = nil;
    self.date = nil;
    self.children = nil;
    self.pages = nil;
    [super dealloc];
}

@end

@implementation TreeListModel

@synthesize cellCount;
@synthesize items;
@synthesize level;
@synthesize lookup;

#pragma mark -
#pragma mark accessors

- (NSInteger)cellCount
{
    return cell_count;
}


- (void)setItems:(NSMutableArray *)newItems
{
    @synchronized (self) 
    {
        if (items != newItems) 
        {
            [items release];
            items = nil;
            
            if (newItems) 
            {
                items = [newItems retain];
                [self recalculate];
            }
        }
    }
}

#pragma mark -
#pragma mark model item data access

- (id)itemForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return [self.lookup objectAtIndex:indexPath.row];
}

- (NSInteger)levelForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return [[self.level objectAtIndex:indexPath.row] intValue];
}

- (BOOL)isCellOpenForRowAtIndexPath:(NSIndexPath *)indexPath
{
    HistoryItem *item = [self itemForRowAtIndexPath:indexPath];
    return item.isOpen;
}

- (void)setOpenClose:(BOOL)state forRowAtIndexPath:(NSIndexPath *)indexPath
{
    HistoryItem *item = [self itemForRowAtIndexPath:indexPath];
    item.isOpen = state;
    [self recalculate];
}

#pragma mark -
#pragma mark internal model housekeeping

- (void)recalculate
{
    self.lookup = [NSMutableArray array];
    self.level = [NSMutableArray array];
    
    cell_count = 0;
    
    for (HistoryItem *item in self.items) 
    {
        cell_count += [self recalculateWithItem:item andLevel:0];
    }
}

- (int)recalculateWithItem:(HistoryItem*)item andLevel:(int)lvl
{
    int count = 1;

    [self.lookup addObject:item];
    [self.level addObject:[NSNumber numberWithInt:lvl]];

    BOOL isOpen = [item isOpen];

    if (isOpen) 
    {
        for (HistoryItem *child in item.children) 
        {
            count += [self recalculateWithItem:child andLevel:lvl + 1];
        }
    }

    return count;
}

#pragma mark -
#pragma mark init and dealloc

- (id)init
{
	self = [super init];
    
	if (self) 
    {
        self.lookup = [NSMutableArray array];
        self.level = [NSMutableArray array];

        return self;
    }
    
    return nil;
}

- (id)initWithHistoryArray:(NSMutableArray *)array
{
	self = [self init];
    
    if (self) 
    {
        self.items = array;
        return self;
    }
    
    return nil;
}

- (void)dealloc
{
	self.items         = nil;
	self.lookup        = nil;
	self.level         = nil;
	[super dealloc];
}


@end
