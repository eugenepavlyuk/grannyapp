//
//  GREventCell.h
//  GrannyApp
//
//  Created by Eugene Pavlyuk on 9/21/12.
//  Copyright (c) 2012 Home. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface GREventCell : UIView

@property (nonatomic, retain) UIImageView *leftBorderImageView;
@property (nonatomic, retain) UIImageView *lowBorderImageView;
@property (nonatomic, retain) UIImageView *backgroundImageView;
@property (nonatomic, retain) UIColor *todayBackgroundColor;
@property (nonatomic, assign) BOOL currentDay;

@end
