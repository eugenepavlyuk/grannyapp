//
//  CDatePickerViewEx.h
//  GrannyApp
//
//  Created by Eugene Pavlyuk on 9/30/12.
//
//

#import <UIKit/UIKit.h>

@interface GRDatePickerView : UIPickerView <UIPickerViewDelegate, UIPickerViewDataSource>

@property (nonatomic, strong, readonly) NSDate *date;
@property (nonatomic, assign) BOOL bShowYears;

- (void)selectToday;

- (NSInteger)getDay;
- (NSInteger)getMonth;
- (NSInteger)getYear;

- (void)setYear:(NSInteger)year;
- (void)setMonth:(NSInteger)month;
- (void)setDay:(NSInteger)day;

@end
