//
//  GRHeaderCell.h
//  GrannyApp
//
//  Created by Eugene Pavlyuk on 9/20/12.
//  Copyright (c) 2012 Home. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface GRHeaderCell : UIView

@property (nonatomic, retain) UILabel *label;
@property (nonatomic, retain) UIImageView *leftBorder;
@property (nonatomic, retain) UIImageView *currentDayShelf;
@property (nonatomic, retain) UIImageView *foregroundImageView;
@property (nonatomic, retain) UIColor *todayBackgroundColor;
@property (nonatomic, retain) UIColor *todayTitleColor;
@property (nonatomic, retain) UIFont *titleFont;
@property (nonatomic, assign) BOOL currentDay;

@end
