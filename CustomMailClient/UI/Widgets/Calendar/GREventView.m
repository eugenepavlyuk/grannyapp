//
//  GREventView.m
//  GrannyApp
//
//  Created by Eugene Pavlyuk on 9/20/12.
//  Copyright (c) 2012 Home. All rights reserved.
//

#import "GREventView.h"
#import "GRWeekView.h"
#import "EventButton.h"
#import "Event.h"


@implementation GREventView

@synthesize weekView;
@synthesize event;
@synthesize xOffset=_xOffset;
@synthesize yOffset=_yOffset;
@synthesize titleLabel;
@synthesize backgroundButtonView;

- (id)initWithFrame:(CGRect)frame 
{
    if (self = [super initWithFrame:frame]) 
    {
		[self setupCustomInitialisation];
    }
    
    return self;
}

- (id)initWithCoder:(NSCoder *)decoder 
{
	if (self = [super initWithCoder:decoder]) 
    {
		[self setupCustomInitialisation];
	}
    
	return self;
}

- (void)setupCustomInitialisation 
{
	twoFingerTapIsPossible = NO;
	multipleTouches = NO;
	delegate = self;
	self.exclusiveTouch = YES;	
    
    self.backgroundColor = [UIColor clearColor];
    
//    UIImage *background = [[UIImage imageNamed:@"calendar_last_event_back_ipad"] stretchableImageWithLeftCapWidth:20 topCapHeight:20];
    
    self.backgroundButtonView = [EventButton buttonWithType:UIButtonTypeCustom];
//    [self.backgroundButtonView setBackgroundImage:background forState:UIControlStateNormal];
    [self addSubview:self.backgroundButtonView];
    self.backgroundButtonView.imageView.contentMode = UIViewContentModeCenter;
    
    [self.backgroundButtonView setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    self.backgroundButtonView.titleLabel.font = [UIFont fontWithName:@"Helvetica-Bold" size:21];
    self.backgroundButtonView.titleLabel.numberOfLines = 2;
    self.backgroundButtonView.titleLabel.textAlignment = NSTextAlignmentCenter;
    
    self.titleLabel = [[[UILabel alloc] initWithFrame:CGRectZero] autorelease];
    
    self.titleLabel.textAlignment = NSTextAlignmentCenter;
    
    self.titleLabel.textColor = [UIColor blackColor];
    self.titleLabel.font = [UIFont fontWithName:@"Helvetica-Bold" size:21];
    self.titleLabel.backgroundColor = [UIColor clearColor];
    [self addSubview:self.titleLabel];
    
    //self.backgroundButtonView.backgroundColor = rgb(0xb7cfc8);
}

- (void)layoutSubviews 
{
    [super layoutSubviews];
    
	_textRect = CGRectMake(CGRectGetMinX(self.bounds),
						   CGRectGetMinY(self.bounds),
						   CGRectGetWidth(self.bounds),
						   CGRectGetHeight(self.bounds));
    
    self.titleLabel.frame = self.bounds;
    
    if (IS_IPAD)
    {
        self.titleLabel.hidden = YES;
    }
    
    self.backgroundButtonView.frame = self.bounds;
}

- (void)tapDetectingView:(TapDetectingView *)view gotSingleTapAtPoint:(CGPoint)tapPoint 
{
//	if ([self.weekView.delegate respondsToSelector:@selector(weekView:eventTapped:)]) 
//    {
//        [self.weekView.delegate weekView:self.weekView eventTapped:self.event];
//	}
}

- (NSDate *)beginningOfDayFromDate:(NSDate *)date 
{
	NSDateComponents *components = [[AppDelegate getInstance].globalCalendar components:DATE_COMPONENTS fromDate:date];
	[components setDay:[components day]];
	[components setHour:0];
	[components setMinute:0];
	[components setSecond:0];
	return [[AppDelegate getInstance].globalCalendar dateFromComponents:components];
}

- (void)didMoveToWindow
{
    {
        if ([event.persistent boolValue])
        {
            if ([event isEvent])
            {
                [self.backgroundButtonView setImage:self.lockImage forState:UIControlStateNormal];
            }
            else
            {
                [self.backgroundButtonView setImage:self.lockImage forState:UIControlStateNormal];
            }
        }
        else
        {
            [self.backgroundButtonView setImage:self.eventImage forState:UIControlStateNormal];
        }
    }
}

- (void)setEvent:(Event *)newEvent
{
    if (event != newEvent)
    {
        [event release];
        event = [newEvent retain];
        
        [self.backgroundButtonView removeTarget:nil action:nil forControlEvents:UIControlEventTouchUpInside];
        
//        NSDate *beginDate = [self beginningOfDayFromDate:event.date];
//        NSDate *currentBeginDate = [self beginningOfDayFromDate:[NSDate date]];
//
//
//        if ([beginDate compare:currentBeginDate] == NSOrderedAscending && [beginDate compare:currentBeginDate] != NSOrderedSame)
//        {
//            UIImage *background = [[UIImage imageNamed:@"calendar_last_event_back_ipad"] stretchableImageWithLeftCapWidth:20 topCapHeight:20];
//            
//            [self.backgroundButtonView setBackgroundImage:background forState:UIControlStateNormal];
//        }
//        else
//        {
//            UIImage *background = nil;
//            
////            if (IS_IPAD)
//            {
//                background = [[UIImage imageNamed:@"alarm_next_event_back_ipad"] stretchableImageWithLeftCapWidth:20 topCapHeight:20];
//            }
////            else
////            {
////                background = [[UIImage imageNamed:@"calendar_next_event_back_ipad"] stretchableImageWithLeftCapWidth:20 topCapHeight:20];
////            }
//            
//            [self.backgroundButtonView setBackgroundImage:background forState:UIControlStateNormal];
//        }
        
        [self.backgroundButtonView addTarget:self action:@selector(eventButtonTapped) forControlEvents:UIControlEventTouchUpInside];
    }
}

- (void)dealloc
{
    self.weekView = nil;
    [event release];
    self.titleLabel = nil;
    self.backgroundButtonView = nil;
    self.lockImage = nil;
    self.eventImage = nil;
    [super dealloc];
}

- (void)eventButtonTapped
{
    if ([self.weekView.delegate respondsToSelector:@selector(weekView:eventTapped:)])
    {
        [self.weekView.delegate weekView:self.weekView eventTapped:self.event];
	}
}

@end
