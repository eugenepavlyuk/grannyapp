//
//  MAHourView.h
//  GrannyApp
//
//  Created by Eugene Pavlyuk on 9/20/12.
//  Copyright (c) 2012 Home. All rights reserved.
//

#import <UIKit/UIKit.h>

@class GRWeekView;

@interface GRHourView : UIView 

- (BOOL)timeIs24HourFormat;

@property (nonatomic, retain) GRWeekView *weekView;
@property (nonatomic, retain) UIColor *textColor;
@property (nonatomic, retain) UIFont *textFont UI_APPEARANCE_SELECTOR;
@property (nonatomic, assign) NSNumber *yOffset UI_APPEARANCE_SELECTOR;

@end
