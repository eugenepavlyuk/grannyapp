//
//  GRHourCell.h
//  GrannyApp
//
//  Created by Eugene Pavlyuk on 9/20/12.
//  Copyright (c) 2012 Home. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface GRHourCell : UIView

@property (nonatomic, retain) UILabel *label;
@property (nonatomic, retain) UIImageView *border;
@property (nonatomic, retain) UIColor *borderColor;

@end
