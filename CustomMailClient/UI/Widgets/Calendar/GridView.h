

#import <UIKit/UIKit.h>

@class GRWeekView;
@class Event;

@interface GridView : UIView 
{
	unsigned int _rows;
	unsigned int _columns;
}

@property (readwrite, assign) unsigned int rows;
@property (readwrite, assign) unsigned int columns;

@property (nonatomic, retain) UIColor *todayBackgroundColor;
@property (nonatomic, retain) UIColor *colorForEvent;

@property (readonly) CGFloat cellWidth;
@property (readonly) CGFloat cellHeight;

- (void)addEventToOffset:(unsigned int)offset event:(Event *)event weekView:(GRWeekView *)weekView;

- (void)showCurrentDay:(NSInteger)day;

@end
