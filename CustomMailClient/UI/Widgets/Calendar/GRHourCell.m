//
//  GRHourCell.m
//  GrannyApp
//
//  Created by Eugene Pavlyuk on 9/20/12.
//  Copyright (c) 2012 Home. All rights reserved.
//

#import "GRHourCell.h"

@implementation GRHourCell

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    
    if (self)
    {        
        self.label = [[[UILabel alloc] initWithFrame:frame] autorelease];
        self.label.backgroundColor = [UIColor clearColor];
        self.label.textAlignment = NSTextAlignmentCenter;
        self.label.shadowColor = [UIColor clearColor];
        [self addSubview:self.label];
        
        self.backgroundColor = [UIColor clearColor];
        
        self.border = [[[UIImageView alloc] init] autorelease];
        self.border.backgroundColor = self.borderColor;
        
        [self addSubview:self.border];
    }
    
    return self;
}

//- (void)didMoveToWindow
//{
//    self.border.backgroundColor = self.borderColor;
//}

- (void)dealloc
{
    self.label = nil;
    self.border = nil;
    self.borderColor = nil;
    [super dealloc];
}

- (void)layoutSubviews
{
    [super layoutSubviews];
    
    self.label.frame = CGRectMake(0, 0, self.bounds.size.width, self.bounds.size.height);
    
    self.border.frame = CGRectMake(0, self.bounds.size.height - 1, self.bounds.size.width, 1);
}

- (void)setBorderColor:(UIColor *)borderColor
{
    [_borderColor release];
    _borderColor = [borderColor retain];
    
    self.border.backgroundColor = _borderColor;
}

@end
