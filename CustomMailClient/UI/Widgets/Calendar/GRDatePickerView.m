//
//  GRDatePickerView
//  GrannyApp
//
//  Created by Eugene Pavlyuk on 9/30/12.
//
//

#import "GRDatePickerView.h"


// Identifiers of components
#define MONTH ( 0 )
#define DAY ( 1 )
#define YEAR ( 2 )


// Identifies for component views
#define LABEL_TAG 43


@interface GRDatePickerView()

@property (nonatomic, strong) NSArray *months;
@property (nonatomic, strong) NSArray *days;
@property (nonatomic, strong) NSArray *years;

- (NSArray *)nameOfDays;
- (NSArray *)nameOfMonths;
- (NSArray *)nameOfYears;

- (UILabel *)labelForComponent:(NSInteger)component selected:(BOOL)selected;
- (NSString *)titleForRow:(NSInteger)row forComponent:(NSInteger)component;
- (NSInteger)bigRowMonthCount;
- (NSInteger)bigRowDayCount;
- (NSInteger)bigRowYearCount;
- (NSString *)currentMonthName;
- (NSString *)currentDayName;
- (NSString *)currentYearName;

@end

#define bigRowCount 1000
#define rowHeight 44.f

@implementation GRDatePickerView
{
    NSInteger monthNow;
    NSInteger dayNow;
    NSInteger yearNow;
}

@synthesize years;
@synthesize months;
@synthesize days = _days;

- (void)awakeFromNib
{
    [super awakeFromNib];
    
    NSCalendar *calendar = [NSCalendar currentCalendar];
    NSDateComponents *dateComponents = [calendar components:NSYearCalendarUnit|NSMonthCalendarUnit|NSDayCalendarUnit fromDate:[NSDate date]];
    
    monthNow = [dateComponents month] - 1;
    dayNow = [dateComponents day] - 1;
    yearNow = [dateComponents year];
    
    _bShowYears = YES;
    
    self.months = [self nameOfMonths];
    self.days = [self nameOfDays];
    self.years = [self nameOfYears];
    
    self.delegate = self;
    self.dataSource = self;
    
    [self selectToday];
}

- (void)setBShowYears:(BOOL)showYears
{
    _bShowYears = showYears;
    
    [self reloadAllComponents];
}

- (NSDate *)date
{
    NSInteger monthCount = [self.months count];
    NSString *month = [self.months objectAtIndex:([self selectedRowInComponent:MONTH] % monthCount)];
    
    NSInteger daysCount = [self.days count];
    NSString *day = [self.days objectAtIndex:([self selectedRowInComponent:DAY] % daysCount)];
    
    NSString *year = [self.years objectAtIndex:[self selectedRowInComponent:YEAR]];
    
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"MMMM:dd:yyyy"];
    NSDate *date = [formatter dateFromString:[NSString stringWithFormat:@"%@:%@:%@", month, day, year]];
    return date;
}

#pragma mark - UIPickerViewDelegate

- (UIView *)pickerView: (UIPickerView *)pickerView
           viewForRow: (NSInteger)row
         forComponent: (NSInteger)component
          reusingView: (UIView *)view
{    
    UILabel *returnView = nil;
    
    if(view.tag == LABEL_TAG)
    {
        returnView = (UILabel *)view;
    }
    else
    {
        returnView = [self labelForComponent: component selected: NO];
    }
    
    returnView.text = [self titleForRow:row forComponent:component];
        
    return returnView;
}

- (CGFloat)pickerView:(UIPickerView *)pickerView rowHeightForComponent:(NSInteger)component
{
    return rowHeight;
}

#pragma mark - UIPickerViewDataSource

- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView
{
    if (self.bShowYears)
    {
        return 3;
    }
    else
    {
        return 2;
    }
}

- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component
{
    if(component == MONTH)
    {
        return [self bigRowMonthCount];
    }
    
    if(component == YEAR)
    {
        return [self bigRowYearCount];
    }
    
    return [self bigRowDayCount];
}

#pragma mark - Util

- (NSInteger)bigRowMonthCount
{
    return [self.months count]  * bigRowCount;
}

- (NSInteger)bigRowDayCount
{
    return [self.days count]  * bigRowCount;
}

- (NSInteger)bigRowYearCount
{
    return [self.years count];
}

- (NSString *)titleForRow:(NSInteger)row forComponent:(NSInteger)component
{
    if(component == MONTH)
    {
        NSInteger monthCount = [self.months count];
        return [[self.months objectAtIndex:(row % monthCount)] uppercaseString];
    }
    
    if(component == YEAR)
    {
        return [[self.years objectAtIndex:row] uppercaseString];
    }
    
    NSInteger dayCount = [self.days count];
    
    NSInteger selectedMonthRow = ([self selectedRowInComponent:MONTH] % [self.months count]);
    
    NSInteger selectedYearRow = [self selectedRowInComponent:YEAR];
    
    if ((selectedMonthRow == monthNow)
        && (selectedYearRow == yearNow)
        && ((row % dayCount) == dayNow)
        )
    {
        return NSLocalizedString(@"Today", @"Today");
    }
    
    return [self.days objectAtIndex:(row % dayCount)];
}

- (CGFloat)pickerView:(UIPickerView *)pickerView widthForComponent:(NSInteger)component
{
    if (component == 1)
    {
        return 100.f;
    }
    else if (component == 2)
    {
        return 114;
    }
    
    return 254;
}

- (UILabel *)labelForComponent:(NSInteger)component selected:(BOOL)selected
{
    NSInteger width = 0;
    
    if (component == 1)
    {
        width = 100;
    }
    else if (component == 2)
    {
        width = 114;
    }
    else
    {
        width = 254;
    }
    
    CGRect frame = CGRectMake(0.f, 0.f, width, rowHeight);
    
    UILabel *label = [[UILabel alloc] initWithFrame:frame];
    label.textAlignment = NSTextAlignmentCenter;
    label.backgroundColor = [UIColor clearColor];
    label.textColor = [UIColor blackColor];
    label.font = [UIFont boldSystemFontOfSize:38.f];
    label.userInteractionEnabled = NO;
    label.adjustsFontSizeToFitWidth = YES;
    
    label.tag = LABEL_TAG;
    
    return label;
}

- (NSArray *)nameOfMonths
{
    NSDateFormatter *dateFormatter = [NSDateFormatter new];
    return [dateFormatter standaloneMonthSymbols];
}

- (NSArray *)nameOfDays
{
    NSMutableArray *days = [NSMutableArray array];
    
    for(NSInteger day = 1; day <= 31; day++)
    {
        NSString *dayStr = [NSString stringWithFormat:@"%i", day];
        [days addObject:dayStr];
    }
    
    return days;
}

- (NSArray *)nameOfYears
{
    NSMutableArray *yearsArray = [NSMutableArray array];
    
    for(NSInteger year = 0; year <= 4000; year++)
    {
        NSString *yearStr = [NSString stringWithFormat:@"%i", year];
        [yearsArray addObject:yearStr];
    }
    
    return yearsArray;
}

- (void)selectToday
{
    NSInteger month = [self selectedRowInComponent:MONTH] + [self bigRowMonthCount] / 2;
    NSInteger day = [self selectedRowInComponent:DAY] + [self bigRowDayCount] / 2;
    
    NSInteger year = 0;
    
    if (self.bShowYears)
    {
       year = [self selectedRowInComponent:YEAR];
    }
    
    [self selectRow: month
        inComponent: MONTH
           animated: NO];
    
    [self selectRow: day
        inComponent: DAY
           animated: NO];
    
    if (self.bShowYears)
    {
        [self selectRow: year
            inComponent: YEAR
               animated: NO];
    }
}

- (NSString *)currentMonthName
{
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"MMMM"];
    return [formatter stringFromDate:[NSDate date]];
}

- (NSString *)currentDayName
{
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"dd"];
    return [formatter stringFromDate:[NSDate date]];
}

- (NSString *)currentYearName
{
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"yyyy"];
    return [formatter stringFromDate:[NSDate date]];
}

- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component
{
    NSCalendar *calendar = [NSCalendar currentCalendar];
    
    NSDateComponents *dateComponents = [calendar components:NSYearCalendarUnit|NSMonthCalendarUnit|NSDayCalendarUnit fromDate:[NSDate date]];
    
    dateComponents.month = ([self selectedRowInComponent:MONTH] % [self.months count]) + 1;
    
    if (self.bShowYears)
    {
        dateComponents.year = [self selectedRowInComponent:YEAR];
    }
    
    dateComponents.day = 15;
    
    NSDate *date = [calendar dateFromComponents:dateComponents];
    
    NSRange range = [calendar rangeOfUnit:NSDayCalendarUnit inUnit:NSMonthCalendarUnit forDate:date];
    
    NSInteger index = ([self selectedRowInComponent:DAY] % [self.days count]);
    
    if (index >= range.length)
    {
        [pickerView reloadComponent:1];
        [pickerView selectRow:range.length - 1 inComponent:1 animated:YES];
    }
    
    if (component == MONTH || component == YEAR)
    {
        [pickerView reloadComponent:1];
    }
}

- (NSInteger)getDay
{
    NSInteger modul = 31;
    
    NSInteger day = [self selectedRowInComponent:DAY] % modul;
    
    return day;
}

- (NSInteger)getMonth
{
    NSInteger modul = 12;
    
    NSInteger month = [self selectedRowInComponent:MONTH] % modul;
    
    return month;
}

- (NSInteger)getYear
{
    if (self.bShowYears)
    {
        NSInteger year = [self selectedRowInComponent:YEAR];
        
        return year;
    }
    else
    {
        return 0;
    }
}

- (void)setMonth:(NSInteger)month
{
    [self selectRow:month inComponent:MONTH animated:NO];
}

- (void)setDay:(NSInteger)day
{
    [self selectRow:day inComponent:DAY animated:NO];
}

- (void)setYear:(NSInteger)year
{
    if (self.bShowYears)
    {
        [self selectRow:year inComponent:YEAR animated:NO];
    }
}

@end
