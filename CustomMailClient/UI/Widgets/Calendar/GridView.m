


#import "GridView.h"
#import "GREventView.h"
#import "Event.h"
#import "GREventCell.h"
#import "EventButton.h"

@implementation GridView
{
    NSMutableArray *columnsArray;
}

- (id)initWithFrame:(CGRect)frame 
{
    if (self = [super initWithFrame:frame]) 
    {
		[self setupCustomInitialisation];
    }
    
    return self;
}

- (id)initWithCoder:(NSCoder *)decoder 
{
	if (self = [super initWithCoder:decoder]) 
    {
		[self setupCustomInitialisation];
	}
    
	return self;
}


- (void)setupCustomInitialisation 
{
	self.rows             = 8;
	self.columns          = 8;
    
    columnsArray = [[NSMutableArray array] retain];
    
    for (int i = 0; i < DAYS_IN_WEEK; i++)
    {
        NSMutableArray *hours = [NSMutableArray array];
        
        [columnsArray addObject:hours];
        
        for (int j = 0; j < HOURS_IN_DAY; j++)
        {
            GREventCell *eventCell = [[[GREventCell alloc] initWithFrame:CGRectZero] autorelease];
            [self addSubview:eventCell];
            [hours addObject:eventCell];
        }
    }
}

- (void)setRows:(unsigned int)rows 
{
	_rows = rows;
}

- (void)setColumns:(unsigned int)columns 
{
	_columns = columns;
}

- (unsigned int)rows 
{
	return _rows;
}

- (unsigned int)columns 
{
	return _columns;
}

- (CGFloat)cellWidth 
{
	if (_columns > 0) 
    {
		return self.bounds.size.width  / _columns;
	} 
    else 
    {
		return 0;
	}
}

- (CGFloat)cellHeight 
{
	if (_rows > 0) 
    {
		return self.bounds.size.height / _rows;
	} 
    else 
    {
		return 0;
	}
}

- (void)addEventToOffset:(unsigned int)offset event:(Event *)event weekView:(GRWeekView *)weekView 
{
	GREventView *eventView = [[[GREventView alloc] initWithFrame:CGRectZero] autorelease];
	eventView.weekView = weekView;
	eventView.event = event;
	eventView.xOffset = offset;
    
    //eventView.backgroundButtonView.backgroundColor = self.colorForEvent;
    
    if (!IS_IPAD)
    {
        //eventView.titleLabel.text = event.name;
    }
    else
    {
        //[eventView.backgroundButtonView setTitle:nil forState:UIControlStateNormal];
//        [eventView.backgroundButtonView setTitle:event.name forState:UIControlStateNormal];
    }
    
	[self addSubview:eventView];
}

- (void)layoutSubviews 
{
    [super layoutSubviews];
    
	CGFloat cellWidth = self.cellWidth;
	CGFloat cellHeight = self.cellHeight;
	
	for (id view in self.subviews) 
    {
		if (![NSStringFromClass([view class])isEqualToString:@"GREventView"]) 
        {
			continue;
		}
        
		GREventView *eventView = (GREventView *) view;
        
        if ([eventView.event.persistent boolValue])
        {
            eventView.frame = CGRectMake(cellWidth * eventView.xOffset,
                                         cellHeight / MINUTES_IN_HOUR * [eventView.event minutesSinceMidnight],
                                         40,
                                         30);
            
            eventView.alpha = 0.7f;
        }
        else
        {
            eventView.frame = CGRectMake(cellWidth * eventView.xOffset + cellWidth - 35,
                                         cellHeight / MINUTES_IN_HOUR * [eventView.event minutesSinceMidnight],
                                         40,
                                         /*cellHeight / MINUTES_IN_HOUR * [eventView.event durationInMinutes]*/40);
            
            eventView.alpha = 1.0f;
        }
        
		[eventView setNeedsLayout];
	}
    
    for (int i = 0; i < DAYS_IN_WEEK; i++)
    {
        NSMutableArray *hours = [columnsArray objectAtIndex:i];
        
        for (int j = 0; j < HOURS_IN_DAY; j++)
        {
            GREventCell *cell = [hours objectAtIndex:j];
            
            cell.frame = CGRectMake(cellWidth * i, cellHeight * j, cellWidth, cellHeight);
        }
    }
}

- (void)dealloc
{
    [columnsArray release];
    [super dealloc];
}

- (void)showCurrentDay:(NSInteger)day
{
    for (int i = 0; i < DAYS_IN_WEEK; i++)
    {
        NSMutableArray *hours = [columnsArray objectAtIndex:i];
        
        for (int j = 0; j < HOURS_IN_DAY; j++)
        {
            GREventCell *cell = [hours objectAtIndex:j];
            
            cell.todayBackgroundColor = self.todayBackgroundColor;
            
            if (i == day)
            {
                cell.currentDay = YES;
            }
            else
            {
                cell.currentDay = NO;
            }
        }
    }
}

@end