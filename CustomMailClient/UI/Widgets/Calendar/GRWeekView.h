
#import <UIKit/UIKit.h>

@class GridView;
@class GRHourView;
@class GRWeekdayBarView;
@class EventBase;
@class Settings;

@protocol MAWeekViewDataSource, MAWeekViewDelegate;


@interface WeekCalendarSwipeGestureRecognizer : UISwipeGestureRecognizer



@end



@interface GRWeekView : UIView <UIGestureRecognizerDelegate>
{
	UIScrollView *_scrollView;
    
	UISwipeGestureRecognizer *_swipeLeftRecognizer, *_swipeRightRecognizer;
	
	id<MAWeekViewDataSource> _dataSource;
	id<MAWeekViewDelegate> _delegate;
    
    CGPoint startLocation;
}

@property (nonatomic, retain) GridView *gridView;
@property (nonatomic, retain) GRHourView *hourView;
@property (nonatomic, retain) GRWeekdayBarView *weekdayBarView;
@property (nonatomic, retain) Settings *settings;
@property (nonatomic, retain) UIColor *colorForEvent;

@property (nonatomic, copy) NSDate *week;
@property (nonatomic, assign) IBOutlet id<MAWeekViewDataSource> dataSource;
@property (nonatomic, assign) IBOutlet id<MAWeekViewDelegate> delegate;

- (void)reloadData;

- (void)scrollTo7Hours;
- (void)changeWeekRight;
- (void)changeWeekLeft;
- (void)changeWeekToday;

@end

@protocol MAWeekViewDataSource <NSObject>

- (NSArray *)weekView:(GRWeekView *)weekView eventsForDate:(NSDate *)date;

@end

@protocol MAWeekViewDelegate <NSObject>

@optional

- (void)weekView:(GRWeekView *)weekView eventTapped:(EventBase *)event;
- (void)weekView:(GRWeekView *)weekView weekDidChange:(NSDate *)week;

@end