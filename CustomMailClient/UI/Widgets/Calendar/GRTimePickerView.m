//
//  GRTimePickerView.m
//  GrannyApp
//
//  Created by Eugene Pavluk on 10/16/12.
//
//

#import "GRTimePickerView.h"

// Identifiers of components
#define HOUR ( 0 )
#define MINUTE ( 1 )
//#define TIME ( 2 )


// Identifies for component views
#define LABEL_TAG 43



@interface GRTimePickerView()

@property (nonatomic, strong) NSIndexPath *todayIndexPath;
@property (nonatomic, strong) NSArray *hours;
@property (nonatomic, strong) NSArray *minutes;
//@property (nonatomic, strong) NSArray *times;

- (NSArray *)nameOfMinutes;
- (NSArray *)nameOfHours;
//- (NSArray *)nameOfTimes;

- (UILabel *)labelForComponent:(NSInteger)component selected:(BOOL)selected;
- (NSString *)titleForRow:(NSInteger)row forComponent:(NSInteger)component;
- (NSIndexPath *)todayPath;
- (NSInteger)bigRowHourCount;
- (NSInteger)bigRowMinuteCount;

@end

#define bigRowCount 1000
#define rowHeight 44.f

@implementation GRTimePickerView

@synthesize todayIndexPath;
@synthesize hours;
@synthesize minutes;
//@synthesize times;
//@synthesize longTimeFormat;
@synthesize componentWidth;

- (void)reset
{
    self.hours = [self nameOfHours];
    self.minutes = [self nameOfMinutes];
//    self.times = [self nameOfTimes];
    self.todayIndexPath = [self todayPath];
    
    [self reloadAllComponents];
    
    [self selectToday];
}

- (void)awakeFromNib
{
    [super awakeFromNib];
    
    componentWidth = 80;
    
//    timeRow = 0;
//    longTimeFormat = YES;
    
    self.hours = [self nameOfHours];
    self.minutes = [self nameOfMinutes];
//    self.times = [self nameOfTimes];
    self.todayIndexPath = [self todayPath];
    
    self.delegate = self;
    self.dataSource = self;
    
    [self selectToday];
}

- (NSInteger)getHour
{
    NSInteger modul = 24;
    
//    if (!longTimeFormat)
//    {
//        modul = 24;
//    }
    
    NSInteger hour = [self selectedRowInComponent:HOUR] % modul;

//    hour += 1;
//    
//    if (timeRow == 0 && hour < 12)
//    {
//        hour = hour;
//    }
//    else if (timeRow == 0 && hour == 12)
//    {
//        hour = 0;
//    }
//    else if (timeRow == 1 && hour == 12)
//    {
//        hour  = hour;
//    }
//    else
//    {
//        hour += 12;
//    }
    
    return hour;
}

- (NSInteger)getMinute
{
    NSInteger minute = [self selectedRowInComponent:MINUTE] % 60;
    
    return minute;
}

- (void)setHour:(NSInteger)hour
{
    ///
    
//    if (hour == 0)
//    {
//        hour = 12;
//        timeRow = 0;
//    }
//    else if (hour == 12)
//    {
//        hour = 12;
//        timeRow = 1;
//    }
//    else if (hour > 12)
//    {
//        timeRow = 1;
//        hour -= 12;
//    }
//    else
//    {
//        timeRow = 0;
//        hour = hour;
//    }
//    ///
//    
//    hour -= 1;
//    
//    [self selectRow:timeRow inComponent:TIME animated:NO];
    
    [self selectRow:hour inComponent:HOUR animated:NO];
}

- (void)setMinute:(NSInteger)minute
{
    [self selectRow:minute inComponent:MINUTE animated:NO];
}

#pragma mark - UIPickerViewDelegate

- (UIView *)pickerView: (UIPickerView *)pickerView
            viewForRow: (NSInteger)row
          forComponent: (NSInteger)component
           reusingView: (UIView *)view
{
    UILabel *returnView = nil;
    
    if(view.tag == LABEL_TAG)
    {
        returnView = (UILabel *)view;
    }
    else
    {
        returnView = [self labelForComponent: component selected: NO];
    }
    
    returnView.text = [self titleForRow:row forComponent:component];
    
    return returnView;
}

- (CGFloat)pickerView:(UIPickerView *)pickerView rowHeightForComponent:(NSInteger)component
{
    return rowHeight;
}

#pragma mark - UIPickerViewDataSource

- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView
{
//    if (!longTimeFormat)
//    {
//        return 3;
//    }
//    
    return 2;
}

- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component
{
//    if (component == TIME)
//    {
//        return [times count];
//    }
//    
    if(component == HOUR)
    {
        return [self bigRowHourCount];
    }
    
    return [self bigRowMinuteCount];
}

#pragma mark - Util

- (NSInteger)bigRowHourCount
{
    return [self.hours count]  * bigRowCount;
}

- (NSInteger)bigRowMinuteCount
{
    return [self.minutes count]  * bigRowCount;
}

- (NSString *)titleForRow:(NSInteger)row forComponent:(NSInteger)component
{
//    if(component == TIME)
//    {
//        return [[self.times objectAtIndex:row] uppercaseString];
//    }
//    
    if(component == HOUR)
    {
        NSInteger hourCount = [self.hours count];
        return [[self.hours objectAtIndex:(row % hourCount)] uppercaseString];
    }
    
    NSInteger minuteCount = [self.minutes count];
    return [self.minutes objectAtIndex:(row % minuteCount)];
}

- (CGFloat)pickerView:(UIPickerView *)pickerView widthForComponent:(NSInteger)component
{
    return componentWidth;
}

- (UILabel *)labelForComponent:(NSInteger)component selected:(BOOL)selected
{
    NSInteger width = componentWidth;
    
    CGRect frame = CGRectMake(0.f, 0.f, width, rowHeight);
    
    UILabel *label = [[UILabel alloc] initWithFrame:frame];
    label.textAlignment = NSTextAlignmentCenter;
    label.backgroundColor = [UIColor clearColor];
    label.textColor = [UIColor blackColor];
    label.font = [UIFont boldSystemFontOfSize:38.f];
    label.userInteractionEnabled = NO;
    
    label.tag = LABEL_TAG;
    
    return label;
}

- (NSArray *)nameOfHours
{
    NSMutableArray *hoursArray = [NSMutableArray array];
    
    NSInteger modul = 24;
    
//    if (!longTimeFormat)
//    {
//        modul = 12;
//        
//        for (int i = 1; i <= modul; i++)
//        {
//            NSString *string = [NSString stringWithFormat:@"%02i", i];
//            [hoursArray addObject:string];
//        }
//    }
//    else
    {
        for (int i = 0; i < modul; i++)
        {
            NSString *string = [NSString stringWithFormat:@"%02i", i];
            [hoursArray addObject:string];
        }
    }
    
    return hoursArray;
}

//- (NSArray *)nameOfTimes
//{
//    NSMutableArray *timesArray = [NSMutableArray array];
//
//    [timesArray addObject:@"AM"];
//    [timesArray addObject:@"PM"];
//    
//    return timesArray;
//}

- (NSArray *)nameOfMinutes
{
    NSMutableArray *minutesArray = [NSMutableArray array];
    
    for (int i = 0; i < 60; i++)
    {
        NSString *string = [NSString stringWithFormat:@"%02i", i];
        [minutesArray addObject:string];
    }
    
    return minutesArray;
}

- (void)selectToday
{
    NSInteger row = [self selectedRowInComponent:0] + [self bigRowHourCount] / 2;
    NSInteger section = [self selectedRowInComponent:1] + [self bigRowMinuteCount] / 2;
    
    self.todayIndexPath = [NSIndexPath indexPathForRow:row inSection:section];
    
    [self selectRow: self.todayIndexPath.row
        inComponent: HOUR
           animated: NO];
    
    [self selectRow: self.todayIndexPath.section
        inComponent: MINUTE
           animated: NO];
    
//    if (!longTimeFormat)
//    {
//        [self selectRow: timeRow
//            inComponent: TIME
//               animated: NO];
//    }
}

- (NSIndexPath *)todayPath // row - month ; section - day
{
    CGFloat row = 0.f;
    CGFloat section = 0.f;
    
    NSString *hour = @"00";
    NSString *minute  = @"00";
    
//    timeRow = 0;
    
    //set table on the middle
    for(NSString *cellHour in self.hours)
    {
        if([cellHour isEqualToString:hour])
        {
            row = [self.hours indexOfObject:cellHour];
            row = row + [self bigRowHourCount] / 2;
            break;
        }
    }
    
    for(NSString *cellMinute in self.minutes)
    {
        if([cellMinute isEqualToString:minute])
        {
            section = [self.minutes indexOfObject:cellMinute];
            section = section + [self bigRowMinuteCount] / 2;
            break;
        }
    }
    
    return [NSIndexPath indexPathForRow:row inSection:section];
}

- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component
{

}


@end
