//
//  EventButton.m
//  GrannyApp
//
//  Created by Eugene Pavluk on 7/8/13.
//
//

#import "EventButton.h"

@implementation EventButton

- (void)awakeFromNib
{
    self.imageView.contentMode = UIViewContentModeCenter;
}

- (void)layoutSubviews
{
    [super layoutSubviews];
    
    self.imageView.frame = CGRectMake(0, 0, 30, self.bounds.size.height);
//
//    if (self.imageView.image)
//    {
//        self.titleLabel.frame = CGRectMake(30, 0, self.bounds.size.width - 30, self.bounds.size.height);
//    }
//    else
//    {
//        self.titleLabel.frame = self.bounds;
//    }
}

@end
