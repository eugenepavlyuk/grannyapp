//
//  GRWeekdayBarView.m
//  GrannyApp
//
//  Created by Eugene Pavlyuk on 9/19/12.
//  Copyright (c) 2012 Home. All rights reserved.
//

#import "GRWeekdayBarView.h"
#import "GRWeekView.h"
#import "GridView.h"
#import "GRHourView.h"
#import "GRHeaderCell.h"


@interface GRWeekdayBarView()

@property (nonatomic, retain) NSDateFormatter *dateFormatter;
@property (nonatomic, retain) NSMutableArray *weekDaysLabels;
@property (nonatomic, retain) UIView *separatorView;

@end

@implementation GRWeekdayBarView
{
    NSDate *week;
    NSDateFormatter *dateFormatter;
}

@synthesize weekView;
@synthesize textColor;
@synthesize sundayColor; 
@synthesize todayColor;
@synthesize weekdays;
@synthesize weekDaysLabels;
@synthesize dateFormatter;

- (id)initWithFrame:(CGRect)rect
{
    self = [super initWithFrame:rect];
    
    if (self)
    {
        weekDaysLabels = [[NSMutableArray array] retain];
        
        for (int i = 0; i < DAYS_IN_WEEK; i++)
        {
            GRHeaderCell *cell = [[[GRHeaderCell alloc] initWithFrame:CGRectZero] autorelease];
            [self addSubview:cell];
            [weekDaysLabels addObject:cell];
        }
        
        self.separatorView = [[UIView alloc] initWithFrame:CGRectMake(0, rect.size.height - 1, rect.size.width, 1)];
        self.separatorView.backgroundColor = RGBCOLOR(190, 190, 190);
        
        [self addSubview:self.separatorView];
    }
    
    return self;
}

- (void)setWeek:(NSDate *)newWeek 
{
    [week release];
    
	week = [newWeek copy];
	
	NSDate *date = week;
	NSDateComponents *components;
	NSDateComponents *components2 = [[[NSDateComponents alloc] init] autorelease];
	[components2 setDay:1];
	
    [weekdays release];
    
	weekdays = [[NSMutableArray alloc] init];
	
	for (register unsigned int i = 0; i < DAYS_IN_WEEK; i++) 
    {
		[weekdays addObject:date];
		components = [[AppDelegate getInstance].globalCalendar components:DATE_COMPONENTS fromDate:date];
		[components setDay:1];
		date = [[AppDelegate getInstance].globalCalendar dateByAddingComponents:components2 toDate:date options:0];
	}	
    
    [self.weekView.gridView showCurrentDay:-1];
}

- (NSDate *)week 
{
	return week;
}

- (void)layoutSubviews
{
    [super layoutSubviews];
    
	register unsigned int i = 0;
	
	const CGFloat cellWidth = self.weekView.gridView.cellWidth;
    const CGFloat cellMinX = self.weekView.hourView.bounds.size.width;
	
	NSDateComponents *todayComponents = [[AppDelegate getInstance].globalCalendar components:DATE_COMPONENTS fromDate:[NSDate date]];
    
	for (NSDate *date in weekdays) 
    {
		NSDateComponents *components = [[AppDelegate getInstance].globalCalendar components:DATE_COMPONENTS fromDate:date];
        
        NSString *dayStr = [[self.dateFormatter stringFromDate:date] capitalizedString];
        
		NSString *displayText = [NSString stringWithFormat:@"%@. %i", dayStr, [components day]];
		
		CGRect rect = CGRectMake(i * cellWidth + cellMinX, 0, cellWidth, self.bounds.size.height);
		
        GRHeaderCell *cell = [weekDaysLabels objectAtIndex:i];
        
        cell.label.text = displayText;
        
        cell.todayBackgroundColor = self.todayBackgroundColor;
        
        cell.frame = rect;
        
		if ([todayComponents day] == [components day] &&
			[todayComponents month] == [components month] &&
			[todayComponents year] == [components year]) 
        {
            cell.label.textColor = self.todayColor;
            cell.currentDay = YES;
            
            [self.weekView.gridView showCurrentDay:i];
		} 
        else if ([components weekday] == 1) 
        {
            cell.currentDay = NO;
            cell.label.textColor = self.sundayColor;
		} 
        else 
        {
            cell.currentDay = NO;
            cell.label.textColor = self.textColor;
		}
		
		i++;
	}
    
    self.separatorView.frame = CGRectMake(0, self.bounds.size.height - 1, self.bounds.size.width, 1);
}

- (NSDateFormatter *)dateFormatter 
{
	if (!dateFormatter) 
    {
		dateFormatter = [[NSDateFormatter alloc] init];
        [self.dateFormatter setDateFormat:@"EEE"];
	}
    
	return dateFormatter;
}

- (void)dealloc
{
    self.week = nil;
    self.weekDaysLabels = nil;
    self.textColor = nil;
    self.sundayColor = nil;
    self.todayColor = nil;
    self.todayBackgroundColor = nil;
    self.weekdays = nil;
    self.dateFormatter = nil;
    [super dealloc];
}

@end
