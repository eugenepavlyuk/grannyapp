//
//  GRWeekdayBarView.h
//  GrannyApp
//
//  Created by Eugene Pavlyuk on 9/19/12.
//  Copyright (c) 2012 Home. All rights reserved.
//

#import <UIKit/UIKit.h>

@class GRWeekView;

@interface GRWeekdayBarView : UIView 

@property (nonatomic, assign) GRWeekView *weekView;
@property (nonatomic, copy) NSDate *week;
@property (nonatomic, retain) UIColor *textColor; 
@property (nonatomic, retain) UIColor *sundayColor; 
@property (nonatomic, retain) UIColor *todayColor;
@property (nonatomic, retain) UIColor *todayBackgroundColor;
@property (nonatomic, retain) NSMutableArray *weekdays;

@end