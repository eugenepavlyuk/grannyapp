//
//  GRMinuteSecondsPickerView.h
//  GrannyApp
//
//  Created by Eugene Pavluk on 6/22/13.
//
//

#import <UIKit/UIKit.h>

@interface GRMinuteSecondsPickerView : UIPickerView <UIPickerViewDelegate, UIPickerViewDataSource>

@property (nonatomic, assign) NSInteger componentWidth;

- (void)reset;
- (void)selectToday;

- (NSInteger)getSecond;
- (NSInteger)getMinute;

- (void)setSecond:(NSInteger)second;
- (void)setMinute:(NSInteger)minute;

- (NSInteger)bigRowSecondCount;

@end
