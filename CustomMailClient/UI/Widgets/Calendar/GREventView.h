//
//  GREventView.h
//  GrannyApp
//
//  Created by Eugene Pavlyuk on 9/20/12.
//  Copyright (c) 2012 Home. All rights reserved.
//

#import "TapDetectingView.h"

@class Event;
@class GRWeekView;
@class EventButton;

@interface GREventView : TapDetectingView <TapDetectingViewDelegate> 
{
	CGRect _textRect;
	size_t _xOffset;
	size_t _yOffset;
}

- (void)setupCustomInitialisation;

@property (nonatomic, retain) GRWeekView *weekView;
@property (nonatomic, retain) Event *event;

@property (nonatomic, retain) UILabel *titleLabel;
@property (nonatomic, retain) EventButton *backgroundButtonView;
@property (nonatomic, assign) size_t xOffset;
@property (nonatomic, assign) size_t yOffset;
@property (nonatomic, retain) UIImage *lockImage UI_APPEARANCE_SELECTOR;
@property (nonatomic, retain) UIImage *eventImage UI_APPEARANCE_SELECTOR;

@end
