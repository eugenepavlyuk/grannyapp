//
//  GRMinuteSecondsPickerView.m
//  GrannyApp
//
//  Created by Eugene Pavluk on 6/22/13.
//
//

#import "GRMinuteSecondsPickerView.h"

// Identifiers of components
#define SECOND ( 1 )
#define MINUTE ( 0 )


// Identifies for component views
#define LABEL_TAG 43

@interface GRMinuteSecondsPickerView()

@property (nonatomic, strong) NSIndexPath *todayIndexPath;
@property (nonatomic, strong) NSArray *seconds;
@property (nonatomic, strong) NSArray *minutes;

- (NSArray *)nameOfMinutes;
- (NSArray *)nameOfSeconds;

- (UILabel *)labelForComponent:(NSInteger)component selected:(BOOL)selected;
- (NSString *)titleForRow:(NSInteger)row forComponent:(NSInteger)component;
- (NSIndexPath *)todayPath;
- (NSInteger)bigRowMinuteCount;

@end

#define bigRowCount 1000
#define rowHeight 44.f


@implementation GRMinuteSecondsPickerView

@synthesize todayIndexPath;
@synthesize seconds;
@synthesize minutes;
@synthesize componentWidth;

- (void)reset
{
    self.seconds = [self nameOfSeconds];
    self.minutes = [self nameOfMinutes];
    self.todayIndexPath = [self todayPath];
    
    [self reloadAllComponents];
    
    [self selectToday];
}

- (void)awakeFromNib
{
    [super awakeFromNib];
    
    componentWidth = 80;
    
    //    timeRow = 0;
    //    longTimeFormat = YES;
    
    self.seconds = [self nameOfSeconds];
    self.minutes = [self nameOfMinutes];
    self.todayIndexPath = [self todayPath];
    
    self.delegate = self;
    self.dataSource = self;
    
    [self selectToday];
}

- (NSInteger)getSecond
{
    NSInteger modul = 60;
        
    NSInteger second = [self selectedRowInComponent:SECOND] % modul;
        
    return second;
}

- (NSInteger)getMinute
{
    NSInteger minute = [self selectedRowInComponent:MINUTE] % 60;
    
    return minute;
}

- (void)setSecond:(NSInteger)second
{
    int row = (bigRowCount - (bigRowCount % 60)) / 2 + second;
    
    [self selectRow:row inComponent:SECOND animated:NO];
}

- (void)setMinute:(NSInteger)minute
{
    int row = (bigRowCount - (bigRowCount % 60)) / 2 + minute;
    
    [self selectRow:row inComponent:MINUTE animated:NO];
}

#pragma mark - UIPickerViewDelegate

- (UIView *)pickerView: (UIPickerView *)pickerView
            viewForRow: (NSInteger)row
          forComponent: (NSInteger)component
           reusingView: (UIView *)view
{
    UILabel *returnView = nil;
    
    if(view.tag == LABEL_TAG)
    {
        returnView = (UILabel *)view;
    }
    else
    {
        returnView = [self labelForComponent: component selected: NO];
    }
    
    returnView.text = [self titleForRow:row forComponent:component];
    
    return returnView;
}

- (CGFloat)pickerView:(UIPickerView *)pickerView rowHeightForComponent:(NSInteger)component
{
    return rowHeight;
}

#pragma mark - UIPickerViewDataSource

- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView
{
    return 2;
}

- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component
{
    if(component == SECOND)
    {
        return [self bigRowSecondCount];
    }
    
    return [self bigRowMinuteCount];
}

#pragma mark - Util

- (NSInteger)bigRowSecondCount
{
    return [self.seconds count]  * bigRowCount;
}

- (NSInteger)bigRowMinuteCount
{
    return [self.minutes count]  * bigRowCount;
}

- (NSString *)titleForRow:(NSInteger)row forComponent:(NSInteger)component
{
    if(component == SECOND)
    {
        NSInteger secondCount = [self.seconds count];
        return [[self.seconds objectAtIndex:(row % secondCount)] uppercaseString];
    }
    
    NSInteger minuteCount = [self.minutes count];
    return [self.minutes objectAtIndex:(row % minuteCount)];
}

- (CGFloat)pickerView:(UIPickerView *)pickerView widthForComponent:(NSInteger)component
{
    return componentWidth;
}

- (UILabel *)labelForComponent:(NSInteger)component selected:(BOOL)selected
{
    NSInteger width = componentWidth;
    
    CGRect frame = CGRectMake(0.f, 0.f, width, rowHeight);
    
    UILabel *label = [[UILabel alloc] initWithFrame:frame];
    label.textAlignment = NSTextAlignmentCenter;
    label.backgroundColor = [UIColor clearColor];
    label.textColor = [UIColor blackColor];
    label.font = [UIFont boldSystemFontOfSize:28.f];
    label.userInteractionEnabled = NO;
    
    label.tag = LABEL_TAG;
    
    return label;
}

- (NSArray *)nameOfSeconds
{
    NSMutableArray *secondsArray = [NSMutableArray array];
    
    NSInteger modul = 60;
    
    {
        for (int i = 0; i < modul; i++)
        {
            NSString *string = [NSString stringWithFormat:@"%02i", i];
            [secondsArray addObject:string];
        }
    }
    
    return secondsArray;
}

- (NSArray *)nameOfMinutes
{
    NSMutableArray *minutesArray = [NSMutableArray array];
    
    for (int i = 0; i < 60; i++)
    {
        NSString *string = [NSString stringWithFormat:@"%02i", i];
        [minutesArray addObject:string];
    }
    
    return minutesArray;
}

- (void)selectToday
{
    NSInteger row = [self selectedRowInComponent:SECOND] + [self bigRowSecondCount] / 2;
    NSInteger section = [self selectedRowInComponent:MINUTE] + [self bigRowMinuteCount] / 2;
    
    self.todayIndexPath = [NSIndexPath indexPathForRow:row inSection:section];
    
    [self selectRow: self.todayIndexPath.row
        inComponent: SECOND
           animated: NO];
    
    [self selectRow: self.todayIndexPath.section
        inComponent: MINUTE
           animated: NO];
}

- (NSIndexPath *)todayPath // row - month ; section - day
{
    CGFloat row = 0.f;
    CGFloat section = 0.f;
    
    NSString *second = @"00";
    NSString *minute  = @"00";
    
    //    timeRow = 0;
    
    //set table on the middle
    for(NSString *cellSecond in self.seconds)
    {
        if([cellSecond isEqualToString:second])
        {
            row = [self.seconds indexOfObject:cellSecond];
            row = row + [self bigRowSecondCount] / 2;
            break;
        }
    }
    
    for(NSString *cellMinute in self.minutes)
    {
        if([cellMinute isEqualToString:minute])
        {
            section = [self.minutes indexOfObject:cellMinute];
            section = section + [self bigRowMinuteCount] / 2;
            break;
        }
    }
    
    return [NSIndexPath indexPathForRow:row inSection:section];
}

- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component
{
    
}


@end
