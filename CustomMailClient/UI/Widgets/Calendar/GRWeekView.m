
#import "GRWeekView.h"
#import "GRWeekdayBarView.h"
#import "Event.h"               
#import <QuartzCore/QuartzCore.h> 
#import "GridView.h"            
#import "TapDetectingView.h"      
#import "GRHourView.h"
#import "Settings.h"

@implementation WeekCalendarSwipeGestureRecognizer

- (BOOL)canBePreventedByGestureRecognizer:(UIGestureRecognizer *)preventingGestureRecognizer
{
    return NO;
}

/**
 * Claim we will not prevent anyone else gesture recognizer from doing it's job.
 */
- (BOOL)canPreventGestureRecognizer:(UIGestureRecognizer *)preventedGestureRecognizer
{
    return NO;
}

@end


@interface GRWeekView ()

- (void)setupCustomInitialisation;
- (NSDate *)firstDayOfWeekFromDate:(NSDate *)date;
- (NSDate *)nextWeekFromDate:(NSDate *)date;
- (NSDate *)previousWeekFromDate:(NSDate *)date;
- (void)handleSwipeFrom:(UISwipeGestureRecognizer *)recognizer;

@property (nonatomic, retain) UIScrollView *scrollView;
@property (nonatomic, retain) UISwipeGestureRecognizer *swipeLeftRecognizer;
@property (nonatomic, retain) UISwipeGestureRecognizer *swipeRightRecognizer;

@end

@implementation GRWeekView

@synthesize delegate =_delegate;
@synthesize week;
@synthesize settings;
@synthesize gridView;
@synthesize scrollView;
@synthesize hourView;
@synthesize weekdayBarView;
@synthesize swipeLeftRecognizer;
@synthesize swipeRightRecognizer;

- (id)initWithFrame:(CGRect)frame 
{
    if (self = [super initWithFrame:frame]) 
    {
		[self setupCustomInitialisation];
    }
    
    return self;
}

- (id)initWithCoder:(NSCoder *)decoder 
{
	if (self = [super initWithCoder:decoder]) 
    {
		[self setupCustomInitialisation];
	}
    
	return self;
}

- (void)scrollTo7Hours
{
    [self.scrollView setContentOffset:CGPointMake(0, 300)];
}

- (void)setupCustomInitialisation 
{
	self.week = [NSDate date];
    
	[self addSubview:self.weekdayBarView];
	
	[self addSubview:self.scrollView];
	
	[self.scrollView addSubview:self.hourView];
	[self.scrollView addSubview:self.gridView];
	
	[self addGestureRecognizer:self.swipeLeftRecognizer];
	[self addGestureRecognizer:self.swipeRightRecognizer];
}

- (void)layoutSubviews 
{
    [super layoutSubviews];
    
    self.weekdayBarView.frame = CGRectMake(0, 0, self.bounds.size.width, 65);
    
	[self.weekdayBarView setNeedsLayout];
	
    NSInteger width = 114;
    
    if (!IS_IPAD)
    {
        width = 60;
    }
    
	self.hourView.frame = CGRectMake(0,
									 0, 
									 width,
									 65 * HOURS_IN_DAY);
	
	[self.hourView setNeedsLayout];
	
	self.scrollView.frame = CGRectMake(0,
									   self.weekdayBarView.bounds.size.height,
									   self.bounds.size.width,
									   self.bounds.size.height - self.weekdayBarView.bounds.size.height);
	
	self.scrollView.contentSize = CGSizeMake(CGRectGetWidth(self.bounds),
											 CGRectGetHeight(self.hourView.bounds));
	
	self.gridView.frame = CGRectMake(CGRectGetMaxX(self.hourView.bounds),
									 0,
									 CGRectGetWidth(self.bounds) - CGRectGetWidth(self.hourView.bounds),
									 CGRectGetHeight(self.hourView.bounds));
    [self.gridView setNeedsLayout];
}

- (UIScrollView *)scrollView 
{
	if (!_scrollView) 
    {
		_scrollView = [[UIScrollView alloc] init];
		_scrollView.backgroundColor      = [UIColor whiteColor];
		_scrollView.scrollEnabled        = TRUE;
		_scrollView.alwaysBounceVertical = TRUE;
		_scrollView.canCancelContentTouches = NO;
        _scrollView.bounces = NO;
	}
    
	return _scrollView;
}

- (void)setColorForEvent:(UIColor *)aColorForEvent
{
    _colorForEvent = aColorForEvent;
    
    self.gridView.colorForEvent = _colorForEvent;
}

- (GRHourView *)hourView 
{
	if (!hourView) 
    {
		hourView = [[GRHourView alloc] initWithFrame:CGRectZero];
		hourView.weekView        = self;
		hourView.backgroundColor = [UIColor clearColor];
		hourView.textColor       = RGBCOLOR(151, 151, 151);
	}
    
	return hourView;
}

- (GRWeekdayBarView *)weekdayBarView 
{
	if (!weekdayBarView) 
    {
		weekdayBarView = [[GRWeekdayBarView alloc] initWithFrame:CGRectZero];
		weekdayBarView.weekView        = self;
		weekdayBarView.backgroundColor = [UIColor clearColor];
		weekdayBarView.textColor       = RGBCOLOR(151, 151, 151);
		weekdayBarView.sundayColor     = RGBCOLOR(151, 151, 151);
		weekdayBarView.todayColor      = RGBCOLOR(151, 151, 151);
	}
    
	return weekdayBarView;
}

- (GridView *)gridView 
{
	if (!gridView)
    {		
		gridView = [[GridView alloc] initWithFrame:CGRectZero];
		gridView.backgroundColor = [UIColor whiteColor];
		gridView.rows            = HOURS_IN_DAY;
		gridView.columns         = DAYS_IN_WEEK;
	}
    
	return gridView;
}

- (UISwipeGestureRecognizer *)swipeLeftRecognizer 
{
	if (!_swipeLeftRecognizer) 
    {
		_swipeLeftRecognizer = [[WeekCalendarSwipeGestureRecognizer alloc] initWithTarget:self action:@selector(handleSwipeFrom:)];
		_swipeLeftRecognizer.direction = UISwipeGestureRecognizerDirectionLeft;
        _swipeLeftRecognizer.delegate = self;
	}
    
	return _swipeLeftRecognizer;
}

- (UISwipeGestureRecognizer *)swipeRightRecognizer 
{
	if (!_swipeRightRecognizer) 
    {
		_swipeRightRecognizer = [[WeekCalendarSwipeGestureRecognizer alloc] initWithTarget:self action:@selector(handleSwipeFrom:)];
		_swipeRightRecognizer.direction = UISwipeGestureRecognizerDirectionRight;
        _swipeRightRecognizer.delegate = self;
	}
    
	return _swipeRightRecognizer;
}

- (BOOL)gestureRecognizerShouldBegin:(UIGestureRecognizer *)gestureRecognizer
{
    if ([gestureRecognizer isKindOfClass:[WeekCalendarSwipeGestureRecognizer class]])
    {
        return YES;
    }
    
    return YES;
}

- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldRecognizeSimultaneouslyWithGestureRecognizer:(UIGestureRecognizer *)otherGestureRecognizer
{
    if ([gestureRecognizer isKindOfClass:[WeekCalendarSwipeGestureRecognizer class]])
    {
        return YES;
    }
    
    return YES;
}

- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldReceiveTouch:(UITouch *)touch
{
    if ([gestureRecognizer isKindOfClass:[WeekCalendarSwipeGestureRecognizer class]])
    {
        return YES;
    }
    
    return YES;
}

- (void)setDataSource:(id <MAWeekViewDataSource>)dataSource 
{
	_dataSource = dataSource;
	[self reloadData];
}

- (id <MAWeekViewDataSource>)dataSource 
{
	return _dataSource;
}

- (void)setWeek:(NSDate *)date 
{
    [week release];
    
	NSDate *firstOfWeek = [self firstDayOfWeekFromDate:date];
	week = [firstOfWeek retain];
	self.weekdayBarView.week = week;
	[self.weekdayBarView setNeedsLayout];
	
	[self reloadData];
	
	if ([self.delegate respondsToSelector:@selector(weekView:weekDidChange:)]) 
    {
        [self.delegate weekView:self weekDidChange:self.week];
	}
}

NSInteger Event_sortByStartTime(id ev1, id ev2, void *keyForSorting) {
	Event *event1 = (Event *)ev1;
	Event *event2 = (Event *)ev2;
    
	int v1 = [event1 minutesSinceMidnight];
	int v2 = [event2 minutesSinceMidnight];
	
	if (v1 < v2) {
		return NSOrderedAscending;
	} else if (v1 > v2) {
		return NSOrderedDescending;
	} else {
		/* Event start time is the same, compare by duration.
		 */
		int d1 = [event1 durationInMinutes];
		int d2 = [event2 durationInMinutes];
		
		if (d1 < d2) {
			/*
			 * Event with a shorter duration is after an event
			 * with a longer duration. Looks nicer when drawing the events.
			 */
			return NSOrderedDescending;
		} else if (d1 > d2) {
			return NSOrderedAscending;
		} else {
			/*
			 * The last resort: compare by title.
			 */
			return [event1.name compare:event2.name];
		}
	}
}

- (void)reloadData 
{	
	for (id view in self.gridView.subviews) 
    {
		if ([NSStringFromClass([view class])isEqualToString:@"GREventView"]) 
        {
			[view removeFromSuperview];
		}
	}
	
	size_t d = 0;
    
	for (NSDate *weekday in self.weekdayBarView.weekdays) 
    {
		NSArray *events = [self.dataSource weekView:self eventsForDate:weekday];
		
		for (id e in [events sortedArrayUsingFunction:Event_sortByStartTime context:NULL]) 
        {
			Event *event = e;
			
            [self.gridView addEventToOffset:d event:event weekView:self];
		}
        
		d++;
	}
}

- (void)changeWeekToday
{
    self.week = [NSDate date];
}

- (void)changeWeekRight
{
    self.week = [self nextWeekFromDate:week];
}

- (void)changeWeekLeft
{
    self.week = [self previousWeekFromDate:week];
}

- (void)handleSwipeFrom:(UISwipeGestureRecognizer *)recognizer
{
    if (recognizer.state == UIGestureRecognizerStateBegan)
    {
        startLocation = [recognizer locationInView:self];
    }
    else if (recognizer.state == UIGestureRecognizerStateEnded)
    {
        CGPoint stopLocation = [recognizer locationInView:self];
        CGFloat dx = stopLocation.x - startLocation.x;
        CGFloat dy = stopLocation.y - startLocation.y;
        CGFloat distance = sqrt(dx*dx + dy*dy );
        
        if (distance > 250)
        {
            if (recognizer.direction == UISwipeGestureRecognizerDirectionLeft)
            {
                [self changeWeekRight];
            }
            else  if (recognizer.direction == UISwipeGestureRecognizerDirectionRight)
            {
                [self changeWeekLeft];
            }
        }
    }
}

- (NSDate *)firstDayOfWeekFromDate:(NSDate *)date 
{
	NSDateComponents *components = [[AppDelegate getInstance].globalCalendar components:DATE_COMPONENTS fromDate:date];
    
    NSInteger firstDay = 2;
    
    if ([settings.weedStartsFrom intValue])
    {
        firstDay = 1;
    }
    
	[components setDay:([components day] - ([components weekday] - firstDay))];
	[components setHour:0];
	[components setMinute:0];
	[components setSecond:0];
	return [[AppDelegate getInstance].globalCalendar dateFromComponents:components];
}

- (NSDate *)nextWeekFromDate:(NSDate *)date 
{
	NSDateComponents *components = [[AppDelegate getInstance].globalCalendar components:DATE_COMPONENTS fromDate:date];
    
    NSInteger firstDay = 2;
    
    if ([settings.weedStartsFrom intValue])
    {
        firstDay = 1;
    }
    
	[components setDay:([components day] - ([components weekday] - firstDay - 7))];
	[components setHour:0];
	[components setMinute:0];
	[components setSecond:0];
	return [[AppDelegate getInstance].globalCalendar dateFromComponents:components];
}

- (NSDate *)previousWeekFromDate:(NSDate *)date 
{
	NSDateComponents *components = [[AppDelegate getInstance].globalCalendar components:DATE_COMPONENTS fromDate:date];
    
    NSInteger firstDay = 2;
    
    if ([settings.weedStartsFrom intValue])
    {
        firstDay = 1;
    }
    
	[components setDay:([components day] - ([components weekday] - firstDay + 7))];
	[components setHour:0];
	[components setMinute:0];
	[components setSecond:0];
	return [[AppDelegate getInstance].globalCalendar dateFromComponents:components];
}

- (void)dealloc
{
    self.settings = nil;
    self.week = nil;
    self.gridView = nil;
    self.scrollView = nil;
    self.hourView = nil;
    self.weekdayBarView = nil;
    self.swipeLeftRecognizer = nil;
    self.swipeRightRecognizer = nil;
    
    [super dealloc];
}

@end
