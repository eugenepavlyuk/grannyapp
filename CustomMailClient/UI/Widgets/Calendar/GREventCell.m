//
//  GREventCell.m
//  GrannyApp
//
//  Created by Eugene Pavlyuk on 9/21/12.
//  Copyright (c) 2012 Home. All rights reserved.
//

#import "GREventCell.h"

@implementation GREventCell

@synthesize leftBorderImageView;
@synthesize lowBorderImageView;
@synthesize backgroundImageView;
@synthesize currentDay;

- (id)initWithFrame:(CGRect)frame
{
    if (self = [super initWithFrame:frame])
    {
        self.backgroundImageView = [[[UIImageView alloc] init] autorelease];
        self.backgroundImageView.backgroundColor = RGBCOLOR(231, 239, 237);
        [self addSubview:self.backgroundImageView];
        
        self.leftBorderImageView = [[[UIImageView alloc] init] autorelease];
        self.leftBorderImageView.backgroundColor = RGBCOLOR(190, 190, 190);
        [self addSubview:self.leftBorderImageView];
        
        self.lowBorderImageView = [[[UIImageView alloc] init] autorelease];
        self.lowBorderImageView.backgroundColor = RGBCOLOR(190, 190, 190);
        [self addSubview:self.lowBorderImageView];
    }
    
    return self;
}

- (void)layoutSubviews
{
    [super layoutSubviews];
    
    self.leftBorderImageView.frame = CGRectMake(0, 0, 1, self.bounds.size.height);
    self.lowBorderImageView.frame = CGRectMake(0, self.bounds.size.height - 1, self.bounds.size.width, 1);
    
    self.backgroundImageView.frame = self.bounds;
}

- (void)dealloc
{
    self.leftBorderImageView = nil;
    self.lowBorderImageView = nil;
    self.backgroundImageView = nil;
    [super dealloc];
}

- (void)setCurrentDay:(BOOL)bCurrentDay
{
    currentDay = bCurrentDay;
    
    self.backgroundImageView.backgroundColor = self.todayBackgroundColor;
    
    if (currentDay)
    {
        self.backgroundImageView.hidden = NO;
    }
    else
    {
        self.backgroundImageView.hidden = YES;
    }
}

@end
