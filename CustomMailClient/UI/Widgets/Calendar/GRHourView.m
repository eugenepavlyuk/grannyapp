//
//  MAHourView.m
//  GrannyApp
//
//  Created by Eugene Pavlyuk on 9/20/12.
//  Copyright (c) 2012 Home. All rights reserved.
//

#import "GRHourView.h"
#import "GRWeekView.h"
#import "GridView.h"
#import "GRHourCell.h"
#import "DataManager.h"
#import "Settings.h"

static NSString * HOURS_AM_PM[] = {
	@" 12 AM", @" 1 AM", @" 2 AM", @" 3 AM", @" 4 AM", @" 5 AM", @" 6 AM", @" 7 AM", @" 8 AM", @" 9 AM", @" 10 AM", @" 11 AM",
	@" noon ", @" 1 PM", @" 2 PM", @" 3 PM", @" 4 PM", @" 5 PM", @" 6 PM", @" 7 PM", @" 8 PM", @" 9 PM", @" 10 PM", @" 11 PM", @" 12 PM"
};

static NSString * HOURS_24[] = {
	@" 0:00", @" 1:00", @" 2:00", @" 3:00", @" 4:00", @" 5:00", @" 6:00", @" 7:00", @" 8:00", @" 9:00", @" 10:00", @" 11:00",
	@" 12:00", @" 13:00", @" 14:00", @" 15:00", @" 16:00", @" 17:00", @" 18:00", @" 19:00", @" 20:00", @" 21:00", @" 22:00", @" 23:00", @" 24:00"
};

@implementation GRHourView
{
    NSMutableArray *hoursCells;
}

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    
    if (self)
    {
        hoursCells = [[NSMutableArray array] retain];
        
        for (int i = 0; i < HOURS_IN_DAY; i++) 
        {
            GRHourCell *cell = [[[GRHourCell alloc] initWithFrame:CGRectZero] autorelease];
            
            [hoursCells addObject:cell];
            
            [self addSubview:cell];
        }
    }
    
    return self;
}

- (BOOL)timeIs24HourFormat 
{
//	NSDateFormatter *formatter = [[[NSDateFormatter alloc] init] autorelease];
//	[formatter setDateStyle:NSDateFormatterNoStyle];
//	[formatter setTimeStyle:NSDateFormatterShortStyle];
//	NSString *dateString = [formatter stringFromDate:[NSDate date]];
//	NSRange amRange = [dateString rangeOfString:[formatter AMSymbol]];
//	NSRange pmRange = [dateString rangeOfString:[formatter PMSymbol]];
//	BOOL is24Hour = amRange.location == NSNotFound && pmRange.location == NSNotFound;
    
    Settings *settings = [[DataManager sharedInstance] getEntityWithName:kSettingsEntityName];
    
    if ([settings.timeFormat intValue])
    {
        return YES;
    }
    
	return NO;
}

- (void)layoutSubviews
{
    [super layoutSubviews];
    
    register unsigned int i;
    
	const CGFloat cellHeight = self.weekView.gridView.cellHeight;
	NSString **HOURS = ([self timeIs24HourFormat] ? HOURS_24 : HOURS_AM_PM);
	
	for (i = 0; i < HOURS_IN_DAY; i++)
    {
		CGRect rect = CGRectMake(0,
								 (cellHeight * i) + [self.yOffset integerValue],
								 self.bounds.size.width,
								 cellHeight);
        
        GRHourCell *cell = [hoursCells objectAtIndex:i];
        
//        if (i <= 12)
//        {
            cell.label.text = HOURS[i];
        
        cell.label.textColor = self.textColor;
        cell.label.font = self.textFont;
        
//        }
//        else
//        {
//            cell.label.textColor = [UIColor colorWithRed:95.f/255.f green:44.f/255.f blue:41.f/255.f alpha:1.f];
//        }
        
        cell.frame = rect;
	}
}

- (void)dealloc
{
    self.weekView = nil;
    self.textFont = nil;
    self.textColor = nil;
    
    [hoursCells release];
    
    [super dealloc];
}

@end
