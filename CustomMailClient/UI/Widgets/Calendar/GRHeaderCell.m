//
//  GRHeaderCell.m
//  GrannyApp
//
//  Created by Eugene Pavlyuk on 9/20/12.
//  Copyright (c) 2012 Home. All rights reserved.
//

#import "GRHeaderCell.h"

@implementation GRHeaderCell

@synthesize label;
@synthesize currentDay;
@synthesize leftBorder;
@synthesize currentDayShelf;
@synthesize foregroundImageView;

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    
    if (self)
    {
        self.foregroundImageView = [[[UIImageView alloc] init] autorelease];
        self.foregroundImageView.backgroundColor = self.todayBackgroundColor;
        [self addSubview:self.foregroundImageView];
        
        self.label = [[[UILabel alloc] initWithFrame:frame] autorelease];
        self.label.backgroundColor = [UIColor clearColor];
        self.label.textAlignment = NSTextAlignmentCenter;
        self.label.shadowColor = [UIColor clearColor];
        self.label.font = self.titleFont;
        [self addSubview:self.label];
        
        self.leftBorder = [[[UIImageView alloc] init] autorelease];
        self.leftBorder.backgroundColor = RGBCOLOR(151, 151, 151);
        [self addSubview:self.leftBorder];
        
        self.currentDayShelf = [[[UIImageView alloc] init] autorelease];
        [self addSubview:self.currentDayShelf];
        
        self.currentDayShelf.hidden = YES;
    }
    
    return self;
}

- (void)didMoveToWindow
{
    self.label.font = self.titleFont;
}

- (void)dealloc
{
    self.label = nil;
    self.leftBorder = nil;
    self.currentDayShelf = nil;
    self.foregroundImageView = nil;
    self.todayBackgroundColor = nil;
    self.todayTitleColor = nil;
    [super dealloc];
}

- (void)layoutSubviews
{
    [super layoutSubviews];
    
    self.label.frame = self.bounds;
    self.currentDayShelf.frame = CGRectMake(0, self.bounds.size.height - 5, self.bounds.size.width, 5);
    self.leftBorder.frame = CGRectMake(0, 0, 1, self.bounds.size.height);
    self.foregroundImageView.frame = self.bounds;
}

- (void)setCurrentDay:(BOOL)bCurrentDay
{
    currentDay = bCurrentDay;
    
    self.foregroundImageView.backgroundColor = self.todayBackgroundColor;
    //self.label.textColor = self.todayTitleColor;
    
    if (currentDay)
    {
        self.currentDayShelf.hidden = NO;
        self.foregroundImageView.hidden = NO;
    }
    else
    {
        self.currentDayShelf.hidden = YES;
        self.foregroundImageView.hidden = YES;
    }
}

@end
