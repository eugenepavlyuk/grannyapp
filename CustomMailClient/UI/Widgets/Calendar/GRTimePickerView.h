//
//  GRTimePickerView.h
//  GrannyApp
//
//  Created by Eugene Pavluk on 10/16/12.
//
//

#import <UIKit/UIKit.h>

@interface GRTimePickerView : UIPickerView <UIPickerViewDelegate, UIPickerViewDataSource>
{
//    NSInteger timeRow;
}

//@property (nonatomic, assign) BOOL longTimeFormat;
@property (nonatomic, assign) NSInteger componentWidth;

- (void)reset;
- (void)selectToday;

- (NSInteger)getHour;
- (NSInteger)getMinute;

- (void)setHour:(NSInteger)hour;
- (void)setMinute:(NSInteger)minute;

@end
