//
//  GRPhotoView.h
//  Granny
//
//  Created by Ievgen Pavliuk on 3/28/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol PhotoLoadingOperationDelegate <NSObject>

- (void)fullScreenImageLoaded:(UIImage *)image;

@end

@interface GRPhotoLoadOperation : NSOperation {
    BOOL      finished;
	BOOL      executing;
    id<PhotoLoadingOperationDelegate> delegate;
    ALAsset *asset;
}
@property (nonatomic, assign) id<PhotoLoadingOperationDelegate> delegate;
@property (nonatomic, retain) ALAsset *asset;

- (void)startOperation;
@end

@interface GRPhotoView : UIView <UIScrollViewDelegate, PhotoLoadingOperationDelegate> {
    UIScrollView *contentView;
    NSOperationQueue *queue;
    GRPhotoLoadOperation *operation;
}

- (id)initWithAsset:(ALAsset *)asset frame:(CGRect)frame;
- (void)positioningImageView;
@end
