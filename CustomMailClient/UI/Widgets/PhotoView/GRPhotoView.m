//
//  GRPhotoView.m
//  Granny
//
//  Created by Ievgen Pavliuk on 3/28/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "GRPhotoView.h"
#import <QuartzCore/QuartzCore.h>

@implementation GRPhotoLoadOperation

@synthesize delegate;
@synthesize asset;

#pragma mark Operation managment


- (BOOL)isExecuting {
	return executing;
}

- (BOOL)isFinished {
	return finished;
}

- (void)main {
	NSAutoreleasePool *pool = [[NSAutoreleasePool alloc] init];
    [self startOperation];
	while (!finished) {
		[NSThread sleepForTimeInterval:0.01f];
	}
	[pool release];
}

static void addRoundedRectToPath(CGContextRef context, CGRect rect, float ovalWidth, float ovalHeight)
{
    float fw, fh;
    
    if (ovalWidth == 0 || ovalHeight == 0) {
        CGContextAddRect(context, rect);
        return;
    }
    CGContextSaveGState(context);
    CGContextTranslateCTM (context, CGRectGetMinX(rect), CGRectGetMinY(rect));
    CGContextScaleCTM (context, ovalWidth, ovalHeight);
    fw = CGRectGetWidth (rect) / ovalWidth;
    fh = CGRectGetHeight (rect) / ovalHeight;
    CGContextMoveToPoint(context, fw, fh/2);
    CGContextAddArcToPoint(context, fw, fh, fw/2, fh, 1);
    CGContextAddArcToPoint(context, 0, fh, 0, fh/2, 1);
    CGContextAddArcToPoint(context, 0, 0, fw/2, 0, 1);
    CGContextAddArcToPoint(context, fw, 0, fw, fh/2, 1);
    CGContextClosePath(context);
    CGContextRestoreGState(context);
}

- (UIImage *) roundCorners: (UIImage*) img
{
    int w = img.size.width;
    int h = img.size.height;
    
    CGColorSpaceRef colorSpace = CGColorSpaceCreateDeviceRGB();
    CGContextRef context = CGBitmapContextCreate(NULL, w, h, 8, 4 * w, colorSpace, kCGBitmapAlphaInfoMask);
    
    CGContextBeginPath(context);
    CGRect rect = CGRectMake(0, 0, img.size.width, img.size.height);
    addRoundedRectToPath(context, rect, 15, 15);
    CGContextClosePath(context);
    CGContextClip(context);
    
    CGContextDrawImage(context, CGRectMake(0, 0, w, h), img.CGImage);
    
    CGImageRef imageMasked = CGBitmapContextCreateImage(context);
    CGContextRelease(context);
    CGColorSpaceRelease(colorSpace);
    
    return [UIImage imageWithCGImage:imageMasked];
}

- (void)startOperation {
    UIImage *image;
    if (IS_IPAD) {
        image = [UIImage imageWithCGImage:[[self.asset defaultRepresentation] fullScreenImage]];
    } else {
        image = [UIImage imageWithCGImage:[[self.asset defaultRepresentation]fullScreenImage]];
    }
    
    //image = [self roundCorners:image];
    
    [self performSelectorOnMainThread:@selector(callOperationCompleted:) withObject:image waitUntilDone:NO];
    finished = YES;
}

- (void)callOperationCompleted:(UIImage *)image {
    if (delegate != nil) {
        if ([delegate respondsToSelector:@selector(fullScreenImageLoaded:)]) {
            [delegate performSelector:@selector(fullScreenImageLoaded:) withObject:image];
        }        
    }
}

- (void)dealloc {
    delegate = nil;
    [asset release];
    [super dealloc];
}
@end

@implementation GRPhotoView

- (id)initWithAsset:(ALAsset *)asset frame:(CGRect)frame 
{
    if ((self = [super initWithFrame:frame])) 
    {
        self.clipsToBounds = NO;
        self.layer.cornerRadius = 15.f;
        self.layer.shadowOpacity = 0.7f;
        self.layer.shadowRadius = 15.f;
        self.layer.shadowColor = [UIColor blackColor].CGColor;
        self.layer.shadowRadius = 10.f;
        
        contentView = [[UIScrollView alloc] initWithFrame:CGRectMake(20, 0, frame.size.width - 40, frame.size.height)];
        contentView.pagingEnabled = NO;
        contentView.showsHorizontalScrollIndicator = YES;
        contentView.showsVerticalScrollIndicator = YES;
        contentView.scrollsToTop = NO;
        contentView.delegate = self;
        contentView.minimumZoomScale = 1.0;
        contentView.maximumZoomScale = 2.0;
        
        //contentView.layer.cornerRadius = 15.f;
        contentView.clipsToBounds = YES;

        contentView.backgroundColor = [UIColor clearColor];
        [self addSubview:contentView];

        UIImage *image = [UIImage imageWithCGImage:[asset aspectRatioThumbnail]];
        UIImageView *imageView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, image.size.width, image.size.height)];
//        imageView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
        imageView.autoresizingMask = UIViewAutoresizingNone;
        imageView.contentMode = UIViewContentModeScaleAspectFit;
        imageView.tag = 555;
        [imageView setImage:image];
        [contentView addSubview:imageView];
        imageView.center = CGPointMake(contentView.bounds.size.width / 2, contentView.bounds.size.height / 2);
        
//        imageView.layer.cornerRadius = 15.f;
        imageView.clipsToBounds = YES;
        imageView.backgroundColor = [UIColor clearColor];
        
        //[self positioningImageView];
        
        queue = [[NSOperationQueue alloc] init];
        operation = [[GRPhotoLoadOperation alloc] init];
        operation.asset = asset;
        operation.delegate = self;
        [queue addOperation:operation];
        
//        imageView.frame = [self centeredFrameForScrollView:contentView andUIView:imageView];
        
        [imageView release];
        imageView = nil;
    }
    return self;
}

- (void)dealloc {
    [contentView release];
    operation.delegate = nil;
    [operation release];
    [queue cancelAllOperations];
    [queue release];
    [super dealloc];
}

- (void)layoutSubviews {
    [super layoutSubviews];
    [self positioningImageView];
    
    UIImageView *imageView = (UIImageView *)[contentView viewWithTag:555];
    [self centeredFrameForScrollView:contentView andUIView:imageView];
}

- (void)positioningImageView {
    [contentView setZoomScale:1.0f];

    contentView.frame = CGRectMake(20, 0, self.frame.size.width - 40, self.frame.size.height);
    contentView.contentSize = CGSizeMake(self.frame.size.width - 40, self.frame.size.height);

    UIImageView *imageView = (UIImageView *)[contentView viewWithTag:555];
    CGRect iRect;
    
    if (IS_IPAD) 
    {
        if (self.frame.size.width < self.frame.size.height) 
        {
            if (imageView.frame.size.width > imageView.frame.size.height) 
            {
                iRect = CGRectMake(0, (self.frame.size.height - self.frame.size.width)/2, self.frame.size.width, self.frame.size.height/2);
            } else {
                iRect = CGRectMake(0, 0, self.frame.size.width, self.frame.size.height);
            }
        } else {
            if (imageView.frame.size.width > imageView.frame.size.height) {
                iRect = CGRectMake(0, 0, self.frame.size.width, self.frame.size.height);
            } else {
                iRect = CGRectMake(0, 0, self.frame.size.width, self.frame.size.height)        ;
            }
        }
    } 
    else 
    {
        if (self.frame.size.width < self.frame.size.height) {
            if (imageView.frame.size.width > imageView.frame.size.height) {
                iRect = CGRectMake(0, (self.frame.size.height - self.frame.size.width)/2, self.frame.size.width, self.frame.size.height/2);
            } else {
                iRect = CGRectMake(0, 0, self.frame.size.width, self.frame.size.height);
            }
        } else {
            if (imageView.frame.size.width > imageView.frame.size.height) {
                iRect = CGRectMake(0, 0, self.frame.size.width, self.frame.size.height);
            } else {
                iRect = CGRectMake((self.frame.size.width - self.frame.size.height)/2, 0, self.frame.size.width/2, self.frame.size.height)        ;
            }
        }
        
    }
    imageView.frame = iRect;
    imageView.center = CGPointMake(contentView.bounds.size.width / 2, contentView.bounds.size.height / 2);
}

#pragma mark - GRPhotoLoadOperation delegate

- (void)fullScreenImageLoaded:(UIImage *)image {
    UIImageView *imageView = (UIImageView *)[contentView viewWithTag:555];
    if (imageView) {
        [imageView setImage:image];
    }
}

#pragma mark - UIScrollViewDelegate

- (UIView *)viewForZoomingInScrollView:(UIScrollView *)scrollView {
    
    return [contentView viewWithTag:555];
    
}

//- (void)scrollViewDidEndZooming:(UIScrollView *)scrollView withView:(UIView *)view atScale:(float)scale {
//    
//    [scrollView setZoomScale:scale+0.01 animated:NO];
//    
//    [scrollView setZoomScale:scale animated:NO];
//    
//}

- (CGRect)centeredFrameForScrollView:(UIScrollView *)scroll andUIView:(UIView *)rView {
    CGSize boundsSize = scroll.bounds.size;
    CGRect frameToCenter = rView.frame;
    // center horizontally
    if (frameToCenter.size.width < boundsSize.width) {
        frameToCenter.origin.x = (boundsSize.width - frameToCenter.size.width) / 2;
    }
    else {
        frameToCenter.origin.x = 0;
    }
    // center vertically
    if (frameToCenter.size.height < boundsSize.height) {
        frameToCenter.origin.y = (boundsSize.height - frameToCenter.size.height) / 2;
    }
    else {
        frameToCenter.origin.y = 0;
    }
    return frameToCenter;
}

- (void)scrollViewDidZoom:(UIScrollView *)scrollView {
    UIImageView *imageView = (UIImageView *)[contentView viewWithTag:555];
    imageView.frame = [self centeredFrameForScrollView:scrollView andUIView:imageView];
}

- (void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    UIImageView *imageView = (UIImageView *)[contentView viewWithTag:555];
    imageView.frame = [self centeredFrameForScrollView:scrollView andUIView:imageView];
}

@end
