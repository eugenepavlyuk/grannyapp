//
//  GRPhotoPreview.m
//  Granny
//
//  Created by Ievgen Pavliuk on 3/28/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "GRPhotoPreview.h"
#import <QuartzCore/QuartzCore.h>

@implementation GRPhotoPreview

@synthesize index;
@synthesize delegate;

- (id)initWithImage:(UIImage *)image frame:(CGRect)frame 
{
    if ((self = [super initWithFrame:frame])) 
    {
        self.userInteractionEnabled = YES;
        self.clipsToBounds = NO;
        
        self.backgroundColor = [UIColor whiteColor];
        
        self.layer.cornerRadius = 15.f;
        self.layer.shadowRadius = 4.f;
        self.layer.shadowColor = [UIColor blackColor].CGColor;
        self.layer.shadowOpacity = 0.7f;
        
        self.imageView = [[UIImageView alloc] initWithImage:image];
        self.imageView.frame = CGRectMake(0, 0, frame.size.width, frame.size.height);
        [self addSubview:self.imageView];
        self.imageView.backgroundColor = [UIColor clearColor];
        self.imageView.contentMode = UIViewContentModeScaleAspectFit;
        
        if (IS_IPAD)
        {
            self.imageView.layer.cornerRadius = 15.f;
        }
        else
        {
            self.imageView.layer.cornerRadius = 5.f;
        }
        
        self.imageView.clipsToBounds = YES;
        [self.imageView release];
    }
    
    return self;
}

- (id)initForPhotoviewerWithImage:(UIImage *)image frame:(CGRect)frame
{
    if ((self = [super initWithFrame:frame])) 
    {
        self.userInteractionEnabled = YES;
        self.clipsToBounds = NO;
        
        self.backgroundColor = [UIColor clearColor];
                
        self.imageView = [[UIImageView alloc] initWithImage:image];
        self.imageView.frame = CGRectMake(0, 0, frame.size.width, frame.size.height);
        [self addSubview:self.imageView];
        self.imageView.backgroundColor = [UIColor clearColor];
        self.imageView.contentMode = UIViewContentModeScaleAspectFit;
        self.imageView.clipsToBounds = YES;
        [self.imageView release];
    }
    
    return self;
}

- (id)initForAttachemntWithImage:(UIImage *)image frame:(CGRect)frame {
    if ((self = [super initWithFrame:frame])) 
    {
        self.userInteractionEnabled = YES;
        self.backgroundColor = [UIColor clearColor];
        
        self.layer.cornerRadius = 15.f;
        
        self.imageView = [[UIImageView alloc] initWithImage:image];
        self.imageView.frame = CGRectMake(0, 0, frame.size.width, frame.size.height);
        [self addSubview:self.imageView];
        self.imageView.clipsToBounds = YES;
        self.imageView.layer.cornerRadius = 15.f;
        [self.imageView release];
    }
    
    return self;
}
 


- (void)dealloc
{
    delegate = nil;
    self.imageView = nil;
    [super dealloc];
}

#pragma mark - UserInteraction

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
}

- (void)touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event {
    [delegate photoPreviewSelectedWithIndex:self.index];
}

@end
