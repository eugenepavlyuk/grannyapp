//
//  GRPhotoPreview.h
//  Granny
//
//  Created by Ievgen Pavliuk on 3/28/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol GRPhotoPreviewDelegate <NSObject>

- (void)photoPreviewSelectedWithIndex:(NSInteger)index;

@end


@interface GRPhotoPreview : UIView 
{    
    NSInteger index;
    id<GRPhotoPreviewDelegate> delegate;
}

@property (nonatomic, assign) NSInteger index;
@property (nonatomic, assign) id<GRPhotoPreviewDelegate> delegate;
@property (nonatomic, retain) UIImageView *imageView;

- (id)initWithImage:(UIImage *)image frame:(CGRect)frame;
- (id)initForAttachemntWithImage:(UIImage *)image frame:(CGRect)frame;
- (id)initForPhotoviewerWithImage:(UIImage *)image frame:(CGRect)frame;

@end
