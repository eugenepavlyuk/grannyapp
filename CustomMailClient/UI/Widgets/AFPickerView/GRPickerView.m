//
//  GRPickerView.m
//  Granny App
//
//  Created by Eugene Pavlyuk on 24.11.11.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "GRPickerView.h"

#define kRowHeightiPad  38


@implementation GRPickerView

#pragma mark - Synthesization

@synthesize background;
@synthesize dataSource;
@synthesize delegate;
@synthesize selectedRow = currentRow;
@synthesize rowFont = _rowFont;

#pragma mark - Custom getters/setters

- (void)setSelectedRow:(int)selectedRow
{
    if (selectedRow >= rowsCount)
        return;
    
    currentRow = selectedRow;
    
    float height = kRowHeightiPad;
    
//    if (!IS_IPAD)
//    {
//        height = 38;
//    }
    
    [contentView setContentOffset:CGPointMake(0.0, height * currentRow) animated:NO];
}

- (void)setRowFont:(UIFont *)rowFont
{
    [_rowFont release];
    
    _rowFont = [rowFont retain];
    
    for (UILabel *aLabel in visibleViews) 
    {
        aLabel.font = _rowFont;
    }
    
    for (UILabel *aLabel in recycledViews) 
    {
        aLabel.font = _rowFont;
    }
}

#pragma mark - Initialization

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) 
    {
        // setup
        [self setup];
        
        // backgound
        self.background = [[[UIImageView alloc] initWithFrame:self.bounds] autorelease];
        [self addSubview:self.background];
        
        // content
        contentView = [[[UIScrollView alloc] initWithFrame:CGRectMake(0.0, 2.0, frame.size.width, frame.size.height)] autorelease];
        contentView.showsHorizontalScrollIndicator = NO;
        contentView.showsVerticalScrollIndicator = NO;
        contentView.delegate = self;
        [self addSubview:contentView];
        
        UITapGestureRecognizer *tapRecognizer = [[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(didTap:)] autorelease];
        [contentView addGestureRecognizer:tapRecognizer];
        
        // glass
        UIImage *glassImage = [[UIImage imageNamed:@"picker_glass_ipad.png"] stretchableImageWithLeftCapWidth:100 topCapHeight:0];
        if (IS_IPAD)
        {
            glassImageView = [[[UIImageView alloc] initWithFrame:CGRectMake(0.0, 76.0, frame.size.width, glassImage.size.height)] autorelease];
        }
        else
        {
            glassImageView = [[[UIImageView alloc] initWithFrame:CGRectMake(0.0, 76.0, frame.size.width, glassImage.size.height)] autorelease];
        }
        glassImageView.image = glassImage;
        [self addSubview:glassImageView];
    }
    return self;
}




- (void)setup
{
    _rowFont = [[UIFont boldSystemFontOfSize:28.0] retain];
    
    currentRow = 0;
    rowsCount = 0;
    visibleViews = [[NSMutableSet alloc] init];
    recycledViews = [[NSMutableSet alloc] init];
}

- (void)dealloc
{
    [visibleViews release];
    [recycledViews release];
    
    self.dataSource = nil;
    self.delegate = nil;
    self.background = nil;
    
    [_rowFont release];
    [super dealloc];
}


#pragma mark - Buisness

- (void)reloadData
{
    // empry views
    currentRow = 0;
    rowsCount = 0;
    
    for (UIView *aView in visibleViews) 
        [aView removeFromSuperview];
    
    for (UIView *aView in recycledViews)
        [aView removeFromSuperview];
    
    [visibleViews release];
    [recycledViews release];
    
    visibleViews = [[NSMutableSet alloc] init];
    recycledViews = [[NSMutableSet alloc] init];
    
    rowsCount = [dataSource numberOfRowsInPickerView:self];
    [contentView setContentOffset:CGPointMake(0.0, 0.0) animated:NO];
    
//    if (IS_IPAD)
//    {
        contentView.contentSize = CGSizeMake(contentView.frame.size.width, kRowHeightiPad * rowsCount + 4 * kRowHeightiPad);    
//    }
//    else
//    {
//        contentView.contentSize = CGSizeMake(contentView.frame.size.width, kRowHeightiPad * rowsCount + 2 * kRowHeightiPad);    
//    }
    
    [self tileViews];
}




- (void)determineCurrentRow
{
    float height = kRowHeightiPad;
    
//    if (!IS_IPAD)
//    {
//        height = 30;
//    }
    
    CGFloat delta = contentView.contentOffset.y;
    int position = round(delta / height);
    currentRow = position;
    [contentView setContentOffset:CGPointMake(0.0, height * position) animated:YES];
    [delegate pickerView:self didSelectRow:currentRow];
}




- (void)didTap:(id)sender
{
    float height = kRowHeightiPad;
    
//    if (!IS_IPAD)
//    {
//        height = 30;
//    }
    
    UITapGestureRecognizer *tapRecognizer = (UITapGestureRecognizer *)sender;
    CGPoint point = [tapRecognizer locationInView:self];
    int steps = floor(point.y / height) - 2;
    [self makeSteps:steps];
}




- (void)makeSteps:(int)steps
{
    if (steps == 0 || steps > 2 || steps < -2)
        return;
    
    float height = kRowHeightiPad;
    
//    if (!IS_IPAD)
//    {
//        height = 30;
//    }
    
    [contentView setContentOffset:CGPointMake(0.0, height * currentRow) animated:NO];
    
    int newRow = currentRow + steps;
    if (newRow < 0 || newRow >= rowsCount)
    {
        if (steps == -2)
            [self makeSteps:-1];
        else if (steps == 2)
            [self makeSteps:1];
        
        return;
    }
    
    currentRow = currentRow + steps;
    [contentView setContentOffset:CGPointMake(0.0, height * currentRow) animated:YES];
    [delegate pickerView:self didSelectRow:currentRow];
}




#pragma mark - recycle queue

- (UIView *)dequeueRecycledView
{
	UIView *aView = [recycledViews anyObject];
	
    if (aView) 
    {
        [aView retain];
        [recycledViews removeObject:aView];
    }
    
    return aView;
}



- (BOOL)isDisplayingViewForIndex:(NSUInteger)index
{
    float height = kRowHeightiPad;
    
//    if (!IS_IPAD)
//    {
//        height = 30;
//    }
    
	BOOL foundPage = NO;
    for (UIView *aView in visibleViews) 
	{
        int viewIndex = aView.frame.origin.y / height - 2;
        if (viewIndex == index) 
		{
            foundPage = YES;
            break;
        }
    }
    return foundPage;
}




- (void)tileViews
{
    float height = kRowHeightiPad;
    
//    if (!IS_IPAD)
//    {
//        height = 30;
//    }
    
    // Calculate which pages are visible
    CGRect visibleBounds = contentView.bounds;
    int firstNeededViewIndex = floorf(CGRectGetMinY(visibleBounds) / height) - 2;
    int lastNeededViewIndex  = floorf((CGRectGetMaxY(visibleBounds) / height)) - 2;
    firstNeededViewIndex = MAX(firstNeededViewIndex, 0);
    lastNeededViewIndex  = MIN(lastNeededViewIndex, rowsCount - 1);
	    
    // Recycle no-longer-visible pages 
	for (UIView *aView in visibleViews) 
    {
        int viewIndex = aView.frame.origin.y / height - 2;
        if (viewIndex < firstNeededViewIndex || viewIndex > lastNeededViewIndex) 
        {
            [recycledViews addObject:aView];
            [aView removeFromSuperview];
        }
    }
    
    [visibleViews minusSet:recycledViews];
    
    // add missing pages
	for (int index = firstNeededViewIndex; index <= lastNeededViewIndex; index++) 
	{
        if (![self isDisplayingViewForIndex:index]) 
		{
            UILabel *label = (UILabel *)[self dequeueRecycledView];
            
			if (label == nil)
            {
				label = [[[UILabel alloc] initWithFrame:CGRectMake(0, 0, self.frame.size.width, height)] autorelease];
                label.backgroundColor = [UIColor clearColor];
                label.font = self.rowFont;
                label.textAlignment = NSTextAlignmentCenter;
                label.textColor = RGBACOLOR(0.0, 0.0, 0.0, 0.75);
                
                [label retain];
            }
            
            [self configureView:label atIndex:index];
            [contentView addSubview:label];
            [visibleViews addObject:label];
            
            [label release];
        }
    }
}




- (void)configureView:(UIView *)view atIndex:(NSUInteger)index
{
    float height = kRowHeightiPad;
//    
//    if (!IS_IPAD)
//    {
//        height = 30;
//    }
    
    UILabel *label = (UILabel *)view;
    label.text = [dataSource pickerView:self titleForRow:index];
    CGRect frame = label.frame;
    
    //label.backgroundColor = [UIColor blackColor];
    
//    if (IS_IPAD)
//    {
        frame.origin.y = height * index + 78.0;
//    }
//    else
//    {
//        frame.origin.y = height * index + 60.0;
//    }
    
    label.frame = frame;
}




#pragma mark - UIScrollViewDelegate

- (void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    [self tileViews];
}




- (void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate
{
    if (!decelerate)
        [self determineCurrentRow];
}




- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView
{
    [self determineCurrentRow];
}

@end
