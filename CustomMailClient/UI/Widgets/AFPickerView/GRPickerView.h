//
//  GRPickerView.h
//  Granny App
//
//  Created by Eugene Pavlyuk on 24.11.11.
//  Copyright (c) 2011 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <QuartzCore/QuartzCore.h>


@protocol AFPickerViewDataSource;
@protocol AFPickerViewDelegate;

@interface GRPickerView : UIView <UIScrollViewDelegate>
{
    id <AFPickerViewDataSource> dataSource;
    id <AFPickerViewDelegate> delegate;
    UIScrollView *contentView;
    UIImageView *glassImageView;
    
    int currentRow;
    int rowsCount; 
    
    CGPoint previousOffset;
    BOOL isScrollingUp;
    
    // recycling
    NSMutableSet *recycledViews;
    NSMutableSet *visibleViews;
    
    UIFont *_rowFont;
}

@property (nonatomic, assign) id <AFPickerViewDataSource> dataSource;
@property (nonatomic, assign) id <AFPickerViewDelegate> delegate;
@property (nonatomic, assign) int selectedRow;
@property (nonatomic, retain) UIFont *rowFont;
@property (nonatomic, retain) UIImageView *background;

- (void)setup;
- (void)reloadData;
- (void)determineCurrentRow;
- (void)didTap:(id)sender;
- (void)makeSteps:(int)steps;

// recycle queue
- (UIView *)dequeueRecycledView;
- (BOOL)isDisplayingViewForIndex:(NSUInteger)index;
- (void)tileViews;
- (void)configureView:(UIView *)view atIndex:(NSUInteger)index;

@end



@protocol AFPickerViewDataSource <NSObject>

- (NSInteger)numberOfRowsInPickerView:(GRPickerView *)pickerView;
- (NSString *)pickerView:(GRPickerView *)pickerView titleForRow:(NSInteger)row;

@end



@protocol AFPickerViewDelegate <NSObject>

- (void)pickerView:(GRPickerView *)pickerView didSelectRow:(NSInteger)row;

@end