//
//  ContactTableViewCell.h
//  GrannyApp
//
//  Created by Eugene Pavlyuk on 6/13/12.
//  Copyright (c) 2012 Home. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ContactTableViewCellDelegate.h"

@interface EmailButton : UIButton

@end

@class GRContact;

@interface ContactTableViewCell : UITableViewCell

@property (nonatomic, retain) IBOutlet UIButton *favButton;
@property (nonatomic, retain) IBOutlet UILabel *nameLabel;
@property (nonatomic, retain) IBOutlet UILabel *infoLabel;
@property (nonatomic, retain) NSIndexPath *indexPath;

@property (nonatomic, assign) id<ContactTableViewCellDelegate> delegate;

+ (NSString*)getReuseIdentifier;
+ (float)cellHeight;

- (IBAction)favButtonTapped;

- (void)adaptToContact:(GRContact*)contact;

@end
