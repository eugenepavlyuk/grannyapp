//
//  ContactTableViewCell.m
//  GrannyApp
//
//  Created by Eugene Pavlyuk on 6/13/12.
//  Copyright (c) 2012 Home. All rights reserved.
//

#import "ContactTableViewCell.h"
#import "GRContact.h"

@implementation EmailButton

- (void)unhighlightButton
{
    self.selected = NO;
}

- (void)highlightButton
{
    self.selected = YES;
}

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    
    if (self)
    {
        self.titleLabel.textAlignment = NSTextAlignmentLeft;
        self.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
        self.titleLabel.font = [UIFont systemFontOfSize:18];
    }
    
    return self;
}

- (void)layoutSubviews
{
    [super layoutSubviews];
    
    self.titleLabel.textAlignment = NSTextAlignmentLeft;
}

@end


@implementation ContactTableViewCell
{
    NSMutableArray *emailButtons;
}

@synthesize favButton;
@synthesize nameLabel;
@synthesize indexPath;

- (void)awakeFromNib
{
    self.contentView.backgroundColor = [UIColor clearColor];
}

- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder:aDecoder];
    
    if (self)
    {
        emailButtons = [[NSMutableArray array] retain];
    }
    
    return self;
}

+ (NSString*)getReuseIdentifier
{
    return @"ContactTableViewCellIdentifier";
}

+ (float)cellHeight
{
    return 75;
}

- (void)dealloc
{
    self.favButton = nil;
    self.nameLabel = nil;
    self.indexPath = nil;
    
    [emailButtons release];
    
    [super dealloc];
}

- (IBAction)favButtonTapped
{
    [self.delegate contactTableViewCell:self didTapFavoriteButtonAtIndexPath:self.indexPath];
}

- (void)adaptToContact:(GRContact*)contact
{
    NSMutableString *name = [NSMutableString string];
    
    if ([contact.firstname length])
    {
        [name appendString:contact.firstname];
    }
    
    if ([contact.lastname length])
    {
        if ([name length])
        {
            [name appendString:@" "];
        }
        
        [name appendString:contact.lastname];
    }
    
    self.nameLabel.text = name;
    
    if ([contact isFavorite])
    {
        [self.favButton setImage:[UIImage imageNamed:@"favoritestab"] forState:UIControlStateNormal];
    }
    else
    {
        [self.favButton setImage:[UIImage imageNamed:@"favorite-unactive"] forState:UIControlStateNormal];
    }
    
    [emailButtons makeObjectsPerformSelector:@selector(removeFromSuperview)];
    [emailButtons removeAllObjects];
    
    //    if ([contact.phoneHome length])
    //    {
    //        [info appendString:contact.phoneHome];
    //    }
    
    if ([contact.email length])
    {
        EmailButton *emailButton = [EmailButton buttonWithType:UIButtonTypeCustom];
        [emailButton setTitle:contact.email forState:UIControlStateNormal];
        [emailButtons addObject:emailButton];
    }
    
    if ([contact.email1 length])
    {
        EmailButton *emailButton = [EmailButton buttonWithType:UIButtonTypeCustom];
        [emailButton setTitle:contact.email1 forState:UIControlStateNormal];
        [emailButtons addObject:emailButton];
    }
    
    if ([contact.email2 length])
    {
        EmailButton *emailButton = [EmailButton buttonWithType:UIButtonTypeCustom];
        [emailButton setTitle:contact.email2 forState:UIControlStateNormal];
        [emailButtons addObject:emailButton];
    }
    
    if ([contact.email3 length])
    {
        EmailButton *emailButton = [EmailButton buttonWithType:UIButtonTypeCustom];
        [emailButton setTitle:contact.email3 forState:UIControlStateNormal];
        [emailButtons addObject:emailButton];
    }
    
    if ([contact.email4 length])
    {
        EmailButton *emailButton = [EmailButton buttonWithType:UIButtonTypeCustom];
        [emailButton setTitle:contact.email4 forState:UIControlStateNormal];
        [emailButtons addObject:emailButton];
    }
    
    if ([contact.email5 length])
    {
        EmailButton *emailButton = [EmailButton buttonWithType:UIButtonTypeCustom];
        [emailButton setTitle:contact.email5 forState:UIControlStateNormal];
        [emailButtons addObject:emailButton];
    }
    
    int y = self.nameLabel.frame.origin.y;
    
    for (EmailButton *emailButton in emailButtons)
    {
        [emailButton addTarget:self action:@selector(emailButtonTapped:) forControlEvents:UIControlEventTouchUpInside];
        
        [self.contentView addSubview:emailButton];
        emailButton.autoresizingMask = self.infoLabel.autoresizingMask;
        emailButton.frame = CGRectMake(self.infoLabel.frame.origin.x, y, self.infoLabel.frame.size.width, self.infoLabel.frame.size.height);
        y += 25;
        
        [emailButton setTitleColor:[UIColor colorWithRed:133.f/255.f green:133.f/255.f blue:133.f/255.f alpha:1.f] forState:UIControlStateNormal];
        [emailButton setTitleColor:rgb(0xee5015) forState:UIControlStateSelected];
        
        if ([emailButton.titleLabel.text isEqualToString:contact.selectedEmail])
        {
            [emailButton highlightButton];
        }
    }
}

- (void)emailButtonTapped:(EmailButton*)button
{
    [emailButtons makeObjectsPerformSelector:@selector(unhighlightButton)];
    
    [button highlightButton];
    
    [self.delegate contactTableViewCell:self didSelectEmail:button.titleLabel.text  inCellAtIndexPath:self.indexPath];
}

- (void)layoutSubviews
{
    [super layoutSubviews];
    
    int y = self.nameLabel.frame.origin.y;
    
    for (EmailButton *emailButton in emailButtons)
    {
        emailButton.frame = CGRectMake(self.infoLabel.frame.origin.x, y, self.infoLabel.frame.size.width, self.infoLabel.frame.size.height);
        y += 25;
    }
}

@end
