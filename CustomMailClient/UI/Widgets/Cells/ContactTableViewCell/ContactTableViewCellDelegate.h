//
//  ContactTableViewCellDelegate.h
//  GrannyApp
//
//  Created by Eugene Pavluk on 9/12/13.
//
//

#import <Foundation/Foundation.h>

@class ContactTableViewCell;

@protocol ContactTableViewCellDelegate <NSObject>

- (void)contactTableViewCell:(ContactTableViewCell*)cell didSelectEmail:(NSString*)emailString inCellAtIndexPath:(NSIndexPath*)indexPath;

- (void)contactTableViewCell:(ContactTableViewCell*)cell didTapFavoriteButtonAtIndexPath:(NSIndexPath*)indexPath;

@end
