//
//  GRMailInboxCell.h
//  GrannyApp
//
//  Created by Ievgen Pavliuk on 4/13/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TMailMessage.h"
#import "TMailSentMessage.h"
#import "TMailDeletedMessage.h"
#import "TMailDraftMessage.h"

@interface GRMailInboxCell : UITableViewCell

@property (nonatomic, retain) IBOutlet UILabel *lMessageTopic;
@property (nonatomic, retain) IBOutlet UILabel *lSenderName;
@property (nonatomic, retain) IBOutlet UILabel *lDate;
@property (nonatomic, retain) IBOutlet UILabel *lTime;
@property (nonatomic, retain) IBOutlet UIImageView *accountImage;
@property (nonatomic, retain) IBOutlet UIImageView *readImage;
@property (nonatomic, retain) IBOutlet UIImageView *unreadImage;

- (void)update:(TMailMessage *)message;
- (void)updateForSent:(TMailSentMessage *)message;
- (void)updateForTrash:(TMailDeletedMessage *)message;
- (void)updateForDraft:(TMailDraftMessage *)message;
- (void)updateMore;


@end
