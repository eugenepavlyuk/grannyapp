//
//  TimelineTableViewCell.m
//  GrannyApp
//
//  Created by Eugene Pavlyuk on 7/26/12.
//  Copyright (c) 2012 Home. All rights reserved.
//

#import "TimelineTableViewCell.h"

@implementation TimelineTableViewCell

@synthesize imageView;
@synthesize titleLabel;

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    self.imageView.highlighted = selected;
    self.titleLabel.highlighted = selected;
}

- (void)setHighlighted:(BOOL)highlighted animated:(BOOL)animated
{
    [super setHighlighted:highlighted animated:animated];
    
    self.imageView.highlighted = highlighted;
    self.titleLabel.highlighted = highlighted;
}

- (void)dealloc
{
    self.titleLabel = nil;
    self.imageView = nil;
    [super dealloc];
}

@end
