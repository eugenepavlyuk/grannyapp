//
//  TimelineTableViewCell.h
//  GrannyApp
//
//  Created by Eugene Pavlyuk on 7/26/12.
//  Copyright (c) 2012 Home. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TimelineTableViewCell : UITableViewCell

@property (nonatomic, retain) IBOutlet UIImageView *imageView;
@property (nonatomic, retain) IBOutlet UILabel *titleLabel;

@end
