//
//  GRSettingsMenuCell.m
//  GrannyApp
//
//  Created by Ievgen Pavliuk on 7/27/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "GRSettingsMenuCell.h"

@implementation GRSettingsMenuCell

@synthesize iconView;
@synthesize lTitle;

- (void)updateForIndex:(NSInteger)menuIndex {
    self.lTitle.textColor = RGBCOLOR(128, 128, 128);
    self.lTitle.font = [UIFont fontWithName:@"Helvetica" size:22];
    index = menuIndex;
    
    switch (menuIndex) {
        case 0:
            [self.iconView setImage:[UIImage imageNamed:@"Settings_HomeBtnDefault"]];
            //self.iconView.frame = CGRectMake(0, 10, 88, 88);
            self.lTitle.text = NSLocalizedString(@"Home", @"Home");
            break;

        case 1:
            [self.iconView setImage:[UIImage imageNamed:@"Settings_MailUnselectedIcon"]];
            //self.iconView.frame = CGRectMake(0, 10, 88, 88);
            self.lTitle.text = NSLocalizedString(@"Email", @"Email");
            break;
            
        case 2:
            [self.iconView setImage:[UIImage imageNamed:@"Settings_EventUnselectedIcon"]];
            //self.iconView.frame = CGRectMake(0, 10, 70, 87);
            self.lTitle.text = NSLocalizedString(@"Date&Time", @"Date&Time");
            break;
            
        case 3:
            [self.iconView setImage:[UIImage imageNamed:@"Settings_CalendarUnselectedIcon"]];
            //self.iconView.frame = CGRectMake(0, 10, 70, 87);
            self.lTitle.text = NSLocalizedString(@"Calendar", @"Calendar");
            break;
            
        case 4:
            [self.iconView setImage:[UIImage imageNamed:@"Settings_AlarmUnselectedIcon"]];
            //self.iconView.frame = CGRectMake(0, 15, 80, 82);
            self.lTitle.text = NSLocalizedString(@"Alarms", @"Alarms");
            break;
            
        case 5:
            [self.iconView setImage:[UIImage imageNamed:@"Settings_MedicineAlarmUnselectedIcon"]];
            //self.iconView.frame = CGRectMake(0, 15, 80, 82);
            self.lTitle.text = NSLocalizedString(@"Medicine Alarms", @"Medicine Alarms");
            break;
            
        case 6:
            [self.iconView setImage:[UIImage imageNamed:@"Settings_RadioUnselectedIcon"]];
            //self.iconView.frame = CGRectMake(0, 2, 96, 96);
            self.lTitle.text = NSLocalizedString(@"Radio", @"Radio");
            break;
        case 7:
            [self.iconView setImage:[UIImage imageNamed:@"Settings_ContactUnselectedIcon"]];
            //self.iconView.frame = CGRectMake(0, 24, 70, 74);
            self.lTitle.text = NSLocalizedString(@"Contacts", @"Contacts");
            break;
        case 8:
            [self.iconView setImage:[UIImage imageNamed:@"Settings_PhotoUnselectedIcon"]];
            //self.iconView.frame = CGRectMake(0, 22, 93, 76);
            self.lTitle.text = NSLocalizedString(@"Photo", @"Photo");
            break;
//            case 9:
//                [self.iconView setImage:[UIImage imageNamed:@"Settings_MenuTVIconOff.png"]];
//                //self.iconView.frame = CGRectMake(0, 11, 91, 87);
//                self.lTitle.text = NSLocalizedString(@"TV", @"TV");
//                break;
//            case 10:
        case 9:
            [self.iconView setImage:[UIImage imageNamed:@"Settings_SOSUnselectedIcon"]];
            //self.iconView.frame = CGRectMake(0, 22, 84, 75);
            self.lTitle.text = NSLocalizedString(@"Emergency", @"Emergency");
            break;
            
        default:
            break;
    }
}


- (void)markAsSelected {
    self.lTitle.textColor = rgb(0x0027fa);
    self.lTitle.font = [UIFont fontWithName:@"Helvetica-Bold" size:22];
    switch (index) {
        case 0:
            [self.iconView setImage:[UIImage imageNamed:@"Settings_HomeBtnOn"]];
            break;
        case 1:
            [self.iconView setImage:[UIImage imageNamed:@"Settings_MailSelectedIcon"]];
            break;
        case 2:
            [self.iconView setImage:[UIImage imageNamed:@"Settings_EventSelectedIcon"]];
            break;
            
        case 3:
            [self.iconView setImage:[UIImage imageNamed:@"Settings_CalendarSelectedIcon"]];
            break;
            
        case 4:
            [self.iconView setImage:[UIImage imageNamed:@"Settings_AlarmSelectedIcon"]];
            break;
            
        case 5:
            [self.iconView setImage:[UIImage imageNamed:@"Settings_MedicineAlarmSelectedIcon"]];
            break;
            
        case 6:
            [self.iconView setImage:[UIImage imageNamed:@"Settings_RadioSelectedIcon"]];
            break;
        case 7:
            [self.iconView setImage:[UIImage imageNamed:@"Settings_ContactSelectedIcon"]];
            break;
        case 8:
            [self.iconView setImage:[UIImage imageNamed:@"Settings_PhotoSelectedIcon"]];
            break;
//        case 9:
//            [self.iconView setImage:[UIImage imageNamed:@"Settings_MenuTVIconOn.png"]];
//            break;
            
//        case 10:
        case 9:
            [self.iconView setImage:[UIImage imageNamed:@"Settings_SOSSelectedIcon"]];
            break;
            
        default:
            break;
    }
}

- (void)markAsUnSelected {
    self.lTitle.textColor = RGBCOLOR(128, 128, 128);
    self.lTitle.font = [UIFont fontWithName:@"Helvetica" size:22];
    switch (index) {
        case 0:
            [self.iconView setImage:[UIImage imageNamed:@"Settings_HomeBtnDefault"]];
            break;
        case 1:
            [self.iconView setImage:[UIImage imageNamed:@"Settings_MailUnselectedIcon"]];
            break;
        case 2:
            [self.iconView setImage:[UIImage imageNamed:@"Settings_EventUnselectedIcon"]];
            break;
        
        case 3:
            [self.iconView setImage:[UIImage imageNamed:@"Settings_CalendarUnselectedIcon"]];
            break;

        case 4:
            [self.iconView setImage:[UIImage imageNamed:@"Settings_AlarmUnselectedIcon"]];
            break;
            
        case 5:
            [self.iconView setImage:[UIImage imageNamed:@"Settings_MedicineAlarmUnselectedIcon"]];
            break;

        case 6:
            [self.iconView setImage:[UIImage imageNamed:@"Settings_RadioUnselectedIcon"]];
            break;
            
        case 7:
            [self.iconView setImage:[UIImage imageNamed:@"Settings_ContactUnselectedIcon"]];
            break;
        case 8:
            [self.iconView setImage:[UIImage imageNamed:@"Settings_PhotoUnselectedIcon"]];
            break;
//        case 9:
//            [self.iconView setImage:[UIImage imageNamed:@"Settings_MenuTVIconOff.png"]];
//            break;
            
//        case 10:
        case 9:
            [self.iconView setImage:[UIImage imageNamed:@"Settings_SOSUnselectedIcon"]];
            break;
            
        default:
            break;
    }
}

- (void)layoutSubviews
{
    [super layoutSubviews];
    
    
}

- (void)dealloc {
    [lTitle release];
    [iconView release];
    [super dealloc];
}

@end
