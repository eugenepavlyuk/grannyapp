//
//  GRSettingsMenuCell.h
//  GrannyApp
//
//  Created by Ievgen Pavliuk on 7/27/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface GRSettingsMenuCell : UITableViewCell
{
    UILabel      *lTitle;
    UIImageView  *iconView;
    
    NSInteger     index;
}

@property (nonatomic, retain) IBOutlet UILabel      *lTitle;
@property (nonatomic, retain) IBOutlet UIImageView  *iconView;

- (void)updateForIndex:(NSInteger)menuIndex;
- (void)markAsSelected;
- (void)markAsUnSelected;

@end
