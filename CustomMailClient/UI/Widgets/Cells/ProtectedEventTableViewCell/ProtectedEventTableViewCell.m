//
//  AlarmTableViewCell.m
//  GrannyApp
//
//  Created by Eugene Pavlyuk on 5/10/12.
//  Copyright (c) 2012 Home. All rights reserved.
//

#import "ProtectedEventTableViewCell.h"

@implementation ProtectedEventTableViewCell

@synthesize titleLabel;
@synthesize backgroundImageView;
@synthesize protectedIcon;
@synthesize timeLabel;
@synthesize repeatLabel;

- (void)awakeFromNib
{
    if (IS_IPAD)
    {
        self.rtLabel.font = [UIFont boldSystemFontOfSize:26];
    }
}

- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder:aDecoder];
    
    if (self)
    {
        
    }
    
    return self;
}

- (void)dealloc
{
    self.timeLabel = nil;
    self.repeatLabel = nil;
    self.titleLabel = nil;
    self.backgroundImageView = nil;
    self.protectedIcon = nil;
    [super dealloc];
}

@end
