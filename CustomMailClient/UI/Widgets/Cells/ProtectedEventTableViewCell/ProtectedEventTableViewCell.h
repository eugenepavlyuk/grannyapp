//
//  AlarmTableViewCell.h
//  GrannyApp
//
//  Created by Eugene Pavlyuk on 5/10/12.
//  Copyright (c) 2012 Home. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "RTLabel.h"

@interface ProtectedEventTableViewCell : UITableViewCell

@property (nonatomic, retain) IBOutlet UILabel *titleLabel;
@property (nonatomic, retain) IBOutlet UILabel *timeLabel;
@property (nonatomic, retain) IBOutlet UILabel *repeatLabel;
@property (nonatomic, retain) IBOutlet UIImageView *backgroundImageView;
@property (nonatomic, retain) IBOutlet UIImageView *protectedIcon;
@property (nonatomic, retain) IBOutlet UIImageView *alarmIcon;
@property (nonatomic, retain) IBOutlet RTLabel *rtLabel;

@end
