//
//  GRMailInboxCell.m
//  GrannyApp
//
//  Created by Ievgen Pavliuk on 4/13/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "GRMailInboxCell.h"
#import "GRContact.h"
#import "GRPeopleManager.h"
#import "GRMailDataManager.h"

@implementation GRMailInboxCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self)
    {        
    }
    return self;
}

- (void)dealloc
{
    [_unreadImage release];
    [_readImage release];
    [_lMessageTopic release];
    [_lSenderName release];
    [_lDate release];
    [_lTime release];
    [_accountImage release];
    [super dealloc];
}

- (void)updateMore {
    self.lMessageTopic.hidden = YES;
    self.lSenderName.hidden = YES;
    self.lDate.hidden = YES;
    self.lTime.hidden = YES;
    self.accountImage.hidden = YES;
    
    if (IS_IPAD) 
    {
        UILabel *lMore = [[[UILabel alloc] initWithFrame:CGRectMake(50, 50, 400, 30)] autorelease];
        lMore.backgroundColor = [UIColor clearColor];
        lMore.font = [UIFont fontWithName:@"Helvetica-Bold" size:22];
        lMore.text = NSLocalizedString(@"Load more messages", @"Load more messages");
        CGSize size = [lMore.text sizeWithFont:lMore.font];
        lMore.frame = CGRectMake((int)((self.contentView.frame.size.width - size.width)/2), (int)((self.contentView.frame.size.height - size.height)/2), size.width, size.height);
        [self.contentView addSubview:lMore];
    } else {
        UILabel *lMore = [[[UILabel alloc] initWithFrame:CGRectMake(70, 20, 240, 18)] autorelease];
        lMore.backgroundColor = [UIColor clearColor];
        lMore.font = [UIFont fontWithName:@"Helvetica-Bold" size:14];
        lMore.text = NSLocalizedString(@"Load more messages", @"Load more messages");
        CGSize size = [lMore.text sizeWithFont:lMore.font];
        lMore.frame = CGRectMake((int)((self.contentView.frame.size.width - size.width)/2), (int)((self.contentView.frame.size.height - size.height)/2), size.width, size.height);
        [self.contentView addSubview:lMore];
    }
    
}

- (void)update:(TMailMessage *)message
{
    self.lMessageTopic.text = message.subject;
    if (message.senderName) {
        self.lSenderName.text = [NSString stringWithFormat:@"%@", message.senderName];
    } else {
        self.lSenderName.text = [NSString stringWithFormat:@"<%@>", message.senderEmail];
    }
    
    if ([message.isRead boolValue]) {
        self.readImage.hidden = YES;
    } else {
        self.readImage.hidden = NO;
    }
    
    [self updateDateLabelsWithDate:message.date dateFormat:@"dd.MM.yyyy" timeFormat:@"HH:mm"];
    
    GRContact *contact = [[GRPeopleManager sharedManager] findContactByEmail:message.senderEmail];
    [self updateWithContact:contact];
}

- (void)updateForSent:(TMailSentMessage *)message
{
    NSLog(@"%@", [message subject]);
    self.lMessageTopic.text = [message subject];
    
    [self updateDateLabelsWithDate:message.date dateFormat:@"dd.MM.yyyy" timeFormat:nil];
    
    self.lMessageTopic.text = [message subject];
    
    NSArray *tos = [NSKeyedUnarchiver unarchiveObjectWithData:message.tos];
    NSString *toString = @"";
    
    int i = 0;
    
    for (NSString *email in tos)
    {
        if (i > 1)
        {
            toString = [toString stringByAppendingString:NSLocalizedString(@"and others", @"and others")];
            break ;
        }
        
        GRContact *contact = [[GRPeopleManager sharedManager] findContactByEmail:email];
        
        if (contact)
        {
            toString = [toString stringByAppendingFormat:@"%@", contact.fullname];
        }
        else
        {
            toString = [toString stringByAppendingFormat:@"%@", email];
        }
        
        if (i != [tos count] - 1)
        {
            toString = [toString stringByAppendingString:@", "];
        }
        
        i++;
    }
    
    if ([toString length] != 0)
    {
        //toString = [toString substringToIndex:[toString length]-1];
        
        self.lSenderName.text = [NSString stringWithFormat:@"%@", toString];
    }
    if ([tos count] > 0)
    {
        GRContact *contact = [[GRPeopleManager sharedManager] findContactByEmail:[tos objectAtIndex:0]];
        [self updateWithContact:contact];
    }
}

- (void)updateForTrash:(TMailDeletedMessage *)message
{
    self.lMessageTopic.text = [message subject];
    
    if (message.senderName)
        self.lSenderName.text = [NSString stringWithFormat:@"%@", message.senderName];
    else
    {
        if (message.senderEmail)
            self.lSenderName.text = [NSString stringWithFormat:@"<%@>", message.senderEmail];
        else
            self.lSenderName.text = [NSString stringWithFormat:@"<%@>", NSLocalizedString(@"not specified", nil)];
    }
    
   [self updateDateLabelsWithDate:message.date dateFormat:@"dd.MM.yyyy" timeFormat:nil];
    
    GRContact *contact = [[GRPeopleManager sharedManager] findContactByEmail:message.senderEmail];
    [self updateWithContact:contact];
}

- (void)updateForDraft:(TMailDraftMessage *)message
{
    self.lMessageTopic.text = [message subject];
    self.lSenderName.text = message.senderEmail;
    
    [self updateDateLabelsWithDate:message.date dateFormat:@"dd.MM.yyyy" timeFormat:nil];
    
    NSArray * toContacts = [[GRMailDataManager sharedManager] getAllToContactsForDraftMessage:message];
    GRContact * firstContact = (toContacts.count)? [toContacts objectAtIndex:0] : nil;
    
    [self updateWithContact:firstContact];
}

- (void)updateAccountImageWithImagePath:(NSString *)imagePath
{
    NSData *imageData = [NSData dataWithContentsOfFile:imagePath];
    UIImage *iconImage = [UIImage imageWithData:imageData];
    self.accountImage.layer.masksToBounds = YES;
    self.accountImage.layer.borderWidth = 1.0f;
    self.accountImage.layer.borderColor = [UIColor lightGrayColor].CGColor;
    [self.accountImage.layer setCornerRadius:(IS_IPAD)? 15.0f : 5.0f];
    [self.accountImage setImage:iconImage];
}

- (void)updateDateLabelsWithDate:(NSDate *)date dateFormat:(NSString *)dateFormat timeFormat:(NSString *)timeFormat
{
    static NSDateFormatter *df = nil;
    if (!df) {
        df = [[NSDateFormatter alloc] init];
    }
    
	[df setDateFormat:dateFormat];
    self.lDate.text = [df stringFromDate:date];
    
    if (timeFormat) {
        self.lTime.hidden = NO;
        [df setDateFormat:timeFormat];
        self.lTime.text = [df stringFromDate:date];
//        [self.lTime sizeToFit];
        
//        self.lTime.center = CGPointMake(CGRectGetMinX(self.readImage.frame) - 12 - CGRectGetWidth(self.lTime.bounds)/2, CGRectGetMidY(self.contentView.bounds));
//        self.lTime.frame = CGRectIntegral(self.lTime.frame);
//        
//        self.lDate.center = CGPointMake(CGRectGetMinX(self.lTime.frame) - 8 - CGRectGetWidth(self.lDate.bounds)/2, CGRectGetMidY(self.contentView.bounds));
//        self.lDate.frame = CGRectIntegral(self.lDate.frame);
}
    else {
        self.lTime.hidden = YES;
//        self.lDate.center = CGPointMake(CGRectGetMinX(self.readImage.frame) - 12 - CGRectGetWidth(self.lDate.bounds)/2, CGRectGetMidY(self.contentView.bounds));
//        self.lDate.frame = CGRectIntegral(self.lDate.frame);
    }
}

- (void)updateWithContact:(GRContact *)contact
{
    if (contact)
    {
        if (contact.photourl && [[NSFileManager defaultManager] fileExistsAtPath:contact.photourl])
        {
            self.accountImage.hidden = NO;
            [self updateAccountImageWithImagePath:contact.photourl];
        }
        else {
            self.accountImage.hidden = YES;
        }
    }
}

@end
