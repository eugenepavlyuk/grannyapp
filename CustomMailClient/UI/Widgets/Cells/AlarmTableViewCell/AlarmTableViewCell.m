//
//  AlarmTableViewCell.m
//  GrannyApp
//
//  Created by Eugene Pavlyuk on 5/10/12.
//  Copyright (c) 2012 Home. All rights reserved.
//

#import "AlarmTableViewCell.h"

@implementation AlarmTableViewCell

@synthesize titleLabel;
@synthesize backgroundImageView;
@synthesize protectedIcon;
@synthesize timeLabel;
@synthesize repeatLabel;

- (void)awakeFromNib
{
    self.rtLabel.font = [UIFont boldSystemFontOfSize:26];
}

- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder:aDecoder];
    
    if (self)
    {
        
    }
    
    return self;
}

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)dealloc
{
    self.timeLabel = nil;
    self.repeatLabel = nil;
    self.titleLabel = nil;
    self.backgroundImageView = nil;
    self.protectedIcon = nil;
    [super dealloc];
}

@end
