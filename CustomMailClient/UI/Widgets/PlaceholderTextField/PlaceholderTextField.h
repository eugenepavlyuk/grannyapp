//
//  PlaceholderTextField.h
//  TopMeUp
//
//  Created by Ievgen Pavliuk on 12/28/13.
//  Copyright (c) 2013 Documatic. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PlaceholderTextField : UITextField
{
    UILabel *_placeholderLabel;
    
    NSNumber *placeholderFontSize;
    NSString *placeholderFontName;
}

@property (nonatomic, copy) NSString *placeholderText;
@property (nonatomic, assign) UIEdgeInsets insets;
@property (nonatomic, retain) UIColor *placeholderColor UI_APPEARANCE_SELECTOR;
@property (nonatomic, retain) UIFont *placeholderFont UI_APPEARANCE_SELECTOR;

@end
