//
//  PlaceholderTextField.m
//  TopMeUp
//
//  Created by Ievgen Pavliuk on 12/28/13.
//  Copyright (c) 2013 Documatic. All rights reserved.
//

#import "PlaceholderTextField.h"

@interface PlaceholderTextField()

@property (nonatomic, retain) UILabel *placeholderLabel;

@end


@implementation PlaceholderTextField

+ (void)initialize
{
    PlaceholderTextField *placeholderTextField = [PlaceholderTextField appearance];
    placeholderTextField.placeholderColor = rgb(0xee5015);
    placeholderTextField.placeholderFont = [UIFont fontWithName:@"Helvetica-Oblique" size:18];
}

- (void)didMoveToWindow
{
    _placeholderLabel.textColor = self.placeholderColor;
    
//    if (placeholderFontSize)
//    {
//        if ([placeholderFontName isEqualToString:@"ER"])
//        {
//            _placeholderLabel.font = kEdmondsansRegularFontWithSize([placeholderFontSize floatValue]);
//        }
//        else
//        {
//            _placeholderLabel.font = kDroidSansBoldFontWithSize([placeholderFontSize floatValue]);
//        }
//    }
//    else
    {
        _placeholderLabel.font = self.placeholderFont;
    }
}

- (void)awakeFromNib
{
    [super awakeFromNib];
    
    [self addSubview:self.placeholderLabel];
    
    self.placeholderLabel.hidden = ([self.text length]);
}

- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder:aDecoder];
    
    if (self)
    {
        _insets = UIEdgeInsetsMake(0, 10, 0, 10);
        [_placeholderText release];
        _placeholderText = [self.placeholder copy];
        self.placeholder = @"";
        
        [[NSNotificationCenter defaultCenter] addObserver:self
                                                 selector:@selector(textFieldDidChange:)
                                                     name:UITextFieldTextDidChangeNotification
                                                   object:self];
    }
    
    return self;
}

- (void)textFieldDidChange:(NSNotification*)notification
{
    _placeholderLabel.hidden = [self.text length];
}

- (UILabel*)placeholderLabel
{
    if (!_placeholderLabel)
    {
        _placeholderLabel = [[UILabel alloc] initWithFrame:CGRectInset(self.bounds, _insets.left, _insets.top)];
        _placeholderLabel.userInteractionEnabled = NO;
        _placeholderLabel.textAlignment = self.textAlignment;
        _placeholderLabel.text = self.placeholderText;
        _placeholderLabel.backgroundColor = [UIColor clearColor];
        
        self.leftViewMode = UITextFieldViewModeAlways;
        self.rightViewMode = UITextFieldViewModeAlways;
        
        self.leftView = [[[UIView alloc] initWithFrame:CGRectMake(0, 0, _insets.left, self.bounds.size.height)] autorelease];
        self.rightView = [[[UIView alloc] initWithFrame:CGRectMake(0, 0, _insets.right, self.bounds.size.height)] autorelease];
        self.leftView.backgroundColor = [UIColor clearColor];
        self.rightView.backgroundColor = [UIColor clearColor];
    }
    
    return _placeholderLabel;
}

- (void)layoutSubviews
{
    [super layoutSubviews];
    
    self.placeholderLabel.frame = CGRectInset(self.bounds, _insets.left, _insets.top);
}

- (void)setInsets:(UIEdgeInsets)insets
{
    _insets = insets;
    
    [self setNeedsLayout];
}

- (void)setText:(NSString *)theText
{
    _placeholderLabel.hidden = [theText length];
    
    [super setText:theText];
}

- (void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
    
    self.placeholderFont = nil;
    self.placeholderColor = nil;
    self.placeholderText = nil;
    self.placeholderLabel = nil;
    
    [super dealloc];
}

- (void)setPlaceholder:(NSString *)placeholder
{
    if (_placeholderLabel)
    {
        self.placeholderText = placeholder;
        _placeholderLabel.text = self.placeholderText;
    }
    else
    {
        [super setPlaceholder:placeholder];
    }
}

- (void)setPlaceholderColor:(UIColor *)placeholderColor
{
    _placeholderColor = placeholderColor;
    
    self.placeholderLabel.textColor = _placeholderColor;
}

- (void)setPlaceholderFont:(UIFont *)placeholderFont
{
    _placeholderFont = placeholderFont;
    
    self.placeholderLabel.font = _placeholderFont;
}

@end
