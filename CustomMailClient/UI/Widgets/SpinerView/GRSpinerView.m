//
//  GRSpinerView.m
//  GrannyApp
//
//  Created by Ievgen Pavliuk on 5/31/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "GRSpinerView.h"

@implementation GRSpinerView

@synthesize infoLabel;

- (void)awakeFromNib
{
    container.layer.cornerRadius = 20.f;
    container.clipsToBounds = YES;
}

@end
