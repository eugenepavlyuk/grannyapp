//
//  GRSpinerView.h
//  GrannyApp
//
//  Created by Ievgen Pavliuk on 5/31/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface GRSpinerView : UIView
{
    UILabel *infoLabel;
    IBOutlet UIView *container;
}

@property (nonatomic, retain) IBOutlet UILabel *infoLabel;

@end
