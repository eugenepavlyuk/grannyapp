//
//  GRDayHeader.m
//  GrannyApp
//
//  Created by Eugene Pavlyuk on 10/5/12.
//
//

#import "GRDayHeader.h"

@implementation GRDayHeader

@synthesize titleLabel;
@synthesize backgroundImageView;

- (void)awakeFromNib
{
    [super awakeFromNib];
}

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    
    if (self)
    {
        // Initialization code
    }
    
    return self;
}

- (void)didMoveToWindow
{
    self.titleLabel.font = self.titleFont;
}

- (void)dealloc
{
    self.titleLabel = nil;
    self.backgroundImageView = nil;
    self.titleFont = nil;
    [super dealloc];
}

@end
