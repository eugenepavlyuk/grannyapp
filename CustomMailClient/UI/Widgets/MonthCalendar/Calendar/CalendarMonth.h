//
//  CalendarMonth.h
//  Calendar
//
//  Created by Lloyd Bottomley on 29/04/10.
//  Copyright 2010 Savage Media Pty Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CalendarViewControllerDelegate.h"


@class CalendarLogic;

@interface CalendarMonth : UIView
{
	CalendarLogic *calendarLogic;
	NSArray *datesIndex;
	NSArray *buttonsIndex;
    NSArray *headersIndex;
	
	NSInteger numberOfDaysInWeek;
	NSInteger selectedButton;
	NSDate *selectedDate;
}

@property (nonatomic, retain) CalendarLogic *calendarLogic;
@property (nonatomic, retain) NSArray *datesIndex;
@property (nonatomic, retain) NSArray *buttonsIndex;
@property (nonatomic, retain) NSArray *headersIndex;
@property (nonatomic) NSInteger numberOfDaysInWeek;
@property (nonatomic) NSInteger selectedButton;
@property (nonatomic, retain) NSDate *selectedDate;
@property (nonatomic, assign) NSObject<CalendarViewControllerDelegate> *delegate;

- (id)initWithFrame:(CGRect)frame logic:(CalendarLogic *)aLogic;

- (void)selectButtonForDate:(NSDate *)aDate;

- (void)refresh;
- (void)reloadData;

@end
