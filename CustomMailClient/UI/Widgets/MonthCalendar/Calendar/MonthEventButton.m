//
//  MonthEventButton.m
//  GrannyApp
//
//  Created by Eugene Pavluk on 12/12/13.
//
//

#import "MonthEventButton.h"

@implementation MonthEventButton

- (void)awakeFromNib
{
    self.imageView.contentMode = UIViewContentModeCenter;
    self.titleLabel.numberOfLines = 2;
}

- (void)layoutSubviews
{
    [super layoutSubviews];
    
    self.imageView.frame = CGRectMake(0, 0, 30, self.bounds.size.height);
//    
//    if (self.imageView.image)
//    {
//        self.titleLabel.frame = CGRectMake(30, self.bounds.size.height / 2, self.bounds.size.width - 30, self.bounds.size.height / 2);
//    }
//    else
//    {
//        self.titleLabel.frame = CGRectMake(0, self.bounds.size.height / 2, self.bounds.size.width, self.bounds.size.height / 2);
//    }
}


@end
