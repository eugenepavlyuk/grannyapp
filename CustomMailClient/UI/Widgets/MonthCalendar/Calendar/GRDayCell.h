//
//  GRDayCell.h
//  GrannyApp
//
//  Created by Eugene Pavlyuk on 10/5/12.
//
//

#import <UIKit/UIKit.h>

@class EventButton;
@class Event;

@interface GRDayCell : UIView

@property (nonatomic, retain) IBOutlet UILabel *dayLabel;
@property (nonatomic, retain) IBOutlet UILabel *todayLabel;
@property (nonatomic, retain) IBOutlet UIImageView *backgroundImageView;
@property (nonatomic, retain) IBOutlet UIImageView *borderImageView;
@property (nonatomic, retain) IBOutlet UIImageView *lowBorderImageView;
@property (nonatomic, retain) IBOutlet UILabel *otherEventsLabel;
@property (nonatomic, retain) IBOutlet EventButton *eventButton;
@property (nonatomic, retain) IBOutlet EventButton *protectedEventButton;
@property (nonatomic, retain) IBOutlet EventButton *protectedEventButton2;
@property (nonatomic, retain) IBOutlet EventButton *protectedEventButton3;

@property (nonatomic, retain) UIColor *previousMonthDayColor UI_APPEARANCE_SELECTOR;
@property (nonatomic, retain) UIColor *nextMonthDayColor UI_APPEARANCE_SELECTOR;
@property (nonatomic, retain) UIColor *currentMonthDayColor UI_APPEARANCE_SELECTOR;
@property (nonatomic, retain) UIColor *todayDayColor UI_APPEARANCE_SELECTOR;
@property (nonatomic, retain) UIColor *todayBackgroundDayColor UI_APPEARANCE_SELECTOR;
@property (nonatomic, retain) UIColor *todayDayTitleColor UI_APPEARANCE_SELECTOR;
@property (nonatomic, retain) UIColor *eventTitleColor UI_APPEARANCE_SELECTOR;
@property (nonatomic, retain) UIFont *dayFont UI_APPEARANCE_SELECTOR;
@property (nonatomic, retain) UIColor *eventBackgroundColor UI_APPEARANCE_SELECTOR;
@property (nonatomic, retain) UIColor *eventProtectedBackgroundColor UI_APPEARANCE_SELECTOR;
@property (nonatomic, retain) UIImage *lockImage UI_APPEARANCE_SELECTOR;
@property (nonatomic, retain) UIImage *eventImage UI_APPEARANCE_SELECTOR;

@property (nonatomic, retain) Event *event;
@property (nonatomic, retain) Event *protectedEvent;
@property (nonatomic, retain) Event *protectedEvent2;
@property (nonatomic, retain) Event *protectedEvent3;

@end
