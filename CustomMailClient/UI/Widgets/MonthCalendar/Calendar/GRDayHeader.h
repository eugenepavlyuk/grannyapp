//
//  GRDayHeader.h
//  GrannyApp
//
//  Created by Eugene Pavlyuk on 10/5/12.
//
//

#import <UIKit/UIKit.h>

@interface GRDayHeader : UIView

@property (nonatomic, retain) IBOutlet UILabel *titleLabel;
@property (nonatomic, retain) IBOutlet UIImageView *backgroundImageView;
@property (nonatomic, retain) UIFont *titleFont UI_APPEARANCE_SELECTOR;

@end
