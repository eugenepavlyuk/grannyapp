//
//  CalendarMonth.m
//  Calendar
//
//  Created by Lloyd Bottomley on 29/04/10.
//  Copyright 2010 Savage Media Pty Ltd. All rights reserved.
//

#import "CalendarMonth.h"
#import "GRDayCell.h"
#import "CalendarLogic.h"
#import "GRDayHeader.h"
#import "Event.h"
#import "DataManager.h"
#import "Settings.h"
#import "EventButton.h"

@implementation CalendarMonth


#pragma mark -
#pragma mark Getters / setters

@synthesize calendarLogic;
@synthesize datesIndex;
@synthesize buttonsIndex;
@synthesize headersIndex;

@synthesize numberOfDaysInWeek;
@synthesize selectedButton;
@synthesize selectedDate;
@synthesize delegate;


#pragma mark -
#pragma mark Memory management

- (void)dealloc
{
	self.calendarLogic = nil;
	self.datesIndex = nil;
	self.buttonsIndex = nil;
	self.selectedDate = nil;
    self.headersIndex = nil;
    [super dealloc];
}

#pragma mark -
#pragma mark Initialization

- (void) refresh
{
    [self.subviews makeObjectsPerformSelector:@selector(removeFromSuperview)];
    
    // Size is static
	NSInteger numberOfWeeks = 5;
	selectedButton = -1;
	
	NSCalendar *calendar = [AppDelegate getInstance].globalCalendar;
    
    Settings *settings = [[DataManager sharedInstance] getEntityWithName:kSettingsEntityName];
    
    if ([settings.weedStartsFrom intValue])
    {
        [calendar setFirstWeekday:1];
    }
    else
    {
        [calendar setFirstWeekday:2];
    }
    
    NSDateComponents *components = [calendar components:(NSDayCalendarUnit | NSMonthCalendarUnit | NSYearCalendarUnit) fromDate:[NSDate date]];
	NSDate *todayDate = [calendar dateFromComponents:components];
	
		NSDateFormatter *formatter = [[[NSDateFormatter alloc] init] autorelease];
		NSArray *daySymbols = [formatter shortWeekdaySymbols];
		self.numberOfDaysInWeek = [daySymbols count];
        
        CGFloat width = (self.bounds.size.width) / self.numberOfDaysInWeek;
		
		// Build calendar buttons (6 weeks of 7 days)
		NSMutableArray *aDatesIndex = [[[NSMutableArray alloc] init] autorelease];
		NSMutableArray *aButtonsIndex = [[[NSMutableArray alloc] init] autorelease];
        NSMutableArray *aLabelsIndex = [[[NSMutableArray alloc] init] autorelease];
		
        CGFloat dayHeight = (self.bounds.size.height) / (numberOfWeeks + 1);
        
		for (NSInteger aWeek = 0; aWeek <= numberOfWeeks; aWeek ++)
        {
			CGFloat positionY = (aWeek * dayHeight);
			
			for (NSInteger aWeekday = 1; aWeekday <= numberOfDaysInWeek; aWeekday ++)
            {
				CGFloat positionX = ((aWeekday - 1) * width);
				CGRect dayFrame = CGRectMake(positionX, positionY, width, dayHeight);
				NSDate *dayDate = [CalendarLogic dateForWeekday:aWeekday
														 onWeek:aWeek
												  referenceDate:[calendarLogic referenceDate]];
				NSDateComponents *dayComponents = [calendar
												   components:NSDayCalendarUnit fromDate:dayDate];
				
                GRDayCell *cell = (GRDayCell*)[[[NSBundle mainBundle] loadNibNamed:@"GRDayCell" owner:nil options:nil] lastObject];
                
                cell.frame = dayFrame;
                cell.tag = [aDatesIndex count];
                
				UIColor *titleColor = nil;
                
                [self addSubview:cell];
                
                NSInteger distance = [self.calendarLogic distanceOfDateFromCurrentMonth:dayDate];
                
				if (distance == 0)
                {
					titleColor = cell.currentMonthDayColor;
				}
                else if (distance > 0)
                {
                    titleColor = cell.nextMonthDayColor;
                }
                else
                {
                    titleColor = cell.previousMonthDayColor;
                }
                
                cell.dayLabel.text = [NSString stringWithFormat:@"%d", [dayComponents day]];
                cell.dayLabel.font = cell.dayFont;
                cell.todayLabel.font = cell.dayFont;
                cell.todayLabel.textColor = cell.todayDayTitleColor;
                
				if ([dayDate compare:todayDate] != NSOrderedSame)
                {
                    cell.backgroundImageView.image = nil;
                    cell.backgroundImageView.backgroundColor = [UIColor clearColor];
                    cell.dayLabel.textColor = titleColor;
                    
                    cell.todayLabel.text = @"";
				}
                else
                {
                    cell.backgroundImageView.image = nil;
                    cell.backgroundImageView.backgroundColor = cell.todayBackgroundDayColor;
                    cell.dayLabel.textColor = cell.todayDayColor;
                    
                    //cell.todayLabel.text = NSLocalizedString(@"Today", @"Today");
				}
                
				[cell.eventButton addTarget:self action:@selector(dayButtonPressed:) forControlEvents:UIControlEventTouchUpInside];
                
                [cell.protectedEventButton addTarget:self action:@selector(protectedEventButtonPressed:) forControlEvents:UIControlEventTouchUpInside];
                
                [cell.protectedEventButton2 addTarget:self action:@selector(protectedEventButtonPressed:) forControlEvents:UIControlEventTouchUpInside];
                
                [cell.protectedEventButton3 addTarget:self action:@selector(protectedEventButtonPressed:) forControlEvents:UIControlEventTouchUpInside];
                
				// Save
				[aDatesIndex addObject:dayDate];
				[aButtonsIndex addObject:cell];
			}
        }
		
		self.datesIndex = [[aDatesIndex copy] autorelease];
		self.buttonsIndex = [[aButtonsIndex copy] autorelease];
        
        
        NSInteger firstWeekday = [calendar firstWeekday] - 1;
        
		for (NSInteger aWeekday = 0; aWeekday < numberOfDaysInWeek; aWeekday ++)
        {
 			NSInteger symbolIndex = aWeekday + firstWeekday;
            
			if (symbolIndex >= numberOfDaysInWeek)
            {
				symbolIndex -= numberOfDaysInWeek;
			}
            
			NSString *symbol = [daySymbols objectAtIndex:symbolIndex];
			CGFloat positionX = (aWeekday * width);
			CGRect aFrame = CGRectMake(positionX, 25, width, 45);
			
            GRDayHeader *header = (GRDayHeader*)[[[NSBundle mainBundle] loadNibNamed:@"GRDayHeader" owner:nil options:nil] lastObject];
            header.frame = aFrame;
			header.titleLabel.text = [symbol stringByAppendingString:@"."];
			[self addSubview:header];
            [aLabelsIndex addObject:header];
		}
        
        self.headersIndex = [[aLabelsIndex copy] autorelease];
}

// Calendar object init
- (id)initWithFrame:(CGRect)frame logic:(CalendarLogic *)aLogic
{
	// Size is static
	NSInteger numberOfWeeks = 5;
	selectedButton = -1;

	NSCalendar *calendar = [AppDelegate getInstance].globalCalendar;
    
    Settings *settings = [[DataManager sharedInstance] getEntityWithName:kSettingsEntityName];
    
    if ([settings.weedStartsFrom intValue])
    {
        [calendar setFirstWeekday:1];
    }
    else
    {
        [calendar setFirstWeekday:2];
    }
    
    NSDateComponents *components = [calendar components:(NSDayCalendarUnit | NSMonthCalendarUnit | NSYearCalendarUnit) fromDate:[NSDate date]];
	NSDate *todayDate = [calendar dateFromComponents:components];
	
    if ((self = [super initWithFrame:frame]))
    {
        // Initialization code
		self.backgroundColor = [UIColor whiteColor]; 
		self.opaque = YES;
		self.clipsToBounds = NO;
		self.clearsContextBeforeDrawing = NO;
						
		NSDateFormatter *formatter = [[[NSDateFormatter alloc] init] autorelease];
		NSArray *daySymbols = [formatter shortWeekdaySymbols];
		self.numberOfDaysInWeek = [daySymbols count];
		
		// Setup weekday names
        
        CGFloat width = (self.bounds.size.width) / self.numberOfDaysInWeek;
		
		// Build calendar buttons (6 weeks of 7 days)
		NSMutableArray *aDatesIndex = [[[NSMutableArray alloc] init] autorelease];
		NSMutableArray *aButtonsIndex = [[[NSMutableArray alloc] init] autorelease];
        NSMutableArray *aLabelsIndex = [[[NSMutableArray alloc] init] autorelease];
		
        CGFloat dayHeight = (self.bounds.size.height) / (numberOfWeeks + 1);
        NSInteger dayIndex = 1;
        if ([settings.weedStartsFrom intValue] == 1) {  //start from Sunday
            dayIndex = 0;
        }
        
		for (NSInteger aWeek = 0; aWeek <= numberOfWeeks; aWeek ++)
        {
			CGFloat positionY = (aWeek * dayHeight);
			
			for (NSInteger aWeekday = dayIndex; aWeekday < numberOfDaysInWeek+dayIndex; aWeekday ++)
            {
				CGFloat positionX = ((aWeekday - dayIndex) * width);
				CGRect dayFrame = CGRectMake(positionX, positionY, width, dayHeight);
				NSDate *dayDate = [CalendarLogic dateForWeekday:aWeekday 
														 onWeek:aWeek 
												  referenceDate:[aLogic referenceDate]];
				NSDateComponents *dayComponents = [calendar 
												   components:NSDayCalendarUnit fromDate:dayDate];
                
                GRDayCell *cell = (GRDayCell*)[[[NSBundle mainBundle] loadNibNamed:@"GRDayCell" owner:nil options:nil] lastObject];
                
                cell.frame = dayFrame;
                cell.tag = [aDatesIndex count];
                
                [self addSubview:cell];
                
				UIColor *titleColor = nil;
                
				NSInteger distance = [self.calendarLogic distanceOfDateFromCurrentMonth:dayDate];
                
				if (distance == 0)
                {
					titleColor = cell.currentMonthDayColor;
				}
                else if (distance > 0)
                {
                    titleColor = cell.nextMonthDayColor;
                }
                else
                {
                    titleColor = cell.previousMonthDayColor;
                }
                
                cell.dayLabel.text = [NSString stringWithFormat:@"%d", [dayComponents day]];
                cell.dayLabel.font = cell.dayFont;
                cell.todayLabel.font = cell.dayFont;
                cell.todayLabel.textColor = cell.todayDayTitleColor;
                
				if ([dayDate compare:todayDate] != NSOrderedSame)
                {
                    cell.backgroundImageView.image = nil;
                    cell.todayLabel.text = @"";
                    
                    cell.dayLabel.textColor = titleColor;
				}
                else
                {
                    cell.backgroundImageView.image = nil;
                    
                    //cell.todayLabel.text = NSLocalizedString(@"Today", @"Today");
                    cell.dayLabel.textColor = cell.todayDayColor;
				}
                
				[cell.eventButton addTarget:self action:@selector(dayButtonPressed:) forControlEvents:UIControlEventTouchUpInside];
                
                [cell.protectedEventButton addTarget:self action:@selector(protectedEventButtonPressed:) forControlEvents:UIControlEventTouchUpInside];
                
                [cell.protectedEventButton2 addTarget:self action:@selector(protectedEventButtonPressed:) forControlEvents:UIControlEventTouchUpInside];
                
                [cell.protectedEventButton3 addTarget:self action:@selector(protectedEventButtonPressed:) forControlEvents:UIControlEventTouchUpInside];
                
                //cell.eventButton.backgroundColor = cell.eventBackgroundColor;
                //cell.protectedEventButton.backgroundColor = cell.eventProtectedBackgroundColor;
                //[cell.eventButton setTitleColor:cell.eventTitleColor forState:UIControlStateNormal];
                
				// Save
				[aDatesIndex addObject:dayDate];
				[aButtonsIndex addObject:cell];
			}
		}
		
		// save
		self.calendarLogic = aLogic;
		self.datesIndex = [[aDatesIndex copy] autorelease];
		self.buttonsIndex = [[aButtonsIndex copy] autorelease];
        
        
        NSArray *dayHeadersSymbols = [formatter shortWeekdaySymbols];

        if ([settings.weedStartsFrom intValue] == 0) {  //start from Monday
            NSUInteger firstWeekdayIndex = 1;
            if (firstWeekdayIndex > 0)
            {
                dayHeadersSymbols = [[dayHeadersSymbols subarrayWithRange:NSMakeRange(firstWeekdayIndex, 7-firstWeekdayIndex)]
                              arrayByAddingObjectsFromArray:[dayHeadersSymbols subarrayWithRange:NSMakeRange(0,firstWeekdayIndex)]];
            }
        }

		for (NSInteger aWeekday = 0; aWeekday < numberOfDaysInWeek; aWeekday ++)
        {
            
			NSString *symbol = [dayHeadersSymbols objectAtIndex:aWeekday];
			CGFloat positionX = (aWeekday * width);
			CGRect aFrame = CGRectMake(positionX, 25, width, 45);
			
            GRDayHeader *header = (GRDayHeader*)[[[NSBundle mainBundle] loadNibNamed:@"GRDayHeader" owner:nil options:nil] lastObject];
            header.frame = aFrame;
			header.titleLabel.text = [symbol stringByAppendingString:@"."];
			[self addSubview:header];
            [aLabelsIndex addObject:header];
		}
        
        self.headersIndex = [[aLabelsIndex copy] autorelease];
    }
    
    return self;
}

- (void)layoutSubviews
{
    [super layoutSubviews];
    
    CGFloat dayHeight = (self.bounds.size.height) / 6;
    CGFloat width = (self.bounds.size.width) / 7;
    
    int i = 0;
    
    for (NSInteger aWeek = 0; aWeek <= 5; aWeek ++)
    {
        CGFloat positionY = (aWeek * dayHeight);
        
        for (NSInteger aWeekday = 1; aWeekday <= numberOfDaysInWeek; aWeekday ++)
        {
            CGFloat positionX = ((aWeekday - 1) * width);
            CGRect dayFrame = CGRectMake(positionX, positionY, width, dayHeight);
            
            UIButton *button = [buttonsIndex objectAtIndex:i];
            button.frame = dayFrame;
            
            i++;
        }
    }
    
    for (NSInteger j = 0; j < [headersIndex count]; j++)
    {
        CGFloat positionX = (j * width);
        CGRect dayFrame = CGRectMake(positionX, 25, width, 45);
        
        UIView *header = [headersIndex objectAtIndex:j];
        header.frame = dayFrame;
    }
}

#pragma mark -
#pragma mark UI Controls

- (void)dayButtonPressed:(id)sender
{
	//[calendarLogic setReferenceDate:[datesIndex objectAtIndex:[sender tag]]];
    
    
}

- (void)protectedEventButtonPressed:(id)sender
{
    
}

- (void)selectButtonForDate:(NSDate *)aDate
{
    
}

- (void)reloadData
{
    NSCalendar *calendar = [AppDelegate getInstance].globalCalendar;
    
    
    Settings *settings = [[DataManager sharedInstance] getEntityWithName:kSettingsEntityName];
    
    if ([settings.weedStartsFrom intValue])
    {
        [calendar setFirstWeekday:1];
    }
    else
    {
        [calendar setFirstWeekday:2];
    }
    
	NSDateComponents *components = [calendar components:(NSDayCalendarUnit | NSMonthCalendarUnit | NSYearCalendarUnit) fromDate:[NSDate date]];
	NSDate *todayDate = [calendar dateFromComponents:components];
    
    NSInteger dayIndex = 1;
    if ([settings.weedStartsFrom intValue] == 1) {  //start from Sunday
//        dayIndex = 0;
    }

    for (NSInteger aWeek = 0; aWeek <= 5; aWeek ++)
    {
        for (NSInteger aWeekday = dayIndex; aWeekday < numberOfDaysInWeek+dayIndex; aWeekday ++)
        {
            NSDate *dayDate = [CalendarLogic dateForWeekday:aWeekday
                                                     onWeek:aWeek
                                              referenceDate:[calendarLogic referenceDate]];
                        
            NSArray *events = [delegate eventsForDate:dayDate];
            
            GRDayCell *cell = [buttonsIndex objectAtIndex:(aWeek * 7 + aWeekday - dayIndex)];
            
            [cell.eventButton removeTarget:nil action:nil forControlEvents:UIControlEventTouchUpInside];
            
            [cell.protectedEventButton removeTarget:nil action:nil forControlEvents:UIControlEventTouchUpInside];
            
            [cell.protectedEventButton2 removeTarget:nil action:nil forControlEvents:UIControlEventTouchUpInside];
            
            [cell.protectedEventButton3 removeTarget:nil action:nil forControlEvents:UIControlEventTouchUpInside];
            
            cell.event = nil;
            cell.protectedEvent = nil;
            cell.protectedEvent2 = nil;
            cell.protectedEvent3 = nil;
            
            cell.protectedEventButton.hidden = YES;
            cell.protectedEventButton2.hidden = YES;
            cell.protectedEventButton3.hidden = YES;
            cell.eventButton.hidden = YES;
            
            if ([events count])
            {
                int i = 0;
                int j = 0;
                
                for (Event *event in events)
                {
                    if ([event.persistent boolValue])
                    {
                        if (i == 0)
                        {
                            cell.protectedEventButton.hidden = NO;
                            i++;
                        }
                        else if (i == 1)
                        {
                            cell.protectedEventButton2.hidden = NO;
                            i++;
                        }
                        else if (i == 2)
                        {
                            cell.protectedEventButton3.hidden = NO;
                            i++;
                        }
                    }
                    else
                    {
                        if (j < 1)
                        {
                            cell.eventButton.hidden = NO;
                            j++;
                        }
                    }
                }
                
                if ([events count] > j + i)
                {
                    cell.otherEventsLabel.hidden = NO;
                    
                    cell.otherEventsLabel.text = [NSString stringWithFormat:NSLocalizedString(@"%i more", @"%i more"), [events count] - (j + i)];
                }
                else
                {
                    cell.otherEventsLabel.hidden = YES;
                }
                
                Event *event = nil;
                
                for (Event *e in events)
                {
                    if (![e.persistent boolValue])
                    {
                        event = e;
                        break ;
                    }
                }
                
                //[cell.eventButton setTitle:event.name forState:UIControlStateNormal];
                
                cell.event = event;
                
                i = 0;
                
                for (Event *e in events)
                {
                    if ([e.persistent boolValue])
                    {
                        if (i == 0)
                        {
                            cell.protectedEvent = e;
                            i++;
                        }
                        else if (i == 1)
                        {
                            cell.protectedEvent2 = e;
                            i++;
                        }
                        else if (i == 2)
                        {
                            cell.protectedEvent3 = e;
                            i++;
                        }
                        else
                        {
                            break ;
                        }
                    }
                }
                
                [cell.eventButton addTarget:self action:@selector(eventSelected:) forControlEvents:UIControlEventTouchUpInside];
                [cell.protectedEventButton addTarget:self action:@selector(eventSelected:) forControlEvents:UIControlEventTouchUpInside];
                [cell.protectedEventButton2 addTarget:self action:@selector(eventSelected:) forControlEvents:UIControlEventTouchUpInside];
                [cell.protectedEventButton3 addTarget:self action:@selector(eventSelected:) forControlEvents:UIControlEventTouchUpInside];
            }
            else
            {
                cell.eventButton.hidden = YES;
                cell.protectedEventButton.hidden = YES;
                cell.protectedEventButton2.hidden = YES;
                cell.protectedEventButton3.hidden = YES;
                cell.otherEventsLabel.hidden = YES;
            }
            
            //cell.eventButton.backgroundColor = cell.eventBackgroundColor;
            //cell.protectedEventButton.backgroundColor = cell.eventProtectedBackgroundColor;
            //[cell.eventButton setTitleColor:cell.eventTitleColor forState:UIControlStateNormal];
            
            if ([todayDate compare:dayDate] == NSOrderedDescending)
            {
                cell.otherEventsLabel.textColor = [UIColor colorWithRed:162.f/255.f green:162.f/255.f blue:162.f/255.f alpha:1.f];
            }
            else
            {
                cell.otherEventsLabel.textColor = [UIColor darkGrayColor];
            }
        }
    }
}

- (void)eventSelected:(UIButton*)button
{
    GRDayCell *cell  = (GRDayCell*)[button superview];
    
    if (button == cell.eventButton)
    {
        [self.delegate calendarViewController:nil didSelectEvent:cell.event];
    }
    else if (button == cell.protectedEventButton)
    {
        [self.delegate calendarViewController:nil didSelectEvent:cell.protectedEvent];
    }
    else if (button == cell.protectedEventButton2)
    {
        [self.delegate calendarViewController:nil didSelectEvent:cell.protectedEvent2];
    }
    else if (button == cell.protectedEventButton3)
    {
        [self.delegate calendarViewController:nil didSelectEvent:cell.protectedEvent3];
    }
}

@end
