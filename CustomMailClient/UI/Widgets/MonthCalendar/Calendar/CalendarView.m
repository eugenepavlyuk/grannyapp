//
//  CalendarView.m
//  Calendar
//
//  Created by Lloyd Bottomley on 29/04/10.
//  Copyright Savage Media Pty Ltd 2010. All rights reserved.
//

#import "CalendarView.h"

#import "CalendarLogic.h"
#import "CalendarMonth.h"

@interface CalendarView()

@property (nonatomic, retain) UISwipeGestureRecognizer *swipeLeftRecognizer;
@property (nonatomic, retain) UISwipeGestureRecognizer *swipeRightRecognizer;

- (void)handleSwipeFrom:(UISwipeGestureRecognizer *)recognizer;

@end


@implementation CalendarView


#pragma mark -
#pragma mark Getters / setters

@synthesize calendarViewControllerDelegate;

@synthesize calendarLogic;
@synthesize calendarView;
@synthesize calendarViewNew;
@synthesize selectedDate;

@synthesize swipeLeftRecognizer;
@synthesize swipeRightRecognizer;


- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder:aDecoder];
    
    if (self)
    {
        
    }
    
    return self;
}

- (void)awakeFromNib
{
    [super awakeFromNib];
    
    self.isSetup = NO;
}

- (void)setSelectedDate:(NSDate *)aDate
{
	[selectedDate autorelease];
	selectedDate = [aDate retain];
	
	[calendarLogic setReferenceDate:aDate];
	[calendarView selectButtonForDate:aDate];
}

- (void)setCalendarViewControllerDelegate:(NSObject<CalendarViewControllerDelegate> *)newCalendarViewControllerDelegate
{
    calendarViewControllerDelegate = newCalendarViewControllerDelegate;
    
    self.calendarView.delegate = newCalendarViewControllerDelegate;
}

#pragma mark -
#pragma mark Memory management

- (void)dealloc
{
	self.calendarViewControllerDelegate = nil;
	
	self.calendarLogic.calendarLogicDelegate = nil;
	self.calendarLogic = nil;
	
	self.calendarView = nil;
	self.calendarViewNew = nil;
	
	self.selectedDate = nil;
    
    self.swipeLeftRecognizer = nil;
    self.swipeRightRecognizer = nil;
	
    [super dealloc];
}


#pragma mark -
#pragma mark View delegate

- (void)setup
{
    if (self.isSetup)
    {
        return ;
    }
    
	self.clearsContextBeforeDrawing = NO;
	self.opaque = YES;
	
	NSDate *aDate = selectedDate;
    
	if (aDate == nil)
    {
		aDate = [CalendarLogic dateForToday];
	}
	
	CalendarLogic *aCalendarLogic = [[CalendarLogic alloc] initWithDelegate:self referenceDate:aDate];
	self.calendarLogic = aCalendarLogic;
	[aCalendarLogic release];

    [self addGestureRecognizer:self.swipeLeftRecognizer];
	[self addGestureRecognizer:self.swipeRightRecognizer];
    
	CalendarMonth *aCalendarView = [[CalendarMonth alloc] initWithFrame:self.bounds logic:calendarLogic];
	[aCalendarView selectButtonForDate:selectedDate];
	[self addSubview:aCalendarView];
    [aCalendarView refresh];
    
    aCalendarView.delegate = calendarViewControllerDelegate;
	
	self.calendarView = aCalendarView;
	[aCalendarView release];
    
    if (!self.isSetup)
    {
        [self reloadData];
    }
    
    self.isSetup = YES;
}

- (UISwipeGestureRecognizer *)swipeLeftRecognizer
{
	if (!_swipeLeftRecognizer)
    {
		_swipeLeftRecognizer = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(handleSwipeFrom:)];
		_swipeLeftRecognizer.direction = UISwipeGestureRecognizerDirectionLeft;
	}
    
	return _swipeLeftRecognizer;
}

- (UISwipeGestureRecognizer *)swipeRightRecognizer
{
	if (!_swipeRightRecognizer)
    {
		_swipeRightRecognizer = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(handleSwipeFrom:)];
		_swipeRightRecognizer.direction = UISwipeGestureRecognizerDirectionRight;
	}
    
	return _swipeRightRecognizer;
}

- (void)handleSwipeFrom:(UISwipeGestureRecognizer *)recognizer
{
	if (recognizer.direction == UISwipeGestureRecognizerDirectionLeft)
    {
		[calendarLogic selectNextMonth];
	}
    else  if (recognizer.direction == UISwipeGestureRecognizerDirectionRight)
    {
		[calendarLogic selectPreviousMonth];
	}
}

#pragma mark -
#pragma mark CalendarLogic delegate

- (void)calendarLogic:(CalendarLogic *)aLogic dateSelected:(NSDate *)aDate
{
	[selectedDate autorelease];
	selectedDate = [aDate retain];
	
	if ([calendarLogic distanceOfDateFromCurrentMonth:selectedDate] == 0)
    {
		[calendarView selectButtonForDate:selectedDate];
	}
	
//	[calendarViewControllerDelegate calendarViewController:self dateDidChange:aDate];
}

- (void)calendarLogic:(CalendarLogic *)aLogic monthChangeDirection:(NSInteger)aDirection
{
	BOOL animate = NO;
	
	CGFloat distance = self.bounds.size.width;
    
	if (aDirection < 0)
    {
		distance = -distance;
	}
	
	CalendarMonth *aCalendarView = [[CalendarMonth alloc] initWithFrame:self.bounds logic:aLogic];
	aCalendarView.userInteractionEnabled = NO;
    aCalendarView.delegate = calendarViewControllerDelegate;
    
	if ([calendarLogic distanceOfDateFromCurrentMonth:selectedDate] == 0)
    {
		[aCalendarView selectButtonForDate:selectedDate];
	}
    
	[self insertSubview:aCalendarView belowSubview:calendarView];
    [aCalendarView refresh];
	
	self.calendarViewNew = aCalendarView;
	[aCalendarView release];
	
	if (animate)
    {
		[UIView beginAnimations:NULL context:nil];
		[UIView setAnimationDelegate:self];
		[UIView setAnimationDidStopSelector:@selector(animationMonthSlideComplete)];
		[UIView setAnimationDuration:0.3];
		[UIView setAnimationCurve:UIViewAnimationCurveEaseInOut];
	}
	
	calendarView.frame = CGRectOffset(calendarView.frame, -distance, 0);
	aCalendarView.frame = CGRectOffset(aCalendarView.frame, -distance, 0);
	
	if (animate)
    {
		[UIView commitAnimations];
	}
    else
    {
		[self animationMonthSlideComplete];
	}
    
    [calendarViewControllerDelegate calendarViewController:nil dateDidChange:calendarLogic.referenceDate];
}

- (void)animationMonthSlideComplete
{
	// Get rid of the old one.
	[calendarView removeFromSuperview];
	
	// replace
	self.calendarView = calendarViewNew;
	self.calendarViewNew = nil;

    [self.calendarView reloadData];
    
	calendarView.userInteractionEnabled = YES;
}

- (void)layoutSubviews
{
    [super layoutSubviews];
    
    self.calendarView.frame = self.bounds;
}

- (void)reloadData
{
    [self.calendarView reloadData];
}

- (void)changeMonthToCurrent
{
    [self.calendarLogic selectCurrentMonth];
}

@end
