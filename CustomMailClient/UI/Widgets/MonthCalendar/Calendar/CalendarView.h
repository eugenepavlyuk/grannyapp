//
//  CalendarView.h
//  Calendar
//
//  Created by Lloyd Bottomley on 29/04/10.
//  Copyright Savage Media Pty Ltd 2010. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "CalendarLogicDelegate.h"
#import "CalendarViewControllerDelegate.h"

@class CalendarLogic;
@class CalendarMonth;

@interface CalendarView : UIView <CalendarLogicDelegate>
{
	NSObject <CalendarViewControllerDelegate> *calendarViewControllerDelegate;
	
	CalendarLogic *calendarLogic;
	CalendarMonth *calendarView;
	CalendarMonth *calendarViewNew;
	NSDate *selectedDate;
    
    UISwipeGestureRecognizer *_swipeLeftRecognizer, *_swipeRightRecognizer;
}

@property (nonatomic, assign) NSObject <CalendarViewControllerDelegate> *calendarViewControllerDelegate;

@property (nonatomic, retain) CalendarLogic *calendarLogic;
@property (nonatomic, retain) CalendarMonth *calendarView;
@property (nonatomic, retain) CalendarMonth *calendarViewNew;
@property (nonatomic, retain) NSDate *selectedDate;

@property (nonatomic, assign) BOOL isSetup;

- (void)animationMonthSlideComplete;

- (void)setup;

- (void)reloadData;

- (void)changeMonthToCurrent;

@end

