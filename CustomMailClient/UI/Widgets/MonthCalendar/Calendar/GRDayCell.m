//
//  GRDayCell.m
//  GrannyApp
//
//  Created by Eugene Pavlyuk on 10/5/12.
//
//

#import "GRDayCell.h"
#import "EventButton.h"

@implementation GRDayCell

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    
    if (self)
    {
        // Initialization code
    }
    
    return self;
}

- (void)awakeFromNib
{
    [super awakeFromNib];
    
    self.dayLabel.font = [UIFont fontWithName:@"Helvetica-Bold" size:30];
}

- (void)dealloc
{
    self.event = nil;
    
    self.protectedEvent = nil;
    self.protectedEvent2 = nil;
    self.protectedEvent3 = nil;
    
    self.eventButton = nil;
    self.protectedEventButton = nil;
    self.protectedEventButton2 = nil;
    self.protectedEventButton3 = nil;
    self.dayLabel = nil;
    self.todayLabel = nil;
    self.backgroundImageView = nil;
    self.otherEventsLabel = nil;
    self.borderImageView = nil;
    self.lowBorderImageView = nil;
    
    self.previousMonthDayColor = nil;
    self.nextMonthDayColor = nil;
    self.currentMonthDayColor = nil;
    self.todayDayColor = nil;
    self.dayFont = nil;
    self.eventBackgroundColor = nil;
    self.todayDayTitleColor = nil;
    self.eventProtectedBackgroundColor = nil;
    self.todayBackgroundDayColor = nil;
    self.lockImage = nil;
    self.eventImage = nil;
    
    [super dealloc];
}

- (void)didMoveToWindow
{
    [self.eventButton setImage:self.eventImage forState:UIControlStateNormal];
    [self.protectedEventButton setImage:self.lockImage forState:UIControlStateNormal];
    [self.protectedEventButton2 setImage:self.lockImage forState:UIControlStateNormal];
    [self.protectedEventButton3 setImage:self.lockImage forState:UIControlStateNormal];
}

@end
