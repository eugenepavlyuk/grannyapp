//
//  GRMailAttachemntCell.h
//  GrannyApp
//
//  Created by Ievgen Pavliuk on 6/1/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>

#define MimeTypeKey @"mimekey"
#define PathKey     @"path"
#define FileNameKey @"name"

@protocol GRMailAttachemntCellDelegate <NSObject>

- (void)mailAttachmentCellRemoveWithIndex:(NSInteger)index;

@end

@interface GRMailAttachemntCell : UICollectionViewCell

@property (nonatomic, assign)id<GRMailAttachemntCellDelegate> delegate;
@property (nonatomic, assign) NSInteger index;
@property (nonatomic, retain) IBOutlet UILabel *fileNameLabel;
@property (nonatomic, retain) IBOutlet UIImageView *icon;

- (void)updateForItem:(NSDictionary *)item;

@end
