//
//  GRCommonAlertView.m
//  GrannyApp
//
//  Created by Ievgen Pavliuk on 8/2/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "GRCommonAlertView.h"

@implementation GRCommonAlertView
@synthesize delegate;
#pragma mark - TV channel delete confirm

- (id)initForTVChannelDeleteConfirmWithView:(UIView *)view
{
    CGFloat superWidth = view.frame.size.width;
    CGFloat superHeight = view.frame.size.height;
    
    if (IS_IPAD)
    {
        superWidth = 1024.0f;
        superHeight = 748.0f;
    }
    
    if ((self = [super initWithFrame:CGRectMake(0, 0, superWidth, superHeight)]))
    {
        NSString *fileName = @"Settings_AlertMailBck_iPhone.png";
        if (IS_IPAD)
            fileName = @"Common_Alertview_bck";
        
        alertAction = GRAlertRadioStationDelete;
        self.backgroundColor = [UIColor clearColor];
        UIImageView *backgroundImage = [[[UIImageView alloc] initWithImage:[[UIImage imageNamed:fileName] resizableImageWithCapInsets:UIEdgeInsetsMake(40, 40, 40, 40)]] autorelease];
        backgroundImage.frame = CGRectMake(0, 0, backgroundImage.frame.size.width, backgroundImage.frame.size.height);
        
        UIView *contentView = [[[UIView alloc] initWithFrame:CGRectMake(self.frame.size.width/2 - backgroundImage.frame.size.width/2, self.frame.size.height/2 - backgroundImage.frame.size.height/2, backgroundImage.frame.size.width, backgroundImage.frame.size.height)] autorelease];
        [contentView addSubview:backgroundImage];
        
        UILabel *lTitle = [[[UILabel alloc] initWithFrame:CGRectMake(0, 0, contentView.frame.size.width, contentView.frame.size.height/2)] autorelease];
        lTitle.font = [UIFont fontWithName:@"Helvetica-Bold" size:IS_IPAD ? 22 : 16];
        lTitle.textColor = [UIColor whiteColor];
        lTitle.backgroundColor = [UIColor clearColor];
        lTitle.textAlignment = NSTextAlignmentCenter;
        lTitle.numberOfLines = 2;
        lTitle.text = @"ARE YOU SURE YOU WANT TO DELETE THIS TV CHANNEL?";
        [contentView addSubview:lTitle];
        
        fileName = @"Settings_AlertCancelBtn_iPhone.png";
        if (IS_IPAD)
            fileName = @"Settings_AlertCancelBtn.png";
        UIImage *imageNo = [UIImage imageNamed:fileName];
        
        fileName = @"Settings_StationDeleteYesBtn_iPhone.png";
        if (IS_IPAD)
            fileName = @"Settings_StationDeleteYesBtn.png";
        UIImage *imageYes = [UIImage imageNamed:fileName];
        
        UIButton *noBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        [noBtn setBackgroundImage:imageNo forState:UIControlStateNormal];
        noBtn.frame = CGRectMake((int)((contentView.frame.size.width - imageNo.size.width - imageYes.size.width - 5)/2), contentView.frame.size.height - imageNo.size.height - 10, imageNo.size.width, imageNo.size.height);
        [noBtn addTarget:self action:@selector(stationDeleteNoBtnPressed:) forControlEvents:UIControlEventTouchUpInside];
        [contentView addSubview:noBtn];
        
        UIButton *yesBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        [yesBtn setBackgroundImage:imageYes forState:UIControlStateNormal];
        yesBtn.frame = CGRectMake(noBtn.frame.origin.x + noBtn.frame.size.width + 5, noBtn.frame.origin.y, imageYes.size.width, imageYes.size.height);
        [yesBtn addTarget:self action:@selector(stationDeleteYesBtnPressed:) forControlEvents:UIControlEventTouchUpInside];
        [contentView addSubview:yesBtn];
        
        [self addSubview:contentView];
        
        if (IS_IPAD)
            [[AppDelegate getInstance].window.rootViewController.view addSubview:self];
        else
            [view addSubview:self];
    }
    return self;
}

- (void)stationDeleteNoBtnPressed:(id)sender {
    if ([delegate respondsToSelector:@selector(commonAlert:buttonWithIndex:)]) {
        [delegate commonAlert:self buttonWithIndex:0];
    }
    [self removeFromSuperview];
}

- (void)stationDeleteYesBtnPressed:(id)sender {
    if ([delegate respondsToSelector:@selector(commonAlert:buttonWithIndex:)]) {
        [delegate commonAlert:self buttonWithIndex:1];
    }
    [self removeFromSuperview];
}

- (void)onOk
{
    if ([delegate respondsToSelector:@selector(commonAlert:buttonWithIndex:)])
        [delegate commonAlert:self buttonWithIndex:0];
    [self removeFromSuperview];
}

- (void)onCancel
{
    if ([delegate respondsToSelector:@selector(commonAlert:buttonWithIndex:)])
        [delegate commonAlert:self buttonWithIndex:1];
    [self removeFromSuperview];
}

- (void)onRead
{
    if ([delegate respondsToSelector:@selector(commonAlert:buttonWithIndex:)])
        [delegate commonAlert:self buttonWithIndex:0];
    [self removeFromSuperview];
}
- (void)onWrite
{
    if ([delegate respondsToSelector:@selector(commonAlert:buttonWithIndex:)])
        [delegate commonAlert:self buttonWithIndex:1];
    [self removeFromSuperview];
}

- (void) okBtnPressed
{
    [self removeFromSuperview];
}

#pragma mark - TV channel edit confirm

- (id)initForTVChannelEditConfirmWithView:(UIView *)view
{
    CGFloat superWidth = view.frame.size.width;
    CGFloat superHeight = view.frame.size.height;
    
    if (IS_IPAD)
    {
        superWidth = 1024.0f;
        superHeight = 748.0f;
    }
    
    if ((self = [super initWithFrame:CGRectMake(0, 0, superWidth, superHeight)]))
    {
        NSString *fileName = @"Settings_AlertOkBck_iPhone.png";
        if (IS_IPAD)
            fileName = @"Settings_StationEditAlertBck.png";
        
        alertAction = GRAlertRadioStationDelete;
        self.backgroundColor = [UIColor clearColor];
        UIImageView *backgroundImage = [[[UIImageView alloc] initWithImage:[UIImage imageNamed:fileName]] autorelease];
        backgroundImage.frame = CGRectMake(0, 0, backgroundImage.frame.size.width, backgroundImage.frame.size.height);
        
        UIView *contentView = [[[UIView alloc] initWithFrame:CGRectMake(self.frame.size.width/2 - backgroundImage.frame.size.width/2, self.frame.size.height/2 - backgroundImage.frame.size.height/2, backgroundImage.frame.size.width, backgroundImage.frame.size.height)] autorelease];
        [contentView addSubview:backgroundImage];
        
        UILabel *lTitle = [[[UILabel alloc] initWithFrame:CGRectMake(0, 0, contentView.frame.size.width, contentView.frame.size.height/2)] autorelease];
        lTitle.font = [UIFont fontWithName:@"Helvetica-Bold" size:IS_IPAD ? 22 : 16];
        lTitle.textColor = [UIColor whiteColor];
        lTitle.backgroundColor = [UIColor clearColor];
        lTitle.textAlignment = NSTextAlignmentCenter;
        lTitle.numberOfLines = 2;
        lTitle.text = @"THE TV CHANNEL WAS SUCCESSFULLY UPDATED";
        [contentView addSubview:lTitle];
        
        fileName = @"Settings_StationEditAlertOkBtn_iPhone.png";
        if (IS_IPAD)
            fileName = @"Settings_StationEditAlertOkBtn.png";
        
        UIImage *image = [UIImage imageNamed:fileName];
        
        UIButton *noBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        [noBtn setBackgroundImage:image forState:UIControlStateNormal];
        noBtn.frame = CGRectMake((int)((contentView.frame.size.width - image.size.width)/2), contentView.frame.size.height - image.size.height - 10, image.size.width, image.size.height);
        //        noBtn.frame = CGRectMake(contentView.frame.size.width/2 - noBtn.frame.size.width/2, contentView.frame.size.height/2, noBtn.frame.size.width, noBtn.frame.size.height);
        [noBtn addTarget:self action:@selector(stationDeleteNoBtnPressed:) forControlEvents:UIControlEventTouchUpInside];
        [contentView addSubview:noBtn];
        
        [self addSubview:contentView];
        
        if (IS_IPAD)
            [[AppDelegate getInstance].window.rootViewController.view addSubview:self];
        else
            [view addSubview:self];
    }
    
    return self;
}

- (id)initCommonAlert:(NSString *)alertMessage
{
    CGFloat superWidth = 480.0f;
    CGFloat superHeight = 300.0f;
    if (IS_IPAD)
    {
        superWidth = 1024.0f;
        superHeight = 748.0f;
    }
    if ((self = [super initWithFrame:CGRectMake(0, 0, superWidth, superHeight)]))
    {
        alertAction = GRAlertRadioStationDelete;
        self.backgroundColor = [UIColor clearColor];
        UIImageView *backgroundImage = [[[UIImageView alloc] initWithImage:[UIImage imageNamed:@"Settings_AlertOkBck.png"]] autorelease];
        backgroundImage.frame = CGRectMake(0, 0, backgroundImage.frame.size.width, backgroundImage.frame.size.height);
        
        UIView *contentView = [[[UIView alloc] initWithFrame:CGRectMake((int)((self.frame.size.width - backgroundImage.frame.size.width)/2), 30+(int)((self.frame.size.height - backgroundImage.frame.size.height)/2), backgroundImage.frame.size.width, backgroundImage.frame.size.height)] autorelease];
        [contentView addSubview:backgroundImage];
        
        UILabel *lTitle = [[[UILabel alloc] initWithFrame:CGRectMake(0, 0, contentView.frame.size.width, contentView.frame.size.height/2)] autorelease];
        lTitle.font = [UIFont fontWithName:@"Helvetica-Bold" size:22];
        lTitle.textColor = [UIColor whiteColor];
        lTitle.backgroundColor = [UIColor clearColor];
        lTitle.textAlignment = NSTextAlignmentCenter;
        lTitle.numberOfLines = 2;
        lTitle.text = alertMessage;
        [contentView addSubview:lTitle];
        
        UIButton *okBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        [okBtn setBackgroundImage:[UIImage imageNamed:@"Settings_StationEditAlertOkBtn.png"] forState:UIControlStateNormal];
        okBtn.frame = CGRectMake(0, 0, 171, 53);
        okBtn.frame = CGRectMake(contentView.frame.size.width/2 - okBtn.frame.size.width/2, contentView.frame.size.height/2, okBtn.frame.size.width, okBtn.frame.size.height);
        [okBtn addTarget:self action:@selector(okBtnPressed) forControlEvents:UIControlEventTouchUpInside];
        [contentView addSubview:okBtn];
        
        [self addSubview:contentView];
        [[AppDelegate getInstance].window.rootViewController.view addSubview:self];
    }
    return self;
}

- (id)initCommonAlert:(NSString *)alertMessage inView:(UIView *) view
{
    CGFloat superWidth = view.frame.size.width;
    CGFloat superHeight = view.frame.size.height;
    if (IS_IPAD)
    {
        superWidth = 1024.0f;
        superHeight = 768.0f;
    }
    if ((self = [super initWithFrame:CGRectMake(0, 0, superWidth, superHeight)]))
    {
        alertAction = GRAlertRadioStationDelete;
        self.backgroundColor = [UIColor colorWithWhite:0 alpha:0.8];
        
        NSString *alertBkgImageName = @"common_alert_bkg";
        
        UIImageView *alertBkg = [[UIImageView alloc] initWithImage:[UIImage imageNamed:alertBkgImageName]];
        
        UIView *contentView = [[UIView alloc] init];
        CGRect frame = contentView.frame;
        frame.size = alertBkg.frame.size;
        contentView.frame = frame;
        contentView.center = self.center;
        
        [contentView addSubview:alertBkg];
        [alertBkg release];
        
        UILabel *lTitle = [[UILabel alloc] initWithFrame:CGRectMake(10, 20, contentView.frame.size.width-35, contentView.frame.size.height/2-10)];
        
        NSInteger fontSize = 45;
        if ([alertMessage rangeOfString:@"No internet"].location != NSNotFound) {
            fontSize = 24;
        }
        
        lTitle.font = [UIFont fontWithName:@"Helvetica-Bold" size:IS_IPAD ? fontSize : 16];
        lTitle.textColor = [UIColor colorWithWhite:0.26f alpha:1.0f];
        lTitle.textAlignment = NSTextAlignmentCenter;
        lTitle.numberOfLines = 2;
        lTitle.adjustsFontSizeToFitWidth = YES;
        lTitle.backgroundColor = [UIColor clearColor];
        lTitle.text = alertMessage;
        [contentView addSubview:lTitle];
        [lTitle release];
        
        UIButton *noBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        frame = noBtn.frame;
        frame.size = CGSizeMake(148, 65);
        noBtn.frame = frame;
        noBtn.center = CGPointMake(CGRectGetMidX(contentView.bounds), contentView.frame.size.height * 3.0f/4.0f);
        noBtn.frame = CGRectIntegral(noBtn.frame);
        noBtn.backgroundColor = [UIColor colorWithWhite:0.65f alpha:1.0f];
        [noBtn setTitle:NSLocalizedString(@"OK", @"OK") forState:UIControlStateNormal];
        noBtn.titleLabel.font = [UIFont fontWithName:@"Helvetica-Bold" size:48.0f];
        [noBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        [noBtn addTarget:self action:@selector(stationDeleteNoBtnPressed:) forControlEvents:UIControlEventTouchUpInside];
        [contentView addSubview:noBtn];
        
        [self addSubview:contentView];
        [contentView release];
        
        if (IS_IPAD)
            [[AppDelegate getInstance].window.rootViewController.view addSubview:self];
        else
            [view addSubview:self];
    }
    return self;
}

- (id)initCommonAlert:(NSString *)alertMessage inView:(UIView *)view pointerPosition:(CGPoint)pointer {
    CGFloat superWidth = view.frame.size.width;
    CGFloat superHeight = view.frame.size.height;
    
    if (IS_IPAD)
    {
        superWidth = 1024.0f;
        superHeight = 748.0f;
    }
    if ((self = [super initWithFrame:CGRectMake(0, 0, superWidth, superHeight)]))
    {
        alertAction = GRAlertRadioStationDelete;
        self.backgroundColor = [UIColor colorWithWhite:0 alpha:0.8];
        
        NSString *alertBkgImageName = @"email_recipient_alert_bkg";
        
        UIImageView *alertBkg = [[UIImageView alloc] initWithImage:[UIImage imageNamed:alertBkgImageName]];
        
        UIView *contentView = [[UIView alloc] init];
        CGRect frame = contentView.frame;
        frame.size = alertBkg.frame.size;
        contentView.frame = frame;
        contentView.center = CGPointMake(pointer.x, pointer.y + contentView.frame.size.height/2);
        contentView.frame = CGRectIntegral(contentView.frame);
        [contentView addSubview:alertBkg];
        [alertBkg release];
        
        UILabel *lTitle = [[UILabel alloc] initWithFrame:CGRectMake(5, 30,
                                                                    contentView.frame.size.width - 10, CGRectGetMidY(contentView.bounds) - 10)];
        
        NSInteger fontSize = 48;
        if ([alertMessage rangeOfString:@"No internet"].location != NSNotFound) {
            fontSize = 24;
        }
        
        lTitle.font = [UIFont fontWithName:@"Helvetica-Bold" size:IS_IPAD ? fontSize : 16];
        lTitle.textColor = [UIColor colorWithWhite:0.26f alpha:1.0f];
        lTitle.textAlignment = NSTextAlignmentCenter;
        lTitle.numberOfLines = 2;
        lTitle.text = alertMessage;
        [contentView addSubview:lTitle];
        [lTitle release];
        
        UIButton *noBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        frame = noBtn.frame;
        frame.size = CGSizeMake(148, 65);
        noBtn.frame = frame;
        noBtn.center = CGPointMake(CGRectGetMidX(contentView.bounds), contentView.frame.size.height * 3.0f/4.0f);
        noBtn.frame = CGRectIntegral(noBtn.frame);
        noBtn.backgroundColor = [UIColor colorWithWhite:0.65f alpha:1.0f];
        [noBtn setTitle:NSLocalizedString(@"OK", @"OK") forState:UIControlStateNormal];
        noBtn.titleLabel.font = [UIFont fontWithName:@"Helvetica-Bold" size:48.0f];
        [noBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        [noBtn addTarget:self action:@selector(stationDeleteNoBtnPressed:) forControlEvents:UIControlEventTouchUpInside];
        [contentView addSubview:noBtn];
        
        [self addSubview:contentView];
        [contentView release];
        
        if (IS_IPAD)
            [[AppDelegate getInstance].window.rootViewController.view addSubview:self];
        else
            [view addSubview:self];
    }
    return self;
}

- (id)initCommonChooseAlert:(NSString *)alertMessage inView:(UIView *) view
{
    CGFloat superWidth = view.frame.size.width;
    CGFloat superHeight = view.frame.size.height;
    
    if (IS_IPAD)
    {
        superWidth = 1024.0f;
        superHeight = 748.0f;
    }
    
    if ((self = [super initWithFrame:CGRectMake(0, 0, superWidth, superHeight)]))
    {
        NSString *fileName = @"Settings_StationDeleteAlertBck_iPhone.png";
        if (IS_IPAD)
            fileName = @"Settings_StationDeleteAlertBck.png";
        
        alertAction = GRAlertRadioStationDelete;
        self.backgroundColor = [UIColor clearColor];
        UIImageView *backgroundImage = [[[UIImageView alloc] initWithImage:[UIImage imageNamed:fileName]] autorelease];
        backgroundImage.frame = CGRectMake(0, 0, backgroundImage.frame.size.width, backgroundImage.frame.size.height);
        
        UIView *contentView = [[[UIView alloc] initWithFrame:CGRectMake(self.frame.size.width/2 - backgroundImage.frame.size.width/2, self.frame.size.height/2 - backgroundImage.frame.size.height/2, backgroundImage.frame.size.width, backgroundImage.frame.size.height)] autorelease];
        [contentView addSubview:backgroundImage];
        
        UILabel *lTitle = [[[UILabel alloc] initWithFrame:CGRectMake(0, 0, contentView.frame.size.width, contentView.frame.size.height/2)] autorelease];
        lTitle.font = [UIFont fontWithName:@"Helvetica-Bold" size:IS_IPAD ? 22 : 16];
        lTitle.textColor = [UIColor whiteColor];
        lTitle.backgroundColor = [UIColor clearColor];
        lTitle.textAlignment = NSTextAlignmentCenter;
        lTitle.numberOfLines = 2;
        lTitle.text = alertMessage;
        [contentView addSubview:lTitle];
        
        fileName = @"Settings_StationDeleteNoBtn_iPhone.png";
        if (IS_IPAD)
            fileName = @"Settings_StationEditAlertOkBtn_iPad.png";
        UIImage *imageNo = [UIImage imageNamed:fileName];
        
        fileName = @"Settings_StationEditAlertOkBtn_iPhone.png";
        if (IS_IPAD)
            fileName = @"Settings_AlertCancelBtn.png";
        UIImage *imageYes = [UIImage imageNamed:fileName];
        
        UIButton *noBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        [noBtn setBackgroundImage:imageNo forState:UIControlStateNormal];
        noBtn.frame = CGRectMake((int)((contentView.frame.size.width - imageNo.size.width - imageYes.size.width - 5)/2), contentView.frame.size.height - imageNo.size.height - 10, imageNo.size.width, imageNo.size.height);
        [noBtn addTarget:self action:@selector(stationDeleteNoBtnPressed:) forControlEvents:UIControlEventTouchUpInside];
        [contentView addSubview:noBtn];
        
        UIButton *yesBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        [yesBtn setBackgroundImage:imageYes forState:UIControlStateNormal];
        yesBtn.frame = CGRectMake(noBtn.frame.origin.x + noBtn.frame.size.width + 5, noBtn.frame.origin.y, imageYes.size.width, imageYes.size.height);
        [yesBtn addTarget:self action:@selector(stationDeleteYesBtnPressed:) forControlEvents:UIControlEventTouchUpInside];
        [contentView addSubview:yesBtn];
        
        [self addSubview:contentView];
        
        if (IS_IPAD)
            [[AppDelegate getInstance].window.rootViewController.view addSubview:self];
        else
            [view addSubview:self];
    }
    return self;
}

- (id)initContactAlertWhenCallsNotAvailableWithAlert:(NSString *)alertMessage inView:(UIView *) view
{
    CGFloat superWidth = view.frame.size.width;
    CGFloat superHeight = view.frame.size.height;
    
    if (IS_IPAD)
    {
        superWidth = 1024.0f;
        superHeight = 768.0f;
    }
    
    if ((self = [super initWithFrame:CGRectMake(0, 0, superWidth, superHeight)]))
    {
        NSString *fileName = @"Common_Alertview_bck~ipad.png";
        
        self.backgroundColor = [UIColor clearColor];
        
        UIView *blackView = [[[UIView alloc] initWithFrame:self.frame] autorelease];
        blackView.backgroundColor = [UIColor blackColor];
        blackView.alpha = 0.85;
        [self addSubview:blackView];
        
        UIImageView *backgroundImage = [[[UIImageView alloc] initWithImage:[[UIImage imageNamed:fileName] resizableImageWithCapInsets:UIEdgeInsetsMake(30, 30, 30, 30)]] autorelease];
        
            backgroundImage.frame = CGRectMake(0, 0, backgroundImage.frame.size.width, backgroundImage.frame.size.height);
        
        UIView *contentView = [[[UIView alloc] initWithFrame:CGRectMake(self.frame.size.width/2 - backgroundImage.frame.size.width/2, self.frame.size.height/2 - backgroundImage.frame.size.height/2, backgroundImage.frame.size.width, backgroundImage.frame.size.height)] autorelease];
        [contentView addSubview:backgroundImage];
        
        UIImage *image = [UIImage imageNamed:@"people_alert_btn_h"];
        
        UILabel *lTitle = [[[UILabel alloc] initWithFrame:CGRectMake(20, 0, contentView.frame.size.width - 40, contentView.frame.size.height/3)] autorelease];
        lTitle.font = [UIFont fontWithName:@"Helvetica-Bold" size:IS_IPAD ? 35 : 16];
        lTitle.textColor = [UIColor blackColor];
        lTitle.backgroundColor = [UIColor clearColor];
        lTitle.textAlignment = NSTextAlignmentCenter;
        lTitle.numberOfLines = 10;
        lTitle.text = alertMessage;
        CGSize size = [lTitle.text sizeWithFont:lTitle.font constrainedToSize:CGSizeMake(contentView.frame.size.width - 40, 10000)];
        lTitle.frame = CGRectMake(20, 20, contentView.bounds.size.width - 40, size.height);
        [contentView addSubview:lTitle];
        
        UIButton *writeBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        [writeBtn setBackgroundImage:[image resizableImageWithCapInsets:UIEdgeInsetsMake(20, 20, 20, 20)] forState:UIControlStateNormal];
        [writeBtn setTitle:NSLocalizedString(@"OK", nil) forState:UIControlStateNormal];
        [writeBtn setTitleColor:rgb(0xffffff) forState:UIControlStateHighlighted];
        [writeBtn setTitleColor:rgb(0x000000) forState:UIControlStateNormal];
        writeBtn.titleLabel.font = [UIFont boldSystemFontOfSize:35];
        writeBtn.frame = CGRectMake(0, lTitle.frame.origin.y + lTitle.frame.size.height + 15, (contentView.bounds.size.width) / 2, 80);
        
        writeBtn.center = CGPointMake((contentView.bounds.size.width) / 2, writeBtn.center.y);
        
        [writeBtn addTarget:self action:@selector(okBtnPressed) forControlEvents:UIControlEventTouchUpInside];
        [contentView addSubview:writeBtn];
        
        [self addSubview:contentView];
        
        if (IS_IPAD)
            [[AppDelegate getInstance].window.rootViewController.view addSubview:self];
        else
            [view addSubview:self];
    }
    
    return self;
}

- (id)initNoEmailAlert:(NSString *)alertMessage inView:(UIView *) view
{
    CGFloat superWidth = view.frame.size.width;
    CGFloat superHeight = view.frame.size.height;
    
    if (IS_IPAD)
    {
        superWidth = 1024.0f;
        superHeight = 768.0f;
    }
    
    if ((self = [super initWithFrame:CGRectMake(0, 0, superWidth, superHeight)]))
    {
        NSString *fileName = @"Common_Alertview_bck~ipad.png";
        
        self.backgroundColor = [UIColor clearColor];
        
        UIView *blackView = [[[UIView alloc] initWithFrame:self.frame] autorelease];
        blackView.backgroundColor = [UIColor blackColor];
        blackView.alpha = 0.85;
        [self addSubview:blackView];
        
        UILabel *lTitle = [[[UILabel alloc] initWithFrame:CGRectZero] autorelease];
        lTitle.font = [UIFont fontWithName:@"Helvetica-Bold" size:IS_IPAD ? 35 : 16];
        lTitle.textColor = [UIColor blackColor];
        lTitle.backgroundColor = [UIColor clearColor];
        lTitle.textAlignment = NSTextAlignmentCenter;
        lTitle.numberOfLines = 3;
        lTitle.text = alertMessage;
        
        CGSize size = [lTitle.text sizeWithFont:lTitle.font constrainedToSize:CGSizeMake(360, 10000)];
        lTitle.frame = CGRectMake(20, 10, size.width, size.height);
        
        UIView *contentView = [[[UIView alloc] initWithFrame:CGRectMake(self.frame.size.width/2 - 200, self.frame.size.height/2 - size.height/ 2 - 80, 400, size.height + 140)] autorelease];
        
        [contentView addSubview:lTitle];
        
        
        UIImageView *backgroundImage = [[[UIImageView alloc] initWithImage:[[UIImage imageNamed:fileName] resizableImageWithCapInsets:UIEdgeInsetsMake(40, 40, 40, 40)]] autorelease];
        backgroundImage.frame = CGRectMake(0, 0, contentView.bounds.size.width, contentView.bounds.size.height);
        

        [contentView addSubview:backgroundImage];
        [contentView sendSubviewToBack:backgroundImage];
        
        UIImage *image = [UIImage imageNamed:@"Common_Alertview_btn.png"];
        
        //        image = [UIImage imageNamed:@"Common_Alertview_Cancel_btn.png"];
        UIButton *cancelBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        [cancelBtn setBackgroundImage:image forState:UIControlStateNormal];
        [cancelBtn setTitle:NSLocalizedString(@"OK", nil) forState:UIControlStateNormal];
        [cancelBtn setTitleColor:rgb(0xffffff) forState:UIControlStateHighlighted];
        [cancelBtn setTitleColor:rgb(0x848484) forState:UIControlStateNormal];
        cancelBtn.titleLabel.font = [UIFont boldSystemFontOfSize:30];
        cancelBtn.frame = CGRectMake((int)((contentView.frame.size.width - image.size.width)/2), contentView.bounds.size.height - 100, image.size.width, image.size.height);
        [cancelBtn addTarget:self action:@selector(okBtnPressed) forControlEvents:UIControlEventTouchUpInside];
        [contentView addSubview:cancelBtn];
        
        [self addSubview:contentView];
        
        if (IS_IPAD)
            [[AppDelegate getInstance].window.rootViewController.view addSubview:self];
        else
            [view addSubview:self];
    }
    return self;
}

#pragma mark - Mail Account created confirm

- (id)initForMailAccountCreatedErrorWithSize:(CGSize)_size
{
    CGFloat superWidth = _size.width;
    CGFloat superHeight = _size.height;
    
    if (IS_IPAD) 
    {
        superWidth = 1024.0f;
        superHeight = 748.0f;
    }
    
    if ((self = [super initWithFrame:CGRectMake(0, 0, superWidth, superHeight)])) 
    {
        NSString *fileName = @"Settings_AlertMailBck_iPhone.png";
        if (IS_IPAD)
            fileName = @"Settings_AlertMailBck.png";
        
        alertAction = GRAlertRadioStationDelete;
        self.backgroundColor = [UIColor clearColor];
        UIImageView *backgroundImage = [[[UIImageView alloc] initWithImage:[UIImage imageNamed:fileName]] autorelease];
        backgroundImage.frame = CGRectMake(0, 0, backgroundImage.frame.size.width, backgroundImage.frame.size.height);
        
        UIView *contentView = [[[UIView alloc] initWithFrame:CGRectMake(self.frame.size.width/2 - backgroundImage.frame.size.width/2, self.frame.size.height/2 - backgroundImage.frame.size.height/2, backgroundImage.frame.size.width, backgroundImage.frame.size.height)] autorelease];
        [contentView addSubview:backgroundImage];
        
        UILabel *lTitle = [[[UILabel alloc] initWithFrame:CGRectMake(0, 0, contentView.frame.size.width, contentView.frame.size.height/2)] autorelease];
        lTitle.font = [UIFont fontWithName:@"Helvetica-Bold" size:IS_IPAD ? 22 : 16];
        lTitle.textColor = [UIColor whiteColor];
        lTitle.backgroundColor = [UIColor clearColor];
        lTitle.textAlignment = NSTextAlignmentCenter;
        lTitle.numberOfLines = 2;
        lTitle.text = NSLocalizedString(@"THE USERNAME OR PASSWORD IS INCORRECT. PLEASE TRY AGAIN", @"THE USERNAME OR PASSWORD IS INCORRECT. PLEASE TRY AGAIN");
        [contentView addSubview:lTitle];
        
        fileName = @"Settings_AlertCancelBtn_iPhone.png";
        if (IS_IPAD)
            fileName = @"Settings_AlertCancelBtn.png";
        
        UIImage *image = [UIImage imageNamed:fileName];
        UIButton *noBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        [noBtn setBackgroundImage:image forState:UIControlStateNormal];
        noBtn.frame = CGRectMake((int)((contentView.frame.size.width - image.size.width*2 - 5)/2), contentView.frame.size.height - image.size.height - 10, image.size.width, image.size.height);
        [noBtn addTarget:self action:@selector(stationDeleteNoBtnPressed:) forControlEvents:UIControlEventTouchUpInside];
        [contentView addSubview:noBtn];
        
        fileName = @"Settings_StationEditAlertOkBtn_iPhone.png";
        if (IS_IPAD)
            fileName = @"Settings_StationEditAlertOkBtn_iPad.png";
        
        image = [UIImage imageNamed:fileName];
        UIButton *yesBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        [yesBtn setBackgroundImage:image forState:UIControlStateNormal];
        yesBtn.frame = CGRectMake(noBtn.frame.origin.x + noBtn.frame.size.width + 5, noBtn.frame.origin.y, image.size.width, image.size.height);
        [yesBtn addTarget:self action:@selector(stationDeleteYesBtnPressed:) forControlEvents:UIControlEventTouchUpInside];
        [contentView addSubview:yesBtn];
        
        [self addSubview:contentView];
        [[AppDelegate getInstance].window.rootViewController.view addSubview:self];
    }
    return self;
}

- (id)initCommonAlertWithText:(NSString *)alertMessage size:(CGSize)_size
{
    CGFloat superWidth = _size.width;
    CGFloat superHeight = _size.height;
    
    if ((self = [super initWithFrame:CGRectMake(0, 0, superWidth, superHeight)]))
    {
        alertAction = GRAlertRadioStationDelete;
        self.backgroundColor = [UIColor clearColor];
        UIImageView *backgroundImage = [[[UIImageView alloc] initWithImage:[UIImage imageNamed:@"Mail_Alert_Background.png"]] autorelease];
        backgroundImage.frame = CGRectMake(0, 0, backgroundImage.frame.size.width, backgroundImage.frame.size.height);
        
        UIView *contentView = [[[UIView alloc] initWithFrame:CGRectMake(self.frame.size.width/2 - backgroundImage.frame.size.width/2, self.frame.size.height/2 - backgroundImage.frame.size.height/2, backgroundImage.frame.size.width, backgroundImage.frame.size.height)] autorelease];
        [contentView addSubview:backgroundImage];
        
        UILabel *lTitle = [[[UILabel alloc] initWithFrame:CGRectMake(0, 5, contentView.frame.size.width, contentView.frame.size.height/2)] autorelease];
        lTitle.font = [UIFont fontWithName:@"Helvetica-Bold" size:16];
        lTitle.textColor = [UIColor whiteColor];
        lTitle.backgroundColor = [UIColor clearColor];
        lTitle.textAlignment = NSTextAlignmentCenter;
        lTitle.numberOfLines = 2;
        lTitle.text = alertMessage;
        [contentView addSubview:lTitle];
        
        UIImage *image = [UIImage imageNamed:@"Mail_Alert_SaveDraft_Btn.png"];
        UIButton *noBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        [noBtn setBackgroundImage:image forState:UIControlStateNormal];
        noBtn.frame = CGRectMake((int)((contentView.frame.size.width - image.size.width*2 - 5)/2), contentView.frame.size.height - image.size.height - 10, image.size.width, image.size.height);
        [noBtn addTarget:self action:@selector(onSave:) forControlEvents:UIControlEventTouchUpInside];
        [contentView addSubview:noBtn];
        
        image = [UIImage imageNamed:@"Mail_Alert_Delete_Btn.png"];
        UIButton *yesBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        [yesBtn setBackgroundImage:image forState:UIControlStateNormal];
        yesBtn.frame = CGRectMake(noBtn.frame.origin.x + noBtn.frame.size.width + 5, noBtn.frame.origin.y, image.size.width, image.size.height);
        [yesBtn addTarget:self action:@selector(onDelete:) forControlEvents:UIControlEventTouchUpInside];
        [contentView addSubview:yesBtn];
        
        [self addSubview:contentView];
        [[AppDelegate getInstance].window.rootViewController.view addSubview:self];
    }
    
    return self;
}

- (void)onSave:(id)sender {
    if ([delegate respondsToSelector:@selector(commonAlert:buttonWithIndex:)]) {
        [delegate commonAlert:self buttonWithIndex:0];
    }
    [self removeFromSuperview];
}

- (void)onDelete:(id)sender {
    if ([delegate respondsToSelector:@selector(commonAlert:buttonWithIndex:)]) {
        [delegate commonAlert:self buttonWithIndex:1];
    }
    [self removeFromSuperview];
}

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)dealloc {
    delegate = nil;
    [super dealloc];
}

@end
