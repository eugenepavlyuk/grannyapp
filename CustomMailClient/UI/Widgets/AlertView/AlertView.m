//
//  AlertView.m
//
//  Created by Eugene Pavlyuk on 1/19/11.
//

#import "AlertView.h"
#import "Event.h"
#import "MedicineAlarm.h"
#import <QuartzCore/QuartzCore.h>

@interface AlertOverlayWindow : UIWindow
{
}
@property (nonatomic,retain) UIWindow* oldKeyWindow;

@property (nonatomic, assign) BOOL useRadialGradient;

@end

@implementation  AlertOverlayWindow

@synthesize oldKeyWindow;
@synthesize useRadialGradient;

- (id)init
{
    self = [super init];
    
    if (self)
    {
        self.useRadialGradient = YES;
    }
    
    return self;
}

- (void) makeKeyAndVisible
{
	self.oldKeyWindow = [[UIApplication sharedApplication] keyWindow];
	self.windowLevel = UIWindowLevelAlert;
	[super makeKeyAndVisible];
}

- (void) resignKeyWindow
{
	[super resignKeyWindow];
	[self.oldKeyWindow makeKeyWindow];
}

- (void) drawRect: (CGRect) rect
{
	// render the radial gradient behind the alertview
	if (useRadialGradient)
    {
        CGFloat width			= self.frame.size.width;
        CGFloat height			= self.frame.size.height;
        CGFloat locations[3]	= { 0.0, 0.5, 1.0 	};
        CGFloat components[12]	= {	1, 1, 1, 0.5,
            0, 0, 0, 0.5,
            0, 0, 0, 0.7	};
        
        CGColorSpaceRef colorspace = CGColorSpaceCreateDeviceRGB();
        CGGradientRef backgroundGradient = CGGradientCreateWithColorComponents(colorspace, components, locations, 3);
        CGColorSpaceRelease(colorspace);
        
        CGContextDrawRadialGradient(UIGraphicsGetCurrentContext(), 
                                    backgroundGradient, 
                                    CGPointMake(width/2, height/2), 0,
                                    CGPointMake(width/2, height/2), width,
                                    0);
        
        CGGradientRelease(backgroundGradient);
    }
    else
    {
        [super drawRect:rect];
    }
}

- (void) dealloc
{
	self.oldKeyWindow = nil;
	
	[super dealloc];
}

@end

@interface AlertView (private)

@property (nonatomic, readonly) NSMutableArray* buttons;
@property (nonatomic, readonly) UILabel* titleLabel;
@property (nonatomic, readonly) UILabel* messageLabel;
@property (nonatomic, readonly) UITextView* messageTextView;

- (void) AlertView_commonInit;
- (void) releaseWindow: (int) buttonIndex;
- (void) pulse;
- (CGSize) titleLabelSize;
- (CGSize) messageLabelSize;
- (CGSize) inputTextFieldSize;
- (CGSize) buttonsAreaSize_Stacked;
- (CGSize) buttonsAreaSize_SideBySide;
- (CGSize) recalcSizeAndLayout: (BOOL) layout;

@end

@interface AlertViewController : UIViewController
{
}

@end

@implementation AlertViewController

- (BOOL) shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation
{
    return UIInterfaceOrientationIsLandscape(toInterfaceOrientation);
}

- (BOOL)prefersStatusBarHidden
{
    return YES;
}

- (void)willAnimateRotationToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation duration:(NSTimeInterval)duration
{
	AlertView* av = [self.view.subviews lastObject];
	if (!av || ![av isKindOfClass:[AlertView class]])
		return;
	// resize the alertview if it wants to make use of any extra space (or needs to contract)
	[UIView animateWithDuration:duration 
					 animations:^{
						 [av sizeToFit];
                         
                         if ([av isKindOfClass:[CommentAlertView class]])
                         {
                             av.center = CGPointMake( CGRectGetMidX( self.view.bounds ), 150 );
                         }
                         else
                         {
                             av.center = CGPointMake( CGRectGetMidX( self.view.bounds ), CGRectGetMidY( self.view.bounds ) );
                         }
                         
						 av.frame = CGRectIntegral( av.frame );
					 }];
}

- (void) dealloc
{
	[super dealloc];
}

@end


@implementation AlertView

@synthesize delegate;
@synthesize cancelButtonIndex;
@synthesize firstOtherButtonIndex;
@synthesize buttonLayout;
@synthesize width;
@synthesize maxHeight;
@synthesize usesMessageTextView;
@synthesize backgroundImage = _backgroundImage;
@synthesize style;
@synthesize userInfo;

const CGFloat kAlertView_LeftMargin	= 15.0;
const CGFloat kAlertView_TopMargin	= 16.0;
const CGFloat kAlertView_BottomMargin = 15.0;
const CGFloat kAlertView_RowMargin	= 5.0;
const CGFloat kAlertView_ColumnMargin = 10.0;

- (id) init 
{
	if ( ( self = [super init] ) )
	{
		[self AlertView_commonInit];
	}
	return self;
}

- (id) initWithFrame:(CGRect)frame
{
	if ( ( self = [super initWithFrame: frame] ) )
	{
		[self AlertView_commonInit];
		
		if ( !CGRectIsEmpty( frame ) )
		{
			width = frame.size.width;
			maxHeight = frame.size.height;
		}
	}
	return self;
}

- (IBAction)okButtonTapped
{
    
}

- (IBAction)cancelButtonTapped
{
    
}

- (id) initWithTitle: (NSString *) t message: (NSString *) m delegate: (id) d cancelButtonTitle: (NSString *) cancelButtonTitle otherButtonTitles: (NSString *) otherButtonTitles, ...
{
	if ( (self = [super init] ) ) // will call into initWithFrame, thus AlertView_commonInit is called
	{
		self.title = t;
		self.message = m;
		self.delegate = d;
		
		if ( nil != cancelButtonTitle )
		{
			[self addButtonWithTitle: cancelButtonTitle ];
			self.cancelButtonIndex = 0;
		}
		
		if ( nil != otherButtonTitles )
		{
			firstOtherButtonIndex = [self.buttons count];
			[self addButtonWithTitle: otherButtonTitles ];
			
			va_list args;
			va_start(args, otherButtonTitles);
			
			id arg;
			while ( nil != ( arg = va_arg( args, id ) ) ) 
			{
				if ( ![arg isKindOfClass: [NSString class] ] )
					return nil;
				
				[self addButtonWithTitle: (NSString*)arg ];
			}
		}
	}
	
	return self;
}

- (CGSize) sizeThatFits: (CGSize) unused 
{
	CGSize s = [self recalcSizeAndLayout: NO];
	return s;
}

- (void) layoutSubviews
{
	[self recalcSizeAndLayout: YES];
}

- (void) drawRect:(CGRect)rect
{
	[self.backgroundImage drawInRect: rect];
}

- (void)dealloc 
{
	[_backgroundImage release];
	[_buttons release];
	[_titleLabel release];
	[_messageLabel release];
	[_messageTextView release];
	[_messageTextViewMaskImageView release];
	self.userInfo = nil;
	[[NSNotificationCenter defaultCenter] removeObserver: self ];
	
    [super dealloc];
}


- (void) AlertView_commonInit
{
	self.backgroundColor = [UIColor clearColor];
	self.autoresizingMask = UIViewAutoresizingFlexibleLeftMargin | UIViewAutoresizingFlexibleRightMargin | UIViewAutoresizingFlexibleTopMargin | UIViewAutoresizingFlexibleBottomMargin; 
	
	// defaults:
	style = AlertViewStyleNormal;
	self.width = 0; // set to default
	self.maxHeight = 0; // set to default
	buttonLayout = AlertViewButtonLayoutNormal;
	cancelButtonIndex = -1;
	firstOtherButtonIndex = -1;
}

- (void) setWidth:(CGFloat) w
{
	if ( w <= 0 )
		w = 284;
	
	width = MAX( w, self.backgroundImage.size.width );
}

- (CGFloat) width
{
	if ( nil == self.superview )
		return width;
	
	CGFloat maxWidth = self.superview.bounds.size.width - 20;
	
	return MIN( width, maxWidth );
}

- (void) setMaxHeight:(CGFloat) h
{
	if ( h <= 0 )
		h = 358;
	
	maxHeight = MAX( h, self.backgroundImage.size.height );
}

- (CGFloat) maxHeight
{
	if ( nil == self.superview )
		return maxHeight;
	
	return MIN( maxHeight, self.superview.bounds.size.height - 20 );
}

- (void) setStyle:(AlertViewStyle)newStyle
{
	if ( style != newStyle )
	{
		style = newStyle;
		
		if ( style == AlertViewStyleInput )
		{
			// need to watch for keyboard
			[[NSNotificationCenter defaultCenter] addObserver: self selector: @selector( onKeyboardWillShow:) name: UIKeyboardWillShowNotification object: nil];
			[[NSNotificationCenter defaultCenter] addObserver: self selector: @selector( onKeyboardWillHide:) name: UIKeyboardWillHideNotification object: nil];
		}
	}
}

- (void) onKeyboardWillShow: (NSNotification*) note
{
	NSValue* v = [note.userInfo objectForKey: UIKeyboardFrameEndUserInfoKey];
	CGRect kbframe = [v CGRectValue];
	kbframe = [self.superview convertRect: kbframe fromView: nil];
	
	if ( CGRectIntersectsRect( self.frame, kbframe) )
	{
		CGPoint c = self.center;
		
		if ( self.frame.size.height > kbframe.origin.y - 20 )
		{
			self.maxHeight = kbframe.origin.y - 20;
			[self sizeToFit];
			[self layoutSubviews];
		}
		
		c.y = kbframe.origin.y / 2;
		
		[UIView animateWithDuration: 0.2 
						 animations: ^{
							 self.center = c;
							 self.frame = CGRectIntegral(self.frame);
						 }];
	}
}

- (void) onKeyboardWillHide: (NSNotification*) note
{
	[UIView animateWithDuration: 0.2 
					 animations: ^{
						 self.center = CGPointMake( CGRectGetMidX( self.superview.bounds ), CGRectGetMidY( self.superview.bounds ));
						 self.frame = CGRectIntegral(self.frame);
					 }];
}

- (NSMutableArray*) buttons
{
	if ( _buttons == nil )
	{
		_buttons = [[NSMutableArray arrayWithCapacity:4] retain];
	}
	
	return _buttons;
}

- (UILabel*) titleLabel
{
	if ( _titleLabel == nil )
	{
		_titleLabel = [[UILabel alloc] init];
		_titleLabel.font = [UIFont boldSystemFontOfSize: 18];
		_titleLabel.backgroundColor = [UIColor clearColor];
		_titleLabel.textColor = [UIColor whiteColor];
		_titleLabel.textAlignment = NSTextAlignmentCenter;
		_titleLabel.lineBreakMode = NSLineBreakByWordWrapping;
		_titleLabel.numberOfLines = 0;
	}
	
	return _titleLabel;
}

- (UILabel*) messageLabel
{
	if ( _messageLabel == nil )
	{
		_messageLabel = [[UILabel alloc] init];
		_messageLabel.font = [UIFont systemFontOfSize:25];
		_messageLabel.backgroundColor = [UIColor clearColor];
		_messageLabel.textColor = [UIColor blackColor];
		_messageLabel.textAlignment = NSTextAlignmentCenter;
		_messageLabel.lineBreakMode = NSLineBreakByWordWrapping;
		_messageLabel.numberOfLines = 0;
	}
	
	return _messageLabel;
}

- (UITextView*) messageTextView
{
	if ( _messageTextView == nil )
	{
		_messageTextView = [[UITextView alloc] init];
		_messageTextView.editable = NO;
		_messageTextView.font = [UIFont systemFontOfSize: 16];
		_messageTextView.backgroundColor = [UIColor whiteColor];
		_messageTextView.textColor = [UIColor darkTextColor];
		_messageTextView.textAlignment = NSTextAlignmentLeft;
		_messageTextView.bounces = YES;
		_messageTextView.alwaysBounceVertical = YES;
		_messageTextView.layer.cornerRadius = 5;
	}
	
	return _messageTextView;
}

- (UIImageView*) messageTextViewMaskView
{
	if ( _messageTextViewMaskImageView == nil )
	{
		UIImage* shadowImage = [[UIImage imageNamed:@"AlertViewMessageListViewShadow.png"] stretchableImageWithLeftCapWidth:6 topCapHeight:7];
		
		_messageTextViewMaskImageView = [[UIImageView alloc] initWithImage: shadowImage];
		_messageTextViewMaskImageView.userInteractionEnabled = NO;
		_messageTextViewMaskImageView.layer.masksToBounds = YES;
		_messageTextViewMaskImageView.layer.cornerRadius = 6;
	}
	return _messageTextViewMaskImageView;
}

- (UITextField*) inputTextField
{
	if ( _inputTextField == nil )
	{
		_inputTextField = [[UITextField alloc] init];
		_inputTextField.borderStyle = UITextBorderStyleRoundedRect;
	}
	
	return _inputTextField;
}

- (UIImage*) backgroundImage
{
	if ( _backgroundImage == nil )
	{
		self.backgroundImage = [[UIImage imageNamed: @"AlertViewBackground.png"] stretchableImageWithLeftCapWidth: 15 topCapHeight: 30];
	}
	
	return _backgroundImage;
}

- (void) setTitle:(NSString *)t
{
	self.titleLabel.text = t;
}

- (NSString*) title 
{
	return self.titleLabel.text;
}

- (void) setMessage:(NSString *)t
{
	self.messageLabel.text = t;
	self.messageTextView.text = t;
}

- (NSString*) message  
{
	return self.messageLabel.text;
}

- (NSInteger) numberOfButtons
{
	return [self.buttons count];
}

- (void) setCancelButtonIndex:(NSInteger)buttonIndex
{
	// avoid a NSRange exception
	if ( buttonIndex < 0 || buttonIndex >= [self.buttons count] )
		return;
	
	cancelButtonIndex = buttonIndex;
	
	UIButton* b = [self.buttons objectAtIndex: buttonIndex];
	
	UIImage* buttonBgNormal = [UIImage imageNamed: @"AlertViewCancelButtonBackground.png"];
	buttonBgNormal = [buttonBgNormal stretchableImageWithLeftCapWidth: buttonBgNormal.size.width / 2.0 topCapHeight: buttonBgNormal.size.height / 2.0];
	[b setBackgroundImage: buttonBgNormal forState: UIControlStateNormal];
	
	UIImage* buttonBgPressed = [UIImage imageNamed: @"AlertViewButtonBackground_Highlighted.png"];
	buttonBgPressed = [buttonBgPressed stretchableImageWithLeftCapWidth: buttonBgPressed.size.width / 2.0 topCapHeight: buttonBgPressed.size.height / 2.0];
	[b setBackgroundImage: buttonBgPressed forState: UIControlStateHighlighted];
}

- (BOOL) isVisible
{
	return self.superview != nil;
}

- (NSInteger) addButtonWithTitle: (NSString *) t
{
	UIButton* b = [UIButton buttonWithType: UIButtonTypeCustom];
	[b setTitle: t forState: UIControlStateNormal];
	
	UIImage* buttonBgNormal = [UIImage imageNamed: @"AlertViewButtonBackground.png"];
	buttonBgNormal = [buttonBgNormal stretchableImageWithLeftCapWidth: buttonBgNormal.size.width / 2.0 topCapHeight: buttonBgNormal.size.height / 2.0];
	[b setBackgroundImage: buttonBgNormal forState: UIControlStateNormal];
	
	UIImage* buttonBgPressed = [UIImage imageNamed: @"AlertViewButtonBackground_Highlighted.png"];
	buttonBgPressed = [buttonBgPressed stretchableImageWithLeftCapWidth: buttonBgPressed.size.width / 2.0 topCapHeight: buttonBgPressed.size.height / 2.0];
	[b setBackgroundImage: buttonBgPressed forState: UIControlStateHighlighted];
	
	[b addTarget: self action: @selector(onButtonPress:) forControlEvents: UIControlEventTouchUpInside];
	
	[self.buttons addObject: b];
	
	[self setNeedsLayout];
	
	return self.buttons.count-1;
}

- (NSString *) buttonTitleAtIndex:(NSInteger)buttonIndex
{
	// avoid a NSRange exception
	if ( buttonIndex < 0 || buttonIndex >= [self.buttons count] )
		return nil;
	
	UIButton* b = [self.buttons objectAtIndex: buttonIndex];
	
	return [b titleForState: UIControlStateNormal];
}

- (void) dismissWithClickedButtonIndex: (NSInteger)buttonIndex animated: (BOOL) animated
{
	if ( self.style == AlertViewStyleInput && [self.inputTextField isFirstResponder] )
	{
		[self.inputTextField resignFirstResponder];
	}
	
	if ( [self.delegate respondsToSelector: @selector(alertView:willDismissWithButtonIndex:)] )
	{
		[self.delegate alertView: self willDismissWithButtonIndex: buttonIndex ];
	}
	
	if ( animated )
	{
		self.window.backgroundColor = [UIColor clearColor];
		self.window.alpha = 1;
		
		[UIView animateWithDuration: 0.2 
						 animations: ^{
							 [self.window resignKeyWindow];
							 self.window.alpha = 0;
						 }
						 completion: ^(BOOL finished) {
							 [self releaseWindow: buttonIndex];
						 }];
		
		[UIView commitAnimations];
	}
	else
	{
		[self.window resignKeyWindow];
		
		[self releaseWindow: buttonIndex];
	}
}

- (void) releaseWindow: (int) buttonIndex
{
	if ( [self.delegate respondsToSelector: @selector(alertView:didDismissWithButtonIndex:)] )
	{
		[self.delegate alertView: self didDismissWithButtonIndex: buttonIndex ];
	}
	
	// the one place we release the window we allocated in "show"
	// this will propogate releases to us (AlertView), and our AlertViewController
	
	[self.window release];
}

- (void) show
{
	[[NSRunLoop currentRunLoop] runMode: NSDefaultRunLoopMode beforeDate:[NSDate date]];
	
	AlertViewController* avc = [[[AlertViewController alloc] init] autorelease];
	avc.view.backgroundColor = [UIColor clearColor];
	
	// $important - the window is released only when the user clicks an alert view button
	AlertOverlayWindow* ow = [[AlertOverlayWindow alloc] initWithFrame: [UIScreen mainScreen].bounds];
	ow.alpha = 0.0;
	ow.backgroundColor = [UIColor colorWithRed:0.f green:0.f blue:0.f alpha:0.6f];
	ow.rootViewController = avc;
	[ow makeKeyAndVisible];
	
	// fade in the window
	[UIView animateWithDuration: 0.2 animations: ^{
		ow.alpha = 1;
	}];
	
	// add and pulse the alertview
	// add the alertview
	[avc.view addSubview: self];
	[self sizeToFit];
	self.center = CGPointMake( CGRectGetMidX( avc.view.bounds ), CGRectGetMidY( avc.view.bounds ) );;
	self.frame = CGRectIntegral( self.frame );
	[self pulse];
	
	if ( self.style == AlertViewStyleInput )
	{
		[self layoutSubviews];
		[self.inputTextField becomeFirstResponder];
	}
}

- (void) pulse
{
    self.transform = CGAffineTransformMakeScale(0.6, 0.6);
	[UIView animateWithDuration: 0.2 
					 animations: ^{
						 self.transform = CGAffineTransformMakeScale(1.1, 1.1);
					 }
					 completion: ^(BOOL finished){
						 [UIView animateWithDuration:1.0/15.0
										  animations: ^{
											  self.transform = CGAffineTransformMakeScale(0.9, 0.9);
										  }
										  completion: ^(BOOL finished){
											  [UIView animateWithDuration:1.0/7.5
															   animations: ^{
																   self.transform = CGAffineTransformIdentity;
															   }];
										  }];
					 }];
	
}

- (void) onButtonPress: (id) sender
{
	int buttonIndex = [_buttons indexOfObjectIdenticalTo: sender];
	
	if ( [self.delegate respondsToSelector: @selector(alertView:clickedButtonAtIndex:)] )
	{
		[self.delegate alertView: self clickedButtonAtIndex: buttonIndex ];
	}
	
	if ( buttonIndex == self.cancelButtonIndex )
	{
		if ( [self.delegate respondsToSelector: @selector(alertViewCancel:)] )
		{
			[self.delegate alertViewCancel: self ];
		}	
	}
	
	[self dismissWithClickedButtonIndex: buttonIndex  animated: YES];
}

- (CGSize) recalcSizeAndLayout: (BOOL) layout
{
	BOOL	stacked = !(self.buttonLayout == AlertViewButtonLayoutNormal && [self.buttons count] == 2 );
	
	CGFloat maxWidth = self.width - (kAlertView_LeftMargin * 2);
	
	CGSize  titleLabelSize = [self titleLabelSize];
	CGSize  messageViewSize = [self messageLabelSize];
	CGSize  inputTextFieldSize = [self inputTextFieldSize];
	CGSize  buttonsAreaSize = stacked ? [self buttonsAreaSize_Stacked] : [self buttonsAreaSize_SideBySide];
	
	CGFloat inputRowHeight = self.style == AlertViewStyleInput ? inputTextFieldSize.height + kAlertView_RowMargin : 0;
	
	CGFloat totalHeight = kAlertView_TopMargin + titleLabelSize.height + kAlertView_RowMargin + messageViewSize.height + inputRowHeight + kAlertView_RowMargin + buttonsAreaSize.height + kAlertView_BottomMargin;
	
	if ( totalHeight > self.maxHeight )
	{
		// too tall - we'll condense by using a textView (with scrolling) for the message
		
		totalHeight -= messageViewSize.height;
		//$$what if it's still too tall?
		messageViewSize.height = self.maxHeight - totalHeight;
		
		totalHeight = self.maxHeight;
		
		self.usesMessageTextView = YES;
	}
	
	if ( layout )
	{
		// title
		CGFloat y = kAlertView_TopMargin;
		if ( self.title != nil )
		{
			self.titleLabel.frame = CGRectMake( kAlertView_LeftMargin, y, titleLabelSize.width, titleLabelSize.height );
			[self addSubview: self.titleLabel];
			y += titleLabelSize.height + kAlertView_RowMargin;
		}
		
		// message
		if ( self.message != nil )
		{
			if ( self.usesMessageTextView )
			{
				self.messageTextView.frame = CGRectMake( kAlertView_LeftMargin, y, messageViewSize.width, messageViewSize.height );
				[self addSubview: self.messageTextView];
				y += messageViewSize.height + kAlertView_RowMargin;
				
				UIImageView* maskImageView = [self messageTextViewMaskView];
				maskImageView.frame = self.messageTextView.frame;
				[self addSubview: maskImageView];
			}
			else
			{
				self.messageLabel.frame = CGRectMake( kAlertView_LeftMargin, y, messageViewSize.width, messageViewSize.height );
				[self addSubview: self.messageLabel];
				y += messageViewSize.height + kAlertView_RowMargin;
			}
		}
		
		// input
		if ( self.style == AlertViewStyleInput )
		{
			self.inputTextField.frame = CGRectMake( kAlertView_LeftMargin, y, inputTextFieldSize.width, inputTextFieldSize.height );
			[self addSubview: self.inputTextField];
			y += inputTextFieldSize.height + kAlertView_RowMargin;
		}
		
		// buttons
		CGFloat buttonHeight = [[self.buttons objectAtIndex:0] sizeThatFits: CGSizeZero].height;
		if ( stacked )
		{
			CGFloat buttonWidth = maxWidth;
			for ( UIButton* b in self.buttons )
			{
				b.frame = CGRectMake( kAlertView_LeftMargin, y, buttonWidth, buttonHeight );
				[self addSubview: b];
				y += buttonHeight + kAlertView_RowMargin;
			}
		}
		else
		{
			CGFloat buttonWidth = (maxWidth - kAlertView_ColumnMargin) / 2.0;
			CGFloat x = kAlertView_LeftMargin;
			for ( UIButton* b in self.buttons )
			{
				b.frame = CGRectMake( x, y, buttonWidth, buttonHeight );
				[self addSubview: b];
				x += buttonWidth + kAlertView_ColumnMargin;
			}
		}
		
	}
	
	return CGSizeMake( self.width, totalHeight );
}

- (CGSize) titleLabelSize
{
	CGFloat maxWidth = self.width - (kAlertView_LeftMargin * 2);
	CGSize s = [self.titleLabel.text sizeWithFont: self.titleLabel.font constrainedToSize: CGSizeMake(maxWidth, 1000) lineBreakMode: self.titleLabel.lineBreakMode];
	if ( s.width < maxWidth )
		s.width = maxWidth;
	
	return s;
}

- (CGSize) messageLabelSize
{
	CGFloat maxWidth = self.width - (kAlertView_LeftMargin * 2);
	CGSize s = [self.messageLabel.text sizeWithFont: self.messageLabel.font constrainedToSize: CGSizeMake(maxWidth, 1000) lineBreakMode: self.messageLabel.lineBreakMode];
	if ( s.width < maxWidth )
		s.width = maxWidth;
	
	return s;
}

- (CGSize) inputTextFieldSize
{
	if ( self.style == AlertViewStyleNormal)
		return CGSizeZero;
	
	CGFloat maxWidth = self.width - (kAlertView_LeftMargin * 2);
	
	CGSize s = [self.inputTextField sizeThatFits: CGSizeZero];
	
	return CGSizeMake( maxWidth, s.height );
}

- (CGSize) buttonsAreaSize_SideBySide
{
	CGFloat maxWidth = self.width - (kAlertView_LeftMargin * 2);
	
	CGSize bs = [[self.buttons objectAtIndex:0] sizeThatFits: CGSizeZero];
	
	bs.width = maxWidth;
	
	return bs;
}

- (CGSize) buttonsAreaSize_Stacked
{
	CGFloat maxWidth = self.width - (kAlertView_LeftMargin * 2);
	int buttonCount = [self.buttons count];
	
	CGSize bs = [[self.buttons objectAtIndex:0] sizeThatFits: CGSizeZero];
	
	bs.width = maxWidth;
	
	bs.height = (bs.height * buttonCount) + (kAlertView_RowMargin * (buttonCount-1));
	
	return bs;
}

@end


@implementation CommentAlertView

- (void) show
{
	[[NSRunLoop currentRunLoop] runMode: NSDefaultRunLoopMode beforeDate:[NSDate date]];
	
	AlertViewController* avc = [[[AlertViewController alloc] init] autorelease];
	avc.view.backgroundColor = [UIColor clearColor];
	
	// $important - the window is released only when the user clicks an alert view button
	AlertOverlayWindow* ow = [[AlertOverlayWindow alloc] initWithFrame: [UIScreen mainScreen].bounds];
    ow.useRadialGradient = NO;
	ow.alpha = 0.0;
	ow.backgroundColor = [UIColor colorWithRed:0.f green:0.f blue:0.f alpha:0.6f];
	ow.rootViewController = avc;
	[ow makeKeyAndVisible];
	
	// fade in the window
	[UIView animateWithDuration: 0.2 animations: ^{
		ow.alpha = 1;
	}];
	
	// add and pulse the alertview
	// add the alertview
	[avc.view addSubview: self];
	[self sizeToFit];
	self.center = CGPointMake( CGRectGetMidX( avc.view.bounds ), 150 );
	self.frame = CGRectIntegral( self.frame );
	[self pulse];
	
	if ( self.style == AlertViewStyleInput )
	{
		[self layoutSubviews];
		[self.inputTextField becomeFirstResponder];
	}
}

- (UIImage*) backgroundImage
{
	if ( _backgroundImage == nil )
	{
		self.backgroundImage = [UIImage imageNamed:@"alert_background"];
	}
	
	return _backgroundImage;
}

- (UILabel*) titleLabel
{
	if ( _titleLabel == nil )
	{
		_titleLabel = [[UILabel alloc] init];
		_titleLabel.font = [UIFont fontWithName:@"Adelle-Bold" size:20];
		_titleLabel.backgroundColor = [UIColor clearColor];
		_titleLabel.textColor = [UIColor blackColor];
		_titleLabel.textAlignment = NSTextAlignmentCenter;
		_titleLabel.lineBreakMode = NSLineBreakByWordWrapping;
		_titleLabel.numberOfLines = 0;
	}
	
	return _titleLabel;
}

- (UILabel*) messageLabel
{
	if ( _messageLabel == nil )
	{
		_messageLabel = [[UILabel alloc] init];
		_messageLabel.font = [UIFont systemFontOfSize: 16];
		_messageLabel.backgroundColor = [UIColor clearColor];
		_messageLabel.textColor = [UIColor darkGrayColor];
		_messageLabel.textAlignment = NSTextAlignmentCenter;
		_messageLabel.lineBreakMode = NSLineBreakByWordWrapping;
		_messageLabel.numberOfLines = 0;
	}
	
	return _messageLabel;
}

- (NSInteger) addButtonWithTitle: (NSString *) t
{
	UIButton* b = [UIButton buttonWithType: UIButtonTypeCustom];
	[b setTitle: t forState: UIControlStateNormal];
    b.titleLabel.font = [UIFont systemFontOfSize:14];
	UIImage* buttonBgNormal = [UIImage imageNamed: @"login_small_reg_btn.png"];
	[b setBackgroundImage: buttonBgNormal forState: UIControlStateNormal];
	
	UIImage* buttonBgPressed = [UIImage imageNamed: @"login_small_reg_btn_h.png"];
	[b setBackgroundImage: buttonBgPressed forState: UIControlStateHighlighted];
	
	[b addTarget: self action: @selector(onButtonPress:) forControlEvents: UIControlEventTouchUpInside];
	
	[self.buttons addObject: b];
	
	[self setNeedsLayout];
	
	return self.buttons.count-1;
}

- (CGSize) buttonsAreaSize_Stacked
{	
	CGSize bs = [[self.buttons objectAtIndex:0] sizeThatFits: CGSizeZero];
	
	bs.width = 74.f;
	
	bs.height = 29.f;
	
	return bs;
}

- (CGSize) recalcSizeAndLayout: (BOOL) layout
{
	BOOL	stacked = !(self.buttonLayout == AlertViewButtonLayoutNormal && [self.buttons count] == 2 );
	
	CGFloat maxWidth = self.width - (kAlertView_LeftMargin * 2);
	
	CGSize  titleLabelSize = [self titleLabelSize];
	CGSize  messageViewSize = [self messageLabelSize];
	CGSize  inputTextFieldSize = [self inputTextFieldSize];
	CGSize  buttonsAreaSize = stacked ? [self buttonsAreaSize_Stacked] : [self buttonsAreaSize_SideBySide];
	
	CGFloat inputRowHeight = self.style == AlertViewStyleInput ? inputTextFieldSize.height + kAlertView_RowMargin : 0;
	
	CGFloat totalHeight = kAlertView_TopMargin + titleLabelSize.height + kAlertView_RowMargin + messageViewSize.height + inputRowHeight + kAlertView_RowMargin + buttonsAreaSize.height + kAlertView_BottomMargin;
	
	if ( totalHeight > self.maxHeight )
	{
		// too tall - we'll condense by using a textView (with scrolling) for the message
		
		totalHeight -= messageViewSize.height;
		//$$what if it's still too tall?
		messageViewSize.height = self.maxHeight - totalHeight;
		
		totalHeight = self.maxHeight;
		
		self.usesMessageTextView = YES;
	}
	
	if ( layout )
	{
		// title
		CGFloat y = kAlertView_TopMargin;
		if ( self.title != nil )
		{
			self.titleLabel.frame = CGRectMake( kAlertView_LeftMargin, y, titleLabelSize.width, titleLabelSize.height );
			[self addSubview: self.titleLabel];
			y += titleLabelSize.height + kAlertView_RowMargin;
		}
		
		// message
		if ( self.message != nil )
		{
			if ( self.usesMessageTextView )
			{
				self.messageTextView.frame = CGRectMake( kAlertView_LeftMargin, y, messageViewSize.width, messageViewSize.height );
				[self addSubview: self.messageTextView];
				y += messageViewSize.height + kAlertView_RowMargin;
				
				UIImageView* maskImageView = [self messageTextViewMaskView];
				maskImageView.frame = self.messageTextView.frame;
				[self addSubview: maskImageView];
			}
			else
			{
				self.messageLabel.frame = CGRectMake( kAlertView_LeftMargin, y, messageViewSize.width, messageViewSize.height );
				[self addSubview: self.messageLabel];
				y += messageViewSize.height + kAlertView_RowMargin;
			}
		}
		
		// input
		if ( self.style == AlertViewStyleInput )
		{
			self.inputTextField.frame = CGRectMake( kAlertView_LeftMargin, y, inputTextFieldSize.width, inputTextFieldSize.height );
			[self addSubview: self.inputTextField];
			y += inputTextFieldSize.height + kAlertView_RowMargin;
		}
		
		// buttons
		CGFloat buttonHeight = [[self.buttons objectAtIndex:0] sizeThatFits: CGSizeZero].height;
		if ( stacked )
		{
//			CGFloat buttonWidth = maxWidth;
			for ( UIButton* b in self.buttons )
			{
				b.frame = CGRectMake( kAlertView_LeftMargin, y + 5, 74, 29 );
                b.center = CGPointMake(self.bounds.size.width / 2, b.center.y);
				[self addSubview: b];
				y += buttonHeight + kAlertView_RowMargin;
			}
		}
		else
		{
			CGFloat buttonWidth = (maxWidth - kAlertView_ColumnMargin) / 2.0;
			CGFloat x = kAlertView_LeftMargin;
			for ( UIButton* b in self.buttons )
			{
				b.frame = CGRectMake( x, y, buttonWidth, buttonHeight );
				[self addSubview: b];
				x += buttonWidth + kAlertView_ColumnMargin;
			}
		}
		
	}
	
	return CGSizeMake( self.width, totalHeight );
}

@end	


@implementation ActionAlertView

- (CGSize) recalcSizeAndLayout: (BOOL) layout
{    
    CGSize  titleLabelSize = [self titleLabelSize];
    CGSize  messageViewSize = [self messageLabelSize];
    CGSize  buttonsAreaSize = [self buttonsAreaSize_SideBySide];
    
    CGFloat inputRowHeight = 0;
    
    CGFloat totalHeight = kAlertView_TopMargin + titleLabelSize.height + kAlertView_RowMargin + messageViewSize.height + inputRowHeight + kAlertView_RowMargin + buttonsAreaSize.height + kAlertView_BottomMargin;
    
    if ( totalHeight > self.maxHeight )
    {
        // too tall - we'll condense by using a textView (with scrolling) for the message
        
        totalHeight -= messageViewSize.height;
        //$$what if it's still too tall?
        messageViewSize.height = self.maxHeight - totalHeight;
        
        totalHeight = self.maxHeight;
        
        self.usesMessageTextView = YES;
    }
    
    if ( layout )
    {
        // title
        CGFloat y = kAlertView_TopMargin;
        if ( self.title != nil )
        {
            self.titleLabel.frame = CGRectMake( kAlertView_LeftMargin, y, titleLabelSize.width, titleLabelSize.height );
            [self addSubview: self.titleLabel];
            y += titleLabelSize.height + kAlertView_RowMargin;
        }
        
        // message
        if ( self.message != nil )
        {
            if ( self.usesMessageTextView )
            {
                self.messageTextView.frame = CGRectMake( kAlertView_LeftMargin, y, messageViewSize.width, messageViewSize.height );
                [self addSubview: self.messageTextView];
                y += messageViewSize.height + kAlertView_RowMargin;
                
                UIImageView* maskImageView = [self messageTextViewMaskView];
                maskImageView.frame = self.messageTextView.frame;
                [self addSubview: maskImageView];
            }
            else
            {
                self.messageLabel.frame = CGRectMake( kAlertView_LeftMargin, y, messageViewSize.width, messageViewSize.height );
                [self addSubview: self.messageLabel];
                y += messageViewSize.height + kAlertView_RowMargin;
            }
        }
        
        // buttons
        //CGFloat buttonHeight = [[self.buttons objectAtIndex:0] sizeThatFits: CGSizeZero].height;
        
        {
            //CGFloat buttonWidth = (maxWidth - kAlertView_ColumnMargin) / 2.0;
            CGFloat x = 25;
            int i = 0;
            for ( UIButton* b in self.buttons )
            {
                b.frame = CGRectMake( x + i * 130 + x * i, y + 5, 130, 45 );
                [self addSubview: b];
             
                //x += buttonWidth + 70;//kAlertView_ColumnMargin;
                i++;
            }
        }
        
    }
    
    return CGSizeMake( self.width, totalHeight );
}

- (UIImage*) backgroundImage
{
	if ( _backgroundImage == nil )
	{
		self.backgroundImage = [UIImage imageNamed:@"people_alert.png"];
	}
	
	return _backgroundImage;
}

- (UILabel*) titleLabel
{
	if ( _titleLabel == nil )
	{
		_titleLabel = [[UILabel alloc] init];
		_titleLabel.font = [UIFont boldSystemFontOfSize:20];
		_titleLabel.backgroundColor = [UIColor clearColor];
		_titleLabel.textColor = [UIColor whiteColor];
		_titleLabel.textAlignment = NSTextAlignmentCenter;
		_titleLabel.lineBreakMode = NSLineBreakByWordWrapping;
		_titleLabel.numberOfLines = 0;
        _titleLabel.shadowColor = [UIColor darkGrayColor];
        _titleLabel.shadowOffset = CGSizeMake(0, 1);
	}
	
	return _titleLabel;
}

- (NSInteger) addButtonWithTitle: (NSString *) t
{
	UIButton* b = [UIButton buttonWithType: UIButtonTypeCustom];
	[b setTitle: t forState: UIControlStateNormal];
    [b setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    b.titleLabel.font = [UIFont boldSystemFontOfSize:20];
	
	UIImage* buttonBgNormal = [UIImage imageNamed: @"people_btn.png"];
	[b setBackgroundImage: buttonBgNormal forState: UIControlStateNormal];
	[b addTarget: self action: @selector(onButtonPress:) forControlEvents: UIControlEventTouchUpInside];
	
	[self.buttons addObject: b];
	
	[self setNeedsLayout];
	
	return self.buttons.count-1;
}

- (CGFloat) width
{
	return 130 * [self.buttons count] + ([self.buttons count] + 1)* 25;
}

@end

@implementation DeleteAlertView

- (CGSize) recalcSizeAndLayout: (BOOL) layout
{    
    CGSize  titleLabelSize = [self titleLabelSize];
    CGSize  messageViewSize = [self messageLabelSize];
    CGSize  buttonsAreaSize = [self buttonsAreaSize_SideBySide];
    
    CGFloat inputRowHeight = 0;
    
    CGFloat totalHeight = kAlertView_TopMargin + titleLabelSize.height + kAlertView_RowMargin + messageViewSize.height + inputRowHeight + kAlertView_RowMargin + buttonsAreaSize.height + kAlertView_BottomMargin;
    
    if ( totalHeight > self.maxHeight )
    {
        // too tall - we'll condense by using a textView (with scrolling) for the message
        
        totalHeight -= messageViewSize.height;
        //$$what if it's still too tall?
        messageViewSize.height = self.maxHeight - totalHeight;
        
        totalHeight = self.maxHeight;
        
        self.usesMessageTextView = YES;
    }
    
    if ( layout )
    {
        // title
        CGFloat y = kAlertView_TopMargin + 10;
        if ( self.title != nil )
        {
            self.titleLabel.frame = CGRectMake( kAlertView_LeftMargin, y, titleLabelSize.width, titleLabelSize.height );
            [self addSubview: self.titleLabel];
            y += titleLabelSize.height + kAlertView_RowMargin;
        }
        
        // message
        if ( self.message != nil )
        {
            if ( self.usesMessageTextView )
            {
                self.messageTextView.frame = CGRectMake( kAlertView_LeftMargin, y, messageViewSize.width, messageViewSize.height );
                [self addSubview: self.messageTextView];
                y += messageViewSize.height + kAlertView_RowMargin;
                
                UIImageView* maskImageView = [self messageTextViewMaskView];
                maskImageView.frame = self.messageTextView.frame;
                [self addSubview: maskImageView];
            }
            else
            {
                self.messageLabel.frame = CGRectMake( kAlertView_LeftMargin, y, messageViewSize.width, messageViewSize.height );
                [self addSubview: self.messageLabel];
                y += messageViewSize.height + kAlertView_RowMargin;
            }
        }
        
        // buttons
        //CGFloat buttonHeight = [[self.buttons objectAtIndex:0] sizeThatFits: CGSizeZero].height;
        
        {
            //CGFloat buttonWidth = (maxWidth - kAlertView_ColumnMargin) / 2.0;
            CGFloat x = 25;
            
            y += 10;
            
            int i = 0;
            for ( UIButton* b in self.buttons )
            {
                b.frame = CGRectMake( x + i * 130 + x * i, y + 15, 130, 45 );
                [self addSubview: b];
                
                //x += buttonWidth + 70;//kAlertView_ColumnMargin;
                i++;
            }
        }
        
    }
    
    return CGSizeMake( self.width, totalHeight );
}

- (UIImage*) backgroundImage
{
	if ( _backgroundImage == nil )
	{
		self.backgroundImage = [[UIImage imageNamed: @"Common_Alertview_bck"] resizableImageWithCapInsets:UIEdgeInsetsMake(40, 40, 40, 40)];
	}
	
	return _backgroundImage;
}

- (UILabel*) titleLabel
{
	if ( _titleLabel == nil )
	{
		_titleLabel = [[UILabel alloc] init];
		_titleLabel.font = [UIFont boldSystemFontOfSize:28];
		_titleLabel.backgroundColor = [UIColor clearColor];
		_titleLabel.textColor = [UIColor blackColor];
		_titleLabel.textAlignment = NSTextAlignmentCenter;
		_titleLabel.lineBreakMode = NSLineBreakByWordWrapping;
		_titleLabel.numberOfLines = 0;
        _titleLabel.shadowColor = [UIColor clearColor];
        _titleLabel.shadowOffset = CGSizeMake(0, 1);
	}
	
	return _titleLabel;
}

- (NSInteger) addButtonWithTitle: (NSString *) t
{
	UIButton* b = [UIButton buttonWithType: UIButtonTypeCustom];
	[b setTitle: t forState: UIControlStateNormal];
	[b addTarget: self action: @selector(onButtonPress:) forControlEvents: UIControlEventTouchUpInside];
	
    b.titleLabel.font = [UIFont boldSystemFontOfSize:18];
    
	[self.buttons addObject: b];
	
    if ([self.buttons count] == 1)
    {
        [b setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
        [b setBackgroundImage:[[UIImage imageNamed:@"people_alert_btn_h"] resizableImageWithCapInsets:UIEdgeInsetsMake(20, 20, 20, 20)] forState:UIControlStateNormal];
    }
    else
    {
        [b setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
        [b setBackgroundImage:[[UIImage imageNamed:@"people_alert_btn"] resizableImageWithCapInsets:UIEdgeInsetsMake(20, 20, 20, 20)] forState:UIControlStateNormal];
    }
    
	[self setNeedsLayout];
	
	return self.buttons.count-1;
}

- (CGFloat) width
{
	return 130 * [self.buttons count] + ([self.buttons count] + 1)* 25;
}

- (void) show
{
	[[NSRunLoop currentRunLoop] runMode: NSDefaultRunLoopMode beforeDate:[NSDate date]];
	
	AlertViewController* avc = [[[AlertViewController alloc] init] autorelease];
	avc.view.backgroundColor = [UIColor clearColor];
	
	// $important - the window is released only when the user clicks an alert view button
	AlertOverlayWindow* ow = [[AlertOverlayWindow alloc] initWithFrame: [UIScreen mainScreen].bounds];
    ow.useRadialGradient = NO;
	ow.alpha = 0.0;
	ow.backgroundColor = [UIColor colorWithRed:0.f green:0.f blue:0.f alpha:0.6f];
	ow.rootViewController = avc;
	[ow makeKeyAndVisible];
	
	// fade in the window
	[UIView animateWithDuration: 0.2 animations: ^{
		ow.alpha = 1;
	}];
	
	// add and pulse the alertview
	// add the alertview
	[avc.view addSubview: self];
	[self sizeToFit];
    
    self.center = CGPointMake( CGRectGetMidX( avc.view.bounds ), 350 );
    
	self.frame = CGRectIntegral( self.frame );
	[self pulse];
	
	if ( self.style == AlertViewStyleInput )
	{
		[self layoutSubviews];
		[self.inputTextField becomeFirstResponder];
	}
}

@end


@implementation InfoAlertView

- (CGSize) recalcSizeAndLayout: (BOOL) layout
{    
    CGSize  titleLabelSize = [self titleLabelSize];
    CGSize  messageViewSize = [self messageLabelSize];
    CGSize  buttonsAreaSize = [self buttonsAreaSize_SideBySide];
    
    CGFloat inputRowHeight = 0;
    
    CGFloat totalHeight = kAlertView_TopMargin + titleLabelSize.height + kAlertView_RowMargin + messageViewSize.height + inputRowHeight + kAlertView_RowMargin + buttonsAreaSize.height + kAlertView_BottomMargin;
    
    if ( totalHeight > self.maxHeight )
    {
        // too tall - we'll condense by using a textView (with scrolling) for the message
        
        totalHeight -= messageViewSize.height;
        //$$what if it's still too tall?
        messageViewSize.height = self.maxHeight - totalHeight;
        
        totalHeight = self.maxHeight;
        
        self.usesMessageTextView = YES;
    }
    
    if ( layout )
    {
        // title
        CGFloat y = kAlertView_TopMargin;
        if ( self.title != nil )
        {
            self.titleLabel.frame = CGRectMake( kAlertView_LeftMargin, y, titleLabelSize.width, titleLabelSize.height );
            [self addSubview: self.titleLabel];
            y += titleLabelSize.height + kAlertView_RowMargin;
        }
        
        // message
        if ( self.message != nil )
        {
            if ( self.usesMessageTextView )
            {
                self.messageTextView.frame = CGRectMake( kAlertView_LeftMargin, y, messageViewSize.width, messageViewSize.height );
                [self addSubview: self.messageTextView];
                y += messageViewSize.height + kAlertView_RowMargin;
                
                UIImageView* maskImageView = [self messageTextViewMaskView];
                maskImageView.frame = self.messageTextView.frame;
                [self addSubview: maskImageView];
            }
            else
            {
                self.messageLabel.frame = CGRectMake( kAlertView_LeftMargin, y, messageViewSize.width, messageViewSize.height );
                [self addSubview: self.messageLabel];
                y += messageViewSize.height + kAlertView_RowMargin;
            }
        }
        
        // buttons
        //CGFloat buttonHeight = [[self.buttons objectAtIndex:0] sizeThatFits: CGSizeZero].height;
        
        {
            //CGFloat buttonWidth = (maxWidth - kAlertView_ColumnMargin) / 2.0;
            CGFloat x = 75;
            int i = 0;
            for ( UIButton* b in self.buttons )
            {
                b.frame = CGRectMake( x, y + 5, 130, 45 );
                [self addSubview: b];
                
                //x += buttonWidth + 70;//kAlertView_ColumnMargin;
                i++;
            }
        }
        
    }
    
    return CGSizeMake( self.width, totalHeight );
}

- (UIImage*) backgroundImage
{
	if ( _backgroundImage == nil )
	{
		self.backgroundImage = [UIImage imageNamed:@"people_alert.png"];
	}
	
	return _backgroundImage;
}

- (UILabel*) titleLabel
{
	if ( _titleLabel == nil )
	{
		_titleLabel = [[UILabel alloc] init];
		_titleLabel.font = [UIFont boldSystemFontOfSize:25];
		_titleLabel.backgroundColor = [UIColor clearColor];
		_titleLabel.textColor = [UIColor blackColor];
		_titleLabel.textAlignment = NSTextAlignmentCenter;
		_titleLabel.lineBreakMode = NSLineBreakByWordWrapping;
		_titleLabel.numberOfLines = 0;
        _titleLabel.shadowColor = [UIColor darkGrayColor];
        _titleLabel.shadowOffset = CGSizeMake(0, 1);
	}
	
	return _titleLabel;
}

- (NSInteger) addButtonWithTitle: (NSString *) t
{
	UIButton* b = [UIButton buttonWithType: UIButtonTypeCustom];
	[b setTitle: t forState: UIControlStateNormal];
    [b setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    b.titleLabel.font = [UIFont boldSystemFontOfSize:20];
	
	UIImage* buttonBgNormal = [UIImage imageNamed: @"people_btn.png"];
	[b setBackgroundImage: buttonBgNormal forState: UIControlStateNormal];
	[b addTarget: self action: @selector(onButtonPress:) forControlEvents: UIControlEventTouchUpInside];
	
	[self.buttons addObject: b];
	
	[self setNeedsLayout];
	
	return self.buttons.count-1;
}

- (CGFloat) width
{
	return 230 * [self.buttons count] + ([self.buttons count] + 1)* 25;
}

@end


@implementation AlarmAlertView

@synthesize imageContainer;
@synthesize iconImageView;
@synthesize headerImageView;
@synthesize event;

- (IBAction)okButtonTapped
{
    if ( [self.delegate respondsToSelector: @selector(alertView:willDismissWithButtonIndex:)] )
	{
		[self.delegate alertView: self willDismissWithButtonIndex:1];
	}
    
    [self dismissWithClickedButtonIndex: 1  animated: YES];
}

- (IBAction)cancelButtonTapped
{
    if ( [self.delegate respondsToSelector: @selector(alertView:willDismissWithButtonIndex:)] )
	{
		[self.delegate alertView: self willDismissWithButtonIndex:0];
	}
    
    [self dismissWithClickedButtonIndex: 0  animated: YES];
}

- (UILabel*) titleLabel
{
	if ( _titleLabel == nil )
	{
		_titleLabel = [[UILabel alloc] init];
		_titleLabel.font = [UIFont boldSystemFontOfSize:30];
		_titleLabel.backgroundColor = [UIColor clearColor];
		_titleLabel.textColor = [UIColor whiteColor];
		_titleLabel.textAlignment = NSTextAlignmentCenter;
		_titleLabel.lineBreakMode = NSLineBreakByWordWrapping;
		_titleLabel.numberOfLines = 0;
        _titleLabel.shadowColor = [UIColor darkGrayColor];
        _titleLabel.shadowOffset = CGSizeMake(0, 1);
	}
	
	return _titleLabel;
}

- (void)setEvent:(Event *)newEvent
{
    [event release];
    event = [newEvent retain];
    
    NSString *iconName = [[[NSString stringWithFormat:@"%f", [((Event*)event).eventId floatValue]] stringByReplacingOccurrencesOfString:@"." withString:@"_"]stringByAppendingPathExtension:@"png"];
    
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES);
    NSString *dirPath = [paths objectAtIndex:0];
    
    dirPath = [dirPath stringByAppendingPathComponent:iconName];
        
    UIImage *image = [UIImage imageWithContentsOfFile:dirPath];
    
    self.iconImageView.image = image;
}

- (CGSize) sizeThatFits: (CGSize) unused
{
	return self.bounds.size;
}

- (CGSize) recalcSizeAndLayout: (BOOL) layout
{
    return CGSizeMake(0, 0);
}

- (UIImage*) backgroundImage
{
	return nil;
}

- (void) show
{
	[[NSRunLoop currentRunLoop] runMode: NSDefaultRunLoopMode beforeDate:[NSDate date]];
	
	AlertViewController* avc = [[[AlertViewController alloc] init] autorelease];
	avc.view.backgroundColor = [UIColor whiteColor];
	
	// $important - the window is released only when the user clicks an alert view button
	AlertOverlayWindow* ow = [[AlertOverlayWindow alloc] initWithFrame: [UIScreen mainScreen].bounds];
	ow.alpha = 0.0;
	ow.backgroundColor = [UIColor whiteColor];
	ow.rootViewController = avc;
	[ow makeKeyAndVisible];
	
	// fade in the window
	[UIView animateWithDuration: 0.2 animations: ^{
		ow.alpha = 1;
	}];
	
	// add and pulse the alertview
	// add the alertview
	[avc.view addSubview: self];
	[self sizeToFit];
	self.center = CGPointMake( CGRectGetMidX( avc.view.bounds ), CGRectGetMidY( avc.view.bounds ) );;
	self.frame = CGRectIntegral( self.frame );
    
    self.layer.cornerRadius = 0.f;
    self.layer.shadowColor = [UIColor blackColor].CGColor;
    self.layer.shadowOpacity = 0.6f;
    self.layer.shadowOffset = CGSizeMake(3, 3);
    self.layer.shadowRadius = 5.f;
    
	[self pulse];
}

- (NSInteger) addButtonWithTitle: (NSString *) t
{
	UIButton* b = [UIButton buttonWithType: UIButtonTypeCustom];
	[b setTitle: t forState: UIControlStateNormal];
		
	[b addTarget: self action: @selector(onButtonPress:) forControlEvents: UIControlEventTouchUpInside];
	
    b.titleLabel.font = [UIFont boldSystemFontOfSize:30];
    
	[self.buttons addObject: b];
	
    if (!t)
    {
        b.hidden = YES;
    }
    
    if ([self.buttons count] == 1)
    {
        [b setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        [b setBackgroundImage:[UIImage imageNamed:@"alarm_snooze_btn"] forState:UIControlStateNormal];
    }
    else
    {
        [b setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        [b setBackgroundImage:[[UIImage imageNamed:@"alarm_snooze_btn"] resizableImageWithCapInsets:UIEdgeInsetsMake(0, 30, 0, 30)] forState:UIControlStateNormal];
    }
    
	[self setNeedsLayout];
	
	return self.buttons.count-1;
}

- (void)dealloc
{
    self.event = nil;
    [super dealloc];
}

@end


@implementation MedicineAlarmAlertView

- (void)dealloc
{
    self.container = nil;
    [super dealloc];
}

- (UILabel*) titleLabel
{
	if ( _titleLabel == nil )
	{
		_titleLabel = [[UILabel alloc] init];
		_titleLabel.font = [UIFont boldSystemFontOfSize:50];
		_titleLabel.backgroundColor = [UIColor clearColor];
		_titleLabel.textColor = [UIColor blackColor];
		_titleLabel.textAlignment = NSTextAlignmentCenter;
		_titleLabel.lineBreakMode = NSLineBreakByWordWrapping;
		_titleLabel.numberOfLines = 0;
        _titleLabel.shadowColor = [UIColor clearColor];
        _titleLabel.shadowOffset = CGSizeMake(0, 0);
	}
	
	return _titleLabel;
}

- (UILabel*) messageLabel
{
	if ( _messageLabel == nil )
	{
		_messageLabel = [[UILabel alloc] init];
		_messageLabel.font = [UIFont boldSystemFontOfSize: 30];
		_messageLabel.backgroundColor = [UIColor clearColor];
		_messageLabel.textColor = [UIColor colorWithRed:143.f/255.f green:143.f/255.f blue:143.f/255.f alpha:1.f];
		_messageLabel.textAlignment = NSTextAlignmentCenter;
		_messageLabel.lineBreakMode = NSLineBreakByWordWrapping;
		_messageLabel.numberOfLines = 0;
        
        _messageLabel.hidden = NO;
        
        _messageLabel.shadowColor = [UIColor clearColor];
        _messageLabel.shadowOffset = CGSizeMake(0, 0);
	}
	
	return _messageLabel;
}

//- (UIView*) imageContainer
//{
//	return nil;
//}

//- (UIImageView*) iconImageView
//{
//	if (iconImageView == nil )
//	{
//		iconImageView = [[UIImageView alloc] init];
//        
//        iconImageView.backgroundColor = [UIColor whiteColor];
//        iconImageView.clipsToBounds = YES;
//	}
//	
//	return iconImageView;
//}

- (UIImageView*) headerImageView
{
	return nil;
}

- (void)setEvent:(MedicineAlarm *)newEvent
{
    [event release];
    event = [newEvent retain];
    
    if ([newEvent.medicineType integerValue] == 4)
    {
        NSString *iconName = [[[NSString stringWithFormat:@"%f", [((Event*)event).eventId floatValue]] stringByReplacingOccurrencesOfString:@"." withString:@"_"]stringByAppendingPathExtension:@"png"];
        
        NSArray *paths = NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES);
        NSString *dirPath = [paths objectAtIndex:0];
        
        dirPath = [dirPath stringByAppendingPathComponent:iconName];
        
        UIImage *image = [UIImage imageWithContentsOfFile:dirPath];
        
        self.iconImageView.image = image;
        self.iconImageView.contentMode = UIViewContentModeScaleAspectFill;
    }
    else
    {
        self.iconImageView.contentMode = UIViewContentModeCenter;
        
        if ([newEvent.medicineType intValue] == 1)
        {
            self.iconImageView.image = [UIImage imageNamed:@"medicine_popup_pills_icon"];
        }
        else if ([newEvent.medicineType intValue] == 2)
        {
            self.iconImageView.image = [UIImage imageNamed:@"medicine_popup_bottle_icon"];
        }
        else if ([newEvent.medicineType intValue] == 3)
        {
            self.iconImageView.image = [UIImage imageNamed:@"medicine_popup_bag_icon"];
        }
    }
}

- (CGSize) sizeThatFits: (CGSize) unused
{
	return self.bounds.size;
}

- (CGSize) recalcSizeAndLayout: (BOOL) layout
{
    return CGSizeMake(0, 0);
}

- (UIImage*) backgroundImage
{
    return nil;
}

- (CGFloat) width
{
    return 917;
}

- (NSInteger) addButtonWithTitle: (NSString *) t
{
	UIButton* b = [UIButton buttonWithType: UIButtonTypeCustom];
	[b setTitle: t forState: UIControlStateNormal];
    
	[b addTarget: self action: @selector(onButtonPress:) forControlEvents: UIControlEventTouchUpInside];
	
    b.titleLabel.font = [UIFont boldSystemFontOfSize:24];
    
	[self.buttons addObject: b];
	
    if (!t)
    {
        b.hidden = YES;
    }
    
    if ([self.buttons count] == 1)
    {
        [b setTitleColor:[UIColor darkGrayColor] forState:UIControlStateNormal];
        [b setBackgroundImage:[[UIImage imageNamed:@"alarm_edit_btn_ipad"] resizableImageWithCapInsets:UIEdgeInsetsMake(0, 40, 0, 40)] forState:UIControlStateNormal];
    }
    else
    {
        [b setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
        [b setBackgroundImage:[[UIImage imageNamed:@"alarm_edit_btn_ipad"] resizableImageWithCapInsets:UIEdgeInsetsMake(0, 40, 0, 40)] forState:UIControlStateNormal];
    }
    
	[self setNeedsLayout];
	
	return self.buttons.count-1;
}

- (void) show
{
	[[NSRunLoop currentRunLoop] runMode: NSDefaultRunLoopMode beforeDate:[NSDate date]];
	
	AlertViewController* avc = [[[AlertViewController alloc] init] autorelease];
	avc.view.backgroundColor = [UIColor whiteColor];
	
	// $important - the window is released only when the user clicks an alert view button
	AlertOverlayWindow* ow = [[AlertOverlayWindow alloc] initWithFrame: [UIScreen mainScreen].bounds];
	ow.alpha = 0.0;
	ow.backgroundColor = [UIColor whiteColor];
	ow.rootViewController = avc;
	[ow makeKeyAndVisible];
	
	// fade in the window
	[UIView animateWithDuration: 0.2 animations: ^{
		ow.alpha = 1;
	}];
	
	// add and pulse the alertview
	// add the alertview
	[avc.view addSubview: self];
	[self sizeToFit];
	self.center = CGPointMake( CGRectGetMidX( avc.view.bounds ), CGRectGetMidY( avc.view.bounds ) );
	self.frame = CGRectIntegral( self.frame );
	[self pulse];
	
    self.backgroundColor = [UIColor clearColor];
    self.layer.cornerRadius = 250.f;
    self.layer.shadowColor = [UIColor blackColor].CGColor;
    self.layer.shadowOpacity = 0.6f;
    self.layer.shadowOffset = CGSizeMake(3, 3);
    self.layer.shadowRadius = 5.f;
    
//    UIBezierPath *path = [UIBezierPath bezierPathWithRoundedRect:self.bounds cornerRadius:250.f];
//    
//    self.layer.shadowPath = path.CGPath;
    self.layer.masksToBounds = NO;
    self.clipsToBounds = NO;
    
//	if ( self.style == AlertViewStyleInput )
//	{
//		[self layoutSubviews];
//		[self.inputTextField becomeFirstResponder];
//	}
    
    self.container.clipsToBounds = YES;
    self.container.backgroundColor = [UIColor whiteColor];
    self.container.layer.cornerRadius = 250.f;
}

@end
