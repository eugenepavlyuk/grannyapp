//
//  GRCommonAlertView.h
//  GrannyApp
//
//  Created by Ievgen Pavliuk on 8/2/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef enum {
    GRAlertRadioStationDelete = 1,
} AlertAction;

@protocol GRCommonAlertDelegate <NSObject>
@optional
- (void)commonAlert:(id)alertView buttonWithIndex:(NSInteger)index;

@end

@interface GRCommonAlertView : UIView {
    
    AlertAction alertAction;
    id<GRCommonAlertDelegate> delegate;
}
@property (nonatomic, assign) id<GRCommonAlertDelegate> delegate;

- (id)initForMailAccountCreatedErrorWithSize:(CGSize)_size;

- (id)initForTVChannelEditConfirmWithView:(UIView *)view;
- (id)initForTVChannelDeleteConfirmWithView:(UIView *)view;

- (id)initCommonAlert:(NSString *)alertMessage;
- (id)initCommonAlertWithText:(NSString *)alertMessage size:(CGSize)_size;
- (id)initCommonAlert:(NSString *)alertMessage inView:(UIView *) view;
- (id)initCommonAlert:(NSString *)alertMessage inView:(UIView *)view pointerPosition:(CGPoint)pointer;
- (id)initCommonChooseAlert:(NSString *)alertMessage inView:(UIView *) view;
- (id)initContactAlertWhenCallsNotAvailableWithAlert:(NSString *)alertMessage inView:(UIView *) view;
- (id)initNoEmailAlert:(NSString *)alertMessage inView:(UIView *) view;

@end
