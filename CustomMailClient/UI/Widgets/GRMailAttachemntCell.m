//
//  GRMailAttachemntCell.m
//  GrannyApp
//
//  Created by Ievgen Pavliuk on 6/1/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "GRMailAttachemntCell.h"
#import "UIImage+Resize.h"
#import <ImageIO/ImageIO.h>

CGImageRef createThumbnailImageFromData (NSData * data, int imageSize)
{
    CGImageRef        myThumbnailImage = NULL;
    CGImageSourceRef  myImageSource;
    CFDictionaryRef   myOptions = NULL;
    CFStringRef       myKeys[3];
    CFTypeRef         myValues[3];
    CFNumberRef       thumbnailSize;
    
    // Create an image source from NSData; no options.
    myImageSource = CGImageSourceCreateWithData((CFDataRef)data,
                                                NULL);
    // Make sure the image source exists before continuing.
    if (myImageSource == NULL){
        fprintf(stderr, "Image source is NULL.");
        return  NULL;
    }
    
    // Package the integer as a  CFNumber object. Using CFTypes allows you
    // to more easily create the options dictionary later.
    thumbnailSize = CFNumberCreate(NULL, kCFNumberIntType, &imageSize);
    
    // Set up the thumbnail options.
    myKeys[0] = kCGImageSourceCreateThumbnailWithTransform;
    myValues[0] = (CFTypeRef)kCFBooleanTrue;
    myKeys[1] = kCGImageSourceCreateThumbnailFromImageIfAbsent;
    myValues[1] = (CFTypeRef)kCFBooleanTrue;
    myKeys[2] = kCGImageSourceThumbnailMaxPixelSize;
    myValues[2] = (CFTypeRef)thumbnailSize;
    
    myOptions = CFDictionaryCreate(NULL, (const void **) myKeys,
                                   (const void **) myValues, 2,
                                   &kCFTypeDictionaryKeyCallBacks,
                                   & kCFTypeDictionaryValueCallBacks);
    
    // Create the thumbnail image using the specified options.
    myThumbnailImage = CGImageSourceCreateThumbnailAtIndex(myImageSource,
                                                           0,
                                                           myOptions);
    // Release the options dictionary and the image source
    // when you no longer need them.
    CFRelease(thumbnailSize);
    CFRelease(myOptions);
    CFRelease(myImageSource);
    
    // Make sure the thumbnail image exists before continuing.
    if (myThumbnailImage == NULL){
        fprintf(stderr, "Thumbnail image not created from image source.");
        return NULL;
    }
    
    return myThumbnailImage;
}

@implementation GRMailAttachemntCell

#pragma mark - Initialization

- (void)awakeFromNib {
    [super awakeFromNib];
    
    self.icon.layer.borderWidth = 1.0f;
    self.icon.layer.borderColor = [UIColor lightGrayColor].CGColor;
    self.icon.layer.cornerRadius = 15.0f;
    self.icon.layer.masksToBounds = YES;
}

- (void)updateForItem:(NSDictionary *)item {
    [self updateContentWithItem:item];
}

- (void)updateContentWithItem:(NSDictionary *)item {
    self.fileNameLabel.text = [item objectForKey:FileNameKey];
    
    NSString *filePath = [item objectForKey:PathKey];
    NSString *mimeType = [item objectForKey:MimeTypeKey];
    
    UIImage *im = nil;
    if ([mimeType rangeOfString:@"image" options:NSCaseInsensitiveSearch].location != NSNotFound)
    {
        NSData *data = [NSData dataWithContentsOfFile:filePath];
        im = [UIImage imageWithData:data];
        im = [im resizedImageWithContentMode:UIViewContentModeScaleAspectFit bounds:self.icon.bounds.size interpolationQuality:0];
    }
    else if ([mimeType rangeOfString:@"pdf" options:NSCaseInsensitiveSearch].location != NSNotFound)
    {
        CGImageRef imRef = createThumbnailImageFromData([NSData dataWithContentsOfFile:filePath], (int)self.icon.bounds.size.width);
        im = [UIImage imageWithCGImage:imRef scale:[UIScreen mainScreen].scale orientation:0];
    }
    
    if (im) {
        self.icon.backgroundColor = [UIColor whiteColor];
        self.icon.image = im;
    }
    else {
        self.icon.image = nil;
        self.icon.backgroundColor = [UIColor yellowColor];
    }
}

- (void)prepareForReuse
{
    [super prepareForReuse];
}

 - (void)dealloc {
     [_fileNameLabel release];
     _delegate = nil;
     [super dealloc];
}


#pragma mark - Helpers

- (NSString *)getFileIconName:(NSString *)contentType
{
    NSString *iconName = @"bmp.png";
    if ([contentType rangeOfString:@"image"].location != NSNotFound)
    {
        iconName = @"jpeg.png";
    }
    if ([contentType rangeOfString:@"officedocument"].location != NSNotFound) {
        iconName = @"docx mac.png";
    }
    if ([contentType rangeOfString:@"pdf"].location != NSNotFound) {
        iconName = @"pdf.png";
    }
    
    
    return iconName;
}

@end
