//
//  GRGridCell.m
//
//  Created by Eugene Pavlyuk on 4/20/11.
//  Copyright 2012 Mothermobile. 
//

#import <QuartzCore/QuartzCore.h>
#import "GRGridCell.h"


@interface GRGridCell ()

@property (nonatomic, retain) UIView *contentView;

- (void)repositionImageAndLabel;

@end

@implementation GRGridCell

@synthesize imageView = imageView_;
@synthesize label = label_;
@synthesize index;
@synthesize contentView;
@synthesize selected;
@synthesize deleteButton;
@synthesize webView;
@synthesize spinner;

- (id)initCellWithRect:(CGRect)rect
{
    self = [super initWithFrame:rect];
    
    if (self) 
    {
        UIImageView *bkg = [[[UIImageView alloc] initWithImage:[UIImage imageNamed:@"inet_gridcell_bkg"]] autorelease];
        [self addSubview:bkg];
        
        self.contentView = [[[UIView alloc] initWithFrame:CGRectMake(0, 0, 300, 190)] autorelease];
        self.contentView.center = self.center;
        self.contentView.backgroundColor = [UIColor whiteColor];
        self.contentView.layer.cornerRadius = 5.f;
        self.contentView.clipsToBounds = YES;
        [self addSubview:self.contentView];
        
        self.imageView = [[[UIImageView alloc] initWithFrame:rect] autorelease];
        self.imageView.clipsToBounds = YES;
        self.imageView.contentMode = UIViewContentModeScaleAspectFill;
        [contentView addSubview:self.imageView];
        
        
        self.webView = [[[UIWebView alloc] initWithFrame:rect] autorelease];
        self.webView.clipsToBounds = NO;
        self.webView.userInteractionEnabled = NO;
        self.webView.scalesPageToFit = YES;
        self.webView.delegate = self;
        [contentView addSubview:self.webView];
        
        self.deleteButton = [UIButton buttonWithType:UIButtonTypeCustom];
        [self.deleteButton setBackgroundImage:[UIImage imageNamed:@"inet_favcell_delete_btn_bkg"] forState:UIControlStateNormal];
        [self.deleteButton sizeToFit];
        CGRect delFrame = self.deleteButton.frame;
        delFrame.origin = CGPointMake(CGRectGetWidth(self.bounds) - delFrame.size.width, 0);
        self.deleteButton.frame = delFrame;
        [self.deleteButton addTarget:self action:@selector(deleteButtonTapped) forControlEvents:UIControlEventTouchUpInside];
        
        [self addSubview:self.deleteButton];
        
        self.spinner = [[[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray] autorelease];
        self.spinner.hidesWhenStopped = YES;
        [self.contentView addSubview:self.spinner];
    }
    
    return self;
}

- (void)dealloc 
{  
    [imageView_ release];
    [label_ release];

    self.spinner = nil;
    self.webView = nil;
    
    self.deleteButton = nil;
    
    [super dealloc];
}

- (void)layoutSubviews 
{
    [super layoutSubviews];
    
    [self repositionImageAndLabel];
}

- (void)repositionImageAndLabel 
{
    self.contentView.frame = CGRectInset(self.bounds, 20, 20);
    self.imageView.frame = self.contentView.bounds;
    self.webView.frame = CGRectMake(0, 0, self.parentSize.width, self.parentSize.height);
    
    self.deleteButton.frame = CGRectMake(self.bounds.size.width - self.deleteButton.frame.size.width + 10, -10, self.deleteButton.frame.size.width, self.deleteButton.frame.size.height);
    
        self.spinner.center = CGPointMake(self.contentView.bounds.size.width / 2.f, self.contentView.bounds.size.height / 2.f);
}

#pragma mark -
#pragma mark Properties

- (void)setSelected:(BOOL)bSelected 
{  
    selected = bSelected;
    
    if (selected) 
    {
        self.imageView.image = [UIImage imageNamed:@"day_selected_ipad"];      
    } 
    else 
    {
        self.imageView.image = [UIImage imageNamed:@"day_unselected_ipad"];
    }
}

- (void)deleteButtonTapped
{
    [[NSNotificationCenter defaultCenter] postNotificationName:@"CancelButtonTappedNotification" object:self];
}

- (void)startAnimating
{
    CABasicAnimation* anim = [CABasicAnimation animationWithKeyPath:@"transform.rotation"];

    [anim setToValue:[NSNumber numberWithFloat:-M_PI/60]];

    [anim setFromValue:[NSNumber numberWithDouble:M_PI/60]]; // rotation angle

    [anim setDuration:0.1];

    [anim setRepeatCount:NSUIntegerMax];

    [anim setAutoreverses:YES];

    [self.layer addAnimation:anim forKey:@"iconShake"];

//    CABasicAnimation* anim = [CABasicAnimation animationWithKeyPath:@"transform.rotation"];
//
//    [anim setToValue:[NSNumber numberWithFloat:0.0f]];
//
//    [anim setFromValue:[NSNumber numberWithDouble:M_PI/30]]; // rotation angle
//
//    [anim setDuration:0.1];
//
//    [anim setRepeatCount:NSUIntegerMax];
//
//    [anim setAutoreverses:YES];
//    
//    [self.loginButton.layer addAnimation:anim forKey:@"iconShake"];
}

- (void)stopAnimating
{
    [self.layer removeAllAnimations];
}

- (BOOL)webView:(UIWebView *)webView shouldStartLoadWithRequest:(NSURLRequest *)request navigationType:(UIWebViewNavigationType)navigationType
{
    return YES;
}

- (void)webViewDidStartLoad:(UIWebView *)webView
{
    [self.spinner startAnimating];
}

- (void)webViewDidFinishLoad:(UIWebView *)webView
{    
    if ([UIScreen instancesRespondToSelector:@selector(scale)] && [[UIScreen mainScreen] scale] == 2.0f)
    {
        UIGraphicsBeginImageContextWithOptions(self.parentSize, NO, 2.0f);
    }
    else
    {
        UIGraphicsBeginImageContext(self.parentSize);
    }
    
    // Render the web view
	[self.webView.layer renderInContext:UIGraphicsGetCurrentContext()];
	
    // Get the image
    UIImage *image = [UIGraphicsGetImageFromCurrentImageContext() retain];
    
    // End the graphics context
	UIGraphicsEndImageContext();
    
    if (self.successBlock)
    {
        // Send the message.
        self.successBlock(image);
    }
    
    [image release];
    
    self.successBlock = nil;
    
    [self.spinner stopAnimating];
}

- (void)webView:(UIWebView *)webView didFailLoadWithError:(NSError *)error
{
    [self.spinner stopAnimating];
    
    if (self.failureBlock)
    {
        self.failureBlock();
    }
    
    self.failureBlock = nil;
}

- (void)createScreenshotFromUrl:(NSURL*)url
                 competionBlock:(screenshotCreated)completion
                   failureBlock:(screenshotFailed)failure
{
    self.successBlock = completion;
    self.failureBlock = failure;
    
    [self.webView loadRequest:[NSURLRequest requestWithURL:url]];
}

@end
