//
//  GRGridCell.h
//
//  This is the cell that goes into a BDGridViewController. 
//
//  Created by Eugene Pavlyuk on 4/20/11.
//  Copyright 2011 Mothermobile. 

#import <UIKit/UIKit.h>

@interface BDGridCell : UIView 
{
    BOOL selected;
}

@property (nonatomic, retain) UIImageView *imageView;
@property (nonatomic, retain) UILabel *label;
@property (nonatomic, retain) UILabel *dateLabel;
@property (nonatomic, retain) UIView *labelView;
@property (nonatomic, retain) UIImageView *checkmark;


@property (nonatomic, retain) UIImage *image;
@property (nonatomic, copy) NSString *caption;
@property (nonatomic, assign) NSUInteger index;
@property (nonatomic, assign) UIEdgeInsets contentInset;
@property (nonatomic, assign, getter=isSelected) BOOL selected;

- (id)initCell;

@end


@interface ContactCell: BDGridCell

@property (nonatomic, retain) UIView *backView;

@end 


@interface PhotoCell: BDGridCell

@property (nonatomic, retain) UIImageView *photoImageView;

- (id)initCellWithRoundedCorners;

@end