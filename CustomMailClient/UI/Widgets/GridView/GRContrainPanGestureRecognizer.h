//
//  GRContrainPanGestureRecognizer.h
//
//  Created by Brian Dewey on 4/26/11.
//  Copyright 2011 Brian Dewey. 

#import <Foundation/Foundation.h>
#import <UIKit/UIGestureRecognizerSubclass.h>

@interface GRContrainPanGestureRecognizer : UIGestureRecognizer 
{
    
}

@property (nonatomic, assign) BOOL enforceConstraints;
@property (nonatomic, assign) CGFloat dx;
@property (nonatomic, assign) CGFloat dy;
@property (nonatomic, assign) CGPoint translation;
@property (nonatomic, assign) CGPoint initialTouchPoint;
@property (nonatomic, assign) CGPoint currentTouchPoint;

@end
