//
//  GRContrainPanGestureRecognizer.m
//
//  Created by Brian Dewey on 4/26/11.
//  Copyright 2011 Brian Dewey. 

#import "GRContrainPanGestureRecognizer.h"

@implementation GRContrainPanGestureRecognizer

@synthesize enforceConstraints = enforceConstraints_;
@synthesize dx = dx_;
@synthesize dy = dy_;
@synthesize translation = translation_;
@synthesize initialTouchPoint = initialTouchPoint_;
@synthesize currentTouchPoint = currentTouchPoint_;

- (id)initWithTarget:(id)target action:(SEL)action 
{  
    self = [super initWithTarget:target action:action];
    
    if (self != nil) 
    {
        self.enforceConstraints = YES;
    }
    
    return self;
}

- (void)dealloc 
{
    [super dealloc];
}

#pragma mark -
#pragma mark Touch

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event 
{  
    [super touchesBegan:touches withEvent:event];
    
    if ([self numberOfTouches] != 1) 
    {
        //
        //  This gesture starts with a single touch.
        //

        self.state = UIGestureRecognizerStateFailed;
        return;
    }
    
    self.initialTouchPoint = [[touches anyObject] locationInView:self.view];
}

- (void)touchesMoved:(NSSet *)touches withEvent:(UIEvent *)event 
{  
    [super touchesMoved:touches withEvent:event];
    
    if ([self numberOfTouches] != 1) 
    {
        self.state = UIGestureRecognizerStateFailed;
    }
    
    if (self.state == UIGestureRecognizerStateFailed) 
    {
        return;
    }

    self.currentTouchPoint = [[touches anyObject] locationInView:self.view];

    //
    //  Compute the translation from |initialPoint| to |currentPoint|.
    //

    translation_.x = currentTouchPoint_.x - initialTouchPoint_.x;
    translation_.y = currentTouchPoint_.y - initialTouchPoint_.y;

    //
    //  If we're still in the |UIGestureRecognizerStatePossible| state, then look
    //  at the translation to see if we've moved far enough to qualify as
    //  beginning.
    //

    if (self.state == UIGestureRecognizerStatePossible) 
    {
        //
        //  Note that if enforceConstraints is NO, then we will always fall into
        //  UIGestureRecognizerStateBegan, no matter how far or in what direction
        //  we move.
        //

        if (!self.enforceConstraints ||
            ((ABS(translation_.x) >= dx_) && (ABS(translation_.y) < dy_))) 
        {          
            //
            //  We are now beginning the gesture.
            //

            self.state = UIGestureRecognizerStateBegan;
        } 
        else if (ABS(translation_.y) >= dy_) 
        {
          //
          //  We moved too far vertically. Fail.
          //
          
          self.state = UIGestureRecognizerStateFailed;
        }

    } 
    else 
    {
        self.state = UIGestureRecognizerStateChanged;
    }
}

- (void)touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event 
{  
    [super touchesEnded:touches withEvent:event];
    
    if ((self.state == UIGestureRecognizerStateBegan) ||
      (self.state == UIGestureRecognizerStateChanged)) 
    {
        self.state = UIGestureRecognizerStateEnded;
    }
}

@end
