//
//  GRGridView.h
//
//
//  Created by Eugene Pavlyuk on 4/21/11.
//  Copyright 2011 Brian Dewey. 

#import <UIKit/UIKit.h>

@class GRGridCell;

@protocol GRGridViewDataSource;
@protocol GRGridViewDelegate;


@interface GRGridView : UIScrollView<UIGestureRecognizerDelegate> 
{
    
}

@property (nonatomic, assign) CGFloat minimumPadding;
@property (nonatomic, assign, readonly) CGFloat padding;
@property (nonatomic, assign) CGFloat topContentPadding;
@property (nonatomic, retain) UIView *headerView;
@property (nonatomic, readonly) NSUInteger cellsPerRow;
@property (nonatomic, readonly) NSUInteger countOfRows;
@property (nonatomic, assign) NSUInteger dropCapWidth;
@property (nonatomic, assign) NSUInteger dropCapHeight;
@property (nonatomic, assign) IBOutlet id<GRGridViewDataSource> dataSource;
@property (nonatomic, assign) IBOutlet id<GRGridViewDelegate> gridViewDelegate;
@property (nonatomic, readonly) NSInteger firstVisibleIndex;
@property (nonatomic, readonly) NSInteger lastVisibleIndex;
@property (nonatomic, retain) NSMutableSet *selectedCells;

- (NSUInteger)cellIndexForGridIndex:(NSUInteger)gridIndex;
- (NSUInteger)gridIndexForCellIndex:(NSUInteger)cellIndex;
- (NSUInteger)countOfGridRowsGivenCellsPerRow:(NSUInteger)cellsPerRow;
- (void)reloadData;
- (CGRect)frameForCellAtIndex:(NSUInteger)index;
- (GRGridCell *)insertCellAtIndex:(NSUInteger)index;
- (void)deleteCellAtIndex:(NSUInteger)index;
- (GRGridCell *)dequeueCell;
- (void)setActiveGap:(NSUInteger)activeGap animated:(BOOL)animated;
- (void)toggleCellSelection:(GRGridCell *)cell;
- (void)unselectAllCells;

@end

@protocol GRGridViewDataSource <NSObject>

- (NSUInteger)gridViewCountOfCells:(GRGridView *)gridView;
- (CGSize)gridViewSizeOfCell:(GRGridView *)gridView;
- (GRGridCell *)gridView:(GRGridView *)gridView cellForIndex:(NSUInteger)index;

@end

@protocol GRGridViewDelegate <NSObject>

@optional

- (BOOL)gridViewShouldEdit:(GRGridView *)gridView;
- (void)gridView:(GRGridView *)gridView didTapCell:(GRGridCell *)cell;
- (void)gridView:(GRGridView *)gridView didLongTapCell:(GRGridCell *)cell;
- (void)gridView:(GRGridView *)gridView didInsertAtPoint:(NSUInteger)insertionPoint fromRect:(CGRect)rect;
- (void)gridView:(GRGridView *)gridView didInsertIntoCell:(GRGridCell *)cell;
- (void)gridView:(GRGridView *)gridView didMoveItemFromIndex:(NSUInteger)initialIndex toIndex:(NSUInteger)finalIndex;
- (void)gridView:(GRGridView *)gridView didCut:(NSSet *)indexes;
- (void)gridView:(GRGridView *)gridView didCopy:(NSSet *)indexes;
- (BOOL)gridViewCanPaste:(GRGridView *)gridView;
- (void)gridView:(GRGridView *)gridView didPasteAtPoint:(NSUInteger)insertionPoint;
- (void)gridView:(GRGridView *)gridView didPasteIntoCell:(GRGridCell *)cell;
- (void)gridView:(GRGridView *)gridView didDelete:(NSSet *)indexes;
- (void)addNewEntity;
- (void)pasteNewEntity;
- (BOOL)isSupportAdd;
- (BOOL)isSupportPaste;
- (BOOL)isSupportShare;
- (BOOL)isSupportCopy;
- (BOOL)isSupportMove;
- (BOOL)isSupportFavorite;
- (BOOL)isSupportRename;

@end
