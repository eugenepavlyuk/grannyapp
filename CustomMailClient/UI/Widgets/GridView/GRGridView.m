//
//  GRGridView.m
//
//  Created by Eugene Pavlyuk on 4/21/11.
//  Copyright 2011 Brian Dewey. 

#import "GRGridCell.h"
#import "GRGridView.h"
#import "GRContrainPanGestureRecognizer.h"

#define kEditModeFrameInset       ((CGFloat)13.0)
#define kAnimationDuration        (0.1)
#define kMinimumPadding         (1)

#define kMoveCellMinX             (25)
#define kMoveCellMaxY             (10)

@interface GRGridView ()

@property (nonatomic, retain) NSMutableSet *viewCells;
@property (nonatomic, retain) NSMutableSet *recycledCells;
@property (nonatomic, assign) NSUInteger insertionPoint;
@property (nonatomic, assign) NSUInteger activeGap;
@property (nonatomic, retain) GRGridCell *pannedCell;
@property (nonatomic, assign) CGRect editMenuTarget;
@property (nonatomic, retain) GRContrainPanGestureRecognizer *cellPanRecognizer;

- (void)setup;
- (CGFloat)topPaddingIncludingHeader;
- (void)recomputeContentSize;
- (NSUInteger)indexForPoint:(CGPoint)point;
- (BOOL)isCellVisible:(NSUInteger)index;
- (void)configureCell:(GRGridCell *)cell;
- (void)selectCell:(GRGridCell *)cell;
- (void)unselectCell:(GRGridCell *)cell;
- (void)adjustAllVisibleCellFrames;
- (void)tileCells;
- (void)handleTap:(UITapGestureRecognizer *)tapGesture;
- (void)handleLongTap:(UILongPressGestureRecognizer *)gestureRecognizer;
- (void)handleCellPan:(GRContrainPanGestureRecognizer *)panGesture;
- (void)addNew;
- (CGSize)sizeForIndex:(NSUInteger)index;

@end


@implementation GRGridView

@synthesize viewCells = viewCells_;
@synthesize recycledCells = recycledCells_;
@synthesize selectedCells = selectedCells_;
@synthesize insertionPoint = insertionPoint_;
@synthesize activeGap = activeGap_;
@synthesize pannedCell = pannedCell_;
@synthesize padding = padding_;
@synthesize editMenuTarget = editMenuTarget_;
@synthesize cellPanRecognizer = cellPanRecognizer_;
@synthesize dataSource = dataSource_;
@synthesize gridViewDelegate = gridViewDelegate_;
@synthesize minimumPadding = minimumPadding_;
@synthesize topContentPadding = topContentPadding_;
@synthesize headerView = headerView_;
@synthesize cellsPerRow = cellsPerRow_;
@synthesize countOfRows = countOfRows_;
@synthesize dropCapWidth = dropCapWidth_;
@synthesize dropCapHeight = dropCapHeight_;
@synthesize firstVisibleIndex = firstVisibleIndex_;
@synthesize lastVisibleIndex = lastVisibleIndex_;


- (id)initWithFrame:(CGRect)frame 
{
    self = [super initWithFrame:frame];

    if (self != nil) 
    {
        [self setup];
    }
    
    return self;
}

- (id)initWithCoder:(NSCoder *)aDecoder 
{
    self = [super initWithCoder:aDecoder];
    
    if (self != nil) 
    {
        [self setup];
    }
    
    return self;
}

- (void)dealloc 
{  
    [headerView_ release];
    [viewCells_ release];
    [recycledCells_ release];
    [selectedCells_ release];
    [pannedCell_ release];
    [cellPanRecognizer_ release];
    [super dealloc];
}

- (void)setup 
{  
    viewCells_ = [[NSMutableSet alloc] initWithCapacity:5];
    recycledCells_ = [[NSMutableSet alloc] initWithCapacity:5];
    selectedCells_ = [[NSMutableSet alloc] initWithCapacity:1];
    activeGap_ = NSNotFound;
    insertionPoint_ = NSNotFound;
    minimumPadding_ = kMinimumPadding;
    
    dropCapWidth_ = dropCapHeight_ = 1;

    UITapGestureRecognizer *tap = [[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleTap:)] autorelease];
    tap.cancelsTouchesInView = NO;
    [self addGestureRecognizer:tap];

    UILongPressGestureRecognizer *longPress = [[[UILongPressGestureRecognizer alloc] 
                                              initWithTarget:self 
                                              action:@selector(handleLongTap:)] autorelease];
    longPress.delegate = self;
    [self addGestureRecognizer:longPress];

//    self.cellPanRecognizer = [[[GRContrainPanGestureRecognizer alloc]
//                             initWithTarget:self 
//                             action:@selector(handleCellPan:)] autorelease];
//    self.cellPanRecognizer.delegate = self;
//    self.cellPanRecognizer.dx = kMoveCellMinX;
//    self.cellPanRecognizer.dy = kMoveCellMaxY;
//    [self addGestureRecognizer:self.cellPanRecognizer];
}

#pragma mark -
#pragma mark Properties

- (void)validateViewCellsAndActiveGap 
{  
//    for (GRGridCell *cell in self.viewCells) 
//    {
//
//    }
}

- (void)setActiveGap:(NSUInteger)desiredActiveGap animated:(BOOL)animated 
{  
    [self validateViewCellsAndActiveGap];
    
    if (activeGap_ != desiredActiveGap) 
    {
        if (desiredActiveGap < activeGap_) 
        {
            for (GRGridCell *cell in self.viewCells) 
            {
                if (cell.index >= desiredActiveGap && cell.index < activeGap_) 
                {
                    cell.index++;
                }
            }          
        } 
        else 
        {
            for (GRGridCell *cell in self.viewCells) 
            {
                if (cell.index > activeGap_ && cell.index <= desiredActiveGap) 
                {
                    cell.index--;
                }
            }
        }
        
        activeGap_ = desiredActiveGap;
        [self validateViewCellsAndActiveGap];
        
        if (animated) 
        {
              [UIView animateWithDuration:0.2 
                                    delay:0 
                                  options:UIViewAnimationOptionBeginFromCurrentState | UIViewAnimationOptionAllowUserInteraction 
                               animations:
               ^(void) {
                
                [self adjustAllVisibleCellFrames];
              } completion:nil];
      
        } 
        else 
        {
            [self adjustAllVisibleCellFrames];
        }
    }
}

- (void)setPannedCell:(GRGridCell *)pannedCell 
{  
    if (self.pannedCell != nil) 
    {
        self.pannedCell.index = self.activeGap;
        [self.viewCells addObject:self.pannedCell];
    }
    
    [pannedCell_ autorelease];
    pannedCell_ = [pannedCell retain];
    
    if (self.pannedCell != nil) 
    {
        [self.viewCells removeObject:self.pannedCell];
        activeGap_ = self.pannedCell.index;
    } 
    else 
    {
        activeGap_ = NSNotFound;
    }
}

- (void)setHeaderView:(UIView *)headerView 
{  
    if (headerView == headerView_) 
    {
        return;
    }
    
    if (headerView_ != nil) 
    {
        [headerView_ removeFromSuperview];
    }
    
    [headerView_ release];
    headerView_ = [headerView retain];
    
    if (headerView_ != nil) 
    {
        CGSize frameSize = headerView_.frame.size;
        headerView_.frame = CGRectMake(0, topContentPadding_, frameSize.width, frameSize.height);
        [self addSubview:headerView_];
    }
}

- (void)setTopContentPadding:(CGFloat)topContentPadding 
{  
    topContentPadding_ = topContentPadding;
    
    if (headerView_ != nil) 
    {
        CGSize frameSize = headerView_.frame.size;
        headerView_.frame = CGRectMake(0, topContentPadding_, frameSize.width, frameSize.height);
    }
}

#pragma mark - Logical / Grid Index translation

- (NSUInteger)cellIndexForGridIndex:(NSUInteger)gridIndex 
{  
    NSUInteger row = gridIndex / self.cellsPerRow;
    NSUInteger column = gridIndex % self.cellsPerRow;

    if ((column < self.dropCapWidth) && (row < self.dropCapHeight)) 
    {
        //
        //  If both the row & column index are less than the drop cap dimensions,
        //  then we're looking at the first cell.
        //

        return 0;
    }

    if (row < self.dropCapHeight) 
    {
        NSUInteger truncatedCellsPerRow = self.cellsPerRow - self.dropCapWidth;
        return 1 + (row * truncatedCellsPerRow) + (column - self.dropCapWidth);
    }

    //
    //  In this case, we're in the "full" portion of the grid. Each row gets a
    //  full row of new cells.
    //

    NSUInteger firstFullIndex = 1 + (self.cellsPerRow - self.dropCapWidth) * self.dropCapHeight;
    return firstFullIndex + ((row - self.dropCapHeight) * self.cellsPerRow) + column;
}

- (NSUInteger)gridIndexForCellIndex:(NSUInteger)cellIndex 
{  
    if (cellIndex == 0) 
    {
        //
        //  This is the same no matter what drop cap we're using.
        //

        return 0;
    }
  
    //
    //  Compute the first index in the "full" region of the grid.
    //  This is the region below the drop cap, that gets a full complement of 
    //  cells in each row.
    //
  
    NSUInteger row, column;
    NSUInteger firstFullRegionIndex = 1 + (self.cellsPerRow - self.dropCapWidth) * self.dropCapHeight;
    
    if (cellIndex < firstFullRegionIndex) 
    {
        //
        //  We're in the truncated region. Compute the corresponding grid index.
        //

        NSUInteger cellsPerTruncatedRow = self.cellsPerRow - self.dropCapWidth;
        row = (cellIndex - 1) / cellsPerTruncatedRow;
        column = self.dropCapWidth + ((cellIndex - 1) % cellsPerTruncatedRow);
    } 
    else 
    {
        //
        //  We're in the full region. 
        //

        NSUInteger indexIntoFullRegion = cellIndex - firstFullRegionIndex;
        row = self.dropCapHeight + indexIntoFullRegion / self.cellsPerRow;
        column = indexIntoFullRegion % self.cellsPerRow;
    }

    return row * self.cellsPerRow + column;
}

////////////////////////////////////////////////////////////////////////////////

- (NSUInteger)countOfGridRowsGivenCellsPerRow:(NSUInteger)cellsPerRow 
{  
    CGFloat numberOfCells = [self.dataSource gridViewCountOfCells:self];

    //
    //  Simple adjustment for drop cap.
    //

    numberOfCells += (dropCapWidth_ * dropCapHeight_) - 1;
    return MAX(1, ceil(numberOfCells / cellsPerRow));
}

#pragma mark -
#pragma mark Cell management

- (CGFloat)topPaddingIncludingHeader 
{  
    CGFloat topPadding = self.topContentPadding;
    
    if (self.headerView != nil) 
    {
        topPadding += self.headerView.frame.size.height;
    }
    
    return topPadding;
}

- (void)recomputeContentSize 
{
    CGSize cellSize = [self.dataSource gridViewSizeOfCell:self];
    CGFloat width = MAX(cellSize.width, CGRectGetWidth(self.frame));

    //
    //  These are floats so we don't do integer arithmetic in the computation
    //  for |totalRows|.
    //

    CGFloat cellsPerRow = MAX(1, floor(width / cellSize.width));
    cellsPerRow_ = round(cellsPerRow);
    countOfRows_ = [self countOfGridRowsGivenCellsPerRow:cellsPerRow_];

    //
    //  Adjust |cellsPerRow| until |padding| is at least |kMinimumPadding|.
    //

    do 
    {
        //
        //  Compute the "slop" in the layout. This is horizontal space left after
        //  placing |self.cellsPerRow| cells in a row. We want to distribute that 
        //  slop evenly between each column... this is |padding|.
        //

        CGFloat slop = CGRectGetWidth(self.bounds) - self.cellsPerRow * cellSize.width;
        padding_ = MAX(0, slop / (self.cellsPerRow + 1));
        
        if (padding_ < self.minimumPadding) 
        {
            cellsPerRow_--;
            countOfRows_ = [self countOfGridRowsGivenCellsPerRow:cellsPerRow_];
        }
        
    } while (padding_ < self.minimumPadding);

    self.contentSize = CGSizeMake(width, 
                                countOfRows_ * (cellSize.height + self.padding) + [self topPaddingIncludingHeader]);

    //
    //  If the content size height is greater than the bounds height, then the
    //  user will need to scroll vertically to see everything. In that case, we
    //  must enforce that the cellPanRecognizer start only with a horizontal
    //  motion. Otherwise, the cellPanRecognizer is free to recognize any panning
    //  gesture.
    //

    self.cellPanRecognizer.enforceConstraints = self.contentSize.height > self.bounds.size.height;
}

- (NSUInteger)indexForPoint:(CGPoint)point 
{  
    CGSize cellSize = [self.dataSource gridViewSizeOfCell:self];

    CGFloat pointRow = floor((point.y - [self topPaddingIncludingHeader]) / (cellSize.height + self.padding));
    CGFloat pointColumn = floor(point.x / (cellSize.width + self.padding));

    return [self cellIndexForGridIndex:round(pointRow * self.cellsPerRow + pointColumn)];
}

- (BOOL)isCellVisible:(NSUInteger)index 
{  
    BOOL cellFound = NO;

    if (self.pannedCell != nil && self.pannedCell.index == index) 
    {
        return YES;
    }
    
    if (index == self.activeGap) 
    {
        return YES;
    }
    
    for (GRGridCell *cell in viewCells_) 
    {
        if (cell.index == index) 
        {
            cellFound = YES;
            break;
        }
    }
    
    return cellFound;
}

- (CGSize)sizeForIndex:(NSUInteger)index 
{
    CGSize cellSize = [self.dataSource gridViewSizeOfCell:self];

    if (index == 0) 
    {
        //
        //  This is the "drop cap" index. Adjust the cell size appropriately.
        //

        cellSize.width = self.dropCapWidth * (cellSize.width + self.padding) - self.padding;
        cellSize.height = self.dropCapHeight * (cellSize.height + self.padding) - self.padding;
    }
    
    return cellSize;
}

- (CGRect)frameForCellAtIndex:(NSUInteger)index 
{
    index = [self gridIndexForCellIndex:index];
    NSUInteger targetRow = index / self.cellsPerRow;
    NSUInteger targetColumn = index % self.cellsPerRow;
    CGSize cellSize = [self sizeForIndex:index];

    //
    //  Compute the origin of this cell. Note there is |self.padding| pixels 
    //  of padding between each column, including before the first and after the
    //  last.
    //

    CGPoint cellOrigin = CGPointMake(targetColumn * (cellSize.width + self.padding) + self.padding, 
                                   targetRow * (cellSize.height + self.padding) + self.padding + [self topPaddingIncludingHeader]);
    CGRect frame = CGRectMake(cellOrigin.x,
                            cellOrigin.y,
                            cellSize.width, 
                            cellSize.height);
    return frame;
}

- (void)configureCell:(GRGridCell *)cell 
{  
    CGRect newFrame = [self frameForCellAtIndex:cell.index];
    CGRect oldFrame = cell.frame;
    cell.frame = [self frameForCellAtIndex:cell.index];
    
    if (newFrame.size.width != oldFrame.size.width) 
    {
        [cell setNeedsDisplay];
    }
}

- (void)selectCell:(GRGridCell *)cell 
{  
    self.insertionPoint = cell.index;

    //
    //  Right now we only do single selection.
    //

    [self unselectAllCells];
    [selectedCells_ addObject:cell];
    cell.selected = YES;
}

- (void)unselectCell:(GRGridCell *)cell 
{  
    [selectedCells_ removeObject:cell];
    cell.selected = NO;
}

- (void)unselectAllCells 
{  
    for (GRGridCell *cell in selectedCells_) 
    {
        cell.selected = NO;
    }
    
    [selectedCells_ removeAllObjects];
}

- (void)toggleCellSelection:(GRGridCell *)cell 
{  
    if ([cell isSelected]) 
    {
        [self unselectCell:cell];
    } 
    else 
    {
        [self selectCell:cell];
    }
}

- (void)adjustAllVisibleCellFrames 
{
    if (headerView_ != nil) 
    {
        //
        //  The width of the header view will get bounded by |padding| on both
        //  sides.
        //

        CGRect frame = headerView_.frame;
        frame.origin.x = self.padding;
        frame.size.width = self.bounds.size.width - 2 * self.padding;
        headerView_.frame = frame;
    }
    
    for (GRGridCell *cell in self.viewCells) 
    {
        [self configureCell:cell];
    }
}

- (void)layoutSubviews 
{
    [self recomputeContentSize];
    [self tileCells];
    [self adjustAllVisibleCellFrames];
}

- (BOOL)autoresizesSubviews 
{  
    return NO;
}

- (void)tileCells 
{
    CGRect visibleBounds = self.bounds;
    CGSize cellSize = [self.dataSource gridViewSizeOfCell:self];
    CGFloat topPaddingIncludingHeader = [self topPaddingIncludingHeader];
    visibleBounds = CGRectOffset(visibleBounds, 0, -1.0 * topPaddingIncludingHeader);
    CGFloat firstVisibleRow = floor(CGRectGetMinY(visibleBounds) / (cellSize.height +self.padding));
    firstVisibleRow = MAX(0, firstVisibleRow);
    CGFloat lastVisibleRow  = floor((CGRectGetMaxY(visibleBounds)-1) / (cellSize.height +self.padding));
    NSUInteger firstVisibleCell = firstVisibleRow * self.cellsPerRow;
    firstVisibleCell = [self cellIndexForGridIndex:firstVisibleCell];

    NSInteger countOfCells = [self.dataSource gridViewCountOfCells:self];

    //
    //  Adjust |countOfCells| to account for additional space consumed by the drop
    //  cap.
    //

    //  countOfCells += (self.dropCapHeight * self.dropCapWidth) - 1;
    NSInteger firstNonVisibleIndex = (lastVisibleRow + 1) * self.cellsPerRow;

    //
    //  What we just computed was a grid index. Convert it to a cell index.
    //  Do a little bit of trickery to make we don't hit the drop cap.
    //

    firstNonVisibleIndex = [self cellIndexForGridIndex:firstNonVisibleIndex - 1] + 1;

    //
    //  Cap the first non-visible index as the count of cells.
    //

    firstNonVisibleIndex = MIN(countOfCells, firstNonVisibleIndex);

    firstVisibleIndex_ = firstVisibleCell;
    lastVisibleIndex_ = firstNonVisibleIndex;

    //
    //  Go through the currently visible cells and remove the ones we no longer 
    //  need.
    //

    for (GRGridCell *cell in viewCells_) 
    {
        NSUInteger displayIndex = cell.index;
        
        if ((displayIndex < firstVisibleCell) || (displayIndex >= firstNonVisibleIndex)) 
        {
            [recycledCells_ addObject:cell];
            [cell removeFromSuperview];
        }
    }
    
    [viewCells_ minusSet:recycledCells_];

    //
    //  Go through each expected index and make sure we are showing the
    //  necessary cell.
    //

    for (NSInteger i = firstVisibleCell; i < firstNonVisibleIndex; i++) 
    {
        if (![self isCellVisible:i]) 
        {
            GRGridCell *cell = [self.dataSource gridView:self cellForIndex:i];
            
            if (cell != nil) 
            {
                cell.index = i;
                [viewCells_ addObject:cell];
                [self configureCell:cell];
                [self addSubview:cell];
            }
        }
    }
}

- (void)reloadData 
{  
    for (GRGridCell *cell in viewCells_) 
    {
        [recycledCells_ addObject:cell];
        [cell removeFromSuperview];
    }
    
    [viewCells_ minusSet:recycledCells_];

    [self recomputeContentSize];
    [self tileCells];
    [self adjustAllVisibleCellFrames];
}

- (GRGridCell *)insertCellAtIndex:(NSUInteger)index 
{
    [self recomputeContentSize];
    
    [UIView animateWithDuration:0.5 animations:^(void) {

        // 
        //  Go through each view cell and adjust index & frame if needed.
        //

        for (GRGridCell *cell in viewCells_) 
        {
            if (cell.index >= index) 
            {
                cell.index++;
                
                [self configureCell:cell];
                
                if (!CGRectIntersectsRect(cell.frame, self.bounds)) 
                {
                    //
                    //  The cell is no longer visible. Remove it.
                    //

                    [cell removeFromSuperview];
                    [self.recycledCells addObject:cell];
                }
            }
        }
    }];

    [self.viewCells minusSet:self.recycledCells];

    GRGridCell *insertedCell = [self.dataSource gridView:self cellForIndex:index];
    insertedCell.alpha = 0.0;
    
    if (insertedCell != nil) 
    {
        insertedCell.index = index;
        [viewCells_ addObject:insertedCell];
        [self configureCell:insertedCell];
        [self addSubview:insertedCell];
    }

    //
    //  Fade in.
    //

    [UIView animateWithDuration:0.5 animations:^(void) 
    {
        insertedCell.alpha = 1.0;
    }];

    return insertedCell;
}

- (void)deleteCellAtIndex:(NSUInteger)index 
{  
    NSSet *victims = [viewCells_ objectsPassingTest:^BOOL(id obj, BOOL *stop) 
    {
        if (((GRGridCell *)obj).index == index) 
        {
            *stop = YES;
            return YES;
        }
        
        return NO;
    }];
  
    [UIView animateWithDuration:0.5 animations:^(void) 
    {
        for (GRGridCell *victim in victims) 
        {
            victim.alpha = 0.0;
        }
    } 
    completion:^(BOOL finished) 
    {
        for (GRGridCell *victim in victims) 
        {
            [victim removeFromSuperview];
            [viewCells_ removeObject:victim];
        }
    }];

    //
    //  Shift remaining cells.
    //

    [UIView animateWithDuration:0.5 animations:^(void) 
    {
        for (GRGridCell *cell in viewCells_) 
        {
            if (cell.index > index) 
            {
                cell.index--;
                [self configureCell:cell];
            }
        }
    } completion:^(BOOL finished) {

        //
        //  |tileCells| will bring in any new cells that we need.
        //  It looks strange if it's animated.
        //

        [self tileCells];
    }];
}


- (GRGridCell *)dequeueCell 
{  
    GRGridCell *cell = [self.recycledCells anyObject];
    
    if (cell != nil) 
    {
        [[cell retain] autorelease];
        [self.recycledCells removeObject:cell];
    }
    
    return cell;
}

#pragma mark -
#pragma mark Touch

- (BOOL)gestureRecognizerShouldBegin:(UIGestureRecognizer *)gestureRecognizer 
{  
    if ([gestureRecognizer isKindOfClass:[UITapGestureRecognizer class]]) 
    {
        //
        //  We always recognize single taps.
        //

        return YES;
    }

    BOOL isConstrainPanGestureRecognizer = [gestureRecognizer isKindOfClass:[GRContrainPanGestureRecognizer class]];
    BOOL isLongPressGestureRecognizer    = [gestureRecognizer isKindOfClass:[UILongPressGestureRecognizer class]];

    //
    //  For the other kinds of taps, it will depend upon if we allow editing.
    //

    if ([self.gridViewDelegate respondsToSelector:@selector(gridViewShouldEdit:)] &&
      (isConstrainPanGestureRecognizer || isLongPressGestureRecognizer) &&
      ![self.gridViewDelegate gridViewShouldEdit:self]) 
    {
        return NO;
    }

    //
    //  OK, we allow editing. But if this is a pan, we only recognize the gesture
    //  if the delegate will be able to respond to it.
    //

    if (isConstrainPanGestureRecognizer) 
    {
        return [self.gridViewDelegate respondsToSelector:@selector(gridView:didMoveItemFromIndex:toIndex:)];
    } 
    else 
    {
        return YES;
    }
}

- (void)handleTap:(UITapGestureRecognizer *)tapGesture 
{
    if (tapGesture.state == UIGestureRecognizerStateEnded) 
    {
        CGPoint tapLocation = [tapGesture locationInView:self];
        UIView *subview = [[self hitTest:tapLocation withEvent:nil] superview];
        UIView *superView = [subview superview];
      
        if ([subview isKindOfClass:[GRGridCell class]]) 
        {
            GRGridCell *cell = (GRGridCell*)subview;
            
            if (!cell.deleteButton.hidden)
            {
                return ;
            }
            
            if ([self.gridViewDelegate respondsToSelector:@selector(gridView:didTapCell:)]) 
            {
                [self.gridViewDelegate gridView:self didTapCell:(GRGridCell *)subview];
            }
        }
        else if ([superView isKindOfClass:[GRGridCell class]]) 
        {
            if ([self.gridViewDelegate respondsToSelector:@selector(gridView:didTapCell:)]) 
            {
                [self.gridViewDelegate gridView:self didTapCell:(GRGridCell *)superView];
            }
        }
        else
        {
            // just tap somewhere
            [self.gridViewDelegate gridView:nil didTapCell:nil];
        }
    }
}

- (void)handleLongTap:(UILongPressGestureRecognizer *)gestureRecognizer 
{  
    if (gestureRecognizer.state == UIGestureRecognizerStateBegan)
    {
        CGPoint tapLocation = [gestureRecognizer locationInView:self];
        UIView *subview = [[self hitTest:tapLocation withEvent:nil] superview];
        UIView *superView = [subview superview];
        
        if ([subview isKindOfClass:[GRGridCell class]])
        {
            GRGridCell *cell = (GRGridCell*)subview;
            
            if (!cell.deleteButton.hidden)
            {
                return ;
            }
            
            if ([self.gridViewDelegate respondsToSelector:@selector(gridView:didLongTapCell:)])
            {
                [self.gridViewDelegate gridView:self didLongTapCell:(GRGridCell *)subview];
            }
        }
        else if ([superView isKindOfClass:[GRGridCell class]])
        {
            if ([self.gridViewDelegate respondsToSelector:@selector(gridView:didLongTapCell:)])
            {
                [self.gridViewDelegate gridView:self didLongTapCell:(GRGridCell *)superView];
            }
        }
    }
}

- (void)handleCellPan:(GRContrainPanGestureRecognizer *)panGesture 
{  
    if (![self.gridViewDelegate respondsToSelector:@selector(gridView:didMoveItemFromIndex:toIndex:)]) 
    {
        //
        //  Don't do anything of the delegate can't handle it.
        //

        panGesture.state = UIGestureRecognizerStateFailed;
        return;
    }
  
    //
    //  The initial frame of |self.pannedCell|. All translation adjustments are
    //  done from this.
    //

    static CGRect initialFrame;
    UIView *subview = nil;
  
    switch (panGesture.state) 
    {
        case UIGestureRecognizerStateBegan:
            subview = [self hitTest:panGesture.initialTouchPoint withEvent:nil];
            
            if ([subview isKindOfClass:[GRGridCell class]]) 
            {
                self.pannedCell = (GRGridCell *)subview;
                initialFrame = self.pannedCell.frame;
                [self bringSubviewToFront:self.pannedCell];
                [UIView animateWithDuration:0.1 
                                      delay:0.0 
                                    options:UIViewAnimationOptionBeginFromCurrentState | UIViewAnimationOptionAllowUserInteraction
                                 animations:
                 ^(void) 
                {
                    self.pannedCell.frame = CGRectOffset(initialFrame,
                                                       panGesture.translation.x,
                                                       panGesture.translation.y);                  
                } 
                completion:nil];

                } 
            else 
            {
                self.pannedCell = nil;
            }
        break;
      
        case UIGestureRecognizerStateFailed:
            self.pannedCell = nil;
        break;
          
        case UIGestureRecognizerStateEnded:
            if ((self.pannedCell != nil) && (self.activeGap != self.pannedCell.index)) 
            {
                NSUInteger maxIndex = [self.dataSource gridViewCountOfCells:self] - 1;
                self.activeGap = MIN(maxIndex, self.activeGap);
                [self.gridViewDelegate gridView:self 
                           didMoveItemFromIndex:self.pannedCell.index 
                                        toIndex:self.activeGap];
            }
          
            [UIView animateWithDuration:0.2 
                                delay:0.0 
                              options:UIViewAnimationOptionBeginFromCurrentState | UIViewAnimationOptionAllowUserInteraction
                           animations:
            ^(void) 
            {
                self.pannedCell = nil;

                //
                //  Note we're controlling the animation in this block. Don't nest.
                //

                [self adjustAllVisibleCellFrames];
            } completion:nil];
        break;
          
        default:
            if (self.pannedCell != nil) 
            {
                [self setActiveGap:[self indexForPoint:panGesture.currentTouchPoint] animated:YES];
                self.pannedCell.frame = CGRectOffset(initialFrame, 
                                                 panGesture.translation.x, 
                                                 panGesture.translation.y);
            }
        break;
    }
}

#pragma mark -
#pragma mark Cut, copy, paste

- (BOOL)canBecomeFirstResponder 
{
    return YES;
}

- (BOOL)canPerformAction:(SEL)action withSender:(id)sender 
{  
    //
    //  Check and see if the delegate does a global override on editing.
    //
  
    //  if ([self.gridViewDelegate respondsToSelector:@selector(gridViewShouldEdit:)] &&
    //      ![self.gridViewDelegate gridViewShouldEdit:self]) {
    //    
    //    return NO;
    //  }
    //  
    if (action == @selector(cut:)) 
    {
        //    return ([self.selectedCells count] > 0) &&
        //      [self.gridViewDelegate respondsToSelector:@selector(gridView:didCut:)];
        return NO;
    }
    
    if (action == @selector(copy:)) 
    {
//    return ([self.selectedCells count] > 0) &&
//      [self.gridViewDelegate respondsToSelector:@selector(gridView:didCopy:)];
        return NO;
    }
    
    if (action == @selector(paste:)) 
    {
//        if (self.insertionPoint == NSNotFound) 
//        {
//            return NO;
//        }
//        
//        if ([self.gridViewDelegate respondsToSelector:@selector(gridViewCanPaste:)]) 
//        {
//
//            //
//            //  Note: Any delegate that implements gridViewCanPaste: MUST
//            //  also implement gridView:didPasteAtPoint:.
//            //
//          
//            return [self.gridViewDelegate gridViewCanPaste:self];
//        } 
//        else 
//        {      
//            return NO;
//        }
        
        return NO;
    }
    
    if (action == @selector(delete:)) 
    {
//        return ([self.selectedCells count] > 0) &&
//          [self.gridViewDelegate respondsToSelector:@selector(gridView:didDelete:)];
        
        return NO;
    }
    
    if (action == @selector(addNew)) 
    {
        if ([self.gridViewDelegate respondsToSelector:@selector(isSupportAdd)])
        {
            return [self.gridViewDelegate isSupportAdd];
        }
    }
    
    if (action == @selector(pasteNew)) 
    {
        if ([self.gridViewDelegate respondsToSelector:@selector(isSupportPaste)])
        {
            return [self.gridViewDelegate isSupportPaste];
        }
    }
    
    if (action == @selector(shareButtonTapped)) 
    {
        if ([self.gridViewDelegate respondsToSelector:@selector(isSupportShare)])
        {
            return [self.gridViewDelegate isSupportShare];
        }
    }
    
    if (action == @selector(copyButtonTapped)) 
    {
        if ([self.gridViewDelegate respondsToSelector:@selector(isSupportCopy)])
        {
            return [self.gridViewDelegate isSupportCopy];
        }
    }
    
    if (action == @selector(moveButtonTapped)) 
    {
        if ([self.gridViewDelegate respondsToSelector:@selector(isSupportMove)])
        {
            return [self.gridViewDelegate isSupportMove];
        }
    }
    
    if (action == @selector(favoriteButtonTapped)) 
    {
        if ([self.gridViewDelegate respondsToSelector:@selector(isSupportFavorite)])
        {
            return [self.gridViewDelegate isSupportFavorite];
        }
    }
    
    if (action == @selector(renameButtonTapped)) 
    {
        if ([self.gridViewDelegate respondsToSelector:@selector(isSupportRename)])
        {
            return [self.gridViewDelegate isSupportRename];
        }
    }
    
    return NO;
}

- (void)cut:(id)sender 
{  
    NSMutableSet *indexSet = [NSMutableSet setWithCapacity:[self.selectedCells count]];

    for (GRGridCell *cell in self.selectedCells) 
    {
        [indexSet addObject:[NSNumber numberWithUnsignedInteger:cell.index]];
    }
    
    [self unselectAllCells];
    [self.gridViewDelegate gridView:self didCut:indexSet];
}

- (void)copy:(id)sender 
{  
    NSMutableSet *indexSet = [NSMutableSet setWithCapacity:[self.selectedCells count]];

    for (GRGridCell *cell in self.selectedCells) 
    {
        [indexSet addObject:[NSNumber numberWithUnsignedInteger:cell.index]];
    }
    
    [self unselectAllCells];
    [self.gridViewDelegate gridView:self didCopy:indexSet];
}

- (void)paste:(id)sender 
{  
    if (([self.selectedCells count] == 1) && 
      ([self.gridViewDelegate respondsToSelector:@selector(gridView:didPasteIntoCell:)])) 
    {
        GRGridCell *cell = [self.selectedCells anyObject];
        [self unselectAllCells];
        [self.gridViewDelegate gridView:self didPasteIntoCell:cell];
    } 
    else 
    {
        [self unselectAllCells];
        [self.gridViewDelegate gridView:self didPasteAtPoint:self.insertionPoint];
    }
}

- (void)delete:(id)sender 
{  
    NSMutableSet *indexSet = [NSMutableSet setWithCapacity:[self.selectedCells count]];

    for (GRGridCell *cell in self.selectedCells) 
    {
        [indexSet addObject:[NSNumber numberWithUnsignedInteger:cell.index]];
    }
    
    [self unselectAllCells];
    [self.gridViewDelegate gridView:self didDelete:indexSet];
}

- (void)addNew 
{
    [self unselectAllCells];
    
    if ([self.gridViewDelegate respondsToSelector:@selector(addNewEntity)]) 
    {        
        [self.gridViewDelegate addNewEntity];
    } 
}

- (void)pasteNew
{
    [self unselectAllCells];
    
    if ([self.gridViewDelegate respondsToSelector:@selector(pasteNewEntity)]) 
    {
        [self.gridViewDelegate pasteNewEntity];
    }
}

- (void)shareButtonTapped 
{    
    //BDGridCell *cell = (BDGridCell *)[self.selectedCells anyObject];
    [self unselectAllCells];    
}

- (void)copyButtonTapped 
{   
    //BDGridCell *cell = (BDGridCell *)[self.selectedCells anyObject];
    [self unselectAllCells];
}

- (void)moveButtonTapped 
{   
    //BDGridCell *cell = (BDGridCell *)[self.selectedCells anyObject];
    [self unselectAllCells];    
}

- (void)favoriteButtonTapped 
{   
    //BDGridCell *cell = (BDGridCell *)[self.selectedCells anyObject];
    [self unselectAllCells]; 
}

- (void) renameButtonTapped
{
    //BDGridCell *cell = (BDGridCell *)[self.selectedCells anyObject];
    
    [self unselectAllCells]; 
}

@end
