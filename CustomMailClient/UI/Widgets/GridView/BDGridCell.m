//
//  GRGridCell.m
//
//  Created by Eugene Pavlyuk on 4/20/11.
//  Copyright 2012 Mothermobile. 
//

#import <QuartzCore/QuartzCore.h>
#import "BDGridCell.h"

@interface BDGridCell ()

- (void)repositionImageAndLabel;

@end


@implementation ContactCell

@synthesize backView;

- (id)initCell 
{
    self = [super initCell];
    
    if (self) 
    {
        self.layer.cornerRadius = 10.f;
        //self.layer.borderWidth = 1.f;
        self.layer.shadowRadius = 5.f;
        self.layer.shadowColor = [UIColor blackColor].CGColor;
        self.layer.shadowOpacity = 0.6f;
        
        //self.layer.borderColor = [UIColor blackColor].CGColor;
        
        self.clipsToBounds = NO;
        
        self.backView = [[[UIView alloc] initWithFrame:CGRectZero] autorelease];
        self.backView.layer.cornerRadius = 10.f;
        self.backView.backgroundColor = [UIColor whiteColor];
        self.backView.clipsToBounds = YES;
        
        [self addSubview:self.backView];
        
        self.imageView.image = nil;
        
        [self.backView addSubview:self.imageView];
        [self.backView addSubview:self.labelView];
        
        [self sendSubviewToBack:self.backView];
        
        self.label.backgroundColor = [UIColor clearColor];
        self.label.textColor = [UIColor blackColor];
        self.label.font = [UIFont boldSystemFontOfSize:15];
    }
    
    return self;
}

- (void)repositionImageAndLabel 
{        
    self.backView.frame = self.bounds;
    self.imageView.frame = self.bounds;
    self.labelView.frame = self.bounds;
    self.label.frame = CGRectMake(0, self.bounds.size.height - self.bounds.size.height / 4, self.bounds.size.width, self.bounds.size.height / 4);
}

@end


@implementation BDGridCell

@dynamic image;
@dynamic caption;
@synthesize index = index_;
@synthesize contentInset = contentInset_;
@synthesize imageView = imageView_;
@synthesize label = label_;
@synthesize dateLabel;
@synthesize checkmark;
@synthesize labelView = labelView_;
@synthesize selected;

- (id)initCell 
{
    self = [super initWithFrame:CGRectZero];
    
    if (self) 
    {
        self.autoresizingMask = UIViewAutoresizingFlexibleHeight | UIViewAutoresizingFlexibleWidth;
        self.backgroundColor = [UIColor colorWithWhite:1.0 alpha:0.0f];

        self.labelView = [[[UIView alloc] initWithFrame:CGRectZero] autorelease];
        self.labelView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleTopMargin;


        self.label = [[[UILabel alloc] init] autorelease];
        self.label.textAlignment = NSTextAlignmentCenter;
        self.label.textColor = [UIColor whiteColor];
        self.label.backgroundColor = [UIColor clearColor];
        self.label.shadowColor = [UIColor darkGrayColor];
        self.label.shadowOffset = CGSizeMake(1, -1);
        self.label.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
        [self.labelView addSubview:self.label];

        self.imageView = [[[UIImageView alloc] init] autorelease];
        self.imageView.clipsToBounds = YES;
        self.imageView.autoresizingMask = UIViewAutoresizingFlexibleWidth | 
        UIViewAutoresizingFlexibleHeight | 
        UIViewAutoresizingFlexibleBottomMargin;
        [self addSubview:self.imageView];
        [self addSubview:self.labelView];

        self.imageView.image = [UIImage imageNamed:@"day_unselected_ipad"];

        self.checkmark = [[[UIImageView alloc] initWithImage:[UIImage imageNamed:@"tick"]] autorelease];
        [self addSubview:self.checkmark];
        self.checkmark.hidden = YES;
        [self repositionImageAndLabel];
    }
    
    return self;
}

- (void)dealloc 
{  
    [imageView_ release];
    [label_ release];
    self.dateLabel = nil;
    self.checkmark = nil;
    [labelView_ release];

    [super dealloc];
}

- (void)layoutSubviews 
{  
    [self repositionImageAndLabel];
}

- (void)repositionImageAndLabel 
{
    self.imageView.frame = self.bounds;
    self.labelView.frame = self.bounds;
}

#pragma mark -
#pragma mark Properties

- (void)setContentInset:(UIEdgeInsets)contentInset 
{
    contentInset_ = contentInset;
    [self repositionImageAndLabel];
}

- (UIImage *)image 
{  
    return self.imageView.image;
}

- (void)setImage:(UIImage *)image 
{  
    self.imageView.image = image;
}

- (NSString *)caption 
{  
    return self.label.text;
}

- (void)setCaption:(NSString *)caption 
{  
    self.label.text = caption;
}

- (void)setSelected:(BOOL)bSelected 
{  
    selected = bSelected;
    
    if (selected) 
    {
        self.imageView.image = [UIImage imageNamed:@"day_selected_ipad"];      
    } 
    else 
    {
        self.imageView.image = [UIImage imageNamed:@"day_unselected_ipad"];
    }
}

@end


@implementation PhotoCell

@synthesize photoImageView;

- (id)initCell
{
    self = [super initCell];
    
    if (self)
    {
        photoImageView = [[UIImageView alloc] initWithFrame:self.bounds];
        [self addSubview:photoImageView];
        photoImageView.autoresizingMask = UIViewAutoresizingFlexibleHeight | UIViewAutoresizingFlexibleWidth;
        photoImageView.backgroundColor = [UIColor clearColor];
        photoImageView.contentMode = UIViewContentModeScaleAspectFit;
        photoImageView.clipsToBounds = YES;
    }
    
    return self;
}

- (id)initCellWithRoundedCorners
{
    self = [self initCell];
    
    if (self)
    {
        photoImageView.contentMode = UIViewContentModeScaleAspectFill;
        self.layer.shadowOpacity = 1.f;
        self.layer.cornerRadius = 14.f;
        self.layer.shadowOffset = CGSizeZero;
        self.photoImageView.layer.cornerRadius = 14.f;
    }
    
    return self;
}

- (void)repositionImageAndLabel
{
    self.photoImageView.frame = self.bounds;
}

- (void)dealloc
{
    self.photoImageView = nil;
    [super dealloc];
}

- (void)setSelected:(BOOL)bSelected
{
    selected = bSelected;
    
    if (selected)
    {
        self.layer.borderWidth = 10.f;
        self.layer.borderColor = RGBCOLOR(255, 29, 27).CGColor;
    }
    else
    {
        self.layer.borderWidth = 1.f;
        self.layer.borderColor = RGBCOLOR(150, 148, 186).CGColor;
    }
}

@end
