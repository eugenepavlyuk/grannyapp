//
//  GRGridCell.h
//
//  This is the cell that goes into a GRGridView. 
//
//  Created by Eugene Pavlyuk on 4/20/11.
//  Copyright 2011 Mothermobile. 

#import <UIKit/UIKit.h>

typedef void (^screenshotCreated) (UIImage *image);
typedef void (^screenshotFailed) (void);

@interface GRGridCell : UIView <UIWebViewDelegate>
{
    BOOL selected;
}

@property (nonatomic, copy) screenshotCreated successBlock;
@property (nonatomic, copy) screenshotFailed failureBlock;

@property (nonatomic, retain) UIImageView *imageView;
@property (nonatomic, retain) UILabel *label;
@property (nonatomic, retain) UIButton *deleteButton;
@property (nonatomic, retain) UIWebView *webView;
@property (nonatomic, retain) UIActivityIndicatorView *spinner;

@property (nonatomic, assign) NSUInteger index;
@property (nonatomic, assign, getter=isSelected) BOOL selected;

@property (nonatomic, assign) CGSize parentSize;

- (id)initCellWithRect:(CGRect)rect;

- (void)startAnimating;
- (void)stopAnimating;

- (void)createScreenshotFromUrl:(NSURL*)url competionBlock:(screenshotCreated)completion failureBlock:(screenshotFailed)failure;

@end

