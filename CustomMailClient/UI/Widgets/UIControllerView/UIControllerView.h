//
//  UIControllerView.h
//  GrannyApp
//
//  Created by Eugene Pavluk on 12/12/13.
//
//

#import <UIKit/UIKit.h>

@interface UIControllerView : UIView

@property (retain, nonatomic) IBOutlet UIView* hostPickerView;

- (UIImage*)imageForLeftPane;
- (UIImage*)imageForRightPane;
- (UIImage*)imageForSectionWheel;
- (UIImage*)imageForSectionsSeparator;
- (UIImage*)imageForTexture;
- (UIImage*)imageForGlass;
+ (UIPickerView*)findPickerInView:(UIView*)view;

@end
