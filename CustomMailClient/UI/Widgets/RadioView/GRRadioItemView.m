//
//  GRRadioItemView.m
//  GrannyApp
//
//  Created by Ievgen Pavliuk on 7/17/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "GRRadioItemView.h"
#import "GRRadioManager.h"

@implementation GRRadioItemView
{
}

@synthesize lTitle;
@synthesize delegate;
@synthesize mode;

#pragma mark - Initialization

- (void)awakeFromNib
{
    [super awakeFromNib];
    
    self.backgroundColor = [UIColor whiteColor];
    self.layer.borderColor = [UIColor lightGrayColor].CGColor;
    self.layer.borderWidth = 1.f;
    self.layer.shadowOffset = CGSizeMake(2, 3);
    self.layer.shadowOpacity = 0.6f;
    self.layer.shadowColor = [UIColor blackColor].CGColor;
    self.layer.shadowRadius = 5.f;
    self.layer.cornerRadius = 20.f;
}

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self)
    {
    }
    return self;
}

- (void)dealloc {
    delegate = nil;
    [radioItem release];
    [lTitle release];
    [super dealloc];
}


#pragma mark - Update data

- (void)updateWithRadioData:(TRadioStation *)radioData selected:(BOOL)bSelected
{
    if (radioData)
    {
        [radioItem release];
        radioItem = [radioData retain];
        self.lTitle.text = radioData.title;
        
        if (bSelected)
        {
            self.lTitle.textColor = [UIColor whiteColor];
            self.backgroundColor = RGBCOLOR(220, 54, 0);
            self.lTitle.font = [UIFont fontWithName:@"Helvetica-Bold" size:36];
        }
        else
        {
            self.lTitle.textColor = RGBCOLOR(128, 128, 128);
            self.backgroundColor = [UIColor whiteColor];
            self.lTitle.font = [UIFont fontWithName:@"Helvetica" size:36];
        }
    }
    else
    {
        self.lTitle.textColor = RGBCOLOR(128, 128, 128);
        self.lTitle.text = NSLocalizedString(@"Add station", @"Add station");
        self.backgroundColor = [UIColor whiteColor];
        self.lTitle.font = [UIFont fontWithName:@"Helvetica" size:36];
    }
}

@end
