//
//  GRRadioButtonView.m
//  Granny
//
//  Created by Ievgen Pavliuk on 4/10/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "GRRadioButtonView.h"
#import <QuartzCore/QuartzCore.h>

@implementation GRRadioButtonView
@synthesize radioData;
@synthesize delegate;

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        self.layer.masksToBounds = YES;
        [self.layer setCornerRadius:0.5f];
        self.backgroundColor = [UIColor blackColor];
    }
    return self;
}

- (void)dealloc {
    delegate = nil;
    [radioData release];
    [super dealloc];
}

#pragma mark - UserInteraction

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
}

- (void)touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event {
    [delegate performSelector:@selector(radioButtonTappedWithData:) withObject:self.radioData];
}

#pragma mark - Helpers

- (void)setTitle:(NSString *)title {
    if (!title) {
        title = NSLocalizedString(@"Uknown title", @"Uknown title");
    }
    CGSize lSize = [self getSizeForTitleLabel:title];
    lTitle = [[UILabel alloc] initWithFrame:CGRectMake(self.frame.size.width/2 - lSize.width/2, self.frame.size.height/2 - lSize.height/2, lSize.width, lSize.height)];

    lTitle = [[UILabel alloc] initWithFrame:CGRectMake(self.frame.size.width/2 - lSize.width/2, self.frame.size.height/2 - lSize.height/2, lSize.width, lSize.height)];
    lTitle.textColor = [UIColor whiteColor];
    lTitle.font = [UIFont fontWithName:@"Helvetica-Bold" size:16];
    lTitle.textAlignment = NSTextAlignmentCenter;
    lTitle.numberOfLines = lSize.height/16;
    lTitle.backgroundColor = [UIColor clearColor];
    lTitle.text = title;
    [self addSubview:lTitle];

}

- (CGSize)getSizeForTitleLabel:(NSString *)title {
    if (![title isKindOfClass:NSString.class]) {
        return CGSizeZero;
    }
    CGSize expectedLabelSize = CGSizeZero;
    CGSize maximumLabelSize = CGSizeMake(self.frame.size.width-10, self.frame.size.height-20);
    @try {
        expectedLabelSize = [title sizeWithFont: [UIFont fontWithName:@"Helvetica-Bold" size:16]
                              constrainedToSize:maximumLabelSize lineBreakMode:NSLineBreakByTruncatingTail];
    }
    @catch (NSException *exception) {
        expectedLabelSize = CGSizeZero;
    }
    @finally {
        
    }
    return expectedLabelSize;
}


/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

@end
