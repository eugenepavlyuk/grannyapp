//
//  GRRadioItemView.h
//  GrannyApp
//
//  Created by Ievgen Pavliuk on 7/17/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TRadioStation.h"
#import "BDGridCell.h"

@protocol GRRadioItemDelegate <NSObject>

- (void)radioItemselected:(TRadioStation *)radioItem;

@end

enum radioItemMode
{
    rimDefualt,
    rimSelected,
    rimPlay,
    rimAdd
};

@interface GRRadioItemView : BDGridCell
{
    TRadioStation *radioItem;
    UILabel      *lTitle;
    
    id<GRRadioItemDelegate> delegate;
    
    int mode;
}
@property (nonatomic, retain) IBOutlet UILabel      *lTitle;
@property (nonatomic, assign) id<GRRadioItemDelegate> delegate;
@property int mode;


- (void)updateWithRadioData:(TRadioStation *)radioData selected:(BOOL)selected;
@end
