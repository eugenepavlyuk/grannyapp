//
//  GRRadioSearchBtnView.h
//  GrannyApp
//
//  Created by Ievgen Pavliuk on 8/3/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol GRRadiSearcBtnDelegate <NSObject>

- (void)radioSearchBtnSelectedWithIndex:(NSInteger)index;

@end

@interface GRRadioSearchBtnView : UIView {
    
    NSInteger btnIndex;
    id<GRRadiSearcBtnDelegate> delegate;
}
@property (nonatomic, assign) id<GRRadiSearcBtnDelegate> delegate;

- (id)initWithFrame:(CGRect)frame index:(NSInteger) index;
@end
