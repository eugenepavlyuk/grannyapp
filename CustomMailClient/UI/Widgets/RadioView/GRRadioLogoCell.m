//
//  GRRadioLogoCell.m
//  GrannyApp
//
//  Created by Ievgen Pavliuk on 9/26/13.
//
//

#import "GRRadioLogoCell.h"

@implementation GRRadioLogoCell

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        self.layer.borderColor = [UIColor blackColor].CGColor;
        self.layer.borderWidth = 2.0f;
        self.layer.cornerRadius = 5.0f;
        self.layer.shadowColor = [UIColor blackColor].CGColor;
        self.layer.shadowRadius = 3.0f;
        self.layer.shadowOffset = CGSizeMake(0.0f, 2.0f);
        self.layer.shadowOpacity = 0.5f;
        self.layer.rasterizationScale = [UIScreen mainScreen].scale;
        self.layer.shouldRasterize = YES;
        
        backGroundImage = [[UIImageView alloc] init];
        backGroundImage.frame = CGRectMake(0, 0, self.contentView.frame.size.width, self.contentView.frame.size.height);
        backGroundImage.layer.cornerRadius = 5.0f;
        [self.contentView addSubview:backGroundImage];
    }
    
    return self;
}

- (void)updateForItem:(ALAsset *)asset {
    UIImage *pImage = nil;
    
    if ([[[UIDevice currentDevice] systemVersion] floatValue] >= 5.f)
    {
        pImage = [UIImage imageWithCGImage:[asset aspectRatioThumbnail]];
    }
    else
    {
        pImage = [UIImage imageWithCGImage:[asset thumbnail]];
    }

    if (pImage) {
        [backGroundImage setImage:pImage];
    }
    
}


- (void)prepareForReuse
{
    [super prepareForReuse];
}
- (void)dealloc {
    [backGroundImage release];
    [super dealloc];
}

@end
