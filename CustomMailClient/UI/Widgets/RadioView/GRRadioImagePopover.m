//
//  GRRadioImagePopover.m
//  GrannyApp
//
//  Created by Ievgen Pavliuk on 9/26/13.
//
//

#import "GRRadioImagePopover.h"
#import "GRRadioLogoCell.h"

#define IMAGE_SIZE 84

@implementation GRRadioImagePopover
@synthesize photoCollection;
@synthesize delegate;


#pragma mark - Initialization

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)awakeFromNib {
    [super awakeFromNib];
    
    UICollectionViewFlowLayout *flowLayout = [[UICollectionViewFlowLayout alloc] init];
    [flowLayout setItemSize:CGSizeMake(IMAGE_SIZE, IMAGE_SIZE)];
    [flowLayout setScrollDirection:UICollectionViewScrollDirectionVertical];
    [flowLayout setSectionInset:UIEdgeInsetsMake(3.0f, 0.0f, 0, 0.0f)];
    [self.photoCollection setBackgroundColor:[UIColor whiteColor]];
    [self.photoCollection setCollectionViewLayout:flowLayout];
    [self.photoCollection setAllowsSelection:YES];
    [self.photoCollection registerClass:[GRRadioLogoCell class]forCellWithReuseIdentifier:@"GRRadioLogoCell"];
    [flowLayout release];
    [self startLoadingPhotoFromGallery];
}

- (void)startLoadingPhotoFromGallery
{
    void (^assetEnumerator)(ALAsset *, NSUInteger, BOOL *) = ^(ALAsset *result, NSUInteger index, BOOL *stop) {
        if(result != NULL) {
            //            NSLog(@"See Asset: %@", result);
            [photoAssets addObject:result];
        }
    };
    
    void (^assetGroupEnumerator)(ALAssetsGroup *, BOOL *) =  ^(ALAssetsGroup *group, BOOL *stop) {
    	if(group != nil) {
            [group enumerateAssetsUsingBlock:assetEnumerator];
        } else {
            [self.photoCollection reloadData];
        }
    };
    
    photoAssets = [[NSMutableArray alloc] init];
    library = [[ALAssetsLibrary alloc] init];
    [library enumerateGroupsWithTypes:ALAssetsGroupSavedPhotos
                           usingBlock:assetGroupEnumerator
                         failureBlock: ^(NSError *error) {
                             NSLog(@"Failure");
                         }];
}



- (void)dealloc {
    [photoCollection release];
    [photoAssets release];
    [library release];
    delegate = nil;
    [super dealloc];
}


#pragma mark - UICollectionViewDataSource

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return [photoAssets count];
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *identifier = @"GRRadioLogoCell";
    
    GRRadioLogoCell *collectionCell =
    [collectionView dequeueReusableCellWithReuseIdentifier:identifier forIndexPath:indexPath];
    ALAsset *asset = [photoAssets objectAtIndex:indexPath.row];
    [collectionCell updateForItem:asset];
    
    return collectionCell;
}

#pragma mark - UICollectionViewDelegate

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    ALAsset *asset = [photoAssets objectAtIndex:indexPath.row];
    UIImage *pImage = nil;
    
    if ([[[UIDevice currentDevice] systemVersion] floatValue] >= 5.f)
    {
        pImage = [UIImage imageWithCGImage:[asset aspectRatioThumbnail]];
    }
    else
    {
        pImage = [UIImage imageWithCGImage:[asset thumbnail]];
    }
    if (pImage) {
        NSURL *url = [[asset defaultRepresentation] url];
        [self.delegate imageSelectedWithPath:[url absoluteString] image:pImage];
    }
    self.hidden = YES;
}


@end
