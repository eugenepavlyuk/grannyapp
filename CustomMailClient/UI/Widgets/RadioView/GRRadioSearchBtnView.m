//
//  GRRadioSearchBtnView.m
//  GrannyApp
//
//  Created by Ievgen Pavliuk on 8/3/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "GRRadioSearchBtnView.h"

@implementation GRRadioSearchBtnView
@synthesize delegate;

- (id)initWithFrame:(CGRect)frame index:(NSInteger) index
{
    self = [super initWithFrame:frame];
    if (self) {
        self.userInteractionEnabled = YES;
        btnIndex = index;
        
        UIImageView *backgroundImageView = [[[UIImageView alloc] initWithFrame:CGRectMake(2, 0, self.frame.size.width-2, self.frame.size.height)] autorelease];
        
        if (!IS_IPAD) {
            backgroundImageView.frame = CGRectMake(2, 2, self.frame.size.width-2, self.frame.size.height);
        }
        
        backgroundImageView.tag = 111;

        UILabel *lTitle = [[[UILabel alloc] initWithFrame:CGRectMake(0, self.frame.size.height - 40, self.frame.size.width, 30)] autorelease];
        lTitle.backgroundColor = [UIColor clearColor];
        lTitle.font = [UIFont fontWithName:@"Helvetica" size:18];
        lTitle.textColor = RGBCOLOR(96, 96, 96);
        lTitle.textAlignment = NSTextAlignmentCenter;
        lTitle.tag = 222;
        
        if (!IS_IPAD) {
            lTitle.frame = CGRectMake(0, self.frame.size.height - 20, self.frame.size.width, 15);
            lTitle.font = [UIFont fontWithName:@"Helvetica-Bold" size:10];
        }
        
        NSString *iconViewImagePath = nil;
        switch (index) {
            case 0:
//                [backgroundImageView setImage:[UIImage imageNamed:@"Settings_RadioSearchLeftBtnSelectedBck.png"]];
                iconViewImagePath = @"Settings_ListBtn";
                lTitle.text = NSLocalizedString(@"From List", @"From List");
                break;
            case 1:
//                [backgroundImageView setImage:[UIImage imageNamed:@"Settings_RadioSearchMiddleBtnSelectedBck"]];
                iconViewImagePath = @"Settings_SearchBtn";
                lTitle.text = NSLocalizedString(@"Search", @"Search");
                break;
            case 2:
//                [backgroundImageView setImage:[UIImage imageNamed:@"Settings_RadioSearchRightBtnSelectedBck"]];
                backgroundImageView.frame = CGRectMake(3, 0, self.frame.size.width, self.frame.size.height);
                if (!IS_IPAD) {
                    backgroundImageView.frame = CGRectMake(3, 2, self.frame.size.width, self.frame.size.height);
                }

                iconViewImagePath = @"Settings_RadioSearchManualBtn";
                lTitle.text = NSLocalizedString(@"Add Manually", @"Add Manually");
                break;
                
            default:
                break;
        }
        UIImageView *iconView = [[[UIImageView alloc] initWithImage:[UIImage imageNamed:iconViewImagePath]] autorelease];
        
        iconView.frame = CGRectMake(self.frame.size.width/2 - iconView.frame.size.width/2, self.frame.size.height/2 - iconView.frame.size.height/2 - 10, iconView.frame.size.width, iconView.frame.size.height);
        
        if (!IS_IPAD) {
            iconView.frame = CGRectMake(self.frame.size.width/2 - iconView.frame.size.width/4, self.frame.size.height/2 - iconView.frame.size.height/4 - 10, iconView.frame.size.width/2, iconView.frame.size.height/2);
        }
        
        
        [self addSubview:backgroundImageView];
        [self addSubview:iconView];
        [self addSubview:lTitle];
        backgroundImageView.hidden = YES;
        
    }
    return self;
}


#pragma mark - userInteractionEnabled

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
    UILabel *lTitle = (UILabel *)[self viewWithTag:222];
    lTitle.textColor = [UIColor whiteColor];
    
    UIImageView *backgroundImageView = (UIImageView *)[self viewWithTag:111];
    backgroundImageView.hidden = NO;
}


- (void)touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event {
    UIImageView *backgroundImageView = (UIImageView *)[self viewWithTag:111];
    backgroundImageView.hidden = YES;
    if ([delegate respondsToSelector:@selector(radioSearchBtnSelectedWithIndex:)]) {
        [delegate radioSearchBtnSelectedWithIndex:btnIndex];
    }
}

@end
