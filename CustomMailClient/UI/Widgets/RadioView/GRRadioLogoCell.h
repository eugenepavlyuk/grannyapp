//
//  GRRadioLogoCell.h
//  GrannyApp
//
//  Created by Ievgen Pavliuk on 9/26/13.
//
//

#import <Foundation/Foundation.h>

@interface GRRadioLogoCell : UICollectionViewCell {
    UIImageView *backGroundImage;

}

- (void)updateForItem:(ALAsset *)asset;
@end
