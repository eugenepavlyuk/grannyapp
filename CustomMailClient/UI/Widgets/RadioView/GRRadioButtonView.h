//
//  GRRadioButtonView.h
//  Granny
//
//  Created by Ievgen Pavliuk on 4/10/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface GRRadioButtonView : UIView {
    
    NSDictionary *radioData;
    UILabel      *lTitle;
    id delegate;
}
@property (nonatomic, retain) id delegate;
@property (nonatomic, retain) NSDictionary *radioData;

- (CGSize)getSizeForTitleLabel:(NSString *)title;
- (void)setTitle:(NSString *)title;
@end
