//
//  GRRadioImagePopover.h
//  GrannyApp
//
//  Created by Ievgen Pavliuk on 9/26/13.
//
//

#import <UIKit/UIKit.h>

@protocol GRRadioImagePopoverDelegate <NSObject>

- (void)imageSelectedWithPath:(NSString *)path image:(UIImage *)image;

@end

@interface GRRadioImagePopover : UIView <UICollectionViewDataSource, UICollectionViewDelegate> {
    
    UICollectionView   *photoCollection;
    NSMutableArray     *photoAssets;
    ALAssetsLibrary    *library;
    id<GRRadioImagePopoverDelegate> delegate;
}
@property (nonatomic, retain) IBOutlet UICollectionView *photoCollection;
@property (nonatomic, assign) id<GRRadioImagePopoverDelegate> delegate;


@end
