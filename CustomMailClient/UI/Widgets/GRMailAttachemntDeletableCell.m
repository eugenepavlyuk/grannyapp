//
//  GRMailAttachemntDeletableCell.m
//  GrannyApp
//
//  Created by admin on 11/12/13.
//
//

#import "GRMailAttachemntDeletableCell.h"

@implementation GRMailAttachemntDeletableCell

- (void)dealloc {
    
    [_deleteButton release];
    
    [super dealloc];
}

- (void)awakeFromNib {
    [super awakeFromNib];
    
    self.layer.borderWidth = 1.0f;
    self.layer.borderColor = [UIColor lightGrayColor].CGColor;
    self.layer.cornerRadius = 15.0f;
    self.layer.masksToBounds = YES;
}

- (IBAction)removeAttachmentBtnTapped:(id)sender {
    if (self.delegate && [self.delegate respondsToSelector:@selector(mailAttachmentCellRemoveWithIndex:)]) {
        [self.delegate mailAttachmentCellRemoveWithIndex:self.index];
    }
}

@end
