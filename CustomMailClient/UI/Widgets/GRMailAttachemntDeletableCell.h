//
//  GRMailAttachemntDeletableCell.h
//  GrannyApp
//
//  Created by admin on 11/12/13.
//
//

#import "GRMailAttachemntCell.h"

@interface GRMailAttachemntDeletableCell : GRMailAttachemntCell

@property (nonatomic, retain) IBOutlet UIButton *deleteButton;

- (IBAction)removeAttachmentBtnTapped:(id)sender;

- (void)awakeFromNib;

@end
