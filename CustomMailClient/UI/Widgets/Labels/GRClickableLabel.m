//
//  GRClickableLabel.m
//  GrannyApp
//
//  Created by Ievgen Pavliuk on 7/20/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "GRClickableLabel.h"

@implementation GRClickableLabel
@synthesize delegate;
@synthesize object;


- (id)initWithFrame:(CGRect)frame {
    if ((self = [super initWithFrame:frame])) {
        self.backgroundColor = [UIColor clearColor];
        self.userInteractionEnabled = YES;
    }
    return self;
}

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
    
}


- (void)touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event {
    if ([delegate respondsToSelector:@selector(clickableLabelSelectedWithObject:)]) {
        [delegate clickableLabelSelectedWithObject:self.object];
    }
}

- (void)dealloc {
    delegate = nil;
    [object release];
    [super dealloc];
}
@end
