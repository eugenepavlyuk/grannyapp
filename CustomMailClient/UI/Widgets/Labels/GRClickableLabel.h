//
//  GRClickableLabel.h
//  GrannyApp
//
//  Created by Ievgen Pavliuk on 7/20/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol GRClickableLabelDelegate <NSObject>

- (void)clickableLabelSelectedWithObject:(id)object;

@end

@interface GRClickableLabel : UILabel {
    id <GRClickableLabelDelegate> delegate;
    id object;
}
@property (nonatomic, assign) id<GRClickableLabelDelegate> delegate;
@property (nonatomic, retain) id object;

@end
