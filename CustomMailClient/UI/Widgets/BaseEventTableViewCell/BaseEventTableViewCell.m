//
//  BaseEventTableViewCell.m
//  GrannyApp
//
//  Created by Eugene Pavluk on 4/17/13.
//
//

#import "BaseEventTableViewCell.h"

@interface BaseEventTableViewCell()

@end


@implementation BaseEventTableViewCell

- (void)awakeFromNib
{
    self.separatorOffset = CGRectMake(0, 0, 0, 2);
    
    self.backgroundImageView = [[UIImageView alloc] initWithFrame:self.bounds];
    self.backgroundImageView.backgroundColor = [UIColor clearColor];
    
    self.backgroundImageView.image = [[UIImage imageNamed:@"alarm_cell_background_ipad"] stretchableImageWithLeftCapWidth:0 topCapHeight:5];
    
    [self.contentView insertSubview:self.backgroundImageView atIndex:0];
    
    self.separatorImageView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 1, self.bounds.size.width, 2)];
    
    self.separatorImageView.image = [[UIImage imageNamed:@"alarm_separator_ipad"] stretchableImageWithLeftCapWidth:1 topCapHeight:0];
    
    [self.contentView insertSubview:self.separatorImageView aboveSubview:self.backgroundImageView];
}

+ (NSString*)cellIdentifier
{
    return @"BaseEventTableViewCellIdentifier";
}

- (void)layoutSubviews
{
    [super layoutSubviews];
    
    self.backgroundImageView.frame = self.bounds;
    
    self.separatorImageView.frame = CGRectMake(self.separatorOffset.size.width - self.separatorOffset.origin.x, self.bounds.size.height - self.separatorOffset.size.height - self.separatorOffset.origin.y, self.bounds.size.width - 2 * self.separatorOffset.size.width, 2);
}

@end
