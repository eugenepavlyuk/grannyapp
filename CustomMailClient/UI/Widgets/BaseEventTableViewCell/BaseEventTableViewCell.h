//
//  BaseEventTableViewCell.h
//  GrannyApp
//
//  Created by Eugene Pavluk on 4/17/13.
//
//

#import <UIKit/UIKit.h>

@interface BaseEventTableViewCell : UITableViewCell

@property (nonatomic, retain) UIImageView *backgroundImageView;
@property (nonatomic, retain) UIImageView *separatorImageView;
@property (nonatomic, assign) CGRect separatorOffset;

+ (NSString*)cellIdentifier;

@end
