//
//  GRMailAttachmentView.m
//  GrannyApp
//
//  Created by Ievgen Pavliuk on 7/25/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "GRMailAttachmentView.h"
#import "GRCommonAlertView.h"
#import "UIImage+Resize.h"

@implementation GRMailAttachmentView
{
    NSData *imageData;
}

#pragma mark - Initialization

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)dealloc {
    self.saveBtn = nil;
    [imageData release];
    [_contentView release];
    [_imageView release];
    [super dealloc];
}

- (void)awakeFromNib {
    self.saveBtn.hidden = YES;
    self.saveBtn.layer.borderWidth = 1.0f;
    self.saveBtn.layer.cornerRadius = 16.0f;
    self.saveBtn.layer.masksToBounds = YES;
    self.saveBtn.layer.borderColor = [UIColor grayColor].CGColor;
}

#pragma mark - Actions

- (IBAction)closeBtnTapped:(id)sender {
    [self removeFromSuperview];
}
- (IBAction)saveBtnTapped:(id)sender
{
    ALAssetsLibrary * library = [[[ALAssetsLibrary alloc] init] autorelease];
    [library writeImageDataToSavedPhotosAlbum:imageData metadata:nil completionBlock:^(NSURL *assetURL, NSError *error)
     {
         if (error)
             [[[GRCommonAlertView alloc] initCommonAlert:NSLocalizedString(@"Error", @"Error") inView:self] autorelease];
         else
             [[[GRCommonAlertView alloc] initCommonAlert:NSLocalizedString(@"This attachment was saved to Photos", @"This attachment was saved to Photos") inView:self] autorelease];
    }];
}

#pragma mark - Load image

- (void)loadImage:(NSString *)filePath
{
    imageData = [[NSData alloc] initWithContentsOfFile:filePath];
    UIImage *image = [UIImage imageWithData:imageData];
    if (image) {
        if (image.size.width > image.size.height) {
            UIImage *newImage = [image resizedImageWithContentMode:UIViewContentModeScaleAspectFill bounds:self.imageView.bounds.size interpolationQuality:0];
            self.imageView.image = newImage;
        } else {
            self.imageView.image = image;
        }
    }
}

- (void)loadDocument:(NSString *)filePath {
    UIWebView *webView = [[[UIWebView alloc] initWithFrame:CGRectMake(0, 0,self.contentView.frame.size.width, self.contentView.frame.size.height)] autorelease];
    webView.scalesPageToFit = YES;
    [self.contentView insertSubview:webView atIndex:0];
    NSURLRequest *request = [NSURLRequest requestWithURL:[NSURL fileURLWithPath:filePath]];
    [webView loadRequest:request];
}


@end
