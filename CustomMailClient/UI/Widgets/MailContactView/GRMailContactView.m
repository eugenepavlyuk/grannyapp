//
//  GRMainContactView.m
//  GrannyApp
//
//  Created by Ievgen Pavliuk on 5/30/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "GRMailContactView.h"
#import "GRCommonAlertView.h"

@implementation GRMailContactView
@synthesize delegate;
@synthesize contactIndex;
@synthesize isGreen;

- (id)initWithRecient:(GRContact *)contact favorites:(BOOL)_isFavorites
{
    if ((self = [super init]))
    {
        isFavorite = _isFavorites;
        rContact = [contact retain];
        lName = [[UILabel alloc] init];
        lName.backgroundColor = [UIColor clearColor];
        lName.textAlignment = NSTextAlignmentLeft;
        
        UIImageView *backGroundImage = nil;
        if (_isFavorites)
        {
            if (contact.photourl) {
                NSData *imageData = [NSData dataWithContentsOfFile:contact.photourl];
                UIImage *image = [UIImage imageWithData:imageData];
                backGroundImage = [[[UIImageView alloc] initWithImage:image] autorelease];
                backGroundImage.layer.masksToBounds = YES;
                
                if (IS_IPAD) 
                {
                    [backGroundImage.layer setCornerRadius:15.0f];
                } 
                else 
                {
                    [backGroundImage.layer setCornerRadius:5.0f];
                }
            } 
            else 
            {
                if (IS_IPAD) 
                    lName.font = [UIFont fontWithName:@"Helvetica-Bold" size:13];
                else
                    lName.font = [UIFont fontWithName:@"Helvetica-Bold" size:8];
                lName.text = [NSString stringWithFormat:@"%@ %@", contact.firstname ? contact.firstname : @"", contact.lastname ? contact.lastname : @""];
                lName.textColor = [UIColor grayColor];
                lName.numberOfLines = 2;
                lName.textAlignment = NSTextAlignmentCenter;
                backGroundImage = [[[UIImageView alloc] initWithImage:[UIImage imageNamed:@"Mail_Fav_ContactIconBckg.png"]] autorelease];
            }
        }
        else
        {
            lName.text = [NSString stringWithFormat:@"%@ %@", contact.firstname ? contact.firstname : @"", contact.lastname ? contact.lastname : @""];
            
            if (IS_IPAD) 
                lName.font = [UIFont fontWithName:@"Helvetica-Bold" size:24];
            else
                lName.font = [UIFont fontWithName:@"Helvetica-Bold" size:12];
            lName.textColor = [UIColor whiteColor];
            if (contactIndex%2 == 0)
                backGroundImage = [[[UIImageView alloc] initWithImage:[UIImage imageNamed: @"Mail_Contact_GrayBck.png"]] autorelease];
            else
                backGroundImage = [[[UIImageView alloc] initWithImage:[UIImage imageNamed: @"Mail_Contact_YellowBck.png"]] autorelease];
        }
        
        backGroundImage.tag = 1;
        [self addSubview:backGroundImage];
        [self addSubview:lName];
    }
    return self;
}

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
    }
    return self;
}

- (void)dealloc {
    delegate = nil;
    [rContact release];
    [lName release];
    [super dealloc];
}

- (void)layoutSubviews
{
    [super layoutSubviews];
    lName.frame = CGRectMake(10, 0, self.frame.size.width-20, self.frame.size.height);
    UIImageView *backgroundImage = (UIImageView *)[self viewWithTag:1];
    if (isFavorite)
    {
        lName.frame = CGRectMake(10, 30, self.frame.size.width-20, self.frame.size.height);
        if (IS_IPAD) 
        {
            backgroundImage.frame = CGRectMake(2, 2, self.frame.size.width-4, self.frame.size.height-4);        
        } else {
            if (rContact.photourl) {
                backgroundImage.frame = CGRectMake(3, 3, self.frame.size.width-6, self.frame.size.height-6);        
            } else {
                backgroundImage.frame = CGRectMake(2, 2, self.frame.size.width-4, self.frame.size.height-4);        
            }
        }
    }
    else
    {
        backgroundImage.frame = CGRectMake(0, 0, self.frame.size.width, self.frame.size.height);
        if (isGreen)
            backgroundImage.image = [[UIImage imageNamed:@"people_non_favorite_background_ipad"] stretchableImageWithLeftCapWidth:4 topCapHeight:0];
        else
            backgroundImage.image = [[UIImage imageNamed:@"people_non_favorite_background2_ipad"] stretchableImageWithLeftCapWidth:4 topCapHeight:0];
//        if (contactIndex < 2)
//        {
//            if (contactIndex%2 == 0)
//                backgroundImage.image = [[UIImage imageNamed:@"people_non_favorite_background_ipad"] stretchableImageWithLeftCapWidth:4 topCapHeight:0];
//            else
//                backgroundImage.image = [[UIImage imageNamed:@"people_non_favorite_background2_ipad"] stretchableImageWithLeftCapWidth:4 topCapHeight:0];
//        }
//        else
//        {
//            if (((contactIndex-1) % 2) == 0)
//            {
//                backgroundImage.image = [[UIImage imageNamed:@"people_non_favorite_background2_ipad"] stretchableImageWithLeftCapWidth:4 topCapHeight:0];
//            }
//            else
//            {
//                backgroundImage.image = [[UIImage imageNamed:@"people_non_favorite_background_ipad"] stretchableImageWithLeftCapWidth:4 topCapHeight:0];
//            }
//        }
    }
}

#pragma mark - UserInteraction

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
}

- (void)touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event
{
    NSString *contactName = rContact.selectedEmail ? rContact.selectedEmail:rContact.email;
    if ([self validEmail:contactName])
        [delegate mailContactSelectedWithContact:rContact];
    else
    {
        [[[GRCommonAlertView alloc] initCommonAlert:NSLocalizedString(@"Please enter a valid email address", @"Please enter a valid email address")  inView:self] autorelease];
    }
    
//    UIImageView *imageView = (UIImageView *)[self viewWithTag:1];
//    if (imageView.image == [UIImage imageNamed:@"Mail_Contact_YellowBck.png"])
//        [self setUnSelected];
//    else
//        [self setSelected];
//    if (!isFavorite)
//    {
//        [delegate allContactSelectedWithIndex:self.contactIndex];
//    }
}

- (void)setSelected
{
    if (!isFavorite)
    {
        UIImageView *backgroundImage = (UIImageView *)[self viewWithTag:1];
        [backgroundImage setImage:[UIImage imageNamed:@"Mail_Contact_YellowBck.png"]];
    }
}

- (void)setUnSelected {
    if (!isFavorite) {
        UIImageView *backgroundImage = (UIImageView *)[self viewWithTag:1];
        [backgroundImage setImage:[UIImage imageNamed:@"Mail_Contact_GrayBck.png"]];
    }
}

- (BOOL) validEmail:(NSString *)email
{
    NSString *strEmailMatchstring = @"\\b([a-zA-Z0-9%_.+\\-]+)@([a-zA-Z0-9.\\-]+?\\.[a-zA-Z]{2,6})\\b";
    NSPredicate *regExPredicate = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", strEmailMatchstring];
    return [regExPredicate evaluateWithObject:email];
}
@end
