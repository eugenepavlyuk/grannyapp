//
//  GRAllContactsSelector.m
//  GrannyApp
//
//  Created by Ievgen Pavliuk on 4/30/13.
//
//

#import "GRAllContactsSelector.h"
#import "GRPeopleManager.h"
#import "GRContact.h"


#define CONTACT_SIZE CGSizeMake(348.0f, 62.0f)



@implementation GRAllContactsSelector
@synthesize contentTable;
@synthesize delegate;
@synthesize loadingView;
@synthesize searchBar;
@synthesize searchPeople;
@synthesize otherSearchContacts;

#pragma mark - Initialization

- (void)awakeFromNib {
    [super awakeFromNib];
    self.loadingView.layer.cornerRadius = 16.0f;
    self.contentTable.separatorStyle = UITableViewCellSeparatorStyleNone;
    
    UIImage *image = [UIImage imageNamed:@"Search_people_cancel_btn~ipad.png"];
    
    UIView *rightView = [[[UIView alloc] initWithFrame:CGRectMake(0, 0, image.size.width*1.4, image.size.height)] autorelease];
    
    UIButton *cancelBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    cancelBtn.frame = CGRectMake(0, 0, image.size.width, image.size.height);
    [cancelBtn setBackgroundImage:image forState:UIControlStateNormal];
    [cancelBtn addTarget:self action:@selector(cancelSearch) forControlEvents:UIControlEventTouchUpInside];
    [rightView addSubview:cancelBtn];
    self.searchBar.rightViewMode = UITextFieldViewModeAlways;
    self.searchBar.rightView = rightView;
    self.searchBar.leftViewMode = UITextFieldViewModeAlways;
    self.searchBar.leftView = [[[UIView alloc] initWithFrame:CGRectMake(0, 0, 20, 1)] autorelease];
    self.searchBar.leftView.backgroundColor = [UIColor clearColor];
    
    self.searchPeople = [NSMutableArray array];
    self.otherSearchContacts = [NSMutableArray array];
    [self customizeViews];
    [self startLoadingContacts];
}

- (void)customizeViews {
    UIBezierPath *maskPath = [UIBezierPath bezierPathWithRoundedRect:[self bounds]
                                                   byRoundingCorners:(UIRectCornerBottomLeft | UIRectCornerBottomRight)
                                                         cornerRadii:CGSizeMake(10.0, 10.0)];
    
    CAShapeLayer *maskLayer = [CAShapeLayer layer];
    maskLayer.frame = [self bounds];
    maskLayer.path = maskPath.CGPath;
    
    self.layer.mask = maskLayer;
    [self.layer setMasksToBounds:YES];
}

- (void)startLoadingContacts {
    self.loadingView.hidden = NO;
    tableData = [[NSMutableArray alloc] init];
    NSLog(@"**************  All contacts %i", [tableData count]);
    [self contactsLoaded];

}

- (void)contactsLoaded {
    self.loadingView.hidden = YES;
    sortedPeople = [[NSMutableArray alloc ]initWithArray:[GRPeopleManager sharedManager].contactsWithMail];
    [sortedPeople sortUsingComparator:^NSComparisonResult(id obj1, id obj2)
     {
         NSString *first = ((GRContact *)obj1).firstname;
         NSString *second = ((GRContact *)obj2).firstname;
         
         if ([[NSScanner scannerWithString:first] scanInt:nil] || !first)
             return NSOrderedDescending;
         return [first compare:second];
     }];
    [self startSearchWithText:@""];
}

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
    }
    return self;
}


- (void)dealloc {
    self.otherSearchContacts = nil;
    self.searchPeople = nil;
    self.searchBar = nil;
    [contentTable release];
    [tableData release];
    [loadingView release];
    [sortedPeople release];
    [super dealloc];
}

#pragma mark - UITableViewDataSource

- (NSInteger) tableView: (UITableView *) tableView numberOfRowsInSection: (NSInteger) section
{
    NSInteger modul = 0;
    
    if (IS_IPAD)
    {
        modul = 2;
    }
    else
    {
        if (UIInterfaceOrientationIsLandscape([UIApplication sharedApplication].statusBarOrientation))
        {
            modul = 2;
        }
        else
        {
            modul = 1;
        }
    }
    
    
        if (([tableData count] % 2) == 0)
        {
            return [tableData count]/2;
        } else {
            return [tableData count]/2 + 1;
        }

//        return ([tableData count] + (modul - 1)) / modul;
}


- (UITableViewCell *) tableView: (UITableView *) tableView cellForRowAtIndexPath: (NSIndexPath *) indexPath {
	UITableViewCell * tableCell;
	
    NSString * CellIdentifier = nil;
    if ([self.searchBar.text length] == 0)
        CellIdentifier = [NSString stringWithFormat:@"%d%d", indexPath.section, indexPath.row];
	
	tableCell = [tableView dequeueReusableCellWithIdentifier: CellIdentifier];
	if (tableCell == nil)
    {
		tableCell = [[[UITableViewCell alloc] initWithStyle: UITableViewCellStyleDefault reuseIdentifier: CellIdentifier] autorelease];
        
        tableCell.selectionStyle = UITableViewCellSelectionStyleNone;
        CGFloat posX = 0.0f;
        
            if (displayIndex <  [tableData count])
            {
                GRContact *contact1 = [tableData objectAtIndex:displayIndex];
                GRMailContactView *cView = [[GRMailContactView alloc] initWithRecient:contact1 favorites:NO];
                cView.frame = CGRectMake(posX, 0, CONTACT_SIZE.width, CONTACT_SIZE.height);
                cView.tag = displayIndex;
                cView.delegate = self;
                [tableCell.contentView addSubview:cView];
                [cView release];
                
                displayIndex++;
                if (displayIndex <  [tableData count])
                {
                    posX = CONTACT_SIZE.width;
                    contact1 = [tableData objectAtIndex:displayIndex];
                    cView = [[GRMailContactView alloc] initWithRecient:contact1 favorites:NO];
                    cView.frame = CGRectMake(posX, 0, CONTACT_SIZE.width, CONTACT_SIZE.height);
                    cView.tag = displayIndex+1;
                    cView.delegate = self;
                    [tableCell.contentView addSubview:cView];
                    [cView release];
                }
                displayIndex++;
            }
	}
    NSLog(@"********** Cell index : %i", [indexPath row]);
    return tableCell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
	return 62.0f;
}

#pragma mark - UITableViewDelegate

- (void) tableView: (UITableView *) tableView didSelectRowAtIndexPath: (NSIndexPath *) indexPath {
}

#pragma mark - GRMailContactDelegate

- (void)mailContactSelectedWithContact:(GRContact *)contact {
    if (delegate) {
        if ([delegate respondsToSelector:@selector(mailContactSelectedWithContact:)]) {
            [delegate performSelector:@selector(mailContactSelectedWithContact:) withObject:contact];
        }
    }
}

#pragma mark - UITextFieldDelegate's methods

- (void)updateView
{
    displayIndex = 0;
    [self.searchPeople removeAllObjects];
    [self.searchPeople addObjectsFromArray:[self getContactsWithText:self.searchBar.text]];
    
    [self.otherSearchContacts removeAllObjects];
    for (GRContact *contact in searchPeople)
    {
        [self.otherSearchContacts addObject:contact];
    }
    
    [contentTable reloadData];
    
//    [searchBar becomeFirstResponder];
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    NSString *text = textField.text;
    if(string.length == 0) {
        text = [text substringToIndex:range.location];
    } else if (string.length == 1) {
        text = [text stringByAppendingFormat:@"%@", string];
    } else if (string.length > 1) {
        text = string;
    }
    [self startSearchWithText:text];
    return YES;
}

- (BOOL)textFieldShouldClear:(UITextField *)textField
{
    return YES;
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    displayIndex = 0;
    [textField resignFirstResponder];
    return YES;
}
-(void)textFieldDidEndEditing:(UITextField *)textField
{
    if (textField.text.length == 0) {
        [self startSearchWithText:@""];
    }
    displayIndex = 0;
}


- (void)startSearchWithText:(NSString *)text {
    displayIndex = 0;
    [tableData removeAllObjects];
    dispatch_queue_t myQueue = dispatch_queue_create("Search people queue",NULL);
    dispatch_async(myQueue, ^{
        
        if (text.length == 0) {
            [tableData addObjectsFromArray:sortedPeople];
        } else {
            for (GRContact *record in sortedPeople) {
                NSString *firstnameStr = @"";
                NSString *lastnameStr = @"";
                
                if (record.firstname)
                {
                    firstnameStr = record.firstname;
                }
                
                if (record.lastname)
                {
                    lastnameStr = record.lastname;
                }
                
                NSString *fullname = [NSString stringWithFormat:@"%@ %@", firstnameStr, lastnameStr];
                
                if ([fullname rangeOfString:text options:NSCaseInsensitiveSearch].location != NSNotFound)
                {
                    [tableData addObject:record];
                }

            }
        }
        dispatch_async(dispatch_get_main_queue(), ^{
            [contentTable reloadData];
        });
    });
        
}



- (void) cancelSearch
{
    displayIndex = 0;
    [self startSearchWithText:@""];
    [self.searchBar setText:@""];
    [self.searchBar resignFirstResponder];
}
@end
