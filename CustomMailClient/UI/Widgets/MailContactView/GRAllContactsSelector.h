//
//  GRAllContactsSelector.h
//  GrannyApp
//
//  Created by Ievgen Pavliuk on 4/30/13.
//
//

#import <UIKit/UIKit.h>
#import "GRMailContactView.h"

@interface GRAllContactsSelector : UIView <UITableViewDelegate, UITableViewDataSource, GRMailContactDelegate> {
    
    UITableView     *contentTable;
    NSMutableArray  *tableData;
    NSMutableArray  *sortedPeople;
    
    NSInteger        displayIndex;
    id               delegate;
    UIView          *loadingView;
    
    int rowsCount;
}
@property (nonatomic, retain) NSMutableArray *searchPeople;
@property (nonatomic, retain) NSMutableArray *otherSearchContacts;
@property (nonatomic, retain) IBOutlet UITextField *searchBar;
@property (nonatomic, retain) IBOutlet UITableView *contentTable;
@property (nonatomic, retain) IBOutlet UIView *loadingView;
@property (nonatomic, assign) id delegate;

@end
