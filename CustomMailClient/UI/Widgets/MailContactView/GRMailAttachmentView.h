//
//  GRMailAttachmentView.h
//  GrannyApp
//
//  Created by Ievgen Pavliuk on 7/25/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface GRMailAttachmentView : UIView

@property (nonatomic, retain) IBOutlet UIView *contentView;
@property (nonatomic, retain) IBOutlet UIImageView *imageView;
@property (nonatomic, retain) IBOutlet UIButton *saveBtn;

- (IBAction)closeBtnTapped:(id)sender;
- (IBAction)saveBtnTapped:(id)sender;

- (void)loadImage:(NSString *)filePath;
- (void)loadDocument:(NSString *)filePath;
@end
