//
//  GRMailRecipientView.h
//  GrannyApp
//
//  Created by Ievgen Pavliuk on 7/11/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "GRMailContactView.h"
#import "GRContact.h"
#import "GRAllContactsSelector.h"


typedef enum {
    GRMailContactViewTo = 1,
    GRMailContactViewBcc = 2,
} ContactViewType;


@protocol GRMailRecipientViewDelegate <NSObject>
- (void)mailRecipientSelectedWithContact:(NSMutableArray *)contacts;
- (void)mailBccRecipientSelectedWithContact:(NSMutableArray *)contacts;
@end

@interface GRMailRecipientView : UIView <GRMailContactDelegate, UITextFieldDelegate>

@property(nonatomic, assign) id<GRMailRecipientViewDelegate> delegate;
@property(nonatomic, assign) ContactViewType contactType;
@property (nonatomic, retain) NSMutableArray *selectedContacts;

- (IBAction)OkBtnTaped:(id)sender;
- (void) setBckViewImage;
- (void) createContactView;
@end
