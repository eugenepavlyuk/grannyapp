//
//  GRMailAttachmentPreviewCell.h
//  GrannyApp
//
//  Created by admin on 12/12/13.
//
//

#import <UIKit/UIKit.h>

@interface GRMailAttachmentPreviewCell : UICollectionViewCell

@property (nonatomic, retain) IBOutlet UIImageView *iconImage;

- (void)selected;
- (void)unselected;

@end
