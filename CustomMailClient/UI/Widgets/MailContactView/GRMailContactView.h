//
//  GRMainContactView.h
//  GrannyApp
//
//  Created by Ievgen Pavliuk on 5/30/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "GRContact.h"

@protocol GRMailContactDelegate <NSObject>

@optional
- (void)mailContactSelectedWithContact:(GRContact *)contact;
- (void)allContactSelectedWithIndex:(NSInteger)index;

@end

@interface GRMailContactView : UIView {
    NSInteger                   contactIndex;
    GRContact             *rContact;
    id <GRMailContactDelegate>  delegate;
    BOOL                        isFavorite;
    UILabel                    *lName; 
}
@property (nonatomic, assign) id <GRMailContactDelegate> delegate;
@property (nonatomic, assign) NSInteger contactIndex;
@property BOOL isGreen;

- (id)initWithRecient:(GRContact *)contact favorites:(BOOL)_isFavorites;
- (void)setSelected;
- (void)setUnSelected;
@end
