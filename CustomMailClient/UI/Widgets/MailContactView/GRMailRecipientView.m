//
//  GRMailRecipientView.m
//  GrannyApp
//
//  Created by Ievgen Pavliuk on 7/11/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "GRMailRecipientView.h"
#import "DataManager.h"
#import "GRPeopleManager.h"
#import "GRCommonAlertView.h"

#import "GRContactSearchBar.h"
#import "MBProgressHUD.h"
#import "GRPeopleManager.h"
#import "GRContactTableViewCell.h"
#import "GRFavoriteContactTableViewCell.h"
#import "GRContactView.h"

#define FAVORITES_TOTAL 43
#define FAVORITES_PADDING 5.f
#define FAVORITES_ITEMS_AT_ROW 5
#define FAVORITES_ITEM_WIDTH 130

#define ALLCONTACTS_WIDTH 300
#define ALLCONTACTS_HEIGHT 98
#define ALLCONTACTS_PADDING 3
#define ALLCONTACTS_ITEMS_AT_ROW 2
#define ALLCONTACTS_TOTAL 43

#define KEYBOARD_HEIGHT_LANDSCAPE_IPAD 352

@interface GRMailRecipientView() <GRContactCellDelegate>

@property (nonatomic, retain) IBOutlet UIScrollView *favoritesScrollView;
@property (nonatomic, retain) IBOutlet UIImageView *bckViewImage;
@property (nonatomic, retain) IBOutlet UIScrollView *selectedContactsScrollView;

@property (nonatomic, retain) IBOutlet UITableView *gridView;
@property (nonatomic, retain) IBOutlet GRContactSearchBar *searchBar;
@property (nonatomic, retain) IBOutlet UIActivityIndicatorView *spinnerView;
@property (retain, nonatomic) IBOutlet UIImageView *headerImageView;

@property (nonatomic, retain) NSArray *otherSearchContacts;
@property (nonatomic, retain) NSMutableArray *otherContacts;
@property (nonatomic, retain) NSMutableArray *favorites;
@property (nonatomic, assign) BOOL isSearchActive;

@property (nonatomic, retain) UITextField *emailTextField;

@property (nonatomic, retain) GRContact *selectedContact;
@property (nonatomic, assign) CGFloat xx;
@end

@implementation GRMailRecipientView

#pragma mark - initialization

- (id)initWithCoder:(NSCoder *)aDecoder {
    self = [super initWithCoder:aDecoder];
    if (self) {
        _isSearchActive = NO;
        
        self.otherSearchContacts = [NSMutableArray array];
        self.favorites = [NSMutableArray array];
        self.otherContacts = [NSMutableArray array];
        
        [[NSNotificationCenter defaultCenter] addObserver:self
                                                 selector:@selector(applicationDidBecomeActive:)
                                                     name:UIApplicationDidBecomeActiveNotification
                                                   object:nil];
        
        [[NSNotificationCenter defaultCenter] addObserver:self
                                                 selector:@selector(resyncingContactsNotification:)
                                                     name:kResyncingContactsNotification
                                                   object:nil];
        
        [[NSNotificationCenter defaultCenter] addObserver:self
                                                 selector:@selector(syncingContactsFinishedNotification:)
                                                     name:kSyncingContactsFinishedNotification
                                                   object:nil];
        
        [[NSNotificationCenter defaultCenter] addObserver:self
                                                 selector:@selector(contactSearchBarDidCancelNotification:)
                                                     name:GRContactSearchBarDidCancelNotification
                                                   object:nil];

    }
    return self;
}

- (void)applicationDidBecomeActive:(NSNotification*)notification
{
    [self.spinnerView startAnimating];
    
    [[GRPeopleManager sharedManager] updateContactsWithCompletionBlock:^{
        
        self.favorites = [[[GRPeopleManager sharedManager].favoriteContacts mutableCopy] autorelease];
        
        for (int i = 0; i < [self.favorites count]; )
        {
            GRContact *contact = [self.favorites objectAtIndex:i];
            
            NSString *contactName = contact.selectedEmail ? contact.selectedEmail : contact.email;
            
            if (![self validEmail:contactName])
            {
                [self.favorites removeObject:contact];
            }
            else
            {
                i++;
            }
        }
        
        self.otherContacts = [[[GRPeopleManager sharedManager].contactsWithMail mutableCopy] autorelease];
        
        NSSortDescriptor *favDescriptor = [[NSSortDescriptor alloc] initWithKey:@"favorite" ascending:NO];
        NSSortDescriptor *fullnameDescriptor = [[NSSortDescriptor alloc] initWithKey:@"fullContactName" ascending:YES selector:@selector(numbersLastComparing:)];
        
        NSArray *descriptors = [NSArray arrayWithObjects:favDescriptor, fullnameDescriptor, nil];
        
        [self.otherContacts sortUsingDescriptors:descriptors];
        
        [self reloadTableView];
    }];
}

- (void)resyncingContactsNotification:(NSNotification*)notification
{
    [self reloadTableView];
}

- (void)syncingContactsFinishedNotification:(NSNotification*)notification
{
    [self reloadTableView];
}

- (void)contactSearchBarDidCancelNotification:(NSNotification *)note
{
    _isSearchActive = NO;
    [_gridView reloadData];
}

- (void)awakeFromNib
{
    if (!IS_IPAD)
    {
        self.frame = CGRectMake(0, 0, self.frame.size.width, self.frame.size.height);
    }
    
    if ([[UIDevice currentDevice].systemVersion floatValue] >= 7.f)
    {
        [_gridView setTintColor:[UIColor blackColor]];
    }
    
    [_gridView reloadData];
    
    self.selectedContacts = [NSMutableArray array];
    
    _emailTextField = [[UITextField alloc] initWithFrame:CGRectMake(0, 0, _selectedContactsScrollView.frame.size.width, _selectedContactsScrollView.frame.size.height)];
    _emailTextField.delegate = self;
    _emailTextField.keyboardType = UIKeyboardTypeEmailAddress;
    _emailTextField.autocorrectionType = UITextAutocorrectionTypeNo;
    _emailTextField.font = [UIFont systemFontOfSize:28.0f];
    _emailTextField.contentVerticalAlignment = UIControlContentVerticalAlignmentCenter;
    [_selectedContactsScrollView addSubview:_emailTextField];
    
    UIImage *headerBackground = nil;
    
    if (IS_IPAD)
    {
        headerBackground = [[UIImage imageNamed:@"people_header_background"] resizableImageWithCapInsets:UIEdgeInsetsMake(0, 40, 0, 40)];
        

    }
    else
    {
        headerBackground = [[UIImage imageNamed:@"people_header_background"] stretchableImageWithLeftCapWidth:36 topCapHeight:0];
    }
    
    self.headerImageView.image = headerBackground;
    
    // Create the path (with only the top-left corner rounded)
    UIBezierPath *maskPath = [UIBezierPath bezierPathWithRoundedRect:self.headerImageView.bounds
                                                   byRoundingCorners:UIRectCornerTopLeft | UIRectCornerTopRight
                                                         cornerRadii:CGSizeMake(10.0, 10.0)];
    
    // Create the shape layer and set its path
    CAShapeLayer *maskLayer = [CAShapeLayer layer];
    maskLayer.frame = self.headerImageView.bounds;
    maskLayer.path = maskPath.CGPath;
    
    // Set the newly created shape layer as the mask for the image view's layer
    self.headerImageView.layer.mask = maskLayer;
}

- (void) setBckViewImage
{
    if (self.contactType == GRMailContactViewBcc)
        self.bckViewImage.image = [UIImage imageNamed:@"Mail_CC_Contact_Background.png"];
    else
        self.bckViewImage.image = nil;
}

- (void)willMoveToSuperview:(UIView *)newSuperview {
    MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self animated:YES];
    hud.userInteractionEnabled = NO;
    hud.labelText = NSLocalizedString(@"Sorting...", @"Sorting...");
    
    hud.mode = MBProgressHUDModeIndeterminate;
    
    [[GRPeopleManager sharedManager] updateContactsWithCompletionBlock:^{
        
        self.favorites = [[[GRPeopleManager sharedManager].favoriteContacts mutableCopy] autorelease];
        
        for (int i = 0; i < [self.favorites count]; )
        {
            GRContact *contact = [self.favorites objectAtIndex:i];
            
            NSString *contactName = contact.selectedEmail ? contact.selectedEmail : contact.email;
            
            if (![self validEmail:contactName])
            {
                [self.favorites removeObject:contact];
            }
            else
            {
                i++;
            }
        }
        
        self.otherContacts = [[[GRPeopleManager sharedManager].contactsWithMail mutableCopy] autorelease];
        
        NSSortDescriptor *favDescriptor = [[NSSortDescriptor alloc] initWithKey:@"favorite" ascending:NO];
        NSSortDescriptor *fullnameDescriptor = [[NSSortDescriptor alloc] initWithKey:@"fullContactName" ascending:YES selector:@selector(numbersLastComparing:)];
        
        NSArray *descriptors = [NSArray arrayWithObjects:favDescriptor, fullnameDescriptor, nil];
        
        [self.otherContacts sortUsingDescriptors:descriptors];
        
        [self.gridView reloadData];
        
        [MBProgressHUD hideHUDForView:self animated:YES];
    }];
}

#pragma mark - GRMailContactDelegate
- (void) deleteContact:(UIButton *) button
{
    [_selectedContacts removeObjectAtIndex:button.tag];
    for (UIView *b in _selectedContactsScrollView.subviews)
    {
        if ([b isKindOfClass:[UIButton class]])
            [b removeFromSuperview];
    }
    [self createContactView];
}

- (void) createContactView
{
    _xx = 0;
    for (int i = 0; i < _selectedContacts.count; i++)
    {
        GRContact *contact = [_selectedContacts objectAtIndex:i];
        
        UIImage *image = [UIImage imageNamed:@"Mail_ContactIconBckg.png"];
        
        UIButton *selectedView = [UIButton buttonWithType:UIButtonTypeCustom];
        if (IS_IPAD)
            selectedView.frame = CGRectMake(_xx, (int)((_selectedContactsScrollView.frame.size.height - image.size.height)/2), 400, image.size.height);
        else
            selectedView.frame = CGRectMake(_xx, (int)((_selectedContactsScrollView.frame.size.height - image.size.height)/2), 200, image.size.height);
        _xx = selectedView.frame.origin.x + selectedView.frame.size.width;
        selectedView.tag = i;
        [selectedView addTarget:self action:@selector(deleteContact:) forControlEvents:UIControlEventTouchUpInside];
        [_selectedContactsScrollView addSubview:selectedView];
        
        _selectedContactsScrollView.contentSize = CGSizeMake(_xx, _selectedContactsScrollView.frame.size.height);
        
        UIImageView *selectedContactImage = [[[UIImageView alloc] init] autorelease];
        selectedContactImage.autoresizesSubviews = YES;
        selectedContactImage.contentMode = UIViewContentModeScaleToFill;
        if (contact.photourl)
        {
            NSData *imageData = [NSData dataWithContentsOfFile:contact.photourl];
            UIImage *image = [UIImage imageWithData:imageData];
            selectedContactImage.layer.masksToBounds = YES;
            if (IS_IPAD)
            {
                [selectedContactImage.layer setCornerRadius:15.0f];
            }
            else
            {
                [selectedContactImage.layer setCornerRadius:5.0f];
            }
            [selectedContactImage setImage:image];
        }
        else
            [selectedContactImage setImage:image];
        
        if (IS_IPAD)
            selectedContactImage.frame = CGRectMake(0, (int)((selectedView.frame.size.height - 70)/2), 70, 70);
        else
            selectedContactImage.frame = CGRectMake(0, (int)((selectedView.frame.size.height - 49)/2), 49, 49);
        [selectedView addSubview:selectedContactImage];
        
        if (!contact.photourl)
        {
            
            UILabel *selectedContactName = [[[UILabel alloc] init] autorelease];
            selectedContactName.text =
                [NSString stringWithFormat:@"%@ %@",
                 contact.firstname? contact.firstname : @"",
                 contact.lastname? contact.lastname : @""];
            selectedContactName.textAlignment = NSTextAlignmentCenter;
            selectedContactName.lineBreakMode = NSLineBreakByTruncatingTail;
            selectedContactName.numberOfLines = 2;
            selectedContactName.font = [UIFont boldSystemFontOfSize: IS_IPAD ? 14 : 10];
            selectedContactName.textColor = [UIColor lightGrayColor];
            selectedContactName.backgroundColor = [UIColor clearColor];
            selectedContactName.frame = CGRectMake(0, 0, selectedContactImage.frame.size.width, selectedContactImage.frame.size.height);
            [selectedContactImage addSubview:selectedContactName];

        }
        
        UILabel *selectedContactMale = [[[UILabel alloc] init] autorelease];
        selectedContactMale.textAlignment = NSTextAlignmentLeft;
        selectedContactMale.lineBreakMode = NSLineBreakByTruncatingTail;
        selectedContactMale.numberOfLines = 1;
        selectedContactMale.font = [UIFont boldSystemFontOfSize: IS_IPAD ? 28 : 14];
        selectedContactMale.textColor = [UIColor blackColor];
        selectedContactMale.backgroundColor = [UIColor clearColor];
        
        if (IS_IPAD)
            selectedContactMale.frame = CGRectMake(selectedContactImage.frame.origin.x + selectedContactImage.frame.size.width + 5, (int)((selectedView.frame.size.height - 36)/2), selectedView.frame.size.width - selectedContactImage.frame.size.width - 5, 36);
        else
            selectedContactMale.frame = CGRectMake(selectedContactImage.frame.origin.x + selectedContactImage.frame.size.width + 5, (int)((selectedView.frame.size.height - 20)/2), selectedView.frame.size.width - selectedContactImage.frame.size.width - 5, 20);
        [selectedView addSubview:selectedContactMale];
        NSString *contactMail = contact.selectedEmail ? contact.selectedEmail:contact.email;

        if (!contactMail || [contactMail length] == 0)
        {
            selectedContactMale.text = NSLocalizedString(@"No Emails", @"No Emails");
        }
        else
        {
            selectedContactMale.text = contactMail;
        }
    }
    _emailTextField.frame = CGRectMake(_xx, 0, _selectedContactsScrollView.frame.size.width, _selectedContactsScrollView.frame.size.height);
    _selectedContactsScrollView.contentSize = CGSizeMake(_emailTextField.frame.origin.x + _emailTextField.frame.size.width, _selectedContactsScrollView.frame.size.height);
}

#pragma mark - Actions methods

- (IBAction)OkBtnTaped:(id)sender
{
    [self endEditing:YES];
    GRCommonAlertView *alert = nil;
    switch (self.contactType)
    {
        case GRMailContactViewTo:
            if ([_delegate respondsToSelector:@selector(mailRecipientSelectedWithContact:)])
            {
                if (_emailTextField.text.length > 0)
                {
                    if ([self validEmail:_emailTextField.text])
                    {
                        GRContact *contact;
                        
                        contact = [[GRPeopleManager sharedManager] findContactByEmail:_emailTextField.text];
                        if (!contact) {
                            contact = [[[GRContact alloc] init] autorelease];
                            contact.email = _emailTextField.text;
                            contact.recId = [NSNumber numberWithInt:987];
                        }
                        [_selectedContacts addObject:contact];
                        [_delegate mailRecipientSelectedWithContact:_selectedContacts];
                    }
                    else
                    {
                        alert = [[[GRCommonAlertView alloc] initCommonAlert:NSLocalizedString(@"Please enter a valid email address", @"Please enter a valid email address")  inView:self] autorelease];
                        alert.delegate = nil;
                    }
                }
                else
                    [_delegate mailRecipientSelectedWithContact:_selectedContacts];
            }
            break;
        case GRMailContactViewBcc:
            if ([_delegate respondsToSelector:@selector(mailBccRecipientSelectedWithContact:)])
            {
                if (_emailTextField.text.length > 0)
                {
                    if ([self validEmail:_emailTextField.text])
                    {
                        GRContact *contact = [[[GRContact alloc] init] autorelease];
                        contact.email = _emailTextField.text;
                        contact.recId = [NSNumber numberWithInt:987];
                        [_selectedContacts addObject:contact];
                        
                        [_delegate mailBccRecipientSelectedWithContact:_selectedContacts];
                    }
                    else
                    {
                        alert = [[[GRCommonAlertView alloc] initCommonAlert:NSLocalizedString(@"Please enter a valid email address", @"Please enter a valid email address") inView:self] autorelease];
                        alert.delegate = nil;
                    }
                }
                else
                    [_delegate mailBccRecipientSelectedWithContact:_selectedContacts];
            }
            break;
            
        default:
            break;
    }
    
    if (!alert)
        [self removeFromSuperview];
}

- (BOOL) validEmail:(NSString *)email
{
    NSString *strEmailMatchstring = @"\\b([a-zA-Z0-9%_.+\\-]+)@([a-zA-Z0-9.\\-]+?\\.[a-zA-Z]{2,6})\\b";
    NSPredicate *regExPredicate = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", strEmailMatchstring];
    return [regExPredicate evaluateWithObject:email];
}

// ============================= added ============================

- (void)reloadTableView
{
    [self.spinnerView stopAnimating];
    [self.gridView reloadData];
}

- (void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
    
    self.bckViewImage = nil;
    self.selectedContacts = nil;
    self.otherSearchContacts = nil;
    self.otherContacts = nil;
    self.favorites = nil;
    
    _delegate = nil;
    [_favoritesScrollView release];
    [_selectedContact release];
    
    self.gridView = nil;
    self.searchBar = nil;
    
    self.spinnerView = nil;
    
    [_headerImageView release];
    [_emailTextField release];
    [super dealloc];
}

- (NSArray*)getContactsWithText:(NSString*)text
{
    NSMutableArray *array = [NSMutableArray array];
    
    NSInteger count = [[[GRPeopleManager sharedManager] contactsWithMail] count];
    
    for (int i = 0; i < count; i++)
    {
        GRContact *record = [[[GRPeopleManager sharedManager] contactsWithMail] objectAtIndex:i];
        
        NSString *firstnameStr = @"";
        NSString *lastnameStr = @"";
        
        if (record.firstname)
        {
            firstnameStr = record.firstname;
        }
        
        if (record.lastname)
        {
            lastnameStr = record.lastname;
        }
        
        NSString *fullname = [NSString stringWithFormat:@"%@ %@", firstnameStr, lastnameStr];
        
        if ([fullname rangeOfString:text options:NSCaseInsensitiveSearch].location != NSNotFound)
        {
            [array addObject:record];
        }
    }
    
    return array;
}

#pragma mark - UITextFieldDelegate's methods

- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField
{
    if (textField == self.searchBar) {
        if (!_isSearchActive)
        {
            [self performSelector:@selector(updateView) withObject:nil afterDelay:0.1f];
        }
    }
    
    return YES;
}

- (void)textFieldDidBeginEditing:(UITextField *)textField
{
    if (textField == self.searchBar) {
        _isSearchActive = YES;
    }
}

- (void)updateView
{
    self.otherSearchContacts = [self getContactsWithText:self.searchBar.text];
    
    [_gridView reloadData];
    [_searchBar becomeFirstResponder];
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    if (textField == self.searchBar) {
        [self performSelector:@selector(updateView) withObject:nil afterDelay:0.1f];
    }
    
    return YES;
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    if (textField == self.searchBar) {
        [textField resignFirstResponder];
        _isSearchActive = NO;
    }
    
    return YES;
}

#pragma mark - UITableViewDataSource's methods

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 2;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    NSInteger modul = [self modulForSection:section];
    
    if (section == 0)
    {
        if (_isSearchActive)
        {
            return 0;//([self.favoriteSearchContacts count] + 1 + (modul - 1)) / modul;
        }
        else
        {
            //return ([[GRPeopleManager sharedManager].favoriteContacts count] + 1 + (modul - 1)) / modul;
            if (!self.favorites.count) {
                return 0;
            }
            return ([self.favorites count] + (modul - 1)) / modul;
        }
    }
    else
    {
        if (_isSearchActive)
        {
            return ([self.otherSearchContacts count] + (modul - 1)) / modul;
        }
        else
        {
            return ([self.otherContacts count] + (modul - 1)) / modul;
        }
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 0;
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
    if (section == 0)
    {
        return 105;
    }
    
    return 0;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    return nil;
}

- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section
{
    if (section == 0)
    {
        NSInteger height = 105;
        
        UIView *footerView = [[[UIView alloc] initWithFrame:CGRectMake(0, 0, tableView.bounds.size.width, height)] autorelease];
        
        footerView.backgroundColor = [UIColor clearColor];
        
        [footerView addSubview:self.searchBar];
        
        self.searchBar.center = CGPointMake((int)footerView.bounds.size.width / 2, (int)footerView.bounds.size.height / 2);
        
        return footerView;
    }
    
    return nil;
}

- (UITableViewCell *)tableView:(UITableView *)tableView
         cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    GRContactTableViewCell *contactTableViewCell = nil;
    
    NSInteger number = 0;
    NSInteger modul = [self modulForSection:indexPath.section];
    
    if (indexPath.section == 0)
    {
        contactTableViewCell = [tableView dequeueReusableCellWithIdentifier:[GRFavoriteContactTableViewCell reuseIdentifier]];
        
        if (!contactTableViewCell)
        {
            contactTableViewCell = [[[NSBundle mainBundle] loadNibNamed:@"GRFavoriteContactTableViewCell" owner:nil options:nil] lastObject];
        }
        number = [self favoritesContactsCount];//[[GRPeopleManager sharedManager].favoriteContacts count] + 1;
    }
    else
    {
        contactTableViewCell = [tableView dequeueReusableCellWithIdentifier:[GRContactTableViewCell reuseIdentifier]];
        
        if (!contactTableViewCell)
        {
            contactTableViewCell = [[[NSBundle mainBundle] loadNibNamed:@"GRContactTableViewCell" owner:nil options:nil] lastObject];
            contactTableViewCell.backgroundColor = [UIColor clearColor];
        }
        
        if (_isSearchActive)
        {
            number = [self.otherSearchContacts count];
        }
        else
        {
            number = [self.otherContacts count];
        }
    }
    
    if (number - (indexPath.row + 1) * modul >= 0)
    {
        number = modul;
    }
    else
    {
        number = number % modul;
    }
    
    contactTableViewCell.indexPath = indexPath;
    
    [contactTableViewCell.contentView.subviews makeObjectsPerformSelector:@selector(removeFromSuperview)];
    
    contactTableViewCell.delegate = self;
    
    contactTableViewCell.numberOfChildren = number;
    
    [contactTableViewCell layoutContent];
    
    return contactTableViewCell;
}

#pragma mark - UITableViewDelegate's methods

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.section == 0)
    {
        return [GRFavoriteContactTableViewCell cellHeight];
    }
    
    return [GRContactTableViewCell cellHeight];
}

#pragma mark - GRContactCellDelegate's methods

- (UIView*)viewForContactAtIndexPath:(NSIndexPath*)indexPath withIndex:(NSNumber*)index
{
    NSInteger i = [index intValue];
    
    NSInteger objectIndex = 0;
    
    GRContact *contact = nil;
    
    GRContactView *contactView = nil;
    
    NSInteger modul = [self modulForSection:indexPath.section];
    
    if (indexPath.section == 0)
    {
        objectIndex = indexPath.row * modul + i;
        
        CGSize size = CGSizeMake(150, 150);
        
        contactView = [[[NSBundle mainBundle] loadNibNamed:@"GRFavoriteContactView" owner:nil options:nil] lastObject];
        
        contactView.frame = CGRectMake(20 + i * size.width + i * 20, 0, size.width, size.height);
        
        if ([self.favorites count] > objectIndex)
        {
            contact = [self.favorites objectAtIndex:objectIndex];
            
            UIImage *image = [UIImage imageWithContentsOfFile:contact.photourl];
            
            contactView.coverButton.userInteractionEnabled = YES;
            
            if (!image)
            {
                contactView.titleLabel.hidden = NO;
                contactView.coverImageView.image = nil;
                [contactView.coverButton setImage:nil forState:UIControlStateNormal];
            }
            else
            {
                contactView.titleLabel.hidden = YES;
                contactView.coverImageView.image = image;
                [contactView.coverButton setImage:nil forState:UIControlStateNormal];
            }
        }
    }
    else
    {
        objectIndex = indexPath.row * modul + i;
        
        CGSize size = CGSizeMake(self.gridView.bounds.size.width / modul, [GRContactTableViewCell cellHeight]);
        
        contactView = [[[NSBundle mainBundle] loadNibNamed:@"GRContactContainerView" owner:nil options:nil] lastObject];
        
        contactView.frame = CGRectMake(i * size.width, 0, size.width, size.height);
        
        if (indexPath.row == 0)
        {
            contactView.topSeparatorView.hidden = NO;
        }
        else
        {
            contactView.topSeparatorView.hidden = YES;
        }
        
        if (_isSearchActive)
        {
            contact = [self.otherSearchContacts objectAtIndex:objectIndex];
        }
        else
        {
            contact = [self.otherContacts objectAtIndex:objectIndex];
        }
        
        contactView.coverImageView.image = nil;
        
        contactView.titleLabel.hidden = NO;
    }
    
    NSMutableString *name = [NSMutableString string];
    
    if ([contact.firstname length])
    {
        [name appendString:contact.firstname];
    }
    
    if ([contact.lastname length])
    {
        if ([name length])
        {
            [name appendString:@" "];
        }
        
        [name appendString:contact.lastname];
    }
    
    contactView.titleLabel.text = name;
    
    return contactView;
}

- (void)willSelectView:(GRContactContainerView *)contactView atIndexPath:(NSIndexPath *)indexPath andIndex:(NSNumber *)index {
    
    if (indexPath.section != 0) {
        
        contactView.titleLabel.textColor = [UIColor whiteColor];
        contactView.coverImageView.image = [[UIImage imageNamed:@"people_selected_contact_background"] resizableImageWithCapInsets:UIEdgeInsetsMake(0, 20, 0, 20)];
        
        double delayInSeconds = 0.2;
        dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, (int64_t)(delayInSeconds * NSEC_PER_SEC));
        dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
            contactView.titleLabel.textColor = RGBCOLOR(128, 128, 128);
            contactView.coverImageView.image = nil;
        });
    }
}

- (void)contactTappedWithIndexPath:(NSIndexPath*)indexPath andIndex:(NSNumber*)index
{
    NSInteger i = [index intValue];
    
    NSInteger objectIndex = 0;
    
    GRContact *contact = nil;
    
    NSInteger modul = [self modulForSection:indexPath.section];
    
    if (indexPath.section == 0)
    {
        objectIndex = indexPath.row * modul + i;
        
        if ([self.favorites count] > objectIndex)
        {
            contact = [self.favorites objectAtIndex:objectIndex];
        }
        else
        {
            contact = nil;
        }
    }
    else
    {
        objectIndex = indexPath.row * modul + i;
        
        if (_isSearchActive)
        {
            contact = [self.otherSearchContacts objectAtIndex:objectIndex];
        }
        else
        {
            contact = [self.otherContacts objectAtIndex:objectIndex];
        }
    }
    
    BOOL isExist = NO;
    if (_selectedContacts && _selectedContacts.count > 0)
    {
        for (GRContact *c in _selectedContacts)
        {
            if ([c.recId isEqualToNumber:contact.recId])
            {
                isExist = YES;
                break;
            }
        }
    }
    
    if (!isExist)
    {
        [_selectedContacts addObject:contact];
        
        for (UIView *b in _selectedContactsScrollView.subviews)
        {
            if ([b isKindOfClass:[UIButton class]])
                [b removeFromSuperview];
        }
        [self createContactView];
        
        if (_selectedContact)
        {
            [_selectedContact release];
            _selectedContact = nil;
        }
        _selectedContact = [contact retain];
    }
}

#pragma mark - Helper Methods
- (NSUInteger)modulForSection:(NSUInteger)section {
    if (section == 0) {
        return 4;
    }
    
    if (!IS_IPAD && (UIInterfaceOrientationIsPortrait([UIApplication sharedApplication].statusBarOrientation))) {
        return 1;
    }
    
    return 2;
}

- (NSUInteger)favoritesContactsCount {
    return self.favorites.count;
}

@end
