//
//  GRMailAddAttachmentView.h
//  GrannyApp
//
//  Created by Ievgen Pavliuk on 7/13/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "GRPhotoPreview.h"

@protocol GRMailAddAttachmentViewDelegate <NSObject>

- (void)mailAttachemntsSelectedWithImages:(NSArray *)assets;

@end

@interface GRMailAddAttachmentView : UIView

@property (nonatomic, retain) IBOutlet UIView *headerView;
@property (nonatomic, retain) IBOutlet UIScrollView *photosScrollView;
@property (nonatomic, retain) IBOutlet UICollectionView *collectionView;

@property (nonatomic, assign) id<GRMailAddAttachmentViewDelegate> delegate;

- (IBAction)okBtnTapped:(id)sender;
- (IBAction)exitTapped:(id)sender;

@end
