//
//  GRMailAddAttachmentView.m
//  GrannyApp
//
//  Created by Ievgen Pavliuk on 7/13/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "GRMailAddAttachmentView.h"

#import "GRMailAttachmentPreviewCell.h"

static NSString *const cellIdentifier = @"AttachmentPreviewCell";

@interface GRMailAddAttachmentView () <UICollectionViewDataSource, UICollectionViewDelegate>
@property (nonatomic, retain) NSMutableArray *assets;
@property (nonatomic, retain) NSMutableArray *selectedImages;
@property (nonatomic, retain) ALAssetsLibrary *library;
@end

@implementation GRMailAddAttachmentView

- (void)awakeFromNib 
{
    self.frame = CGRectMake(62, 46, 900, 675);
    self.headerView.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"email_msg_header_bkg"]];
    [self.collectionView registerNib:[UINib nibWithNibName:@"GRMailAttachmentPreviewCell" bundle:nil] forCellWithReuseIdentifier:cellIdentifier];
    self.collectionView.allowsMultipleSelection = YES;
    
    self.assets = [[[NSMutableArray alloc] init] autorelease];
    self.selectedImages = [[[NSMutableArray alloc] initWithCapacity:5] autorelease];
}

- (void)didMoveToSuperview {
    [self startLoadPhotosFromGallery];
}

- (void)dealloc {
    
    _delegate = nil;
    [_photosScrollView release];
    [_assets release];
    [_selectedImages release];
    [_headerView release];
    
    [super dealloc];
}

#pragma mark - Layout views

- (void)startLoadPhotosFromGallery {
    void (^assetEnumerator)(ALAsset *, NSUInteger, BOOL *) = ^(ALAsset *result, NSUInteger index, BOOL *stop) {
        if(result != NULL) {
            [_assets addObject:result];
        }
    };
    
    void (^assetGroupEnumerator)(ALAssetsGroup *, BOOL *) =  ^(ALAssetsGroup *group, BOOL *stop) {
    	if(group != nil) {
            [group enumerateAssetsUsingBlock:assetEnumerator];
        }
        else
        {
            [_collectionView reloadData];
        }
    };
    
    self.assets = [[[NSMutableArray alloc] init] autorelease];
    self.library = [[[ALAssetsLibrary alloc] init] autorelease];
    
    [_library enumerateGroupsWithTypes:ALAssetsGroupSavedPhotos
                            usingBlock:assetGroupEnumerator
                          failureBlock: ^(NSError *error) {
                              NSLog(@"Failure");
                          }];
}

#pragma mark - Actions
- (IBAction)okBtnTapped:(id)sender
{
    if ([_delegate respondsToSelector:@selector(mailAttachemntsSelectedWithImages:)])
    {
        [_delegate mailAttachemntsSelectedWithImages:_selectedImages];
    }
    [self removeFromSuperview];
}

- (IBAction)exitTapped:(id)sender {
    [self removeFromSuperview];
}

#pragma mark - UICollectionView Data Source Methods
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return _assets.count;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    
    GRMailAttachmentPreviewCell *collectionCell =
    [collectionView dequeueReusableCellWithReuseIdentifier:cellIdentifier
                                              forIndexPath:indexPath];
    
    ALAsset *item = [_assets objectAtIndex:indexPath.row];
    collectionCell.iconImage.image = [UIImage imageWithCGImage:item.thumbnail];
    
    return collectionCell;
}

#pragma mark - UICollectionView Delegate Methods
- (void)collectionView:(UICollectionView *)collectionView didHighlightItemAtIndexPath:(NSIndexPath *)indexPath
{
    GRMailAttachmentPreviewCell *cell = (GRMailAttachmentPreviewCell *)[collectionView cellForItemAtIndexPath:indexPath];
    cell.contentView.alpha = 0.6f;
}

- (void)collectionView:(UICollectionView *)collectionView didUnhighlightItemAtIndexPath:(NSIndexPath *)indexPath {
    GRMailAttachmentPreviewCell *cell = (GRMailAttachmentPreviewCell *)[collectionView cellForItemAtIndexPath:indexPath];
    cell.contentView.alpha = 1.0f;
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    GRMailAttachmentPreviewCell *cell = (GRMailAttachmentPreviewCell *)[collectionView cellForItemAtIndexPath:indexPath];
    [cell selected];
    [_selectedImages addObject:[_assets objectAtIndex:indexPath.row]];
}

- (void)collectionView:(UICollectionView *)collectionView didDeselectItemAtIndexPath:(NSIndexPath *)indexPath {
    GRMailAttachmentPreviewCell *cell = (GRMailAttachmentPreviewCell *)[collectionView cellForItemAtIndexPath:indexPath];
    [cell unselected];
    [_selectedImages removeObject:[_assets objectAtIndex:indexPath.row]];
}

@end
