//
//  GRMailAttachmentPreviewCell.m
//  GrannyApp
//
//  Created by admin on 12/12/13.
//
//

#import "GRMailAttachmentPreviewCell.h"

@implementation GRMailAttachmentPreviewCell

- (void)dealloc {
    
    [_iconImage release];
    
    [super dealloc];
}

- (void)awakeFromNib {
    [super awakeFromNib];
    
    [self unselected];
}

- (void)unselected {
    self.layer.borderWidth = 1.0f;
    self.layer.borderColor = [UIColor lightGrayColor].CGColor;
    self.layer.masksToBounds = YES;
    self.layer.cornerRadius = 25.0f;
}

- (void)selected {
    self.layer.borderWidth = 10.0f;
    self.layer.borderColor = rgb(0xff1d25).CGColor;
    self.layer.masksToBounds = YES;
    self.layer.cornerRadius = 25.0f;
}

@end
