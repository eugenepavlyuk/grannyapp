//
//  GRResource.m
//  GrannyApp
//
//  Created by Ievgen Pavliuk on 4/13/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "GRResource.h"

@implementation GRResource

+ (UITableViewCell *)tableCellFromNib:(NSString *)nibName owner:(id)owner cellId:(NSString *)cellId {
    NSArray* topLevelObjs = [[NSBundle mainBundle] loadNibNamed:nibName owner:owner options:nil];
	if ( topLevelObjs == nil ) {
		NSLog(@"Error! Could not load %@ file.\n", nibName);
		return nil;
	}	
    for ( id<NSObject> nibObject in topLevelObjs ) {
        if ( [nibObject isKindOfClass:[UITableViewCell class]] ) {
            UITableViewCell *cell = (UITableViewCell *)nibObject;
            if ( NSOrderedSame == [cell.reuseIdentifier compare:cellId] ) {
                return cell;
            }
        }
    }
	return nil;		    
}

+ (UIView *)viewFromNib:(NSString *)nibName owner:(id)owner {
    NSArray* topLevelObjs = [[NSBundle mainBundle] loadNibNamed:nibName owner:owner options:nil];
	if ( topLevelObjs == nil ) {
		NSLog(@"Error! Could not load %@ file.\n", nibName);
		return nil;
	} else {
        return  (UIView *)[topLevelObjs objectAtIndex:0];
    }
}

@end
