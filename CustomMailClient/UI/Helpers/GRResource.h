//
//  GRResource.h
//  GrannyApp
//
//  Created by Ievgen Pavliuk on 4/13/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface GRResource : NSObject {
    
}
+ (UITableViewCell *)tableCellFromNib:(NSString *)nibName owner:(id)owner cellId:(NSString *)cellId;
+ (UIView *)viewFromNib:(NSString *)nibName owner:(id)owner;

@end
