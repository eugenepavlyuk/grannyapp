//
//  GRMainWindow.h
//  Granny
//
//  Created by Ievgen Pavliuk on 3/27/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "GRSpinerView.h"
#import "GRSettingsiPadController.h"

@interface GRMainWindow : UIWindow {
    
    UINavigationController *navController;
    GRSpinerView           *spinerView;
    GRSettingsiPadController *settingsController;
}
@property (nonatomic, retain) UINavigationController *navController;

- (void)displayErrorMessageWithString:(NSString *)error;
- (void)displayInfoMessageWithString:(NSString *) msg;

- (void)unBlockUI;
- (void)blockUIWithMessage:(NSString *)message isBlack:(BOOL)isBlack;

- (void)onMainController;
- (void)onSettingsSplitController;
@end
