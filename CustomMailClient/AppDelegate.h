//
//  AppDelegate.h
//  CustomMailClient
//
//  Created by Ievgen Pavliuk on 4/10/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <AVFoundation/AVFoundation.h>
#import <AudioToolbox/AudioToolbox.h>
#import "GRMainWindow.h"
#import "GRResource.h"
#import "GRCommonAlertView.h"
#import "YLReachability_.h"
//#import "GRRadioChannelsLoader.h"


@interface AppDelegate : UIResponder <UIApplicationDelegate, GRCommonAlertDelegate>
{    
	NSMutableArray	*myMessages;
    BOOL connectionLost;
    YLReachability* internetReach;
}

@property (strong, nonatomic) GRMainWindow *window;
@property (nonatomic, strong) NSCalendar *globalCalendar;

- (void)initialyzeAudioBackground;

+ (AppDelegate *)getInstance;

- (void) displayAlertWithMessage:(NSString *)message;

@end
