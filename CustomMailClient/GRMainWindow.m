//
//  GRMainWindow.m
//  Granny
//
//  Created by Ievgen Pavliuk on 3/27/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "GRMainWindow.h"
#import "GRMainController.h"
#import "GRResource.h"
#import "GRSpinerView.h"
#import "GRCommonAlertView.h"
#import "GRMainNavigationController.h"

@implementation GRMainWindow
@synthesize navController;

- (id)initWithFrame:(CGRect)frame
{
    if ((self = [super initWithFrame:frame]))
    {
        GRMainController *mainController = [[GRMainController alloc] init];
        navController = [[GRMainNavigationController alloc] initWithRootViewController:mainController];
        self.rootViewController = navController;
        [mainController release];
        
        [self addSubview:navController.view];
        
        spinerView = [(GRSpinerView *)[GRResource viewFromNib:@"GRSpinerView" owner:nil] retain];
        
        spinerView.frame = CGRectMake(0, 20, spinerView.frame.size.width, spinerView.frame.size.height);
        spinerView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
        
    }
    return self;
}

#pragma mark - Messaging

- (void)displayErrorMessageWithString:(NSString *)error {
    GRCommonAlertView *alert = [[[GRCommonAlertView alloc] initCommonAlert:error inView:self.navController.topViewController.view] autorelease];
    alert.delegate = nil;
//   	UIAlertView *alert = [[UIAlertView alloc] initWithTitle: NSLocalizedString(@"Error!", @"Error!")
//													message: error
//												   delegate: self
//										  cancelButtonTitle: NSLocalizedString(@"OK", @"OK")
//										  otherButtonTitles: nil];
//	[alert show];
//	[alert release];
}

- (void)displayInfoMessageWithString:(NSString *) msg {
    GRCommonAlertView *alert = [[[GRCommonAlertView alloc] initCommonAlert:msg inView:self.navController.topViewController.view] autorelease];
    alert.delegate = nil;
//   	UIAlertView *alert = [[UIAlertView alloc] initWithTitle: NSLocalizedString(@"Info!", @"Info!")
//													message: msg
//												   delegate: self
//										  cancelButtonTitle: NSLocalizedString(@"OK", @"OK")
//										  otherButtonTitles: nil];
//	[alert show];
//	[alert release];
}



#pragma mark -
#pragma mark UIHelpers

- (void)blockUIWithMessage:(NSString *)message isBlack:(BOOL)isBlack {
    CGFloat superWidth;
    CGFloat superHeight;
    UIInterfaceOrientation orientation = [[UIApplication sharedApplication] statusBarOrientation];
    if(orientation == UIInterfaceOrientationLandscapeLeft ||orientation == UIInterfaceOrientationLandscapeRight) {
        if (IS_IPAD) {
            superWidth = 1024.0f;
            superHeight = 768.0f;
        } else {
            superWidth = 480.0f;
            superHeight = 300.0f;
        }
    } else {
        if (IS_IPAD) {
            superWidth = 768.0f;
            superHeight = 1024.0f;
        } else {
            superWidth = 320.0f;
            superHeight = 460.0f;
        }
    }
    spinerView.frame = CGRectMake(0, 20, superWidth, superHeight);
    spinerView.infoLabel.text = message;
    UIViewController *modalController = nil;
    if ([self.rootViewController isKindOfClass:UINavigationController.class]) {
        for (UIViewController *vc in [(UINavigationController *)self.rootViewController viewControllers]) {
            if (vc.presentedViewController) {
                modalController = vc.presentedViewController;
                break;
            }
        }
        
        if (modalController) {
            [modalController.view addSubview:spinerView];
            spinerView.frame = CGRectMake(0, 0, superWidth, superHeight);
        } else {
            [self.rootViewController.view addSubview:spinerView];
        }
    } else {
        spinerView.frame = CGRectMake(0, 0, superWidth, superHeight+20);
        [self.rootViewController.view addSubview:spinerView];
    }
}

- (void)unBlockUI {
    [spinerView removeFromSuperview];
}

- (void)dealloc {
    [navController release];
    [spinerView release];
    [settingsController release];
    [super dealloc];
}

#pragma mark - Settings Split Controller


- (void)onSettingsSplitController
{
    if (!settingsController)
    {
        settingsController = [[GRSettingsiPadController alloc] initWithNibName:@"GRSettingsiPadController" bundle:nil];
    }
    
    self.rootViewController = settingsController;
}

- (void)onMainController {
    if (settingsController) {
        [settingsController release];
        settingsController = nil;
    }
    self.rootViewController = self.navController;
}

@end
