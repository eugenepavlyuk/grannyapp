//
//  GRMailServiceManager.m
//  GrannyApp
//
//  Created by Ievgen Pavliuk on 4/12/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "GRMailServiceManager.h"
#import "DataManager.h"
#import "GRMailSettingsNewAccountController.h"
#import "GRReadMailOperation.h"
#import "GRMailFetchOperation.h"
#import "GRMailSentOperation.h"
#import "GRMailMoveOperation.h"
#import "GRMailAppendOperation.h"
#import <AVFoundation/AVFoundation.h>

#define FOLDER_INBOX  0
#define FOLDER_DRAFTS 1
#define FOLDER_SENT   2
#define FOLDER_TRASH  3

@interface GRMailServiceManager ()
@property (nonatomic, retain) AVAudioPlayer *avAudioPlayer;
@end

@implementation GRMailServiceManager
@synthesize delegate, queue, currentFolder, currentOperation, sendingMessagesQueue, imapSession, smtpSession, currentAccount;

static GRMailServiceManager *instance = nil;

#pragma mark - Initialization

+ (GRMailServiceManager *)shared {
    if (!instance) {
        instance = [[GRMailServiceManager alloc] init];
    }
    return instance;
}

- (id)init
{
    if ((self = [super init]))
    {
        self.queue = [[[NSOperationQueue alloc] init] autorelease];
        [self.queue addObserver:self forKeyPath:@"operations" options:0 context:NULL];

        self.sendingMessagesQueue = [[[NSOperationQueue alloc] init] autorelease];
        [sendingMessagesQueue setMaxConcurrentOperationCount:1];
    }
    return self;
}

#pragma mark - Mail action LOGIN

- (void)mailActionVerification:(NSMutableDictionary *)mailSettings {
    self.imapSession = [[MCOIMAPSession alloc] init];
	self.imapSession.hostname = [mailSettings objectForKey:@"server"];
	self.imapSession.port = [[mailSettings objectForKey:@"port"]  integerValue];
	self.imapSession.username = [mailSettings objectForKey:@"userName"];
	self.imapSession.password = [mailSettings objectForKey:@"password"];

	self.imapSession.connectionType = MCOConnectionTypeTLS;
	self.imapSession.connectionLogger = ^(void * connectionID, MCOConnectionLogType type, NSData * data) {
//        @synchronized(self) {
//            if (type != MCOConnectionLogTypeSentPrivate) {
//                NSLog(@"!!!!!!!!!  event logged:%p %i withData: %@", connectionID, type, [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding]);
//            }
//        }
    };
    
	NSLog(@"checking account");
	imapCheckOp = [self.imapSession checkAccountOperation];
	[imapCheckOp start:^(NSError *error) {
		if (error == nil) {
			[self verificationSucceedWithUserName:[mailSettings objectForKey:@"userName"]];
		} else {
            [self verificationFailed:error];
			NSLog(@"error loading account: %@", error);
		}
		imapCheckOp = nil;
	}];

}

- (void)verificationSucceedWithUserName:(NSString *)userName {
    NSLog(@"finish loading account");
    self.imapSession = nil;
    if ([delegate respondsToSelector:@selector(mailActionSucceed:)]) {
        [delegate mailActionSucceed:MAIL_VERIFICATION_OP];
    }
}


- (void)verificationFailed:(NSError *)error {
    NSLog(@"error loading account: %@", [error localizedDescription]);
    self.imapSession = nil;
    if ([delegate respondsToSelector:@selector(mailActionFailed:)]) {
        [delegate mailActionFailed:error];
    }
}

#pragma mark - Read folder

- (void)mailActionGetMailsFromFolder:(MailFolderIndex)folderIndex isMore:(BOOL)isLoadMore {
    if (![self isReadOperationCanProcceedForFolder:folderIndex])
        return;
    if (!self.imapSession) {
        [self initSession];
    }
    
    GRReadMailOperation *op = [[GRReadMailOperation alloc] init];
    op.folderIndex = folderIndex;
    op.delegate = self;
    op.isLoadMore = isLoadMore;
    [self.queue addOperation:op];
    [op release];
}

- (BOOL)isReadOperationCanProcceedForFolder:(MailFolderIndex)folderIndex {
    BOOL flag = YES;
    for (NSOperation *op in [self.queue operations]) {
        if ([op isKindOfClass:GRReadMailOperation.class]) {
            GRReadMailOperation *mailOperation = (GRReadMailOperation *)op;
            if (!mailOperation.isFinished && mailOperation.folderIndex == folderIndex) {
                flag = NO;
                break;
            }
        }
    }
    return flag;
}


#pragma mark - Fetch message

- (void)mailActionFetchMessageWithUID:(NSInteger)uid folderIndex:(NSInteger)folderIndex {
    if (![self isFetchOperationIsInUse:uid]) {
        GRMailFetchOperation *op = [[GRMailFetchOperation alloc] init];
        op.uid = uid;
        op.folderIndex = folderIndex;
        op.delegate = self;
        [self.queue addOperation:op];
        [op release];
    }
}

- (BOOL)isFetchOperationIsInUse:(NSInteger)uid {
    BOOL flag = NO;
    for (NSOperation *op in [self.queue operations]) {
        if ([op isKindOfClass:GRMailFetchOperation.class]) {
            if (((GRMailFetchOperation*)op).uid == uid) {
                flag = YES;
                break;
            }
        }
    }
    
    return flag;
}

#pragma mark - Send operation

- (void)mailActionSentMessage:(GRSMTPMessage *)message {
    if (!self.smtpSession) {
        [self initSmtpSession];
    }
    if (![self isSendOperationIsInUse:message.uid]) {
        GRMailSentOperation *op = [[GRMailSentOperation alloc] init];
        op.sentMessage = message;
        op.delegate = self;
        [self.queue addOperation:op];
        [op release];
    }
}


- (BOOL)isSendOperationIsInUse:(NSInteger)uid {
    BOOL flag = NO;
    for (NSOperation *op in [self.queue operations]) {
        if ([op isKindOfClass:GRMailSentOperation.class]) {
            if (((GRMailSentOperation *)op).sentMessage.uid == uid) {
                flag = YES;
                break;
            }
        }
    }
    return flag;
}

- (NSInteger)getSendingOperationsCount {
    NSInteger count = 0;
    for (NSOperation *op in [self.queue operations]) {
        if ([op isKindOfClass:GRMailSentOperation.class]) {
            count++;
        }
    }
    return count;
}

#pragma mark - Move mail to folders operation

- (void)moveMailFromFolder:(NSInteger)fromFolderIndex toFolder:(NSInteger)toFolderIndex withUid:(NSInteger)uid {
    GRMailMoveOperation *op = [[GRMailMoveOperation alloc] init];
    op.uid = uid;
    op.targetFolderIndex = fromFolderIndex;
    op.destFolderIndex = toFolderIndex;
    op.delegate = self;
    [self.queue addOperation:op];
    [op release];
}


#pragma mark - Append operation

- (void)mailActionAppendMessage:(GRSMTPMessage *)message toFolderIndex:(NSInteger)targetFolderIndex {
    GRMailAppendOperation *op = [[GRMailAppendOperation alloc] init];
    op.delegate = self;
    op.sentMessage = message;
    op.targetFolderIndex = targetFolderIndex;
    [self.queue addOperation:op];
    [op release];
}

#pragma mark - GRMailSentOperation delegate

- (void)sendMailOperationSucceed {
    if ([delegate respondsToSelector:@selector(mailActionSucceed:)]) {
        [delegate mailActionSucceed:MAIL_SEND_OP];
    }
    
    
    if (!self.avAudioPlayer) {
        NSURL * swooshURL = [[NSBundle mainBundle] URLForResource:@"swoosh" withExtension:@"m4a"];
        self.avAudioPlayer = [[[AVAudioPlayer alloc] initWithContentsOfURL:swooshURL error:NULL] autorelease];
    }
    [self.avAudioPlayer play];
    
    [[AppDelegate getInstance] displayAlertWithMessage:@"Your email has been sent"];
}

#pragma mark - GRReadMailOperation delegate

- (void)readMailOperationSucceed:(GRReadMailOperation *)op {
    switch (op.folderIndex) {
        case MAIL_FOLDER_INBOX:
            if ([delegate respondsToSelector:@selector(mailActionSucceed:)]) {
                [delegate mailActionSucceed:MAIL_INBOX_OP];
            }
            break;
        case MAIL_FOLDER_DRAFTS:
            if ([delegate respondsToSelector:@selector(mailActionSucceed:)]) {
                [delegate mailActionSucceed:MAIL_DRAFTS_OP];
            }
            break;
        case MAIL_FOLDER_SENT:
            if ([delegate respondsToSelector:@selector(mailActionSucceed:)]) {
                [delegate mailActionSucceed:MAIL_SENT_OP];
            }
            break;
        case MAIL_FOLDER_TRASH:
            if ([delegate respondsToSelector:@selector(mailActionSucceed:)]) {
                [delegate mailActionSucceed:MAIL_TRASH_OP];
            }
            break;
            
        default:
            break;
    }
}

- (void)readMailOperationFailed:(NSError *)error {
    if ([delegate respondsToSelector:@selector(mailActionFailed:)]) {
        [delegate mailActionFailed:error];
    }
}

#pragma mark - GRReadMailOperation delegate
- (void)fetchOperationCompleted {
    if ([delegate respondsToSelector:@selector(mailActionSucceed:)]) {
        [delegate mailActionSucceed:MAIL_FETCH_OP];
    }
}

- (void)fetchOperationFailed:(NSError *)error {
    if ([delegate respondsToSelector:@selector(mailActionFailed:)]) {
        [delegate mailActionFailed:error];
    }
}

#pragma mark - GRMailMoveOperation delegate

- (void)moveMessageSucceed {
    if ([delegate respondsToSelector:@selector(mailActionSucceed:)]) {
        [delegate mailActionSucceed:MAIL_MOVE_OP];
    }
}

#pragma mark - GRMailAppendOperation delegate

- (void)appendMessageSucceed {
    if ([delegate respondsToSelector:@selector(mailActionSucceed:)]) {
        [delegate mailActionSucceed:MAIL_APPEND_OP];
    }
}


#pragma mark - Helpers

- (void)initSession {
    imapSession = [[MCOIMAPSession alloc] init];
    self.imapSession.hostname = self.currentAccount.server;
	self.imapSession.port = [self.currentAccount.port  integerValue];
	self.imapSession.username = self.currentAccount.userName;
	self.imapSession.password = self.currentAccount.password;
	self.imapSession.connectionType = MCOConnectionTypeTLS;
	self.imapSession.connectionLogger = ^(void * connectionID, MCOConnectionLogType type, NSData * data) {
        @synchronized(self) {
            //if (type != MCOConnectionLogTypeSentPrivate) {
                NSLog(@"!!!!!!event logged:%p %i withData: %@", connectionID, type, [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding]);
           // }
        }
    };
}

- (void)initSmtpSession {
    smtpSession = [[MCOSMTPSession alloc] init];
    NSLog(@"%@", self.currentAccount.smtpServer);
    NSLog(@"%i", [self.currentAccount.smtpPort  integerValue]);
    self.smtpSession.hostname = self.currentAccount.smtpServer;
	self.smtpSession.port = [self.currentAccount.smtpPort  integerValue];
	self.smtpSession.username = self.currentAccount.userName;
	self.smtpSession.password = self.currentAccount.password;
    
    if ([self.currentAccount.connectionType integerValue] == 0)
    {
        self.smtpSession.connectionType = MCOConnectionTypeClear;
    }
    else if ([self.currentAccount.connectionType integerValue] == 1)
    {
        self.smtpSession.connectionType = MCOConnectionTypeStartTLS;
    }
    else
    {
        self.smtpSession.connectionType = MCOConnectionTypeTLS;
    }
    
	self.smtpSession.connectionLogger = ^(void * connectionID, MCOConnectionLogType type, NSData * data) {
        @synchronized(self) {
            if (type != MCOConnectionLogTypeSentPrivate) {
                NSLog(@"SMTP: event logged:%p %i withData: %@", connectionID, type, [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding]);
            }
        }
    };
}

- (void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary *)change context:(void *)context {
    NSLog(@"Listening");
	if( object == queue && [keyPath isEqual:@"operations"]) {
		if([queue.operations count] == 0) {
			[self performSelectorOnMainThread:@selector(allMailOperationsDone) withObject:nil waitUntilDone:YES];
		}
	}
}

- (void)allMailOperationsDone {
    if ([delegate respondsToSelector:@selector(mailActionSucceed:)]) {
        [delegate mailActionSucceed:MAIL_ALL_OPERATIONS_DONE];
    }
}

- (void)resetSession {
    self.imapSession = nil;
    self.smtpSession = nil;
    self.currentAccount = nil;
    for (NSOperation *op in [self.queue operations]) {
        if ([op isKindOfClass:GRReadMailOperation.class]) {
            GRReadMailOperation *mailOperation  = (GRReadMailOperation *)op;
            mailOperation.finished = YES;
        }
        if ([op isKindOfClass:GRMailSentOperation.class]) {
            GRMailSentOperation *mailOperation  = (GRMailSentOperation *)op;
            [mailOperation cancelSending];
        }
    }
    
    [self resetFetching];
    [self.queue cancelAllOperations];
}

- (void)resetFetching {
    for (NSOperation *op in [self.queue operations]) {
        if ([op isKindOfClass:GRMailFetchOperation.class]) {
//            GRMailFetchOperation *mailOperation  = (GRMailFetchOperation *)op;
//            mailOperation.finished = YES;
//            [mailOperation stopFetching];
        }
    }
}

- (void)dealloc
{
    [_avAudioPlayer release];
    self.sendingMessagesQueue = nil;
    self.currentOperation = nil;
    self.queue = nil;
    self.imapSession = nil;
    self.smtpSession = nil;
    [super dealloc];
}

- (void)setupMailAccount:(TMailAccount *)account {
    if (self.currentAccount) {
        self.currentAccount = nil;
    }
    self.currentAccount = account;
}


@end
