//
//  GRMailSentOperation.h
//  GrannyApp
//
//  Created by Ievgen Pavliuk on 7/15/13.
//
//

#import <Foundation/Foundation.h>
#import "GRMailServiceManager.h"

@interface GRMailSentOperation : NSOperation {
    
    BOOL     finished;
    BOOL     executing;
    BOOL     isSentCompleted;
    BOOL     isTimeOut;
    NSInteger  tick;
    id       delegate;

    GRSMTPMessage *sentMessage;
    NSMutableArray *sentOperations;
    MCOSMTPSendOperation * op;
}
@property (nonatomic, retain) GRSMTPMessage *sentMessage;
@property (nonatomic, assign) id       delegate;

- (void)cancelSending;
@end
