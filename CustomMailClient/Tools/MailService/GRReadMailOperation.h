//
//  GRReadMailOperation.h
//  GrannyApp
//
//  Created by Ievgen Pavliuk on 7/9/13.
//
//

#import <Foundation/Foundation.h>
#import "GRMailServiceManager.h"
#import "DataManager.h"

@interface GRReadMailOperation : NSOperation {
    
    BOOL     isLoadMore;
    BOOL     finished;
	BOOL     executing;
    BOOL     isLoadingCompleted;
    
    NSInteger folderIndex;

    NSMutableArray *completedMessages;
    
    id delegate;
}
@property (nonatomic, assign) BOOL  isLoadMore;
@property (nonatomic, assign) BOOL  finished;
@property (nonatomic, assign) NSInteger folderIndex;;
@property (nonatomic, assign) id delegate;

@end
