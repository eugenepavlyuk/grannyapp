//
//  GRReadMailOperation.m
//  GrannyApp
//
//  Created by Ievgen Pavliuk on 7/9/13.
//
//

#import "GRReadMailOperation.h"

@implementation GRReadMailOperation
@synthesize isLoadMore;
@synthesize finished;
@synthesize folderIndex;
@synthesize delegate;

- (BOOL)isExecuting {
	return executing;
}

- (BOOL)isFinished {
	return finished;
}

- (void)start {
    @autoreleasepool {
        executing = YES;
        finished = NO;
        [self runOperation];
        while (!finished) {
            //NSLog(@"Wait");
            [NSThread sleepForTimeInterval:0.1f];
        }
    }
}

- (void)runOperation {
    [self startListenFinishLoading];
    completedMessages = [[NSMutableArray alloc] init];
    MCOIMAPSession *imapSession = [GRMailServiceManager shared].imapSession;
    [self startReadFolderInfo:imapSession];
}


- (void)startReadFolderInfo:(MCOIMAPSession *)imapSession {
    
    MCOMailProvider *provider = [[MCOMailProvidersManager sharedManager] providerForEmail:[GRMailServiceManager shared].currentAccount.userName];
    
    NSString       *targetFolder = nil;
    
    switch (self.folderIndex) {
        case MAIL_FOLDER_INBOX:
            targetFolder = @"INBOX";
            break;
        case MAIL_FOLDER_DRAFTS:
            targetFolder = [provider draftsFolderPath];
            break;
        case MAIL_FOLDER_SENT:
            targetFolder = [provider sentMailFolderPath];
            break;
        case MAIL_FOLDER_TRASH:
            targetFolder = [provider trashFolderPath];
            break;
            
        default:
            break;
    }
    
    MCOIMAPFolderInfoOperation *folderInfo = [imapSession folderInfoOperation:targetFolder];

    [folderInfo start:^(NSError *error, MCOIMAPFolderInfo *info) {
        
        NSArray *localMessages = [[GRMailDataManager sharedManager] findHeaderMessagesByFolderID:self.folderIndex];
        
        NSInteger fetchLimit = MAIL_FETCH_SIZE;
        NSInteger startPoint = 1;
        
        if (!self.isLoadMore)
        {
            startPoint = [info messageCount] - fetchLimit;
        }
        else
        {
            startPoint = [info messageCount] - [localMessages count] - fetchLimit;
        }
        
        if (startPoint <= 0)
        {
            fetchLimit = ([info messageCount] - 1);
            startPoint = 1;
        }
        
        //fetchLimit -= 1;
        
        MCOIndexSet *numbers = [MCOIndexSet indexSetWithRange:MCORangeMake(startPoint, fetchLimit)];
        
        [self startReadHeaders:imapSession numbers:numbers];
    }];
    
    /**
     Returns an operation to fetch messages by (sequence) number.
     For example: show 50 most recent uids.
     NSString *folder = @"INBOX";
     MCOIMAPFolderInfoOperation *folderInfo = [session folderInfoOperation:folder];
     
     [folderInfo start:^(NSError *error, MCOIMAPFolderInfo *info) {
     int numberOfMessages = 50;
     numberOfMessages -= 1;
     MCOIndexSet *numbers = [MCOIndexSet indexSetWithRange:MCORangeMake([info messageCount] - numberOfMessages, numberOfMessages)];
     
     MCOIMAPFetchMessagesOperation *fetchOperation = [session fetchMessagesByNumberOperationWithFolder:folder
     requestKind:MCOIMAPMessagesRequestKindUid
     numbers:numbers];
     
     [fetchOperation start:^(NSError *error, NSArray *messages, MCOIndexSet *vanishedMessages) {
     for (MCOIMAPMessage * message in messages) {
     NSLog(@"%u", [message uid]);
     }
     }];
     }];
     */

}


- (void)startReadHeaders:(MCOIMAPSession *)imapSession numbers:(MCOIndexSet *)numbers {
    
    
    
//    MCOIMAPFetchFoldersOperation * op = [imapSession fetchAllFoldersOperation];
//    [op start:^(NSError * error, NSArray *folders) {
//        for (MCOIMAPFolder *folder in folders) {
//            
//            char delimeter = folder.delimiter;
//            NSString* string = [NSString stringWithFormat:@"%c" , delimeter];
//            NSLog(@"delimeter %@", string);
//        }
//    }];
//
    
    if (![GRMailServiceManager shared].currentAccount.userName)
    {
        isLoadingCompleted = YES;
        
        return ;
    }
    
    MCOMailProvider *provider = [[MCOMailProvidersManager sharedManager] providerForEmail:[GRMailServiceManager shared].currentAccount.userName];
    
    NSString       *targetFolder = nil;
    
    switch (self.folderIndex) {
        case MAIL_FOLDER_INBOX:
            targetFolder = @"INBOX";
            break;
        case MAIL_FOLDER_DRAFTS:
            targetFolder = [provider draftsFolderPath];
            break;
        case MAIL_FOLDER_SENT:
            targetFolder = [provider sentMailFolderPath];
            break;
        case MAIL_FOLDER_TRASH:
            targetFolder = [provider trashFolderPath];
            break;
            
        default:
            break;
    }
    
	MCOIMAPMessagesRequestKind requestKind = (MCOIMAPMessagesRequestKind)
	(MCOIMAPMessagesRequestKindHeaders | MCOIMAPMessagesRequestKindStructure |
	 MCOIMAPMessagesRequestKindInternalDate | MCOIMAPMessagesRequestKindHeaderSubject |
	 MCOIMAPMessagesRequestKindFlags);
    
    MCOIMAPFetchMessagesOperation *imapMessagesFetchOp = [imapSession fetchMessagesByNumberOperationWithFolder:targetFolder
                                                                                          requestKind:requestKind
                                                                                              numbers:numbers];

    
	[imapMessagesFetchOp setProgress:^(unsigned int progress) {
		//NSLog(@"progress: %u", progress);
	}];
	
	[imapMessagesFetchOp start:^(NSError *error, NSArray *messages, MCOIndexSet *vanishedMessages) {
        if (error) {
            [self operationFailed:error];
            isLoadingCompleted = YES;
        } else {
            NSSortDescriptor *sort = [NSSortDescriptor sortDescriptorWithKey:@"header.date" ascending:NO];
            NSArray *sortedMessages =  [messages sortedArrayUsingDescriptors:@[sort]];
            
            for (MCOIMAPMessage *message in sortedMessages) {
                [completedMessages addObject:message];
            }
            
            isLoadingCompleted = YES;
        }
	}];
    //imapMessagesFetchOp = nil;
}

- (void)startListenFinishLoading {
    [NSThread detachNewThreadSelector:@selector(listen) toTarget:self withObject:nil];
}

- (void)listen {
    @autoreleasepool {
        while (!isLoadingCompleted) {
            [NSThread sleepForTimeInterval:0.1f];
        }
        for (MCOIMAPMessage *message in completedMessages) {
            if (self.finished) {
                break;
            }
            [self saveMessageInDataBase:message];
        }
        NSLog(@"***************** Finished loading mails");
        [self performSelectorOnMainThread:@selector(callOperationCompleted) withObject:nil waitUntilDone:NO];
        executing = NO;
        self.finished = YES;
    }
}

- (void)operationFailed:(NSError *)error {
    [self performSelectorOnMainThread:@selector(callOperationFailed:) withObject:error waitUntilDone:NO];
    executing = NO;
    self.finished = YES;
}

- (void)callOperationCompleted {
    if ([delegate respondsToSelector:@selector(readMailOperationSucceed:)]) {
        [delegate performSelector:@selector(readMailOperationSucceed:) withObject:self];
    }
}

- (void)callOperationFailed:(NSError *)error {
    if ([delegate respondsToSelector:@selector(readMailOperationFailed:)]) {
        [delegate performSelector:@selector(readMailOperationFailed:) withObject:error];
    }
    
}

#pragma mark - Helpers

- (void)saveMessageInDataBase:(MCOIMAPMessage *)message {
    NSArray *results = [[GRMailDataManager sharedManager] findHeaderMessageByID:message.uid folderID:self.folderIndex];
    if ([results count] == 0) {
        switch (self.folderIndex) {
            case MAIL_FOLDER_INBOX:
                [[GRMailDataManager sharedManager] saveInboxMessage:message];
                break;
            case MAIL_FOLDER_DRAFTS:
                [[GRMailDataManager sharedManager] saveDraftMessage:message];
                break;
            case MAIL_FOLDER_SENT:
                [[GRMailDataManager sharedManager] saveSentMessage:message];
                break;
            case MAIL_FOLDER_TRASH:
                [[GRMailDataManager sharedManager] saveTrashMessage:message];
                break;
                
            default:
                break;
        }

    }
}


- (void)dealloc {
    [completedMessages release];
    delegate = nil;
    [super dealloc];
}
@end
