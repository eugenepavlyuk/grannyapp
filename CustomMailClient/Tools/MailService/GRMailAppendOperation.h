//
//  GRMailAppendOperation.h
//  GrannyApp
//
//  Created by Ievgen Pavliuk on 7/17/13.
//
//

#import <Foundation/Foundation.h>
#import "GRMailServiceManager.h"

@interface GRMailAppendOperation : NSOperation {
    BOOL     finished;
    BOOL     executing;
    BOOL     isSentCompleted;
    BOOL     isTimeOut;
    NSInteger  tick;
    id       delegate;
    
    GRSMTPMessage *sentMessage;
    NSInteger      targetFolderIndex;

}
@property (nonatomic, retain) GRSMTPMessage  *sentMessage;
@property (nonatomic, assign) id              delegate;
@property (nonatomic, assign) NSInteger       targetFolderIndex;

@end
