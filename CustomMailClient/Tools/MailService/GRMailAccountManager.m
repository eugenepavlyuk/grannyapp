//
//  GRMailAccountManager.m
//  GrannyApp
//
//  Created by Ievgen Pavliuk on 4/12/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "GRMailAccountManager.h"
#import "DataManager.h"
#import "RecientContact.h"

@implementation GRMailAccountManager 
@synthesize mailType;
@synthesize isBusy;

static GRMailAccountManager *instance = nil;

+ (GRMailAccountManager *)manager {
    if (!instance) {
        instance = [[GRMailAccountManager alloc] init];
    }
    return instance;
}

- (id)init {
    if ((self = [super init])) {
        accounts = [[NSMutableArray alloc] init];
    }
    return self;
}


- (void)createAccount:(NSDictionary *)dictionary {
    TMailAccount *accountModel = nil;
    NSArray *_accounts = [[DataManager sharedInstance] allForEntity:@"TMailAccount" forFieldName:@"accountName" withFieldValue:[dictionary objectForKey:@"accountName"]];
    for (TMailAccount *tModel in _accounts) {
        accountModel = tModel;
    }
    if (!accountModel) {
        accountModel = (TMailAccount *)[[DataManager sharedInstance] createEntityWithName:@"TMailAccount"];
    }
    accountModel.accountName = [dictionary objectForKey:@"accountName"];
    accountModel.type = [dictionary objectForKey:@"type"];
    accountModel.server = [dictionary objectForKey:@"server"];
    accountModel.port = (NSNumber *)[dictionary objectForKey:@"port"];
    accountModel.userName = [dictionary objectForKey:@"userName"];
    accountModel.password = [dictionary objectForKey:@"password"];
    accountModel.isSSL = [NSNumber numberWithBool:YES];
    accountModel.isActive = [NSNumber numberWithBool:NO];
    accountModel.smtpServer = [dictionary objectForKey:@"smtpServer"];
    accountModel.smtpPort = (NSNumber *)[dictionary objectForKey:@"smtpPort"];
    accountModel.connectionType = [dictionary objectForKey:@"connectionType"];
    [[DataManager sharedInstance] save];
}

- (void)removeAccount:(TMailAccount *)accountEntity {
    [self removeAllMessagesForAccount:accountEntity];
    [self removeAllAttachemntsForAccount:accountEntity];
    [self removeAllHeadersForAccount:accountEntity];
    [self removeAllPendingMessagesForAccount:accountEntity];
    [[DataManager sharedInstance] removeEntityModel:accountEntity];
}

- (void)removeAllMessagesForAccount:(TMailAccount *)accountEntity {
    NSArray *messages = [[GRMailDataManager sharedManager] getAllMessagesForAccountName:accountEntity.accountName];
    for (TMailMessage *m in messages) {
        [[DataManager sharedInstance] removeEntityModel:m];
    }
    
    messages = [[GRMailDataManager sharedManager] getAllDraftMessagesForAccountName:accountEntity.accountName];
    for (TMailDraftMessage *m in messages) {
        [[DataManager sharedInstance] removeEntityModel:m];
    }
    messages = [[GRMailDataManager sharedManager] getAllSentMessagesForAccountName:accountEntity.accountName];
    for (TMailSentMessage *m in messages) {
        [[DataManager sharedInstance] removeEntityModel:m];
    }
    messages = [[GRMailDataManager sharedManager] getAllTrashMessagesForAccountName:accountEntity.accountName];
    for (TMailDeletedMessage *m in messages) {
        [[DataManager sharedInstance] removeEntityModel:m];
    }
}

- (void)removeAllAttachemntsForAccount:(TMailAccount *)accountEntity {
    NSArray *rows = [[GRMailDataManager sharedManager] getAllAttachmentsForAccount:accountEntity];
    for (TMessageAttachment *m in rows) {
        [[DataManager sharedInstance] removeEntityModel:m];
    }
}

- (void)removeAllHeadersForAccount:(TMailAccount *)accountEntity {
    NSArray *rows = [[GRMailDataManager sharedManager] findHeadersByAccountName:accountEntity.accountName];
    for (THeaders *m in rows) {
        [[DataManager sharedInstance] removeEntityModel:m];
    }
}

- (void)removeAllPendingMessagesForAccount:(TMailAccount *)accountEntity {
    NSArray *rows = [[GRMailDataManager sharedManager] getAllPendingMessagesForAccount:accountEntity.accountName];
    for (TPendingMessage *m in rows) {
        [[DataManager sharedInstance] removeEntityModel:m];
    }
}

- (NSArray *)getAccounts {
    NSArray *data = [[DataManager sharedInstance] allRowsForEntity:@"TMailAccount"];
    return data;
}

- (void)resetAccount {
}

- (void)dealloc {
    [super dealloc];
}

#pragma mark - Account management

- (NSMutableDictionary *)getCurrentMailSettings {
    NSMutableDictionary *settings = nil;
    NSArray *objects = nil;
    NSArray *keys = nil;
    switch (self.mailType) {
        case GRMAIL_TYPE_GMAIL:
            keys = [NSArray arrayWithObjects:@"type", @"server", @"port", @"smtpServer", @"smtpPort", nil];
            objects = [NSArray arrayWithObjects:@"gmail", @"imap.gmail.com", [NSNumber numberWithInt:993], @"smtp.gmail.com", [NSNumber numberWithInt:465], nil];
            break;
        case GRMAIL_TYPE_HOTMAIL:
            keys = [NSArray arrayWithObjects:@"type", @"server", @"port", @"smtpServer", @"smtpPort", nil];
            objects = [NSArray arrayWithObjects:@"pop3", @"pop3.live.com", [NSNumber numberWithInt:995], @"smtp.gmail.com", [NSNumber numberWithInt:587], nil];
            break;
            
        default:
            break;
    }
    
    settings = [NSMutableDictionary dictionaryWithObjects:objects forKeys:keys];
    return settings;
}

- (void)createFakePeople {
    NSArray *data = [[DataManager sharedInstance] allRowsForEntity:@"RecientContact"];
    if ([data count] == 0) {
        for (int i=0; i<4; i++) {
            RecientContact *newContact = [[DataManager sharedInstance] createEntityWithName:@"RecientContact"];
            switch (i) {
                case 0:
                    newContact.lastname = @"Gustavsen";
                    newContact.firstname = @"Rasmus";
                    newContact.phone = @"111";
                    newContact.email = @"rg@mothermobile.com";
                    break;
                case 1:
                    newContact.lastname = @"Igor";
                    newContact.firstname = @"Nikolaev";
                    newContact.phone = @"111";
                    newContact.email = @"in@mothermobile.com";
                    break;
                case 2:
                    newContact.lastname = @"Eugene";
                    newContact.firstname = @"Pavluk";
                    newContact.phone = @"111";
                    newContact.email = @"ep@mothermobile.com";
                    break;
                case 3:
                    newContact.lastname = @"AirDigi";
                    newContact.firstname = @"Music";
                    newContact.phone = @"111";
                    newContact.email = @"music@mothermobile.com";
                    break;
                    
                default:
                    break;
            }
            [[DataManager sharedInstance] save];
        }
    }
}

@end
