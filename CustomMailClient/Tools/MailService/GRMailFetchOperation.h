//
//  GRMailFetchOperation.h
//  GrannyApp
//
//  Created by Ievgen Pavliuk on 7/11/13.
//
//

#import <Foundation/Foundation.h>
#import "GRMailServiceManager.h"
#import "DataManager.h"
#import "GRMailDataManager.h"

@interface GRMailFetchOperation : NSOperation {
    NSInteger uid;
    NSInteger folderIndex;
    id        delegate;
    
    BOOL     finished;
    BOOL     executing;
    BOOL     isFetchingCompleted;

    NSMutableArray         *ops;
    MCOMessageParser       *fetchedMessage;
    NSInteger               tick;

}
@property (nonatomic, assign) NSInteger uid;
@property (nonatomic, assign) NSInteger folderIndex;
@property (nonatomic, assign) id delegate;
@property (nonatomic, assign) BOOL  finished;

- (void)stopFetching;
@end
