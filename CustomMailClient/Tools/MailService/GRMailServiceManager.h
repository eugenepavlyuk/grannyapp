//
//  GRMailServiceManager.h
//  GrannyApp
//
//  Created by Ievgen Pavliuk on 4/12/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "TMailAccount.h"
#import "GRMailDataManager.h"
#import "GRSMTPMessage.h"
#import "GRMailServiceManager.h"
#import <MailCore/MailCore.h>

#define MAIL_VERIFICATION_OP      1
#define MAIL_INBOX_OP             2
#define MAIL_DRAFTS_OP            3
#define MAIL_SENT_OP              4
#define MAIL_TRASH_OP             5
#define MAIL_ALL_OPERATIONS_DONE  6
#define MAIL_FETCH_OP             7
#define MAIL_SEND_OP              8
#define MAIL_MOVE_OP              9
#define MAIL_APPEND_OP            10

#define MAIL_FETCH_SIZE      10

typedef enum indexes
{
    MAIL_FOLDER_INBOX = 0,
    MAIL_FOLDER_DRAFTS = 1,
    MAIL_FOLDER_SENT = 2,
    MAIL_FOLDER_TRASH = 3
} MailFolderIndex;



@protocol GRMailServiceDelegate <NSObject>

@optional
- (void)mailActionSucceed:(NSInteger)operationCode;
- (void)mailActionFailed:(NSError *)error;
@end

@class MCOIMAPOperation;
@class MCOIMAPSession;
@class MCOSMTPSession;

@interface GRMailServiceManager : NSObject {
    
    NSOperationQueue *queue;
    NSOperationQueue *sendingMessagesQueue;
    id<GRMailServiceDelegate> delegate;
    NSInteger currentFolder;
    NSOperation *currentOperation;
    
    MCOIMAPOperation *imapCheckOp;
    MCOIMAPSession *imapSession;
    MCOSMTPSession *smtpSession;
    TMailAccount   *currentAccount;
}
@property (nonatomic, retain) NSOperation *currentOperation;
@property (nonatomic, retain) NSOperationQueue *queue;
@property (nonatomic, retain) NSOperationQueue *sendingMessagesQueue;
@property (nonatomic, assign) id<GRMailServiceDelegate> delegate;
@property (nonatomic, assign) NSInteger currentFolder;

@property (nonatomic, strong) MCOIMAPSession *imapSession;
@property (nonatomic, strong) MCOSMTPSession *smtpSession;
@property (nonatomic, retain) TMailAccount   *currentAccount;


+ (GRMailServiceManager *)shared;

#pragma mark - Mail action LOGIN
- (void)mailActionVerification:(NSMutableDictionary *)mailSettings;

#pragma mark - Mail action INBOX
- (void)mailActionGetMailsFromFolder:(MailFolderIndex)folderIndex isMore:(BOOL)isLoadMore;

#pragma mark - Mail action FETCH
- (void)mailActionFetchMessageWithUID:(NSInteger)uid folderIndex:(NSInteger)folderIndex;

#pragma mark - Send operation
- (void)mailActionSentMessage:(GRSMTPMessage *)message;
- (NSInteger)getSendingOperationsCount;

#pragma mark - Move mail to folders operation
- (void)moveMailFromFolder:(NSInteger)fromFolderIndex toFolder:(NSInteger)toFolderIndex withUid:(NSInteger)uid;

#pragma mark - Append operation
- (void)mailActionAppendMessage:(GRSMTPMessage *)message toFolderIndex:(NSInteger)targetFolderIndex;

#pragma mark - Helpers
- (void)resetSession;
- (void)resetFetching;
- (void)setupMailAccount:(TMailAccount *)account;
@end

