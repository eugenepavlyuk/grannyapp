//
//  GRMailSentOperation.m
//  GrannyApp
//
//  Created by Ievgen Pavliuk on 7/15/13.
//
//

#import "GRMailSentOperation.h"

#define TIME_OUT_SENDING 2400

@implementation GRMailSentOperation
@synthesize sentMessage;
@synthesize delegate;

- (BOOL)isExecuting {
	return executing;
}

- (BOOL)isFinished {
	return finished;
}

- (void)start {
    @autoreleasepool {
        finished = NO;
        executing = YES;
        [self runOperation];
        [self startTimeOutListener];
        while (!finished) {
            NSLog(@"Wait");
            [NSThread sleepForTimeInterval:0.1];
        }
    }
}

- (void)runOperation {
    [self startListenFinishLoading];
    MCOMessageBuilder * builder = [[MCOMessageBuilder alloc] init];
    [[builder header] setFrom:[MCOAddress addressWithDisplayName:@"" mailbox:self.sentMessage.senderMail]];
    NSMutableArray *tos = [NSMutableArray array];
    for (NSString *mail in self.sentMessage.tos) {
        MCOAddress *address = [MCOAddress addressWithMailbox:mail];
        [tos addObject:address];
    }
    [[builder header] setTo:tos];
    [[builder header] setSubject:self.sentMessage.subject];
    NSString *formattedString = [self.sentMessage.plainText stringByReplacingOccurrencesOfString:@"\n" withString:@"<br>"];
    if ([self.sentMessage.htmlText length] != 0) {
        NSString *htmlAppendText = [NSString stringWithFormat:@"<div>%@</div> <p> %@", formattedString, self.sentMessage.htmlText];
        [builder setHTMLBody:htmlAppendText];
    } else {
        [builder setHTMLBody:[NSString stringWithFormat:@"<div>%@</div>", formattedString]];
    }
    for (NSDictionary *atmDict in self.sentMessage.attachments) {
        [builder addAttachment:[MCOAttachment attachmentWithContentsOfFile:[atmDict objectForKey:@"path"]]];
    }
    NSData * rfc822Data = [builder data];
    
    MCOSMTPSession *smtpSession = [GRMailServiceManager shared].smtpSession;
    op = [(MCOSMTPSendOperation *)[smtpSession sendOperationWithData:rfc822Data] retain];
    [op start:^(NSError * error) {
        if (!error) {
            isSentCompleted = YES;
        } else {
            [self performSelectorOnMainThread:@selector(operationFailed:) withObject:error waitUntilDone:NO];
        }
    }];
    [builder release];
}

- (void)startListenFinishLoading {
    [NSThread detachNewThreadSelector:@selector(listen) toTarget:self withObject:nil];
}

- (void)listen {
    @autoreleasepool {
        while (!isSentCompleted) {
            [NSThread sleepForTimeInterval:0.1f];
        }
        if (!finished) {
            [self performSelectorOnMainThread:@selector(callOperationCompleted) withObject:nil waitUntilDone:NO];
        }
        executing = NO;
        finished = YES;
        NSLog(@"Sent completed");
    }
}

- (void)callOperationCompleted {
    [self removePendingMessageFromDB];
    if ([delegate respondsToSelector:@selector(sendMailOperationSucceed)]) {
        [delegate performSelector:@selector(sendMailOperationSucceed) withObject:nil];
    }
}

- (void)removePendingMessageFromDB {
    [[GRMailDataManager sharedManager] removePendingMessageWithID:self.sentMessage.uid];
}

- (void)operationFailed:(NSError *)error {
    NSLog(@"!!!!!!!! ERROR: %@", [error localizedDescription]);
    if ([delegate respondsToSelector:@selector(readMailOperationFailed:)]) {
        [delegate performSelector:@selector(readMailOperationFailed:) withObject:error];
    }
    executing = NO;
    finished = YES;
}


- (void)dealloc {
    delegate = nil;
    [sentMessage release];
    [op release];
    [super dealloc];
}

- (void)cancelSending {
    [self performSelectorOnMainThread:@selector(removePendingMessageFromDB) withObject:nil waitUntilDone:YES];
    [op cancel];
    isSentCompleted = YES;
    executing = NO;
    finished = YES;
}

#pragma mark - Time out

- (void)startTimeOutListener {
    [NSThread detachNewThreadSelector:@selector(listenTimeOut) toTarget:self withObject:nil];

}

- (void)listenTimeOut {
    @autoreleasepool {
        while (!isTimeOut && !finished) {
            if (tick == TIME_OUT_SENDING) {
                isTimeOut = YES;
                NSMutableDictionary* details = [NSMutableDictionary dictionary];
                [details setValue:@"There is no internet connection. Your message will be sent later" forKey:NSLocalizedDescriptionKey];
                NSError *error = [NSError errorWithDomain:@"Error" code:200 userInfo:details];
                [self performSelectorOnMainThread:@selector(operationFailed:) withObject:error waitUntilDone:NO];
            } else {
                tick++;
            }
            NSLog(@"Listen Time Out");
            [NSThread sleepForTimeInterval:0.1f];
        }
    }
}


@end
