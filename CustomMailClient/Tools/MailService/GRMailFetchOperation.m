//
//  GRMailFetchOperation.m
//  GrannyApp
//
//  Created by Ievgen Pavliuk on 7/11/13.
//
//

#import "GRMailFetchOperation.h"

@implementation GRMailFetchOperation
@synthesize uid;
@synthesize folderIndex;
@synthesize delegate;
@synthesize finished;

- (BOOL)isExecuting {
	return executing;
}

- (BOOL)isFinished {
	return finished;
}

- (void)start {
    @autoreleasepool {
        finished = NO;
        executing = YES;
        [self runOperation];
        while (!finished) {
            NSLog(@"Wait");
            [NSThread sleepForTimeInterval:0.1f];
        }
    }
}

- (void)runOperation {
    [self startListenFinishLoading];
    MCOIMAPSession *imapSession = [GRMailServiceManager shared].imapSession;
    
    MCOMailProvider *provider = [[MCOMailProvidersManager sharedManager] providerForEmail:[GRMailServiceManager shared].currentAccount.userName];
    
    NSString       *targetFolder = nil;
    
    switch (self.folderIndex) {
        case MAIL_FOLDER_INBOX:
            targetFolder = @"INBOX";
            break;
        case MAIL_FOLDER_DRAFTS:
            targetFolder = [provider draftsFolderPath];
            break;
        case MAIL_FOLDER_SENT:
            targetFolder = [provider sentMailFolderPath];
            break;
        case MAIL_FOLDER_TRASH:
            targetFolder = [provider trashFolderPath];
            break;
            
        default:
            break;
    }
    
    
    MCOIMAPFetchContentOperation * op = [imapSession fetchMessageByUIDOperationWithFolder:targetFolder uid:self.uid];
    [op start:^(NSError * error, NSData * data) {
        if ([error code] != MCOErrorNone) {
            return;
        }
        
        NSAssert(data != nil, @"data != nil");
        
        MCOMessageParser * msg = [MCOMessageParser messageParserWithData:data];
        fetchedMessage = [msg retain];
        isFetchingCompleted = YES;
    }];
    op = nil;
}


- (void)startListenFinishLoading {
    [NSThread detachNewThreadSelector:@selector(listen) toTarget:self withObject:nil];
}

- (void)listen {
    @autoreleasepool {
        while (!isFetchingCompleted) {
            [NSThread sleepForTimeInterval:0.1f];
        }
        if (fetchedMessage && !finished) {
            [self performSelectorOnMainThread:@selector(updateFetchedMessageInDataBase) withObject:nil waitUntilDone:NO];
        }
        NSLog(@"***************** Finished Fetching message");
        [self performSelectorOnMainThread:@selector(callOperationCompleted) withObject:nil waitUntilDone:NO];
        finished = YES;
        executing = NO;
    }
}

- (void)stopFetching {
    for(MCOOperation * op in ops) {
        [op cancel];
    }
    [ops removeAllObjects];
    isFetchingCompleted = YES;
    finished = YES;
}

- (void)callOperationCompleted {
    if ([delegate respondsToSelector:@selector(fetchOperationCompleted)]) {
        [delegate performSelector:@selector(fetchOperationCompleted) withObject:nil];
    }
}


#pragma mark - Helpers

- (void)updateFetchedMessageInDataBase {
    [[GRMailDataManager sharedManager] fetchMessage:fetchedMessage uid:self.uid folderIndex:self.folderIndex];
    [self saveAttachements];
}

- (void)saveAttachements {
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES);
    NSString *cachePath = [paths objectAtIndex:0];
    NSArray *attachments = fetchedMessage.attachments;
    for (MCOAttachment *attachment in attachments) {
        NSLog(@"File contentID: %@", attachment.contentID);
        NSLog(@"File uniqueId: %@", attachment.uniqueID);
        NSLog(@"File name: %@", attachment.filename);
        NSLog(@"Mime type: %@", attachment.mimeType);
        NSString *storePath = [NSString stringWithFormat:@"%@/%i", cachePath, self.uid];
        
        [[NSFileManager defaultManager] createDirectoryAtPath:storePath
                                  withIntermediateDirectories:YES
                                                   attributes:nil
                                                        error:nil];
        
        NSString *filePath = [storePath stringByAppendingPathComponent:attachment.uniqueID];
        
        filePath = [filePath stringByAppendingPathExtension:[attachment.filename pathExtension]];
        
        [[NSFileManager defaultManager] createFileAtPath:filePath contents:attachment.data attributes:nil];
        
        [[GRMailDataManager sharedManager] saveAttachemntWithName:attachment.filename
                                                      contentType:attachment.mimeType
                                                        messageID:self.uid
                                                         uniqueId:attachment.uniqueID
                                                         filePath:filePath];
    }
    for (MCOAttachment *attachment in fetchedMessage.htmlInlineAttachments) {
        NSLog(@"File contentID: %@", attachment.contentID);
        NSLog(@"File uniqueId: %@", attachment.uniqueID);
        NSLog(@"File name: %@", attachment.filename);
        NSLog(@"Mime type: %@", attachment.mimeType);
//        NSString *cutFileName = [attachment.filename substringToIndex:[attachment.filename rangeOfString:@"."].location];
//        NSString *storePath = [NSString stringWithFormat:@"%@/%@", cachePath, [NSString stringWithFormat:@"%@.jpg", cutFileName]];
        
        NSString *storePath = [NSString stringWithFormat:@"%@/%i", cachePath, self.uid];
        
        [[NSFileManager defaultManager] createDirectoryAtPath:storePath
                                  withIntermediateDirectories:YES
                                                   attributes:nil
                                                        error:nil];
        
        NSString *filePath = [storePath stringByAppendingPathComponent:attachment.uniqueID];
        
        filePath = [filePath stringByAppendingPathExtension:[attachment.filename pathExtension]];
        
        [[NSFileManager defaultManager] createFileAtPath:filePath contents:attachment.data attributes:nil];
        
        [[GRMailDataManager sharedManager] saveAttachemntWithName:attachment.filename
                                                      contentType:attachment.mimeType
                                                        messageID:self.uid
                                                         uniqueId:attachment.uniqueID
                                                         filePath:filePath];
    }
}

- (void)dealloc {
    delegate = nil;
    fetchedMessage = nil;
    [ops release];
    [super dealloc];
}
@end
