//
//  GRMailMoveOperation.m
//  GrannyApp
//
//  Created by Ievgen Pavliuk on 7/16/13.
//
//

#import "GRMailMoveOperation.h"
#import "GRMailDataManager.h"
#import <mailcore2-ios/MailCore/MCOIMAPCopyMessagesOperation.h>

@implementation GRMailMoveOperation
@synthesize delegate;
@synthesize uid;
@synthesize targetFolderIndex;
@synthesize destFolderIndex;

- (BOOL)isExecuting {
	return executing;
}

- (BOOL)isFinished {
	return finished;
}

- (void)start {
    @autoreleasepool {
        finished = NO;
        executing = YES;
        [self runOperation];
        while (!finished) {
            //NSLog(@"Wait");
            [NSThread sleepForTimeInterval:0.1];
        }
    }
}

- (void)runOperation {
   [self startListenFinishLoading];
    if (self.targetFolderIndex == MAIL_FOLDER_TRASH) {
        isMoveCompleted = YES;
    } else {
        MCOIMAPSession *imapSession = [GRMailServiceManager shared].imapSession;
        
        MCOMailProvider *provider = [[MCOMailProvidersManager sharedManager] providerForEmail:[GRMailServiceManager shared].currentAccount.userName];
        
        NSString       *targetFolder = nil;
        
        switch (self.targetFolderIndex) {
            case MAIL_FOLDER_INBOX:
                targetFolder = @"INBOX";
                break;
            case MAIL_FOLDER_DRAFTS:
                targetFolder = [provider draftsFolderPath];
                break;
            case MAIL_FOLDER_SENT:
                targetFolder = [provider sentMailFolderPath];
                break;
            case MAIL_FOLDER_TRASH:
                targetFolder = [provider trashFolderPath];
                break;
                
            default:
                break;
        }
        
        NSString       *dstFolder = nil;
        
        switch (self.destFolderIndex) {
            case MAIL_FOLDER_INBOX:
                dstFolder = [provider allMailFolderPath];
                break;
            case MAIL_FOLDER_DRAFTS:
                dstFolder = [provider draftsFolderPath];
                break;
            case MAIL_FOLDER_SENT:
                dstFolder = [provider sentMailFolderPath];
                break;
            case MAIL_FOLDER_TRASH:
                dstFolder = [provider trashFolderPath];
                break;
                
            default:
                break;
        }
        
        MCOIMAPCopyMessagesOperation * op = [imapSession copyMessagesOperationWithFolder:targetFolder
                                                                                    uids:[MCOIndexSet indexSetWithIndex:self.uid]
                                                                              destFolder:dstFolder];
        [op start:^(NSError * error, NSDictionary * uidMapping) {
            isMoveCompleted = YES;
            NSLog(@"copied to folder with UID %@", uidMapping);
        }];
        op = nil;
    }
}

- (void)startListenFinishLoading {
    [NSThread detachNewThreadSelector:@selector(listen) toTarget:self withObject:nil];
}

- (void)listen {
    @autoreleasepool {
        while (!isMoveCompleted) {
            [NSThread sleepForTimeInterval:0.1f];
        }
        [self performSelectorOnMainThread:@selector(removeMessageFromDataBase) withObject:nil waitUntilDone:YES];
        [self performSelectorOnMainThread:@selector(callOperationCompleted) withObject:nil waitUntilDone:NO];
        executing = NO;
        finished = YES;
        NSLog(@"Sent completed");
    }
}

- (void)removeMessageFromDataBase {
    switch (self.targetFolderIndex) {
        case MAIL_FOLDER_INBOX:
            [[GRMailDataManager sharedManager] removeIndoxMessageByID:self.uid];
            break;
        case MAIL_FOLDER_DRAFTS:
            [[GRMailDataManager sharedManager] removeDraftsMessageByID:self.uid];
            break;
        case MAIL_FOLDER_SENT:
            [[GRMailDataManager sharedManager] removeSentMessageByID:self.uid];
            break;
        case MAIL_FOLDER_TRASH:
            [[GRMailDataManager sharedManager] removeTrashMessageByID:self.uid];
            break;
            
        default:
            break;
    }
}

- (void)callOperationCompleted {
    if ([delegate respondsToSelector:@selector(moveMessageSucceed)]) {
        [delegate performSelector:@selector(moveMessageSucceed) withObject:nil];
    }
}

- (void)operationFailed:(NSError *)error {
//    NSLog(@"!!!!!!!! ERROR: %@", [error localizedDescription]);
//    if ([delegate respondsToSelector:@selector(readMailOperationFailed:)]) {
//        [delegate performSelector:@selector(readMailOperationFailed:) withObject:error];
//    }
    executing = NO;
    finished = YES;
}

- (void)dealloc {
    delegate = nil;
    [super dealloc];
}

@end
