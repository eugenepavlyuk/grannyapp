//
//  GRMailMoveOperation.h
//  GrannyApp
//
//  Created by Ievgen Pavliuk on 7/16/13.
//
//

#import <Foundation/Foundation.h>
#import "GRMailServiceManager.h"

@interface GRMailMoveOperation : NSOperation {
    BOOL     finished;
    BOOL     executing;
    BOOL     isMoveCompleted;
    NSInteger uid;
    NSInteger targetFolderIndex;
    NSInteger destFolderIndex;

    id delegate;

}
@property (nonatomic, assign) id delegate;
@property (nonatomic, assign) NSInteger uid;
@property (nonatomic, assign) NSInteger targetFolderIndex;
@property (nonatomic, assign) NSInteger destFolderIndex;

@end
