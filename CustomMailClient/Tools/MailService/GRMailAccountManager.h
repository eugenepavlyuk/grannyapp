//
//  GRMailAccountManager.h
//  GrannyApp
//
//  Created by Ievgen Pavliuk on 4/12/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "TMailAccount.h"
#import "GRMailDataManager.h"

typedef enum {
    GRMAIL_TYPE_GMAIL = 1,
    GRMAIL_TYPE_HOTMAIL = 2,
    GRMAIL_TYPE_OTHER = 3
} MailType;


@interface GRMailAccountManager : NSObject {

    NSMutableArray *accounts;
    
    MailType mailType;
    BOOL     isBusy;
}

@property (nonatomic, assign) MailType mailType;
@property (nonatomic, assign) BOOL isBusy;

+ (GRMailAccountManager *)manager;

- (void)createAccount:(NSDictionary *)dictionary;
- (NSArray *)getAccounts;
- (void)removeAccount:(TMailAccount *)accountEntity;
- (void)resetAccount;
- (NSMutableDictionary *)getCurrentMailSettings;
- (void)createFakePeople;
@end
