//
//  GRMailAppendOperation.m
//  GrannyApp
//
//  Created by Ievgen Pavliuk on 7/17/13.
//
//

#import "GRMailAppendOperation.h"

@implementation GRMailAppendOperation
@synthesize sentMessage;
@synthesize delegate;
@synthesize targetFolderIndex;

#define TIME_OUT_SENDING 2400

- (BOOL)isExecuting {
	return executing;
}

- (BOOL)isFinished {
	return finished;
}

- (void)start {
    @autoreleasepool {
        finished = NO;
        executing = YES;
        [self runOperation];
        [self startTimeOutListener];
        while (!finished) {
            //NSLog(@"Wait");
            [NSThread sleepForTimeInterval:0.1];
        }
    }
}

- (void)runOperation {
    [self startListenFinishLoading];
    MCOMessageBuilder * builder = [[MCOMessageBuilder alloc] init];
    [[builder header] setFrom:[MCOAddress addressWithDisplayName:@"" mailbox:self.sentMessage.senderMail]];
    NSMutableArray *tos = [NSMutableArray array];
    for (NSString *mail in self.sentMessage.tos) {
        MCOAddress *address = [MCOAddress addressWithMailbox:mail];
        [tos addObject:address];
    }
    [[builder header] setTo:tos];
    [[builder header] setSubject:self.sentMessage.subject];
    [builder setTextBody:self.sentMessage.plainText];
    [builder setHTMLBody:[NSString stringWithFormat:@"<div>%@<div>", self.sentMessage.plainText]];
    for (NSDictionary *atmDict in self.sentMessage.attachments) {
        [builder addAttachment:[MCOAttachment attachmentWithContentsOfFile:[atmDict objectForKey:@"path"]]];
    }
    NSData * rfc822Data = [builder data];
    
    MCOIMAPSession *imapSession = [GRMailServiceManager shared].imapSession;
    
    MCOMailProvider *provider = [[MCOMailProvidersManager sharedManager] providerForEmail:[GRMailServiceManager shared].currentAccount.userName];
    
    NSString       *targetFolder = nil;
    
    switch (self.targetFolderIndex) {
        case MAIL_FOLDER_INBOX:
            targetFolder = @"INBOX";
            break;
        case MAIL_FOLDER_DRAFTS:
            targetFolder = [provider draftsFolderPath];
            break;
        case MAIL_FOLDER_SENT:
            targetFolder = [provider sentMailFolderPath];
            break;
        case MAIL_FOLDER_TRASH:
            targetFolder = [provider trashFolderPath];
            break;
            
        default:
            break;
    }
    
    MCOIMAPAppendMessageOperation * op = (MCOIMAPAppendMessageOperation *)[imapSession appendMessageOperationWithFolder:targetFolder messageData:rfc822Data flags:MCOMessageFlagNone];
    [op start:^(NSError * error, uint32_t createdUID) {
        if (error == nil) {
            NSLog(@"created message with UID %lu", (unsigned long) createdUID);
        }
        isSentCompleted = YES;
    }];
    op = nil;
    [builder release];
}

- (void)startListenFinishLoading {
    [NSThread detachNewThreadSelector:@selector(listen) toTarget:self withObject:nil];
}

- (void)listen {
    @autoreleasepool {
        while (!isSentCompleted) {
            [NSThread sleepForTimeInterval:0.1f];
        }
        [self performSelectorOnMainThread:@selector(callOperationCompleted) withObject:nil waitUntilDone:NO];
        executing = NO;
        finished = YES;
        NSLog(@"Sent completed");
    }
}

- (void)callOperationCompleted {
    if ([delegate respondsToSelector:@selector(appendMessageSucceed)]) {
        [delegate performSelector:@selector(appendMessageSucceed) withObject:nil];
    }
}

- (void)operationFailed:(NSError *)error {
    NSLog(@"!!!!!!!! ERROR: %@", [error localizedDescription]);
//    if ([delegate respondsToSelector:@selector(readMailOperationFailed:)]) {
//        [delegate performSelector:@selector(readMailOperationFailed:) withObject:error];
//    }
    executing = NO;
    finished = YES;
}


- (void)dealloc {
    delegate = nil;
    [sentMessage release];
    [super dealloc];
}

#pragma mark - Time out

- (void)startTimeOutListener {
    [NSThread detachNewThreadSelector:@selector(listenTimeOut) toTarget:self withObject:nil];
    
}

- (void)listenTimeOut {
    @autoreleasepool {
        while (!isTimeOut && !finished) {
            if (tick == TIME_OUT_SENDING) {
                isTimeOut = YES;
                NSMutableDictionary* details = [NSMutableDictionary dictionary];
                [details setValue:@"There is no internet connection. Your message will be sent later" forKey:NSLocalizedDescriptionKey];
                NSError *error = [NSError errorWithDomain:@"Error" code:200 userInfo:details];
                [self performSelectorOnMainThread:@selector(operationFailed:) withObject:error waitUntilDone:NO];
            } else {
                tick++;
            }
            NSLog(@"Listen Time Out");
            [NSThread sleepForTimeInterval:0.1f];
        }
    }
}

@end
