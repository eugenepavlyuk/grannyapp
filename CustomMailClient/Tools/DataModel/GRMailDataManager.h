//
//  GRMailDataManager.h
//  GrannyApp
//
//  Created by Ievgen Pavliuk on 6/19/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "GRContact.h"
#import "GRMailServiceManager.h"
#import "TMailSentMessage.h"
#import "TMailDeletedMessage.h"
#import "TMailDraftMessage.h"
#import "TMailMessage.h"
#import "TMessageAttachment.h"
#import "THeaders.h"
#import "TPendingMessage.h"
#import "GRSMTPMessage.h"

@class MCOIMAPMessage;
@class MCOMessageParser;

@interface GRMailDataManager : NSObject
{
    NSMutableArray *listReadMessages;
    NSMutableArray *listSentMessages;
    NSMutableArray *listDraftMessages;
    NSMutableArray *listTrashMessages;
    
    BOOL fromContact;
    GRContact *contact;
}

@property (nonatomic, retain) NSMutableArray *listReadMessages;
@property (nonatomic, retain) NSMutableArray *listSentMessages;
@property (nonatomic, retain) NSMutableArray *listDraftMessages;
@property (nonatomic, retain) NSMutableArray *listTrashMessages;
@property (nonatomic, retain) GRContact *contact;
@property BOOL fromContact;

+ (GRMailDataManager *)sharedManager;
- (BOOL)isMessageExistsWithID:(NSString *)messageID;
- (BOOL)isSentMessageExistsWithID:(NSString *)messageID;

- (NSArray *)getAllMessagesForAccountName:(NSString *)accountName;
- (NSArray *)getAllSentMessagesForAccountName:(NSString *)accountName;
- (NSArray *)getAllDraftMessagesForAccountName:(NSString *)accountName;
- (NSArray *)getAllTrashMessagesForAccountName:(NSString *)accountName;
- (NSArray *)getAllInboxMessagesForEmail:(NSString *)email accountName:(NSString *)accountName;
- (NSArray *)getAllAccountsByName:(NSString *)accountName;

#pragma mark - contacts
- (NSMutableArray *)getAllToContactsForDraftMessage:(TMailDraftMessage *)draftMessage;
- (NSMutableArray *)getAllCCContactsForDraftMessage:(TMailDraftMessage *)draftMessage;

#pragma mark - new methods
- (void)saveInboxMessage:(MCOIMAPMessage *)message;
- (void)saveSentMessage:(MCOIMAPMessage *)message;
- (void)saveTrashMessage:(MCOIMAPMessage *)message;
- (void)saveDraftMessage:(MCOIMAPMessage *)message;
- (void)saveMessageHeader:(MCOIMAPMessage *)message folderID:(NSInteger)folderID ;
- (NSArray *)findHeaderMessageByID:(NSInteger)messageID  folderID:(NSInteger)folderID;
- (NSArray *)findHeaderMessagesByFolderID:(NSInteger)folderID;
- (NSArray *)findHeadersByAccountName:(NSString *)accountName;

- (void)removeIndoxMessageByID:(NSInteger)uid;
- (void)removeSentMessageByID:(NSInteger)uid;
- (void)removeDraftsMessageByID:(NSInteger)uid;
- (void)removeTrashMessageByID:(NSInteger)uid;

#pragma mark - fetch messages
- (void)fetchMessage:(MCOMessageParser *)message uid:(NSInteger)uid folderIndex:(NSInteger)folderIndex ;


#pragma mark - Attachmnets
- (void)saveAttachemntWithName:(NSString *)fileName
                   contentType:(NSString *)contentType
                     messageID:(NSInteger)uid
                      uniqueId:(NSString*)uniqueId
                      filePath:(NSString *)filePath;

- (NSArray *)getAttachmentsForMessageID:(NSInteger)uid;
- (NSArray *)getAllAttachmentsForAccount:(TMailAccount *)accountEntity;

#pragma mark - Pending message
- (void)savePendingMessage:(GRSMTPMessage *)sMessage;
- (void)removePendingMessageWithID:(NSInteger)uid;
- (NSArray *)getAllPendingMessagesForAccount:(NSString *)accountName;

- (BOOL)isTrashMessageExistsWithID:(NSString *)messageID;
- (BOOL)isDraftMessageExistsWithID:(NSString *)messageID;

- (NSArray *)messageWithID:(NSString *)messageID;
- (NSArray *)sentMessageWithID:(NSString *)messageID;
- (NSArray *)draftMessageWithID:(NSString *)messageID;
- (NSArray *)trashMessageWithID:(NSString *)messageID;

@end
