//
//  Settings.h
//  GrannyApp
//
//  Created by Alexander Ermolaev on 5/14/13.
//
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

#define kSettingsEntityName     @"Settings"

@interface Settings : NSManagedObject

@property (nonatomic, retain) NSNumber * clockOn;
@property (nonatomic, retain) NSString * contact_name;
@property (nonatomic, retain) NSString * contact_phone;
@property (nonatomic, retain) NSString * contact_picture;
@property (nonatomic, retain) NSString * doctor_name;
@property (nonatomic, retain) NSString * doctor_phone;
@property (nonatomic, retain) NSString * doctor_picture;
@property (nonatomic, retain) NSNumber * duration;
@property (nonatomic, retain) NSString * home_name;
@property (nonatomic, retain) NSString * home_phone;
@property (nonatomic, retain) NSString * home_picture;
@property (nonatomic, retain) NSString * hospital_name;
@property (nonatomic, retain) NSString * hospital_phone;
@property (nonatomic, retain) NSString * hospital_picture;
@property (nonatomic, retain) NSNumber * imageOn;
@property (nonatomic, retain) NSString * linkToImage;
@property (nonatomic, retain) NSNumber * photoSliderOn;
@property (nonatomic, retain) NSNumber * radioOn;
@property (nonatomic, retain) NSNumber * screenSaverState;
@property (nonatomic, retain) NSNumber * slidingTimeMinutes;
@property (nonatomic, retain) NSNumber * slidingTimeSeconds;
@property (nonatomic, retain) NSNumber * snoozeDuration;
@property (nonatomic, retain) NSNumber * timeFormat;
@property (nonatomic, retain) NSNumber * weedStartsFrom;
@property (nonatomic, retain) NSString * personalMessage;

@end
