//
//  THeaders.h
//  GrannyApp
//
//  Created by Ievgen Pavliuk on 7/9/13.
//
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>


@interface THeaders : NSManagedObject

@property (nonatomic, retain) NSNumber * uid;
@property (nonatomic, retain) NSNumber * folderID;
@property (nonatomic, retain) NSString * accountName;
@property (nonatomic, retain) NSNumber * deleted;

@end
