//
//  TMailAccount.h
//  GrannyApp
//
//  Created by Ievgen Pavliuk on 4/17/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>


@interface TMailAccount : NSManagedObject

@property (nonatomic, retain) NSString * accountName;
@property (nonatomic, retain) NSString * type;
@property (nonatomic, retain) NSString * server;
@property (nonatomic, retain) NSNumber * port;
@property (nonatomic, retain) NSString * userName;
@property (nonatomic, retain) NSString * password;
@property (nonatomic, retain) NSNumber * isActive;
@property (nonatomic, retain) NSNumber * isSSL;
@property (nonatomic, retain) NSNumber * smtpPort;
@property (nonatomic, retain) NSString * smtpServer;
@property (nonatomic, retain) NSNumber * connectionType;

@end
