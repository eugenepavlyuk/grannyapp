//
//  TVChannel.m
//  GrannyApp
//
//  Created by Ievgen Pavliuk on 9/3/12.
//
//

#import "TVChannel.h"


@implementation TVChannel

@dynamic channelID;
@dynamic channelTitle;
@dynamic channelUrl;

@dynamic language;
@dynamic country;
@dynamic genre;

@end
