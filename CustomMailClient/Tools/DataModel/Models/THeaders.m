//
//  THeaders.m
//  GrannyApp
//
//  Created by Ievgen Pavliuk on 7/9/13.
//
//

#import "THeaders.h"


@implementation THeaders

@dynamic uid;
@dynamic folderID;
@dynamic accountName;
@dynamic deleted;

@end
