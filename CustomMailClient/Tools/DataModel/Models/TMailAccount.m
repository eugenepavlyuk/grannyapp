//
//  TMailAccount.m
//  GrannyApp
//
//  Created by Ievgen Pavliuk on 4/17/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "TMailAccount.h"


@implementation TMailAccount

@dynamic accountName;
@dynamic type;
@dynamic server;
@dynamic port;
@dynamic userName;
@dynamic password;
@dynamic isActive;
@dynamic isSSL;
@dynamic smtpPort;
@dynamic smtpServer;
@dynamic connectionType;

@end
