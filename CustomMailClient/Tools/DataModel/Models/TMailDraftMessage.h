//
//  TMailDraftMessage.h
//  GrannyApp
//
//  Created by Ievgen Pavliuk on 9/7/12.
//
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>
#import "TMailMessageBase.h"

@interface TMailDraftMessage : TMailMessageBase

@end
