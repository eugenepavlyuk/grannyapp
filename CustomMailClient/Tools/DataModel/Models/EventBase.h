//
//  EventBase.h
//  GrannyApp
//
//  Created by Eugene Pavluk on 4/9/14.
//
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

typedef enum {
    ET_None = -1,
    ET_Daily = 0,
    ET_Weekly = 1,
    ET_Monthly = 2,
    ET_Annually = 3,
    ET_Once = 4
} EventType;

typedef enum {
    MT_Monday = 1,
    MT_Tuesday = 2,
    MT_Wednesday = 3,
    MT_Thursday = 4,
    MT_Friday = 5,
    MT_Saturday = 6,
    MT_Sunday = 7
} MedicineAlarmType;

#define kNameKey            @"name"
#define kEventIdKey         @"eventId"

#define kWeekdaysKey        @"weekDays"
#define kEventTypeKey       @"eventType"
#define kDayKey             @"day"
#define kMonthKey           @"month"
#define kYearKey            @"year"
#define kHourTimeKey        @"hourTime"
#define kMinuteTimeKey      @"minuteTime"

#define kHasSnoozeTimeKey   @"hasSnoozeTime"
#define kHasNotificationKey @"hasNotification"

#define kEventBaseEntityName     @"EventBase"

@interface EventBase : NSManagedObject

@property (nonatomic, retain) NSDate * date;
@property (nonatomic, retain) NSNumber * eventId;
@property (nonatomic, retain) NSNumber * expired;
@property (nonatomic, retain) NSNumber * hasNotification;
@property (nonatomic, retain) NSNumber * hasSnoozeTime;
@property (nonatomic, retain) NSString * name;
@property (nonatomic, retain) NSNumber * persistent;
@property (nonatomic, retain) NSNumber * type;
@property (nonatomic, retain) NSNumber * minute;
@property (nonatomic, retain) NSNumber * hour;

@property (nonatomic, retain) NSDate * snoozeDate;

- (unsigned int)minutesSinceMidnight;
- (unsigned int)durationInMinutes;

- (void)scheduleLocalNotifications;

- (void)rescheduleEvent;

- (BOOL)isMatch;
- (BOOL)isEvent;
- (BOOL)hasImage;
- (NSDate*)notifyDate;
- (NSString*)sound;

- (UILocalNotification*)localNotification;
+ (UILocalNotification*)localNotification;

@end
