//
//  GRContact.h
//  GrannyApp
//
//  Created by Eugene Pavlyuk on 6/13/12.
//  Copyright (c) 2012 Home. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <AddressBook/AddressBook.h>

@interface GRContact : NSObject <NSCoding, NSCopying>
{
   NSString * firstname;
   NSString * fullname;
   NSString * lastname;
   NSString * photourl;
   NSString * photoPreviewUrl;
    
   NSString * phoneMobile;
   NSString * phoneiPhone;
   NSString * phoneHome;
   NSString * phoneWork;
   NSString * phoneMain;
   NSString * phoneHomeFax;
   NSString * phoneWorkFax;
   NSString * phoneOtherFax;
   NSString * phonePager;
    
   NSString * phoneSkype;
    
   NSString * email;
   NSString * email1;
   NSString * email2;
   NSString * email3;
   NSString * email4;
   NSString * email5;
   NSString * selectedEmail;
    
   NSString * country;
   NSString * city;
   NSString * street;
   NSString * postCode;
   NSString * title;
   NSNumber *recId;
   BOOL favorite;
}

@property (nonatomic, copy) NSString * fullContactName;

@property (nonatomic, copy) NSString * firstname;
@property (nonatomic, copy) NSString * fullname;
@property (nonatomic, copy) NSString * lastname;
@property (nonatomic, copy) NSString * photourl;
@property (nonatomic, copy) NSString * photoPreviewUrl;

@property (nonatomic, copy) NSString * phoneMobile;
@property (nonatomic, copy) NSString * phoneiPhone;
@property (nonatomic, copy) NSString * phoneHome;
@property (nonatomic, copy) NSString * phoneWork;
@property (nonatomic, copy) NSString * phoneMain;
@property (nonatomic, copy) NSString * phoneHomeFax;
@property (nonatomic, copy) NSString * phoneWorkFax;
@property (nonatomic, copy) NSString * phoneOtherFax;
@property (nonatomic, copy) NSString * phonePager;

@property (nonatomic, copy) NSString * phoneSkype;

@property (nonatomic, copy) NSString * email;
@property (nonatomic, copy) NSString * email1;
@property (nonatomic, copy) NSString * email2;
@property (nonatomic, copy) NSString * email3;
@property (nonatomic, copy) NSString * email4;
@property (nonatomic, copy) NSString * email5;
@property (nonatomic, copy) NSString * selectedEmail;

@property (nonatomic, copy) NSString * country;
@property (nonatomic, copy) NSString * city;
@property (nonatomic, copy) NSString * street;
@property (nonatomic, copy) NSString * postCode;
@property (nonatomic, copy) NSString * title;
@property (nonatomic, copy) NSNumber *recId;
@property (nonatomic, assign, getter = isFavorite) BOOL favorite;

+ (GRContact*)contactWithRecord:(ABRecordRef)record;

- (NSString*)titleForPhoneSectionForPhone:(NSString*)candidate;
- (NSString*)titleForEmailSectionForEmail:(NSString*)candidate;

@end
