//
//  RecientContact.m
//  GrannyApp
//
//  Created by Eugene Pavlyuk on 5/23/12.
//  Copyright (c) 2012 Home. All rights reserved.
//

#import "RecientContact.h"


@implementation RecientContact

@dynamic firstname;
@dynamic lastname;
@dynamic photourl;
@dynamic phone;
@dynamic email;

@end
