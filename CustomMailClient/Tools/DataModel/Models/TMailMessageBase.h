//
//  TMailMessageBase.h
//  GrannyApp
//
//  Created by admin on 09/12/13.
//
//

#import <CoreData/CoreData.h>

@interface TMailMessageBase : NSManagedObject

@property (nonatomic, retain) NSString * accountName;
@property (nonatomic, retain) NSDate * date;
@property (nonatomic, retain) NSNumber * isFetched;
@property (nonatomic, retain) NSNumber * isRead;
@property (nonatomic, retain) NSString * mailType;
@property (nonatomic, retain) NSString * messageBodyHTML;
@property (nonatomic, retain) NSString * messageBodyPlain;
@property (nonatomic, retain) NSString * messageID;
@property (nonatomic, retain) NSString * receivers;
@property (nonatomic, retain) NSString * senderEmail;
@property (nonatomic, retain) NSString * senderName;
@property (nonatomic, retain) NSString * senderPhotoUrl;
@property (nonatomic, retain) NSString * subject;
@property (nonatomic, retain) NSData * tos;
@property (nonatomic, retain) NSNumber * uid;
@property (nonatomic, retain) NSNumber *isInlineAttachments;

@end
