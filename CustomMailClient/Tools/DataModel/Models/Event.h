//
//  Event.h
//  GrannyApp
//
//  Created by Eugene Pavlyuk on 8/9/12.
//  Copyright (c) 2012 Home. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>
#import "EventBase.h"

#define kYearEndKey         @"yearEnd"
#define kMonthEndKey        @"monthEnd"
#define kDayEndKey          @"dayEnd"
#define kHourTimeEndKey     @"hourTimeEnd"
#define kMinuteTimeEndKey   @"minuteTimeEnd"

#define kEventEntityName     @"Event"

@interface Event : EventBase

@property (nonatomic, retain) NSDate * endDate;
@property (nonatomic, retain) NSNumber * month;
@property (nonatomic, retain) NSNumber * year;
@property (nonatomic, retain) NSNumber * day;

@property (nonatomic, retain) NSNumber * monthEnd;
@property (nonatomic, retain) NSNumber * yearEnd;
@property (nonatomic, retain) NSNumber * dayEnd;
@property (nonatomic, retain) NSNumber * minuteEnd;
@property (nonatomic, retain) NSNumber * hourEnd;

- (BOOL)hasImage;

@end
