//
//  RadioChannel.h
//  GrannyApp
//
//  Created by Eugene Pavluk on 7/25/13.
//
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>


@interface RadioChannel : NSManagedObject

@property (nonatomic, retain) NSString * channelID;
@property (nonatomic, retain) NSString * channelTitle;
@property (nonatomic, retain) NSString * channelUrl;
@property (nonatomic, retain) NSString * country;
@property (nonatomic, retain) NSString * genre;
@property (nonatomic, retain) NSString * language;

@end
