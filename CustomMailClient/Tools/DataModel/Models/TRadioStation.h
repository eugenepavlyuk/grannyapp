//
//  TRadioStation.h
//  GrannyApp
//
//  Created by Ievgen Pavliuk on 8/1/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>


@interface TRadioStation : NSManagedObject

@property (nonatomic, retain) NSString * title;
@property (nonatomic, retain) NSString * strimUrl;
@property (nonatomic, retain) NSString * stationID;
@property (nonatomic, retain) NSString * logoPath;

@end
