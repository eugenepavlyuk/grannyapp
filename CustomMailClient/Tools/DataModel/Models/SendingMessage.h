//
//  SendingMessage.h
//  GrannyApp
//
//  Created by Alexander Ermolaev on 6/12/13.
//
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>


@interface SendingMessage : NSManagedObject

@property (nonatomic, retain) id account;
@property (nonatomic, retain) id message;

@end
