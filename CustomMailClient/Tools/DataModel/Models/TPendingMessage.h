//
//  TPendingMessage.h
//  GrannyApp
//
//  Created by Ievgen Pavliuk on 7/15/13.
//
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>


@interface TPendingMessage : NSManagedObject

@property (nonatomic, retain) NSNumber * uid;
@property (nonatomic, retain) NSString * senderMail;
@property (nonatomic, retain) NSData * tos;
@property (nonatomic, retain) NSData * attachments;
@property (nonatomic, retain) NSString * subject;
@property (nonatomic, retain) NSString * textPlain;
@property (nonatomic, retain) NSString * textHTML;
@property (nonatomic, retain) NSString * accountName;

@end
