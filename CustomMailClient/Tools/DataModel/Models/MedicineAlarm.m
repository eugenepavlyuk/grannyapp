//
//  MedicineAlarm.m
//  GrannyApp
//
//  Created by Eugene Pavluk on 9/24/13.
//
//

#import "MedicineAlarm.h"
#import "NSDate+Calendar.h"
#import "AlarmManager.h"
#import "DataManager.h"
#import "Settings.h"

@implementation MedicineAlarm

@dynamic medicineType;
@dynamic weekDays;
@dynamic eventIdentifier;

- (NSDate*)notifyDate
{
    if ([self.type intValue] == ET_Daily) // daily event
    {
        NSDate *date = [NSDate dateWithYear:[[NSDate date] year]
                                      month:[[NSDate date] month]
                                        day:[[NSDate date] day]
                                       hour:[self.hour integerValue]
                                     minute:[self.minute integerValue]
                                     second:0];
        
        return date;
    }
    else if ([self.type intValue] == ET_Weekly)
    {
        NSInteger weekDay = [[NSDate date] weekday] == 1 ? 7 : [[NSDate date] weekday] - 1;
        
        if ([self.weekDays containsObject:@(weekDay)])
        {
            NSDate *date = [[NSDate date] dateBySettingHour:[self.hour integerValue]
                                                     minute:[self.minute integerValue]
                                                     second:0];
            
            return date;
        }
        else
        {
            [NSDate dateWithYear:0 month:0 day:0];
        }
    }
    else if([self.type intValue] == ET_None)
    {
        return [NSDate dateWithYear:0 month:0 day:0];
    }
    
    return nil;
}

- (BOOL)isMatch
{
    NSInteger secondTime  = [[NSDate date] second];
    
    if (secondTime != 0)
    {
        return NO;
    }
    
    NSDate *now = [NSDate dateWithYear:[[NSDate date] year]
                                 month:[[NSDate date] month]
                                   day:[[NSDate date] day]
                                  hour:[[NSDate date] hour]
                                minute:[[NSDate date] minute]
                                second:0];
    
    NSDate *testDate = nil;
    
    if ([self.hasSnoozeTime boolValue])
    {
        if ([[self notifyDate] isLessDate:now]) // passed
        {
            if ([self.snoozeDate isLessDate:now]) // passed
            {
                return NO;
            }
            else if (!self.snoozeDate)
            {
                return NO;
            }
            else
            {
                testDate = self.snoozeDate;
            }
        }
        else
        {
            testDate = [self notifyDate];
        }
    }
    else
    {
        if ([[self notifyDate] isLessDate:now]) // passed
        {
            return NO;
        }
        else
        {
            testDate = [self notifyDate];
        }
    }
    
    NSInteger hour    = [testDate hour];
    NSInteger minute  = [testDate minute];
    
    NSInteger hourTime    = [[NSDate date] hour];
    NSInteger minuteTime  = [[NSDate date] minute];
    
    if ([self.type intValue] == ET_Daily)
    {
        if ((hour == hourTime) && (minute == minuteTime))
        {
            return YES;
        }
    }
    else if ([self.type intValue] == ET_Weekly)
    {
        NSInteger weekDay = [[NSDate date] weekday] == 1 ? 7 : [[NSDate date] weekday] - 1;
        
        if ([self.weekDays containsObject:@(weekDay)])
        {
            if ((hour == hourTime) && (minute == minuteTime))
            {
                return YES;
            }
        }
    }
    else
    {
        return NO;
    }
    
    return NO;
}

- (void)scheduleLocalNotifications
{
    [super scheduleLocalNotifications];
    
    if ([self.type integerValue] == ET_None || [self.type integerValue] == ET_Weekly)
    {
        if ([self.type integerValue] == ET_Weekly && [AlarmManager sharedInstance].eventStore)
        {
            EKCalendar *calendar = [[AlarmManager sharedInstance].eventStore defaultCalendarForNewEvents];
            
            EKEvent *event = [[AlarmManager sharedInstance].eventStore eventWithIdentifier:self.eventIdentifier];
            
            if (!event)
            {
                event = [EKEvent eventWithEventStore:[AlarmManager sharedInstance].eventStore];
            }
            
            event.calendar = calendar;
            event.title = self.name;
            event.notes = @"";
            event.startDate = [self notifyDate];
            event.endDate = [[self notifyDate] dateByAddingMinute:5];
            
            NSMutableArray *days = [NSMutableArray array];
            
            for (NSNumber *day in self.weekDays)
            {
                NSInteger intDay = [day integerValue];
                
                if (intDay == 7)
                {
                    [days addObject:[EKRecurrenceDayOfWeek dayOfWeek:1]];
                }
                else
                {
                    [days addObject:[EKRecurrenceDayOfWeek dayOfWeek:intDay + 1]];
                }
            }
            
            EKRecurrenceRule *er = [[EKRecurrenceRule alloc] initRecurrenceWithFrequency:EKRecurrenceFrequencyWeekly
                                                                                interval:1
                                                                           daysOfTheWeek:days
                                                                          daysOfTheMonth:nil
                                                                         monthsOfTheYear:nil
                                                                          weeksOfTheYear:nil
                                                                           daysOfTheYear:nil
                                                                            setPositions:nil end:nil];
            
            [event addRecurrenceRule:er];
            
            NSError *error = nil;
            
            NSArray *alarms = event.alarms;
            
            for (EKAlarm *alarm in alarms)
            {
                [event removeAlarm:alarm];
            }
            
            Settings *settings = [[DataManager sharedInstance] getEntityWithName:kSettingsEntityName];
            
            for (int i = 0; i < 0; i++)
            {
                EKAlarm *alarm = [[EKAlarm alloc] init];
                
                NSTimeInterval timeInterval = i * [settings.snoozeDuration integerValue] * 60;
                
                alarm.relativeOffset = timeInterval;
                [event addAlarm:alarm];
            }
            
            [[AlarmManager sharedInstance].eventStore saveEvent:event span:EKSpanFutureEvents commit:YES error:&error];
            
            if (error) {
                NSLog(@"event save error: %@", [error description]);
            } else {
                self.eventIdentifier = event.eventIdentifier;
                
                [[DataManager sharedInstance] save];
            }
        }
        
        return ;
    }
    
    UILocalNotification *localNotification = [self localNotification];
    BOOL osVersionLessThan7 = ([[UIDevice currentDevice].systemVersion floatValue] < 7.0);
    localNotification.repeatInterval = (osVersionLessThan7) ? NSDayCalendarUnit : NSCalendarUnitDay;
    
    [[UIApplication sharedApplication] scheduleLocalNotification:localNotification];
    
    if (![self.hasSnoozeTime boolValue] || !self.snoozeDate)
    {
        return ;
    }
    
    localNotification = [self localNotification];
    
    localNotification.fireDate = self.snoozeDate;
    localNotification.repeatInterval = (osVersionLessThan7) ? NSDayCalendarUnit : NSCalendarUnitDay;
    localNotification.userInfo = @{kEventIdKey : self.eventId, @"isNotification" : @(YES)};
    
    [[UIApplication sharedApplication] scheduleLocalNotification:localNotification];
}

- (void)removeEvent
{
    EKEvent *event = [[AlarmManager sharedInstance].eventStore eventWithIdentifier:self.eventIdentifier];
    
    if (event)
    {
        [[AlarmManager sharedInstance].eventStore removeEvent:event span:EKSpanFutureEvents commit:YES error:nil];
    }
}

- (NSString*)sound
{
    return @"marimba";
}

@end
