//
//  TMessageAttachment.h
//  GrannyApp
//
//  Created by Ievgen Pavliuk on 7/19/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>


@interface TMessageAttachment : NSManagedObject

@property (nonatomic, retain) NSString * fileName;
@property (nonatomic, retain) NSNumber * messageID;
@property (nonatomic, retain) NSString * contentType;
@property (nonatomic, retain) NSString * uniqueId;
@property (nonatomic, retain) NSString * filePath;
@property (nonatomic, retain) NSString * accountName;

@end
