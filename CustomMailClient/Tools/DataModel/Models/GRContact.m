//
//  GRContact.m
//  GrannyApp
//
//  Created by Eugene Pavlyuk on 6/13/12.
//  Copyright (c) 2012 Home. All rights reserved.
//

#import "GRContact.h"

@implementation GRContact

@synthesize firstname;
@synthesize fullname;
@synthesize lastname;
@synthesize photourl;

@synthesize phoneMobile;
@synthesize phoneiPhone;
@synthesize phoneHome;
@synthesize phoneWork;
@synthesize phoneMain;
@synthesize phoneHomeFax;
@synthesize phoneWorkFax;
@synthesize phoneOtherFax;
@synthesize phonePager;

@synthesize phoneSkype;

@synthesize email;
@synthesize email1;
@synthesize email2;
@synthesize email3;
@synthesize email4;
@synthesize email5;
@synthesize selectedEmail;

@synthesize recId;
@synthesize favorite;
@synthesize postCode;
@synthesize street;
@synthesize city;
@synthesize country;
@synthesize title;
@synthesize photoPreviewUrl;

+ (GRContact*)contactWithRecord:(ABRecordRef)record
{
    GRContact *contact = [[GRContact alloc] init];
    
    NSString *firstname = (__bridge NSString*)ABRecordCopyValue(record, kABPersonFirstNameProperty);
    NSString *lastname = (__bridge NSString*)ABRecordCopyValue(record, kABPersonLastNameProperty);
    NSString *jobTitle = (__bridge NSString*)ABRecordCopyValue(record, kABPersonJobTitleProperty);
    
    ABRecordID recordId = ABRecordGetRecordID(record);
    
    ABMultiValueRef phones = ABRecordCopyValue(record, kABPersonPhoneProperty);
    
    for(CFIndex i = 0; i < ABMultiValueGetCount(phones); i++)
    {
        NSString* mobileLabel = (__bridge NSString*)ABMultiValueCopyLabelAtIndex(phones, i);
        
        if([mobileLabel isEqualToString:(NSString *)kABPersonPhoneMobileLabel])
        {
            NSString *mobilePhone = (__bridge NSString*)ABMultiValueCopyValueAtIndex(phones, i);
            
            contact.phoneMobile = mobilePhone;
        }
        else if ([mobileLabel isEqualToString:(NSString*)kABPersonPhoneIPhoneLabel])
        {
            NSString *mobileiPhone = (__bridge NSString*)ABMultiValueCopyValueAtIndex(phones, i);
            
            contact.phoneiPhone = mobileiPhone;
        }
        else if ([mobileLabel isEqualToString:(NSString*)kABHomeLabel])
        {
            NSString *mobileHome = (__bridge NSString*)ABMultiValueCopyValueAtIndex(phones, i);
            
            contact.phoneHome = mobileHome;
        }
        else if ([mobileLabel isEqualToString:(NSString*)kABWorkLabel])
        {
            NSString *mobileWork = (__bridge NSString*)ABMultiValueCopyValueAtIndex(phones, i);
            
            contact.phoneWork = mobileWork;
        }
        else if ([mobileLabel isEqualToString:(NSString*)kABPersonPhoneMainLabel])
        {
            NSString *mobileMain = (__bridge NSString*)ABMultiValueCopyValueAtIndex(phones, i);
            
            contact.phoneMain = mobileMain;
        }
        else if ([mobileLabel isEqualToString:(NSString*)kABPersonPhoneHomeFAXLabel])
        {
            NSString *mobileHomeFax = (__bridge NSString*)ABMultiValueCopyValueAtIndex(phones, i);
            
            contact.phoneHomeFax = mobileHomeFax;
        }
        else if ([mobileLabel isEqualToString:(NSString*)kABPersonPhoneWorkFAXLabel])
        {
            NSString *mobileWorkFax = (__bridge NSString*)ABMultiValueCopyValueAtIndex(phones, i);
            
            contact.phoneWorkFax = mobileWorkFax;
        }
        else if ([mobileLabel isEqualToString:(NSString*)kABPersonPhoneOtherFAXLabel])
        {
            NSString *mobileOtherFax = (__bridge NSString*)ABMultiValueCopyValueAtIndex(phones, i);
            
            contact.phoneOtherFax = mobileOtherFax;
        }
        else if ([mobileLabel isEqualToString:(NSString*)kABPersonPhonePagerLabel])
        {
            NSString *mobilePager = (__bridge NSString*)ABMultiValueCopyValueAtIndex(phones, i);
            
            contact.phonePager = mobilePager;
        }
    }
    
    CFRelease(phones);
    
    ABMultiValueRef emails = ABRecordCopyValue(record, kABPersonEmailProperty);
    
    NSString* email = @"";
    NSString* email1 = @"";
    NSString* email2 = @"";
    NSString* email3 = @"";
    NSString* email4 = @"";
    NSString* email5 = @"";
    
    for(CFIndex i = 0; i < ABMultiValueGetCount(emails); i++)
    {
        switch (i) {
            case 0:
                email = (__bridge NSString*)ABMultiValueCopyValueAtIndex(emails, i);
                break;
                
            case 1:
                email1 = (__bridge NSString*)ABMultiValueCopyValueAtIndex(emails, i);
                break;
                
            case 2:
                email2 = (__bridge NSString*)ABMultiValueCopyValueAtIndex(emails, i);
                break;
                
            case 3:
                email3 = (__bridge NSString*)ABMultiValueCopyValueAtIndex(emails, i);
                break;
                
            case 4:
                email4 = (__bridge NSString*)ABMultiValueCopyValueAtIndex(emails, i);
                break;
                
            case 5:
                email5 = (__bridge NSString*)ABMultiValueCopyValueAtIndex(emails, i);
                break;
                
            default:
                break;
        }
        
        if (i > 5)
        {
            break;
        }
    }
    
    CFRelease(emails);
    
    NSString *skype = nil;
    
    if ([[UIDevice currentDevice].systemVersion floatValue] >= 5.f)
    {
        ABMultiValueRef messangers = ABRecordCopyValue(record, kABPersonInstantMessageProperty);
        
        for(CFIndex x = 0; x < ABMultiValueGetCount(messangers); x++)
        {
            CFDictionaryRef dict = ABMultiValueCopyValueAtIndex(messangers, x);
            
            NSDictionary *nsdict = (__bridge NSDictionary *)dict;
            
            if ([[nsdict valueForKey:@"service"] isEqualToString:@"Skype"])
            {
                skype = [[nsdict valueForKey:@"username"] copy];
            }
            
            if (dict)
            {
                CFRelease(dict);
            }
        }
        
        if (messangers)
        {
            CFRelease(messangers);
        }
    }
    
    NSData *imgData = (__bridge NSData *)ABPersonCopyImageData(record);
    
    if (imgData)
    {
        NSArray *paths = NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES);
        NSString *dirPath = [paths objectAtIndex:0];
        NSString *prevdirPath = [paths objectAtIndex:0];
        
        NSString *fileName = [[NSString stringWithFormat:@"%i", (int)recordId] stringByAppendingPathExtension:@"png"];
        NSString *prevFileName = [[NSString stringWithFormat:@"preview%i", (int)recordId] stringByAppendingPathExtension:@"png"];
        
        dirPath = [dirPath stringByAppendingPathComponent:fileName];
        prevdirPath = [prevdirPath stringByAppendingPathComponent:prevFileName];
        
        [[NSFileManager defaultManager] createFileAtPath:dirPath contents:imgData attributes:nil];
        
        contact.photourl = dirPath;
        
        if ([[NSFileManager defaultManager] fileExistsAtPath:prevdirPath])
        {
            [[NSFileManager defaultManager] removeItemAtPath:prevdirPath error:nil];
        }
        
        if (![[NSFileManager defaultManager] fileExistsAtPath:prevdirPath])
        {
            [[NSFileManager defaultManager] makePreviewImageForData:imgData path:prevdirPath];
        }
        
        contact.photoPreviewUrl = prevdirPath;
    }
    
    contact.lastname = lastname;
    contact.firstname = firstname;
    
    contact.email = email;
    contact.email1 = email1;
    contact.email2 = email2;
    contact.email3 = email3;
    contact.email4 = email4;
    contact.email5 = email5;
    contact.selectedEmail = nil;
    
    contact.recId = @(recordId);
    contact.phoneSkype = skype;
    
    NSString* country = nil;
    NSString* city = nil;
    NSString* street = nil;
    NSString* postCode = nil;
    
    
    ABMultiValueRef aMulti = ABRecordCopyValue(record, kABPersonAddressProperty);
    
    if (aMulti != nil)
    {
        int aMultiCount = ABMultiValueGetCount(aMulti);
        
        for (int i = 0; i < aMultiCount; ++i)
        {
            NSDictionary *abDict = (__bridge NSDictionary *)ABMultiValueCopyValueAtIndex(aMulti, i);
            
            country = [[abDict objectForKey:(NSString *)kABPersonAddressCountryKey] copy];
            city = [[abDict objectForKey:(NSString *)kABPersonAddressCityKey] copy];
            street = [[abDict objectForKey:(NSString *)kABPersonAddressStreetKey] copy];
            postCode = [[abDict objectForKey:(NSString *)kABPersonAddressZIPKey] copy];
        }
        
        CFRelease(aMulti);
    }
    
    contact.city = city;
    contact.street = street;
    contact.country = country;
    contact.postCode = postCode;
    contact.title = jobTitle;
    
    NSMutableString *name = [NSMutableString string];
    
    if ([contact.firstname length])
    {
        [name appendString:contact.firstname];
    }
    
    if ([contact.lastname length])
    {
        if ([name length])
        {
            [name appendString:@" "];
        }
        
        [name appendString:contact.lastname];
    }
    
    contact.fullname = [name lowercaseString];
    
    return contact;
}

- (id)initWithCoder:(NSCoder *)aDecoder
{
    if ((self=[super init]))
    {        
        firstname = [aDecoder decodeObjectForKey:@"firstname"];
        fullname = [aDecoder decodeObjectForKey:@"fullname"];
        lastname = [aDecoder decodeObjectForKey:@"lastname"];
        photourl = [aDecoder decodeObjectForKey:@"photourl"];
        photoPreviewUrl = [aDecoder decodeObjectForKey:@"photoPreviewUrl"];
        
        phoneMobile = [aDecoder decodeObjectForKey:@"phoneMobile"];
        email = [aDecoder decodeObjectForKey:@"email"];
        email1 = [aDecoder decodeObjectForKey:@"email1"];
        email2 = [aDecoder decodeObjectForKey:@"email2"];
        email3 = [aDecoder decodeObjectForKey:@"email3"];
        email4 = [aDecoder decodeObjectForKey:@"email4"];
        email5 = [aDecoder decodeObjectForKey:@"email5"];
        selectedEmail = [aDecoder decodeObjectForKey:@"selectedEmail"];
    }
    return self;
}

- (void)encodeWithCoder:(NSCoder *)encoder
{
    if (self.firstname) [encoder encodeObject:self.firstname forKey:@"firstname"];
    if (self.fullname) [encoder encodeObject:self.fullname forKey:@"type"];
    if (self.lastname) [encoder encodeObject:self.lastname forKey:@"lastname"];
    if (self.photourl) [encoder encodeObject:self.photourl forKey:@"photourl"];
    if (self.photoPreviewUrl) [encoder encodeObject:self.photoPreviewUrl forKey:@"photoPreviewUrl"];
    
    if (self.phoneMobile) [encoder encodeObject:self.phoneMobile forKey:@"phoneMobile"];
    
    if (self.email) [encoder encodeObject:self.email forKey:@"email"];
    if (self.email1) [encoder encodeObject:self.email1 forKey:@"email1"];
    if (self.email2) [encoder encodeObject:self.email2 forKey:@"email2"];
    if (self.email3) [encoder encodeObject:self.email3 forKey:@"email3"];
    if (self.email4) [encoder encodeObject:self.email4 forKey:@"email4"];
    if (self.email5) [encoder encodeObject:self.email5 forKey:@"email5"];
    if (self.selectedEmail) [encoder encodeObject:self.selectedEmail forKey:@"selectedEmail"];
}

- (void)dealloc
{
    self.firstname = nil;
    self.fullname = nil;
    self.lastname = nil;
    self.photourl = nil;
    
    self.phoneMobile = nil;
    self.phoneiPhone = nil;
    self.phoneHome = nil;
    self.phoneWork = nil;
    self.phoneMain = nil;
    self.phoneHomeFax = nil;
    self.phoneWorkFax = nil;
    self.phoneOtherFax = nil;
    self.phonePager = nil;
    
    self.phoneSkype = nil;
    
    self.email = nil;
    self.email1 = nil;
    self.email2 = nil;
    self.email3 = nil;
    self.email4 = nil;
    self.email5 = nil;
    self.selectedEmail = nil;
    
    self.country = nil;
    self.city = nil;
    self.street = nil;
    self.postCode = nil;
    self.title = nil;
    self.recId = nil;
    self.photoPreviewUrl = nil;
}

- (NSString*)fullContactName
{
    NSMutableString *name = [NSMutableString string];
    
    if ([self.firstname length])
    {
        [name appendString:self.firstname];
    }
    
    if ([self.lastname length])
    {
        if ([name length])
        {
            [name appendString:@" "];
        }
        
        [name appendString:self.lastname];
    }
    
    if (![[name stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]] length])
    {
        return @"9999";
    }
    
    return name;
}

- (NSString*)titleForPhoneSectionForPhone:(NSString*)candidate
{
    NSString *sectionTitle = nil;
    
    if ([self.phoneMobile length])
    {
        sectionTitle = self.phoneMobile;
    }
    else if ([self.phoneiPhone length])
    {
        sectionTitle = self.phoneiPhone;
    }
    else if ([self.phoneHome length])
    {
        sectionTitle = self.phoneHome;
    }
    else if ([self.phoneWork length])
    {
        sectionTitle = self.phoneWork;
    }
    else if ([self.phoneMain length])
    {
        sectionTitle = self.phoneMain;
    }
    else if ([self.phoneHomeFax length])
    {
        sectionTitle = self.phoneHomeFax;
    }
    else if ([self.phoneWorkFax length])
    {
        sectionTitle = self.phoneWorkFax;
    }
    else if ([self.phoneOtherFax length])
    {
        sectionTitle = self.phoneOtherFax;
    }
    else if ([self.phonePager length])
    {
        sectionTitle = self.phonePager;
    }

    if (sectionTitle == candidate)
    {
        return NSLocalizedString(@"Phone:", @"Phone:");
    }
    else
    {
        return @"";
    }
}

- (NSString*)titleForEmailSectionForEmail:(NSString*)candidate
{
    NSString *sectionTitle = nil;
    
    if ([self.email length])
    {
        sectionTitle = self.email;
    }
    else if ([self.email1 length])
    {
        sectionTitle = self.email1;
    }
    else if ([self.email2 length])
    {
        sectionTitle = self.email2;
    }
    else if ([self.email3 length])
    {
        sectionTitle = self.email3;
    }
    else if ([self.email4 length])
    {
        sectionTitle = self.email4;
    }
    else if ([self.email5 length])
    {
        sectionTitle = self.email5;
    }
    
    if (sectionTitle == candidate)
    {
        return NSLocalizedString(@"Email:", @"Email:");
    }
    else
    {
        return @"";
    }
}

#pragma mark - NSCopying protocol

- (id)copyWithZone:(NSZone *)zone
{
    GRContact *theContact = [[GRContact alloc] init];
    
    theContact.firstname = self.firstname;
    theContact.fullname = self.fullname;
    theContact.lastname = self.lastname;
    theContact.photourl = self.photourl;
    
    theContact.phoneMobile = self.phoneMobile;
    theContact.phoneiPhone = self.phoneiPhone;
    theContact.phoneHome = self.phoneHome;
    theContact.phoneWork = self.phoneWork;
    theContact.phoneMain = self.phoneMain;
    theContact.phoneHomeFax = self.phoneHomeFax;
    theContact.phoneWorkFax = self.phoneWorkFax;
    theContact.phoneOtherFax = self.phoneOtherFax;
    theContact.phonePager = self.phonePager;
    
    theContact.phoneSkype = self.phoneSkype;
    
    theContact.email = self.email;
    theContact.email1 = self.email1;
    theContact.email2 = self.email2;
    theContact.email3 = self.email3;
    theContact.email4 = self.email4;
    theContact.email5 = self.email5;
    theContact.selectedEmail = self.selectedEmail;
    
    theContact.recId = self.recId;
    theContact.favorite = self.favorite;
    theContact.postCode = self.postCode;
    theContact.street = self.street;
    theContact.city = self.city;
    theContact.country = self.country;
    theContact.title = self.title;
    theContact.photoPreviewUrl = self.photoPreviewUrl;
    
    return theContact;
}

@end
