//
//  Alarm.h
//  GrannyApp
//
//  Created by Eugene Pavluk on 4/9/14.
//
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>
#import "EventBase.h"

#define kAlarmEntityName     @"Alarm"

@interface Alarm : EventBase

@property (nonatomic, retain) NSNumber * month;
@property (nonatomic, retain) NSNumber * year;
@property (nonatomic, retain) NSNumber * day;

@end
