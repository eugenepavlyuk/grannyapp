//
//  TMailSentMessage.h
//  GrannyApp
//
//  Created by Ievgen Pavliuk on 7/25/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>
#import "TMailMessageBase.h"


@interface TMailSentMessage : TMailMessageBase

@end
