//
//  TMailMessage.h
//  GrannyApp
//
//  Created by Alexander Ermolaev on 3/7/13.
//
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>
#import "TMailMessageBase.h"

@interface TMailMessage : TMailMessageBase

@end
