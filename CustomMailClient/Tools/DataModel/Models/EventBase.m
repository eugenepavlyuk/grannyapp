//
//  EventBase.m
//  GrannyApp
//
//  Created by Eugene Pavluk on 4/9/14.
//
//

#import "EventBase.h"


@implementation EventBase

@dynamic date;
@dynamic eventId;
@dynamic expired;
@dynamic hasNotification;
@dynamic hasSnoozeTime;
@dynamic name;
@dynamic persistent;
@dynamic type;
@dynamic minute;
@dynamic hour;

@synthesize snoozeDate;

- (void)dealloc
{
    self.snoozeDate = nil;
    [super dealloc];
}

#define DATE_CMP(X, Y) ([X year] == [Y year] && [X month] == [Y month] && [X day] == [Y day])

- (unsigned int)minutesSinceMidnight
{
	unsigned int fromMidnight = 0;
	
	NSDateComponents *startComponents = [[AppDelegate getInstance].globalCalendar components:DATE_COMPONENTS fromDate:self.date];
	
    fromMidnight = [startComponents hour] * MINUTES_IN_HOUR + [startComponents minute];
	
	/* The minimum duration for an event is 30 minutes because of the grid size.
	 * If the event starts, say, 23:59, adjust the start time to 23:30.
	 */
	int d = DAY_IN_MINUTES - MIN_EVENT_DURATION_IN_MINUTES;
    
	if (fromMidnight > d)
    {
		fromMidnight = d;
	}
    
	return fromMidnight;
}

- (unsigned int)durationInMinutes
{
	unsigned int duration = 60;
	
    if (duration < MIN_EVENT_DURATION_IN_MINUTES)
    {
        duration = MIN_EVENT_DURATION_IN_MINUTES;
    }
    
	return duration;
}

- (void)rescheduleEvent
{
    self.snoozeDate = nil;
    
    [self scheduleLocalNotifications];
}

- (void)scheduleLocalNotifications
{
    [[UIApplication sharedApplication] cancelLocalNotificationsForEventId:self.eventId];
    
    if ([self.expired boolValue])
    {
        return ;
    }
}

- (NSString*)sound
{
    return nil;
}

- (BOOL)isEvent
{
    return NO;
}

- (BOOL)isMatch
{
    return NO;
}

- (BOOL)hasImage
{
    return NO;
}

- (NSDate*)notifyDate
{
    return nil;
}

+ (UILocalNotification*)localNotification
{
    UILocalNotification *localNotification = [[[UILocalNotification alloc] init] autorelease];
    
    localNotification.hasAction = YES;
    localNotification.alertAction = @"OK";
    
    return localNotification;
}

- (UILocalNotification*)localNotification
{
    UILocalNotification *localNotification = [EventBase localNotification];
    
    localNotification.fireDate = [self notifyDate];
    localNotification.alertBody = self.name;
    localNotification.soundName = [[self sound] stringByAppendingPathExtension:@"mp3"];
    localNotification.userInfo = @{kEventIdKey : self.eventId};
    
    return localNotification;
}

@end
