//
//  TRadioStation.m
//  GrannyApp
//
//  Created by Ievgen Pavliuk on 8/1/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "TRadioStation.h"


@implementation TRadioStation

@dynamic title;
@dynamic strimUrl;
@dynamic stationID;
@dynamic logoPath;
@end
