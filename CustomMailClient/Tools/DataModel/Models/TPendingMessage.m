//
//  TPendingMessage.m
//  GrannyApp
//
//  Created by Ievgen Pavliuk on 7/15/13.
//
//

#import "TPendingMessage.h"


@implementation TPendingMessage

@dynamic uid;
@dynamic senderMail;
@dynamic tos;
@dynamic subject;
@dynamic textPlain;
@dynamic textHTML;
@dynamic accountName;
@dynamic attachments;

@end
