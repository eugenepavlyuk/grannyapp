//
//  TMessageAttachment.m
//  GrannyApp
//
//  Created by Ievgen Pavliuk on 7/19/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "TMessageAttachment.h"


@implementation TMessageAttachment

@dynamic fileName;
@dynamic messageID;
@dynamic contentType;
@dynamic uniqueId;
@dynamic filePath;
@dynamic accountName;

@end
