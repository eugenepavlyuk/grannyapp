//
//  InternetPages.h
//  GrannyApp
//
//  Created by Eugene Pavlyuk on 7/24/12.
//  Copyright (c) 2012 Home. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>


@interface InternetPages : NSManagedObject

@property (nonatomic, retain) id favorites;
@property (nonatomic, retain) id readinglist;
@property (nonatomic, retain) id history;

@end
