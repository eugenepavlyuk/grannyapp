//
//  InternetPages.m
//  GrannyApp
//
//  Created by Eugene Pavlyuk on 7/24/12.
//  Copyright (c) 2012 Home. All rights reserved.
//

#import "InternetPages.h"


@implementation InternetPages

@dynamic favorites;
@dynamic readinglist;
@dynamic history;

@end
