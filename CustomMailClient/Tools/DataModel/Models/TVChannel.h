//
//  TVChannel.h
//  GrannyApp
//
//  Created by Ievgen Pavliuk on 9/3/12.
//
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>


@interface TVChannel : NSManagedObject

@property (nonatomic, retain) NSString * channelID;
@property (nonatomic, retain) NSString * channelTitle;
@property (nonatomic, retain) NSString * channelUrl;

@property (nonatomic, retain) NSString * language;
@property (nonatomic, retain) NSString * genre;
@property (nonatomic, retain) NSString * country;

@end
