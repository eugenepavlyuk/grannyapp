//
//  TMailAccount.m
//  GrannyApp
//
//  Created by Ievgen Pavliuk on 4/17/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "DBMailAccount.h"


@implementation DBMailAccount

@synthesize accountName;
@synthesize type;
@synthesize server;
@synthesize port;
@synthesize userName;
@synthesize password;
@synthesize isActive;
@synthesize isSSL;
@synthesize smtpPort;
@synthesize smtpServer;

- (id)initWithTMailAccount:(TMailAccount *)account
{
    if ((self=[super init]))
    {
        accountName = account.accountName;
        type = account.type;
        server = account.server;
        port = account.port;
        userName = account.userName;
        password = account.password;
        isActive = account.isActive;
        isSSL = account.isSSL;
        smtpPort = account.smtpPort;
        smtpServer = account.smtpServer;
    }
    return self;
}
- (id)initWithCoder:(NSCoder *)aDecoder
{
    if ((self=[super init]))
    {
        accountName = [[aDecoder decodeObjectForKey:@"accountName"] retain];
        type = [[aDecoder decodeObjectForKey:@"type"] retain];
        server = [[aDecoder decodeObjectForKey:@"server"] retain];
        port = [[aDecoder decodeObjectForKey:@"port"] retain];
        userName = [[aDecoder decodeObjectForKey:@"userName"] retain];
        password = [[aDecoder decodeObjectForKey:@"password"] retain];
        isActive = [[aDecoder decodeObjectForKey:@"isActive"] retain];
        isSSL = [[aDecoder decodeObjectForKey:@"isSSL"] retain];
        smtpPort = [[aDecoder decodeObjectForKey:@"smtpPort"] retain];
        smtpServer = [[aDecoder decodeObjectForKey:@"smtpServer"] retain];
    }
    return self;
}

- (void)encodeWithCoder:(NSCoder *)encoder
{
    if (self.accountName) [encoder encodeObject:self.accountName forKey:@"accountName"];
    if (self.type) [encoder encodeObject:self.type forKey:@"type"];
    if (self.server) [encoder encodeObject:self.server forKey:@"server"];
    if (self.port) [encoder encodeObject:self.port forKey:@"port"];
    if (self.userName) [encoder encodeObject:self.userName forKey:@"userName"];
    if (self.password) [encoder encodeObject:self.password forKey:@"password"];
    if (self.isActive) [encoder encodeObject:self.isActive forKey:@"isActive"];
    if (self.isSSL) [encoder encodeObject:self.isSSL forKey:@"isSSL"];
    if (self.smtpPort) [encoder encodeObject:self.smtpPort forKey:@"smtpPort"];
    if (self.smtpServer) [encoder encodeObject:self.smtpServer forKey:@"smtpServer"];
}
@end
