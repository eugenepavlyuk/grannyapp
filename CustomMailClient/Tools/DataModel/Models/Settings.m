//
//  Settings.m
//  GrannyApp
//
//  Created by Alexander Ermolaev on 5/14/13.
//
//

#import "Settings.h"


@implementation Settings

@dynamic clockOn;
@dynamic contact_name;
@dynamic contact_phone;
@dynamic contact_picture;
@dynamic doctor_name;
@dynamic doctor_phone;
@dynamic doctor_picture;
@dynamic duration;
@dynamic home_name;
@dynamic home_phone;
@dynamic home_picture;
@dynamic hospital_name;
@dynamic hospital_phone;
@dynamic hospital_picture;
@dynamic imageOn;
@dynamic linkToImage;
@dynamic photoSliderOn;
@dynamic radioOn;
@dynamic screenSaverState;
@dynamic slidingTimeMinutes;
@dynamic slidingTimeSeconds;
@dynamic snoozeDuration;
@dynamic timeFormat;
@dynamic weedStartsFrom;
@dynamic personalMessage;

@end
