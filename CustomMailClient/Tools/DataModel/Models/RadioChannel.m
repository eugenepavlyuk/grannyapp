//
//  RadioChannel.m
//  GrannyApp
//
//  Created by Eugene Pavluk on 7/25/13.
//
//

#import "RadioChannel.h"


@implementation RadioChannel

@dynamic channelID;
@dynamic channelTitle;
@dynamic channelUrl;
@dynamic country;
@dynamic genre;
@dynamic language;

@end
