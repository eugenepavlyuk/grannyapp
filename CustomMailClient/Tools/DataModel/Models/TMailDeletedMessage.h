//
//  TMailDeletedMessage.h
//  GrannyApp
//
//  Created by Ievgen Pavliuk on 9/5/12.
//
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>
#import "TMailMessageBase.h"


@interface TMailDeletedMessage : TMailMessageBase

@property (nonatomic, retain) NSNumber * deleted;

@end
