//
//  TMailMessageBase.m
//  GrannyApp
//
//  Created by admin on 09/12/13.
//
//

#import "TMailMessageBase.h"

@implementation TMailMessageBase

@dynamic accountName;
@dynamic date;
@dynamic isFetched;
@dynamic isRead;
@dynamic mailType;
@dynamic messageBodyHTML;
@dynamic messageBodyPlain;
@dynamic messageID;
@dynamic receivers;
@dynamic senderEmail;
@dynamic senderName;
@dynamic senderPhotoUrl;
@dynamic subject;
@dynamic tos;
@dynamic uid;
@dynamic isInlineAttachments;

@end
