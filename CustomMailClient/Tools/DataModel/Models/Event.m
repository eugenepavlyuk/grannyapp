//
//  Event.m
//  GrannyApp
//
//  Created by Eugene Pavlyuk on 8/9/12.
//  Copyright (c) 2012 Home. All rights reserved.
//

#import "Event.h"
#import "DataManager.h"
#import "Settings.h"
#import "NSDate+Calendar.h"

@implementation Event

@dynamic endDate;
@dynamic month;
@dynamic year;
@dynamic day;
@dynamic monthEnd;
@dynamic yearEnd;
@dynamic dayEnd;
@dynamic hourEnd;
@dynamic minuteEnd;

- (void)dealloc
{
    [super dealloc];
}

- (unsigned int)durationInMinutes
{
	unsigned int duration = 0;
	
	duration = (int) (([self.endDate timeIntervalSince1970] - [self.date timeIntervalSince1970]) / (double) MINUTES_IN_HOUR);
    
    if (duration < MIN_EVENT_DURATION_IN_MINUTES)
    {
        duration = MIN_EVENT_DURATION_IN_MINUTES;
    }
    
	return duration;
}

- (NSDate*)notifyDate
{
    if ([self.type intValue] == ET_Annually)
    {
        NSDate *date = [NSDate dateWithYear:[[NSDate date] year]
                                     month:[self.month integerValue]
                                       day:[self.day integerValue]
                                      hour:[self.hour integerValue]
                                    minute:[self.minute integerValue]
                                    second:0];
        
        return date;
    }
    else if ([self.type intValue] == ET_Once) // once event
    {
        NSDate *date = [NSDate dateWithYear:[self.year integerValue]
                                      month:[self.month integerValue]
                                        day:[self.day integerValue]
                                       hour:[self.hour integerValue]
                                     minute:[self.minute integerValue]
                                     second:0];
        
        return date;
    }
    else if ([self.type intValue] == ET_Daily) // daily event
    {
        NSDate *date = [NSDate dateWithYear:[[NSDate date] year]
                                     month:[[NSDate date] month]
                                       day:[[NSDate date] day]
                                      hour:[self.hour integerValue]
                                    minute:[self.minute integerValue]
                                    second:0];
        
        return date;
    }
    else if ([self.type intValue] == ET_Weekly) // weekdays event
    {
        NSInteger weekday = [self.date weekday];
        
        NSDate *date = [[[NSDate date] dateBySettingWeekday:weekday] dateBySettingHour:[self.hour integerValue]
                                                                 minute:[self.minute integerValue]
                                                                 second:0];
        
        return date;
    }
    else if ([self.type intValue] == ET_Monthly) // monthly event
    {
        NSDate *date = [NSDate dateWithYear:[[NSDate date] year]
                                      month:[[NSDate date] month]
                                        day:[self.day integerValue]
                                       hour:[self.hour integerValue]
                                     minute:[self.minute integerValue]
                                     second:0];
        
        return date;
    }

    return nil;
}

- (BOOL)isMatch
{
    NSInteger secondTime  = [[NSDate date] second];
    
    if (secondTime != 0)
    {
        return NO;
    }
    
    NSDate *now = [NSDate dateWithYear:[[NSDate date] year]
                                 month:[[NSDate date] month]
                                   day:[[NSDate date] day]
                                  hour:[[NSDate date] hour]
                                minute:[[NSDate date] minute]
                                second:0];
    
    NSDate *testDate = nil;
    
    if ([self.hasNotification boolValue])
    {
        Settings *settings = [[DataManager sharedInstance] getEntityWithName:kSettingsEntityName];

        NSTimeInterval timeInterval = [settings.snoozeDuration integerValue] * 60;

        NSTimeInterval newTimeInterval = [[self notifyDate] timeIntervalSince1970] - timeInterval;

        self.snoozeDate = nil;
        
        NSDate *notificationDate = [NSDate dateWithTimeIntervalSince1970:newTimeInterval];
        
        if ([notificationDate isLessDate:now]) // passed
        {
            if ([[self notifyDate] isLessDate:now]) // passed
            {
                return NO;
            }
            else
            {
                testDate = [self notifyDate];
            }
        }
        else
        {
            testDate = notificationDate;
        }
    }
    else
    {
        if ([[self notifyDate] isLessDate:now]) // passed
        {
            return NO;
        }
        else
        {
            testDate = [self notifyDate];
        }
    }
    
    NSInteger hour    = [testDate hour];
    NSInteger minute  = [testDate minute];
    NSInteger weekday = [testDate weekday];
    NSInteger day     = [testDate day];
    NSInteger month   = [testDate month];
    NSInteger year    = [testDate year];
    
    NSInteger hourTime    = [[NSDate date] hour];
    NSInteger minuteTime  = [[NSDate date] minute];
    NSInteger weekDayTime = [[NSDate date] weekday];
    NSInteger dayTime     = [[NSDate date] day];
    NSInteger monthTime   = [[NSDate date] month];
    NSInteger yearTime    = [[NSDate date] year];
    
    if ([self.type intValue] == ET_Annually)
    {
        if ((month == monthTime) && (day == dayTime) && (hour == hourTime) && (minute == minuteTime))
        {
            return YES;
        }
    }
    else if ([self.type intValue] == ET_Once)
    {
        if ((year == yearTime) && (month == monthTime) && (day == dayTime) && (hour == hourTime) && (minute == minuteTime))
        {
            return YES;
        }
    }
    else if ([self.type intValue] == ET_Daily)
    {
        if ((hour == hourTime) && (minute == minuteTime))
        {
            return YES;
        }
    }
    else if ([self.type intValue] == ET_Weekly)
    {
        if ((hour == hourTime) && (minute == minuteTime) && (weekday == weekDayTime))
        {
            return YES;
        }
    }
    else if ([self.type intValue] == ET_Monthly)
    {
        if ((hour == hourTime) && (minute == minuteTime) && (day == dayTime))
        {
            return YES;
        }
    }
    
    return NO;
}

- (void)scheduleLocalNotifications
{
    [super scheduleLocalNotifications];
    
    UILocalNotification *localNotification = [self localNotification];
    
    BOOL osVersionLessThan7 = ([[UIDevice currentDevice].systemVersion floatValue] < 7.0);
    
    switch ([self.type integerValue])
    {
        case ET_Daily:
        {
            localNotification.repeatInterval = (osVersionLessThan7) ? NSDayCalendarUnit : NSCalendarUnitDay;
        }; break;
            
        case ET_Weekly:
        {
            localNotification.repeatInterval = (osVersionLessThan7) ? NSWeekCalendarUnit : NSWeekCalendarUnit;
        }; break;
            
        case ET_Monthly:
        {
            localNotification.repeatInterval = (osVersionLessThan7) ? NSMonthCalendarUnit : NSCalendarUnitMonth;
        }; break;
            
        case ET_Annually:
        {
            localNotification.repeatInterval = (osVersionLessThan7) ? NSYearCalendarUnit : NSCalendarUnitYear;
        }; break;
            
        default:
            break;
    }
    
    [[UIApplication sharedApplication] scheduleLocalNotification:localNotification];
    
    if (![self.hasNotification boolValue])
    {
        return ;
    }
    
    localNotification = [self localNotification];
    
    Settings *settings = [[DataManager sharedInstance] getEntityWithName:kSettingsEntityName];
    
    NSTimeInterval timeInterval = [settings.snoozeDuration integerValue] * 60;
    
    NSTimeInterval newTimeInterval = [[self notifyDate] timeIntervalSince1970] - timeInterval;
    
    NSDate *notificationDate = [NSDate dateWithTimeIntervalSince1970:newTimeInterval];
    
    localNotification.fireDate = notificationDate;
        
    localNotification.userInfo = @{kEventIdKey : self.eventId, @"isNotification" : @(YES)};
    
    [[UIApplication sharedApplication] scheduleLocalNotification:localNotification];
}

- (BOOL)hasImage
{
    NSString *iconName = [[[NSString stringWithFormat:@"%f", [self.eventId floatValue]] stringByReplacingOccurrencesOfString:@"." withString:@"_"]stringByAppendingPathExtension:@"png"];
    
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES);
    NSString *dirPath = [paths objectAtIndex:0];
    
    dirPath = [dirPath stringByAppendingPathComponent:iconName];
    
    return [[NSFileManager defaultManager] fileExistsAtPath:dirPath isDirectory:NULL];
}

- (NSString*)sound
{
    return  @"harp";
}

- (BOOL)isEvent
{
    return YES;
}

@end
