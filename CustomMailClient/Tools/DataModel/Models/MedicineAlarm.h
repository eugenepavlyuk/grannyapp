//
//  MedicineAlarm.h
//  GrannyApp
//
//  Created by Eugene Pavluk on 9/24/13.
//
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>
#import "EventBase.h"

#define kMedicineTypeKey       @"medicineType"

#define kMedicineAlarmEntityName     @"MedicineAlarm"

@interface MedicineAlarm : EventBase

@property (nonatomic, retain) NSNumber *medicineType;
@property (nonatomic, retain) id weekDays;
@property (nonatomic, retain) NSString *eventIdentifier;

- (void)removeEvent;

@end
