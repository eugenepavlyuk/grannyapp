//
//  RecientContact.h
//  GrannyApp
//
//  Created by Eugene Pavlyuk on 5/23/12.
//  Copyright (c) 2012 Home. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>


@interface RecientContact : NSManagedObject

@property (nonatomic, retain) NSString * firstname;
@property (nonatomic, retain) NSString * lastname;
@property (nonatomic, retain) NSString * photourl;
@property (nonatomic, retain) NSString * phone;
@property (nonatomic, retain) NSString * email;

@end
