//
//  Favorite.h
//  GrannyApp
//
//  Created by Eugene Pavlyuk on 6/13/12.
//  Copyright (c) 2012 Home. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

#define kFavoriteEntity         @"Favorite"

@interface Favorite : NSManagedObject

@property (nonatomic, retain) id contacts;
@property (nonatomic, retain) id emails;

@end
