//
//  Alarm.m
//  GrannyApp
//
//  Created by Eugene Pavluk on 4/9/14.
//
//

#import "Alarm.h"
#import "DataManager.h"
#import "Settings.h"
#import "NSDate+Calendar.h"

@implementation Alarm

@dynamic month;
@dynamic year;
@dynamic day;

- (NSDate*)notifyDate
{
    if ([self.type intValue] == ET_Annually)
    {
        NSDate *date = [NSDate dateWithYear:[[NSDate date] year]
                                      month:[self.month integerValue]
                                        day:[self.day integerValue]
                                       hour:[self.hour integerValue]
                                     minute:[self.minute integerValue]
                                     second:0];
        
        return date;
    }
    else if ([self.type intValue] == ET_Once)
    {
        NSDate *date = [NSDate dateWithYear:[self.year integerValue]
                                      month:[self.month integerValue]
                                        day:[self.day integerValue]
                                       hour:[self.hour integerValue]
                                     minute:[self.minute integerValue]
                                     second:0];
        
        return date;
    }
    else if ([self.type intValue] == ET_Daily)
    {
        NSDate *date = [NSDate dateWithYear:[[NSDate date] year]
                                      month:[[NSDate date] month]
                                        day:[[NSDate date] day]
                                       hour:[self.hour integerValue]
                                     minute:[self.minute integerValue]
                                     second:0];
        
        return date;
    }
    else if ([self.type intValue] == ET_Weekly)
    {
        NSInteger weekday = [self.date weekday];
        
        NSDate *date = [[[NSDate date] dateBySettingWeekday:weekday] dateBySettingHour:[self.hour integerValue]
                                                                                minute:[self.minute integerValue]
                                                                                second:0];
        
        return date;
    }
    else if ([self.type intValue] == ET_Monthly) // monthly event
    {
        NSDate *date = [NSDate dateWithYear:[[NSDate date] year]
                                      month:[[NSDate date] month]
                                        day:[self.day integerValue]
                                       hour:[self.hour integerValue]
                                     minute:[self.minute integerValue]
                                     second:0];
        
        return date;
    }
    
    return nil;
}

- (BOOL)isMatch
{
    NSInteger secondTime  = [[NSDate date] second];
    
    if (secondTime != 0)
    {
        return NO;
    }
    
    NSDate *now = [NSDate dateWithYear:[[NSDate date] year]
                                 month:[[NSDate date] month]
                                   day:[[NSDate date] day]
                                  hour:[[NSDate date] hour]
                                minute:[[NSDate date] minute]
                                second:0];
    
    NSDate *testDate = nil;
    
    if ([self.hasSnoozeTime boolValue])
    {
        if ([[self notifyDate] isLessDate:now]) // passed
        {
            if ([self.snoozeDate isLessDate:now]) // passed
            {
                return NO;
            }
            else if (!self.snoozeDate)
            {
                return NO;
            }
            else
            {
                testDate = self.snoozeDate;
            }
        }
        else
        {
            testDate = [self notifyDate];
        }
    }
    else
    {
        if ([[self notifyDate] isLessDate:now]) // passed
        {
            return NO;
        }
        else
        {
            testDate = [self notifyDate];
        }
    }
    
    NSInteger hour    = [testDate hour];
    NSInteger minute  = [testDate minute];
    NSInteger weekday = [testDate weekday];
    NSInteger day     = [testDate day];
    NSInteger month   = [testDate month];
    NSInteger year    = [testDate year];
    
    NSInteger hourTime    = [[NSDate date] hour];
    NSInteger minuteTime  = [[NSDate date] minute];
    NSInteger weekDayTime = [[NSDate date] weekday];
    NSInteger dayTime     = [[NSDate date] day];
    NSInteger monthTime   = [[NSDate date] month];
    NSInteger yearTime    = [[NSDate date] year];
    
    if ([self.type intValue] == ET_Annually)
    {
        if ((month == monthTime) && (day == dayTime) && (hour == hourTime) && (minute == minuteTime))
        {
            return YES;
        }
    }
    else if ([self.type intValue] == ET_Once)
    {
        if ((year == yearTime) && (month == monthTime) && (day == dayTime) && (hour == hourTime) && (minute == minuteTime))
        {
            return YES;
        }
    }
    else if ([self.type intValue] == ET_Daily)
    {
        if ((hour == hourTime) && (minute == minuteTime))
        {
            return YES;
        }
    }
    else if ([self.type intValue] == ET_Weekly)
    {
        if ((hour == hourTime) && (minute == minuteTime) && (weekday == weekDayTime))
        {
            return YES;
        }
    }
    else if ([self.type intValue] == ET_Monthly)
    {
        if ((hour == hourTime) && (minute == minuteTime) && (day == dayTime))
        {
            return YES;
        }
    }
    
    return NO;
}

- (void)scheduleLocalNotifications
{
    [super scheduleLocalNotifications];
    
    UILocalNotification *localNotification = [self localNotification];
    
    BOOL osVersionLessThan7 = ([[UIDevice currentDevice].systemVersion floatValue] < 7.0);
    
    switch ([self.type integerValue])
    {
        case ET_Daily:
        {
            localNotification.repeatInterval = (osVersionLessThan7) ? NSDayCalendarUnit : NSCalendarUnitDay;
        }; break;
            
        case ET_Weekly:
        {
            localNotification.repeatInterval = (osVersionLessThan7) ? NSWeekCalendarUnit : NSWeekCalendarUnit;
        }; break;
            
        case ET_Monthly:
        {
            localNotification.repeatInterval = (osVersionLessThan7) ? NSMonthCalendarUnit : NSCalendarUnitMonth;
        }; break;
            
        case ET_Annually:
        {
            localNotification.repeatInterval = (osVersionLessThan7) ? NSYearCalendarUnit : NSCalendarUnitYear;
        }; break;
            
        default:
            break;
    }
    
    [[UIApplication sharedApplication] scheduleLocalNotification:localNotification];
    
    if (![self.hasSnoozeTime boolValue] || !self.snoozeDate)
    {
        return ;
    }
    
    localNotification = [self localNotification];
    
    localNotification.fireDate = self.snoozeDate;
    
    localNotification.userInfo = @{kEventIdKey : self.eventId, @"isNotification" : @(YES)};
    
    [[UIApplication sharedApplication] scheduleLocalNotification:localNotification];
}

- (NSString*)sound
{
    return @"xylophone";
}

@end
