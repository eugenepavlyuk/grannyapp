//
//  GRRadioDataManager.m
//  GrannyApp
//
//  Created by Ievgen Pavliuk on 8/1/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "GRRadioDataManager.h"
#import "DataManager.h"
#import "TRadioStation.h"

@implementation GRRadioDataManager

static GRRadioDataManager *instance = nil;

+ (GRRadioDataManager *)shared {
    if (!instance) {
        instance = [[GRRadioDataManager alloc] init];
    }
    return instance;
}


- (void)saveRadioStation:(NSDictionary *)station {
    TRadioStation *messageModel = (TRadioStation *)[[DataManager sharedInstance] createEntityWithName:@"TRadioStation"];
    id name = [station objectForKey:@"name"];
    if (name == [NSNull null]) {
        return;
    }
    NSDictionary *streams = [station objectForKey:@"streams"];
    id url = [streams objectForKey:@"stream_url"];
    if (url == [NSNull null]) {
        return;
    }
    NSLog(@"%@", [station objectForKey:@"sid"]);
    messageModel.stationID = [station objectForKey:@"sid"];
    messageModel.title = (NSString *)name;
    messageModel.strimUrl = (NSString *)url;
    [[DataManager sharedInstance] save];
}

- (void)createRadioOnStart:(NSDictionary*)station
{
    TRadioStation *messageModel = (TRadioStation *)[[DataManager sharedInstance] createEntityWithName:@"TRadioStation"];
    messageModel.title = [station objectForKey:@"title"];
    messageModel.strimUrl = [station objectForKey:@"url"];
    messageModel.stationID = [NSString stringWithFormat:@"%i", [[self getAllSavedStations] count]+1189834];
}

- (void)updateRadioStation:(NSDictionary *)station {
    NSArray *allRows = [[DataManager sharedInstance] allForEntity:@"TRadioStation" forFieldName:@"stationID" withFieldValue:[station objectForKey:@"sid"]];
//    NSString *sid = [station objectForKey:@"sid"];
    for (TRadioStation *stModel in allRows) {
        stModel.title = [station objectForKey:@"title"];
        stModel.logoPath = [station objectForKey:@"logoPath"];
        stModel.strimUrl = [station objectForKey:@"url"];
        [[DataManager sharedInstance] save];
    }
    if ([allRows count] == 0) {
        TRadioStation *stModel = (TRadioStation *)[[DataManager sharedInstance] createEntityWithName:@"TRadioStation"];
        stModel.title = [station objectForKey:@"title"];
        stModel.logoPath = [station objectForKey:@"logoPath"];
        stModel.strimUrl = [station objectForKey:@"url"];
        stModel.stationID = [NSString stringWithFormat:@"%i", [[self getAllSavedStations] count]+1189834];
        [[DataManager sharedInstance] save];
    }
}


- (void)deleteRaioStationByID:(NSString *)sid {
    NSArray *allRows = [[DataManager sharedInstance] allForEntity:@"TRadioStation" forFieldName:@"stationID" withFieldValue:sid];
    for (TRadioStation *stModel in allRows) {
        [[DataManager sharedInstance] removeEntityModel:stModel];
    }
}

- (NSArray *)getAllSavedStations {
    return [[DataManager sharedInstance] allRowsForEntity:@"TRadioStation"];
}

- (BOOL)isStationAxists:(NSString *)sid {
    NSArray *allRows = [[DataManager sharedInstance] allForEntity:@"TRadioStation" forFieldName:@"stationID" withFieldValue:sid];
    return ([allRows count] != 0);
}

@end
