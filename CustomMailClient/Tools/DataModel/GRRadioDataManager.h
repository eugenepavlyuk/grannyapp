//
//  GRRadioDataManager.h
//  GrannyApp
//
//  Created by Ievgen Pavliuk on 8/1/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface GRRadioDataManager : NSObject {
    
}

+ (GRRadioDataManager *)shared;
- (void)saveRadioStation:(NSDictionary *)station;
- (void)deleteRaioStationByID:(NSString *)sid;
- (void)updateRadioStation:(NSDictionary *)station;
- (BOOL)isStationAxists:(NSString *)sid;
- (NSArray *)getAllSavedStations;

- (void)createRadioOnStart:(NSDictionary*)station;

@end
