//
//  DataManager.h
//  Granny
//
//  Created by Eugene Pavlyuk on 26.11.11.
//  Copyright (c) 2011 Home. All rights reserved.
//


#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@interface DataManager : NSObject 
{
    NSMutableDictionary *contextQueue;

}

@property (nonatomic, readonly, retain) NSManagedObjectModel *objectModel;
@property (nonatomic, readonly, retain) NSPersistentStoreCoordinator *persistentStoreCoordinator;

+ (DataManager*)sharedInstance;

- (BOOL)save;

- (id)getEntityWithName:(NSString*)entityName;
- (id)createEntityWithName:(NSString*)entityName;

- (void)removeEntityModel:(NSManagedObject *) entityModel;
- (NSArray *)allRowsForEntity:(NSString *)entityName;
- (NSArray *)allForEntity:(NSString *)entityName forFieldName:(NSString *)fieldName withFieldValue:(NSString *)fieldValue;
- (NSArray *)allForEntity:(NSString *)entityName forFieldName1:(NSString *)fieldName1 withFieldValue1:(NSString *)fieldValue1 forFieldName2:(NSString *)fieldName2 withFieldValue2:(NSString *)fieldValue2;
- (NSArray *)allNotDeletedForEntity:(NSString *)entityName forFieldName:(NSString *)fieldName withFieldValue:(NSString *)fieldValue;
- (NSArray *)allForEntityByID:(NSInteger)uid forFieldName:(NSString *)fieldName entityName:(NSString *)entityName accountName:(NSString *)accountName;
- (NSArray *)allHeadersForID:(NSInteger)uid folderID:(NSInteger)folderID accountType:(NSString *)accountName;
- (NSArray *)allHeadersForFolderID:(NSInteger)folderID accountType:(NSString *)accountName;
- (NSArray *)allMessagesForUID:(NSInteger)uid accountName:(NSString *)accountName entityName:(NSString *)entityName;
- (BOOL)isTVChannelExists:(NSString *)sid;
- (void)deleteChannelByID:(NSString *)sid;
- (void)updateTVChannel:(NSDictionary *)channel;
- (void)addChannel:(NSDictionary*)channel;
- (void)addRadioChannel:(NSDictionary*)channel;
- (void)killContext;
@end
