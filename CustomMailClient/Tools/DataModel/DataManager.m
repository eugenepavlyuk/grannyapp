//
//  DataManager.m
//  iHearts
//
//  Created by Eugene Pavlyuk on 26.11.11.
//  Copyright (c) 2011 Home. All rights reserved.
//

#import "DataManager.h"
#import "TVChannel.h"
#import "RadioChannel.h"
#include <pthread.h>


static DataManager *sharedInstance = nil;

@implementation DataManager

@synthesize persistentStoreCoordinator = _persistentStoreCoordinator;
@synthesize objectModel = _objectModel;

+ (DataManager*)sharedInstance
{
    if(sharedInstance)
    {
        return sharedInstance;
    }
    
    @synchronized(self)
    {
        if (!sharedInstance)
        {
            sharedInstance = [[self alloc] init];
        }
    }
    
	return sharedInstance;
}

+ (id)allocWithZone:(NSZone *)zone
{
    if(sharedInstance)
    {
        return sharedInstance;
    }
    
    @synchronized(self)
    {
        if(!sharedInstance)
        {
            sharedInstance = [super allocWithZone:zone];
        }        
    }
    
    return sharedInstance;
}

- (id)init
{
    self = [super init];
    
    if(self)
    {
        contextQueue = [[NSMutableDictionary alloc] init];
    }
    
    return self;
}

- (id)copyWithZone:(NSZone *)zone
{
    return self;
}

- (id)retain
{
    return self;
}

- (NSUInteger)retainCount
{
    return NSUIntegerMax;
}

- (oneway void)release
{
    
}

- (id)autorelease
{
    return self;
}

- (void)dealloc 
{
	[self save];
    [contextQueue release];
	[_persistentStoreCoordinator release];
	[_objectModel release];
    
	[super dealloc];
}

- (NSURL *)applicationDocumentsDirectory
{
    return [[[NSFileManager defaultManager] URLsForDirectory:NSDocumentDirectory inDomains:NSUserDomainMask] lastObject];
}

- (id)getEntityWithName:(NSString*)entityName
{
    NSManagedObjectContext *context = [self getContextByThreadId:[self currentThreadId]];
    NSFetchRequest *request = [[[NSFetchRequest alloc] init] autorelease];
    NSEntityDescription *entity = [NSEntityDescription entityForName:entityName inManagedObjectContext:context];
    [request setEntity:entity];
        
	NSArray *array = [context executeFetchRequest:request error:NULL];
    
    if ([array count])
    {
        return [array objectAtIndex:0];
    }
    else
    {
            NSManagedObject *object = [[DataManager sharedInstance] createEntityWithName:entityName];
                        
            [[DataManager sharedInstance] save];
            
            return object;
    }
    
    return nil;
}



- (id)createEntityWithName:(NSString*)entityName
{
    NSManagedObjectContext *context = [self getContextByThreadId:[self currentThreadId]];
    id newObject = [NSEntityDescription insertNewObjectForEntityForName:entityName inManagedObjectContext:context];
    
    return newObject;
}


- (void)removeEntityModel:(NSManagedObject *) entityModel {
    NSManagedObjectContext *context = [self getContextByThreadId:[self currentThreadId]];

    [context deleteObject:entityModel];
    [self save];
}

- (NSArray *)allForEntity:(NSString *)entityName forFieldName:(NSString *)fieldName withFieldValue:(NSString *)fieldValue
{
    NSManagedObjectContext *context = [self getContextByThreadId:[self currentThreadId]];

    NSFetchRequest *request = [[[NSFetchRequest alloc] init] autorelease];
    NSEntityDescription *entity = [NSEntityDescription entityForName:entityName inManagedObjectContext:context];
    [request setEntity:entity];
    
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"%K like[cd] %@", fieldName, [NSString stringWithFormat:@"*%@*", fieldValue]];
    [request setPredicate:predicate];
    
	// execute
	return [context executeFetchRequest:request error:NULL];
}


- (NSArray *)allNotDeletedForEntity:(NSString *)entityName forFieldName:(NSString *)fieldName withFieldValue:(NSString *)fieldValue
{
    NSManagedObjectContext *context = [self getContextByThreadId:[self currentThreadId]];
    
    NSFetchRequest *request = [[[NSFetchRequest alloc] init] autorelease];
    NSEntityDescription *entity = [NSEntityDescription entityForName:entityName inManagedObjectContext:context];
    [request setEntity:entity];
    
    NSPredicate *predicate1 = [NSPredicate predicateWithFormat:@"%K like[cd] %@", fieldName, [NSString stringWithFormat:@"*%@*", fieldValue]];
    NSPredicate *predicate2 = [NSPredicate predicateWithFormat:@"deleted == %@", [NSNumber numberWithInt:0]];
    NSPredicate *compoundPredicate = [NSCompoundPredicate andPredicateWithSubpredicates:[NSArray arrayWithObjects:predicate1, predicate2, nil]];
    
    
    [request setPredicate:compoundPredicate];
    
	// execute
	return [context executeFetchRequest:request error:NULL];
}

- (NSArray *)allForEntityByID:(NSInteger)uid forFieldName:(NSString *)fieldName entityName:(NSString *)entityName accountName:(NSString *)accountName {
    
    NSManagedObjectContext *context = [self getContextByThreadId:[self currentThreadId]];

    NSFetchRequest *request = [[[NSFetchRequest alloc] init] autorelease];
    NSEntityDescription *entity = [NSEntityDescription entityForName:entityName inManagedObjectContext:context];
    [request setEntity:entity];
    
    if (accountName) {
        NSPredicate *predicate1 = [NSPredicate predicateWithFormat:@"messageID == %i", uid];
        NSPredicate *predicate2 = [NSPredicate predicateWithFormat:@"%K like[cd] %@", @"accountName", [NSString stringWithFormat:@"*%@*", accountName]];
        NSPredicate *compoundPredicate = [NSCompoundPredicate andPredicateWithSubpredicates:[NSArray arrayWithObjects:predicate1, predicate2, nil]];
        [request setPredicate:compoundPredicate];
    } else {
        NSPredicate *predicate = [NSPredicate predicateWithFormat:@"messageID == %i", uid];
        [request setPredicate:predicate];
    }
	// execute
	return [context executeFetchRequest:request error:NULL];
    
}


- (NSArray *)allForEntity:(NSString *)entityName forFieldName1:(NSString *)fieldName1 withFieldValue1:(NSString *)fieldValue1 forFieldName2:(NSString *)fieldName2 withFieldValue2:(NSString *)fieldValue2
{
    NSManagedObjectContext *context = [self getContextByThreadId:[self currentThreadId]];

    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"%K like[cd] %@", fieldName1, [NSString stringWithFormat:@"*%@*", fieldValue1]];
    NSPredicate *predicate2 = [NSPredicate predicateWithFormat:@"%K like[cd] %@", fieldName2, [NSString stringWithFormat:@"*%@*", fieldValue2]];

    
    NSPredicate *compoundPredicate = [NSCompoundPredicate andPredicateWithSubpredicates:[NSArray arrayWithObjects:predicate, predicate2, nil]];

    
    
    NSFetchRequest *request = [[[NSFetchRequest alloc] init] autorelease];
    NSEntityDescription *entity = [NSEntityDescription entityForName:entityName inManagedObjectContext:context];
    [request setEntity:entity];
    
    [request setPredicate:compoundPredicate];
    
	// execute
	return [context executeFetchRequest:request error:NULL];
}

- (NSArray *)allRowsForEntity:(NSString *)entityName
{
    NSManagedObjectContext *context = [self getContextByThreadId:[self currentThreadId]];

    NSFetchRequest *request = [[[NSFetchRequest alloc] init] autorelease];
    
    NSEntityDescription *entity = [NSEntityDescription entityForName:entityName inManagedObjectContext:context];
    [request setEntity:entity];
        
	// execute
	return [context executeFetchRequest:request error:NULL];
}


- (NSArray *)allHeadersForID:(NSInteger)uid folderID:(NSInteger)folderID accountType:(NSString *)accountName {
    NSManagedObjectContext *context = [self getContextByThreadId:[self currentThreadId]];

    NSPredicate *predicate1 = [NSPredicate predicateWithFormat:@"uid == %i", uid];
    NSPredicate *predicate2 = [NSPredicate predicateWithFormat:@"folderID == %i", folderID];
    NSPredicate *predicate3 = [NSPredicate predicateWithFormat:@"%K like[cd] %@", @"accountName", [NSString stringWithFormat:@"*%@*", accountName]];
    NSPredicate *compoundPredicate = [NSCompoundPredicate andPredicateWithSubpredicates:[NSArray arrayWithObjects:predicate1, predicate2, predicate3, nil]];
    
    
    
    NSFetchRequest *request = [[[NSFetchRequest alloc] init] autorelease];
    NSEntityDescription *entity = [NSEntityDescription entityForName:@"THeaders" inManagedObjectContext:context];
    [request setEntity:entity];
    
    [request setPredicate:compoundPredicate];
    
	// execute
	return [context executeFetchRequest:request error:NULL];

}

- (NSArray *)allHeadersForFolderID:(NSInteger)folderID accountType:(NSString *)accountName {
    NSManagedObjectContext *context = [self getContextByThreadId:[self currentThreadId]];

    NSPredicate *predicate2 = [NSPredicate predicateWithFormat:@"folderID == %i", folderID];
    NSPredicate *predicate3 = [NSPredicate predicateWithFormat:@"%K like[cd] %@", @"accountName", [NSString stringWithFormat:@"*%@*", accountName]];
    NSPredicate *compoundPredicate = [NSCompoundPredicate andPredicateWithSubpredicates:[NSArray arrayWithObjects: predicate2, predicate3, nil]];
    
    
    
    NSFetchRequest *request = [[[NSFetchRequest alloc] init] autorelease];
    NSEntityDescription *entity = [NSEntityDescription entityForName:@"THeaders" inManagedObjectContext:context];
    [request setEntity:entity];
    
    [request setPredicate:compoundPredicate];
    
	// execute
	return [context executeFetchRequest:request error:NULL];
}

- (NSArray *)allMessagesForUID:(NSInteger)uid accountName:(NSString *)accountName entityName:(NSString *)entityName  {
    NSManagedObjectContext *context = [self getContextByThreadId:[self currentThreadId]];

    NSFetchRequest *request = [[[NSFetchRequest alloc] init] autorelease];
    NSEntityDescription *entity = [NSEntityDescription entityForName:entityName inManagedObjectContext:context];
    [request setEntity:entity];

    if (!accountName) {
        NSPredicate *predicate1 = [NSPredicate predicateWithFormat:@"uid == %i", uid];
        [request setPredicate:predicate1];
    } else {
        NSPredicate *predicate1 = [NSPredicate predicateWithFormat:@"uid == %i", uid];
        NSPredicate *predicate2 = [NSPredicate predicateWithFormat:@"%K like[cd] %@", @"accountName", [NSString stringWithFormat:@"*%@*", accountName]];
        NSPredicate *compoundPredicate = [NSCompoundPredicate andPredicateWithSubpredicates:[NSArray arrayWithObjects: predicate1, predicate2, nil]];
        
        [request setPredicate:compoundPredicate];
    }
    
	// execute
	return [context executeFetchRequest:request error:NULL];
    
}

- (BOOL)isTVChannelExists:(NSString *)sid {
    NSArray *allRows = [self allForEntity:@"TVChannel" forFieldName:@"channelID" withFieldValue:sid];
    return ([allRows count] != 0);
}

- (void)deleteChannelByID:(NSString *)sid {
    NSArray *allRows = [self allForEntity:@"TVChannel" forFieldName:@"channelID" withFieldValue:sid];
    for (TVChannel *channel in allRows) {
        [self removeEntityModel:channel];
    }
}

- (void)updateTVChannel:(NSDictionary *)channel {
    NSArray *allRows = [self allForEntity:@"TVChannel" forFieldName:@"channelID" withFieldValue:[channel objectForKey:@"sid"]];
//    NSString *sid = [channel objectForKey:@"sid"];
    for (TVChannel *stModel in allRows) {
        stModel.channelTitle = [channel objectForKey:@"title"];
        stModel.channelUrl = [channel objectForKey:@"url"];
        stModel.channelID = [channel objectForKey:@"sid"];
        [self save];
    }
    if ([allRows count] == 0) {
        TVChannel *messageModel = (TVChannel *)[[DataManager sharedInstance] createEntityWithName:@"TVChannel"];
        messageModel.channelTitle = [channel objectForKey:@"title"];
        messageModel.channelUrl = [channel objectForKey:@"url"];
        messageModel.channelID = [NSString stringWithFormat:@"%f", [[NSDate date] timeIntervalSince1970]];
        [self save];
    }
}

- (void)addChannel:(NSDictionary*)channel
{
    TVChannel *messageModel = (TVChannel *)[[DataManager sharedInstance] createEntityWithName:@"TVChannel"];
    messageModel.channelTitle = [channel objectForKey:@"title"];
    messageModel.channelUrl = [channel objectForKey:@"url"];
    messageModel.channelID = [NSString stringWithFormat:@"%f", [[NSDate date] timeIntervalSince1970]];
    
    messageModel.language = channel[@"language"];
    messageModel.country = channel[@"country"];
    messageModel.genre = channel[@"genre"];
}

- (void)addRadioChannel:(NSDictionary*)channel
{
    RadioChannel *messageModel = (RadioChannel *)[[DataManager sharedInstance] createEntityWithName:@"RadioChannel"];
    messageModel.channelTitle = [channel objectForKey:@"title"];
    messageModel.channelUrl = [channel objectForKey:@"url"];
    messageModel.channelID = [NSString stringWithFormat:@"%f", [[NSDate date] timeIntervalSince1970]];
    
    messageModel.language = channel[@"language"];
    messageModel.country = channel[@"country"];
    messageModel.genre = channel[@"genre"];
}

- (NSManagedObjectModel*)objectModel
{
	if (_objectModel)
    {
		return _objectModel;
    }

	NSURL *modelURL = [[NSBundle mainBundle] URLForResource:@"GrannyApp" withExtension:@"momd"];
	_objectModel = [[NSManagedObjectModel alloc] initWithContentsOfURL:modelURL];
    
	return _objectModel;
}



- (NSPersistentStoreCoordinator*)persistentStoreCoordinator
{
	if (_persistentStoreCoordinator)
    {
		return _persistentStoreCoordinator;
    }
    
	// Get the paths to the SQLite file
	NSURL *storeURL = [[self applicationDocumentsDirectory] URLByAppendingPathComponent:@"GrannyApp.sqlite"];
    
	// Define the Core Data version migration options
	NSDictionary *options = [NSDictionary dictionaryWithObjectsAndKeys:
                             [NSNumber numberWithBool:YES], NSMigratePersistentStoresAutomaticallyOption,
                             [NSNumber numberWithBool:YES], NSInferMappingModelAutomaticallyOption,
                             nil];
    
	// Attempt to load the persistent store
	NSError *error = nil;
	_persistentStoreCoordinator = [[NSPersistentStoreCoordinator alloc] initWithManagedObjectModel:self.objectModel];
	if (![_persistentStoreCoordinator addPersistentStoreWithType:NSSQLiteStoreType
                                                   configuration:nil
                                                             URL:storeURL
                                                         options:options
                                                           error:&error]) {
		NSLog(@"Fatal error while creating persistent store: %@", error);
		abort();
	}
    
	return _persistentStoreCoordinator;
}


- (BOOL)save 
{
    NSManagedObjectContext *context = [self getContextByThreadId:[self currentThreadId]];

	if (![context hasChanges])
    {
		return YES;
    }
    
	NSError *error = nil;
    
	if (![context save:&error])
    {
		NSLog(@"Error while saving: %@\n%@", [error localizedDescription], [error userInfo]);
		
		return NO;
	}
    
	return YES;
}

- (NSManagedObjectContext *)getContextByThreadId:(NSString *)threadId {
    NSManagedObjectContext *context = nil;
    id obj = [contextQueue objectForKey:threadId];
    if (obj!= nil && [obj isKindOfClass:NSManagedObjectContext.class]) {
        context = (NSManagedObjectContext *)obj;
    } else {
        context = [[[NSManagedObjectContext alloc] init] autorelease];
        NSPersistentStoreCoordinator *coordinator = [self persistentStoreCoordinator];
        [context setPersistentStoreCoordinator:coordinator];
        [contextQueue setObject:context forKey:threadId];

    }
    return context;
}


- (NSString *)currentThreadId {
    mach_port_t machTID = pthread_mach_thread_np(pthread_self());
    return [NSString stringWithFormat:@"%x", machTID];
}

- (void)killContext {
    [contextQueue removeAllObjects];
}



@end