//
//  GRMailDataManager.m
//  GrannyApp
//
//  Created by Ievgen Pavliuk on 6/19/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "GRMailDataManager.h"
#import "DataManager.h"
#import "GRPeopleManager.h"
#import "GRContact.h"


@implementation GRMailDataManager

@synthesize listReadMessages, listSentMessages, listDraftMessages, listTrashMessages, fromContact, contact;

static GRMailDataManager *instance;

#pragma mark - Initialization

+ (GRMailDataManager *)sharedManager {
    @synchronized(self) {
        if (!instance) {
            instance = [[GRMailDataManager alloc] init];
        }
    }
    return instance;
}

#pragma mark - Create local messages

- (void)saveInboxMessage:(MCOIMAPMessage *)message {
    
    MCOMessageHeader *header = message.header;
    TMailMessage *messageModel = (TMailMessage *)[[DataManager sharedInstance] createEntityWithName:@"TMailMessage"];
    
    messageModel.accountName = [GRMailServiceManager shared].currentAccount.accountName;
    messageModel.messageID = [NSString stringWithFormat:@"%i", message.uid];
    messageModel.messageBodyPlain = @"";
    messageModel.messageBodyHTML = @"";
    messageModel.date = header.receivedDate;
    messageModel.subject = header.subject;
    messageModel.uid = [NSNumber numberWithInteger:message.uid];
    messageModel.senderName = header.sender.displayName;
    messageModel.senderEmail = header.sender.mailbox;
    messageModel.isFetched = [NSNumber numberWithBool:NO];
    NSMutableArray *array = [NSMutableArray array];
    for (MCOAddress *address in header.to) {
        [array addObject:address.mailbox];
    }
    for (MCOAddress *address in header.cc) {
        [array addObject:address.mailbox];
    }
    for (MCOAddress *address in header.bcc) {
        [array addObject:address.mailbox];
    }
    
    NSData *tosData = [NSKeyedArchiver archivedDataWithRootObject:array];
    messageModel.tos = tosData;
    if ((message.flags & MCOMessageFlagSeen) == MCOMessageFlagSeen) {
        messageModel.isRead = [NSNumber numberWithBool:YES];
    } else {
        messageModel.isRead = [NSNumber numberWithBool:NO];
    }
    GRContact *contact2 = [[GRPeopleManager sharedManager] findContactByEmail:messageModel.senderEmail];
    if (contact2)
    {
        NSLog(@"%@", contact2.photourl);
        if (contact2.photourl && [[NSFileManager defaultManager] fileExistsAtPath:contact2.photourl]) {
            messageModel.senderPhotoUrl = contact.photourl;
        }
    }
    for (NSString *s in message.gmailLabels) {
        NSLog(@"%@", s);
    }
    
    MCOAbstractPart *mainPart = message.mainPart;
    NSLog(@"%d", mainPart.partType);
    NSLog(@"%@", mainPart.mimeType);
    if (mainPart.partType == MCOPartTypeMultipartAlternative) {
        messageModel.mailType = @"single";
    } else {
        messageModel.mailType = @"Mixed";
    }
    
    
    [[DataManager sharedInstance] save];
    [self saveMessageHeader:message folderID:MAIL_FOLDER_INBOX];
}


- (void)saveSentMessage:(MCOIMAPMessage *)message {
    MCOMessageHeader *header = message.header;
    if ([header.to count] == 0) {
        return;
    }
    TMailSentMessage *messageModel = (TMailSentMessage *)[[DataManager sharedInstance] createEntityWithName:@"TMailSentMessage"];
    messageModel.accountName = [GRMailServiceManager shared].currentAccount.accountName;
    messageModel.messageID = [NSString stringWithFormat:@"%i", message.uid];;
    messageModel.messageBodyPlain = @"";
    messageModel.messageBodyHTML = @"";
    messageModel.date = header.date;
    messageModel.subject = header.subject;;
    messageModel.mailType = @"imap";
    messageModel.uid = [NSNumber numberWithInteger:message.uid];
    messageModel.isFetched = [NSNumber numberWithBool:NO];
    messageModel.senderName = header.sender.displayName;
    messageModel.senderEmail = header.sender.mailbox;
    NSMutableArray *array = [NSMutableArray array];
    for (MCOAddress *address in header.to) {
        [array addObject:address.mailbox];
    }
    
    NSData *tosData = [NSKeyedArchiver archivedDataWithRootObject:array];
    messageModel.tos = tosData;
    
    GRContact *contact2 = [[GRPeopleManager sharedManager] findContactByEmail:messageModel.senderEmail];
    if (contact2) {
        if (contact2.photourl && [[NSFileManager defaultManager] fileExistsAtPath:contact2.photourl]) {
            messageModel.senderPhotoUrl = contact2.photourl;
        }
    }
    [[DataManager sharedInstance] save];
    [self saveMessageHeader:message folderID:MAIL_FOLDER_SENT];
}


- (void)saveTrashMessage:(MCOIMAPMessage *)message {
    MCOMessageHeader *header = message.header;
    TMailDeletedMessage *messageModel = (TMailDeletedMessage *)[[DataManager sharedInstance] createEntityWithName:@"TMailDeletedMessage"];
    messageModel.accountName = [GRMailServiceManager shared].currentAccount.accountName;
    messageModel.messageID = [NSString stringWithFormat:@"%i", message.uid];
    messageModel.messageBodyPlain = @"";
    messageModel.messageBodyHTML = @"";
    messageModel.date = header.date;
    messageModel.subject = header.subject;
    messageModel.mailType = @"imap";
    messageModel.uid = [NSNumber numberWithInteger:message.uid];
    messageModel.isFetched = [NSNumber numberWithBool:NO];
    messageModel.senderName = header.sender.displayName;
    messageModel.senderEmail = header.sender.mailbox;
    messageModel.deleted = [NSNumber numberWithInt:0];

    NSMutableArray *array = [NSMutableArray array];
    for (MCOAddress *address in header.to) {
        [array addObject:address.mailbox];
    }
    for (MCOAddress *address in header.cc) {
        [array addObject:address.mailbox];
    }
    for (MCOAddress *address in header.bcc) {
        [array addObject:address.mailbox];
    }
    
    
    NSData *tosData = [NSKeyedArchiver archivedDataWithRootObject:array];
    messageModel.tos = tosData;
    
    GRContact *contact2 = [[GRPeopleManager sharedManager] findContactByEmail:messageModel.senderEmail];
    if (contact2) {
        if (contact2.photourl && [[NSFileManager defaultManager] fileExistsAtPath:contact2.photourl]) {
            messageModel.senderPhotoUrl = contact2.photourl;
        }
    }
    [[DataManager sharedInstance] save];
    [self saveMessageHeader:message folderID:MAIL_FOLDER_TRASH];
}

- (void)saveDraftMessage:(MCOIMAPMessage *)message {
    MCOMessageHeader *header = message.header;
    TMailDeletedMessage *messageModel = (TMailDeletedMessage *)[[DataManager sharedInstance] createEntityWithName:@"TMailDraftMessage"];
    messageModel.accountName = [GRMailServiceManager shared].currentAccount.accountName;
    messageModel.messageID = [NSString stringWithFormat:@"%i", message.uid];
    messageModel.messageBodyPlain = @"";
    messageModel.messageBodyHTML = @"";
    messageModel.date = header.date;
    messageModel.subject = header.subject;
    messageModel.mailType = @"imap";
    messageModel.uid = [NSNumber numberWithInteger:message.uid];
    messageModel.isFetched = [NSNumber numberWithBool:NO];
    messageModel.senderName = header.sender.displayName;
    messageModel.senderEmail = header.sender.mailbox;

    NSMutableArray *array = [NSMutableArray array];
    for (MCOAddress *address in header.to) {
        [array addObject:address.mailbox];
    }
    for (MCOAddress *address in header.cc) {
        [array addObject:address.mailbox];
    }
    for (MCOAddress *address in header.bcc) {
        [array addObject:address.mailbox];
    }
    
    NSData *tosData = [NSKeyedArchiver archivedDataWithRootObject:array];
    messageModel.tos = tosData;
    
    GRContact *contact2 = [[GRPeopleManager sharedManager] findContactByEmail:messageModel.senderEmail];
    if (contact2) {
        if (contact2.photourl && [[NSFileManager defaultManager] fileExistsAtPath:contact2.photourl]) {
            messageModel.senderPhotoUrl = contact2.photourl;
        }
    }
    [[DataManager sharedInstance] save];
    [self saveMessageHeader:message folderID:MAIL_FOLDER_DRAFTS];
}


- (void)saveMessageHeader:(MCOIMAPMessage *)message folderID:(NSInteger)folderID {
    THeaders *messageHeader = (THeaders *)[[DataManager sharedInstance] createEntityWithName:@"THeaders"];
    messageHeader.uid = [NSNumber numberWithInteger:message.uid];
    messageHeader.accountName = [GRMailServiceManager shared].currentAccount.accountName;
    messageHeader.folderID = [NSNumber numberWithInt:folderID];
    [[DataManager sharedInstance] save];
}




- (NSArray *)findHeaderMessageByID:(NSInteger)messageID  folderID:(NSInteger)folderID {
    NSArray *results = [[DataManager sharedInstance] allHeadersForID:messageID folderID:folderID accountType:[GRMailServiceManager shared].currentAccount.accountName];
    
    
    return results;
}

- (NSArray *)findHeaderMessagesByFolderID:(NSInteger)folderID {
    NSArray *results = [[DataManager sharedInstance] allHeadersForFolderID:folderID accountType:[GRMailServiceManager shared].currentAccount.accountName];
    return results;
}

- (NSArray *)findHeadersByAccountName:(NSString *)accountName {
    NSArray *results = [[DataManager sharedInstance] allForEntity:@"THeaders" forFieldName:@"accountName" withFieldValue:accountName];
    return results;
}

- (void)removeIndoxMessageByID:(NSInteger)uid {
    NSArray *rows = [[DataManager sharedInstance] allMessagesForUID:uid accountName:[GRMailServiceManager shared].currentAccount.accountName entityName:@"TMailMessage"];
    for (TMailMessage *model in rows) {
        [[DataManager sharedInstance] removeEntityModel:model];
    }
    [self removeHeaderByID:uid];
}

- (void)removeSentMessageByID:(NSInteger)uid {
    NSArray *rows = [[DataManager sharedInstance] allMessagesForUID:uid accountName:[GRMailServiceManager shared].currentAccount.accountName entityName:@"TMailSentMessage"];
    for (TMailSentMessage *model in rows) {
        [[DataManager sharedInstance] removeEntityModel:model];
    }
    [self removeHeaderByID:uid];
}

- (void)removeDraftsMessageByID:(NSInteger)uid {
    NSArray *rows = [[DataManager sharedInstance] allMessagesForUID:uid accountName:[GRMailServiceManager shared].currentAccount.accountName entityName:@"TMailDraftMessage"];
    for (TMailDraftMessage *model in rows) {
        [[DataManager sharedInstance] removeEntityModel:model];
    }
    [self removeHeaderByID:uid];
}

- (void)removeTrashMessageByID:(NSInteger)uid {
    NSArray *rows = [[DataManager sharedInstance] allMessagesForUID:uid accountName:[GRMailServiceManager shared].currentAccount.accountName entityName:@"THeaders"];
    for (THeaders *model in rows) {
        model.deleted = [NSNumber numberWithInt:1];
    }
    NSArray *rows2 = [[DataManager sharedInstance] allMessagesForUID:uid accountName:[GRMailServiceManager shared].currentAccount.accountName entityName:@"TMailDeletedMessage"];
    for (TMailDeletedMessage *model in rows2) {
        model.deleted = [NSNumber numberWithInt:1];
    }

    [[DataManager sharedInstance] save];
}

- (void)removeHeaderByID:(NSInteger)uid {
    NSArray *rows = [[DataManager sharedInstance] allMessagesForUID:uid accountName:[GRMailServiceManager shared].currentAccount.accountName entityName:@"THeaders"];
    for (THeaders *model in rows) {
        [[DataManager sharedInstance] removeEntityModel:model];
    }
}


#pragma mark - Update fetched messages


- (void)fetchMessage:(MCOMessageParser *)message uid:(NSInteger)uid folderIndex:(NSInteger)folderIndex  {
    NSArray *results = nil;
    switch (folderIndex) {
        case MAIL_FOLDER_INBOX:
            results = [[DataManager sharedInstance] allMessagesForUID:uid accountName:[GRMailServiceManager shared].currentAccount.accountName entityName:@"TMailMessage"];
            for (TMailMessage *messageModel in results) {
                messageModel.messageBodyPlain = [message.htmlBodyRendering mco_flattenHTML];
                messageModel.messageBodyHTML = message.htmlBodyRendering;
                messageModel.isFetched = [NSNumber numberWithInt:1];
                messageModel.isRead = [NSNumber numberWithInt:1];
                if (message.htmlInlineAttachments.count != 0) {
                    messageModel.isInlineAttachments = [NSNumber numberWithInt:1];
                }
                [[DataManager sharedInstance] save];
            }
            break;
        case MAIL_FOLDER_DRAFTS:
            results = [[DataManager sharedInstance] allMessagesForUID:uid accountName:[GRMailServiceManager shared].currentAccount.accountName entityName:@"TMailDraftMessage" ];
            for (TMailDraftMessage *messageModel in results) {
                messageModel.messageBodyPlain = [message.htmlBodyRendering mco_flattenHTML];
                messageModel.messageBodyHTML = message.htmlBodyRendering;
                messageModel.isFetched = [NSNumber numberWithInt:1];
                messageModel.isRead = [NSNumber numberWithInt:1];
                if (message.htmlInlineAttachments.count != 0) {
                    messageModel.isInlineAttachments = [NSNumber numberWithInt:1];
                }
                [[DataManager sharedInstance] save];
            }
            break;
        case MAIL_FOLDER_SENT:
            results = [[DataManager sharedInstance] allMessagesForUID:uid accountName:[GRMailServiceManager shared].currentAccount.accountName entityName:@"TMailSentMessage"];
            for (TMailSentMessage *messageModel in results) {
                messageModel.messageBodyPlain = [message.htmlBodyRendering mco_flattenHTML];
                messageModel.messageBodyHTML = message.htmlBodyRendering;
                messageModel.isFetched = [NSNumber numberWithInt:1];
                messageModel.isRead = [NSNumber numberWithInt:1];
                if (message.htmlInlineAttachments.count != 0) {
                    messageModel.isInlineAttachments = [NSNumber numberWithInt:1];
                }
                [[DataManager sharedInstance] save];
            }
            break;
        case MAIL_FOLDER_TRASH:
            results = [[DataManager sharedInstance] allMessagesForUID:uid accountName:[GRMailServiceManager shared].currentAccount.accountName entityName:@"TMailDeletedMessage"];
            for (TMailDeletedMessage *messageModel in results) {
                messageModel.messageBodyPlain = [message.htmlBodyRendering mco_flattenHTML];
                messageModel.messageBodyHTML = message.htmlBodyRendering;
                messageModel.isFetched = [NSNumber numberWithInt:1];
                messageModel.isRead = [NSNumber numberWithInt:1];
                if (message.htmlInlineAttachments.count != 0) {
                    messageModel.isInlineAttachments = [NSNumber numberWithInt:1];
                }
                [[DataManager sharedInstance] save];
            }
            break;
            
        default:
            break;
    }
}


#pragma mark - Pending message

- (void)savePendingMessage:(GRSMTPMessage *)sMessage {
    TPendingMessage *messageModel = (TPendingMessage *)[[DataManager sharedInstance] createEntityWithName:@"TPendingMessage"];
    messageModel.uid = [NSNumber numberWithInteger:sMessage.uid];
    messageModel.senderMail = sMessage.senderMail;
    messageModel.textHTML = sMessage.htmlText;
    messageModel.textPlain = sMessage.plainText;
    messageModel.subject = sMessage.subject;
    messageModel.attachments = [NSKeyedArchiver archivedDataWithRootObject:sMessage.attachments];
    messageModel.tos = [NSKeyedArchiver archivedDataWithRootObject:sMessage.tos];
    messageModel.accountName = [GRMailServiceManager shared].currentAccount.accountName;
    [[DataManager sharedInstance] save];
}

- (void)removePendingMessageWithID:(NSInteger)uid {
    NSArray *rows = [[DataManager sharedInstance] allMessagesForUID:uid accountName:[GRMailServiceManager shared].currentAccount.accountName entityName:@"TPendingMessage"];
    for (TPendingMessage *model in rows) {
        [[DataManager sharedInstance] removeEntityModel:model];
    }
}

- (NSArray *)getAllPendingMessagesForAccount:(NSString *)accountName {
    NSArray *rows = [[DataManager sharedInstance] allForEntity:@"TPendingMessage" forFieldName:@"accountName" withFieldValue:accountName];
    return rows;
}



- (NSArray *)messageWithID:(NSString *)messageID
{
    NSArray *allRows = [[DataManager sharedInstance] allForEntity:@"TMailMessage" forFieldName:@"messageID" withFieldValue:messageID];
    return allRows;
}
- (NSArray *)sentMessageWithID:(NSString *)messageID
{
    NSArray *allRows = [[DataManager sharedInstance] allForEntity:@"TMailSentMessage" forFieldName:@"messageID" withFieldValue:messageID];
    return allRows;
}
- (NSArray *)draftMessageWithID:(NSString *)messageID
{
    NSArray *allRows = [[DataManager sharedInstance] allForEntity:@"TMailDraftMessage" forFieldName:@"messageID" withFieldValue:messageID];
    return allRows;
}
- (NSArray *)trashMessageWithID:(NSString *)messageID
{
    NSArray *allRows = [[DataManager sharedInstance] allForEntity:@"TMailDeletedMessage" forFieldName:@"messageID" withFieldValue:messageID];
    return allRows;
}

- (BOOL)isMessageExistsWithID:(NSString *)messageID  {
    NSArray *allRows = [[DataManager sharedInstance] allForEntity:@"TMailMessage" forFieldName:@"messageID" withFieldValue:messageID];
    return ([allRows count] != 0);
}

- (BOOL)isSentMessageExistsWithID:(NSString *)messageID  {
    NSArray *allRows = [[DataManager sharedInstance] allForEntity:@"TMailSentMessage" forFieldName:@"messageID" withFieldValue:messageID];
    return ([allRows count] != 0);
}

- (BOOL)isTrashMessageExistsWithID:(NSString *)messageID  {
    NSArray *allRows = [[DataManager sharedInstance] allForEntity:@"TMailDeletedMessage" forFieldName:@"messageID" withFieldValue:messageID];
    return ([allRows count] != 0);
}
- (BOOL)isDraftMessageExistsWithID:(NSString *)messageID  {
    NSArray *allRows = [[DataManager sharedInstance] allForEntity:@"TMailDraftMessage" forFieldName:@"messageID" withFieldValue:messageID];
    return ([allRows count] != 0);
}

#pragma mark - Retreive messages

- (NSArray *)getAllMessagesForAccountName:(NSString *)accountName {
    NSArray *allRows = [[DataManager sharedInstance] allForEntity:@"TMailMessage" forFieldName:@"accountName" withFieldValue:accountName];
    NSLog(@"Getting rows");
    return allRows;
}

- (NSArray *)getAllSentMessagesForAccountName:(NSString *)accountName {
    NSArray *allRows = [[DataManager sharedInstance] allForEntity:@"TMailSentMessage" forFieldName:@"accountName" withFieldValue:accountName];
    return allRows;
}

- (NSArray *)getAllTrashMessagesForAccountName:(NSString *)accountName {
    NSArray *allRows = [[DataManager sharedInstance] allNotDeletedForEntity:@"TMailDeletedMessage" forFieldName:@"accountName" withFieldValue:accountName];
    return allRows;
}

- (NSArray *)getAllDraftMessagesForAccountName:(NSString *)accountName {
    NSArray *allRows = [[DataManager sharedInstance] allForEntity:@"TMailDraftMessage" forFieldName:@"accountName" withFieldValue:accountName];
    return allRows;
}

- (NSArray *)getAllInboxMessagesForEmail:(NSString *)email accountName:(NSString *)accountName {
    NSArray *allRows = [[DataManager sharedInstance] allForEntity:@"TMailMessage" forFieldName1:@"senderEmail" withFieldValue1:email forFieldName2:@"accountName" withFieldValue2:accountName];
    return allRows;
}


- (NSArray *)getAllAccountsByName:(NSString *)accountName {
    NSArray *allRows = [[DataManager sharedInstance] allForEntity:@"TMailAccount" forFieldName:@"accountName" withFieldValue:accountName];
    return allRows;
}

#pragma mark - contacts
- (NSMutableArray *)getAllToContactsForDraftMessage:(TMailDraftMessage *)draftMessage
{
    NSMutableArray * toContacts = [NSMutableArray array];
    
    NSArray *tos = [NSKeyedUnarchiver unarchiveObjectWithData:draftMessage.tos];
    for (int i = 0; i < tos.count; i++)
    {
        NSString *sMail = [tos objectAtIndex:i];
        GRContact *sMailContact = [[GRPeopleManager sharedManager] findContactByEmail:sMail];
        if (!sMailContact) {
            sMailContact = [[[GRContact alloc] init] autorelease];
            sMailContact.email = sMail;
            if ([sMail isEqualToString:[GRMailServiceManager shared].currentAccount.userName]) {
                sMailContact.firstname = [GRMailServiceManager shared].currentAccount.accountName;
            }
        }
        [toContacts addObject:sMailContact];
        break;
    }

    
    return toContacts;
}

- (NSMutableArray *)getAllCCContactsForDraftMessage:(TMailDraftMessage *)draftMessage
{
    NSMutableArray * ccContacts = [NSMutableArray array];
    NSArray * tos = [NSKeyedUnarchiver unarchiveObjectWithData:draftMessage.tos];
    
    for (int i = 1; i < tos.count; i++)
    {
        if (i >= tos.count) {
            break;
        }
        
        NSString *sMail = [tos objectAtIndex:i];
        GRContact *sMailContact = [[GRPeopleManager sharedManager] findContactByEmail:sMail];
        if (!sMailContact) {
            sMailContact = [[[GRContact alloc] init] autorelease];
            sMailContact.email = sMail;
            if ([sMail isEqualToString:[GRMailServiceManager shared].currentAccount.userName]) {
                sMailContact.firstname = [GRMailServiceManager shared].currentAccount.accountName;
            }
            [ccContacts addObject:sMailContact];
        }
    }
    return ccContacts;
}

#pragma mark - Attachements Helper

- (void)saveAttachemntWithName:(NSString *)fileName
                   contentType:(NSString *)contentType
                     messageID:(NSInteger)uid
                      uniqueId:(NSString*)uniqueId
                      filePath:(NSString *)filePath
{
    TMessageAttachment *aModel = (TMessageAttachment *)[[DataManager sharedInstance] createEntityWithName:@"TMessageAttachment"];
    aModel.fileName = fileName;
    aModel.contentType = contentType;
    aModel.messageID = [NSNumber numberWithInt:uid];
    aModel.uniqueId = uniqueId;
    aModel.filePath = filePath;
    aModel.accountName = [GRMailServiceManager shared].currentAccount.accountName;
    [[DataManager sharedInstance] save];
}


- (NSArray *)getAttachmentsForMessageID:(NSInteger)uid
{
    NSArray *allRows = [[DataManager sharedInstance] allForEntityByID:uid forFieldName:@"messageID" entityName:@"TMessageAttachment" accountName:[GRMailServiceManager shared].currentAccount.accountName];
    return allRows;

}

- (NSArray *)getAllAttachmentsForAccount:(TMailAccount *)accountEntity {
    NSArray *allRows = [[DataManager sharedInstance] allForEntity:@"TMessageAttachment" forFieldName:@"accountName" withFieldValue:accountEntity.accountName];
    return  allRows;
}

//
//#pragma mark - Current Message helper
//
//- (CTCoreMessage *)findMessageByLocalID:(NSString *)messageId folderIndex:(NSInteger)folderIndex {
//    CTCoreMessage *coreMessage = nil;
//    switch (folderIndex) {
//        case 0:
//            for (CTCoreMessage *msg in self.listReadMessages) {
//                if ([msg.messageId isEqualToString:messageId]) {
//                    coreMessage = msg;
//                    break;
//                }
//            }
//            break;
//        case 1:
//            for (CTCoreMessage *msg in self.listDraftMessages) {
//                if ([msg.messageId isEqualToString:messageId]) {
//                    coreMessage = msg;
//                    break;
//                }
//            }
//            break;
//        case 2:
//            for (CTCoreMessage *msg in self.listSentMessages) {
//                if ([msg.messageId isEqualToString:messageId]) {
//                    coreMessage = msg;
//                    break;
//                }
//            }
//            break;
//        case 3:
//            for (CTCoreMessage *msg in self.listTrashMessages) {
//                if ([msg.messageId isEqualToString:messageId]) {
//                    coreMessage = msg;
//                    break;
//                }
//            }
//            break;
//            
//        default:
//            break;
//    }
//    return coreMessage;
//}

@end
