
// Solitaire for iOS
// tepaanan@gmail.com
// FINLAND


#import <Foundation/Foundation.h>
#include <stdlib.h>

#import "Card.h"
#import "Deck.h"

@protocol SalitareGameDelegate;

@interface MainView : UIView
{
	int xCap;
	int yCap;
	
    UIImage* _backgroundImage;
    
    // Active card under user touch
	Card* _activeCard; 
    
    NSMutableArray* _cardsArray;
    
    // Source decks
    NSMutableArray* _sourceDeckArray;
    NSMutableArray* _targetDeckArray;
    Deck* _wasteDeck1;
    Deck* _wasteDeck2;
    
    CGPoint _prevPoint;
}

- (void)setPositions;
- (Card*)findActiveCard:(CGPoint)point;
- (Deck*)findActiveDeck:(CGPoint)point;
- (void)createCards:(int)width :(int)height;
- (Card*)getRandomCard;
- (BOOL)acceptCardMove:(Deck*)from :(Deck*)to :(Card*)onTopOfCard;

@property (nonatomic, retain) UIImage* backgroundImage;
@property (nonatomic, retain) NSMutableArray* sourceDeckArray;
@property (nonatomic, retain) NSMutableArray* targetDeckArray;
@property (nonatomic, retain) Deck* wasteDeck1;
@property (nonatomic, retain) Deck* wasteDeck2;
@property (nonatomic, retain) NSMutableArray* cardsArray;
@property (nonatomic, assign) NSObject<SalitareGameDelegate> *gameDelegate;

@end
