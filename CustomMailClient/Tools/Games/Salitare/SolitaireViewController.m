//
//  SolitaireViewController.m
//  Granny
//
//  Created by Eugene Pavlyuk on 3/28/12.
//  Copyright (c) 2012 Home. All rights reserved.
//

#import "SolitaireViewController.h"
#import "MainView.h"
#import "UIViewController+backgroundDim.h"

@interface SolitaireViewController()

@property (nonatomic, retain) MainView *mainView;

@property (retain, nonatomic) IBOutlet UIButton *btnPlus;
@end


@implementation SolitaireViewController

@synthesize headerImageView;
@synthesize confirmView;
@synthesize mainView;
@synthesize scrollView;

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    if (!self.mainView)
    {
        if (IS_IPAD)
        {
            CGSize size = CGSizeMake(self.view.bounds.size.width, self.view.bounds.size.height + self.view.bounds.size.height * 0.5);
            
            self.scrollView.contentSize = size;
        }
        else
        {
            self.scrollView.contentSize = CGSizeMake(self.view.bounds.size.width, 400);
        }
        
        NSInteger width;
        
        if (IS_IPAD)
        {
            width = 1024;
        }
        else
        {
            width = 320;
        }
        
        self.mainView = [[[MainView alloc] initWithFrame:CGRectMake(self.scrollView.bounds.size.width/2 - width/2, 0, width, self.scrollView.contentSize.height)] autorelease];
        self.mainView.gameDelegate = self;
        self.mainView.autoresizingMask = UIViewAutoresizingFlexibleLeftMargin | UIViewAutoresizingFlexibleRightMargin;
        self.mainView.clipsToBounds = NO;
        
        [self.scrollView addSubview:self.mainView];
        [self.mainView setPositions];
        
        //[self.scrollView sendSubviewToBack:self.mainView];
    }
}

- (BOOL)prefersStatusBarHidden
{
    return YES;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    if (IS_IPAD)
    {
        return UIInterfaceOrientationIsLandscape(interfaceOrientation);
    }

    return UIInterfaceOrientationIsLandscape(interfaceOrientation);
}


- (void)willRotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)duration
{
    [self.mainView setPositions];
}

- (void)didRotateFromInterfaceOrientation:(UIInterfaceOrientation)fromInterfaceOrientation
{
    //[self.mainView setPositions];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    UIImage *buttonBackground = nil;
    
    if (IS_IPAD)
    {
        buttonBackground = [[UIImage imageNamed:@"games_header"] resizableImageWithCapInsets:UIEdgeInsetsMake(0, 20, 0, 20)];
    }
    else
    {
        buttonBackground = [[UIImage imageNamed:@"games_header"] stretchableImageWithLeftCapWidth:60 topCapHeight:0];
    }
    
    self.headerImageView.image = buttonBackground;
    
    self.confirmView.autoresizingMask = UIViewAutoresizingFlexibleLeftMargin;
}

- (void)didReceiveMemoryWarning
{
	// Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
	
	// Release any cached data, images, etc that aren't in use.
}

- (void)viewDidUnload
{
	// Release any retained subviews of the main view.
	// e.g. self.myOutlet = nil;
    [super viewDidUnload];
    self.headerImageView = nil;
    self.confirmView = nil;
    self.scrollView = nil;
}

- (void)dealloc 
{
    self.headerImageView = nil;
    self.confirmView = nil;
    self.scrollView = nil;
    [_btnPlus release];
    [super dealloc];
}

- (IBAction)homeButtonTapped
{
    [self.navigationController popToRootViewControllerAnimated:YES];
}

- (IBAction)backButtonTapped
{
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)showPopover
{
    [self createBackgrondDimWithButton:self.btnPlus];

    [self.view addSubview:self.confirmView];
    
    
    if (IS_IPAD)
    {
        self.confirmView.frame = CGRectMake(0, 50, self.confirmView.bounds.size.width, self.confirmView.bounds.size.height);
        self.confirmView.center = CGPointMake(self.view.frame.origin.x + self.view.frame.size.width - self.confirmView.frame.size.width / 2, self.confirmView.center.y);
    }
    else
    {
        self.confirmView.frame = CGRectMake(0, 20, self.confirmView.bounds.size.width, self.confirmView.bounds.size.height);
        self.confirmView.center = CGPointMake(self.view.frame.origin.x + self.view.frame.size.width - self.confirmView.frame.size.width / 2, self.confirmView.center.y);        
    }
}

- (IBAction)addButtonTapped
{
    [self showPopover];
}

- (IBAction)newButtonTapped
{
    [self.mainView removeFromSuperview];
        
    [self.confirmView removeFromSuperview];
    [self removeBackgroundView];
    
    NSInteger width;
    
    if (IS_IPAD)
    {
        width = 1024;
    }
    else
    {
        width = 320;
    }
    
    self.mainView = [[[MainView alloc] initWithFrame:CGRectMake(self.scrollView.bounds.size.width/2 - width/2, 0, width, self.scrollView.contentSize.height)] autorelease];
    self.mainView.gameDelegate = self;
    self.mainView.clipsToBounds = NO;
    [self.scrollView addSubview:self.mainView];
    [self.mainView setPositions];
    [self.scrollView sendSubviewToBack:self.mainView];
}

- (IBAction)cancelButtonTapped
{
    [self.confirmView removeFromSuperview];
    [self removeBackgroundView];
}

#pragma mark - SalitareGameDelegate's methods

- (void)cardDraggingBegan
{
    self.scrollView.scrollEnabled = NO;
}

- (void)cardDraggingEnded
{
    self.scrollView.scrollEnabled = YES;
}

@end
