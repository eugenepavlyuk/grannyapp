//
//  SolitaireViewController.h
//  Granny
//
//  Created by Eugene Pavlyuk on 3/28/12.
//  Copyright (c) 2012 Home. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SalitareGameDelegate.h"

@interface SolitaireViewController : UIViewController <SalitareGameDelegate>
{

}

@property (nonatomic, retain) IBOutlet UIImageView *headerImageView;
@property (nonatomic, retain) IBOutlet UIView *confirmView;
@property (nonatomic, retain) IBOutlet UIScrollView *scrollView;

- (IBAction)backButtonTapped;
- (IBAction)homeButtonTapped;
- (IBAction)addButtonTapped;
- (IBAction)newButtonTapped;
- (IBAction)cancelButtonTapped;

@end

