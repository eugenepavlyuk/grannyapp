//
//  SalitareGameDelegate.h
//  GrannyApp
//
//  Created by Eugene Pavluk on 11/6/13.
//
//

#import <Foundation/Foundation.h>

@protocol SalitareGameDelegate <NSObject>

- (void)cardDraggingBegan;
- (void)cardDraggingEnded;

@end
