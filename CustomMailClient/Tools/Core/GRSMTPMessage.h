//
//  GRSMTPMessage.h
//  GrannyApp
//
//  Created by Ievgen Pavliuk on 7/15/13.
//
//

#import <Foundation/Foundation.h>

@interface GRSMTPMessage : NSObject {

    NSInteger         uid;
    NSString         *plainText;
    NSString         *htmlText;
    NSString         *subject;
    NSString         *senderMail;
    NSMutableArray   *tos;
    NSMutableArray   *attachments;
    
}
@property (nonatomic, assign) NSInteger         uid;
@property (nonatomic, retain) NSString         *plainText;
@property (nonatomic, retain) NSString         *htmlText;
@property (nonatomic, retain) NSString         *subject;
@property (nonatomic, retain) NSString         *senderMail;
@property (nonatomic, retain) NSMutableArray   *tos;
@property (nonatomic, retain) NSMutableArray   *attachments;


@end
