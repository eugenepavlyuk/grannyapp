//
//  GRPop3Message.m
//  GrannyApp
//
//  Created by Ievgen Pavliuk on 5/11/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "GRPop3Message.h"

@implementation GRPop3Message
@synthesize messageID;
@synthesize senderAddress;
@synthesize receivedDate;
@synthesize bodyPlainText;
@synthesize bodyHTML;
@synthesize subject;
@synthesize dateStr;


- (void)dealloc {
    [messageID release];
    [senderAddress release];
    [receivedDate release];
    [bodyPlainText release];
    [bodyHTML release];
    [subject release];
    [dateStr release];
    [super dealloc];
}
@end
