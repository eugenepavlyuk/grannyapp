//
//  GRPop3Message.h
//  GrannyApp
//
//  Created by Ievgen Pavliuk on 5/11/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface GRPop3Message : NSObject {
    
    NSString  *messageID;
    NSString  *senderAddress;
    NSString  *subject;
    NSString  *bodyPlainText;
    NSString  *bodyHTML;
    NSString  *dateStr; 
    
    NSDate    *receivedDate;
}
@property (nonatomic, retain) NSString  *messageID;
@property (nonatomic, retain) NSString  *senderAddress;
@property (nonatomic, retain) NSString  *subject;
@property (nonatomic, retain) NSString  *bodyPlainText;
@property (nonatomic, retain) NSString  *bodyHTML;
@property (nonatomic, retain) NSString  *dateStr;
@property (nonatomic, retain) NSDate    *receivedDate;


@end
