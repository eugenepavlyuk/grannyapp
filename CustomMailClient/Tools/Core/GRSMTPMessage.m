//
//  GRSMTPMessage.m
//  GrannyApp
//
//  Created by Ievgen Pavliuk on 7/15/13.
//
//

#import "GRSMTPMessage.h"

@implementation GRSMTPMessage
@synthesize uid;
@synthesize plainText;
@synthesize htmlText;
@synthesize subject;
@synthesize tos;
@synthesize attachments;
@synthesize senderMail;


- (void)dealloc {
    [plainText release];
    [htmlText release];
    [tos release];
    [attachments release];
    [subject release];
    [senderMail release];
    [super dealloc];
}
@end
