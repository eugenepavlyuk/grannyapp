//
//  GRRadioChannelsLoader.h
//  GrannyApp
//
//  Created by Ievgen Pavliuk on 9/18/13.
//
//

#import <Foundation/Foundation.h>
#import "GRRadioManager.h"
#import "RadioTunes.h"

#define RADIO_CSV_LOADER [GRRadioChannelsLoader shared]

@interface GRRadioChannelsLoader : NSObject <YLRadioDelegate,YLAudioSessionDelegate>{
    NSInteger nextIndex;
    NSMutableArray *validStations;
    NSMutableArray *allStations;
    BOOL timerShouldFinish;
    BOOL cancelTimer;
    NSInteger tick;
    
    NSTimer *timer;
}

+ (GRRadioChannelsLoader *)shared;
- (void)startReadDataFormUkCSV:(NSString *)fileName;
- (void)startReadDataFormFranceCSV:(NSString *)fileName;
@end
