//
//  GRRadioChannelsLoader.m
//  GrannyApp
//
//  Created by Ievgen Pavliuk on 9/18/13.
//
//

#import "GRRadioChannelsLoader.h"
#import "CHCSVParser.h"

@implementation GRRadioChannelsLoader

#pragma mark - Initialization

+ (GRRadioChannelsLoader *)shared {
    static dispatch_once_t once;
    static GRRadioChannelsLoader *sharedClient = nil;
    dispatch_once(&once, ^ { sharedClient = [[self alloc] init]; });
    return sharedClient;
}

#pragma mark - Read from CSV

- (void)startReadDataFormUkCSV:(NSString *)fileName  {
    
    validStations = [[NSMutableArray alloc] init];
    allStations = [[NSMutableArray alloc] init];
    NSString *pathToFile =[[NSBundle mainBundle] pathForResource:fileName ofType: @"csv"];
    NSError *error;
    NSString *csvString = [NSString stringWithContentsOfFile:pathToFile encoding:NSASCIIStringEncoding error:&error];
    NSArray *array = [csvString componentsSeparatedByString:@"\n"];
    for (NSString *mainString in array) {
        NSArray *stationArray = [mainString componentsSeparatedByString:@","];
        if (stationArray.count >= 6) {
            NSString *stationName, *stream, *genre;
            stationName = [stationArray objectAtIndex:1];
            stream = [stationArray objectAtIndex:4];
            genre =  [stationArray objectAtIndex:5];
            NSMutableDictionary *channelDict = [NSMutableDictionary dictionaryWithObjectsAndKeys:stationName, @"title",  stream, @"url", [genre uppercaseString], @"genre", @"UK", @"country", @"UK", @"language", nil];
            if ([self validateStation:channelDict]) {
                [allStations addObject:channelDict];
            }
        }
    }
    nextIndex = 1;
    [self startTestNextChannel];
    
}

- (void)startReadDataFormFranceCSV:(NSString *)fileName  {
    validStations = [[NSMutableArray alloc] init];
    allStations = [[NSMutableArray alloc] init];
    NSString *pathToFile =[[NSBundle mainBundle] pathForResource:fileName ofType: @"csv"];
    NSError *error;
    NSString *csvString = [NSString stringWithContentsOfFile:pathToFile encoding:NSASCIIStringEncoding error:&error];
    NSArray *array = [csvString componentsSeparatedByString:@"\n"];
    for (NSString *mainString in array) {
        NSArray *stationArray = [mainString componentsSeparatedByString:@","];
        NSString *stationName, *stream, *genre;
        if (stationArray.count >= 6) {
            stationName = [stationArray objectAtIndex:1];
            stream = [stationArray objectAtIndex:4];
            genre =  [stationArray objectAtIndex:5];
            if (![self validateUrl:stream]) {
                continue;
            }
            if ([stream  rangeOfString:@"\""].location != NSNotFound) {
                NSInteger loc = [stream  rangeOfString:@"\""].location;
                if (loc == 0) {
                    stream = [stream substringFromIndex:1];
                }
                if ([stream  rangeOfString:@"\""].location != NSNotFound) {
                    stream = [stream substringToIndex:[stream  rangeOfString:@"\""].location];
                }
            }
            
            NSMutableDictionary *channelDict = [NSMutableDictionary dictionaryWithObjectsAndKeys:stationName, @"title",  stream, @"url", [genre uppercaseString], @"genre", @"FRANCE", @"country", @"FRANCE", @"language", nil];
            if ([self validateStation:channelDict]) {
                [allStations addObject:channelDict];
            }
        }
  
    }
    nextIndex = 1;
    [self startTestNextChannel];
    
    /*
    validStations = [[NSMutableArray alloc] init];
    allStations = [[NSMutableArray alloc] init];
    NSString *pathToFile =[[NSBundle mainBundle] pathForResource:fileName ofType: @"csv"];
    NSArray *csvArray = [NSArray arrayWithContentsOfCSVFile:pathToFile];
    NSString *stationName, *stream, *genre;
    for (NSArray *stationArray in csvArray) {
        NSLog(@"%i", stationArray.count);
        if (stationArray.count >= 6) {
            stationName = [stationArray objectAtIndex:1];
            stream = [stationArray objectAtIndex:4];
            genre =  [stationArray objectAtIndex:5];
            if (![self validateUrl:stream]) {
                continue;
            }
            if ([stream  rangeOfString:@"\""].location != NSNotFound) {
                NSInteger loc = [stream  rangeOfString:@"\""].location;
                if (loc == 0) {
                    stream = [stream substringFromIndex:1];
                }
                if ([stream  rangeOfString:@"\""].location != NSNotFound) {
                    stream = [stream substringToIndex:[stream  rangeOfString:@"\""].location];
                }
            }
            
            NSMutableDictionary *channelDict = [NSMutableDictionary dictionaryWithObjectsAndKeys:stationName, @"title",  stream, @"url", [genre uppercaseString], @"genre", @"FRANCE", @"country", @"FRANCE", @"language", nil];
            if ([self validateStation:channelDict]) {
                [allStations addObject:channelDict];
            }
        }
    }
    nextIndex = 1;
    [self startTestNextChannel];
    //[self saveDataIntoPlist];
     */
}



- (void)startTestNextChannel {
    if (nextIndex == [allStations count]-1) {
        NSLog(@"!!!!!!!!!!!!!!!!!!   All Validated");
        NSLog(@"%@", validStations);
        [self saveDataIntoPlist];
        return;
    }
    NSDictionary *channelDict = [allStations objectAtIndex:nextIndex];
    [self startPlayStationStream:[channelDict objectForKey:@"url"]];
}

- (void)startPlayStationStream:(NSString *)streamUrl {
    NSLog(@"************************* Start %i from %i  test url %@", nextIndex, [allStations count], streamUrl);
    [GRRadioManager sharedManager].radioPlayer = self;
    [[GRRadioManager sharedManager] stopPlayingRadio];
    
    [[GRRadioManager sharedManager] setupRadioStation:streamUrl];
    
    if([GRRadioManager sharedManager].radio)
    {
        [[GRRadioManager sharedManager].radio setDelegate:self];
        [[GRRadioManager sharedManager].radio play];
    }
    [self startTimer];

}



- (BOOL)validateStation:(NSDictionary *)station {
    id stationName = [station objectForKey:@"title"];
    id stream = [station objectForKey:@"url"];
    id genre = [station objectForKey:@"genre"];

    if (![self isObjectStringNotEmpty:stationName]) {
        return NO;
    }
    if (![self isObjectStringNotEmpty:stream]) {
        return NO;
    }
    if (![self isObjectStringNotEmpty:genre]) {
        return NO;
    }
    
    return YES;
}

- (BOOL)validateUrl:(NSString *)url {
    if ([url rangeOfString:@"http"].location != NSNotFound) {
        return YES;
    } else {
        return NO;
    }
}

- (BOOL)isObjectStringNotEmpty:(id)object {
    BOOL flag = NO;
    if ([object isKindOfClass:NSString.class]) {
        if ([(NSString *)object length] != 0) {
            flag = YES;
        }
    }
    return flag;
}


#pragma mark - Save data into plist

- (void)saveDataIntoPlist {
    NSString *path = [[NSBundle mainBundle] pathForResource:@"RadioStations" ofType:@"plist"];
    NSMutableArray *array = [[NSMutableArray alloc] initWithContentsOfFile:path];
    NSSortDescriptor *sort = [NSSortDescriptor sortDescriptorWithKey:@"sid" ascending:YES];
    [array sortUsingDescriptors:[NSArray arrayWithObject:sort]];
    NSInteger lastIndex = [[(NSDictionary *)[array lastObject] objectForKey:@"sid"] integerValue];
    for (NSMutableDictionary *channel in validStations) {
        lastIndex++;
        [channel setObject:[NSString stringWithFormat:@"%i", lastIndex] forKey:@"sid"];
        [array addObject:channel];
    }
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSCachesDirectory,  NSUserDomainMask, YES);
    NSString *cachesDirectoryPath = [paths objectAtIndex:0];
    NSString *filePath = [cachesDirectoryPath stringByAppendingFormat:@"/RadioStations_Backup.plist"];
    
    [array writeToFile:filePath atomically:YES];
}


#pragma mark -
#pragma mark YLAudioSessionDelegate

- (void)beginInterruption {
}

- (void)endInterruptionWithFlags:(NSUInteger)flags {
}

- (void)headphoneUnplugged {
}

#pragma mark -
#pragma mark MMSRadioDelegate Methods

- (void)radioStateChanged:(YLRadio *)radio
{
    YLRadioState state = [[GRRadioManager sharedManager].radio radioState];
    
    if(state == kRadioStateConnecting)
    {
    }
    else if(state == kRadioStateBuffering)
    {
    }
    else if(state == kRadioStatePlaying)
    {
        [self stopTimer];
        NSDictionary *radioChannel = [allStations objectAtIndex:nextIndex];
        NSLog(@"************************************ Test Succeed **************************************");
        NSLog(@"Radio stream: %@", [radioChannel objectForKey:@"url"]);
        [validStations addObject:radioChannel];
        [[GRRadioManager sharedManager] stopPlayingRadio];
        nextIndex++;
        sleep(1);
        [self startTestNextChannel];
    }
    else if(state == kRadioStateStopped)
    {
    }
    else if(state == kRadioStateError)
    {
        YLRadioError error = [[GRRadioManager sharedManager].radio radioError];
        
        if(error == kRadioErrorPlaylistMMSStreamDetected)
        {
            // copy url because it will be gone when we release our _radio instance
            NSURL *url = [[[GRRadioManager sharedManager].radio url] copy];
            [GRRadioManager sharedManager].radioPlayer = self;
            [[GRRadioManager sharedManager] stopPlayingRadio];
            
            [[GRRadioManager sharedManager] setupRadioStation:[url absoluteString]];
            
            [url release];
            
            if([GRRadioManager sharedManager].radio)
            {
                [[GRRadioManager sharedManager].radio setDelegate:self];
                [[GRRadioManager sharedManager].radio play];
            }
            
            return;
        }
        [self stopTimer];
        [[GRRadioManager sharedManager] stopPlayingRadio];
        NSDictionary *radioChannel = [allStations objectAtIndex:nextIndex];
        NSLog(@"************************************ Test Failed **************************************");
        NSLog(@"Radio stream: %@", [radioChannel objectForKey:@"url"]);
        nextIndex++;
        sleep(1);
        [self startTestNextChannel];
        
        NSString *errorStr = nil;
        if(error == kRadioErrorAudioQueueBufferCreate)
            errorStr = @"Audio buffers could not be created.";
        else if(error == kRadioErrorAudioQueueCreate)
            errorStr = @"Audio queue could not be created.";
        else if(error == kRadioErrorAudioQueueEnqueue)
            errorStr = @"Audio queue enqueue failed.";
        else if(error == kRadioErrorAudioQueueStart)
            errorStr = @"Audio queue could not be started.";
        else if(error == kRadioErrorFileStreamGetProperty)
            errorStr = @"File stream get property failed.";
        else if(error == kRadioErrorFileStreamOpen)
            errorStr = @"File stream could not be opened.";
        else if(error == kRadioErrorPlaylistParsing)
            errorStr = @"Playlist could not be parsed.";
        else if(error == kRadioErrorDecoding)
            errorStr = @"Audio decoding error.";
        else if(error == kRadioErrorHostNotReachable)
            errorStr = @"Radio host not reachable.";
        else if(error == kRadioErrorNetworkError)
            errorStr = @"Network connection error.";
        else
            errorStr = @"No Internet \nconnection";
    }
}

- (void)startTimer {
    tick = 0;
    timer  = [NSTimer scheduledTimerWithTimeInterval: 0.1
                                                  target: self
                                                selector:@selector(tick)
                                                userInfo: nil repeats:YES];
}

- (void)tick {
    tick++;
    if (tick >= 300) {
        NSLog(@"Time out");
        [self stopTimer];
        nextIndex++;
        [self startTestNextChannel];
    }
}

- (void)stopTimer {
    [timer invalidate];
    timer = nil;
}

@end
