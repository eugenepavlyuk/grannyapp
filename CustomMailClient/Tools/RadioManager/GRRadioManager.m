//
//  GRPeopleManager.m
//  GrannyApp
//
//  Created by Ievgen Pavliuk on 6/21/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "GRRadioManager.h"
#import "YLRadio.h"
#import "YLMMSRadio.h"
#import "YLHTTPRadio.h"

@implementation GRRadioManager

@synthesize player, mute, volumeValue, currentStationIndex, is3gOn;

@synthesize radio;
@synthesize isFirstPlaying;

static GRRadioManager *instance;

#pragma mark - Initialization

+ (GRRadioManager *)sharedManager
{
    @synchronized(self)
    {
        if (!instance)
        {
            instance = [[GRRadioManager alloc] init];
        }
        return instance;
    }
}

- (id)init
{
    if ((self  = [super init]))
    {
        self.volumeValue = 0.5;
    }
    return self;
}

- (BOOL) is3G
{
    if ([[YLReachability reachabilityForInternetConnection] currentReachabilityStatus] == ReachableViaWWAN)
        return YES;
    else
        return NO;
}
- (void)dealloc
{
    self.player = nil;
   [super dealloc];
}

- (void)stopPlayingRadio
{
    if (radio)
    {
        radio.delegate = nil;
        [radio shutdown];
        [radio release];
        radio = nil;
    }
}

- (void)setupRadioStation:(NSString*)radioUrl
{
    if([radioUrl hasPrefix:@"mms"])
    {
        radio = [[YLMMSRadio alloc] initWithURL:[NSURL URLWithString:radioUrl]];
    }
    else
    {
        radio = [[YLHTTPRadio alloc] initWithURL:[NSURL URLWithString:radioUrl]];
    }
}

@end
