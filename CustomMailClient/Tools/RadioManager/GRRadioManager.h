//
//  GRPeopleManager.h
//  GrannyApp
//
//  Created by Ievgen Pavliuk on 6/21/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <MediaPlayer/MediaPlayer.h>

@class YLRadio;

@interface GRRadioManager : NSObject
{
    MPMoviePlayerController *player;
    BOOL mute;
    float volumeValue;
    int currentStationIndex;
    BOOL is3gOn;
    
    YLRadio *radio;
    BOOL isFirstPlaying;
}

@property (nonatomic, retain) MPMoviePlayerController *player;
@property BOOL mute;
@property float volumeValue;
@property int currentStationIndex;
@property BOOL is3gOn;
@property (nonatomic, assign) BOOL isFirstPlaying;
@property (nonatomic, retain) NSObject *radioPlayer;

@property (nonatomic, readonly) YLRadio *radio;

+ (GRRadioManager *)sharedManager;

- (BOOL) is3G;

- (void)stopPlayingRadio;
- (void)setupRadioStation:(NSString*)url;

@end
