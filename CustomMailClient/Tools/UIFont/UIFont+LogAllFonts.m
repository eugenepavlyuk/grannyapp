//
//  UIFont+LogAllFonts.m
//  GrannyApp
//
//  Created by Eugene Pavluk on 3/27/13.
//
//

#import "UIFont+LogAllFonts.h"

@implementation UIFont (LogAllFonts)

+ (void)logAllFonts
{
    NSArray *allFamilies = [UIFont familyNames];
    
    NSLog(@"%@", allFamilies);
    
    for (NSString *familyName in allFamilies)
    {
        NSArray *allFonts = [UIFont fontNamesForFamilyName:familyName];
        
        NSLog(@"%@", allFonts);
    }
}

@end
