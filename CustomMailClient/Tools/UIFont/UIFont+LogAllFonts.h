//
//  UIFont+LogAllFonts.h
//  GrannyApp
//
//  Created by Eugene Pavluk on 3/27/13.
//
//

#import <UIKit/UIKit.h>

@interface UIFont (LogAllFonts)

+ (void)logAllFonts;

@end
