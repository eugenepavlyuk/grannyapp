//
//  UIViewController+backgroundDim.h
//  GrannyApp
//
//  Created by Pavel Gubin on 27.01.14.
//
//

#import <UIKit/UIKit.h>

@interface UIViewController (backgroundDim)

- (void) createBackgrondDimWithButton:(UIButton *) targetButton;
- (void) removeBackgroundView;

@end
