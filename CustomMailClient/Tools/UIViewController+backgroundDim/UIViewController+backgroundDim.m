//
//  UIViewController+backgroundDim.m
//  GrannyApp
//
//  Created by Pavel Gubin on 27.01.14.
//
//

#import "UIViewController+backgroundDim.h"

#define tagBackgroundView 50

@implementation UIViewController (backgroundDim)

- (void) createBackgrondDimWithButton:(UIButton *) targetButton
{
    if (![self.view viewWithTag:tagBackgroundView])
    {
        UIView *backgroundView = [[UIView alloc] initWithFrame:self.view.bounds];
        backgroundView.backgroundColor = [UIColor blackColor];
        backgroundView.alpha = 0;
        backgroundView.tag = tagBackgroundView;
        [self.view addSubview:backgroundView];
        
        UIButton *btn = [UIButton buttonWithType:UIButtonTypeCustom];
        btn.frame = targetButton.frame;
        btn.titleLabel.font = targetButton.titleLabel.font;
        UIImage *image = [targetButton imageForState:UIControlStateNormal];
        
        if (image)
        {
            [btn setImage:image forState:UIControlStateNormal];
        }

        NSString *title = [targetButton titleForState:UIControlStateNormal];
        
        if (title.length)
        {
            [btn setTitle:title forState:UIControlStateNormal];
        }
        
        [backgroundView addSubview:btn];
        
        [UIView animateWithDuration:0.3 animations:^{
            backgroundView.alpha = 0.8;
        }];
    }
}

- (void) removeBackgroundView
{
    [UIView animateWithDuration:0.3 animations:^{
        [[self.view viewWithTag:tagBackgroundView] setAlpha:0];
    } completion:^(BOOL finished) {
        [[self.view viewWithTag:tagBackgroundView] removeFromSuperview];
    }];
}

@end
