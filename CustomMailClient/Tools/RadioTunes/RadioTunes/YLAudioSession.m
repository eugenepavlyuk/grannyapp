//
//  YLAudioSession.m
//  RadioTunes
//
//  Copyright (c) 2013 Yakamoz Labs. All rights reserved.
//

#import "YLAudioSession.h"
#import "GCDMulticastDelegate.h"
#import <AVFoundation/AVFoundation.h>

void AudioRouteChangeListenerCallback(void *inUserData,
                                      AudioSessionPropertyID inPropertyID,
                                      UInt32 inPropertyValueSize,
                                      const void *inPropertyValue) {
    if(inPropertyID != kAudioSessionProperty_AudioRouteChange) {
        return;
    }
    
    YLAudioSession *audioSession = (YLAudioSession *)inUserData;
    CFDictionaryRef routeChangeDictionary = inPropertyValue;
    CFStringRef oldRouteRef = CFDictionaryGetValue(routeChangeDictionary, CFSTR(kAudioSession_AudioRouteChangeKey_OldRoute));
    CFNumberRef routeChangeReasonRef = CFDictionaryGetValue(routeChangeDictionary, CFSTR(kAudioSession_AudioRouteChangeKey_Reason));
    SInt32 routeChangeReason;
    CFNumberGetValue(routeChangeReasonRef, kCFNumberSInt32Type, &routeChangeReason);
    if(routeChangeReason == kAudioSessionRouteChangeReason_OldDeviceUnavailable &&
       [(NSString *)oldRouteRef compare:@"Headphone"] == NSOrderedSame) {
        [audioSession reportHeadphoneUnplugged];
    }
}

@interface YLAudioSession()<AVAudioSessionDelegate> {
    GCDMulticastDelegate *_multicastDelegate;
    BOOL _audioSessionInitialized;
    
    dispatch_queue_t _bandwidthQueue;
    UInt64 _bandwidthWWAN;
    UInt64 _bandwidthWifi;
}

@end

@implementation YLAudioSession

+ (YLAudioSession *)sharedInstance {
    static YLAudioSession *sharedInstance = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        if(sharedInstance == nil) {
            sharedInstance = [[YLAudioSession alloc] init];
        }
    });
    
    return sharedInstance;
}

- (id)init {
    self = [super init];
    if(self) {
        _multicastDelegate = (GCDMulticastDelegate<YLAudioSessionDelegate> *)[[GCDMulticastDelegate alloc] init];
        
        _bandwidthQueue = dispatch_queue_create("com.yakamozlabs.bandwidthqueue", DISPATCH_QUEUE_CONCURRENT);
    }
    
    return self;
}

- (void)dealloc {
    [_multicastDelegate release];
    dispatch_release(_bandwidthQueue);
    
    [super dealloc];
}


#pragma mark -
#pragma mark Instance Methods
- (void)startAudioSession {
    if(_audioSessionInitialized) {
        return;
    }
    
    AVAudioSession *audioSession = [AVAudioSession sharedInstance];
    audioSession.delegate = self;
    
    NSError *error;
    if(![audioSession setCategory:AVAudioSessionCategoryPlayback error:&error]) {
        DLog(@"Error: Audio Session category could not be set: %@", error.localizedDescription);
        return;
    }
    
    if(![audioSession setActive:YES error:&error]) {
        DLog(@"Error: Audio Session could not be activated: %@", error.localizedDescription);
        return;
    }
    
    AudioSessionAddPropertyListener(kAudioSessionProperty_AudioRouteChange, AudioRouteChangeListenerCallback, self);
    _audioSessionInitialized = YES;
}

- (void)addDelegate:(id)delegate {
    [_multicastDelegate addDelegate:delegate delegateQueue:dispatch_get_main_queue()];
}

- (void)removeDelegate:(id)delegate {
    [_multicastDelegate removeDelegate:delegate];
}

- (UInt64)bandwidthUsageForConnectionType:(YLNetworkConnectionType)type {
    __block UInt64 bandwidth = 0;
    dispatch_sync(_bandwidthQueue, ^{
        if(type == kNetworkConnectionTypeAll) {
            bandwidth = _bandwidthWifi + _bandwidthWWAN;
        } else if(type == kNetworkConnectionTypeWiFi) {
            bandwidth = _bandwidthWifi;
        } else {
            bandwidth = _bandwidthWWAN;
        }
    });
    
    return bandwidth;
}

- (void)resetBandwidth {
    [self resetBandwidthForConnectionType:kNetworkConnectionTypeAll];
}

- (void)resetBandwidthForConnectionType:(YLNetworkConnectionType)type {
    dispatch_barrier_async(_bandwidthQueue, ^{
        if(type == kNetworkConnectionTypeAll) {
            _bandwidthWifi = 0;
            _bandwidthWWAN = 0;
        } else if(type == kNetworkConnectionTypeWiFi) {
            _bandwidthWifi = 0;
        } else if(type == kNetworkConnectionTypeWWAN) {
            _bandwidthWWAN = 0;
        }
    });
}

- (void)reportHeadphoneUnplugged {
    [(GCDMulticastDelegate<YLAudioSessionDelegate> *)_multicastDelegate headphoneUnplugged];
}

- (void)reportBytes:(NSUInteger)length forConnectionType:(YLNetworkConnectionType)type {
    dispatch_barrier_async(_bandwidthQueue, ^{
        if(type == kNetworkConnectionTypeWiFi) {
            _bandwidthWifi += length;
        } else if(type == kNetworkConnectionTypeWWAN) {
            _bandwidthWWAN += length;
        }
    });
}

#pragma mark -
#pragma mark AVAudioSessionDelegate Methods
- (void)beginInterruption {
    [(GCDMulticastDelegate<YLAudioSessionDelegate> *)_multicastDelegate beginInterruption];
}

- (void)endInterruptionWithFlags:(NSUInteger)flags {
    // re-activate audio session after interruption
    NSError *error;
    if(![[AVAudioSession sharedInstance] setActive:YES error:&error]) {
        DLog(@"Error: Audio Session could not be activated: %@", error);
    }
    
    [(GCDMulticastDelegate<YLAudioSessionDelegate> *)_multicastDelegate endInterruptionWithFlags:flags];
}

- (void)inputIsAvailableChanged:(BOOL)isInputAvailable {
    [(GCDMulticastDelegate<YLAudioSessionDelegate> *)_multicastDelegate inputIsAvailableChanged:isInputAvailable];
}

@end
