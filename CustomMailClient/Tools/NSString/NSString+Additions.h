#import <Foundation/Foundation.h>

@interface NSString (Additions)

+ (NSString*)ordinalNumberFormat:(NSNumber *)numObj;
- (NSString*)phoneNumberWithSpaces;
- (NSComparisonResult)numbersLastComparing:(NSString*)fullname;

@end