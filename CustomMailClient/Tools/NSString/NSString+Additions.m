#import "NSString+Additions.h"

@implementation NSString (Additions)

+ (NSString*)ordinalNumberFormat:(NSNumber *)numObj {
    NSString *ending;
    NSInteger num = [numObj integerValue];
    
    int ones = num % 10;
    int tens = floor(num / 10);
    tens = tens % 10;
    
    if(tens == 1){
        ending = @"th";
    } else {
        switch (ones) {
            case 1:
                ending = @"st";
                break;
            case 2:
                ending = @"nd";
                break;
            case 3:
                ending = @"rd";
                break;
            default:
                ending = @"th";
                break;
        }
    }
    
    return [NSString stringWithFormat:@"%d%@", num, ending];
}

- (NSComparisonResult)numbersLastComparing:(NSString*)fullname
{
    if ([self length] && [fullname length])
    {
        char ch1 = [self characterAtIndex:0];
        
        char ch2 = [fullname characterAtIndex:0];
        
        NSMutableCharacterSet *set = [NSMutableCharacterSet characterSetWithCharactersInString:@"0123456789!@#$%^&*()-=+_§±<>./?\\ "];
        
        if ([set characterIsMember:ch1] && [set characterIsMember:ch2])
        {
            return NSOrderedSame;
        }
        else if ([set characterIsMember:ch1] && ![set characterIsMember:ch2])
        {
            return NSOrderedDescending;
        }
        else if (![set characterIsMember:ch1] && [set characterIsMember:ch2])
        {
            return NSOrderedAscending;
        }
        else
        {
            return [self localizedStandardCompare:fullname];
        }
    }
    else
    {
        if (![self length] && ![fullname length])
        {
            return NSOrderedSame;
        }
        else if (![self length] && [fullname length])
        {
            return NSOrderedDescending;
        }
        else if ([self length] && ![fullname length])
        {
            return NSOrderedAscending;
        }
        else
        {
            return [self localizedStandardCompare:fullname];
        }
    }
}

- (NSString*)phoneNumberWithSpaces
{
    return self;
    
    NSString *theString = [self stringByReplacingOccurrencesOfString:@"#" withString:@""];
//    theString = [theString stringByReplacingOccurrencesOfString:@"-" withString:@""];
//    theString = [theString stringByReplacingOccurrencesOfString:@")" withString:@""];
//    theString = [theString stringByReplacingOccurrencesOfString:@"(" withString:@""];
//    theString = [theString stringByReplacingOccurrencesOfString:@"+" withString:@""];
//    theString = [theString stringByReplacingOccurrencesOfString:@" " withString:@""];
    
    NSMutableString *stringWithSpaces = [NSMutableString string];
    
    int j = 0;
    
    for (int i = 0; i < [theString length]; i++)
    {
        NSRange range = NSMakeRange(i, 1);
        
        NSString *character = [theString substringWithRange:range];
        
        [stringWithSpaces appendString:character];
        
        j++;
        
        if (j == 2)
        {
            j = 0;
            
            [stringWithSpaces appendString:@" "];
        }
    }
    
    return stringWithSpaces;
}

@end