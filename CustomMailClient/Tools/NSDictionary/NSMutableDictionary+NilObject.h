//
//  NSMutableDictionary+NilObject.h
//  GrannyApp
//
//  Created by Eugene Pavluk on 3/26/13.
//
//

#import <Foundation/Foundation.h>

@interface NSMutableDictionary (NilObject)

- (void)setObjectIfNotNil:(NSObject*)object forKey:(NSString*)key;

@end
