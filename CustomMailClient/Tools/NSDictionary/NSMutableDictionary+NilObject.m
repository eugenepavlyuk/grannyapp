//
//  NSMutableDictionary+NilObject.m
//  GrannyApp
//
//  Created by Eugene Pavluk on 3/26/13.
//
//

#import "NSMutableDictionary+NilObject.h"

@implementation NSMutableDictionary (NilObject)

- (void)setObjectIfNotNil:(NSObject*)object forKey:(NSString*)key
{
    if (object)
    {
        [self setObject:object forKey:key];
    }
}

@end
