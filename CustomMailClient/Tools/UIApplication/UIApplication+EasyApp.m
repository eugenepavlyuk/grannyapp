//
//  UIApplication+EasyApp.m
//  GrannyApp
//
//  Created by Eugene Pavluk on 10/28/13.
//
//

#import "UIApplication+EasyApp.h"
#import "Event.h"

@implementation UIApplication (EasyApp)

- (void)cancelLocalNotificationsForEventId:(NSNumber*)eventId
{
    NSArray *allNotifications = [self scheduledLocalNotifications];
    
    NSMutableArray *concreteNotifications = [NSMutableArray array];
    
    for (UILocalNotification *localNotification in allNotifications)
    {
        if ([[localNotification.userInfo objectForKey:kEventIdKey] isEqualToNumber:eventId])
        {
            [concreteNotifications addObject:localNotification];
        }
    }
    
    for (UILocalNotification *localNotification in concreteNotifications)
    {
        [self cancelLocalNotification:localNotification];
    }
}

@end
