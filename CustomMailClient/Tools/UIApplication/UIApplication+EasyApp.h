//
//  UIApplication+EasyApp.h
//  GrannyApp
//
//  Created by Eugene Pavluk on 10/28/13.
//
//

#import <UIKit/UIKit.h>

@interface UIApplication (EasyApp)

- (void)cancelLocalNotificationsForEventId:(NSNumber*)eventId;

@end
