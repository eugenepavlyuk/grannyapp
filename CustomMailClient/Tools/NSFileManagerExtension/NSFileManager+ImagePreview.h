//
//  NSFileManager+ImagePreview.h
//  GrannyApp
//
//  Created by Eugene Pavluk on 3/27/13.
//
//

#import <Foundation/Foundation.h>

@interface NSFileManager (ImagePreview)

- (void)makePreviewImageForData:(NSData *)imageData path:(NSString *)path;

@end
