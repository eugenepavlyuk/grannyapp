//
//  NSFileManager+ImagePreview.m
//  GrannyApp
//
//  Created by Eugene Pavluk on 3/27/13.
//
//

#import "NSFileManager+ImagePreview.h"

@implementation NSFileManager (ImagePreview)

- (void)makePreviewImageForData:(NSData *)imageData path:(NSString *)path
{
    UIImage *image = [UIImage imageWithData:imageData];
    
    if (image)
    {
        CGFloat fixedPrevSize = 98.0f;
        
        if (image.size.width > image.size.height)
        {
            CGFloat lyamda = fixedPrevSize / image.size.height * 100.0f;
            CGFloat calculatedWidth = image.size.width / 100.0f * lyamda;
            CGSize pSize = CGSizeMake(calculatedWidth, fixedPrevSize);
            UIGraphicsBeginImageContext(pSize);
            [image drawInRect:CGRectMake(0.0, 0.0,pSize.width, pSize.height)];
            UIImage *newImage = UIGraphicsGetImageFromCurrentImageContext();
            UIGraphicsEndImageContext();
            
            CGFloat cutX = calculatedWidth/2 - fixedPrevSize/2;
            CGFloat cutY = 0.0f;
            
            CGRect cropRect = CGRectMake(cutX, cutY, fixedPrevSize, fixedPrevSize);
            UIGraphicsBeginImageContext(cropRect.size);
            CGContextRef context = UIGraphicsGetCurrentContext();
            
            // translated rectangle for drawing sub image
            CGRect drawRect = CGRectMake(-cropRect.origin.x, -cropRect.origin.y, newImage.size.width, newImage.size.height);
            
            // clip to the bounds of the image context
            // not strictly necessary as it will get clipped anyway?
            CGContextClipToRect(context, CGRectMake(0, 0, cropRect.size.width, cropRect.size.height));
            
            // draw image
            [newImage drawInRect:drawRect];
            
            // grab image
            UIImage* croppedImage = UIGraphicsGetImageFromCurrentImageContext();
            
            UIGraphicsEndImageContext();
            NSData *prevImageData = UIImagePNGRepresentation(croppedImage);
            [prevImageData writeToFile:path atomically:YES];
        }
        else
        {
            CGFloat lyamda = fixedPrevSize / image.size.width * 100.0f;
            CGFloat calculatedHeight = image.size.height / 100.0f * lyamda;
            CGSize pSize = CGSizeMake(fixedPrevSize, calculatedHeight);
            UIGraphicsBeginImageContext(pSize);
            [image drawInRect:CGRectMake(0.0, 0.0,pSize.width, pSize.height)];
            UIImage *newImage = UIGraphicsGetImageFromCurrentImageContext();
            UIGraphicsEndImageContext();
            
            CGFloat cutX = 0.0f;
            CGFloat cutY = calculatedHeight/2 - fixedPrevSize/2;
            
            CGRect cropRect = CGRectMake(cutX, cutY, fixedPrevSize, fixedPrevSize);
            UIGraphicsBeginImageContext(cropRect.size);
            CGContextRef context = UIGraphicsGetCurrentContext();
            
            // translated rectangle for drawing sub image
            CGRect drawRect = CGRectMake(-cropRect.origin.x, -cropRect.origin.y, newImage.size.width, newImage.size.height);
            
            // clip to the bounds of the image context
            // not strictly necessary as it will get clipped anyway?
            CGContextClipToRect(context, CGRectMake(0, 0, cropRect.size.width, cropRect.size.height));
            
            // draw image
            [newImage drawInRect:drawRect];
            
            // grab image
            UIImage* croppedImage = UIGraphicsGetImageFromCurrentImageContext();
            
            UIGraphicsEndImageContext();
            NSData *prevImageData = UIImagePNGRepresentation(croppedImage);
            [prevImageData writeToFile:path atomically:YES];
        }
    }
}

@end
