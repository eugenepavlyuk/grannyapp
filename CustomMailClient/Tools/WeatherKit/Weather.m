//
//  Weather.m
//  BatApp
//
//  Created by Swechha Prakash on 28/03/14.
//  Copyright (c) 2014 Swechha. All rights reserved.
//

#import "Weather.h"
#import "WeatherKit.h"
#import "OWMWeatherAPI.h"
#import <math.h>

@interface Weather()

@property OWMWeatherAPI *weatherAPI;

@end

@implementation Weather

- (instancetype)init
{
    if (self = [super init]) {
        //init
    }
    return self;
}

- (instancetype)initWithDictionary:(NSDictionary*)response
{
    if (self = [super init]) {
        _isCurrentLocation = NO;
        _latitude = [response[@"coord"][@"lat"] floatValue];
        _longitude = [response[@"coord"][@"long"] floatValue];
        _temperature = roundf([response[@"main"][@"temp"] floatValue]);
        _cityName = response[@"name"];
        _weatherDescription = response[@"weather"][0][@"description"];
        _iconID = response[@"weather"][0][@"icon"];
        _weatherId = response[@"weather"][0][@"id"];
        _humidity = [response[@"main"][@"humidity"] integerValue];
        _temperatureMax = [response[@"main"][@"temp_max"] integerValue];
        _temperatureMin = [response[@"main"][@"temp_min"] integerValue];
        
        NSInteger wId = [_weatherId integerValue];
        
        if (wId == 906)
        {
            self.iconName = @"hail";
        }
        else if (wId == 905 ||
                 wId == 957)
        {
            self.iconName = @"wind";
        }
        else if ([_iconID isEqualToString:@"01d"] ||
            [_iconID isEqualToString:@"01n"])
        {
            self.iconName = @"clear";
        }
        else if ([_iconID isEqualToString:@"02d"] ||
                 [_iconID isEqualToString:@"02n"])
        {
            self.iconName = @"haze";
        }
        else if ([_iconID isEqualToString:@"03d"] ||
            [_iconID isEqualToString:@"03n"])
        {
            self.iconName = @"partly_clouded";
        }
        else if ([_iconID isEqualToString:@"04d"] ||
                 [_iconID isEqualToString:@"04n"])
        {
            if (wId == 804)
            {
                self.iconName = @"overcast";
            }
            else
            {
                self.iconName = @"mostly_clouded";
            }
        }
        else if ([_iconID isEqualToString:@"09d"] ||
                 [_iconID isEqualToString:@"09n"] ||
                 [_iconID isEqualToString:@"10d"] ||
                 [_iconID isEqualToString:@"10n"])
        {
            self.iconName = @"rain";
        }
        else if ([_iconID isEqualToString:@"11d"] ||
                 [_iconID isEqualToString:@"11n"])
        {
            self.iconName = @"thunderstorm";
        }
        else if ([_iconID isEqualToString:@"13d"] ||
                 [_iconID isEqualToString:@"13n"])
        {
            self.iconName = @"snow";
        }
        else if ([_iconID isEqualToString:@"50d"] ||
                 [_iconID isEqualToString:@"50n"])
        {
            self.iconName = @"fog";
        }
        else
        {
            self.iconName = @"haze";
        }
    }
    
    return self;
}

@end
