//
//  AlarmManager.m
//  Granny
//
//  Created by Eugene Pavlyuk on 3/28/12.
//  Copyright (c) 2012 Home. All rights reserved.
//

#import "AlarmManager.h"
#import "AlarmManagerDelegate.h"
#import "Event.h"
#import "MedicineAlarm.h"
#import "DataManager.h"
#import "Settings.h"
#import "NSDate+Calendar.h"

static AlarmManager *sharedInstance = nil;

@interface AlarmManager()

@property (nonatomic, retain) AVAudioPlayer *alarmPlayer;

- (void)enableTimer;
- (void)disableTimer;

@end


@implementation AlarmManager
{
    EKEventStore *eventStore;
}

@synthesize alarmPlayer;
@synthesize eventStore;

+ (AlarmManager*)sharedInstance
{
    if(sharedInstance)
    {
        return sharedInstance;
    }
    
    @synchronized(self)
    {
        if (!sharedInstance)
        {
            sharedInstance = [[self alloc] init];
        }
    }
    
	return sharedInstance;
}

+ (id)allocWithZone:(NSZone *)zone
{
    if(sharedInstance)
    {
        return sharedInstance;
    }
    
    @synchronized(self)
    {
        if(!sharedInstance)
        {
            sharedInstance = [super allocWithZone:zone];
        }        
    }
    
    return sharedInstance;
}

- (id)init
{
    self = [super init];
    
    if(self)
    {
        [self enableTimer];
        
        [[NSNotificationCenter defaultCenter] addObserver:self 
                                                 selector:@selector(disableTimer) 
                                                     name:UIApplicationWillResignActiveNotification 
                                                   object:nil];
        
        [[NSNotificationCenter defaultCenter] addObserver:self 
                                                 selector:@selector(enableTimer) 
                                                     name:UIApplicationDidBecomeActiveNotification 
                                                   object:nil];
        
        watchersArray = [NSMutableArray new];
    }
    
    return self;
}

- (void)accessToCalendarStore
{
    eventStore = [[EKEventStore alloc] init];
    
    [eventStore requestAccessToEntityType:EKEntityTypeEvent completion:^(BOOL granted, NSError *error) {
        if (granted) {
            

        } else {
            
            eventStore = nil;
//            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error"
//                                                            message:@"Access to calendar forbidden"
//                                                           delegate:nil
//                                                  cancelButtonTitle:@"OK"
//                                                  otherButtonTitles:nil];
//            [alert show];
        }
    }];
}

- (void)updateAllEvents
{
    [[UIApplication sharedApplication] cancelAllLocalNotifications];
    
    NSMutableArray *allEvents = [NSMutableArray arrayWithArray:[[DataManager sharedInstance] allRowsForEntity:kEventBaseEntityName]];
    
    for (EventBase *event in allEvents)
    {
        if ([event.expired boolValue])
        {
            continue;
        }
        
        [event rescheduleEvent];
    }
}

- (id)copyWithZone:(NSZone *)zone
{
    return self;
}

- (id)retain
{
    return self;
}

- (NSUInteger)retainCount
{
    return NSUIntegerMax;
}

- (oneway void)release
{
    
}

- (id)autorelease
{
    return self;
}

- (void)dealloc 
{
    [self.alarmPlayer stop];
    self.alarmPlayer = nil;
    [watchersArray release];
    [self disableTimer];
	[super dealloc];
}

- (void)enableTimer
{
    [self disableTimer];
    
    alarmTimer = [[NSTimer scheduledTimerWithTimeInterval:1.0 target:self selector:@selector(checkAlarm) userInfo:nil repeats:YES] retain];
}

- (void)disableTimer
{
    if (alarmTimer)
    {
        if ([alarmTimer isValid])
        {
            [alarmTimer invalidate];
        }
        
        [alarmTimer release];
        alarmTimer = nil;
    }
}

- (void)initPlayerWithSoundType:(NSString*)eventSound
{
	if ([alarmPlayer isPlaying]) 
    {
        [alarmPlayer stop];
    }   		
    
    NSString *extensionName = @"mp3";
    
    NSString* resourcePath = [[NSBundle mainBundle] pathForResource:eventSound ofType:extensionName];
    self.alarmPlayer = [[AVAudioPlayer alloc] initWithContentsOfURL:[NSURL fileURLWithPath:resourcePath] error:nil];
    alarmPlayer.numberOfLoops = 10;
    [alarmPlayer prepareToPlay];
}

- (void)checkAlarm
{
    for (id<AlarmManagerDelegate> watcher in watchersArray)
    {
        [watcher timeChangedInAlarmManager:self];
    }
    
    NSMutableArray *allEvents = [NSMutableArray arrayWithArray:[[DataManager sharedInstance] allRowsForEntity:kEventBaseEntityName]];

    BOOL needsAlarm = NO;
    NSString *eventSoundName = nil;
    
    for (EventBase *event in allEvents)
    {
        if ([event.expired boolValue])
        {
            continue;
        }
        
        BOOL showAlert = NO;
        
        if ([event isMatch])
        {
            needsAlarm = showAlert = YES;
            
            eventSoundName = [event sound];
        }
        
        if (showAlert)
        {
            AlarmAlertView *alertView = nil;
            
            if ([event isKindOfClass:[MedicineAlarm class]])
            {
                alertView = [[[NSBundle mainBundle] loadNibNamed:@"MedicineAlarmAlertView~ipad" owner:nil options:nil] lastObject];
                
                alertView.titleLabel.text = event.name;
            }
            else
            {
                NSString *title = event.name;
                
                if ([event isEvent])
                {
                    if ([event hasImage])
                    {
                        alertView = [[[NSBundle mainBundle] loadNibNamed:@"EventAlertView~ipad" owner:nil options:nil] lastObject];
                    }
                    else
                    {
                        alertView = [[[NSBundle mainBundle] loadNibNamed:@"EventAlertViewWithoutPhoto~ipad" owner:nil options:nil] lastObject];
                    }
                }
                else
                {
                    if ([event hasImage])
                    {
                        alertView = [[[NSBundle mainBundle] loadNibNamed:@"AlarmAlertView~ipad" owner:nil options:nil] lastObject];
                    }
                    else
                    {
                        alertView = [[[NSBundle mainBundle] loadNibNamed:@"AlarmAlertViewWithoutPhoto~ipad" owner:nil options:nil] lastObject];
                    }
                }

                alertView.titleLabel.text = title;
            }

            alertView.delegate = self;
            
            alertView.event = event;
            
            if (![event isEvent])
            {
                if (![event.hasSnoozeTime boolValue])
                {
                    alertView.cancelButton.hidden = YES;
                    alertView.okButton.center = CGPointMake(alertView.bounds.size.width / 2, alertView.okButton.center.y);
                }
            }
            else
            {
                alertView.cancelButton.hidden = YES;
                alertView.okButton.center = CGPointMake(alertView.bounds.size.width / 2, alertView.okButton.center.y);
            }
            
            [alertView show];
        }
    }
    
    if (needsAlarm)
    {
        [self initPlayerWithSoundType:eventSoundName];
        
        [self.alarmPlayer play];
    }
}

- (void)addWatcher:(id<AlarmManagerDelegate>)watcher
{
    if (watcher && ![watchersArray containsObject:watcher])
    {
        [watchersArray addObject:watcher];
    }
}

- (void)removeWatcher:(id<AlarmManagerDelegate>)watcher
{
    [watchersArray removeObject:watcher];
}

- (void)alertView:(AlertView *)alertView didDismissWithButtonIndex:(NSInteger)buttonIndex
{
    if ([alertView isKindOfClass:[AlarmAlertView class]])
    {
        AlarmAlertView *alarmAlertView = (AlarmAlertView*)alertView;
        
        EventBase *event = alarmAlertView.event;
        
        if (buttonIndex == 0)
        {
            Settings *settings = [[DataManager sharedInstance] getEntityWithName:kSettingsEntityName];
            
            NSTimeInterval snoozeTime;
            
            if (![event isEvent])
            {
                if (event.snoozeDate)
                {
                    snoozeTime = [event.snoozeDate timeIntervalSince1970] + [settings.snoozeDuration integerValue] * 60;
                }
                else
                {
                    snoozeTime = [[event notifyDate] timeIntervalSince1970] + [settings.snoozeDuration integerValue] * 60;//5 * 60;
                }
                
                event.snoozeDate = [NSDate dateWithTimeIntervalSince1970:snoozeTime];
                
                [event scheduleLocalNotifications];
            }
            else
            {
                event.snoozeDate = nil;
                
                if ([event.type intValue] == ET_Once)
                {
                    event.expired = [NSNumber numberWithBool:YES];
                }

                [event rescheduleEvent];
            }
        }
        else
        {
            if ([event.type intValue] == ET_Once)
            {
                if ([[event notifyDate] isLessDate:[NSDate date]]) // passed
                {
                    event.expired = @(YES);
                }
            }
            else
            {
                event.snoozeDate = nil;
            }
        }
        
        [self.alarmPlayer stop];
        [[DataManager sharedInstance] save];
    }
}

@end
