//
//  AlarmManagerDelegate.h
//  GrannyApp
//
//  Created by Eugene Pavluk on 11/6/13.
//
//

#import <Foundation/Foundation.h>

@class AlarmManager;

@protocol AlarmManagerDelegate <NSObject>

- (void)timeChangedInAlarmManager:(AlarmManager*)alarmManager;

@end
