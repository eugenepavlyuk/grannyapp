//
//  AlarmManager.h
//  Granny
//
//  Created by Eugene Pavlyuk on 3/28/12.
//  Copyright (c) 2012 Home. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <AVFoundation/AVAudioPlayer.h>
#import <AVFoundation/AVFoundation.h>
#import <EventKit/EventKit.h>
#import "AlertView.h"

@protocol AlarmManagerDelegate;

@interface AlarmManager : NSObject <AlertViewDelegate>
{
    NSTimer *alarmTimer;
    
    AVAudioPlayer *alarmPlayer;
    
    NSMutableArray *watchersArray;
}

@property (nonatomic, strong) EKEventStore *eventStore;

+ (AlarmManager*)sharedInstance;

- (void)accessToCalendarStore;

- (void)updateAllEvents;

- (void)addWatcher:(id<AlarmManagerDelegate>)watcher;
- (void)removeWatcher:(id<AlarmManagerDelegate>)watcher;

@end
