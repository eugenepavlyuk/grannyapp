//
//  GRPeopleManager.h
//  GrannyApp
//
//  Created by Ievgen Pavliuk on 6/21/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <AddressBook/AddressBook.h>
#import "GRContact.h"

typedef void (^completeBlock)();
typedef void (^startBlock)();
typedef void (^updateProgressBlock)(float progress);
typedef void (^successfulCreationBlock)(NSInteger recId);

@class GRContact;

@interface GRPeopleManager : NSObject
{
    ABAddressBookRef addressBook;
    NSMutableArray *contacts;
    NSMutableArray *contactsWithMail;
    
    dispatch_queue_t peopleManagerQueue;
    BOOL isBusy;
}

@property (nonatomic, strong) NSMutableArray *contacts;
@property (nonatomic, strong) NSMutableArray *contactsWithMail;
@property (nonatomic, strong) NSMutableArray *otherContacts;
@property (nonatomic, strong) NSMutableArray *favoriteContacts;
@property (nonatomic, assign) BOOL isBusy;

+ (GRPeopleManager *)sharedManager;
- (GRContact *)findContactByEmail:(NSString *)email;

// new api.
- (void)syncContactsFromAddressBook:(completeBlock)block
                  withProgressBlock:(updateProgressBlock)updateBlock
                     withStartBlock:(startBlock)startBlock;

- (void)populateWithFakeContactsCompletionBlock:(completeBlock)block
                                    updateBlock:(updateProgressBlock)updateBlock
                                     startBlock:(startBlock)startBlock;

- (void)removeAllFakeContacts;
- (void)removeAllContacts;

- (void)updateContactsWithCompletionBlock:(completeBlock)block;

- (void)removeContactFromAddressBook:(GRContact*)contact withCompletionBlock:(completeBlock)complitionBlock;

- (void)createContactWithInfo:(NSDictionary*)info
          withCompletionBlock:(successfulCreationBlock)completeBlock
              withFailedBlock:(completeBlock)failedBlock;

- (void)updateContactSelectedMailForRecId:(NSNumber *)recId selectedMail:(NSString *)selectedMail;

- (void)registerForExternalChanges;
- (void)unregisterForExternalChanges;

@end
