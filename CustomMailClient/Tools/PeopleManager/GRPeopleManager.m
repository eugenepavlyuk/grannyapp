//
//  GRPeopleManager.m
//  GrannyApp
//
//  Created by Ievgen Pavliuk on 6/21/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "GRPeopleManager.h"
#import "Favorite.h"
#import "DataManager.h"

#define NUMBER_OF_FAKE_CONTACTS     3053

// From documentation: Important: You must ensure that an instance of ABAddressBookRef is used by only one thread.

// ABAddressBookCreate is deprecated in iOS 6.0. Use the ABAddressBookCreateWithOptions function instead.)

void externalChangeCallback(ABAddressBookRef addressBook, CFDictionaryRef info, void *context)
{
    [[GRPeopleManager sharedManager] syncContactsFromAddressBook:^{
        [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:NO];
        
        [[NSNotificationCenter defaultCenter] postNotificationName:kSyncingContactsFinishedNotification object:nil];
        
    } withProgressBlock:^(float progress) {
        
    } withStartBlock:^{
        [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:YES];
    }];
    
    [[NSNotificationCenter defaultCenter] postNotificationName:kResyncingContactsNotification object:nil];
}

@implementation GRPeopleManager

@synthesize contacts, contactsWithMail, isBusy;

static GRPeopleManager *instance = nil;

#pragma mark - Initialization

+ (GRPeopleManager *)sharedManager
{
    @synchronized(self)
    {
        if (!instance)
        {
            instance = [[GRPeopleManager alloc] init];
        }
        return instance;
    }
}

- (id)init
{
    if ((self  = [super init]))
    {        
        peopleManagerQueue = dispatch_queue_create("com.easyapp.contactsqueue", DISPATCH_QUEUE_SERIAL);
        
        self.otherContacts = [@[] mutableCopy];
        self.favoriteContacts = [@[] mutableCopy];
    }
    
    return self;
}

- (void)dealloc
{
    self.contactsWithMail = nil;
    self.contacts = nil;
    self.otherContacts = nil;
    self.favoriteContacts = nil;
    
    if (addressBook)
    {
        [self unregisterForExternalChanges];
        CFRelease(addressBook);
    }
    
    addressBook = nil;
}


#pragma mark - People Management

- (GRContact *)findContactByEmail:(NSString *)email {
    GRContact *contact = nil;
    if (!isBusy) {
        
        NSPredicate *predicate = [NSPredicate predicateWithFormat:@"email == %@", email];
        
        NSArray *filteredArray = [contactsWithMail filteredArrayUsingPredicate:predicate];
        
        return [filteredArray lastObject];
        
//        for (GRContact *fcontact in contactsWithMail) {
//            if ([fcontact.email isEqualToString:email]) {
//                contact = fcontact;
//                break;
//            }
//        }
    }
    
    return contact;
}

// new api

- (void)accessAddressBookDatabase:(ABAddressBookRef)theAddressBook withSuccessfulBlock:(completeBlock)completionBlock andWithFailedBlock:(completeBlock)failedBlock
{
    if (ABAddressBookGetAuthorizationStatus() == kABAuthorizationStatusNotDetermined)
    {
        ABAddressBookRequestAccessWithCompletion(theAddressBook, ^(bool granted, CFErrorRef error)
                                                 {
                                                     //The completion handler is called on an arbitrary queue. If your app uses an address book throughout the app, you are responsible for ensuring that all usage of that address book is dispatched to a single queue to ensure correct thread-safe operation.
                                                     
                                                     dispatch_async(peopleManagerQueue, ^{
                                                         if (granted)
                                                         {
                                                             completionBlock();
                                                         }
                                                         else
                                                         {
                                                             failedBlock();
                                                         }
                                                     });
                                                 });
    }
    else if (ABAddressBookGetAuthorizationStatus() == kABAuthorizationStatusAuthorized)
    {
        completionBlock();
    }
    else
    {
        failedBlock();
    }
}

- (void)registerForExternalChanges
{
    if (!addressBook)
    {
        addressBook = ABAddressBookCreateWithOptions(NULL, NULL);
        
        [self accessAddressBookDatabase:addressBook withSuccessfulBlock:^{
            dispatch_async(dispatch_get_main_queue(), ^{
                ABAddressBookRegisterExternalChangeCallback(addressBook, &externalChangeCallback, NULL);
            });
        }
                     andWithFailedBlock:^{
                         dispatch_async(dispatch_get_main_queue(), ^{
                             
                         });
                     }];
    }
    else
    {
        ABAddressBookRegisterExternalChangeCallback(addressBook, &externalChangeCallback, NULL);
    }
}

- (void)unregisterForExternalChanges
{
    if (addressBook)
    {
        ABAddressBookUnregisterExternalChangeCallback(addressBook, &externalChangeCallback, NULL);
    }
}

- (void)syncContactsFromAddressBook:(completeBlock)block
                  withProgressBlock:(updateProgressBlock)updateBlock
                     withStartBlock:(startBlock)startBlock
{
    self.isBusy = YES;
    self.contacts = [NSMutableArray array];
    self.contactsWithMail = [NSMutableArray array];
    
    [self.otherContacts removeAllObjects];
    [self.favoriteContacts removeAllObjects];

    dispatch_async(peopleManagerQueue, ^{
        
        dispatch_async(dispatch_get_main_queue(), ^{
            startBlock();
        });
        
        ABAddressBookRef addressBookRef = ABAddressBookCreateWithOptions(NULL, NULL);
        
        [self accessAddressBookDatabase:addressBookRef withSuccessfulBlock:^{
            
            NSArray *people = (__bridge NSArray *)ABAddressBookCopyArrayOfAllPeople(addressBookRef);
            
            NSInteger count = [people count];
            
            NSLog(@"Starting loading contacts");
            for (int i = 0; i < count; i++)
            {
                ABRecordRef record = (__bridge ABRecordRef)(people[i]);
                
                GRContact *newContact = [GRContact contactWithRecord:record];
                [self supplementContactsWithMailWithContact:newContact];
                [contacts addObject:newContact];

                //NSLog(@"Contact added %@", newContact.firstname);
                
//                dispatch_async(dispatch_get_main_queue(), ^{
//                    updateBlock(((float)i)/count);
//                });
            }
            NSLog(@"Finishing loading contacts");
            self.isBusy = NO;

            CFRelease(addressBookRef);
            
//            dispatch_async(dispatch_get_main_queue(), ^{
//                // check if contact is favorite
//                Favorite *favoriteTable = [[DataManager sharedInstance] getEntityWithName:@"Favorite"];
//                
//                if (!favoriteTable)
//                {
//                    favoriteTable = [[DataManager sharedInstance] createEntityWithName:@"Favorite"];
//                    
//                }
//                
//                NSArray *favorites = favoriteTable.contacts;
//                NSMutableDictionary *emails = [NSMutableDictionary dictionaryWithDictionary:favoriteTable.emails];
//                
//                if ([favorites count])
//                {
//                    for (NSString *favorite in favorites)
//                    {
//                        NSPredicate *predicate = [NSPredicate predicateWithFormat:@"recId == %@", @([favorite integerValue])];
//                        
//                        NSArray *filteredArray = [contacts filteredArrayUsingPredicate:predicate];
//                        
//                        for (GRContact *contact in filteredArray)
//                        {
//                            contact.favorite = YES;
//                        }
//                    }
//                }
//                
//                if ([emails count])
//                {
//                    NSArray *keys = [emails allKeys];
//                    
//                    for (NSString *key in keys)
//                    {
//                        NSPredicate *predicate = [NSPredicate predicateWithFormat:@"recId == %@", @([key integerValue])];
//                        
//                        NSArray *filteredArray = [contacts filteredArrayUsingPredicate:predicate];
//                        
//                        for (GRContact *contact in filteredArray)
//                        {
//                            contact.selectedEmail = [emails objectForKey:[contact.recId stringValue]];
//                        }
//                    }
//                }
//                
//                NSSortDescriptor *favDescriptor = [[NSSortDescriptor alloc] initWithKey:@"favorite" ascending:NO];
//                NSSortDescriptor *fullnameDescriptor = [[NSSortDescriptor alloc] initWithKey:@"fullContactName" ascending:YES selector:@selector(numbersLastComparing:)];
//                
//                NSArray *descriptors = [NSArray arrayWithObjects:favDescriptor, fullnameDescriptor, nil];
//                
//                [self.contacts sortUsingDescriptors:descriptors];
//                
//                NSPredicate *predicate = [NSPredicate predicateWithFormat:@"favorite == YES"];
//                
//                self.favoriteContacts = [[self.contacts filteredArrayUsingPredicate:predicate] mutableCopy];
//                
//                self.otherContacts = [self.contacts mutableCopy];
//                [self.otherContacts removeObjectsInArray:self.favoriteContacts];
//                
//                //            // show favorite contacts at the begining
//                //            [contacts sortUsingComparator:^NSComparisonResult(id obj1, id obj2)
//                //             {
//                //                 GRContact *contact1 = (GRContact*)obj1;
//                //                 GRContact *contact2 = (GRContact*)obj2;
//                //                 
//                //                 return (contact1.favorite < contact2.favorite);
//                //             }];
//            
//                block();
//            });
            
            
            // check if contact is favorite
            Favorite *favoriteTable = [[DataManager sharedInstance] getEntityWithName:@"Favorite"];
            
            NSArray *favorites = favoriteTable.contacts;
            NSMutableDictionary *emails = [NSMutableDictionary dictionaryWithDictionary:favoriteTable.emails];
            
            if ([favorites count])
            {
                for (NSString *favorite in favorites)
                {
                    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"recId == %@", @([favorite integerValue])];
                    
                    NSArray *filteredArray = [contacts filteredArrayUsingPredicate:predicate];
                    
                    for (GRContact *contact in filteredArray)
                    {
                        contact.favorite = YES;
                    }
                }
            }
            
            if ([emails count])
            {
                NSArray *keys = [emails allKeys];
                
                for (NSString *key in keys)
                {
                    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"recId == %@", @([key integerValue])];
                    
                    NSArray *filteredArray = [contacts filteredArrayUsingPredicate:predicate];
                    
                    for (GRContact *contact in filteredArray)
                    {
                        contact.selectedEmail = [emails objectForKey:[contact.recId stringValue]];
                    }
                }
            }
            
            NSSortDescriptor *favDescriptor = [[NSSortDescriptor alloc] initWithKey:@"favorite" ascending:NO];
            NSSortDescriptor *fullnameDescriptor = [[NSSortDescriptor alloc] initWithKey:@"fullContactName" ascending:YES selector:@selector(numbersLastComparing:)];
            
            NSArray *descriptors = [NSArray arrayWithObjects:favDescriptor, fullnameDescriptor, nil];
            
            [self.contacts sortUsingDescriptors:descriptors];
            
            NSPredicate *predicate = [NSPredicate predicateWithFormat:@"favorite == YES"];
            
            self.favoriteContacts = [[self.contacts filteredArrayUsingPredicate:predicate] mutableCopy];
            
            self.otherContacts = [self.contacts mutableCopy];
            [self.otherContacts removeObjectsInArray:self.favoriteContacts];

            dispatch_async(dispatch_get_main_queue(), ^{
                block();
            });
        }
         andWithFailedBlock:^{
             dispatch_async(dispatch_get_main_queue(), ^{
                 // check if contact is favorite
                 
                 [[[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Access denied to the AddressBook", @"access denied")
                                            message:nil
                                           delegate:nil
                                  cancelButtonTitle:@"OK"
                                  otherButtonTitles:nil] show];
                 
                 block();
             });
         }];
    });
}

-(BOOL) validEmail:(NSString*) emailString {
    NSString *regExPattern = @"^[A-Z0-9._%+-]+@[A-Z0-9.-]+\\.[A-Z]{2,4}$";
    NSRegularExpression *regEx = [[NSRegularExpression alloc] initWithPattern:regExPattern options:NSRegularExpressionCaseInsensitive error:nil];
    NSUInteger regExMatches = [regEx numberOfMatchesInString:emailString options:0 range:NSMakeRange(0, [emailString length])];
    if (regExMatches == 0) {
        return NO;
    } else
        return YES;
}

- (void)populateWithFakeContactsCompletionBlock:(completeBlock)block updateBlock:(updateProgressBlock)updateBlock startBlock:(startBlock)startBlock
{
    dispatch_async(peopleManagerQueue, ^{
        
        dispatch_async(dispatch_get_main_queue(), ^{
            startBlock();
        });
        
        ABAddressBookRef addressBookRef = ABAddressBookCreateWithOptions(NULL, NULL);
        
        [self accessAddressBookDatabase:addressBookRef withSuccessfulBlock:^{
            for (int i = 0; i < NUMBER_OF_FAKE_CONTACTS; i++)
            {
                CFErrorRef error;
                
                ABRecordRef person;
                
                person = ABPersonCreate();
                
                ABMutableMultiValueRef phoneNumberMultiValue = ABMultiValueCreateMutable(kABMultiStringPropertyType);
                ABMultiValueAddValueAndLabel(phoneNumberMultiValue , @"0957737798", kABPersonPhoneMobileLabel, NULL);
                ABRecordSetValue(person, kABPersonPhoneProperty, phoneNumberMultiValue, nil); // set the phone number property
                CFRelease(phoneNumberMultiValue);
                
                ABRecordSetValue(person, kABPersonFirstNameProperty, (__bridge CFTypeRef)([NSString stringWithFormat:@"Name %d", i]), nil);
                
                ABMutableMultiValueRef multiEmail = ABMultiValueCreateMutable(kABMultiStringPropertyType);
                ABMultiValueAddValueAndLabel(multiEmail, (__bridge CFTypeRef)([NSString stringWithFormat:@"email%d@gmail.com", i]), kABHomeLabel, NULL);
                ABRecordSetValue(person, kABPersonEmailProperty, multiEmail, &error);
                CFRelease(multiEmail);
                
                ABRecordSetValue(person, kABPersonLastNameProperty, (__bridge CFTypeRef)([NSString stringWithFormat:@"Lastname %d", i]), nil); // his last name
                
                ABMutableMultiValueRef im = ABMultiValueCreateMutable(kABMultiDictionaryPropertyType);
                NSMutableDictionary *imDict = [[NSMutableDictionary alloc] init];
                [imDict setObject:@"Skype"  forKey:(NSString*)kABPersonInstantMessageServiceKey];
                [imDict setObject:[NSString stringWithFormat:@"Skype %d", i] forKey:(NSString*)kABPersonInstantMessageUsernameKey];
                ABMultiValueAddValueAndLabel(im, (__bridge CFTypeRef)(imDict), kABHomeLabel, NULL);
                ABRecordSetValue(person, kABPersonInstantMessageProperty, im, NULL);
                CFRelease(im);
                
                ABMutableMultiValueRef multiAddress = ABMultiValueCreateMutable(kABMultiDictionaryPropertyType);
                
                NSMutableDictionary *addressDictionary = [NSMutableDictionary dictionary];
                
                [addressDictionary setObject:[NSString stringWithFormat:@"Street %d", i] forKey:(NSString *) kABPersonAddressStreetKey];
                
                [addressDictionary setObject:[NSString stringWithFormat:@"City %d", i] forKey:(NSString *)kABPersonAddressCityKey];
                
                [addressDictionary setObject:[NSString stringWithFormat:@"Zip %d", i] forKey:(NSString *)kABPersonAddressZIPKey];
                
                [addressDictionary setObject:[NSString stringWithFormat:@"Country %d", i] forKey:(NSString *)kABPersonAddressCountryKey];
                
                ABMultiValueAddValueAndLabel(multiAddress, (__bridge CFTypeRef)(addressDictionary), kABWorkLabel, NULL);
                ABRecordSetValue(person, kABPersonAddressProperty, multiAddress, &error);
                CFRelease(multiAddress);
                
                //    if (contact)
                {
                    ABAddressBookAddRecord(addressBookRef, person, nil); //add the new person to the record
                }
                //    }
                
                //NSLog(@"%@", (__bridge NSError*) error);
                
                CFRelease(person);
                
//                dispatch_async(dispatch_get_main_queue(), ^{
//                    updateBlock(((float) i) / NUMBER_OF_FAKE_CONTACTS);
//                });
            }
            
            CFErrorRef error = nil;
            
            //if (ABAddressBookHasUnsavedChanges(addressBook))
            {
                
            }
            
            ABAddressBookSave(addressBookRef, &error); //save the record
            
            CFRelease(addressBookRef);
            
            dispatch_async(dispatch_get_main_queue(), ^{
                block();
            });
        } andWithFailedBlock:^{
            dispatch_async(dispatch_get_main_queue(), ^{
                block();
            });
        }];
    });
}

- (void)removeAllContacts
{
    dispatch_async(peopleManagerQueue, ^{
        
        ABAddressBookRef addressBookRef = ABAddressBookCreateWithOptions(NULL, NULL);
        
        [self accessAddressBookDatabase:addressBookRef withSuccessfulBlock:^{
            NSArray *people = (__bridge NSArray *)ABAddressBookCopyArrayOfAllPeople(addressBookRef);
            
            NSInteger count = [people count];
            NSLog(@"People count %i", count);
            for (int i = 0; i < count; i++)
            {
                ABRecordRef record = (__bridge ABRecordRef)(people[i]);
                
                ABAddressBookRemoveRecord(addressBookRef, record, nil);
                NSLog(@"Record removed");
            }
            
            ABAddressBookSave(addressBookRef, nil);
            NSLog(@"Record saved");
            
            CFRelease(addressBookRef);
        } andWithFailedBlock:^{
            
        }];
    });
}

- (void)removeAllFakeContacts
{
    dispatch_async(peopleManagerQueue, ^{
        
        ABAddressBookRef addressBookRef = ABAddressBookCreateWithOptions(NULL, NULL);
        
        [self accessAddressBookDatabase:addressBookRef withSuccessfulBlock:^{
            NSArray *people = (__bridge NSArray *)ABAddressBookCopyPeopleWithName(addressBookRef, (__bridge CFStringRef)@"Name");
            
            NSInteger count = [people count];
            NSLog(@"People count %i", count);
            for (int i = 0; i < count; i++)
            {
                ABRecordRef record = (__bridge ABRecordRef)(people[i]);
                
                ABAddressBookRemoveRecord(addressBookRef, record, nil);
                NSLog(@"Record removed");
            }
            
            ABAddressBookSave(addressBookRef, nil);
            NSLog(@"Record saved");
            
            CFRelease(addressBookRef);
        } andWithFailedBlock:^{
            
        }];
    });
}

- (void)updateContactsWithCompletionBlock:(completeBlock)block
{
    Favorite *favoriteTable = [[DataManager sharedInstance] getEntityWithName:@"Favorite"];
    
    if (!favoriteTable)
    {
        favoriteTable = [[DataManager sharedInstance] createEntityWithName:@"Favorite"];
    }
    
    NSArray *favorites = favoriteTable.contacts;
    NSMutableDictionary *emails = [NSMutableDictionary dictionaryWithDictionary:favoriteTable.emails];
    
    if ([favorites count])
    {
        for (NSString *favorite in favorites)
        {
            NSPredicate *predicate = [NSPredicate predicateWithFormat:@"recId == %@", @([favorite integerValue])];
            
            NSArray *filteredArray = [contacts filteredArrayUsingPredicate:predicate];
            
            for (GRContact *contact in filteredArray)
            {
                contact.favorite = YES;
            }
        }
    }
    
    if ([emails count])
    {
        NSArray *keys = [emails allKeys];
        
        for (NSString *key in keys)
        {
            NSPredicate *predicate = [NSPredicate predicateWithFormat:@"recId == %@", @([key integerValue])];
            
            NSArray *filteredArray = [contacts filteredArrayUsingPredicate:predicate];
            
            for (GRContact *contact in filteredArray)
            {
                contact.selectedEmail = [emails objectForKey:[contact.recId stringValue]];
            }
        }
    }
    
    NSSortDescriptor *favDescriptor = [[NSSortDescriptor alloc] initWithKey:@"favorite" ascending:NO];
    NSSortDescriptor *fullnameDescriptor = [[NSSortDescriptor alloc] initWithKey:@"fullContactName" ascending:YES selector:@selector(numbersLastComparing:)];
    
    NSArray *descriptors = [NSArray arrayWithObjects:favDescriptor, fullnameDescriptor, nil];
    
    [self.contacts sortUsingDescriptors:descriptors];
    
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"favorite == YES"];
    
    self.favoriteContacts = [[self.contacts filteredArrayUsingPredicate:predicate] mutableCopy];
    
    self.otherContacts = [self.contacts mutableCopy];
    [self.otherContacts removeObjectsInArray:self.favoriteContacts];
    //[self.contactsWithMail removeObjectsInArray:self.favoriteContacts];
    
    if (block)
        block();
}

- (void)removeContactFromAddressBook:(GRContact*)contact withCompletionBlock:(completeBlock)complitionBlock
{
    dispatch_async(peopleManagerQueue, ^{
        ABAddressBookRef addressBookRef = ABAddressBookCreateWithOptions(NULL, NULL);
        
        [self accessAddressBookDatabase:addressBookRef withSuccessfulBlock:^{
            CFErrorRef error = nil;
            
            ABRecordRef person = ABAddressBookGetPersonWithRecordID(addressBookRef, [contact.recId integerValue]);
            
            ABAddressBookRemoveRecord(addressBookRef, person, &error);
            
            ABAddressBookSave(addressBookRef, &error);
            
            [self.contacts removeObject:contact];
            [self.otherContacts removeObject:contact];
            [self.favoriteContacts removeObject:contact];
            
            dispatch_async(dispatch_get_main_queue(), ^{
               complitionBlock();
            });
        } andWithFailedBlock:^{
            dispatch_async(dispatch_get_main_queue(), ^{
                complitionBlock();
            });
        }];
    });
}

- (void)createContactWithInfo:(NSDictionary*)info
          withCompletionBlock:(successfulCreationBlock)completeBlock
              withFailedBlock:(completeBlock)failedBlock
{
    dispatch_async(peopleManagerQueue, ^{
        ABAddressBookRef addressBookRef = ABAddressBookCreateWithOptions(NULL, NULL);
        
        [self accessAddressBookDatabase:addressBookRef withSuccessfulBlock:^{
            CFErrorRef error = nil;
            
            ABRecordRef person = NULL;
            
            if (!info[@"recId"])
            {
                person = ABPersonCreate();
            }
            else
            {
                person = ABAddressBookGetPersonWithRecordID(addressBookRef, [info[@"recId"] integerValue]);
            }
            
            if (info[@"mobilePhone"])
            {
                ABMutableMultiValueRef phoneNumberMultiValue = ABMultiValueCreateMutable(kABMultiStringPropertyType);
                ABMultiValueAddValueAndLabel(phoneNumberMultiValue, (__bridge CFTypeRef)(info[@"mobilePhone"]), kABPersonPhoneMobileLabel, NULL);
                ABRecordSetValue(person, kABPersonPhoneProperty, phoneNumberMultiValue, nil); // set the phone number property
                CFRelease(phoneNumberMultiValue);
            }
            
            if (info[@"mobileiPhone"])
            {
                ABMutableMultiValueRef phoneNumberMultiValue = ABMultiValueCreateMutable(kABMultiStringPropertyType);
                ABMultiValueAddValueAndLabel(phoneNumberMultiValue, (__bridge CFTypeRef)(info[@"mobileiPhone"]), kABPersonPhoneIPhoneLabel, NULL);
                ABRecordSetValue(person, kABPersonPhoneProperty, phoneNumberMultiValue, nil); // set the phone number property
                CFRelease(phoneNumberMultiValue);
            }
            
            if (info[@"mobileHome"])
            {
                ABMutableMultiValueRef phoneNumberMultiValue = ABMultiValueCreateMutable(kABMultiStringPropertyType);
                ABMultiValueAddValueAndLabel(phoneNumberMultiValue, (__bridge CFTypeRef)(info[@"mobileHome"]), kABHomeLabel, NULL);
                ABRecordSetValue(person, kABPersonPhoneProperty, phoneNumberMultiValue, nil); // set the phone number property
                CFRelease(phoneNumberMultiValue);
            }
            
            if (info[@"mobileWork"])
            {
                ABMutableMultiValueRef phoneNumberMultiValue = ABMultiValueCreateMutable(kABMultiStringPropertyType);
                ABMultiValueAddValueAndLabel(phoneNumberMultiValue, (__bridge CFTypeRef)(info[@"mobileWork"]), kABWorkLabel, NULL);
                ABRecordSetValue(person, kABPersonPhoneProperty, phoneNumberMultiValue, nil); // set the phone number property
                CFRelease(phoneNumberMultiValue);
            }
            
            if (info[@"mobileMain"])
            {
                ABMutableMultiValueRef phoneNumberMultiValue = ABMultiValueCreateMutable(kABMultiStringPropertyType);
                ABMultiValueAddValueAndLabel(phoneNumberMultiValue, (__bridge CFTypeRef)(info[@"mobileMain"]), kABPersonPhoneMainLabel, NULL);
                ABRecordSetValue(person, kABPersonPhoneProperty, phoneNumberMultiValue, nil); // set the phone number property
                CFRelease(phoneNumberMultiValue);
            }
            
            if (info[@"faxHome"])
            {
                ABMutableMultiValueRef phoneNumberMultiValue = ABMultiValueCreateMutable(kABMultiStringPropertyType);
                ABMultiValueAddValueAndLabel(phoneNumberMultiValue, (__bridge CFTypeRef)(info[@"faxHome"]), kABPersonPhoneHomeFAXLabel, NULL);
                ABRecordSetValue(person, kABPersonPhoneProperty, phoneNumberMultiValue, nil); // set the phone number property
                CFRelease(phoneNumberMultiValue);
            }
            
            if (info[@"faxWork"])
            {
                ABMutableMultiValueRef phoneNumberMultiValue = ABMultiValueCreateMutable(kABMultiStringPropertyType);
                ABMultiValueAddValueAndLabel(phoneNumberMultiValue, (__bridge CFTypeRef)(info[@"faxWork"]), kABPersonPhoneWorkFAXLabel, NULL);
                ABRecordSetValue(person, kABPersonPhoneProperty, phoneNumberMultiValue, nil); // set the phone number property
                CFRelease(phoneNumberMultiValue);
            }
            
            if (info[@"faxOther"])
            {
                ABMutableMultiValueRef phoneNumberMultiValue = ABMultiValueCreateMutable(kABMultiStringPropertyType);
                ABMultiValueAddValueAndLabel(phoneNumberMultiValue, (__bridge CFTypeRef)(info[@"faxOther"]), kABPersonPhoneOtherFAXLabel, NULL);
                ABRecordSetValue(person, kABPersonPhoneProperty, phoneNumberMultiValue, nil); // set the phone number property
                CFRelease(phoneNumberMultiValue);
            }
            
            if (info[@"pager"])
            {
                ABMutableMultiValueRef phoneNumberMultiValue = ABMultiValueCreateMutable(kABMultiStringPropertyType);
                ABMultiValueAddValueAndLabel(phoneNumberMultiValue, (__bridge CFTypeRef)(info[@"pager"]), kABPersonPhonePagerLabel, NULL);
                ABRecordSetValue(person, kABPersonPhoneProperty, phoneNumberMultiValue, nil); // set the phone number property
                CFRelease(phoneNumberMultiValue);
            }
            
            if (info[@"name"])
            {
                ABRecordSetValue(person, kABPersonFirstNameProperty, (__bridge CFTypeRef)(info[@"name"]), nil);
            }
            
            if (info[@"lastname"])
            {
                ABRecordSetValue(person, kABPersonLastNameProperty, (__bridge CFTypeRef)(info[@"lastname"]), nil);
            }
            
            ABMutableMultiValueRef multiEmail = ABMultiValueCreateMutable(kABMultiStringPropertyType);
            
            if (info[@"email"])
            {
                ABMultiValueAddValueAndLabel(multiEmail, (__bridge CFTypeRef)(info[@"email"]), kABHomeLabel, NULL);
            }
            
            if (info[@"email1"])
            {
                ABMultiValueAddValueAndLabel(multiEmail, (__bridge CFTypeRef)(info[@"email1"]), kABWorkLabel, NULL);
            }
            
            if (info[@"email2"])
            {
                ABMultiValueAddValueAndLabel(multiEmail, (__bridge CFTypeRef)(info[@"email2"]), kABOtherLabel, NULL);
            }
            
            if (info[@"email3"])
            {
                ABMultiValueAddValueAndLabel(multiEmail, (__bridge CFTypeRef)(info[@"email3"]), kABOtherLabel, NULL);
            }
            
            if (info[@"email4"])
            {
                ABMultiValueAddValueAndLabel(multiEmail, (__bridge CFTypeRef)(info[@"email4"]), kABOtherLabel, NULL);
            }
            
            if (info[@"email5"])
            {
                ABMultiValueAddValueAndLabel(multiEmail, (__bridge CFTypeRef)(info[@"email5"]), kABOtherLabel, NULL);
            }
            
            ABRecordSetValue(person, kABPersonEmailProperty, multiEmail, &error);
            
            CFRelease(multiEmail);
            
            if (info[@"skype"])
            {
                ABMutableMultiValueRef im = ABMultiValueCreateMutable(kABMultiDictionaryPropertyType);
                NSMutableDictionary *imDict = [[NSMutableDictionary alloc] init];
                [imDict setObject:@"Skype"  forKey:(NSString*)kABPersonInstantMessageServiceKey];
                [imDict setObject:info[@"skype"] forKey:(NSString*)kABPersonInstantMessageUsernameKey];
                ABMultiValueAddValueAndLabel(im, (__bridge CFTypeRef)(imDict), kABHomeLabel, NULL);
                ABRecordSetValue(person, kABPersonInstantMessageProperty, im, NULL);
                CFRelease(im);
            }
            
            if (info[@"icon"])
            {
                NSData *imgData = info[@"iconData"];
                
                if (imgData)
                {
                    ABPersonSetImageData(person, (__bridge CFDataRef)imgData, nil);
                }
            }
            
            ABMutableMultiValueRef multiAddress = ABMultiValueCreateMutable(kABMultiDictionaryPropertyType);
            
            NSMutableDictionary *addressDictionary = [NSMutableDictionary dictionary];
            
            if (info[@"street"])
            {
                [addressDictionary setObject:info[@"street"] forKey:(NSString *) kABPersonAddressStreetKey];
            }
            
            if (info[@"city"])
            {
                [addressDictionary setObject:info[@"city"] forKey:(NSString *)kABPersonAddressCityKey];
            }
            
            if (info[@"zip"])
            {
                [addressDictionary setObject:info[@"zip"] forKey:(NSString *)kABPersonAddressZIPKey];
            }
            
            if (info[@"country"])
            {
                [addressDictionary setObject:info[@"country"] forKey:(NSString *)kABPersonAddressCountryKey];
            }
            
            ABMultiValueAddValueAndLabel(multiAddress, (__bridge CFTypeRef)(addressDictionary), kABWorkLabel, NULL);
            ABRecordSetValue(person, kABPersonAddressProperty, multiAddress, &error);
            CFRelease(multiAddress);
            
            if (!info[@"recId"])
            {
                bool bResult = ABAddressBookAddRecord(addressBookRef, person, nil);
                
                if (bResult)
                {
                    NSLog(@"persion was added");
                }
                else
                {
                    NSLog(@"persion wasn't added");
                }
            }
            
            error = NULL;
            
            NSInteger recId = -1;
            
            if (ABAddressBookHasUnsavedChanges(addressBookRef))
            {
                bool bSaveResult = ABAddressBookSave(addressBookRef, &error); //save the record
                
                if (bSaveResult)
                {
                    recId = ABRecordGetRecordID(person);
                    
                    NSLog(@"person was saved");
                    
                    if (info[@"icon"])
                    {
                        NSData *imgData = info[@"iconData"];
                        
                        if (imgData)
                        {
                            NSArray *paths = NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES);
                            NSString *dirPath = [paths objectAtIndex:0];
                            
                            NSString *fileName = [[NSString stringWithFormat:@"%i", recId] stringByAppendingPathExtension:@"png"];
                            
                            dirPath = [dirPath stringByAppendingPathComponent:fileName];
                            
                            [[NSFileManager defaultManager] createFileAtPath:dirPath contents:imgData attributes:nil];
                        }
                    }
                    
                    if (!info[@"recId"])
                    {
                        GRContact *contact = [GRContact contactWithRecord:person];
                        
                        contact.favorite = [info[@"favorite"] boolValue];
                        
                        [self supplementContactsWithMailWithContact:contact];
                        [self.contacts addObject:contact];
                        [self.otherContacts addObject:contact];
                    }
                }
                else
                {
                    NSLog(@"person wasn't saved, error = %@", error);
                }
            }
            else
            {
                NSLog(@"There were no changes");
            }
            
            if (!info[@"recId"])
            {
                recId = ABRecordGetRecordID(person);
                CFRelease(person);
            }
            else
            {
                recId = [info[@"recId"] integerValue];
            }
            
            CFRelease(addressBookRef);
            
            dispatch_async(dispatch_get_main_queue(), ^{
                completeBlock(recId);
            });
        } andWithFailedBlock:^{
            dispatch_async(dispatch_get_main_queue(), ^{
                failedBlock();
            });
        }];
    });
}

- (void)updateContactSelectedMailForRecId:(NSNumber *)recId selectedMail:(NSString *)selectedMail {
    dispatch_queue_t myQueue = dispatch_queue_create("Update contact mail",NULL);
    dispatch_async(myQueue, ^{
        for (GRContact *contact in contactsWithMail) {
            if ([contact.recId isEqualToNumber:recId]) {
                contact.selectedEmail = selectedMail;
                break;
            }
        }
    });
}

- (void)supplementContactsWithMailWithContact:(GRContact *)contact
{
    if (contact.fullname.length && contact.email && contact.email.length) {
        if ([self validEmail:contact.email]) {
            [contactsWithMail addObject:contact];
        }
    }
}

@end
