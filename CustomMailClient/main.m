//
//  main.m
//  CustomMailClient
//
//  Created by Ievgen Pavliuk on 4/10/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "AppDelegate.h"
#import "GRApplication.h"

int main(int argc, char *argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, NSStringFromClass([GRApplication class]), NSStringFromClass([AppDelegate class]));
    }
}

