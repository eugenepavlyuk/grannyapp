//
//  AppDelegate.m
//  CustomMailClient
//
//  Created by Ievgen Pavliuk on 4/10/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "AppDelegate.h"
#import "Favorite.h"
#import "DataManager.h"
#import "AlarmManager.h"
#import "GRMailAccountManager.h"
#import "GRApplication.h"
#import "TVChannel.h"
#import "GRPeopleManager.h"
#import "TestFlight.h"
#import "GRMailServiceManager.h"
#import "SendingMessage.h"
#import "DataManager.h"
#import "DBMailAccount.h"
#import "GRRadioDataManager.h"
//#import <MailCore/MailCore.h>
#import "GAI.h"
#import "WYPopoverController.h"

@implementation AppDelegate

@synthesize window = _window;

- (void)dealloc
{
    [internetReach release];
    [_window release];
    [super dealloc];
}

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
#ifndef DEBUG
    [TestFlight setDeviceIdentifier:[[[UIDevice currentDevice] identifierForVendor] UUIDString]];
    
#ifndef INTERNAL_BUILD
	[TestFlight takeOff:@"0fb30447-4c70-47d7-8486-e25585455704"];
#else
    [TestFlight takeOff:@"0fb30447-4c70-47d7-8486-e25585455704"];
#endif
#endif

    
    [application setStatusBarHidden:YES];
    
    id favoriteTable = [[DataManager sharedInstance] getEntityWithName:kFavoriteEntity];
    
    if (!favoriteTable)
    {
        favoriteTable = [[DataManager sharedInstance] createEntityWithName:kFavoriteEntity];
    }
    
    [[DataManager sharedInstance] save];
    
    self.globalCalendar = [[NSCalendar currentCalendar] retain];
    
#ifndef DEBUG
    // Optional: automatically send uncaught exceptions to Google Analytics.
    [GAI sharedInstance].trackUncaughtExceptions = YES;
    
    // Optional: set Google Analytics dispatch interval to e.g. 20 seconds.
    [GAI sharedInstance].dispatchInterval = 20;
    
    // Optional: set Logger to VERBOSE for debug information.
    [[[GAI sharedInstance] logger] setLogLevel:kGAILogLevelVerbose];
    
    // Initialize tracker.
    [[GAI sharedInstance] trackerWithTrackingId:@"UA-43769706-1"];
#endif
    
    [DataManager sharedInstance];
    
    //[UIFont logAllFonts];
    
    [self initialyzeAudioBackground];

    self.window = [[[GRMainWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]] autorelease];
    // Override point for customization after application launch.
    self.window.backgroundColor = [UIColor whiteColor];
    [self.window makeKeyAndVisible];
    
    //[[GRMailAccountManager manager] createFakePeople];
    
    GRApplication *app = (GRApplication*)application;
    [app enableTimer];
    
    [[NSNotificationCenter defaultCenter] addObserver: self selector: @selector(reachabilityChanged:) name: kYLReachabilityChangedNotification object: nil];
    internetReach = [[YLReachability reachabilityForInternetConnection] retain];
    [internetReach startNotifier];;
    
    [self checkWaitingMessageForSend];

#ifdef LOAD_RADIO_CHANNELS_FROM_CVS

    //[RADIO_CSV_LOADER startReadDataFormFranceCSV:@"france3"];
    [RADIO_CSV_LOADER startReadDataFormUkCSV:@"uk3"];

#else
    
    if (![[[NSUserDefaults standardUserDefaults] objectForKey:@"radioStationSaved"] boolValue])
    {
        [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"radioStationSaved"];
        [[NSUserDefaults standardUserDefaults] synchronize];
        
        
        NSMutableDictionary *radioStation = [NSMutableDictionary dictionary];
        [radioStation setObject:@"BBC WORLD" forKey:@"name"];
        [radioStation setObject:[NSDictionary dictionaryWithObject:@"mms://a973.l3944038972.c39440.g.lm.akamaistream.net/D/973/39440/v0001/reflector:38972" forKey:@"stream_url"] forKey:@"streams"];
        [radioStation setObject:@"1000" forKey:@"sid"];
        if (![[GRRadioDataManager shared] isStationAxists:@"1000"])
            [[GRRadioDataManager shared] saveRadioStation:radioStation];
        //
        //        [radioStation setObject:@"BBC World Service" forKey:@"name"];
        //        [radioStation setObject:[NSDictionary dictionaryWithObject:@"http://www.bbc.co.uk/worldservice/meta/live/mp3/eneuk.pls" forKey:@"stream_url"] forKey:@"streams"];
        //        [radioStation setObject:@"1001" forKey:@"sid"];
        //        if (![[GRRadioDataManager shared] isStationAxists:@"1001"])
        //            [[GRRadioDataManager shared] saveRadioStation:radioStation];
        //
        [radioStation setObject:@"NPR News" forKey:@"name"];
        [radioStation setObject:[NSDictionary dictionaryWithObject:@"http://69.166.45.47:8000" forKey:@"stream_url"] forKey:@"streams"];
        [radioStation setObject:@"1001" forKey:@"sid"];
        if (![[GRRadioDataManager shared] isStationAxists:@"1001"])
            [[GRRadioDataManager shared] saveRadioStation:radioStation];
        
        [radioStation setObject:@"BBC Radio 3" forKey:@"name"];
        [radioStation setObject:[NSDictionary dictionaryWithObject:@"http://www.bbc.co.uk/radio/listen/live/r3.asx" forKey:@"stream_url"] forKey:@"streams"];
        [radioStation setObject:@"1002" forKey:@"sid"];
        if (![[GRRadioDataManager shared] isStationAxists:@"1002"])
            [[GRRadioDataManager shared] saveRadioStation:radioStation];
        
        
        [radioStation setObject:@"Classic FM" forKey:@"name"];
        [radioStation setObject:[NSDictionary dictionaryWithObject:@"http://media-ice.musicradio.com/ClassicFMMP3.m3u" forKey:@"stream_url"] forKey:@"streams"];
        [radioStation setObject:@"1003" forKey:@"sid"];
        if (![[GRRadioDataManager shared] isStationAxists:@"1003"])
            [[GRRadioDataManager shared] saveRadioStation:radioStation];
        
        
        [radioStation setObject:@"The Music Channel" forKey:@"name"];
        [radioStation setObject:[NSDictionary dictionaryWithObject:@"http://media.kcrw.com/live/kcrwmusic.pls" forKey:@"stream_url"] forKey:@"streams"];
        [radioStation setObject:@"1004" forKey:@"sid"];
        if (![[GRRadioDataManager shared] isStationAxists:@"1004"])
            [[GRRadioDataManager shared] saveRadioStation:radioStation];
        
        
        [radioStation setObject:@"BBC 1Xtra" forKey:@"name"];
        [radioStation setObject:[NSDictionary dictionaryWithObject:@"http://www.bbc.co.uk/1xtra/realmedia/1xtra.asx" forKey:@"stream_url"] forKey:@"streams"];
        [radioStation setObject:@"1005" forKey:@"sid"];
        if (![[GRRadioDataManager shared] isStationAxists:@"1005"])
            [[GRRadioDataManager shared] saveRadioStation:radioStation];
        
        
        [radioStation setObject:@"ABC Punto Radio" forKey:@"name"];
        [radioStation setObject:[NSDictionary dictionaryWithObject:@"http://94.23.22.181:8031/listen.pls" forKey:@"stream_url"] forKey:@"streams"];
        [radioStation setObject:@"1006" forKey:@"sid"];
        if (![[GRRadioDataManager shared] isStationAxists:@"1006"])
            [[GRRadioDataManager shared] saveRadioStation:radioStation];
        
        
        [radioStation setObject:@"Apollo Radio" forKey:@"name"];
        [radioStation setObject:[NSDictionary dictionaryWithObject:@"http://stream.apolloradio.de/APOLLO/mp3.pls" forKey:@"stream_url"] forKey:@"streams"];
        [radioStation setObject:@"1007" forKey:@"sid"];
        if (![[GRRadioDataManager shared] isStationAxists:@"1007"])
            [[GRRadioDataManager shared] saveRadioStation:radioStation];
        
        
        [radioStation setObject:@"Jazz FM" forKey:@"name"];
        [radioStation setObject:[NSDictionary dictionaryWithObject:@"http://listen.onmyradio.net:8002/listen.pls" forKey:@"stream_url"] forKey:@"streams"];
        [radioStation setObject:@"1008" forKey:@"sid"];
        if (![[GRRadioDataManager shared] isStationAxists:@"1008"])
            [[GRRadioDataManager shared] saveRadioStation:radioStation];
        
        
        NSString *path = [[NSBundle mainBundle] pathForResource:@"RadioStations" ofType:@"plist"];
        
        NSMutableArray *array = [[NSMutableArray alloc] initWithContentsOfFile:path];
        
        for (NSDictionary *dict in array)
        {
            [[DataManager sharedInstance] addRadioChannel:dict];
        }
                
        [[DataManager sharedInstance] save];
        
        [array release];
    }
#endif
    
    [[AlarmManager sharedInstance] accessToCalendarStore];
    
    return YES;
}

- (void)applicationWillResignActive:(UIApplication *)application
{
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
    
    GRApplication *app = (GRApplication*)application;
    [app disableTimer];
}

- (void)applicationDidEnterBackground:(UIApplication *)application
{
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later. 
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    
    [[GRPeopleManager sharedManager] registerForExternalChanges];
    
    GRApplication *app = (GRApplication*)application;
    [app disableTimer];
    
    NSMutableArray *sendingMessagesArray = [NSMutableArray arrayWithArray:[[DataManager sharedInstance] allRowsForEntity:@"SendingMessage"]];
    if (sendingMessagesArray.count)
    {
        if ([[GRMailServiceManager shared].currentOperation isExecuting])
        {
            [[DataManager sharedInstance] removeEntityModel:[sendingMessagesArray objectAtIndex:0]];
            [sendingMessagesArray removeObjectAtIndex:0];
        }
    }
//    if ([GRMailServiceManager shared].currentOperation && [[GRMailServiceManager shared].currentOperation isKindOfClass:GRSendMsgOperation.class])
//    {
//        [[GRMailServiceManager shared].currentOperation cancel];
//        [[GRMailServiceManager shared].sendingMessagesQueue cancelAllOperations];
//    }
}

- (void)applicationWillEnterForeground:(UIApplication *)application
{
    [[GRPeopleManager sharedManager] unregisterForExternalChanges];
    
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
    
    GRApplication *app = (GRApplication*)application;
    [app enableTimer];
    
    [self checkWaitingMessageForSend];
}

- (void)applicationDidBecomeActive:(UIApplication *)application
{
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    
    GRApplication *app = (GRApplication*)application;
    [app enableTimer];
    
    [[AlarmManager sharedInstance] updateAllEvents];
}

- (void)applicationWillTerminate:(UIApplication *)application
{    
    GRApplication *app = (GRApplication*)application;
    [app disableTimer];
}

- (void)application:(UIApplication *)application didReceiveLocalNotification:(UILocalNotification *)notification
{
    
}

#pragma mark - Helpers

- (void) displayAlertWithMessage:(NSString *)message {
    GRCommonAlertView *alert = [[[GRCommonAlertView alloc] initCommonAlert:message inView:self.window] autorelease];
    alert.delegate = nil;
}

- (void) reachabilityChanged: (NSNotification* )note
{
	YLReachability* curReach = [note object];
	NSParameterAssert([curReach isKindOfClass: [YLReachability class]]);

	if ([curReach currentReachabilityStatus] == kNotReachable)
    {
//        if ([GRMailServiceManager shared].currentOperation && [[GRMailServiceManager shared].currentOperation isKindOfClass:GRSendMsgOperation.class])
//        {
//            connectionLost = YES;
//            GRCommonAlertView *alert = [[[GRCommonAlertView alloc] initCommonAlert:NSLocalizedString(@"There is no internet connection. Your message will be sent later", nil) inView:self.window] autorelease];
//            alert.delegate = nil;
//        }
    }
    else
    {
//        if (connectionLost)
//        {
//            connectionLost = NO;
//            [self checkWaitingMessageForSend];
//        }
        [self checkWaitingMessageForSend];
    }
}
- (void) checkWaitingMessageForSend
{
//    NSMutableArray *sendingMessagesArray = [NSMutableArray arrayWithArray:[[DataManager sharedInstance] allRowsForEntity:@"SendingMessage"]];
//    if (sendingMessagesArray.count)
//    {
//        if ([[GRMailServiceManager shared].currentOperation isExecuting])
//        {
//            [[DataManager sharedInstance] removeentityModel:[sendingMessagesArray objectAtIndex:0]];
//            [sendingMessagesArray removeObjectAtIndex:0];
//        }
//        for (SendingMessage *message in sendingMessagesArray)
//        {
//            DBMailAccount *mailAccount = [NSKeyedUnarchiver unarchiveObjectWithData:message.account];
//            NSDictionary *msgData = [NSKeyedUnarchiver unarchiveObjectWithData:message.message];
//            
//            [[GRMailServiceManager shared] mailActionSendMessageWithData:msgData account:(TMailAccount *)mailAccount];
//        }
//    }
    NSNotificationCenter *notificationCenter = [NSNotificationCenter defaultCenter];
    [notificationCenter postNotificationName:@"networkStatusRestored"
                                      object:nil
                                    userInfo:nil];
}
- (void)initialyzeAudioBackground {
    AVAudioSession *audioSession = [AVAudioSession sharedInstance];
    
    NSError *setCategoryError = nil;
    [audioSession setCategory:AVAudioSessionCategoryPlayback error:&setCategoryError];
    if (setCategoryError) { /* handle the error condition */ }
    
    NSError *activationError = nil;
    [audioSession setActive:YES error:&activationError];
    if (activationError) { /* handle the error condition */ }
}

#pragma mark - Singleton

+ (AppDelegate *)getInstance 
{
    return (AppDelegate *)[[UIApplication sharedApplication] delegate];
}

@end
