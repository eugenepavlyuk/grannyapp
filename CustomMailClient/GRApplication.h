//
//  GRApplication.h
//  GrannyApp
//
//  Created by Eugene Pavlyuk on 8/7/12.
//  Copyright (c) 2012 Home. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface GRApplication : UIApplication

- (void)enableTimer;
- (void)disableTimer;

@end
