//
//  GRApplication.m
//  GrannyApp
//
//  Created by Eugene Pavlyuk on 8/7/12.
//  Copyright (c) 2012 Home. All rights reserved.
//

#import "GRApplication.h"
#import "GRScreenSaverBaseViewController.h"
#import "DataManager.h"
#import "Settings.h"
#import "AppDelegate.h"
#import "GRInternetViewController.h"

@interface GRApplication()

@property (nonatomic, retain) GRScreenSaverBaseViewController *screenSaverBaseViewController;

@end


@implementation GRApplication
{
    NSTimer *timer;
}

@synthesize screenSaverBaseViewController;

- (void)sendEvent:(UIEvent *)event
{
    [super sendEvent:event];
    
    //[self.screenSaverBaseViewController dismissModalViewControllerAnimated:NO];
    [AppDelegate getInstance].window.rootViewController = [AppDelegate getInstance].window.navController;
    self.screenSaverBaseViewController = nil;
    
    [self renewTimer];
}

- (void)disableTimer
{
    if (timer)
    {
        if ([timer isValid])
        {
            [timer invalidate];
        }
        
        [timer release];
        timer = nil;
    }
}

- (void)enableTimer
{
    [self disableTimer];
    
    Settings *settings = [[DataManager sharedInstance] getEntityWithName:kSettingsEntityName];
    
    timer = [[NSTimer scheduledTimerWithTimeInterval:[settings.duration intValue] * 60 target:self selector:@selector(startScreenSaver) userInfo:nil repeats:NO] retain];
}

- (void)renewTimer
{
    [self enableTimer];
}

- (void)startScreenSaver
{
    Settings *settings = [[DataManager sharedInstance] getEntityWithName:kSettingsEntityName];
    
    if ([settings.screenSaverState boolValue] && !self.screenSaverBaseViewController)
    {
        self.screenSaverBaseViewController = [GRScreenSaverBaseViewController screensaver];
//        screenSaverBaseViewController.modalTransitionStyle = UIModalTransitionStyleCrossDissolve;
//        [[AppDelegate getInstance].window.rootViewController presentModalViewController:screenSaverBaseViewController animated:YES];
        
        [AppDelegate getInstance].window.rootViewController = self.screenSaverBaseViewController;
    }
}

- (BOOL)openURL:(NSURL *)url
{
    if  ([self handleOpenURL:url])
    {
        GRInternetViewController *internetViewController = [[GRInternetViewController alloc] init];
        [[AppDelegate getInstance].window.navController pushViewController:internetViewController animated:YES];
        [internetViewController startLoadAddress:url.absoluteString];
        [internetViewController release];
        return YES;
    }
    else
        return [super openURL:url];
}

- (BOOL)handleOpenURL:(NSURL*)url {
    NSLog(@"my url handler");
    
    if ([[url scheme] isEqualToString:@"tel"])
    {
        return NO;
    }
    else if ([[url scheme] isEqualToString:@"skype"])
    {
        return NO;
    }
    else if ([[url scheme] isEqualToString:@"mailto"])
    {
        return NO;
    }
    else if ([[url host] isEqualToString:@"www.surveymonkey.com"])
    {
        return NO;
    }
    
    return YES;
}

- (void)dealloc
{
    self.screenSaverBaseViewController = nil;
    [super dealloc];
}

@end
